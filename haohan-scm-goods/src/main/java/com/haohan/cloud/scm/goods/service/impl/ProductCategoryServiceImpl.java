/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.ProductCategory;
import com.haohan.cloud.scm.goods.mapper.ProductCategoryMapper;
import com.haohan.cloud.scm.goods.service.ProductCategoryService;
import org.springframework.stereotype.Service;

/**
 * 商品库分类
 *
 * @author haohan
 * @date 2019-05-13 19:06:14
 */
@Service
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryMapper, ProductCategory> implements ProductCategoryService {

}
