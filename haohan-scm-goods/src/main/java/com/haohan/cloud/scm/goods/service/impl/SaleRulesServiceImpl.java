/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.SaleRules;
import com.haohan.cloud.scm.goods.mapper.SaleRulesMapper;
import com.haohan.cloud.scm.goods.service.SaleRulesService;
import org.springframework.stereotype.Service;

/**
 * 售卖规则
 *
 * @author haohan
 * @date 2019-05-13 18:47:37
 */
@Service
public class SaleRulesServiceImpl extends ServiceImpl<SaleRulesMapper, SaleRules> implements SaleRulesService {

}
