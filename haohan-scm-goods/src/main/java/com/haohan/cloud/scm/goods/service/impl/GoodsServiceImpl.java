/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.common.dto.SelfPageMapper;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.goods.mapper.GoodsMapper;
import com.haohan.cloud.scm.goods.service.GoodsService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-13 18:46:46
 */
@Service
@AllArgsConstructor
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    @Caching(evict = {
            @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true),
            @CacheEvict(value = ScmCacheNameConstant.GOODS_INFO_DETAIL, allEntries = true)
    })
    public boolean save(Goods entity) {
        if (StrUtil.isEmpty(entity.getGoodsSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Goods.class, NumberPrefixConstant.GOODS_SN_PR);
            entity.setGoodsSn(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true),
            @CacheEvict(value = ScmCacheNameConstant.GOODS_INFO_DETAIL, allEntries = true)
    })
    public boolean updateById(Goods entity){
        return retBool(baseMapper.updateById(entity));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true),
            @CacheEvict(value = ScmCacheNameConstant.GOODS_INFO_DETAIL, allEntries = true)
    })
    public void flushGoodsCacheById(String goodsId){
        // 刷新商品的缓存
    }

    @Override
    public Goods fetchByName(String shopId, String goodsName) {
        // 同名商品优先取上架的
        List<Goods> list = baseMapper.selectList(Wrappers.<Goods>query().lambda()
                .eq(Goods::getShopId, shopId)
                .eq(Goods::getGoodsName, goodsName)
                .orderByDesc(Goods::getIsMarketable));
        return CollUtil.isEmpty(list) ? null : list.get(0);
    }

    @Override
    public Goods fetchBySn(String goodsSn) {
        List<Goods> list = baseMapper.selectList(Wrappers.<Goods>query().lambda()
                .eq(Goods::getGoodsSn, goodsSn));
        return CollUtil.isEmpty(list) ? null : list.get(0);
    }

    @Override
    public IPage<GoodsExtDTO> findPage(GoodsSqlDTO query) {
        return SelfPageMapper.findPage(query, baseMapper);
    }
}
