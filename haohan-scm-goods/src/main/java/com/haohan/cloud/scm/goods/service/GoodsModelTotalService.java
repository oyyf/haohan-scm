/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;

import java.util.List;

/**
 * 商品规格名称
 *
 * @author haohan
 * @date 2019-05-13 18:46:29
 */
public interface GoodsModelTotalService extends IService<GoodsModelTotal> {

    List<GoodsModelTotal> fetchTotalList(String goodsId);
}
