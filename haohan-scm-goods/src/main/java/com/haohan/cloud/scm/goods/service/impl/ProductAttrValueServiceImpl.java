/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrValue;
import com.haohan.cloud.scm.goods.mapper.ProductAttrValueMapper;
import com.haohan.cloud.scm.goods.service.ProductAttrValueService;
import org.springframework.stereotype.Service;

/**
 * 商品库属性值
 *
 * @author haohan
 * @date 2019-05-13 19:06:21
 */
@Service
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueMapper, ProductAttrValue> implements ProductAttrValueService {

}
