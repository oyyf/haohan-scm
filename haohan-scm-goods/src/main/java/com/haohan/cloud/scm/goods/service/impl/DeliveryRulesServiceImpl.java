/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.DeliveryRules;
import com.haohan.cloud.scm.goods.mapper.DeliveryRulesMapper;
import com.haohan.cloud.scm.goods.service.DeliveryRulesService;
import org.springframework.stereotype.Service;

/**
 * 配送规则
 *
 * @author haohan
 * @date 2019-05-13 18:47:18
 */
@Service
public class DeliveryRulesServiceImpl extends ServiceImpl<DeliveryRulesMapper, DeliveryRules> implements DeliveryRulesService {

}
