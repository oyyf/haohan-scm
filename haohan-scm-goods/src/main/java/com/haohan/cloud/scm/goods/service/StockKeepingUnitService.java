/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.StockKeepingUnit;

/**
 * 库存商品库
 *
 * @author haohan
 * @date 2019-05-13 19:07:17
 */
public interface StockKeepingUnitService extends IService<StockKeepingUnit> {

}
