/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;
import com.haohan.cloud.scm.api.goods.req.PlatformGoodsPriceReq;
import com.haohan.cloud.scm.goods.service.PlatformGoodsPriceService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 平台商品定价
 *
 * @author haohan
 * @date 2019-05-30 10:19:21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PlatformGoodsPrice")
@Api(value = "platformgoodsprice", tags = "platformgoodsprice内部接口服务")
public class PlatformGoodsPriceFeignApiCtrl {

    private final PlatformGoodsPriceService platformGoodsPriceService;


    /**
     * 通过id查询平台商品定价
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(platformGoodsPriceService.getById(id));
    }


    /**
     * 分页查询 平台商品定价 列表信息
     *
     * @param platformGoodsPriceReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPlatformGoodsPricePage")
    public R getPlatformGoodsPricePage(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {
        Page page = new Page(platformGoodsPriceReq.getPageNo(), platformGoodsPriceReq.getPageSize());
        PlatformGoodsPrice platformGoodsPrice = new PlatformGoodsPrice();
        BeanUtil.copyProperties(platformGoodsPriceReq, platformGoodsPrice);

        return new R<>(platformGoodsPriceService.page(page, Wrappers.query(platformGoodsPrice)));
    }


    /**
     * 全量查询 平台商品定价 列表信息
     *
     * @param platformGoodsPriceReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPlatformGoodsPriceList")
    public R getPlatformGoodsPriceList(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {
        PlatformGoodsPrice platformGoodsPrice = new PlatformGoodsPrice();
        BeanUtil.copyProperties(platformGoodsPriceReq, platformGoodsPrice);

        return new R<>(platformGoodsPriceService.list(Wrappers.query(platformGoodsPrice)));
    }


    /**
     * 新增平台商品定价
     *
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @Inner
    @SysLog("新增平台商品定价")
    @PostMapping("/add")
    public R save(@RequestBody PlatformGoodsPrice platformGoodsPrice) {
        return new R<>(platformGoodsPriceService.save(platformGoodsPrice));
    }

    /**
     * 修改平台商品定价
     *
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @Inner
    @SysLog("修改平台商品定价")
    @PostMapping("/update")
    public R updateById(@RequestBody PlatformGoodsPrice platformGoodsPrice) {
        return new R<>(platformGoodsPriceService.updateById(platformGoodsPrice));
    }

    /**
     * 通过id删除平台商品定价
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除平台商品定价")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(platformGoodsPriceService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除平台商品定价")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(platformGoodsPriceService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询平台商品定价")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(platformGoodsPriceService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询平台商品定价总记录}")
    @PostMapping("/countByPlatformGoodsPriceReq")
    public R countByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {

        return new R<>(platformGoodsPriceService.count(Wrappers.query(platformGoodsPriceReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据platformGoodsPriceReq查询一条货位信息表")
    @PostMapping("/getOneByPlatformGoodsPriceReq")
    public R getOneByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {

        return new R<>(platformGoodsPriceService.getOne(Wrappers.query(platformGoodsPriceReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param platformGoodsPriceList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PlatformGoodsPrice> platformGoodsPriceList) {

        return new R<>(platformGoodsPriceService.saveOrUpdateBatch(platformGoodsPriceList));
    }

}
