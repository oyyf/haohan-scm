/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import com.haohan.cloud.scm.api.goods.req.GoodsModelTotalReq;
import com.haohan.cloud.scm.goods.service.GoodsModelTotalService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品规格名称
 *
 * @author haohan
 * @date 2019-05-29 14:25:45
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodsmodeltotal" )
@Api(value = "goodsmodeltotal", tags = "goodsmodeltotal管理")
public class GoodsModelTotalController {

    private final GoodsModelTotalService goodsModelTotalService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodsModelTotal 商品规格名称
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsModelTotalPage(Page page, GoodsModelTotal goodsModelTotal) {
        return new R<>(goodsModelTotalService.page(page, Wrappers.query(goodsModelTotal)));
    }


    /**
     * 通过id查询商品规格名称
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsModelTotalService.getById(id));
    }

    /**
     * 新增商品规格名称
     * @param goodsModelTotal 商品规格名称
     * @return R
     */
    @SysLog("新增商品规格名称" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodsmodeltotal_add')" )
    public R save(@RequestBody GoodsModelTotal goodsModelTotal) {
        return R.failed("不支持");
    }

    /**
     * 修改商品规格名称
     * @param goodsModelTotal 商品规格名称
     * @return R
     */
    @SysLog("修改商品规格名称" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goodsmodeltotal_edit')" )
    public R updateById(@RequestBody GoodsModelTotal goodsModelTotal) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除商品规格名称
     * @param id id
     * @return R
     */
    @SysLog("删除商品规格名称" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goodsmodeltotal_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品规格名称")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodsmodeltotal_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品规格名称")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsModelTotalService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsModelTotalReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品规格名称总记录}")
    @PostMapping("/countByGoodsModelTotalReq")
    public R countByGoodsModelTotalReq(@RequestBody GoodsModelTotalReq goodsModelTotalReq) {

        return new R<>(goodsModelTotalService.count(Wrappers.query(goodsModelTotalReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsModelTotalReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsModelTotalReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsModelTotalReq")
    public R getOneByGoodsModelTotalReq(@RequestBody GoodsModelTotalReq goodsModelTotalReq) {

        return new R<>(goodsModelTotalService.getOne(Wrappers.query(goodsModelTotalReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsModelTotalList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodsmodeltotal_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<GoodsModelTotal> goodsModelTotalList) {

        return R.failed("不支持");
    }


}
