package com.haohan.cloud.scm.goods.core;

import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.vo.GoodsCategoryVO;
import com.haohan.cloud.scm.api.manage.entity.Shop;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/7
 */
public interface GoodsCategoryCoreService {

    /**
     * 分类详情
     *
     * @param categoryId
     * @return
     */
    GoodsCategoryVO fetchInfo(String categoryId);

    /**
     * 新增
     *
     * @param category
     * @return
     */
    boolean addCategory(GoodsCategory category);

    /**
     * 修改
     *
     * @param category
     * @return
     */
    boolean modifyCategory(GoodsCategory category);

    /**
     * 删除
     *
     * @param categoryId
     * @return
     */
    boolean deleteById(String categoryId);

    /**
     * 店铺的默认分类获取(没有时创建)
     *
     * @param shop shopId、merchantId
     * @return
     */
    GoodsCategory fetchDefaultCategory(Shop shop);

    GoodsCategoryVO fetchPlatformCategory(String categoryId);

    /**
     * 查询分类及所有父级
     *
     * @param categoryId
     * @param shopId
     * @return
     */
    List<GoodsCategory> findAllParentWithSelfById(String categoryId, String shopId);
}
