/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import com.haohan.cloud.scm.goods.mapper.GoodsPriceRuleMapper;
import com.haohan.cloud.scm.goods.service.GoodsPriceRuleService;
import org.springframework.stereotype.Service;

/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-13 18:46:24
 */
@Service
public class GoodsPriceRuleServiceImpl extends ServiceImpl<GoodsPriceRuleMapper, GoodsPriceRule> implements GoodsPriceRuleService {

    @Override
    public GoodsPriceRule fetchByGoodsId(String goodsId) {
        return baseMapper.selectList(Wrappers.<GoodsPriceRule>query().lambda()
                .eq(GoodsPriceRule::getGoodsId, goodsId)
        ).stream()
                .findFirst().orElse(null);
    }
}
