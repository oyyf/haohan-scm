/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.dto.PlatformPriceSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;

import java.util.List;

/**
 * 平台商品定价
 *
 * @author haohan
 * @date 2019-05-13 20:38:26
 */
public interface PlatformGoodsPriceService extends IService<PlatformGoodsPrice> {

    /**
     * 查询平台定价列表
     *
     * @param query 必须 pricingPmId、merchantId、queryDate、goodsIds或goodsId
     * @return
     */
    List<PlatformGoodsPrice> fetchListByDate(PlatformPriceSqlDTO query);

    /**
     * 查询平台定价
     * 同一时间段内 一个商家下 商品规格只有一个定价
     *
     * @param query 必须 pricingPmId、merchantId、queryDate、modelId
     * @return
     */
    PlatformGoodsPrice fetchOneByDate(PlatformPriceSqlDTO query);
}
