package com.haohan.cloud.scm.goods.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.goods.req.manage.EditGoodsReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsCategoryVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsCategoryCoreService;
import com.haohan.cloud.scm.goods.core.GoodsCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author dy
 * @date 2020/6/13
 * 商品特殊处理：
 * 供应商商品分类从平台商家分类中选择
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/goods/special/")
@Api(value = "GoodsSpecialManageApiCtrl", tags = "商品特殊处理")
public class GoodsSpecialManageApiCtrl {

    private final GoodsCoreService goodsCoreService;
    private final GoodsCategoryCoreService categoryCoreService;


    @GetMapping("/category/{categoryId}")
    @ApiOperation(value = "特殊商品分类获取对应平台分类")
    public R<GoodsCategoryVO> getById(@PathVariable("categoryId") String categoryId) {
        return RUtil.success(categoryCoreService.fetchPlatformCategory(categoryId));
    }

    /**
     * 新增商品
     *
     * @param req 商品
     * @return R
     */
    @SysLog("特殊处理新增商品")
    @PostMapping("/info")
    @PreAuthorize("@pms.hasPermission('scm_goods_add')")
    public R<Boolean> addGoods(@RequestBody @Valid EditGoodsReq req) {
        req.setGoodsId(null);
        return RUtil.success(goodsCoreService.specialAddGoods(req));
    }

    /**
     * 修改商品
     *
     * @param req 商品
     * @return R
     */
    @SysLog("特殊处理修改商品")
    @PutMapping("/info")
    @PreAuthorize("@pms.hasPermission('scm_goods_edit')")
    public R<Boolean> modifyGoods(@RequestBody @Valid EditGoodsReq req) {
        if (StrUtil.isEmpty(req.getGoodsId())) {
            return R.failed("goodsId不能为空");
        }
        return RUtil.success(goodsCoreService.specialModifyGoods(req));
    }
}
