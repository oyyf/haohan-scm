/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrName;

/**
 * 商品库属性名
 *
 * @author haohan
 * @date 2019-05-13 19:06:24
 */
public interface ProductAttrNameMapper extends BaseMapper<ProductAttrName> {

}
