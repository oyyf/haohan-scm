/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.StockKeepingUnit;
import com.haohan.cloud.scm.api.goods.req.StockKeepingUnitReq;
import com.haohan.cloud.scm.goods.service.StockKeepingUnitService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 库存商品库
 *
 * @author haohan
 * @date 2019-05-29 14:27:56
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/StockKeepingUnit")
@Api(value = "stockkeepingunit", tags = "stockkeepingunit内部接口服务")
public class StockKeepingUnitFeignApiCtrl {

    private final StockKeepingUnitService stockKeepingUnitService;


    /**
     * 通过id查询库存商品库
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(stockKeepingUnitService.getById(id));
    }


    /**
     * 分页查询 库存商品库 列表信息
     * @param stockKeepingUnitReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStockKeepingUnitPage")
    public R getStockKeepingUnitPage(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {
        Page page = new Page(stockKeepingUnitReq.getPageNo(), stockKeepingUnitReq.getPageSize());
        StockKeepingUnit stockKeepingUnit =new StockKeepingUnit();
        BeanUtil.copyProperties(stockKeepingUnitReq, stockKeepingUnit);

        return new R<>(stockKeepingUnitService.page(page, Wrappers.query(stockKeepingUnit)));
    }


    /**
     * 全量查询 库存商品库 列表信息
     * @param stockKeepingUnitReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStockKeepingUnitList")
    public R getStockKeepingUnitList(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {
        StockKeepingUnit stockKeepingUnit =new StockKeepingUnit();
        BeanUtil.copyProperties(stockKeepingUnitReq, stockKeepingUnit);

        return new R<>(stockKeepingUnitService.list(Wrappers.query(stockKeepingUnit)));
    }


    /**
     * 新增库存商品库
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @Inner
    @SysLog("新增库存商品库")
    @PostMapping("/add")
    public R save(@RequestBody StockKeepingUnit stockKeepingUnit) {
        return new R<>(stockKeepingUnitService.save(stockKeepingUnit));
    }

    /**
     * 修改库存商品库
     * @param stockKeepingUnit 库存商品库
     * @return R
     */
    @Inner
    @SysLog("修改库存商品库")
    @PostMapping("/update")
    public R updateById(@RequestBody StockKeepingUnit stockKeepingUnit) {
        return new R<>(stockKeepingUnitService.updateById(stockKeepingUnit));
    }

    /**
     * 通过id删除库存商品库
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除库存商品库")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(stockKeepingUnitService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除库存商品库")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(stockKeepingUnitService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询库存商品库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(stockKeepingUnitService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询库存商品库总记录}")
    @PostMapping("/countByStockKeepingUnitReq")
    public R countByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {

        return new R<>(stockKeepingUnitService.count(Wrappers.query(stockKeepingUnitReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param stockKeepingUnitReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据stockKeepingUnitReq查询一条货位信息表")
    @PostMapping("/getOneByStockKeepingUnitReq")
    public R getOneByStockKeepingUnitReq(@RequestBody StockKeepingUnitReq stockKeepingUnitReq) {

        return new R<>(stockKeepingUnitService.getOne(Wrappers.query(stockKeepingUnitReq), false));
    }


    /**
     * 批量修改OR插入
     * @param stockKeepingUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<StockKeepingUnit> stockKeepingUnitList) {

        return new R<>(stockKeepingUnitService.saveOrUpdateBatch(stockKeepingUnitList));
    }

}
