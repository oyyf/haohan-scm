/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.ServiceSelection;
import com.haohan.cloud.scm.goods.mapper.ServiceSelectionMapper;
import com.haohan.cloud.scm.goods.service.ServiceSelectionService;
import org.springframework.stereotype.Service;

/**
 * 服务选项
 *
 * @author haohan
 * @date 2019-05-13 18:47:54
 */
@Service
public class ServiceSelectionServiceImpl extends ServiceImpl<ServiceSelectionMapper, ServiceSelection> implements ServiceSelectionService {

}
