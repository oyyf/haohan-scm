/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.req.OffGoodsByCateReq;
import com.haohan.cloud.scm.goods.core.IScmGoodsService;
import com.haohan.cloud.scm.goods.service.GoodsCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 商品分类
 *
 * @author haohan
 * @date 2019-05-29 14:25:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodscategory")
@Api(value = "goodscategory", tags = "goodscategory管理")
public class GoodsCategoryController {

    private final GoodsCategoryService goodsCategoryService;

    private final IScmGoodsService iScmGoodsService;

    /**
     * 分页查询
     *
     * @param page          分页对象
     * @param goodsCategory 商品分类
     * @return
     */
    @GetMapping("/page")
    public R getGoodsCategoryPage(Page page, GoodsCategory goodsCategory) {
        return new R<>(goodsCategoryService.page(page, Wrappers.query(goodsCategory)));
    }

    /**
     * 通过id查询商品分类
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsCategoryService.getById(id));
    }


    @SysLog("修改分类状态")
    @PostMapping("/changeCategoryStatus")
    @ApiOperation(value = "修改分类状态")
    public R changeCategoryStatus(@RequestBody GoodsCategory cate) {
        return R.ok(iScmGoodsService.changeCateStatus(cate));
    }

    @SysLog("按分类下架商品")
    @PostMapping("/offGoodsByCate")
    @ApiOperation(value = "按分类下架商品")
    public R offGoodsByCate(@RequestBody OffGoodsByCateReq req) {
        return R.ok(iScmGoodsService.offGoodsByCate(req));
    }
}
