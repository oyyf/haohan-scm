/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.ServiceSelection;

/**
 * 服务选项
 *
 * @author haohan
 * @date 2019-05-13 18:47:54
 */
public interface ServiceSelectionService extends IService<ServiceSelection> {

}
