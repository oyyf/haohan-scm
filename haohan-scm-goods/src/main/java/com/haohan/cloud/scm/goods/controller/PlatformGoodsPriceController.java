/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;
import com.haohan.cloud.scm.api.goods.req.PlatformGoodsPriceReq;
import com.haohan.cloud.scm.goods.service.PlatformGoodsPriceService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 平台商品定价
 *
 * @author haohan
 * @date 2019-05-30 10:19:21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/platformgoodsprice")
@Api(value = "platformgoodsprice", tags = "platformgoodsprice管理")
public class PlatformGoodsPriceController {

    private final PlatformGoodsPriceService platformGoodsPriceService;

    /**
     * 分页查询
     *
     * @param page               分页对象
     * @param platformGoodsPrice 平台商品定价
     * @return
     */
    @GetMapping("/page")
    public R getPlatformGoodsPricePage(Page page, PlatformGoodsPrice platformGoodsPrice) {
        return new R<>(platformGoodsPriceService.page(page, Wrappers.query(platformGoodsPrice)));
    }


    /**
     * 通过id查询平台商品定价
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(platformGoodsPriceService.getById(id));
    }

    /**
     * 新增平台商品定价
     *
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @SysLog("新增平台商品定价")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_platformgoodsprice_add')")
    public R save(@RequestBody PlatformGoodsPrice platformGoodsPrice) {
        return new R<>(platformGoodsPriceService.save(platformGoodsPrice));
    }

    /**
     * 修改平台商品定价
     *
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @SysLog("修改平台商品定价")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_platformgoodsprice_edit')")
    public R updateById(@RequestBody PlatformGoodsPrice platformGoodsPrice) {
        return new R<>(platformGoodsPriceService.updateById(platformGoodsPrice));
    }

    /**
     * 通过id删除平台商品定价
     *
     * @param id id
     * @return R
     */
    @SysLog("删除平台商品定价")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('scm_platformgoodsprice_del')")
    public R removeById(@PathVariable String id) {
        return new R<>(platformGoodsPriceService.removeById(id));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(platformGoodsPriceService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countByPlatformGoodsPriceReq")
    public R countByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {

        return new R<>(platformGoodsPriceService.count(Wrappers.query(platformGoodsPriceReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/getOneByPlatformGoodsPriceReq")
    public R getOneByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq) {

        return new R<>(platformGoodsPriceService.getOne(Wrappers.query(platformGoodsPriceReq), false));
    }

}
