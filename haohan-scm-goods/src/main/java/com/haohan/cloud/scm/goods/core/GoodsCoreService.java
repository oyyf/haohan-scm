package com.haohan.cloud.scm.goods.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.manage.EditGoodsReq;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsListReq;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsPricingReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/16
 */
public interface GoodsCoreService {


    /**
     * 查询商品列表
     *
     * @param page
     * @param goods
     * @return
     */
    IPage<GoodsExtDTO> findExtPage(Page<Goods> page, Goods goods);

    /**
     * 商品列表 带详情
     *
     * @param page
     * @param req
     * @return
     */
    IPage<GoodsVO> findInfoPage(Page<Goods> page, GoodsListReq req);

    /**
     * 查询商品详情列表 带收藏状态
     *
     * @param page
     * @param req
     * @param uid
     * @return
     */
    IPage<GoodsVO> findInfoPage(Page<Goods> page, GoodsListReq req, String uid);


    /**
     * 商品收藏列表 (详情)
     *
     * @param page
     * @param req
     * @param uid
     * @return
     */
    IPage<GoodsVO> findCollectionsPage(Page page, GoodsListReq req, String uid);

    /**
     * 查询商品详情
     *
     * @param goodsSn
     * @return
     */
    GoodsVO fetchInfo(String goodsSn, GoodsPricingReq pricingReq);

    /**
     * 商品信息 带规格及类型列表
     *
     * @param goodsId
     * @return
     */
    GoodsVO fetchGoodsInfoById(String goodsId, GoodsPricingReq pricingReq);

    /**
     * 根据shopId 批量修改 商家id
     *
     * @param shopId
     * @param sourceMerchantId 原商家id
     * @param targetMerchantId 修改后商家id
     */
    void modifyMerchantByShopBatch(String shopId, String sourceMerchantId, String targetMerchantId);

    boolean addGoods(EditGoodsReq req);

    boolean modifyGoods(EditGoodsReq req);

    boolean deleteGoods(String goodsId);

    boolean changeMarketable(String goodsId, boolean marketable);

    boolean changeSort(String goodsId, Integer sort);

    boolean changeStorage(String goodsId, BigDecimal storage);

    boolean updateSalecStatus(String goodsId, YesNoEnum salecFlag);


    /**
     * 根据供应商品创建平台商品
     *
     * @param goodsId
     * @param shopId  创建商品的平台店铺， 为空时默认平台店铺
     * @return
     */
    List<SupplierGoods> createGoodsBySupplyGoods(String goodsId, String shopId);

    boolean specialAddGoods(EditGoodsReq req);

    boolean specialModifyGoods(EditGoodsReq req);
}
