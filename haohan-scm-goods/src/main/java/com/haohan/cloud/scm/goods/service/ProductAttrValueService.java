/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrValue;

/**
 * 商品库属性值
 *
 * @author haohan
 * @date 2019-05-13 19:06:21
 */
public interface ProductAttrValueService extends IService<ProductAttrValue> {

}
