/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.SaleRules;
import com.haohan.cloud.scm.api.goods.req.SaleRulesReq;
import com.haohan.cloud.scm.goods.service.SaleRulesService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 售卖规则
 *
 * @author haohan
 * @date 2019-05-29 14:27:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/salerules" )
@Api(value = "salerules", tags = "salerules管理")
public class SaleRulesController {

    private final SaleRulesService saleRulesService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param saleRules 售卖规则
     * @return
     */
    @GetMapping("/page" )
    public R getSaleRulesPage(Page page, SaleRules saleRules) {
        return new R<>(saleRulesService.page(page, Wrappers.query(saleRules)));
    }


    /**
     * 通过id查询售卖规则
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(saleRulesService.getById(id));
    }

    /**
     * 新增售卖规则
     * @param saleRules 售卖规则
     * @return R
     */
    @SysLog("新增售卖规则" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_salerules_add')" )
    public R save(@RequestBody SaleRules saleRules) {
        return new R<>(saleRulesService.save(saleRules));
    }

    /**
     * 修改售卖规则
     * @param saleRules 售卖规则
     * @return R
     */
    @SysLog("修改售卖规则" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_salerules_edit')" )
    public R updateById(@RequestBody SaleRules saleRules) {
        return new R<>(saleRulesService.updateById(saleRules));
    }

    /**
     * 通过id删除售卖规则
     * @param id id
     * @return R
     */
    @SysLog("删除售卖规则" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_salerules_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(saleRulesService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除售卖规则")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_salerules_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(saleRulesService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询售卖规则")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(saleRulesService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询售卖规则总记录}")
    @PostMapping("/countBySaleRulesReq")
    public R countBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq) {

        return new R<>(saleRulesService.count(Wrappers.query(saleRulesReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据saleRulesReq查询一条货位信息表")
    @PostMapping("/getOneBySaleRulesReq")
    public R getOneBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq) {

        return new R<>(saleRulesService.getOne(Wrappers.query(saleRulesReq), false));
    }


    /**
     * 批量修改OR插入
     * @param saleRulesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_salerules_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SaleRules> saleRulesList) {

        return new R<>(saleRulesService.saveOrUpdateBatch(saleRulesList));
    }


}
