/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;

/**
 * 平台商品定价
 *
 * @author haohan
 * @date 2019-05-13 20:38:26
 */
public interface PlatformGoodsPriceMapper extends BaseMapper<PlatformGoodsPrice> {

}
