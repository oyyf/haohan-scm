/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.ProductCategory;
import com.haohan.cloud.scm.api.goods.req.ProductCategoryReq;
import com.haohan.cloud.scm.goods.service.ProductCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品库分类
 *
 * @author haohan
 * @date 2019-05-29 14:26:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productcategory" )
@Api(value = "productcategory", tags = "productcategory管理")
public class ProductCategoryController {

    private final ProductCategoryService productCategoryService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productCategory 商品库分类
     * @return
     */
    @GetMapping("/page" )
    public R getProductCategoryPage(Page page, ProductCategory productCategory) {
        return new R<>(productCategoryService.page(page, Wrappers.query(productCategory)));
    }


    /**
     * 通过id查询商品库分类
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productCategoryService.getById(id));
    }

    /**
     * 新增商品库分类
     * @param productCategory 商品库分类
     * @return R
     */
    @SysLog("新增商品库分类" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productcategory_add')" )
    public R save(@RequestBody ProductCategory productCategory) {
        return new R<>(productCategoryService.save(productCategory));
    }

    /**
     * 修改商品库分类
     * @param productCategory 商品库分类
     * @return R
     */
    @SysLog("修改商品库分类" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productcategory_edit')" )
    public R updateById(@RequestBody ProductCategory productCategory) {
        return new R<>(productCategoryService.updateById(productCategory));
    }

    /**
     * 通过id删除商品库分类
     * @param id id
     * @return R
     */
    @SysLog("删除商品库分类" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productcategory_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productCategoryService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品库分类")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productcategory_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productCategoryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品库分类")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productCategoryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品库分类总记录}")
    @PostMapping("/countByProductCategoryReq")
    public R countByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq) {

        return new R<>(productCategoryService.count(Wrappers.query(productCategoryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productCategoryReq查询一条货位信息表")
    @PostMapping("/getOneByProductCategoryReq")
    public R getOneByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq) {

        return new R<>(productCategoryService.getOne(Wrappers.query(productCategoryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productcategory_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductCategory> productCategoryList) {

        return new R<>(productCategoryService.saveOrUpdateBatch(productCategoryList));
    }


}
