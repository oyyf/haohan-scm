/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;
import com.haohan.cloud.scm.goods.mapper.GoodsPromotionMapper;
import com.haohan.cloud.scm.goods.service.GoodsPromotionService;
import org.springframework.stereotype.Service;

/**
 * 商品促销库
 *
 * @author haohan
 * @date 2019-08-30 11:31:05
 */
@Service
public class GoodsPromotionServiceImpl extends ServiceImpl<GoodsPromotionMapper, GoodsPromotion> implements GoodsPromotionService {

}
