/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.StandardProductUnit;
import com.haohan.cloud.scm.goods.mapper.StandardProductUnitMapper;
import com.haohan.cloud.scm.goods.service.StandardProductUnitService;
import org.springframework.stereotype.Service;

/**
 * 标准商品库
 *
 * @author haohan
 * @date 2019-05-13 19:06:52
 */
@Service
public class StandardProductUnitServiceImpl extends ServiceImpl<StandardProductUnitMapper, StandardProductUnit> implements StandardProductUnitService {

}
