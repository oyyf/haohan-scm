package com.haohan.cloud.scm.goods.api.ctrl;

import com.haohan.cloud.scm.api.goods.req.imp.GoodsCategoryImportReq;
import com.haohan.cloud.scm.api.goods.req.imp.GoodsImportReq;
import com.haohan.cloud.scm.goods.core.GoodsImportInfoCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author dy
 * @date 2019/10/16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/goods/importInfo")
@Api(value = "ApiImportInfo", tags = "ImportInfo导入管理")
public class GoodsImportInfoApiCtrl {

    private final GoodsImportInfoCoreService goodsImportInfoCoreService;

    @PostMapping("/category")
    @ApiOperation(value = "零售商品分类导入")
    public R<String> category(@RequestBody @Valid GoodsCategoryImportReq req) {
        return R.ok(goodsImportInfoCoreService.categoryImport(req.getCategoryList(), req.getShopId()));
    }

    @PostMapping("/goods")
    @ApiOperation(value = "零售商品导入")
    public R<String> goods(@RequestBody @Valid GoodsImportReq req) {
        return R.ok(goodsImportInfoCoreService.goodsImport(req.getGoodsList(), req.getShopId()));
    }




}
