/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsCollections;
import com.haohan.cloud.scm.api.saleb.req.GoodsCollectionsReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsCollectionsCoreService;
import com.haohan.cloud.scm.goods.service.GoodsCollectionsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 收藏商品
 *
 * @author haohan
 * @date 2019-05-29 13:30:26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsCollections")
@Api(value = "goodscollections", tags = "goodscollections内部接口服务")
public class GoodsCollectionsFeignApiCtrl {

    private final GoodsCollectionsService goodsCollectionsService;
    private final GoodsCollectionsCoreService collectionsCoreService;


    /**
     * 通过id查询收藏商品
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsCollectionsService.getById(id));
    }


    /**
     * 分页查询 收藏商品 列表信息
     *
     * @param goodsCollectionsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsCollectionsPage")
    public R getGoodsCollectionsPage(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {
        Page page = new Page(goodsCollectionsReq.getPageNo(), goodsCollectionsReq.getPageSize());
        GoodsCollections goodsCollections = new GoodsCollections();
        BeanUtil.copyProperties(goodsCollectionsReq, goodsCollections);

        return new R<>(goodsCollectionsService.page(page, Wrappers.query(goodsCollections)));
    }


    /**
     * 全量查询 收藏商品 列表信息
     *
     * @param goodsCollectionsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsCollectionsList")
    public R getGoodsCollectionsList(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {
        GoodsCollections goodsCollections = new GoodsCollections();
        BeanUtil.copyProperties(goodsCollectionsReq, goodsCollections);

        return new R<>(goodsCollectionsService.list(Wrappers.query(goodsCollections)));
    }



    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询收藏商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsCollectionsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询收藏商品总记录}")
    @PostMapping("/countByGoodsCollectionsReq")
    public R countByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {

        return new R<>(goodsCollectionsService.count(Wrappers.query(goodsCollectionsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsCollectionsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsCollectionsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsCollectionsReq")
    public R getOneByGoodsCollectionsReq(@RequestBody GoodsCollectionsReq goodsCollectionsReq) {

        return new R<>(goodsCollectionsService.getOne(Wrappers.query(goodsCollectionsReq), false));
    }

    @Inner
    @PostMapping("/addCollections")
    public R<Boolean> addCollections(@RequestBody GoodsCollectionsReq req) {
        return RUtil.success(collectionsCoreService.addCollections(req.getUid(), req.getGoodsId()));
    }

    @Inner
    @PostMapping("/removeCollections")
    public R<Boolean> removeCollections(@RequestBody GoodsCollectionsReq req) {
        return RUtil.success(collectionsCoreService.removeCollections(req.getUid(), req.getGoodsId()));
    }

}
