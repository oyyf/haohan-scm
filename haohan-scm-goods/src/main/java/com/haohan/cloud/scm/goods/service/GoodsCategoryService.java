/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;

import java.util.List;
import java.util.Set;

/**
 * 商品分类
 *
 * @author haohan
 * @date 2019-05-13 18:46:41
 */
public interface GoodsCategoryService extends IService<GoodsCategory> {

    /**
     * 根据分类名称 查找分类, 只返回一个
     *
     * @param shopId       可为null
     * @param categoryName
     * @return
     */
    GoodsCategory fetchByName(String shopId, String categoryName);

    /**
     * 查询子分类id
     *
     * @param goodsCategoryId
     * @return 当前分类id加入结果集
     */
    Set<String> fetchChildren(String goodsCategoryId);

    List<GoodsCategory> fetchParentList(String goodsCategoryId);

    List<GoodsCategory> fetchAllParentWithSelf(String categoryId);

    /**
     * 查询商品分类树
     *
     * @param shopId
     * @param allShow
     * @return
     */
    List<GoodsCategoryTree> selectTree(String shopId, boolean allShow);

    boolean saveCategory(GoodsCategory category);

    boolean updateCategoryById(GoodsCategory category);

    /**
     * 删除分类及子分类
     *
     * @param categoryId
     * @return
     */
    boolean removeCategoryById(String categoryId);

    /**
     * 当前租户下所有分类层级关系更新
     *
     * @return
     */
    Boolean updateAllRelation();
}
