/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;

/**
 * 商品规格名称
 *
 * @author haohan
 * @date 2019-05-13 18:46:29
 */
public interface GoodsModelTotalMapper extends BaseMapper<GoodsModelTotal> {

}
