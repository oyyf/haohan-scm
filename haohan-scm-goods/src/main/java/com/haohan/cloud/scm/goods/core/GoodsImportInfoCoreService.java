package com.haohan.cloud.scm.goods.core;

import com.haohan.cloud.scm.api.goods.dto.imp.GoodsCategoryImport;
import com.haohan.cloud.scm.api.goods.dto.imp.GoodsImport;

import java.util.List;

/**
 * @author dy
 * @date 2019/10/16
 */
public interface GoodsImportInfoCoreService {


    String categoryImport(List<GoodsCategoryImport> categoryList, String shopId);

    String goodsImport(List<GoodsImport> goodsList, String shopId);
}
