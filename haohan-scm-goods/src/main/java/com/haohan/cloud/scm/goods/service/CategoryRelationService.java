/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.common.tree.TreeNode;
import com.haohan.cloud.scm.api.goods.entity.CategoryRelation;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 分类关系表
 *
 * @author haohan
 * @date 2020-04-08 17:47:17
 */
public interface CategoryRelationService extends IService<CategoryRelation> {

    /**
     * 新建层级关系
     *
     * @param id
     * @param parentId
     * @param clazz
     */
    void insertCategoryRelation(String id, String parentId, Class clazz);

    /**
     * 通过ID删除层级关系
     *
     * @param ids
     * @param clazz
     */
    void deleteAllCategoryRelation(Collection<String> ids, Class clazz);

    /**
     * 更新层级关系
     *
     * @param relation
     */
    void updateCategoryRelation(CategoryRelation relation);

    /**
     * 更新所有层级关系(删除已有数据)
     *
     * @param treeNodeList
     * @param clazz
     * @param <E>
     */
    <E extends TreeNode> void updateAllRelation(List<E> treeNodeList, Class clazz);

    /**
     * 查询所有后代节点
     *
     * @param ancestor
     * @param className
     * @return
     */
    Set<String> fetchAllDescendant(String ancestor, String className);

    /**
     * 查询所有祖先节点
     *
     * @param descendant
     * @param className
     * @return
     */
    Set<String> fetchAllAncestor(String descendant, String className);

}
