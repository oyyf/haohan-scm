/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.StockKeepingUnit;
import com.haohan.cloud.scm.goods.mapper.StockKeepingUnitMapper;
import com.haohan.cloud.scm.goods.service.StockKeepingUnitService;
import org.springframework.stereotype.Service;

/**
 * 库存商品库
 *
 * @author haohan
 * @date 2019-05-13 19:07:17
 */
@Service
public class StockKeepingUnitServiceImpl extends ServiceImpl<StockKeepingUnitMapper, StockKeepingUnit> implements StockKeepingUnitService {

}
