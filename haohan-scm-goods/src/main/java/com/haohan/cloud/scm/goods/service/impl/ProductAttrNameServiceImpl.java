/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrName;
import com.haohan.cloud.scm.goods.mapper.ProductAttrNameMapper;
import com.haohan.cloud.scm.goods.service.ProductAttrNameService;
import org.springframework.stereotype.Service;

/**
 * 商品库属性名
 *
 * @author haohan
 * @date 2019-05-13 19:06:24
 */
@Service
public class ProductAttrNameServiceImpl extends ServiceImpl<ProductAttrNameMapper, ProductAttrName> implements ProductAttrNameService {

}
