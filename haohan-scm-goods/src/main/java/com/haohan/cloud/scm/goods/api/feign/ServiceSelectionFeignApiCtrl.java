/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.ServiceSelection;
import com.haohan.cloud.scm.api.goods.req.ServiceSelectionReq;
import com.haohan.cloud.scm.goods.service.ServiceSelectionService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 服务选项
 *
 * @author haohan
 * @date 2019-05-29 14:27:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ServiceSelection")
@Api(value = "serviceselection", tags = "serviceselection内部接口服务")
public class ServiceSelectionFeignApiCtrl {

    private final ServiceSelectionService serviceSelectionService;


    /**
     * 通过id查询服务选项
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(serviceSelectionService.getById(id));
    }


    /**
     * 分页查询 服务选项 列表信息
     * @param serviceSelectionReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchServiceSelectionPage")
    public R getServiceSelectionPage(@RequestBody ServiceSelectionReq serviceSelectionReq) {
        Page page = new Page(serviceSelectionReq.getPageNo(), serviceSelectionReq.getPageSize());
        ServiceSelection serviceSelection =new ServiceSelection();
        BeanUtil.copyProperties(serviceSelectionReq, serviceSelection);

        return new R<>(serviceSelectionService.page(page, Wrappers.query(serviceSelection)));
    }


    /**
     * 全量查询 服务选项 列表信息
     * @param serviceSelectionReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchServiceSelectionList")
    public R getServiceSelectionList(@RequestBody ServiceSelectionReq serviceSelectionReq) {
        ServiceSelection serviceSelection =new ServiceSelection();
        BeanUtil.copyProperties(serviceSelectionReq, serviceSelection);

        return new R<>(serviceSelectionService.list(Wrappers.query(serviceSelection)));
    }


    /**
     * 新增服务选项
     * @param serviceSelection 服务选项
     * @return R
     */
    @Inner
    @SysLog("新增服务选项")
    @PostMapping("/add")
    public R save(@RequestBody ServiceSelection serviceSelection) {
        return new R<>(serviceSelectionService.save(serviceSelection));
    }

    /**
     * 修改服务选项
     * @param serviceSelection 服务选项
     * @return R
     */
    @Inner
    @SysLog("修改服务选项")
    @PostMapping("/update")
    public R updateById(@RequestBody ServiceSelection serviceSelection) {
        return new R<>(serviceSelectionService.updateById(serviceSelection));
    }

    /**
     * 通过id删除服务选项
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除服务选项")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(serviceSelectionService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除服务选项")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(serviceSelectionService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询服务选项")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(serviceSelectionService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param serviceSelectionReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询服务选项总记录}")
    @PostMapping("/countByServiceSelectionReq")
    public R countByServiceSelectionReq(@RequestBody ServiceSelectionReq serviceSelectionReq) {

        return new R<>(serviceSelectionService.count(Wrappers.query(serviceSelectionReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param serviceSelectionReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据serviceSelectionReq查询一条货位信息表")
    @PostMapping("/getOneByServiceSelectionReq")
    public R getOneByServiceSelectionReq(@RequestBody ServiceSelectionReq serviceSelectionReq) {

        return new R<>(serviceSelectionService.getOne(Wrappers.query(serviceSelectionReq), false));
    }


    /**
     * 批量修改OR插入
     * @param serviceSelectionList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ServiceSelection> serviceSelectionList) {

        return new R<>(serviceSelectionService.saveOrUpdateBatch(serviceSelectionList));
    }

}
