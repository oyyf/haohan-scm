/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;

/**
 * 商品促销库
 *
 * @author haohan
 * @date 2019-08-30 11:31:05
 */
public interface GoodsPromotionMapper extends BaseMapper<GoodsPromotion> {

}
