/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.api.goods.req.GoodsReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.goods.core.GoodsCoreService;
import com.haohan.cloud.scm.goods.core.GoodsModelCoreService;
import com.haohan.cloud.scm.goods.service.GoodsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-29 14:25:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Goods")
@Api(value = "goods", tags = "goods内部接口服务")
public class GoodsFeignApiCtrl {

    private final GoodsService goodsService;
    private final GoodsModelCoreService goodsModelCoreService;
    private final GoodsCoreService goodsCoreService;

    /**
     * 通过id查询商品
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsService.getById(id));
    }


    /**
     * 全量查询 商品 列表信息
     *
     * @param goodsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsList")
    public R getGoodsList(@RequestBody GoodsReq goodsReq) {
        Goods goods = new Goods();
        BeanUtil.copyProperties(goodsReq, goods);

        return new R<>(goodsService.list(Wrappers.query(goods)));
    }


    /**
     * 修改商品
     *
     * @param goods 商品
     * @return R
     */
    @Inner
    @SysLog("修改商品")
    @PostMapping("/updateSalecStatus")
    public R<Boolean> updateSalecStatus(@RequestBody Goods goods) {
        return RUtil.success(goodsCoreService.updateSalecStatus(goods.getId(), goods.getSalecFlag()));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品总记录}")
    @PostMapping("/countByGoodsReq")
    public R<Integer> countByGoodsReq(@RequestBody GoodsReq goodsReq) {
        return RUtil.success(goodsService.count(Wrappers.query(goodsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsReq")
    public R getOneByGoodsReq(@RequestBody GoodsReq goodsReq) {

        return new R<>(goodsService.getOne(Wrappers.query(goodsReq), false));
    }


//    /**
//     * 查询单个或多个商品信息
//     *
//     * @return
//     */
//    @Inner
//    @GetMapping("/queryAllGoodsInfo")
//    public R queryAllGoodsInfo() {
//        return new R<>(iScmGoodsService.getGoodsInfoDTO());
//    }

    /**
     * 查询商品详情列表 带收藏状态
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryGoodsInfoList")
    public R<IPage<GoodsVO>> queryGoodsInfoList(@RequestBody GoodsFeignReq req) {
        boolean idsFlag = CollUtil.isNotEmpty(req.getGoodsIds());
        boolean shopIdFlag = StrUtil.isNotEmpty(req.getShopId());
        if (!idsFlag && !shopIdFlag) {
            throw new ErrorDataException("查询商品列表时缺参数ids,shopId");
        }
        return RUtil.success(goodsCoreService.findInfoPage(new Page<>(req.getCurrent(), req.getSize()), req, req.getUid()));
    }

    /**
     * 根据规格id查询商品信息
     *
     * @param req 必须goodsModelId, 可选buyerId, pricingDate
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsInfoByModelId")
    public R<GoodsVO> fetchGoodsInfoByModelId(@RequestBody GoodsFeignReq req) {
        return RUtil.success(goodsModelCoreService.fetchGoodsInfoByModelId(req.getGoodsModelId(), req.transToPricingReq()));
    }

    /**
     * 分页查询商品列表
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/findExtPage")
    public R<Page<GoodsExtDTO>> findExtPage(@RequestBody GoodsFeignReq req) {
        IPage<GoodsExtDTO> goodsPage = goodsCoreService.findExtPage(new Page<>(req.getCurrent(), req.getSize()), req.transTo());
        Page<GoodsExtDTO> result = new Page<>(goodsPage.getCurrent(), goodsPage.getSize(), goodsPage.getTotal());
        result.setRecords(goodsPage.getRecords());
        return RUtil.success(result);
    }

    /**
     * 商品信息 带规格及类型列表
     *
     * @param req 必须goodsId, 可选 buyerId、pricingDate
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsInfoById")
    public R<GoodsVO> fetchGoodsInfoById(@RequestBody GoodsFeignReq req) {
        return RUtil.success(goodsCoreService.fetchGoodsInfoById(req.getGoodsId(), req.transToPricingReq()));
    }

    /**
     * 根据供应商品创建平台商品
     *
     * @param req goodsId、shopId:创建商品的平台店铺， 为空时默认平台店铺
     * @return
     */
    @Inner
    @PostMapping("/createGoodsBySupplyGoods")
    public R<List<SupplierGoods>> createGoodsBySupplyGoods(@RequestBody GoodsFeignReq req) {
        return RUtil.success(goodsCoreService.createGoodsBySupplyGoods(req.getGoodsId(), req.getShopId()));
    }

    /**
     * 商品收藏列表(详情)
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/findCollectionsPage")
    public R<IPage<GoodsVO>> findCollectionsPage(@RequestBody GoodsFeignReq req) {
        if (StrUtil.isEmpty(req.getShopId())) {
            throw new ErrorDataException("查询商品列表时缺参数shopId");
        }
        return RUtil.success(goodsCoreService.findCollectionsPage(new Page<>(req.getCurrent(), req.getSize()), req, req.getUid()));
    }

}
