/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsGifts;
import com.haohan.cloud.scm.api.goods.req.GoodsGiftsReq;
import com.haohan.cloud.scm.goods.service.GoodsGiftsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 赠品
 *
 * @author haohan
 * @date 2019-05-29 14:25:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodsgifts" )
@Api(value = "goodsgifts", tags = "goodsgifts管理")
public class GoodsGiftsController {

    private final GoodsGiftsService goodsGiftsService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodsGifts 赠品
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsGiftsPage(Page page, GoodsGifts goodsGifts) {
        return new R<>(goodsGiftsService.page(page, Wrappers.query(goodsGifts)));
    }


    /**
     * 通过id查询赠品
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsGiftsService.getById(id));
    }

    /**
     * 新增赠品
     * @param goodsGifts 赠品
     * @return R
     */
    @SysLog("新增赠品" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodsgifts_add')" )
    public R save(@RequestBody GoodsGifts goodsGifts) {
        return new R<>(goodsGiftsService.save(goodsGifts));
    }

    /**
     * 修改赠品
     * @param goodsGifts 赠品
     * @return R
     */
    @SysLog("修改赠品" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goodsgifts_edit')" )
    public R updateById(@RequestBody GoodsGifts goodsGifts) {
        return new R<>(goodsGiftsService.updateById(goodsGifts));
    }

    /**
     * 通过id删除赠品
     * @param id id
     * @return R
     */
    @SysLog("删除赠品" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goodsgifts_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(goodsGiftsService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除赠品")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodsgifts_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsGiftsService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询赠品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsGiftsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询赠品总记录}")
    @PostMapping("/countByGoodsGiftsReq")
    public R countByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq) {

        return new R<>(goodsGiftsService.count(Wrappers.query(goodsGiftsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsGiftsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsGiftsReq")
    public R getOneByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq) {

        return new R<>(goodsGiftsService.getOne(Wrappers.query(goodsGiftsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsGiftsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodsgifts_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<GoodsGifts> goodsGiftsList) {

        return new R<>(goodsGiftsService.saveOrUpdateBatch(goodsGiftsList));
    }


}
