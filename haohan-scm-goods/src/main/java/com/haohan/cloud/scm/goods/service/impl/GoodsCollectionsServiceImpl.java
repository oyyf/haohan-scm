/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.common.dto.SelfPageMapper;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.entity.GoodsCollections;
import com.haohan.cloud.scm.goods.mapper.GoodsCollectionsMapper;
import com.haohan.cloud.scm.goods.service.GoodsCollectionsService;
import org.springframework.stereotype.Service;

/**
 * 收藏商品
 *
 * @author haohan
 * @date 2019-05-13 20:37:43
 */
@Service
public class GoodsCollectionsServiceImpl extends ServiceImpl<GoodsCollectionsMapper, GoodsCollections> implements GoodsCollectionsService {

    @Override
    public YesNoEnum fetchStatus(String uid, String goodsId) {
        return fetchCollections(uid, goodsId) == null ? YesNoEnum.no : YesNoEnum.yes;
    }

    @Override
    public GoodsCollections fetchCollections(String uid, String goodsId) {
        return baseMapper.selectList(Wrappers.<GoodsCollections>query().lambda()
                .eq(GoodsCollections::getUid, uid)
                .eq(GoodsCollections::getGoodsId, goodsId)
        ).stream()
                .findFirst().orElse(null);
    }

    @Override
    public IPage<Goods> findPage(GoodsSqlDTO query) {
        return SelfPageMapper.findPage(query, baseMapper);
    }
}
