/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import com.haohan.cloud.scm.api.goods.req.GoodsPriceRuleReq;
import com.haohan.cloud.scm.goods.service.GoodsPriceRuleService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-29 14:25:54
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodspricerule" )
@Api(value = "goodspricerule", tags = "goodspricerule管理")
public class GoodsPriceRuleController {

    private final GoodsPriceRuleService goodsPriceRuleService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return
     */
    @GetMapping("/page" )
    public R getGoodsPriceRulePage(Page page, GoodsPriceRule goodsPriceRule) {
        return new R<>(goodsPriceRuleService.page(page, Wrappers.query(goodsPriceRule)));
    }


    /**
     * 通过id查询定价规则/市场价/销售价/VIP价格
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(goodsPriceRuleService.getById(id));
    }

    /**
     * 新增定价规则/市场价/销售价/VIP价格
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @SysLog("新增定价规则/市场价/销售价/VIP价格" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_goodspricerule_add')" )
    public R save(@RequestBody GoodsPriceRule goodsPriceRule) {
        return R.failed("不支持");
    }

    /**
     * 修改定价规则/市场价/销售价/VIP价格
     * @param goodsPriceRule 定价规则/市场价/销售价/VIP价格
     * @return R
     */
    @SysLog("修改定价规则/市场价/销售价/VIP价格" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_goodspricerule_edit')" )
    public R updateById(@RequestBody GoodsPriceRule goodsPriceRule) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除定价规则/市场价/销售价/VIP价格
     * @param id id
     * @return R
     */
    @SysLog("删除定价规则/市场价/销售价/VIP价格" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_goodspricerule_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除定价规则/市场价/销售价/VIP价格")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_goodspricerule_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询定价规则/市场价/销售价/VIP价格")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsPriceRuleService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询定价规则/市场价/销售价/VIP价格总记录}")
    @PostMapping("/countByGoodsPriceRuleReq")
    public R countByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {

        return new R<>(goodsPriceRuleService.count(Wrappers.query(goodsPriceRuleReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsPriceRuleReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据goodsPriceRuleReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsPriceRuleReq")
    public R getOneByGoodsPriceRuleReq(@RequestBody GoodsPriceRuleReq goodsPriceRuleReq) {

        return new R<>(goodsPriceRuleService.getOne(Wrappers.query(goodsPriceRuleReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsPriceRuleList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_goodspricerule_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<GoodsPriceRule> goodsPriceRuleList) {

        return R.failed("不支持");
    }


}
