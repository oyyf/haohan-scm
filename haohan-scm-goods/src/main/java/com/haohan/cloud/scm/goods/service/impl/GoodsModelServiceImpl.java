/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsSqlDTO;
import com.haohan.cloud.scm.api.goods.dto.GoodsStorageRankDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.goods.mapper.GoodsModelMapper;
import com.haohan.cloud.scm.goods.service.GoodsModelService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-13 18:46:34
 */
@Service
public class GoodsModelServiceImpl extends ServiceImpl<GoodsModelMapper, GoodsModel> implements GoodsModelService {

    @Override
    @CacheEvict(value = ScmCacheNameConstant.GOODS_MODEL_DETAIL, allEntries = true)
    public boolean save(GoodsModel entity) {
        return retBool(baseMapper.insert(entity));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true),
            @CacheEvict(value = ScmCacheNameConstant.GOODS_INFO_DETAIL, allEntries = true),
            @CacheEvict(value = ScmCacheNameConstant.GOODS_MODEL_DETAIL, allEntries = true)
    })
    public boolean updateById(GoodsModel entity) {
        return retBool(baseMapper.updateById(entity));
    }

    /**
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param id
     * @return
     */
    @Override
    public GoodsModelDTO getInfoById(String id) {
        GoodsSqlDTO query = new GoodsSqlDTO();
        query.setModelId(id);
        return getInfo(query);
    }

    @Override
    public GoodsModelDTO getInfoBySn(String modelSn) {
        GoodsSqlDTO query = new GoodsSqlDTO();
        query.setModelSn(modelSn);
        return getInfo(query);
    }

    @Override
    public GoodsModelDTO getInfo(GoodsSqlDTO query) {
        return baseMapper.getInfo(query);
    }

    /**
     * 分页 查询商品规格列表
     *
     * @param page
     * @param model
     * @return
     */
    @Override
    public IPage<GoodsModelDTO> fetchPage(Page<GoodsModelDTO> page, GoodsModelDTO model) {
        // 默认查询上架商品
        String marketable = (null == model.getIsMarketable() || model.getIsMarketable() != 0) ? YesNoEnum.yes.getType() : YesNoEnum.no.getType();
        return baseMapper.fetchList(page,
                model.getModelName(),
                model.getModelUnit(),
                model.getModelCode(),
                model.getShopId(),
                model.getGoodsSn(),
                model.getGoodsId(),
                marketable,
                model.getGoodsName(),
                model.getGoodsCategoryId(),
                model.getCategoryName());
    }

    /**
     * 查询商品规格列表
     *
     * @param query
     * @return
     */
    @Override
    public List<GoodsModelDTO> findGoodsModelList(GoodsSqlDTO query) {
        return baseMapper.findGoodsModelList(query);
    }

    /**
     * 查询商品库存排行
     *
     * @return
     */
    @Override
    public List<GoodsStorageRankDTO> getStorageRank() {
        return baseMapper.getStorageRank();
    }

    /**
     * 查询商品库存预警数量
     *
     * @return
     */
    @Override
    public Integer queryStorageWarn() {
        return baseMapper.queryStorageWarn();
    }

    /**
     * 查询 规格列表
     *
     * @param goodsId
     * @return
     */
    @Override
    public List<GoodsModel> fetchModelList(String goodsId) {
        QueryWrapper<GoodsModel> query = new QueryWrapper<>();
        query.lambda()
                .eq(GoodsModel::getGoodsId, goodsId);
        return baseMapper.selectList(query);
    }

    /**
     * 查询分类及子分类 的商品规格id列表
     *
     * @param categoryId
     * @return
     */
    @Override
    public List<String> fetchModelListByCategory(String categoryId) {
        return baseMapper.fetchModelListByCategory(categoryId);
    }
}
