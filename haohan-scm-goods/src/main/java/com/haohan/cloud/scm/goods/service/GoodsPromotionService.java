/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;

/**
 * 商品促销库
 *
 * @author haohan
 * @date 2019-08-30 11:31:05
 */
public interface GoodsPromotionService extends IService<GoodsPromotion> {

}
