/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsGifts;
import com.haohan.cloud.scm.api.goods.req.GoodsGiftsReq;
import com.haohan.cloud.scm.goods.service.GoodsGiftsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 赠品
 *
 * @author haohan
 * @date 2019-05-29 14:25:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsGifts")
@Api(value = "goodsgifts", tags = "goodsgifts内部接口服务")
public class GoodsGiftsFeignApiCtrl {

    private final GoodsGiftsService goodsGiftsService;


    /**
     * 通过id查询赠品
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsGiftsService.getById(id));
    }


    /**
     * 分页查询 赠品 列表信息
     * @param goodsGiftsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsGiftsPage")
    public R getGoodsGiftsPage(@RequestBody GoodsGiftsReq goodsGiftsReq) {
        Page page = new Page(goodsGiftsReq.getPageNo(), goodsGiftsReq.getPageSize());
        GoodsGifts goodsGifts =new GoodsGifts();
        BeanUtil.copyProperties(goodsGiftsReq, goodsGifts);

        return new R<>(goodsGiftsService.page(page, Wrappers.query(goodsGifts)));
    }


    /**
     * 全量查询 赠品 列表信息
     * @param goodsGiftsReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsGiftsList")
    public R getGoodsGiftsList(@RequestBody GoodsGiftsReq goodsGiftsReq) {
        GoodsGifts goodsGifts =new GoodsGifts();
        BeanUtil.copyProperties(goodsGiftsReq, goodsGifts);

        return new R<>(goodsGiftsService.list(Wrappers.query(goodsGifts)));
    }


    /**
     * 新增赠品
     * @param goodsGifts 赠品
     * @return R
     */
    @Inner
    @SysLog("新增赠品")
    @PostMapping("/add")
    public R save(@RequestBody GoodsGifts goodsGifts) {
        return new R<>(goodsGiftsService.save(goodsGifts));
    }

    /**
     * 修改赠品
     * @param goodsGifts 赠品
     * @return R
     */
    @Inner
    @SysLog("修改赠品")
    @PostMapping("/update")
    public R updateById(@RequestBody GoodsGifts goodsGifts) {
        return new R<>(goodsGiftsService.updateById(goodsGifts));
    }

    /**
     * 通过id删除赠品
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除赠品")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(goodsGiftsService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除赠品")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsGiftsService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询赠品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsGiftsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询赠品总记录}")
    @PostMapping("/countByGoodsGiftsReq")
    public R countByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq) {

        return new R<>(goodsGiftsService.count(Wrappers.query(goodsGiftsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsGiftsReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsGiftsReq")
    public R getOneByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq) {

        return new R<>(goodsGiftsService.getOne(Wrappers.query(goodsGiftsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsGiftsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<GoodsGifts> goodsGiftsList) {

        return new R<>(goodsGiftsService.saveOrUpdateBatch(goodsGiftsList));
    }

}
