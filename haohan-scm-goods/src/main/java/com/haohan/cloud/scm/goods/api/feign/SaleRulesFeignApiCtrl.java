/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.SaleRules;
import com.haohan.cloud.scm.api.goods.req.SaleRulesReq;
import com.haohan.cloud.scm.goods.service.SaleRulesService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 售卖规则
 *
 * @author haohan
 * @date 2019-05-29 14:27:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SaleRules")
@Api(value = "salerules", tags = "salerules内部接口服务")
public class SaleRulesFeignApiCtrl {

    private final SaleRulesService saleRulesService;


    /**
     * 通过id查询售卖规则
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(saleRulesService.getById(id));
    }


    /**
     * 分页查询 售卖规则 列表信息
     * @param saleRulesReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSaleRulesPage")
    public R getSaleRulesPage(@RequestBody SaleRulesReq saleRulesReq) {
        Page page = new Page(saleRulesReq.getPageNo(), saleRulesReq.getPageSize());
        SaleRules saleRules =new SaleRules();
        BeanUtil.copyProperties(saleRulesReq, saleRules);

        return new R<>(saleRulesService.page(page, Wrappers.query(saleRules)));
    }


    /**
     * 全量查询 售卖规则 列表信息
     * @param saleRulesReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSaleRulesList")
    public R getSaleRulesList(@RequestBody SaleRulesReq saleRulesReq) {
        SaleRules saleRules =new SaleRules();
        BeanUtil.copyProperties(saleRulesReq, saleRules);

        return new R<>(saleRulesService.list(Wrappers.query(saleRules)));
    }


    /**
     * 新增售卖规则
     * @param saleRules 售卖规则
     * @return R
     */
    @Inner
    @SysLog("新增售卖规则")
    @PostMapping("/add")
    public R save(@RequestBody SaleRules saleRules) {
        return new R<>(saleRulesService.save(saleRules));
    }

    /**
     * 修改售卖规则
     * @param saleRules 售卖规则
     * @return R
     */
    @Inner
    @SysLog("修改售卖规则")
    @PostMapping("/update")
    public R updateById(@RequestBody SaleRules saleRules) {
        return new R<>(saleRulesService.updateById(saleRules));
    }

    /**
     * 通过id删除售卖规则
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除售卖规则")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(saleRulesService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除售卖规则")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(saleRulesService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询售卖规则")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(saleRulesService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询售卖规则总记录}")
    @PostMapping("/countBySaleRulesReq")
    public R countBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq) {

        return new R<>(saleRulesService.count(Wrappers.query(saleRulesReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据saleRulesReq查询一条货位信息表")
    @PostMapping("/getOneBySaleRulesReq")
    public R getOneBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq) {

        return new R<>(saleRulesService.getOne(Wrappers.query(saleRulesReq), false));
    }


    /**
     * 批量修改OR插入
     * @param saleRulesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SaleRules> saleRulesList) {

        return new R<>(saleRulesService.saveOrUpdateBatch(saleRulesList));
    }

}
