/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.goods.entity.CategoryRelation;

/**
 * 分类关系表
 *
 * @author haohan
 * @date 2020-04-08 17:47:17
 */
public interface CategoryRelationMapper extends BaseMapper<CategoryRelation> {

}
