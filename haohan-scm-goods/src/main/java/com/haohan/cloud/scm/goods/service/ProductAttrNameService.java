/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.ProductAttrName;

/**
 * 商品库属性名
 *
 * @author haohan
 * @date 2019-05-13 19:06:24
 */
public interface ProductAttrNameService extends IService<ProductAttrName> {

}
