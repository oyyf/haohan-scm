/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.ProductCategory;

/**
 * 商品库分类
 *
 * @author haohan
 * @date 2019-05-13 19:06:14
 */
public interface ProductCategoryService extends IService<ProductCategory> {

}
