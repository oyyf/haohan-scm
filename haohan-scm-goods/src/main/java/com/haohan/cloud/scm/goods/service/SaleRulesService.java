/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.goods.entity.SaleRules;

/**
 * 售卖规则
 *
 * @author haohan
 * @date 2019-05-13 18:47:37
 */
public interface SaleRulesService extends IService<SaleRules> {

}
