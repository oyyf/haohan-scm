package com.haohan.cloud.framework.exception;

import com.haohan.cloud.framework.entity.BaseStatus;

public class WebException extends ActionException {

	/**
	 *
	 */
	private static final long serialVersionUID = 4982014301389550370L;

	public WebException() {

	}

	public WebException(BaseStatus status) {
		super(status.getCode(), status.getMsg());
	}

	public WebException(int code, String msg) {
		super(code, msg);
	}

	public WebException(String message) {
		super(message);
	}

	public WebException(Throwable cause) {
		super(cause);
	}

	public WebException(String message, Throwable cause) {
		super(message, cause);
	}

}
