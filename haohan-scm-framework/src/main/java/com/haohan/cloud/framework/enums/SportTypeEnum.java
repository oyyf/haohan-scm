/**
 * project name:uccore-model
 * package name:com.haohan.uccore.model.enums
 * file name:SportType.java
 * type name:SportType
 */
package com.haohan.cloud.framework.enums;

import com.haohan.cloud.framework.utils.EnumUtils;

/**
 * @Description:
 * TEN(101, "网球"),BAD(102,"羽毛球"),GOL(103, "高尔夫"),BAS(104, "篮球"),SKI(105, "滑雪"),
 * TAB(106, "乒乓球"),GOP(107, "高尔夫练习场"),BOD(108, "健身"),SWI(109, "游泳"),HOR(110, "马术"),
 * YAC(111, "游艇"),SQU(112, "壁球"),BIL(113, "台球"),SKA(114, "滑冰"),BOW(115, "保龄球"),
 * RAC(116, "赛车"),YOG(117, "瑜伽"),FEN(118, "击剑"),DIV(119, "潜水")
 * <br>
 * @author daizq@saidian.com
 * @Date 2015年7月17日 下午5:11:14
 *
 */
public enum SportTypeEnum implements SuperEnum {
    /** 网球 */
    TEN(101, "网球"),
    /** 羽毛球 */
    BAD(102,"羽毛球"),
    /** 高尔夫 */
    GOL(103, "高尔夫"),
    /** 篮球 */
    BAS(104, "篮球"),
    /** 滑雪 */
    SKI(105, "滑雪"),
    /** 乒乓球 */
    TAB(106, "乒乓球"),
    /** 高尔夫练习场 */
    GOP(107, "高尔夫练习场"),
    /** 健身 */
    BOD(108, "健身"),
    /** 游泳 */
    SWI(109, "游泳"),
    /** 马术 */
    HOR(110, "马术"),
    /** 游艇 */
    YAC(111, "游艇"),
    /** 壁球 */
    SQU(112, "壁球"),
    /** 台球 */
    BIL(113, "台球"),
    /** 滑冰 */
    SKA(114, "滑冰"),
    /** 保龄球 */
    BOW(115, "保龄球"),
    /** 赛车 */
    RAC(116, "赛车"),
    /** 瑜伽 */
    YOG(117, "瑜伽"),
    /** 击剑 */
    FEN(118, "击剑"),
    /** 潜水 */
    DIV(119, "潜水");

    /** 编码 */
    private int code;

    /** 描述的KEY */
    private String description;

    /**
     * @param code
     * @param description
     */
    private SportTypeEnum(int code, String description) {
        this.code = code;
        this.description = description;
        EnumUtils.put(this.getClass().getName() + code, this);
    }


    /**
     * <pre>
     * 一个便利的方法，方便使用者通过code获得枚举对象，
     * 对于非法状态，返回null;
     * </pre>
     *
     * @param code
     * @return
     */
    public static SportTypeEnum valueOf(int code) {
        Object obj = EnumUtils.get(SportTypeEnum.class.getName() + code);
        if (null != obj) {
            return (SportTypeEnum) obj;
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
