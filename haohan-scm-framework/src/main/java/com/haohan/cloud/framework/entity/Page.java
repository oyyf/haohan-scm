package com.haohan.cloud.framework.entity;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * 分页数据对象
 * @author zhaokuner
 *
 */
public class Page implements Serializable {

	private static final long serialVersionUID = 4166146415467286558L;

	/**
	 * 当前页码
	 */
	public static String PAGE_INDEX = "pageIndex";
	/**
	 * 每页条数
	 */
	public static String PAGE_SIZE = "pageSize";
	/**
	 * 分页中用到的标志
	 */
	public static String PAGE_MARK = "pageMark";

	private Pattern isInteger = Pattern.compile("^[0-9]+$");

	private final int defaultPageSize = 10;

	private int pageIndex = 0;

	private int firstResult = 0;
	private int pageSize = 10;
	private Serializable pageMark;

	public Page(String pageIndex, String pageSize, Serializable pageMark) {

		pageIndex = null == pageIndex ? "" : pageIndex.trim();
		pageSize = null == pageSize ? "" : pageSize.trim();

		this.pageIndex = isInteger.matcher(pageIndex).matches() ? Integer.parseInt(pageIndex) : 1;
		this.pageIndex = this.pageIndex > 0 ? this.pageIndex : 1;
		this.pageSize = isInteger.matcher(pageSize).matches() ? Integer.parseInt(pageSize) : 1;
		this.pageSize = this.pageSize > 0 ? this.pageSize : defaultPageSize;
		this.pageMark = pageMark;
	}

	public Page(String pageIndex, String pageSize) {
		this(pageIndex, pageSize, null);
	}

	public Page(int pageIndex, int pageSize) {
		this.pageIndex = pageIndex > 0 ? pageIndex : 1;
		this.pageSize = pageSize > 0 ? pageSize : defaultPageSize;
	}

	public int getFirstResult() {
		firstResult = (pageIndex - 1) * pageSize;
		return firstResult;
	}

	public void setFirstResult(int firstResult) {
		this.firstResult = firstResult;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Serializable getPageMark() {
		return pageMark;
	}

	public void setPageMark(Serializable pageMark) {
		this.pageMark = pageMark;
	}

}
