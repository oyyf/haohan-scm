package com.haohan.cloud.framework.dto.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.framework.entity.BaseStatus;
import com.haohan.cloud.framework.utils.JacksonUtils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * 接收到的返回数据
 *
 * @author zhaokuner
 *
 */
public class ApiResolveResp extends BaseResp {

	/**
	 *
	 */
	private static final long serialVersionUID = -7084456799376450043L;

	/** 是否加密 */
	protected Boolean encrypt = false;
	/** 回溯信息 */
	protected String state;
	/** 时间戳（毫秒 */
	protected Long timestamp;
	/** 业务数据 */
	protected String data = new String();

	@JsonIgnore
	protected String original;

	@JsonIgnore
	private String sercertKey;

	public static ApiResolveResp parse(String json) {
		if (null != json && !json.isEmpty()) {
			return JacksonUtils.readValue(json, ApiResolveResp.class);
		}
		return null;
	}

	public <T> T paraseData(Class<T> clazz) {
		return JacksonUtils.readValue(data, clazz);
	}

	public <T> List<T> paraseDataList(Class<T> clazz) {
		return JacksonUtils.readListValue(data, clazz);
	}

	@SuppressWarnings("unchecked")
	public <T> LinkedHashMap<String,T> paraseDataMap(Class<T> clazz) {
		return JacksonUtils.readMapValue(data, LinkedHashMap.class, clazz);
	}

	public ApiResolveResp putStatusEnum(BaseStatus baseStatus) {
		this.code = baseStatus.getCode();
		this.msg = baseStatus.getMsg();
		return this;
	}

	public String toJson() {
		return JacksonUtils.toJson(this);
	}

	public Boolean getEncrypt() {
		return encrypt;
	}

	public void setEncrypt(Boolean encrypt) {
		this.encrypt = encrypt;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSercertKey() {
		return sercertKey;
	}

	public void setSercertKey(String sercertKey) {
		this.sercertKey = sercertKey;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApiResolveResp [code=");
		builder.append(code);
		builder.append(", msg=");
		builder.append(msg);
		builder.append(", encrypt=");
		builder.append(encrypt);
		builder.append(", state=");
		builder.append(state);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", data=");
		builder.append(data);
		builder.append(", original=");
		builder.append(original);
		builder.append(", sercertKey=");
		builder.append(sercertKey);
		builder.append("]");
		return builder.toString();
	}

}
