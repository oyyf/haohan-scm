package com.haohan.cloud.framework.enums;

public interface SuperStrEnum {
	public String getCode() ;

    public void setCode(String code) ;

    public String getDescription() ;

    public void setDescription(String description) ;
}
