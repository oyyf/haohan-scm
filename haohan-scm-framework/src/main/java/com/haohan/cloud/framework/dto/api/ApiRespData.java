package com.haohan.cloud.framework.dto.api;

import com.haohan.cloud.framework.utils.JacksonUtils;

import java.io.Serializable;

/**
 * 返回数据 data基类
 * @author zhaokuner
 *
 */
public class ApiRespData implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1772501545160362738L;

	protected Serializable ext;

	public String toJson(){
		return JacksonUtils.toJson(this);
	}

	public Serializable getExt() {
		return ext;
	}

	public void setExt(Serializable ext) {
		this.ext = ext;
	}

}
