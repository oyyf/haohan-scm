package com.haohan.cloud.framework.dto;

import com.haohan.cloud.framework.utils.JacksonUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * 设备信息
 */
public class Device implements Serializable {

	private static final long serialVersionUID = -515304553575107218L;

	public static final String PARAMS_DEVICE = "device";

	/** 设备ID */
	private String deviceId;
	/** 用户设备操作系统 */
	private String os;
	/** 用户设备操作系统版本号 */
	private String osVer;
	/** 网络类型:3g wifi等 */
	private String netType;
	/** 用户手机号 */
	private String tel;
	/** 运营商名称 */
	private String netServer;
	/** 分辨率 */
	private String screen;
	/** 手机码 */
	private String imsi;
	/** mac */
	private String mac;
	/** ip */
	private String ip;
	/** abi */
	private String abi;
	/** 浏览器 */
	private String userAgent;

	public Device() {

	}

	public static Device init(HttpServletRequest req) {

		Device result = null;

		if (req != null) {
			String device = req.getParameter(PARAMS_DEVICE);
			result = init(device);
		}
		if(null == result){
			result = new Device();
		}

		return result;
	}

	public static Device init(String device) {

		Device result = null;

		if (StringUtils.isNotBlank(device)) {
			result = JacksonUtils.readValue(device, Device.class);
		}
		if(null == result){
			result = new Device();
		}
		return result;
	}

	public String toJson() {
		String json = JacksonUtils.toJson(this);
		return json;
	}

	@Override
	public String toString() {
		return "Device [deviceId=" + deviceId + ", os=" + os + ", osVer="
				+ osVer + ", netType=" + netType + ", tel=" + tel
				+ ", netServer=" + netServer + ", screen=" + screen + ", imsi="
				+ imsi + ", mac=" + mac + ", ip=" + ip + ", abi=" + abi
				+ ", userAgent=" + userAgent + "]";
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOsVer() {
		return osVer;
	}

	public void setOsVer(String osVer) {
		this.osVer = osVer;
	}

	public String getNetType() {
		return netType;
	}

	public void setNetType(String netType) {
		this.netType = netType;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getNetServer() {
		return netServer;
	}

	public void setNetServer(String netServer) {
		this.netServer = netServer;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getAbi() {
		return abi;
	}

	public void setAbi(String abi) {
		this.abi = abi;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

}
