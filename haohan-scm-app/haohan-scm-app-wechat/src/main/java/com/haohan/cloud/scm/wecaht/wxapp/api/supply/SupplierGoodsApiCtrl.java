package com.haohan.cloud.scm.wecaht.wxapp.api.supply;

import com.haohan.cloud.scm.api.supply.feign.SupplierGoodsFeignService;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsDetailReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsListReq;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplierGoods")
@Api(value = "SupplierGoodsApiCtrl", tags = "供应商商品管理接口服务")
public class SupplierGoodsApiCtrl {

    private SupplierGoodsFeignService supplierGoodsFeignService;

    private ScmWechatUtils scmWechatUtils;

    @GetMapping("/queryList")
    @ApiOperation(value = "查询供应商 售卖商品信息列表")
    public R querySupplierGoodsList(@Validated SupplierGoodsListReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return supplierGoodsFeignService.querySupplierGoodsList(req,SecurityConstants.FROM_IN);
    }
    /**
     * 查询供应商 售卖商品信息详情
     * @param req  必须：pmId/uId/supplierId/id
     * @return
     */
    @GetMapping("/queryDetail")
    @ApiOperation(value = "查询供应商 售卖商品信息详情")
    public R querySupplierGoodsDetail(@Validated SupplierGoodsDetailReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return supplierGoodsFeignService.querySupplierGoodsDetail(req,SecurityConstants.FROM_IN);
    }
}
