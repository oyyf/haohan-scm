package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseEmployeeFeignService;
import com.haohan.cloud.scm.api.purchase.req.BuyerAddSupplierReq;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.feign.SupplierEvaluateFeignService;
import com.haohan.cloud.scm.api.supply.feign.SupplierFeignService;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplierManage")
@Api(value = "BaseApiCtrl", tags = "微信小程序基础接口服务")
public class SupplierManageApiCtrl {
    private final ScmWechatUtils scmWechatUtils;
    private final SupplierFeignService supplierFeignService;
    private final SupplierEvaluateFeignService supplierEvaluateFeignService;
    private final PurchaseEmployeeFeignService purchaseEmployeeFeignService;
    private final OfferOrderFeignService offerOrderFeignService;

    /**
     * 查询供应商列表
     * @param req
     * @return
     */
    @GetMapping("/querySupplierList")
    @ApiOperation(value = "查询供应商列表-小程序")
    public R querySupplierList(@Validated QuerySupplierListReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        SupplierReq supplierReq = new SupplierReq();
        BeanUtil.copyProperties(req,supplierReq);
        return supplierFeignService.getSupplierPage(supplierReq, SecurityConstants.FROM_IN);
    }

    /**
     * 查询供应商详情
     * @param req   必须：pmId/uId/id
     * @return
     */
    @GetMapping("/querySupplier")
    @ApiOperation(value = "查询供应商详情-小程序")
    public R querySupplier(@Validated SupplierReq req) {
        scmWechatUtils.queryUPassportById(req.getUid());
        SupplierReq supplierReq = new SupplierReq();
        supplierReq.setPmId(req.getPmId());
        supplierReq.setId(req.getId());
        return supplierFeignService.getOneBySupplierReq(supplierReq,SecurityConstants.FROM_IN);
    }

    /**
     * 查询供应商评价记录列表
     * @param req  必须：pmId/uId/supplierId
     * @return
     */
    @GetMapping("/queryList")
    @ApiOperation(value = "查询供应商评价记录")
    public R querySupplierEvaluateList(@Validated SupplierEvaluateListReq req) {
        scmWechatUtils.queryUPassportById(req.getUid());
        SupplierEvaluateReq supplierEvaluateReq = new SupplierEvaluateReq();
        BeanUtil.copyProperties(req,supplierEvaluateReq);
        return supplierEvaluateFeignService.getSupplierEvaluatePage(supplierEvaluateReq,SecurityConstants.FROM_IN);
    }
    /**
     * 采购员工新增供应商
     * @param req
     * @return
     */
    @PostMapping("/addSupplier")
    @ApiOperation(value = "采购员工新增供应商")
    public R addSupplier(@Validated BuyerAddSupplierReq req){

        return purchaseEmployeeFeignService.addSupplier(req,SecurityConstants.FROM_IN);
    }
    /**
     * 查询报价单列表（根据报价类型和状态）
     * @param req
     * @return
     */
    @PostMapping("/queryOfferOrder")
    @ApiOperation(value = "查询报价单列表")
    public R queryOfferOrder(@RequestBody @Validated QueryOfferOrderReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        BeanUtil.copyProperties(req,offerOrderReq);
        return offerOrderFeignService.getOfferOrderList(offerOrderReq,SecurityConstants.FROM_IN);
    }
    /**
     * 查询供应商货源（加筛选条件）
     * @param req
     * @return
     */
    @GetMapping("/filterOfferOrder")
    @ApiOperation(value = "查询供应商货源（加筛选条件）")
    public R filterOfferOrder(@Validated FilterOfferOrderReq req) {
        scmWechatUtils.queryUPassportById(req.getUid());
        return offerOrderFeignService.filterOfferOrder(req, SecurityConstants.FROM_IN);
    }
}
