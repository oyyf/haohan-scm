package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderDetailFeignService;
import com.haohan.cloud.scm.api.purchase.req.CompleteTaskReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.api.wechat.req.QueryGoodsDeliveryReq;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品交付Api
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/goodsDelivery")
@Api(value = "TaskManageApiCtrl", tags = "商品交付接口服务")
public class GoodsDeliveryApiCtrl {
    private final ScmWechatUtils scmWechatUtils;
    private final PurchaseOrderDetailFeignService purchaseOrderDetailFeignService;

    @GetMapping("/queryGoodsDeliveryList")
    @ApiOperation(value = "查询货品交付列表")
    public R queryGoodsDeliveryList(@Validated QueryGoodsDeliveryReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return new R(scmWechatUtils.queryDetailListByStatus(req));
    }

    @GetMapping("/queryGoodsDeliveryDetail")
    @ApiOperation(value = "查询货品交付详情")
    public R queryGoodsDeliveryDetail(@Validated QueryGoodsDeliveryReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        PurchaseOrderDetailReq query = new PurchaseOrderDetailReq();
        BeanUtil.copyProperties(req,query);
        return purchaseOrderDetailFeignService.getOneByPurchaseOrderDetailReq(query, SecurityConstants.FROM_IN);
    }

    @PostMapping("/completeTask")
    @ApiOperation(value = "点击完成采购明细")
    public R completeTask(@RequestBody @Validated CompleteTaskReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        PurchaseOrderDetail detail = scmWechatUtils.queryPurchaseOrderDetail(req);
        PurchaseOrderDetail upd = new PurchaseOrderDetail();
        upd.setId(detail.getId());
        upd.setPurchaseStatus(req.getPurchaseStatus());
        return purchaseOrderDetailFeignService.updateById(upd,SecurityConstants.FROM_IN);
    }

    @PostMapping("/deliveryTask")
    @ApiOperation(value = "确认揽货")
    public R deliveryTask(@RequestBody @Validated CompleteTaskReq req) {
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseOrderDetailFeignService.deliveryTask(req,SecurityConstants.FROM_IN);
    }
}
