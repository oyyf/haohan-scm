package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.haohan.cloud.scm.api.wechat.req.WxShopReq;

/**
 * @author dy
 * @date 2020/4/20
 */
public interface WechatMerchantCoreService {

    MerchantShopVO platformShopList(WxShopReq req);
}
