package com.haohan.cloud.scm.wecaht.wxapp.core.impl;

import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.haohan.cloud.scm.api.wechat.req.WxShopReq;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatMerchantCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/4/20
 */
@Service
@AllArgsConstructor
public class WechatMerchantCoreServiceImpl implements WechatMerchantCoreService {

    private final ScmWechatUtils scmWechatUtils;

    @Override
    public MerchantShopVO platformShopList(WxShopReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        Merchant merchant = scmWechatUtils.fetchPlatformMerchant();
        return scmWechatUtils.fetchMerchantShop(merchant.getId());
    }
}
