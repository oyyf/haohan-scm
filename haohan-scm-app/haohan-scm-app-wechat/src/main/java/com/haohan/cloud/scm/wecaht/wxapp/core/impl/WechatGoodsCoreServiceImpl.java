package com.haohan.cloud.scm.wecaht.wxapp.core.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.GoodsCollectionsReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsCollectionsReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatGoodsCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dy
 * @date 2020/5/23
 */
@Service
@AllArgsConstructor
public class WechatGoodsCoreServiceImpl implements WechatGoodsCoreService {

    private final ScmWechatUtils scmWechatUtils;

    @Override
    public GoodsVO fetchInfo(WxGoodsQueryReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        req.setBuyerId(fetchBuyerId(req.getUid()));
        return scmWechatUtils.fetchGoodsInfoById(req.transToQuery());
    }

    @Override
    public IPage<GoodsVO> findPage(WxGoodsQueryReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        // 带收藏状态 带平台定价
        req.setBuyerId(fetchBuyerId(req.getUid()));
        return scmWechatUtils.queryGoodsInfoPage(req.transToQuery());
    }

    @Override
    public List<GoodsCategoryTree> findCategoryTree(WxGoodsQueryReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        return scmWechatUtils.findCategoryTree(req.getShopId(), req.getAllShow());
    }

    @Override
    @Cacheable(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, key = "'collections'+ #req")
    public IPage<GoodsVO> findCollectionsPage(WxGoodsQueryReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        // 带收藏状态
        req.setBuyerId(fetchBuyerId(req.getUid()));
        return scmWechatUtils.findCollectionsPage(req.transToQuery());
    }

    @Override
    @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true)
    public boolean addCollections(WxGoodsCollectionsReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        GoodsCollectionsReq query = new GoodsCollectionsReq();
        query.setUid(req.getUid());
        query.setGoodsId(req.getGoodsId());
        return scmWechatUtils.addCollections(query);
    }

    @Override
    @CacheEvict(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, allEntries = true)
    public boolean removeCollections(WxGoodsCollectionsReq req) {
        // uid 验证
        scmWechatUtils.queryUPassportById(req.getUid());
        GoodsCollectionsReq query = new GoodsCollectionsReq();
        query.setUid(req.getUid());
        query.setGoodsId(req.getGoodsId());
        return scmWechatUtils.removeCollections(query);
    }

    /**
     * 获取当前uid对应采购商id
     *
     * @param uid
     * @return
     */
    private String fetchBuyerId(String uid) {
        try {
            Buyer buyer = scmWechatUtils.fetchBuyerByUid(uid);
            return buyer.getId();
        } catch (ErrorDataException e) {
            return null;
        }
    }
}
