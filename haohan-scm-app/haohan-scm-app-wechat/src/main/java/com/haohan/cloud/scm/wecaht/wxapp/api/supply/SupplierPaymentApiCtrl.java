package com.haohan.cloud.scm.wecaht.wxapp.api.supply;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.bill.feign.PayableBillFeignService;
import com.haohan.cloud.scm.api.bill.req.BillInfoFeignReq;
import com.haohan.cloud.scm.api.opc.req.payment.SupplierPaymentReq;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.api.supply.req.QueryPaymentDetailReq;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 *  已修改为使用bill模块
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplierPayment")
@Api(value = "SupplierPaymentApiCtrl", tags = "账单管理接口服务")
public class SupplierPaymentApiCtrl {

    private final PayableBillFeignService payableBillFeignService;
    private final OfferOrderFeignService offerOrderFeignService;

    private final ScmWechatUtils scmWechatUtils;

    @GetMapping("/querySupplierPayment")
    @ApiOperation(value = "查询供应商货款详情")
    public R querySupplierPayment(@Validated QueryPaymentDetailReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        OfferOrderReq q  = new OfferOrderReq();
        q.setPmId(req.getPmId());
        q.setOfferOrderId(req.getOfferOrderId());
        return offerOrderFeignService.getOfferOrderList(q,SecurityConstants.FROM_IN);
    }

    @GetMapping("/querySupplierPaymentPage")
    @ApiOperation(value = "查询供应商货款列表")
    public R querySupplierPaymentPage(SupplierPaymentReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        BillInfoFeignReq feignReq = new BillInfoFeignReq();
        BeanUtil.copyProperties(req, feignReq);
        return payableBillFeignService.findPage(feignReq,SecurityConstants.FROM_IN);
    }


}
