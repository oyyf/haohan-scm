package com.haohan.cloud.scm.wecaht.wxapp.api.supply;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.supply.feign.SupplierFeignService;
import com.haohan.cloud.scm.api.supply.req.QuerySupplierReq;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplierCenter")
@Api(value = "SupplierCenterApiCtrl", tags = "个人中心接口服务")
public class SupplierCenterApiCtrl {

    private final SupplierFeignService supplierFeignService;

    private final ScmWechatUtils scmWechatUtils;

    @GetMapping("/querySupplier")
    @ApiOperation(value = "查询供应商详情")
    public R querySupplier(@Validated QuerySupplierReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        SupplierReq supplierReq = new SupplierReq();
        BeanUtil.copyProperties(req,supplierReq);
        return supplierFeignService.getOneBySupplierReq(supplierReq,SecurityConstants.FROM_IN);
    }

    @PostMapping("/updateSupplier")
    @ApiOperation(value = "编辑供应商详情")
    public R updateSupplier(@RequestBody @Validated SupplierReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return supplierFeignService.updateById(req,SecurityConstants.FROM_IN);
    }
}
