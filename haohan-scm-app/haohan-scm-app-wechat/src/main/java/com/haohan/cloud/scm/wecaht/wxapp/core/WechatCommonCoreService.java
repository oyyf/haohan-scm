package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.haohan.cloud.scm.api.wechat.req.WechatAddReq;
import com.haohan.cloud.scm.api.wechat.req.WxTenantReq;
import com.haohan.cloud.scm.api.wechat.vo.WxPassportVO;
import com.haohan.cloud.scm.api.wechat.vo.WxTenantVO;

/**
 * @author dy
 * @date 2020/5/27
 */
public interface WechatCommonCoreService {

    WxTenantVO fetchTenantId(WxTenantReq req);

    WxPassportVO addPassport(WechatAddReq req);

}
