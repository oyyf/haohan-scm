package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.aftersales.feign.AfterSalesFeignService;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesReq;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesListReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/afterService")
@Api(value = "BaseApiCtrl", tags = "微信小程序基础接口服务")
public class AfterServiceApiCtrl {
    private final AfterSalesFeignService afterSalesFeignService;
    private final ScmWechatUtils scmWechatUtils;
    /**
     * 查询售后单列表
     * @param req
     * @return
     */
    @PostMapping("/queryAfterSalesList")
    @ApiOperation(value = "查询售后单列表--小程序" , notes = "按售后状态分类查询")
    public R queryAfterSalesList(@RequestBody @Validated QueryAfterSalesListReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("没有通行证");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        AfterSalesReq afterSalesReq = new AfterSalesReq();
        afterSalesReq.setPmId(req.getPmId());
        afterSalesReq.setAfterSalesStatus(req.getAfterSalesStatus());
        afterSalesReq.setPageNo(req.getCurrent());
        afterSalesReq.setPageSize(req.getSize());
        return afterSalesFeignService.getAfterSalesPage(afterSalesReq,SecurityConstants.FROM_IN);
    }
    /**
     * 查询售后单详情
     * @param req
     * @return
     */
    @GetMapping("/queryAfterSalesDetail")
    @ApiOperation(value = "查询售后单详情")
    public R queryAfterSalesDetail(@Validated QueryAfterSalesDetailReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("没有通行证");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        return afterSalesFeignService.queryAfterSalesDetail(req, SecurityConstants.FROM_IN);
    }



}
