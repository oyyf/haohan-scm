package com.haohan.cloud.scm.wecaht.wxapp.api;

import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.api.wechat.req.WxBuyerStatusReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBuyerCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/9/19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/buyer")
@Api(value = "BuyerApiCtrl", tags = "微信小程序buyer")
public class WechatBuyerApiCtrl {

    private final WechatBuyerCoreService wechantBuyerCoreService;

    /**
     * 查询采购商是否可下单
     *
     * @param req
     * @return
     */
    @GetMapping("/queryStatus")
    public R<Boolean> buyerStatus(@Validated WxBuyerStatusReq req) {
        return RUtil.success(wechantBuyerCoreService.buyerStatus(req));
    }

    /**
     * 查询采购商是否需下单支付
     *
     * @param req
     * @return
     */
    @GetMapping("/payInfo")
    public R<BuyerPayVO> buyerPayInfo(@Validated WxBuyerStatusReq req) {
        return RUtil.success(wechantBuyerCoreService.buyerPayInfo(req));
    }
}
