/**
 *  *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wecaht.wxapp;

/**
 * @author zgw
 * @date 2019/07/09
 * 数据报表
 */
//@EnablePigxSwagger2
//@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
//@SpringCloudApplication
//@EnablePigxFeignClients(basePackages = {"com.pig4cloud.pigx.*","com.haohan.cloud.scm.api.*"})
//@EnablePigxResourceServer
//@ComponentScan(basePackages = "com.haohan.cloud.scm")
//@MapperScan(basePackages = "com.haohan.cloud.scm.*.mapper")
//public class HaohanScmWxAppServiceApplication {
//
//    public static void main(String[] args) {
//        SpringApplication.run(HaohanScmWxAppServiceApplication.class, args);
//    }
//}
