package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderDetailFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseReportFeignService;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.QueryPurchaseOrderDetailResp;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.feign.SupplierFeignService;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/goodsPurchase")
@Api(value = "BaseApiCtrl", tags = "商品基础接口服务")
public class GoodsPurchaseApiCtrl {

    private final PurchaseOrderFeignService purchaseOrderFeignService;
    private final PurchaseOrderDetailFeignService purchaseOrderDetailFeignService;
    private final ScmWechatUtils scmWechatUtils;
    private final SupplierFeignService supplierFeignService;
    private final PurchaseReportFeignService purchaseReportFeignService;

    @PostMapping("/singlePurchase")
    @ApiOperation(value = "采购单明细 供应商确定(小程序:单品采购)")
    public R singlePurchase(@Validated SinglePurchaseReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseOrderFeignService.singlePurchase(req,SecurityConstants.FROM_IN);
    }

    @PostMapping("/selectOrderSupplier")
    @ApiOperation(value = "采购单明细 供应商确定(小程序:竞价采购)")
    public R selectOrderSupplier(@Validated ChoiceSupplierReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseOrderFeignService.selectOrderSupplier(req,SecurityConstants.FROM_IN);
    }

    @PostMapping("/agreementPurchase")
    @ApiOperation(value = "采购单明细 供应商确定(协议供应)")
    public R agreementPurchase(@Validated AgreementPurchaseReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseOrderFeignService.agreementPurchase(req,SecurityConstants.FROM_IN);
    }
    /**
     * 查询采购单列表
     * @param req
     * @return
     */
    @PostMapping("/queryPurchaseOrderList")
    @ApiOperation(value = "查询采购单列表")
    public R queryPurchaseOrderList(@Validated QueryPurchaseOrderListReq req){
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        PurchaseOrderReq purchaseOrderReq = new PurchaseOrderReq();
        BeanUtil.copyProperties(req,purchaseOrderReq);
        return purchaseOrderFeignService.getPurchaseOrderPage(purchaseOrderReq,SecurityConstants.FROM_IN);
    }
    /**
     * 根据采购单号查询采购单明细列表
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseOrderDetailList")
    @ApiOperation(value = "根据采购单号查询采购单明细列表")
    public R queryPurchaseOrderDetailList(@Validated QueryPurchaseOrderDetailListReq req){
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        PurchaseOrderDetailReq purchaseOrderDetailReq = new PurchaseOrderDetailReq();
        BeanUtil.copyProperties(req,purchaseOrderDetailReq);
        return purchaseOrderDetailFeignService.getPurchaseOrderDetailPage(purchaseOrderDetailReq,SecurityConstants.FROM_IN);
    }
    /**
     * 查询采购单明细
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseOrderDetail")
    @ApiOperation(value = "查询采购单明细")
    public R<QueryPurchaseOrderDetailResp> queryPurchaseOrderDetail(@Validated QueryPurchaseOrderDetailReq req){
        QueryPurchaseOrderDetailResp resp = new QueryPurchaseOrderDetailResp();
        if (StrUtil.isEmpty(req.getUId())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUId());
        PurchaseOrderDetailReq purchaseOrderDetailReq = new PurchaseOrderDetailReq();
        BeanUtil.copyProperties(req,purchaseOrderDetailReq);
        R r = purchaseOrderDetailFeignService.getOneByPurchaseOrderDetailReq(purchaseOrderDetailReq, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()){
            throw new ErrorDataException("查询采购单明细出错");
        }
        PurchaseOrderDetail orderDetail = BeanUtil.toBean(r.getData(), PurchaseOrderDetail.class);
        BeanUtil.copyProperties(orderDetail,resp);
        if (orderDetail.getMethodType()== MethodTypeEnum.agreement){
            //查询供应商信息
            R one = supplierFeignService.getById(orderDetail.getSupplierId(), SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(one) || null == one.getData()){
                throw new ErrorDataException("查询供应商信息出错");
            }
            Supplier supplier = BeanUtil.toBean(one.getData(), Supplier.class);
            resp.setTelephone(supplier.getTelephone());
            resp.setAddress(supplier.getAddress());
            resp.setSupplierLevel(supplier.getSupplierLevel());
        }
        return new R<>(resp);
    }
    /**
     *  采购部  总监发起采购计划
     * @param req
     * @return
     */
    @PostMapping("/initiatePurchasePlanByDirector")
    @ApiOperation(value = "发起采购计划", notes = "总监发起采购计划—小程序")
    public R initiatePurchasePlanByDirector(@Validated InitiatePurchasePlanReq req){
        if (StrUtil.isEmpty(req.getUId())){
            throw new EmptyDataException("通行证id为空");
        }
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseOrderFeignService.initiatePurchasePlanByDirector(req,SecurityConstants.FROM_IN);
    }

    /**
     * 查询所有采购单明细列表（可根据条件筛选）
     * @param req
     * @return
     */
    @PostMapping("/fetchPurchaseOrderDetailList")
    @ApiOperation(value = "查询所有采购单明细列表（可根据条件筛选）")
    public R fetchPurchaseOrderDetailList(@RequestBody @Validated FetchPurchaseOrderDetailListReq req){
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        ChoiceSupplierReq choiceSupplierReq = new ChoiceSupplierReq();
        BeanUtil.copyProperties(req,choiceSupplierReq);
        return purchaseOrderDetailFeignService.getOne(choiceSupplierReq,SecurityConstants.FROM_IN);
    }
    /**
     * 查询采购汇报列表
     *
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseReportList")
    @ApiModelProperty(value = "采购汇报列表",notes = "查询采购汇报列表——小程序")
    public R queryPurchaseReportList(@Validated PurchaseQueryPurchaseReportListReq req) {
        if (StrUtil.isEmpty(req.getUId())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUId());
        PurchaseReportReq report = new PurchaseReportReq();
        BeanUtil.copyProperties(req,report);
        report.setPageSize(req.getSize());
        report.setPageNo(req.getCurrent());
        return purchaseReportFeignService.getPurchaseReportPage(report,SecurityConstants.FROM_IN);
    }
    /**
     * 回复采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/answerPurchaseReport")
    @ApiOperation(value = "回复采购汇报",notes = "回复采购汇报——小程序")
    public R answerPurchaseReport(@Validated PurchaseAnswerPurchaseReportReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseReportFeignService.answerPurchaseReport(req,SecurityConstants.FROM_IN);
    }
    /**
     * 新增 采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/addPurchaseReport")
    public R addPurchaseReport(@Validated PurchaseAddPurchaseReportReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        PurchaseReport report = new PurchaseReport();
        BeanUtil.copyProperties(req,report);
        return purchaseReportFeignService.save(report,SecurityConstants.FROM_IN);
    }
    /**
     * 修改 采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/modifyPurchaseReport")
    public R modifyPurchaseReport(@Validated PurchaseModifyPurchaseReportReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseReportFeignService.modifyPurchaseReport(req,SecurityConstants.FROM_IN);
    }
}
