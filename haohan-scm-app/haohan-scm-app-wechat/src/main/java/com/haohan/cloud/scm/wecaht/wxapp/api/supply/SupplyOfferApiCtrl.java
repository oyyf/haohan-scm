package com.haohan.cloud.scm.wecaht.wxapp.api.supply;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplyOffer")
@Api(value = "SupplyOfferApiCtrl", tags = "报价单接口服务")
public class SupplyOfferApiCtrl {

    private final OfferOrderFeignService offerOrderFeignService;
    private final ScmWechatUtils scmWechatUtils;

    @PostMapping("/queryOfferList")
    @ApiOperation(value = "查询报价单列表")
    public R queryOfferList(@RequestBody @Validated QueryOfferListReq req) {
        Supplier supplier = scmWechatUtils.fetchByUid(req.getPmId(), req.getUid());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        BeanUtil.copyProperties(req, offerOrderReq);
        offerOrderReq.setSupplierId(supplier.getId());
        R r = offerOrderFeignService.getOfferOrderPage(offerOrderReq, SecurityConstants.FROM_IN);
        Object data = r.getData();
        Page p = scmWechatUtils.queryOfferOrder(data);
        return R.ok(p);
    }

    @GetMapping("/queryOfferDetail")
    @ApiOperation(value = "查询报价单详情")
    public R queryOfferDetail(@Validated QueryOfferDetailReq req) {
        Supplier supplier = scmWechatUtils.fetchByUid(req.getPmId(), req.getUid());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        BeanUtil.copyProperties(req, offerOrderReq);
        offerOrderReq.setSupplierId(supplier.getId());
        return offerOrderFeignService.getOneByOfferOrderReq(offerOrderReq, SecurityConstants.FROM_IN);
    }

    @PostMapping("/affirmOffer")
    @ApiOperation(value = "供应商报价")
    public R affirmOffer(@RequestBody @Validated AffirmOfferReq req) {
        scmWechatUtils.queryUPassportById(req.getUid());
        return offerOrderFeignService.affirmOffer(req, SecurityConstants.FROM_IN);
    }

    @PostMapping("/updateOfferOrder")
    @ApiOperation(value = "修改报价单")
    public R updateOfferOrder(@RequestBody @Validated UpdateOfferOrderReq req) {

        return offerOrderFeignService.updateOfferOrder(req, SecurityConstants.FROM_IN);
    }
}
