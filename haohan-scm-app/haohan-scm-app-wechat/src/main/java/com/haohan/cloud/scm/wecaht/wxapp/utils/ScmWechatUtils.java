package com.haohan.cloud.scm.wecaht.wxapp.utils;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.feign.ReceivableBillFeignService;
import com.haohan.cloud.scm.api.bill.req.BillInfoFeignReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.feign.GoodsCategoryFeignService;
import com.haohan.cloud.scm.api.goods.feign.GoodsCollectionsFeignService;
import com.haohan.cloud.scm.api.goods.feign.GoodsFeignService;
import com.haohan.cloud.scm.api.goods.feign.GoodsModelFeignService;
import com.haohan.cloud.scm.api.goods.req.GoodsCategoryReq;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.feign.MerchantFeignService;
import com.haohan.cloud.scm.api.manage.feign.ShopFeignService;
import com.haohan.cloud.scm.api.manage.feign.UPassportFeignService;
import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.feign.MarketRecordFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseEmployeeFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderDetailFeignService;
import com.haohan.cloud.scm.api.purchase.req.CompleteTaskReq;
import com.haohan.cloud.scm.api.purchase.req.MarketRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseEmployeeReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.api.purchase.resp.MarketRecordResp;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.feign.BuyOrderFeignService;
import com.haohan.cloud.scm.api.saleb.feign.BuyerFeignService;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderFeignReq;
import com.haohan.cloud.scm.api.saleb.req.BuyerReq;
import com.haohan.cloud.scm.api.saleb.req.GoodsCollectionsReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyerStatusReq;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.feign.SupplierFeignService;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.haohan.cloud.scm.api.supply.resp.SupplyGoodsDetailResp;
import com.haohan.cloud.scm.api.wechat.req.QueryGoodsDeliveryReq;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderReq;
import com.haohan.cloud.scm.api.wechat.resp.OfferOrderResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Service
@AllArgsConstructor
public class ScmWechatUtils {
    private final UPassportFeignService uPassportFeignService;
    private final PurchaseEmployeeFeignService purchaseEmployeeFeignService;
    private final PurchaseOrderDetailFeignService purchaseOrderDetailFeignService;
    private final SupplierFeignService supplierFeignService;
    private final GoodsCategoryFeignService goodsCategoryFeignService;
    private final MarketRecordFeignService marketRecordFeignService;
    private final OfferOrderFeignService offerOrderFeignService;
    private final GoodsFeignService goodsFeignService;
    private final GoodsModelFeignService goodsModelFeignService;
    private final GoodsCollectionsFeignService goodsCollectionsFeignService;
    private final BuyerFeignService buyerFeignService;
    private final MerchantFeignService merchantFeignService;
    private final ShopFeignService shopFeignService;
    private final ReceivableBillFeignService receivableBillFeignService;
    private final BuyOrderFeignService buyOrderFeignService;

    @Cacheable(value = ScmCacheNameConstant.MANAGE_PLATFORM_MERCHANT, key = "0")
    public Merchant fetchPlatformMerchant() {
        R<Merchant> r = merchantFeignService.fetchPlatformMerchant(SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || r.getData() == null) {
            throw new ErrorDataException("获取平台商家时失败");
        }
        return r.getData();
    }

    @Cacheable(value = ScmCacheNameConstant.MANAGE_MERCHANT_SHOP, key = "#merchantId", condition = "#merchantId != null")
    public MerchantShopVO fetchMerchantShop(String merchantId) {
        R<MerchantShopVO> r = shopFeignService.fetchMerchantShop(merchantId, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("获取商家店铺时失败");
        }
        return r.getData();
    }

    /**
     * 根据id 查询 uPassport
     *
     * @param id
     * @return
     */
    @Cacheable(value = ScmCacheNameConstant.WECHAT_PASSPORT_INFO, key = "#id", condition = "#id != null")
    public UPassport queryUPassportById(String id) {
        R<UPassport> resp = uPassportFeignService.getById(id, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(resp) || resp.getData() == null) {
            throw new ErrorDataException("通行证查询时有误");
        }
        return resp.getData();
    }

    /**
     * 根据id 更新 uPassport
     *
     * @param uPassport id
     * @return
     */
    public Boolean updateUPassport(UPassport uPassport) {
        R resp = uPassportFeignService.updateById(uPassport, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || !((Boolean) resp.getData())) {
            throw new EmptyDataException("uPassport更新有误");
        }
        return true;
    }

    /**
     * 查询供应商信息
     *
     * @param pmId
     * @param uid
     * @return
     */
    public Supplier fetchByUid(String pmId, String uid) {
        // 供应商
        SupplierReq querySupplier = new SupplierReq();
        querySupplier.setPmId(pmId);
        querySupplier.setPassportId(uid);
        R r = supplierFeignService.getOneBySupplierReq(querySupplier, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new ErrorDataException("供应商信息有误");
        }
        return BeanUtil.toBean(r.getData(), Supplier.class);
    }

    /**
     * 查询供应商
     *
     * @param id
     * @return
     */
    public Supplier getSupplierById(String id) {
        R r = supplierFeignService.getById(id, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new ErrorDataException("供应商信息有误");
        }
        return BeanUtil.toBean(r.getData(), Supplier.class);
    }

    /**
     * 根据uid/ telephone 查询 采购员工
     *
     * @param req uid/telephone
     * @return 可能为null
     */
    public PurchaseEmployee fetchPurchaseEmployee(PurchaseEmployeeReq req) {
        // 启用状态
        req.setUseStatus(UseStatusEnum.enabled);
        R resp = purchaseEmployeeFeignService.getOneByPurchaseEmployeeReq(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp)) {
            throw new EmptyDataException("采购员工查询有误");
        } else if (null == resp.getData()) {
            return null;
        }
        return BeanUtil.toBean(resp.getData(), PurchaseEmployee.class);
    }

    /**
     * 根据id 更新采购员工
     *
     * @param req id
     * @return 更新失败 抛出异常
     */
    public Boolean updatePurchaseEmployee(PurchaseEmployee req) {
        // 启用状态
        R resp = purchaseEmployeeFeignService.updateById(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || !((Boolean) resp.getData())) {
            throw new EmptyDataException("采购员工更新有误");
        }
        return true;
    }

    /**
     * 根据状态查询采购明细列表
     *
     * @param query
     * @return
     */
    public List<PurchaseOrderDetail> queryDetailList(PurchaseOrderDetailReq query) {
        R r = purchaseOrderDetailFeignService.getPurchaseOrderDetailList(query, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r.getData() == null) {
            throw new EmptyDataException();
        }
        List list = (List) r.getData();
        List<PurchaseOrderDetail> result = new ArrayList<>();
        for (Object o : list) {
            PurchaseOrderDetail detail = BeanUtil.toBean(o, PurchaseOrderDetail.class);
            result.add(detail);
        }
        return result;
    }

    /**
     * 查询采购交付列表
     *
     * @param req
     * @return
     */
    public List<PurchaseOrderDetail> queryDetailListByStatus(QueryGoodsDeliveryReq req) {
        List<PurchaseOrderDetail> result = new ArrayList<>();
        PurchaseOrderDetailReq query = new PurchaseOrderDetailReq();
        BeanUtil.copyProperties(req, query);
        query.setPurchaseStatus(PurchaseStatusEnum.purchasing);
        List<PurchaseOrderDetail> purchasing = this.queryDetailList(query);
        result.addAll(purchasing);
        query.setPurchaseStatus(PurchaseStatusEnum.stockUp);
        List<PurchaseOrderDetail> stockUp = this.queryDetailList(query);
        result.addAll(stockUp);
        query.setPurchaseStatus(PurchaseStatusEnum.haveSales);
        List<PurchaseOrderDetail> haveSales = this.queryDetailList(query);
        result.addAll(haveSales);
        query.setPurchaseStatus(PurchaseStatusEnum.finish);
        List<PurchaseOrderDetail> finish = this.queryDetailList(query);
        result.addAll(finish);
        return result;
    }

    /**
     * 查询采购单明细
     *
     * @param req
     * @return
     */
    public PurchaseOrderDetail queryPurchaseOrderDetail(CompleteTaskReq req) {
        PurchaseOrderDetailReq query = new PurchaseOrderDetailReq();
        BeanUtil.copyProperties(req, query);
        R r = purchaseOrderDetailFeignService.getOneByPurchaseOrderDetailReq(query, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r.getData() == null) {
            throw new EmptyDataException();
        }
        return BeanUtil.toBean(r.getData(), PurchaseOrderDetail.class);
    }

    /**
     * 根据uid/ telephone 查询 供应商
     *
     * @param req uid/telephone
     * @return 可能为null
     */
    public Supplier fetchSupplier(SupplierReq req) {
        // 启用状态
        req.setStatus(UseStatusEnum.enabled);
        R resp = supplierFeignService.getOneBySupplierReq(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp)) {
            throw new EmptyDataException("查询供应商有误");
        } else if (null == resp.getData()) {
            return null;
        }
        return BeanUtil.toBean(resp.getData(), Supplier.class);
    }


    /**
     * 根据id 更新 供应商
     *
     * @param req id
     * @return 更新失败 抛出异常
     */
    public Boolean updateSupplier(Supplier req) {
        // 启用状态
        R resp = supplierFeignService.updateById(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp) || !((Boolean) resp.getData())) {
            throw new EmptyDataException("供应商更新有误");
        }
        return true;
    }


    /**
     * 查询市场行情详情
     *
     * @param record
     * @return
     */
    public MarketRecordResp queryMarketPrice(MarketRecordReq record) {
        R r = marketRecordFeignService.getOneByMarketRecordReq(record, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r == null) {
            throw new EmptyDataException();
        }
        return BeanUtil.toBean(r.getData(), MarketRecordResp.class);
    }

    /**
     * 查询报价单详情
     *
     * @param req
     * @return
     */
    public SupplyGoodsDetailResp supplyGoodsDetail(OfferOrderReq req) {
        R r = offerOrderFeignService.getOneByOfferOrderReq(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r == null) {
            throw new EmptyDataException();
        }

        return BeanUtil.toBean(r.getData(), SupplyGoodsDetailResp.class);
    }


    /**
     * 补全报价单信息
     *
     * @param list
     * @return
     */
    public Page queryOfferOrder(Object list) {
        Page page = BeanUtil.toBean(list, Page.class);
        List<OfferOrderResp> res = new ArrayList<>();
        page.getRecords().forEach(val -> {
            OfferOrderResp resp = BeanUtil.toBean(val, OfferOrderResp.class);
            if (resp.getGoodsModelId() != null) {
                GoodsModelDTO dto = this.queryGoodsDto(resp.getGoodsModelId());
                resp.setModelName(dto.getModelName());
                resp.setGoodsImg(dto.getModelUrl());
                resp.setUnit(dto.getModelUnit());
            } else {
                Goods goods = this.queryGoods(resp.getGoodsId());
                resp.setGoodsImg(goods.getThumbUrl());
            }
            resp.setCateName(this.queryGoodsInfo(resp.getGoodsId()));
            res.add(resp);
        });
        page.setRecords(res);
        return page;
    }

    /**
     * 查询规格商品信息
     *
     * @param modelId
     * @return
     */
    public GoodsModelDTO queryGoodsDto(String modelId) {
        R<GoodsModelDTO> r = goodsModelFeignService.getInfoById(modelId, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r == null) {
            throw new EmptyDataException();
        }

        return r.getData();
    }

    /**
     * 查询商品信息
     *
     * @param goodsId
     * @return
     */
    public Goods queryGoods(String goodsId) {
        R r = goodsFeignService.getById(goodsId, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r == null) {
            throw new EmptyDataException();
        }
        Goods goods = BeanUtil.toBean(r.getData(), Goods.class);
        return goods;
    }

    /**
     * 通过goodsId查询分类名称
     *
     * @param id
     * @return
     */
    public String queryGoodsInfo(String id) {
        R r = goodsFeignService.getById(id, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r == null) {
            throw new EmptyDataException();
        }
        Goods goods = BeanUtil.toBean(r.getData(), Goods.class);
        return this.queryGoodsCate(goods.getGoodsCategoryId());
    }

    /**
     * 获取商品详情
     *
     * @param req 必须goodsId, 可选 buyerId、pricingDate
     * @return
     */
    @Cacheable(value = ScmCacheNameConstant.GOODS_INFO_DETAIL, key = "#req.goodsId+':'+#req.pricingMerchantId+#req.pricingDate+#req.buyerId")
    public GoodsVO fetchGoodsInfoById(GoodsFeignReq req) {
        R<GoodsVO> r = goodsFeignService.fetchGoodsInfoById(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("获取商品详情时失败" + r.getMsg());
        }
        return r.getData();
    }

    /**
     * 商品详情列表 带收藏状态  带平台定价
     *
     * @param req
     * @return
     */
    @Cacheable(value = ScmCacheNameConstant.WECHAT_BUYER_GOODS_PAGE, key = "#req")
    public IPage<GoodsVO> queryGoodsInfoPage(GoodsFeignReq req) {
        R<Page<GoodsVO>> r = goodsFeignService.queryGoodsInfoList(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new EmptyDataException("查询商品详情列表时失败" + r.getMsg());
        }
        return r.getData();
    }

    public IPage<GoodsVO> findCollectionsPage(GoodsFeignReq req) {
        R<Page<GoodsVO>> r = goodsFeignService.findCollectionsPage(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new EmptyDataException("查询商品收藏列表时失败" + r.getMsg());
        }
        return r.getData();
    }

    /**
     * 根据分类id查询分类
     *
     * @param id
     * @return
     */
    public String queryGoodsCate(String id) {
        R cate = goodsCategoryFeignService.getById(id, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(cate) || cate == null) {
            throw new EmptyDataException();
        }
        return BeanUtil.toBean(cate.getData(), GoodsCategory.class).getName();
    }

    public List<GoodsCategoryTree> findCategoryTree(String shopId, Boolean allShow) {
        GoodsCategoryReq req = new GoodsCategoryReq();
        req.setShopId(shopId);
        req.setAllShow(allShow);
        R<List<GoodsCategoryTree>> r = goodsCategoryFeignService.findCategoryTree(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new EmptyDataException("查询商品分类树时失败" + r.getMsg());
        }
        return r.getData();
    }

    public boolean addCollections(GoodsCollectionsReq query) {
        R<Boolean> r = goodsCollectionsFeignService.addCollections(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("新增商品收藏时失败：" + r.getMsg());
        }
        return r.getData();
    }

    public boolean removeCollections(GoodsCollectionsReq query) {
        R<Boolean> r = goodsCollectionsFeignService.removeCollections(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("取消商品收藏时失败" + r.getMsg());
        }
        return r.getData();
    }

    /**
     * 采购商信息
     *
     * @param uid
     * @return
     */
    @Cacheable(value = ScmCacheNameConstant.WECHAT_BUYER_INFO, key = "#uid", condition = "#uid != null")
    public Buyer fetchBuyerByUid(String uid) {
        BuyerReq req = new BuyerReq();
        req.setPassportId(uid);
        R<Buyer> r = buyerFeignService.getOneByBuyerReq(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("查询采购商信息时失败" + r.getMsg());
        }
        if (null == r.getData()) {
            throw new ErrorDataException("采购商有误");
        }
        return r.getData();
    }

    /**
     * 查询采购商是否可下单
     *
     * @param req buyerId
     * @return
     */
    public Boolean queryBuyerStatus(QueryBuyerStatusReq req) {
        R<Boolean> r = buyerFeignService.buyerStatus(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            return false;
        }
        return r.getData();
    }


    /**
     * 查询采购商是否需下单支付
     *
     * @param req buyerId
     * @return
     */
    public BuyerPayVO queryBuyerPayInfo(QueryBuyerStatusReq req) {
        R<BuyerPayVO> r = buyerFeignService.buyerPayInfo(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            // 默认需下单支付
            return new BuyerPayVO(true, BigDecimal.ZERO);
        }
        return r.getData();
    }

    // 账单

    public IPage<BillInfoDTO> receivableBillFindPage(BillInfoFeignReq query) {
        R<Page<BillInfoDTO>> r = receivableBillFeignService.findPage(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("分页查询应收账单时失败" + r.getMsg());
        }
        return r.getData();
    }

    public BillInfoVO receivableBillInfo(String billSn) {
        BillInfoFeignReq req = new BillInfoFeignReq();
        req.setBillSn(billSn);
        R<BillInfoVO> r = receivableBillFeignService.fetchOneReceivable(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("查询应收账单详情时失败" + r.getMsg());
        }
        return r.getData();
    }

    public BillInfoDTO createReceivableBill(BillInfoDTO query) {
        R<BillInfoDTO> r = receivableBillFeignService.createBill(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("创建应收账单时失败" + r.getMsg());
        }
        return r.getData();
    }


    public IPage<BuyOrderVO> findBuyOrderPage(WxBuyOrderReq req) {
        BuyOrderFeignReq query = new BuyOrderFeignReq();
        BeanUtil.copyProperties(req, query);
        R<Page<BuyOrderVO>> r = buyOrderFeignService.findPage(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询采购订单列表时失败" + r.getMsg());
        }
        return r.getData();
    }

    /**
     * 采购单详情
     *
     * @param buyOrderSn
     * @return
     */
    public BuyOrderVO fetchBuyOrderInfo(String buyOrderSn) {
        R<BuyOrderVO> r = buyOrderFeignService.fetchInfo(buyOrderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r)) {
            throw new ErrorDataException("查询采购单详情时失败" + r.getMsg());
        }
        if (null == r.getData()) {
            throw new ErrorDataException("找不到此采购单");
        }
        return r.getData();
    }

    public BuyOrderVO addBuyOrder(BuyOrderEditReq query) {
        R<BuyOrderVO> r = buyOrderFeignService.addBuyOrder(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("采购单下单时失败:" + r.getMsg());
        }
        return r.getData();
    }

    public Boolean modifyBuyOrder(BuyOrderEditReq query) {
        R<Boolean> r = buyOrderFeignService.modifyBuyOrder(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("采购单修改时失败:" + r.getMsg());
        }
        return r.getData();
    }

    public boolean cancelBuyOrder(String buyOrderSn) {
        R<Boolean> r = buyOrderFeignService.cancelBuyOrder(buyOrderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("采购单取消时失败:" + r.getMsg());
        }
        return r.getData();
    }

    /**
     * 采购商常购买的商品(销量最高的30个)
     *
     * @param buyerId
     * @return
     */
    @Cacheable(value = ScmCacheNameConstant.WECHAT_BUYER_OFTEN_GOODS, key = "#buyerId")
    public List<String> queryBuyOrderOftenModelIds(String buyerId) {
        BuyOrderFeignReq query = new BuyOrderFeignReq();
        query.setBuyerId(buyerId);
        R<List<String>> r = buyOrderFeignService.queryOftenModelIds(query, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询采购商常购买的商品列表时失败:" + r.getMsg());
        }
        return r.getData();
    }

}
