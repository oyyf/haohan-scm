package com.haohan.cloud.scm.wecaht.wxapp.api.supply;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.feign.SupplierGoodsFeignService;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.api.supply.resp.SupplyGoodsDetailResp;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/supplyManage")
@Api(value = "SupplyManageApiCtrl", tags = "供应管理口服务")
public class SupplyManageApiCtrl {

    private final SupplierGoodsFeignService supplierGoodsFeignService;
    private final OfferOrderFeignService offerOrderFeignService;
    private final ScmWechatUtils scmWechatUtils;

    @GetMapping("/querySupplyGoodsRepertoryList")
    @ApiOperation(value = "查询供应商品库存信息列表")
    public R querySupplyGoodsRepertoryList(@Validated QueryGoodsRepertoryListReq req){
        return supplierGoodsFeignService.querySupplyGoodsRepertoryList(req, SecurityConstants.FROM_IN);
    }
    /**
     * 查询供应商品库存详情
     * @param req   必须：id/pmId
     * @return
     */
    @GetMapping("/querySupplyGoodsRepertoryDetail")
    @ApiOperation(value = "查询供应商品库存详情")
    public R querySupplyGoodsRepertoryDetail(@Validated QueryGoodsRepertoryDetailReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return supplierGoodsFeignService.querySupplyGoodsRepertoryDetail(req,SecurityConstants.FROM_IN);
    }
    /**
     * 查询货源信息列表
     * @param req 必须：pmId/uId
     * @return
     */
    @GetMapping("/querySupplyGoodsList")
    @ApiOperation(value = "查询货源信息列表小程序")
    public R querySupplyGoodsList(@Validated SupplyGoodsListReq req){
        Supplier supplier = scmWechatUtils.fetchByUid(req.getPmId(), req.getUid());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        BeanUtil.copyProperties(req,offerOrderReq);
        offerOrderReq.setOfferType(PdsOfferTypeEnum.supplierOffer);
        offerOrderReq.setSupplierId(supplier.getId());
        return offerOrderFeignService.getOfferOrderPage(offerOrderReq,SecurityConstants.FROM_IN);
    }
    /**
     * 查询货源信息详情
     * @param req 必须：pmId/uId
     * @return
     */
    @GetMapping("/querySupplyGoodsDetail")
    @ApiOperation(value = "查询货源信息详情小程序")
    public R querySupplyGoodsDetail(@Validated SupplyGoodsDetailReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        BeanUtil.copyProperties(req,offerOrderReq);
        SupplyGoodsDetailResp res = scmWechatUtils.supplyGoodsDetail(offerOrderReq);
        res.setCateName(scmWechatUtils.queryGoodsCate(res.getGoodsId()));
        Supplier supplier = scmWechatUtils.getSupplierById(res.getSupplierId());
        res.setAddress(supplier.getAddress());
        res.setPhone(supplier.getTelephone());
        res.setLevel(supplier.getSupplierLevel());
        return R.ok(res);
    }
    /**
     * 供应商报价  (新增报价单 货源报价)
     * @param req  必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @PostMapping("/addSupplierOffer")
    public R addSupplierOffer(@Validated OfferOrderReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return offerOrderFeignService.addSupplierOffer(req,SecurityConstants.FROM_IN);
    }

}
