package com.haohan.cloud.scm.wecaht.wxapp.api;

import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.haohan.cloud.scm.api.wechat.req.WxShopReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatMerchantCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2020/4/20
 * 商家店铺相关
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/merchant")
@Api(value = "MerchantApiCtrl", tags = "商家接口服务")
public class WechatMerchantApiCtrl {

    private final WechatMerchantCoreService wechatMerchantCoreService;

    @GetMapping("/shop/list")
    @ApiOperation(value = "平台店铺列表")
    public R<MerchantShopVO> platformShopList(@Validated WxShopReq req) {
        return RUtil.success(wechatMerchantCoreService.platformShopList(req));
    }

}
