package com.haohan.cloud.scm.wecaht.wxapp.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.wechat.req.WxAdvanceBillReq;
import com.haohan.cloud.scm.api.wechat.req.WxBillQueryReq;

/**
 * @author dy
 * @date 2020/5/19
 */
public interface WechatBillCoreService {

    IPage<BillInfoDTO> findReceivablePage(WxBillQueryReq req);

    BillInfoVO fetchInfo(WxBillQueryReq req);

    /**
     * 根据订单创建预付应收账单
     * @param req billType
     * @return
     */
    BillInfoDTO createReceivableBill(WxAdvanceBillReq req);
}
