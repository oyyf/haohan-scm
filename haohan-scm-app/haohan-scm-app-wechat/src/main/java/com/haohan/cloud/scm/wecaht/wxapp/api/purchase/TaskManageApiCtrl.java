package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import com.haohan.cloud.scm.api.purchase.feign.PurchaseEmployeeFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderDetailFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseTaskFeignService;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/taskManage")
@Api(value = "TaskManageApiCtrl", tags = "采购任务管理接口服务")
public class TaskManageApiCtrl {
    private final PurchaseOrderFeignService purchaseOrderFeignService;
    private final PurchaseTaskFeignService purchaseTaskFeignService;
    private final PurchaseEmployeeFeignService purchaseEmployeeFeignService;
    private final ScmWechatUtils scmWechatUtils;
    private final PurchaseOrderDetailFeignService purchaseOrderDetailFeignService;

    @GetMapping("/queryTaskListByDirector")
    @ApiOperation(value = "总监 查询采购任务列表(带采购商品信息)")
    public R queryTaskListByDirector(@Validated PurchasePageReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseOrderFeignService.queryTaskListByDirector(req, SecurityConstants.FROM_IN);
    }

    @GetMapping("/queryPurchaseTask")
    @ApiOperation(value = "查询采购任务详情(带采购商品信息)")
    public R queryPurchaseTask(@Validated QueryPurchaseTaskReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseOrderFeignService.queryPurchaseTask(req, SecurityConstants.FROM_IN);
    }

    @PostMapping("/appointTask")
    @ApiOperation(value = "经理 执行采购任务(分配) 指定采购员")
    public R appointTask(@RequestBody @Validated AppointTaskReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseOrderFeignService.appointTask(req,SecurityConstants.FROM_IN);
    }

    @PostMapping("/auditTaskBatch")
    @ApiOperation(value = "批量通过采购任务", notes = "总监 执行采购任务(审核) 通过—小程序")
    public R auditTaskBatch(@Validated AuditTaskBatchReq req) {
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseOrderFeignService.auditTaskBatch(req,SecurityConstants.FROM_IN);
    }

    @PostMapping("/queryPurchaseTaskList")
    @ApiOperation(value = "采购员工(经理/采购员) 查询采购任务列表(带采购商品信息)")
    public R queryPurchaseTaskList(@Validated PurchasePageReq req){
        return purchaseOrderFeignService.queryPurchaseTaskList(req,SecurityConstants.FROM_IN);
    }
    /**
     * 总监 执行采购任务(审核) 修改采购单明细
     * @param req
     * @return
     */
    @PostMapping("/auditTask")
    @ApiOperation(value = "执行采购任务(审核)——小程序", notes = "总监 执行采购任务(审核) 修改采购单明细")
    public R auditTask(@Validated AuditTaskReq req) {
        scmWechatUtils.queryUPassportById(req.getUId());
        return purchaseTaskFeignService.auditTask(req,SecurityConstants.FROM_IN);
    }

    /**
     * 查询采购员工列表
     *
     * @param req
     * @return
     */
    @PostMapping("/queryEmployeeList")
    @ApiOperation(value = "查询采购员工列表")
    public R queryEmployeeList(@RequestBody @Validated QueryEmployeeListReq req) {
        PurchaseEmployeeReq purchaseEmployeeReq = new PurchaseEmployeeReq();
        purchaseEmployeeReq.setPurchaseEmployeeType(req.getPurchaseEmployeeType());
        purchaseEmployeeReq.setPmId(req.getPmId());
        return purchaseEmployeeFeignService.getPurchaseEmployeeList(purchaseEmployeeReq,SecurityConstants.FROM_IN);
    }

    @GetMapping("/queryPurchaseDetail")
    @PostMapping(value = "查询采购单详情")
    public R queryPurchaseDetail(@Validated QueryPurchaseTaskReq req){
        scmWechatUtils.queryUPassportById(req.getUid());
        return purchaseOrderDetailFeignService.getById(req.getId(),SecurityConstants.FROM_IN);
    }
}
