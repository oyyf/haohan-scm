package com.haohan.cloud.scm.wecaht.wxapp.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsCollectionsReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatGoodsCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author dy
 * @date 2020/5/23
 * 商品查询
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/goods")
@Api(value = "GoodsApiCtrl", tags = "商品查询")
public class WechatGoodsApiCtrl {

    private final WechatGoodsCoreService wechatGoodsCoreService;


    @GetMapping("/page")
    @ApiOperation(value = "商品(详情)分页查询")
    public R<IPage<GoodsVO>> findPage(@Validated WxGoodsQueryReq req) {
        return RUtil.success(wechatGoodsCoreService.findPage(req));
    }


    @GetMapping("/info")
    @ApiOperation(value = "商品详情")
    public R<GoodsVO> fetchInfo(@Validated(SingleGroup.class) WxGoodsQueryReq req) {
        return RUtil.success(wechatGoodsCoreService.fetchInfo(req));
    }

    @GetMapping(value = "/categoryTree")
    @ApiOperation(value = "查询分类树", notes = "查询分类树, 默认不查询全部")
    public R<List<GoodsCategoryTree>> queryCategoryTree(@Validated WxGoodsQueryReq req) {
        // 默认不查询全部
        if (null == req.getAllShow()) {
            req.setAllShow(false);
        }
        return RUtil.success(wechatGoodsCoreService.findCategoryTree(req));
    }

    /**
     * 查询收藏的商品(详情)列表
     *
     * @param req
     * @return
     */
    @GetMapping("/collectionsPage")
    @ApiOperation(value = "商品收藏列表(详情)")
    public R<IPage<GoodsVO>> findCollectionsPage(@Validated WxGoodsQueryReq req) {
        return RUtil.success(wechatGoodsCoreService.findCollectionsPage(req));
    }

    /**
     * 新增收藏商品
     *
     * @param req
     * @return
     */
    @SysLog("新增收藏商品")
    @PostMapping("/collections")
    public R<Boolean> addCollections(@Validated WxGoodsCollectionsReq req) {
        return RUtil.success(wechatGoodsCoreService.addCollections(req));
    }

    /**
     * 取消收藏
     *
     * @param req
     * @return
     */
    @SysLog("取消收藏商品")
    @DeleteMapping("/collections")
    public R<Boolean> removeCollections(@Validated WxGoodsCollectionsReq req) {
        return RUtil.success(wechatGoodsCoreService.removeCollections(req));
    }

}
