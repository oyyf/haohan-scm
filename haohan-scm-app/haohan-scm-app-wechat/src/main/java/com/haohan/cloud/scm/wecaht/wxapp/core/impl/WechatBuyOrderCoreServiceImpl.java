package com.haohan.cloud.scm.wecaht.wxapp.core.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderEditReq;
import com.haohan.cloud.scm.api.wechat.req.WxBuyOrderReq;
import com.haohan.cloud.scm.api.wechat.req.WxGoodsQueryReq;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBuyOrderCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dy
 * @date 2020/5/30
 */
@Service
@AllArgsConstructor
public class WechatBuyOrderCoreServiceImpl implements WechatBuyOrderCoreService {

    private final ScmWechatUtils scmWechatUtils;

    @Override
    public IPage<BuyOrderVO> findPage(WxBuyOrderReq req) {
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        // 采购商类型处理
        switch (buyer.getBuyerType()) {
            case boss:
                req.setMerchantId(buyer.getMerchantId());
                // 查看商家的
                break;
            case operator:
                // 查看所有
                break;
            case self:
            case employee:
            default:
                // 查看采购商的
                req.setBuyerId(buyer.getId());
        }
        // 带明细列表
        return scmWechatUtils.findBuyOrderPage(req);
    }

    @Override
    public BuyOrderVO fetchInfo(WxBuyOrderReq req) {
        scmWechatUtils.fetchBuyerByUid(req.getUid());
        return scmWechatUtils.fetchBuyOrderInfo(req.getBuyOrderSn());
    }

    @Override
    public BuyOrderVO addBuyOrder(WxBuyOrderEditReq req) {
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        // 小程序下单不可修改价格
        req.getDetailList().forEach(detail-> detail.setBuyPrice(null));
        return scmWechatUtils.addBuyOrder(req.transTo(buyer.getId()));
    }

    @Override
    public boolean modifyBuyOrder(WxBuyOrderEditReq req) {
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        // 小程序下单不可修改价格
        req.getDetailList().forEach(detail-> detail.setBuyPrice(null));
        return scmWechatUtils.modifyBuyOrder(req.transTo(buyer.getId()));
    }

    @Override
    public boolean cancelBuyOrder(WxBuyOrderReq req) {
        scmWechatUtils.fetchBuyerByUid(req.getUid());
        return scmWechatUtils.cancelBuyOrder(req.getBuyOrderSn());
    }

    /**
     * 采购商常购买商品列表
     *   只查询 上架的
     * @param req uid 、shopId
     * @return
     */
    @Override
    public IPage<GoodsVO> findOftenPage(WxGoodsQueryReq req) {
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());
        // 采购商常购买的商品规格ids(销量最高的30个)
        List<String> modelIdList = scmWechatUtils.queryBuyOrderOftenModelIds(buyer.getId());
        // 带商品详情(收藏状态)列表
        GoodsFeignReq goodsReq = new GoodsFeignReq();
        goodsReq.setModelIds(modelIdList);
        goodsReq.setShopId(req.getShopId());
        goodsReq.setMarketableFlag(YesNoEnum.yes);
        goodsReq.setCurrent(req.getCurrent());
        goodsReq.setSize(req.getSize());
        goodsReq.setBuyerId(buyer.getId());
        goodsReq.setPricingDate(req.getPricingDate());
        goodsReq.setUid(req.getUid());
        return scmWechatUtils.queryGoodsInfoPage(goodsReq);
    }
}
