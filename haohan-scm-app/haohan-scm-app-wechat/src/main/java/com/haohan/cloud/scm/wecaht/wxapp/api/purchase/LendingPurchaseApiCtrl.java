package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import com.haohan.cloud.scm.api.purchase.feign.LendingRecordFeignService;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 请款采购Api
 * @author cx
 * @date 2019/7/23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/lendingPurchase")
@Api(value = "BaseApiCtrl", tags = "请款采购基础接口服务")
public class LendingPurchaseApiCtrl {
    private final LendingRecordFeignService lendingRecordFeignService;
    private final ScmWechatUtils scmWechatUtils;

    @GetMapping("/queryLendingList")
    @ApiOperation(value = "请款记录列表",notes = "请款记录列表——小程序")
    public R queryLendingList(@Validated PurchaseQueryLendingListReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return lendingRecordFeignService.queryLendingList(req, SecurityConstants.FROM_IN);
    }

    @GetMapping("/queryLendingRecord")
    @ApiOperation(value = "请款记录详情",notes = "请款记录详情——小程序")
    public R queryLendingRecord(@Validated PurchaseQueryLendingReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return lendingRecordFeignService.queryLendingRecord(req, SecurityConstants.FROM_IN);
    }

    @GetMapping("/auditLending")
    @ApiOperation(value = "请款审核",notes = "请款审核——小程序")
    public R auditLending(@Validated PurchaseAuditLendingReq req){
        scmWechatUtils.queryUPassportById(req.getUId());
        return lendingRecordFeignService.auditLending(req, SecurityConstants.FROM_IN);
    }
}
