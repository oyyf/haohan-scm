package com.haohan.cloud.scm.wecaht.wxapp.api;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.wechat.req.WxBillQueryReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBillCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2020/5/19
 * 采购商对应的订单应收账单
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/bill")
@Api(value = "BillApiCtrl", tags = "账单记录管理")
public class WechatBillApiCtrl {

    private final WechatBillCoreService wechatBillCoreService;


    @GetMapping("/receivable/page")
    @ApiOperation(value = "应收账单分页查询")
    public R<IPage<BillInfoDTO>> findReceivablePage(@Validated WxBillQueryReq req) {
        return RUtil.success(wechatBillCoreService.findReceivablePage(req));
    }


    @GetMapping("/receivable/info")
    @ApiOperation(value = "应收订单详情查询")
    public R<BillInfoVO> fetchInfo(@Validated WxBillQueryReq req) {
        if (StrUtil.isEmpty(req.getBillSn())) {
            return R.failed("账单编号不能为空");
        }
        req.setBillType(BillTypeEnum.order);
        return RUtil.success(wechatBillCoreService.fetchInfo(req));
    }


}
