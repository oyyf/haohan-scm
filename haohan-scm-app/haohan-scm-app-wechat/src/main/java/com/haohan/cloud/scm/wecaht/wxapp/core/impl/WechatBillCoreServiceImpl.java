package com.haohan.cloud.scm.wecaht.wxapp.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.req.BillInfoFeignReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.wechat.req.WxAdvanceBillReq;
import com.haohan.cloud.scm.api.wechat.req.WxBillQueryReq;
import com.haohan.cloud.scm.wecaht.wxapp.core.WechatBillCoreService;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/5/19
 */
@Service
@AllArgsConstructor
public class WechatBillCoreServiceImpl implements WechatBillCoreService {

    private final ScmWechatUtils scmWechatUtils;

    @Override
    public IPage<BillInfoDTO> findReceivablePage(WxBillQueryReq req) {
        // 只查询当前采购商对应账单
        Buyer buyer = scmWechatUtils.fetchBuyerByUid(req.getUid());

        BillInfoFeignReq query = new BillInfoFeignReq();
        BeanUtil.copyProperties(req, query);
        // 订单应收
        req.setBillType(BillTypeEnum.order);
        // 采购商类型处理
        switch (buyer.getBuyerType()) {
            case boss:
                query.setMerchantId(buyer.getMerchantId());
                // 查看商家的
                break;
            case operator:
                // 查看所有
                break;
            case self:
            case employee:
            default:
                // 查看采购商的
                query.setCustomerId(buyer.getId());
        }
        return scmWechatUtils.receivableBillFindPage(query);
    }

    @Override
    public BillInfoVO fetchInfo(WxBillQueryReq req) {
        // uid 验证
        scmWechatUtils.fetchBuyerByUid(req.getUid());
        return scmWechatUtils.receivableBillInfo(req.getBillSn());
    }

    /**
     * 根据订单创建预付应收账单
     *
     * @param req billType
     * @return
     */
    @Override
    public BillInfoDTO createReceivableBill(WxAdvanceBillReq req) {
        // uid 验证
        scmWechatUtils.fetchBuyerByUid(req.getUid());
        return scmWechatUtils.createReceivableBill(req.transTo());
    }


}
