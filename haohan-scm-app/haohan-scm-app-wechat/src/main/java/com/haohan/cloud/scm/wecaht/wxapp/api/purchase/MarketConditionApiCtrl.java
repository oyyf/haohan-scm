package com.haohan.cloud.scm.wecaht.wxapp.api.purchase;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.feign.MarketRecordFeignService;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.MarketRecordResp;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.wecaht.wxapp.utils.ScmWechatUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: haohan-fresh-scm
 * @description: 商品采购API
 * @author: Simon
 * @create: 2019-07-18
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/api/wechat/wxapp/marketCondition")
@Api(value = "BaseApiCtrl", tags = "微信小程序基础接口服务")
public class MarketConditionApiCtrl {
    private final MarketRecordFeignService marketRecordFeignService;
    private final ScmWechatUtils scmWechatUtils;

    /**
     * 查询市场行情列表
     * @param req
     * @return
     */
    @GetMapping("/queryMarketPriceList")
    @ApiOperation(value = "市场行情列表" , notes = "按分类分页查询市场行情列表—小程序")
    public R queryMarketPriceList(@Validated PurchaseQueryMarketPriceListReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw  new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        MarketRecordReq record = new MarketRecordReq();
        record.setPmId(req.getPmId());
        record.setGoodsCategoryId(req.getGoodsCategoryId());
        record.setPageNo(req.getCurrent());
        record.setPageSize(req.getSize());
        return marketRecordFeignService.getMarketRecordPage(record, SecurityConstants.FROM_IN);
    }
    /**
     * 查询市场行情详情
     * @param req
     * @return
     */
    @GetMapping("/queryMarketPrice")
    @ApiOperation(value = "市场行情详情" , notes = "查询市场行情详情—小程序")
    public R queryMarketPrice(@Validated PurchaseQueryMarketPriceReq req) {
        if (StrUtil.isEmpty(req.getUId())){
            throw  new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUId());
        MarketRecordReq record = new MarketRecordReq();
        record.setPmId(req.getPmId());
        record.setId(req.getId());
        MarketRecordResp res = scmWechatUtils.queryMarketPrice(record);
        res.setCateName(scmWechatUtils.queryGoodsCate(res.getGoodsCategoryId()));
        Supplier supplier = scmWechatUtils.getSupplierById(res.getSupplierId());
        res.setAddress(supplier.getAddress());
        res.setPhone(supplier.getTelephone());
        return R.ok(res);
    }
    /**
     * 修改市场行情
     *
     * @param req
     * @return
     */
    @PostMapping("/modifyMarketPrice")
    @ApiOperation(value = "修改市场行情详情" , notes = "修改市场行情详情—小程序")
    public R modifyMarketPrice(@Validated PurchaseModifyMarketPriceReq req) {
        if (StrUtil.isEmpty(req.getUid())){
            throw  new EmptyDataException("通行证有错");
        }
        scmWechatUtils.queryUPassportById(req.getUid());
        return marketRecordFeignService.modifyMarketPrice(req,SecurityConstants.FROM_IN);
    }
    /**
     * 增加市场行情
     * @param req
     * @return
     */
    @PostMapping("/addMarketPrice")
    @ApiOperation(value = "增加市场行情" , notes = "增加市场行情详情—小程序")
    public R addMarketPrice(@Validated PurchaseAddMarketPriceReq req) {
        MarketRecord record = new MarketRecord();
        BeanUtil.copyProperties(req,record);
        return marketRecordFeignService.save(record,SecurityConstants.FROM_IN);

    }


}
