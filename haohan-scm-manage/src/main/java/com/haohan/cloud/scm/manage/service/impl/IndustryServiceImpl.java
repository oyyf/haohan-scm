/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.Industry;
import com.haohan.cloud.scm.manage.mapper.IndustryMapper;
import com.haohan.cloud.scm.manage.service.IndustryService;
import org.springframework.stereotype.Service;

/**
 * 行业类型
 *
 * @author haohan
 * @date 2019-05-13 17:17:37
 */
@Service
public class IndustryServiceImpl extends ServiceImpl<IndustryMapper, Industry> implements IndustryService {

}
