/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplateExtInfo;
import com.haohan.cloud.scm.api.manage.req.ShopTemplateExtInfoReq;
import com.haohan.cloud.scm.manage.service.ShopTemplateExtInfoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 店铺模板扩展信息
 *
 * @author haohan
 * @date 2019-05-29 14:28:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shoptemplateextinfo" )
@Api(value = "shoptemplateextinfo", tags = "shoptemplateextinfo管理")
public class ShopTemplateExtInfoController {

    private final ShopTemplateExtInfoService shopTemplateExtInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shopTemplateExtInfo 店铺模板扩展信息
     * @return
     */
    @GetMapping("/page" )
    public R getShopTemplateExtInfoPage(Page page, ShopTemplateExtInfo shopTemplateExtInfo) {
        return new R<>(shopTemplateExtInfoService.page(page, Wrappers.query(shopTemplateExtInfo)));
    }


    /**
     * 通过id查询店铺模板扩展信息
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shopTemplateExtInfoService.getById(id));
    }

    /**
     * 新增店铺模板扩展信息
     * @param shopTemplateExtInfo 店铺模板扩展信息
     * @return R
     */
    @SysLog("新增店铺模板扩展信息" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shoptemplateextinfo_add')" )
    public R save(@RequestBody ShopTemplateExtInfo shopTemplateExtInfo) {
        return new R<>(shopTemplateExtInfoService.save(shopTemplateExtInfo));
    }

    /**
     * 修改店铺模板扩展信息
     * @param shopTemplateExtInfo 店铺模板扩展信息
     * @return R
     */
    @SysLog("修改店铺模板扩展信息" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shoptemplateextinfo_edit')" )
    public R updateById(@RequestBody ShopTemplateExtInfo shopTemplateExtInfo) {
        return new R<>(shopTemplateExtInfoService.updateById(shopTemplateExtInfo));
    }

    /**
     * 通过id删除店铺模板扩展信息
     * @param id id
     * @return R
     */
    @SysLog("删除店铺模板扩展信息" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shoptemplateextinfo_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(shopTemplateExtInfoService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除店铺模板扩展信息")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shoptemplateextinfo_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateExtInfoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询店铺模板扩展信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateExtInfoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopTemplateExtInfoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询店铺模板扩展信息总记录}")
    @PostMapping("/countByShopTemplateExtInfoReq")
    public R countByShopTemplateExtInfoReq(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {

        return new R<>(shopTemplateExtInfoService.count(Wrappers.query(shopTemplateExtInfoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopTemplateExtInfoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shopTemplateExtInfoReq查询一条货位信息表")
    @PostMapping("/getOneByShopTemplateExtInfoReq")
    public R getOneByShopTemplateExtInfoReq(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {

        return new R<>(shopTemplateExtInfoService.getOne(Wrappers.query(shopTemplateExtInfoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopTemplateExtInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shoptemplateextinfo_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ShopTemplateExtInfo> shopTemplateExtInfoList) {

        return new R<>(shopTemplateExtInfoService.saveOrUpdateBatch(shopTemplateExtInfoList));
    }


}
