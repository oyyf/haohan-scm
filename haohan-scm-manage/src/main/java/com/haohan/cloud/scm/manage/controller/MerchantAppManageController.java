/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppManage;
import com.haohan.cloud.scm.api.manage.req.MerchantAppManageReq;
import com.haohan.cloud.scm.manage.service.MerchantAppManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商家应用管理
 *
 * @author haohan
 * @date 2019-05-29 14:26:16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/merchantappmanage" )
@Api(value = "merchantappmanage", tags = "merchantappmanage管理")
public class MerchantAppManageController {

    private final MerchantAppManageService merchantAppManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param merchantAppManage 商家应用管理
     * @return
     */
    @GetMapping("/page" )
    public R getMerchantAppManagePage(Page page, MerchantAppManage merchantAppManage) {
        return new R<>(merchantAppManageService.page(page, Wrappers.query(merchantAppManage)));
    }


    /**
     * 通过id查询商家应用管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(merchantAppManageService.getById(id));
    }

    /**
     * 新增商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @SysLog("新增商家应用管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantappmanage_add')" )
    public R save(@RequestBody MerchantAppManage merchantAppManage) {
        return new R<>(merchantAppManageService.save(merchantAppManage));
    }

    /**
     * 修改商家应用管理
     * @param merchantAppManage 商家应用管理
     * @return R
     */
    @SysLog("修改商家应用管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantappmanage_edit')" )
    public R updateById(@RequestBody MerchantAppManage merchantAppManage) {
        return new R<>(merchantAppManageService.updateById(merchantAppManage));
    }

    /**
     * 通过id删除商家应用管理
     * @param id id
     * @return R
     */
    @SysLog("删除商家应用管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_merchantappmanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(merchantAppManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商家应用管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_merchantappmanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商家应用管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商家应用管理总记录}")
    @PostMapping("/countByMerchantAppManageReq")
    public R countByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq) {

        return new R<>(merchantAppManageService.count(Wrappers.query(merchantAppManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantAppManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据merchantAppManageReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantAppManageReq")
    public R getOneByMerchantAppManageReq(@RequestBody MerchantAppManageReq merchantAppManageReq) {

        return new R<>(merchantAppManageService.getOne(Wrappers.query(merchantAppManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantAppManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_merchantappmanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<MerchantAppManage> merchantAppManageList) {

        return new R<>(merchantAppManageService.saveOrUpdateBatch(merchantAppManageList));
    }


}
