/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.manage.mapper.MerchantMapper;
import com.haohan.cloud.scm.manage.service.MerchantService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-13 17:37:44
 */
@Service
public class MerchantServiceImpl extends ServiceImpl<MerchantMapper, Merchant> implements MerchantService {

    @Override
    @CacheEvict(value = ScmCacheNameConstant.MANAGE_MERCHANT_INFO, key = "#entity.id")
    public boolean updateById(Merchant entity) {
        return retBool(baseMapper.updateById(entity));
    }

    /**
     * 平台商家 （一个租户 唯一）
     *
     * @return
     */
    @Override
    @Cacheable(value = ScmCacheNameConstant.MANAGE_PLATFORM_MERCHANT, key = "0")
    public Merchant fetchPlatformMerchant() {
        return baseMapper.selectList(Wrappers.<Merchant>query().lambda()
                .eq(Merchant::getStatus, MerchantStatusEnum.enabled)
                .eq(Merchant::getPdsType, MerchantPdsTypeEnum.platform)
                .orderByAsc(Merchant::getCreateDate)
        ).stream().findFirst().orElse(null);
    }

    @Override
    @CacheEvict(value = ScmCacheNameConstant.MANAGE_PLATFORM_MERCHANT, allEntries = true)
    public void flushPlatformMerchant() {
        // 刷新平台商家缓存
    }

}
