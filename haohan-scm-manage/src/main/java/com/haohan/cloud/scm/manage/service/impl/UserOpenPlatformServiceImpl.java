/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.UserOpenPlatform;
import com.haohan.cloud.scm.manage.mapper.UserOpenPlatformMapper;
import com.haohan.cloud.scm.manage.service.UserOpenPlatformService;
import org.springframework.stereotype.Service;

/**
 * 第三方开放平台用户管理
 *
 * @author haohan
 * @date 2019-05-13 17:28:09
 */
@Service
public class UserOpenPlatformServiceImpl extends ServiceImpl<UserOpenPlatformMapper, UserOpenPlatform> implements UserOpenPlatformService {

}
