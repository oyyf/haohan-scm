/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.MerchantEmployee;
import com.haohan.cloud.scm.manage.mapper.MerchantEmployeeMapper;
import com.haohan.cloud.scm.manage.service.MerchantEmployeeService;
import org.springframework.stereotype.Service;

/**
 * 员工管理
 *
 * @author haohan
 * @date 2019-05-13 17:37:52
 */
@Service
public class MerchantEmployeeServiceImpl extends ServiceImpl<MerchantEmployeeMapper, MerchantEmployee> implements MerchantEmployeeService {

}
