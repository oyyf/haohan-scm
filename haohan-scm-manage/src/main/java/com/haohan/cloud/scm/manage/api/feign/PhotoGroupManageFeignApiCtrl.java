/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import com.haohan.cloud.scm.api.manage.req.PhotoGroupManageReq;
import com.haohan.cloud.scm.manage.core.PhotoManageCoreService;
import com.haohan.cloud.scm.manage.service.PhotoGroupManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 图片组管理
 *
 * @author haohan
 * @date 2019-05-29 14:27:41
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PhotoGroupManage")
@Api(value = "photogroupmanage", tags = "photogroupmanage内部接口服务")
public class PhotoGroupManageFeignApiCtrl {

    private final PhotoGroupManageService photoGroupManageService;
    private final PhotoManageCoreService photoManageCoreService;


    /**
     * 通过id查询图片组管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(photoGroupManageService.getById(id));
    }


    /**
     * 分页查询 图片组管理 列表信息
     * @param photoGroupManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoGroupManagePage")
    public R getPhotoGroupManagePage(@RequestBody PhotoGroupManageReq photoGroupManageReq) {
        Page page = new Page(photoGroupManageReq.getPageNo(), photoGroupManageReq.getPageSize());
        PhotoGroupManage photoGroupManage =new PhotoGroupManage();
        BeanUtil.copyProperties(photoGroupManageReq, photoGroupManage);

        return new R<>(photoGroupManageService.page(page, Wrappers.query(photoGroupManage)));
    }


    /**
     * 全量查询 图片组管理 列表信息
     * @param photoGroupManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoGroupManageList")
    public R getPhotoGroupManageList(@RequestBody PhotoGroupManageReq photoGroupManageReq) {
        PhotoGroupManage photoGroupManage =new PhotoGroupManage();
        BeanUtil.copyProperties(photoGroupManageReq, photoGroupManage);

        return new R<>(photoGroupManageService.list(Wrappers.query(photoGroupManage)));
    }


    /**
     * 新增图片组管理
     * @param photoGroupManage 图片组管理
     * @return R
     */
    @Inner
    @SysLog("新增图片组管理")
    @PostMapping("/add")
    public R save(@RequestBody PhotoGroupManage photoGroupManage) {
        return new R<>(photoGroupManageService.save(photoGroupManage));
    }

    /**
     * 修改图片组管理
     * @param photoGroupManage 图片组管理
     * @return R
     */
    @Inner
    @SysLog("修改图片组管理")
    @PostMapping("/update")
    public R updateById(@RequestBody PhotoGroupManage photoGroupManage) {
        return new R<>(photoGroupManageService.updateById(photoGroupManage));
    }

    /**
     * 通过id删除图片组管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除图片组管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(photoGroupManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除图片组管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(photoGroupManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询图片组管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(photoGroupManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param photoGroupManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询图片组管理总记录}")
    @PostMapping("/countByPhotoGroupManageReq")
    public R countByPhotoGroupManageReq(@RequestBody PhotoGroupManageReq photoGroupManageReq) {

        return new R<>(photoGroupManageService.count(Wrappers.query(photoGroupManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param photoGroupManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据photoGroupManageReq查询一条货位信息表")
    @PostMapping("/getOneByPhotoGroupManageReq")
    public R getOneByPhotoGroupManageReq(@RequestBody PhotoGroupManageReq photoGroupManageReq) {

        return new R<>(photoGroupManageService.getOne(Wrappers.query(photoGroupManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param photoGroupManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PhotoGroupManage> photoGroupManageList) {

        return new R<>(photoGroupManageService.saveOrUpdateBatch(photoGroupManageList));
    }

    @Inner
    @SysLog("保存或修改图片组图片")
    @PostMapping("/savePhotoGroup")
    public R<PhotoGroupManage> savePhotoGroup(@RequestBody PhotoGroupDTO photoGroupDTO) {
        return R.ok(photoManageCoreService.editPhotoGroup(photoGroupDTO));
    }

    /**
     * 查询图片组图片
     * @return R
     */
    @Inner
    @GetMapping("/fetchByGroupNum")
    public R<PhotoGroupDTO> fetchByGroupNum(@RequestParam("groupNum") String groupNum) {
        return R.ok(photoManageCoreService.fetchByGroupNum(groupNum));
    }

}
