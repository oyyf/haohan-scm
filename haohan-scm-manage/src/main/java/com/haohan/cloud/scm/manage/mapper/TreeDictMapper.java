/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.manage.entity.TreeDict;

/**
 * 树形字典
 *
 * @author haohan
 * @date 2019-05-13 17:07:57
 */
public interface TreeDictMapper extends BaseMapper<TreeDict> {

}
