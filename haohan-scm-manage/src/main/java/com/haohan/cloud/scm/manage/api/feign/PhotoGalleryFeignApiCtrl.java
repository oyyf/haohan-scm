/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.req.PhotoGalleryReq;
import com.haohan.cloud.scm.manage.service.PhotoGalleryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 资源图片库
 *
 * @author haohan
 * @date 2019-05-29 14:27:45
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PhotoGallery")
@Api(value = "photogallery", tags = "photogallery内部接口服务")
public class PhotoGalleryFeignApiCtrl {

    private final PhotoGalleryService photoGalleryService;


    /**
     * 通过id查询资源图片库
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(photoGalleryService.getById(id));
    }


    /**
     * 分页查询 资源图片库 列表信息
     * @param photoGalleryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoGalleryPage")
    public R getPhotoGalleryPage(@RequestBody PhotoGalleryReq photoGalleryReq) {
        Page page = new Page(photoGalleryReq.getPageNo(), photoGalleryReq.getPageSize());
        PhotoGallery photoGallery =new PhotoGallery();
        BeanUtil.copyProperties(photoGalleryReq, photoGallery);

        return new R<>(photoGalleryService.page(page, Wrappers.query(photoGallery)));
    }


    /**
     * 全量查询 资源图片库 列表信息
     * @param photoGalleryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoGalleryList")
    public R getPhotoGalleryList(@RequestBody PhotoGalleryReq photoGalleryReq) {
        PhotoGallery photoGallery =new PhotoGallery();
        BeanUtil.copyProperties(photoGalleryReq, photoGallery);

        return new R<>(photoGalleryService.list(Wrappers.query(photoGallery)));
    }


    /**
     * 新增资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @Inner
    @SysLog("新增资源图片库")
    @PostMapping("/add")
    public R save(@RequestBody PhotoGallery photoGallery) {
        return new R<>(photoGalleryService.save(photoGallery));
    }

    /**
     * 修改资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @Inner
    @SysLog("修改资源图片库")
    @PostMapping("/update")
    public R updateById(@RequestBody PhotoGallery photoGallery) {
        return new R<>(photoGalleryService.updateById(photoGallery));
    }

    /**
     * 通过id删除资源图片库
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除资源图片库")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(photoGalleryService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除资源图片库")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(photoGalleryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询资源图片库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(photoGalleryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询资源图片库总记录}")
    @PostMapping("/countByPhotoGalleryReq")
    public R countByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq) {

        return new R<>(photoGalleryService.count(Wrappers.query(photoGalleryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据photoGalleryReq查询一条货位信息表")
    @PostMapping("/getOneByPhotoGalleryReq")
    public R getOneByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq) {

        return new R<>(photoGalleryService.getOne(Wrappers.query(photoGalleryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param photoGalleryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PhotoGallery> photoGalleryList) {

        return new R<>(photoGalleryService.saveOrUpdateBatch(photoGalleryList));
    }

}
