/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.req.ShopReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.core.ShopManageCoreService;
import com.haohan.cloud.scm.manage.service.ShopService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 店铺
 *
 * @author haohan
 * @date 2019-05-29 14:28:19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Shop")
@Api(value = "shop", tags = "shop内部接口服务")
public class ShopFeignApiCtrl {

    private final ShopService shopService;
    private final ShopManageCoreService shopManageCoreService;


    /**
     * 通过id查询店铺
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R<Shop> getById(@PathVariable("id") String id) {
        return RUtil.success(shopService.getById(id));
    }


    /**
     * 分页查询 店铺 列表信息
     * @param shopReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopPage")
    public R getShopPage(@RequestBody ShopReq shopReq) {
        Page page = new Page(shopReq.getPageNo(), shopReq.getPageSize());
        Shop shop =new Shop();
        BeanUtil.copyProperties(shopReq, shop);

        return new R<>(shopService.page(page, Wrappers.query(shop)));
    }


    /**
     * 全量查询 店铺 列表信息
     * @param shopReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopList")
    public R getShopList(@RequestBody ShopReq shopReq) {
        Shop shop =new Shop();
        BeanUtil.copyProperties(shopReq, shop);

        return new R<>(shopService.list(Wrappers.query(shop)));
    }


    /**
     * 新增店铺
     * @param shop 店铺
     * @return R
     */
    @Inner
    @SysLog("新增店铺")
    @PostMapping("/add")
    public R save(@RequestBody Shop shop) {
        return new R<>(shopService.save(shop));
    }

    /**
     * 修改店铺
     * @param shop 店铺
     * @return R
     */
    @Inner
    @SysLog("修改店铺")
    @PostMapping("/update")
    public R updateById(@RequestBody Shop shop) {
        return new R<>(shopService.updateById(shop));
    }

    /**
     * 通过id删除店铺
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除店铺")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shopService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除店铺")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询店铺")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询店铺总记录}")
    @PostMapping("/countByShopReq")
    public R countByShopReq(@RequestBody ShopReq shopReq) {

        return new R<>(shopService.count(Wrappers.query(shopReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shopReq查询一条货位信息表")
    @PostMapping("/getOneByShopReq")
    public R getOneByShopReq(@RequestBody ShopReq shopReq) {

        return new R<>(shopService.getOne(Wrappers.query(shopReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Shop> shopList) {

        return new R<>(shopService.saveOrUpdateBatch(shopList));
    }

    /**
     * 获取平台商家的采购配送店铺
     *
     * @param pmId 可null
     * @return R
     */
    @Inner
    @PostMapping("/fetchPurchaseShop")
    public R<ShopExtDTO> fetchPurchaseShop(@RequestParam(value = "pmId", required = false) String pmId) {
        return R.ok(shopManageCoreService.fetchPurchaseShop(pmId));
    }

    /**
     * 查询租户默认店铺
     *
     * @return
     */
    @Inner
    @GetMapping("/fetchDefaultShop")
    public R<ShopExtDTO> fetchDefaultShop(){
        return R.ok(shopManageCoreService.fetchDefaultShop());
    }

    /**
     * 获取商家店铺
     *
     * @param merchantId
     * @return R
     */
    @Inner
    @PostMapping("/fetchMerchantShop")
    public R<MerchantShopVO> fetchMerchantShop(@RequestParam(value = "merchantId") String merchantId) {
        return R.ok(shopManageCoreService.fetchMerchantShop(merchantId));
    }
}
