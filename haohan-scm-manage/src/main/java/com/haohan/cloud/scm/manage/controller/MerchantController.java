/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.req.MerchantReq;
import com.haohan.cloud.scm.manage.service.MerchantService;
import com.haohan.cloud.scm.manage.utils.ScmManageUtils;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-29 14:27:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/merchant" )
@Api(value = "merchant", tags = "merchant管理")
public class MerchantController {

    private final MerchantService merchantService;

    private final ScmManageUtils scmManageUtils;

    /**
     * 分页查询
     * @param page 分页对象
     * @param merchant 商家信息
     * @return
     */
    @GetMapping("/page" )
    public R getMerchantPage(Page page, Merchant merchant) {
        return new R<>(merchantService.page(page, Wrappers.query(merchant)));
    }


    /**
     * 通过id查询商家信息
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(merchantService.getById(id));
    }

    /**
     * 新增商家信息
     * @param merchant 商家信息
     * @return R
     */
    @SysLog("新增商家信息" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_merchant_add')" )
    public R save(@RequestBody Merchant merchant) {
        return R.failed("不支持");
    }

    /**
     * 修改商家信息
     * @param merchant 商家信息
     * @return R
     */
    @SysLog("修改商家信息" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_merchant_edit')" )
    public R updateById(@RequestBody Merchant merchant) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除商家信息
     * @param id id
     * @return R
     */
    @SysLog("删除商家信息" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_merchant_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商家信息")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_merchant_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商家信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商家信息总记录}")
    @PostMapping("/countByMerchantReq")
    public R countByMerchantReq(@RequestBody MerchantReq merchantReq) {

        return new R<>(merchantService.count(Wrappers.query(merchantReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据merchantReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantReq")
    public R getOneByMerchantReq(@RequestBody MerchantReq merchantReq) {

        return new R<>(merchantService.getOne(Wrappers.query(merchantReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_merchant_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Merchant> merchantList) {
        return R.failed("不支持");
    }

}
