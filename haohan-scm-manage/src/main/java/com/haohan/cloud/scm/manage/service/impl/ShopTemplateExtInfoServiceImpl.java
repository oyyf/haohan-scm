/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplateExtInfo;
import com.haohan.cloud.scm.manage.mapper.ShopTemplateExtInfoMapper;
import com.haohan.cloud.scm.manage.service.ShopTemplateExtInfoService;
import org.springframework.stereotype.Service;

/**
 * 店铺模板扩展信息
 *
 * @author haohan
 * @date 2019-05-13 17:45:20
 */
@Service
public class ShopTemplateExtInfoServiceImpl extends ServiceImpl<ShopTemplateExtInfoMapper, ShopTemplateExtInfo> implements ShopTemplateExtInfoService {

}
