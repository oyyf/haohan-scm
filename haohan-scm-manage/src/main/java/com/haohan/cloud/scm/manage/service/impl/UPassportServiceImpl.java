/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.manage.mapper.UPassportMapper;
import com.haohan.cloud.scm.manage.service.UPassportService;
import org.springframework.stereotype.Service;

/**
 * 用户通行证
 *
 * @author haohan
 * @date 2019-05-13 17:27:42
 */
@Service
public class UPassportServiceImpl extends ServiceImpl<UPassportMapper, UPassport> implements UPassportService {

    @Override
    public UPassport fetchByTelephone(String telephone) {
        return baseMapper.selectList(Wrappers.<UPassport>query().lambda()
                .eq(UPassport::getTelephone, telephone)
        ).stream().findFirst().orElse(null);
    }

}
