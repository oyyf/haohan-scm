/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.ShopCategory;
import com.haohan.cloud.scm.api.manage.req.ShopCategoryReq;
import com.haohan.cloud.scm.manage.service.ShopCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 店铺分类
 *
 * @author haohan
 * @date 2019-05-29 14:28:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shopcategory" )
@Api(value = "shopcategory", tags = "shopcategory管理")
public class ShopCategoryController {

    private final ShopCategoryService shopCategoryService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shopCategory 店铺分类
     * @return
     */
    @GetMapping("/page" )
    public R getShopCategoryPage(Page page, ShopCategory shopCategory) {
        return new R<>(shopCategoryService.page(page, Wrappers.query(shopCategory)));
    }


    /**
     * 通过id查询店铺分类
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shopCategoryService.getById(id));
    }

    /**
     * 新增店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @SysLog("新增店铺分类" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shopcategory_add')" )
    public R save(@RequestBody ShopCategory shopCategory) {
        return new R<>(shopCategoryService.save(shopCategory));
    }

    /**
     * 修改店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @SysLog("修改店铺分类" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shopcategory_edit')" )
    public R updateById(@RequestBody ShopCategory shopCategory) {
        return new R<>(shopCategoryService.updateById(shopCategory));
    }

    /**
     * 通过id删除店铺分类
     * @param id id
     * @return R
     */
    @SysLog("删除店铺分类" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shopcategory_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(shopCategoryService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除店铺分类")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shopcategory_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopCategoryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询店铺分类")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopCategoryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询店铺分类总记录}")
    @PostMapping("/countByShopCategoryReq")
    public R countByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq) {

        return new R<>(shopCategoryService.count(Wrappers.query(shopCategoryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shopCategoryReq查询一条货位信息表")
    @PostMapping("/getOneByShopCategoryReq")
    public R getOneByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq) {

        return new R<>(shopCategoryService.getOne(Wrappers.query(shopCategoryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shopcategory_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ShopCategory> shopCategoryList) {

        return new R<>(shopCategoryService.saveOrUpdateBatch(shopCategoryList));
    }


}
