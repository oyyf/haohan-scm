/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.req.MerchantFeignReq;
import com.haohan.cloud.scm.api.manage.req.MerchantReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.core.MerchantCoreService;
import com.haohan.cloud.scm.manage.service.MerchantService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-29 14:27:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Merchant")
@Api(value = "merchant", tags = "merchant内部接口服务")
public class MerchantFeignApiCtrl {

    private final MerchantService merchantService;
    private final MerchantCoreService merchantCoreService;

    /**
     * 通过id查询商家信息
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R<Merchant> getById(@PathVariable("id") String id) {
        return RUtil.success(merchantService.getById(id));
    }


    /**
     * 分页查询 商家信息 列表信息
     *
     * @param merchantReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantPage")
    public R getMerchantPage(@RequestBody MerchantReq merchantReq) {
        Page page = new Page(merchantReq.getPageNo(), merchantReq.getPageSize());
        Merchant merchant = new Merchant();
        BeanUtil.copyProperties(merchantReq, merchant);

        return new R<>(merchantService.page(page, Wrappers.query(merchant)));
    }


    /**
     * 全量查询 商家信息 列表信息
     *
     * @param merchantReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantList")
    public R getMerchantList(@RequestBody MerchantReq merchantReq) {
        Merchant merchant = new Merchant();
        BeanUtil.copyProperties(merchantReq, merchant);

        return new R<>(merchantService.list(Wrappers.query(merchant)));
    }


    /**
     * 新增商家信息
     *
     * @param merchant 商家信息
     * @return R
     */
    @Inner
    @SysLog("新增商家信息")
    @PostMapping("/add")
    public R<Merchant> save(@RequestBody Merchant merchant) {
        if (merchantService.save(merchant)) {
            return R.ok(merchant);
        }
        return R.failed("新增商家失败");
    }

    /**
     * 修改商家信息
     *
     * @param merchant 商家信息
     * @return R
     */
    @Inner
    @SysLog("修改商家信息")
    @PostMapping("/update")
    public R updateById(@RequestBody Merchant merchant) {
        return new R<>(merchantService.updateById(merchant));
    }

    /**
     * 通过id删除商家信息
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商家信息")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(merchantService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除商家信息")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询商家信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商家信息总记录}")
    @PostMapping("/countByMerchantReq")
    public R countByMerchantReq(@RequestBody MerchantReq merchantReq) {

        return new R<>(merchantService.count(Wrappers.query(merchantReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param merchantReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据merchantReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantReq")
    public R<Merchant> getOneByMerchantReq(@RequestBody MerchantReq merchantReq) {
        return R.ok(merchantService.getOne(Wrappers.query(merchantReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param merchantList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Merchant> merchantList) {

        return new R<>(merchantService.saveOrUpdateBatch(merchantList));
    }

    /**
     * 查询租户的平台商家
     *
     * @return 可能为null
     */
    @Inner
    @GetMapping("/fetchPlatformMerchant")
    public R<Merchant> fetchPlatformMerchant() {
        return R.ok(merchantService.fetchPlatformMerchant());
    }

    /**
     * 创建平台的商家
     * @param merchant
     * @return
     */
    @Inner
    @PostMapping("/createWithPlatform")
    public R<Merchant> createMerchantWithPlatform(@RequestBody Merchant merchant) {
        return R.ok(merchantCoreService.createMerchantWithPlatform(merchant));
    }

    /**
     * 根据merchantId 集合和 启用状态 查询列表
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/findMerchantList")
    public R<List<MerchantVO>> findMerchantList(@RequestBody MerchantFeignReq req) {
        return R.ok(merchantCoreService.findMerchantList(req));
    }
}
