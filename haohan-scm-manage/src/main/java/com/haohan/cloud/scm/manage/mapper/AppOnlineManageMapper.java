/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.manage.entity.AppOnlineManage;

/**
 * 应用上线管理
 *
 * @author haohan
 * @date 2019-05-13 17:43:17
 */
public interface AppOnlineManageMapper extends BaseMapper<AppOnlineManage> {

}
