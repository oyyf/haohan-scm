/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.manage.entity.Merchant;

/**
 * 商家信息
 *
 * @author haohan
 * @date 2019-05-13 17:37:44
 */
public interface MerchantMapper extends BaseMapper<Merchant> {

}
