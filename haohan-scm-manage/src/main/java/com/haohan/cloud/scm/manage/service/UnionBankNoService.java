/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;

/**
 * 联行号
 *
 * @author haohan
 * @date 2019-05-13 17:16:56
 */
public interface UnionBankNoService extends IService<UnionBankNo> {

}
