package com.haohan.cloud.scm.manage.core.impl;

import cn.hutool.core.collection.CollUtil;
import com.haohan.cloud.scm.common.tools.redis.RedisIdSnUpdateUtil;
import com.haohan.cloud.scm.common.tools.redis.RedisUpdateCoreService;
import com.haohan.cloud.scm.manage.utils.ScmManageUtils;
import com.pig4cloud.pigx.admin.api.entity.SysDictItem;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/8/24
 */
@Service
@Slf4j
@AllArgsConstructor
public class ManageRedisUpdateServiceImpl implements RedisUpdateCoreService {

    private final ScmManageUtils scmManageUtils;
    private final RedisIdSnUpdateUtil redisIdSnUpdateUtil;
    private final JdbcTemplate jdbcTemplate;
    private final String databaseName = "haohan_scm";

    /**
     * 更新 缓存中 逻辑主键sn 的数值
     *
     * @return
     */
    @Override
    public String updateAllSn() {
        List<SysDictItem> list = fetchDictList();
        if (CollUtil.isEmpty(list)) {
            return "无可更新缓存值";
        }
        // 每个租户处理
        List<String> tenantIdList = scmManageUtils.fetchTenantList()
                .stream().map(item -> item.getId().toString())
                .collect(Collectors.toList());

        redisIdSnUpdateUtil.updateAllSn(list, tenantIdList, jdbcTemplate);
        return "更新缓存 逻辑sn值成功";
    }

    /**
     * 更新 缓存中 物理主键id 的数值
     *
     * @return
     */
    @Override
    public String updateAllId() {
        // 过滤 当前数据库表
        List<SysDictItem> list = fetchDictList();
        if (CollUtil.isEmpty(list)) {
            return "无可更新缓存值";
        }
        redisIdSnUpdateUtil.updateAllId(list, jdbcTemplate);
        return "更新缓存 主键id值成功";
    }

    /**
     * 过滤 当前数据库表
     *
     * @return
     */
    private List<SysDictItem> fetchDictList() {
        return scmManageUtils.fetchDictByType(RedisIdSnUpdateUtil.REDIS_SN_TYPE)
                .stream().filter(item -> databaseName.equals(item.getDescription()))
                .collect(Collectors.toList());
    }

}
