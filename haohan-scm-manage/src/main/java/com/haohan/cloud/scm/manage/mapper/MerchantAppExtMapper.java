/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;

/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-13 17:44:27
 */
public interface MerchantAppExtMapper extends BaseMapper<MerchantAppExt> {

}
