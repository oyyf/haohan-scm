/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;

/**
 * 资源图片库
 *
 * @author haohan
 * @date 2019-05-13 17:32:48
 */
public interface PhotoGalleryService extends IService<PhotoGallery> {

}
