/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.TreeDict;
import com.haohan.cloud.scm.manage.mapper.TreeDictMapper;
import com.haohan.cloud.scm.manage.service.TreeDictService;
import org.springframework.stereotype.Service;

/**
 * 树形字典
 *
 * @author haohan
 * @date 2019-05-13 17:07:57
 */
@Service
public class TreeDictServiceImpl extends ServiceImpl<TreeDictMapper, TreeDict> implements TreeDictService {

}
