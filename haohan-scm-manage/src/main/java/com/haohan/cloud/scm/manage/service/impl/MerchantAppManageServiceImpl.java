/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppManage;
import com.haohan.cloud.scm.manage.mapper.MerchantAppManageMapper;
import com.haohan.cloud.scm.manage.service.MerchantAppManageService;
import org.springframework.stereotype.Service;

/**
 * 商家应用管理
 *
 * @author haohan
 * @date 2019-05-13 17:44:03
 */
@Service
public class MerchantAppManageServiceImpl extends ServiceImpl<MerchantAppManageMapper, MerchantAppManage> implements MerchantAppManageService {

}
