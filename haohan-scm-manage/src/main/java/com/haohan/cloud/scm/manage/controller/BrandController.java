/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Brand;
import com.haohan.cloud.scm.api.manage.req.BrandReq;
import com.haohan.cloud.scm.manage.service.BrandService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 品牌管理
 *
 * @author haohan
 * @date 2019-05-29 14:25:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/brand" )
@Api(value = "brand", tags = "brand管理")
public class BrandController {

    private final BrandService brandService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param brand 品牌管理
     * @return
     */
    @GetMapping("/page" )
    public R getBrandPage(Page page, Brand brand) {
        return new R<>(brandService.page(page, Wrappers.query(brand)));
    }


    /**
     * 通过id查询品牌管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(brandService.getById(id));
    }

    /**
     * 新增品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @SysLog("新增品牌管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_brand_add')" )
    public R save(@RequestBody Brand brand) {
        return new R<>(brandService.save(brand));
    }

    /**
     * 修改品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @SysLog("修改品牌管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_brand_edit')" )
    public R updateById(@RequestBody Brand brand) {
        return new R<>(brandService.updateById(brand));
    }

    /**
     * 通过id删除品牌管理
     * @param id id
     * @return R
     */
    @SysLog("删除品牌管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_brand_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(brandService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除品牌管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_brand_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(brandService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询品牌管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(brandService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询品牌管理总记录}")
    @PostMapping("/countByBrandReq")
    public R countByBrandReq(@RequestBody BrandReq brandReq) {

        return new R<>(brandService.count(Wrappers.query(brandReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据brandReq查询一条货位信息表")
    @PostMapping("/getOneByBrandReq")
    public R getOneByBrandReq(@RequestBody BrandReq brandReq) {

        return new R<>(brandService.getOne(Wrappers.query(brandReq), false));
    }


    /**
     * 批量修改OR插入
     * @param brandList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_brand_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Brand> brandList) {

        return new R<>(brandService.saveOrUpdateBatch(brandList));
    }


}
