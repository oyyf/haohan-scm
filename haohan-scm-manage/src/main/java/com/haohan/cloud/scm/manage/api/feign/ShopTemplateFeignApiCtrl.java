/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplate;
import com.haohan.cloud.scm.api.manage.req.ShopTemplateReq;
import com.haohan.cloud.scm.manage.service.ShopTemplateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 店铺模板
 *
 * @author haohan
 * @date 2019-05-29 14:28:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShopTemplate")
@Api(value = "shoptemplate", tags = "shoptemplate内部接口服务")
public class ShopTemplateFeignApiCtrl {

    private final ShopTemplateService shopTemplateService;


    /**
     * 通过id查询店铺模板
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shopTemplateService.getById(id));
    }


    /**
     * 分页查询 店铺模板 列表信息
     * @param shopTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopTemplatePage")
    public R getShopTemplatePage(@RequestBody ShopTemplateReq shopTemplateReq) {
        Page page = new Page(shopTemplateReq.getPageNo(), shopTemplateReq.getPageSize());
        ShopTemplate shopTemplate =new ShopTemplate();
        BeanUtil.copyProperties(shopTemplateReq, shopTemplate);

        return new R<>(shopTemplateService.page(page, Wrappers.query(shopTemplate)));
    }


    /**
     * 全量查询 店铺模板 列表信息
     * @param shopTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopTemplateList")
    public R getShopTemplateList(@RequestBody ShopTemplateReq shopTemplateReq) {
        ShopTemplate shopTemplate =new ShopTemplate();
        BeanUtil.copyProperties(shopTemplateReq, shopTemplate);

        return new R<>(shopTemplateService.list(Wrappers.query(shopTemplate)));
    }


    /**
     * 新增店铺模板
     * @param shopTemplate 店铺模板
     * @return R
     */
    @Inner
    @SysLog("新增店铺模板")
    @PostMapping("/add")
    public R save(@RequestBody ShopTemplate shopTemplate) {
        return new R<>(shopTemplateService.save(shopTemplate));
    }

    /**
     * 修改店铺模板
     * @param shopTemplate 店铺模板
     * @return R
     */
    @Inner
    @SysLog("修改店铺模板")
    @PostMapping("/update")
    public R updateById(@RequestBody ShopTemplate shopTemplate) {
        return new R<>(shopTemplateService.updateById(shopTemplate));
    }

    /**
     * 通过id删除店铺模板
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除店铺模板")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shopTemplateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除店铺模板")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询店铺模板")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询店铺模板总记录}")
    @PostMapping("/countByShopTemplateReq")
    public R countByShopTemplateReq(@RequestBody ShopTemplateReq shopTemplateReq) {

        return new R<>(shopTemplateService.count(Wrappers.query(shopTemplateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shopTemplateReq查询一条货位信息表")
    @PostMapping("/getOneByShopTemplateReq")
    public R getOneByShopTemplateReq(@RequestBody ShopTemplateReq shopTemplateReq) {

        return new R<>(shopTemplateService.getOne(Wrappers.query(shopTemplateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShopTemplate> shopTemplateList) {

        return new R<>(shopTemplateService.saveOrUpdateBatch(shopTemplateList));
    }

}
