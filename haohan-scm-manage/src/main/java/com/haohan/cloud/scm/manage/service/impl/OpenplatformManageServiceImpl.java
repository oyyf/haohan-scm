/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.OpenplatformManage;
import com.haohan.cloud.scm.manage.mapper.OpenplatformManageMapper;
import com.haohan.cloud.scm.manage.service.OpenplatformManageService;
import org.springframework.stereotype.Service;

/**
 * 开放平台应用资料管理
 *
 * @author haohan
 * @date 2019-05-13 17:16:32
 */
@Service
public class OpenplatformManageServiceImpl extends ServiceImpl<OpenplatformManageMapper, OpenplatformManage> implements OpenplatformManageService {

}
