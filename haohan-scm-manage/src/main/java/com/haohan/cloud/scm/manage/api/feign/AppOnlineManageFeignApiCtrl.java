/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.AppOnlineManage;
import com.haohan.cloud.scm.api.manage.req.AppOnlineManageReq;
import com.haohan.cloud.scm.manage.service.AppOnlineManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 应用上线管理
 *
 * @author haohan
 * @date 2019-05-29 14:25:17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/AppOnlineManage")
@Api(value = "apponlinemanage", tags = "apponlinemanage内部接口服务")
public class AppOnlineManageFeignApiCtrl {

    private final AppOnlineManageService appOnlineManageService;


    /**
     * 通过id查询应用上线管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(appOnlineManageService.getById(id));
    }


    /**
     * 分页查询 应用上线管理 列表信息
     * @param appOnlineManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAppOnlineManagePage")
    public R getAppOnlineManagePage(@RequestBody AppOnlineManageReq appOnlineManageReq) {
        Page page = new Page(appOnlineManageReq.getPageNo(), appOnlineManageReq.getPageSize());
        AppOnlineManage appOnlineManage =new AppOnlineManage();
        BeanUtil.copyProperties(appOnlineManageReq, appOnlineManage);

        return new R<>(appOnlineManageService.page(page, Wrappers.query(appOnlineManage)));
    }


    /**
     * 全量查询 应用上线管理 列表信息
     * @param appOnlineManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAppOnlineManageList")
    public R getAppOnlineManageList(@RequestBody AppOnlineManageReq appOnlineManageReq) {
        AppOnlineManage appOnlineManage =new AppOnlineManage();
        BeanUtil.copyProperties(appOnlineManageReq, appOnlineManage);

        return new R<>(appOnlineManageService.list(Wrappers.query(appOnlineManage)));
    }


    /**
     * 新增应用上线管理
     * @param appOnlineManage 应用上线管理
     * @return R
     */
    @Inner
    @SysLog("新增应用上线管理")
    @PostMapping("/add")
    public R save(@RequestBody AppOnlineManage appOnlineManage) {
        return new R<>(appOnlineManageService.save(appOnlineManage));
    }

    /**
     * 修改应用上线管理
     * @param appOnlineManage 应用上线管理
     * @return R
     */
    @Inner
    @SysLog("修改应用上线管理")
    @PostMapping("/update")
    public R updateById(@RequestBody AppOnlineManage appOnlineManage) {
        return new R<>(appOnlineManageService.updateById(appOnlineManage));
    }

    /**
     * 通过id删除应用上线管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除应用上线管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(appOnlineManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除应用上线管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(appOnlineManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询应用上线管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(appOnlineManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param appOnlineManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询应用上线管理总记录}")
    @PostMapping("/countByAppOnlineManageReq")
    public R countByAppOnlineManageReq(@RequestBody AppOnlineManageReq appOnlineManageReq) {

        return new R<>(appOnlineManageService.count(Wrappers.query(appOnlineManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param appOnlineManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据appOnlineManageReq查询一条货位信息表")
    @PostMapping("/getOneByAppOnlineManageReq")
    public R getOneByAppOnlineManageReq(@RequestBody AppOnlineManageReq appOnlineManageReq) {

        return new R<>(appOnlineManageService.getOne(Wrappers.query(appOnlineManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param appOnlineManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<AppOnlineManage> appOnlineManageList) {

        return new R<>(appOnlineManageService.saveOrUpdateBatch(appOnlineManageList));
    }

}
