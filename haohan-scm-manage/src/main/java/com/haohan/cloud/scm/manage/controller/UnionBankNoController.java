/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;
import com.haohan.cloud.scm.api.manage.req.UnionBankNoReq;
import com.haohan.cloud.scm.manage.service.UnionBankNoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 联行号
 *
 * @author haohan
 * @date 2019-05-29 14:29:10
 */
@RestController
@AllArgsConstructor
@RequestMapping("/unionbankno" )
@Api(value = "unionbankno", tags = "unionbankno管理")
public class UnionBankNoController {

    private final UnionBankNoService unionBankNoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param unionBankNo 联行号
     * @return
     */
    @GetMapping("/page" )
    public R getUnionBankNoPage(Page page, UnionBankNo unionBankNo) {
        return new R<>(unionBankNoService.page(page, Wrappers.query(unionBankNo)));
    }


    /**
     * 通过id查询联行号
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(unionBankNoService.getById(id));
    }

    /**
     * 新增联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @SysLog("新增联行号" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_unionbankno_add')" )
    public R save(@RequestBody UnionBankNo unionBankNo) {
        return new R<>(unionBankNoService.save(unionBankNo));
    }

    /**
     * 修改联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @SysLog("修改联行号" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_unionbankno_edit')" )
    public R updateById(@RequestBody UnionBankNo unionBankNo) {
        return new R<>(unionBankNoService.updateById(unionBankNo));
    }

    /**
     * 通过id删除联行号
     * @param id id
     * @return R
     */
    @SysLog("删除联行号" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_unionbankno_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(unionBankNoService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除联行号")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_unionbankno_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(unionBankNoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询联行号")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(unionBankNoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询联行号总记录}")
    @PostMapping("/countByUnionBankNoReq")
    public R countByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq) {

        return new R<>(unionBankNoService.count(Wrappers.query(unionBankNoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据unionBankNoReq查询一条货位信息表")
    @PostMapping("/getOneByUnionBankNoReq")
    public R getOneByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq) {

        return new R<>(unionBankNoService.getOne(Wrappers.query(unionBankNoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param unionBankNoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_unionbankno_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<UnionBankNo> unionBankNoList) {

        return new R<>(unionBankNoService.saveOrUpdateBatch(unionBankNoList));
    }


}
