/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.manage.mapper.PhotoGroupManageMapper;
import com.haohan.cloud.scm.manage.service.PhotoGroupManageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 图片组管理
 *
 * @author haohan
 * @date 2019-05-13 17:32:43
 */
@Service
@AllArgsConstructor
public class PhotoGroupManageServiceImpl extends ServiceImpl<PhotoGroupManageMapper, PhotoGroupManage> implements PhotoGroupManageService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(PhotoGroupManage entity) {
        if (StrUtil.isEmpty(entity.getGroupNum())) {
            String sn = scmIncrementUtil.inrcSnByClass(PhotoGroupManage.class, NumberPrefixConstant.PHOTO_GROUP_SN_PR);
            entity.setGroupNum(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    @Override
    public PhotoGroupManage getByGroupNum(String groupNum) {
        return baseMapper.selectList(Wrappers.<PhotoGroupManage>query().lambda()
                .eq(PhotoGroupManage::getGroupNum, groupNum)
        ).stream().findFirst().orElse(null);
    }

}
