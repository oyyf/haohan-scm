/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.PhotoManage;
import com.haohan.cloud.scm.api.manage.req.PhotoManageReq;
import com.haohan.cloud.scm.manage.service.PhotoManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 图片管理
 *
 * @author haohan
 * @date 2019-05-29 14:27:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PhotoManage")
@Api(value = "photomanage", tags = "photomanage内部接口服务")
public class PhotoManageFeignApiCtrl {

    private final PhotoManageService photoManageService;


    /**
     * 通过id查询图片管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(photoManageService.getById(id));
    }


    /**
     * 分页查询 图片管理 列表信息
     * @param photoManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoManagePage")
    public R getPhotoManagePage(@RequestBody PhotoManageReq photoManageReq) {
        Page page = new Page(photoManageReq.getPageNo(), photoManageReq.getPageSize());
        PhotoManage photoManage =new PhotoManage();
        BeanUtil.copyProperties(photoManageReq, photoManage);

        return new R<>(photoManageService.page(page, Wrappers.query(photoManage)));
    }


    /**
     * 全量查询 图片管理 列表信息
     * @param photoManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPhotoManageList")
    public R getPhotoManageList(@RequestBody PhotoManageReq photoManageReq) {
        PhotoManage photoManage =new PhotoManage();
        BeanUtil.copyProperties(photoManageReq, photoManage);

        return new R<>(photoManageService.list(Wrappers.query(photoManage)));
    }


    /**
     * 新增图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @Inner
    @SysLog("新增图片管理")
    @PostMapping("/add")
    public R save(@RequestBody PhotoManage photoManage) {
        return new R<>(photoManageService.save(photoManage));
    }

    /**
     * 修改图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @Inner
    @SysLog("修改图片管理")
    @PostMapping("/update")
    public R updateById(@RequestBody PhotoManage photoManage) {
        return new R<>(photoManageService.updateById(photoManage));
    }

    /**
     * 通过id删除图片管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除图片管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(photoManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除图片管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(photoManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询图片管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(photoManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询图片管理总记录}")
    @PostMapping("/countByPhotoManageReq")
    public R countByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq) {

        return new R<>(photoManageService.count(Wrappers.query(photoManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据photoManageReq查询一条货位信息表")
    @PostMapping("/getOneByPhotoManageReq")
    public R getOneByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq) {

        return new R<>(photoManageService.getOne(Wrappers.query(photoManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param photoManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PhotoManage> photoManageList) {

        return new R<>(photoManageService.saveOrUpdateBatch(photoManageList));
    }

}
