/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.req.PhotoGalleryReq;
import com.haohan.cloud.scm.manage.service.PhotoGalleryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 资源图片库
 *
 * @author haohan
 * @date 2019-05-29 14:27:45
 */
@RestController
@AllArgsConstructor
@RequestMapping("/photogallery" )
@Api(value = "photogallery", tags = "photogallery管理")
public class PhotoGalleryController {

    private final PhotoGalleryService photoGalleryService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param photoGallery 资源图片库
     * @return
     */
    @GetMapping("/page" )
    public R getPhotoGalleryPage(Page page, PhotoGallery photoGallery) {
        return new R<>(photoGalleryService.page(page, Wrappers.query(photoGallery).orderByDesc("create_date")));
    }


    /**
     * 通过id查询资源图片库
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(photoGalleryService.getById(id));
    }

    /**
     * 新增资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @SysLog("新增资源图片库" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_photogallery_add')" )
    public R save(@RequestBody PhotoGallery photoGallery) {
        return new R<>(photoGalleryService.save(photoGallery));
    }

    /**
     * 修改资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @SysLog("修改资源图片库" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_photogallery_edit')" )
    public R updateById(@RequestBody PhotoGallery photoGallery) {
        return new R<>(photoGalleryService.updateById(photoGallery));
    }

    /**
     * 通过id删除资源图片库
     * @param id id
     * @return R
     */
    @SysLog("删除资源图片库" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_photogallery_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(photoGalleryService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除资源图片库")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_photogallery_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(photoGalleryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询资源图片库")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(photoGalleryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询资源图片库总记录}")
    @PostMapping("/countByPhotoGalleryReq")
    public R countByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq) {

        return new R<>(photoGalleryService.count(Wrappers.query(photoGalleryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据photoGalleryReq查询一条货位信息表")
    @PostMapping("/getOneByPhotoGalleryReq")
    public R getOneByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq) {

        return new R<>(photoGalleryService.getOne(Wrappers.query(photoGalleryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param photoGalleryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_photogallery_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PhotoGallery> photoGalleryList) {

        return new R<>(photoGalleryService.saveOrUpdateBatch(photoGalleryList));
    }


}
