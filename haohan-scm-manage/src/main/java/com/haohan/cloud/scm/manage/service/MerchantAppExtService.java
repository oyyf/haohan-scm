/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;

/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-13 17:44:27
 */
public interface MerchantAppExtService extends IService<MerchantAppExt> {

}
