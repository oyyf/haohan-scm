/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.ShopCategory;
import com.haohan.cloud.scm.api.manage.req.ShopCategoryReq;
import com.haohan.cloud.scm.manage.service.ShopCategoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 店铺分类
 *
 * @author haohan
 * @date 2019-05-29 14:28:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShopCategory")
@Api(value = "shopcategory", tags = "shopcategory内部接口服务")
public class ShopCategoryFeignApiCtrl {

    private final ShopCategoryService shopCategoryService;


    /**
     * 通过id查询店铺分类
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shopCategoryService.getById(id));
    }


    /**
     * 分页查询 店铺分类 列表信息
     * @param shopCategoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopCategoryPage")
    public R getShopCategoryPage(@RequestBody ShopCategoryReq shopCategoryReq) {
        Page page = new Page(shopCategoryReq.getPageNo(), shopCategoryReq.getPageSize());
        ShopCategory shopCategory =new ShopCategory();
        BeanUtil.copyProperties(shopCategoryReq, shopCategory);

        return new R<>(shopCategoryService.page(page, Wrappers.query(shopCategory)));
    }


    /**
     * 全量查询 店铺分类 列表信息
     * @param shopCategoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopCategoryList")
    public R getShopCategoryList(@RequestBody ShopCategoryReq shopCategoryReq) {
        ShopCategory shopCategory =new ShopCategory();
        BeanUtil.copyProperties(shopCategoryReq, shopCategory);

        return new R<>(shopCategoryService.list(Wrappers.query(shopCategory)));
    }


    /**
     * 新增店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @Inner
    @SysLog("新增店铺分类")
    @PostMapping("/add")
    public R save(@RequestBody ShopCategory shopCategory) {
        return new R<>(shopCategoryService.save(shopCategory));
    }

    /**
     * 修改店铺分类
     * @param shopCategory 店铺分类
     * @return R
     */
    @Inner
    @SysLog("修改店铺分类")
    @PostMapping("/update")
    public R updateById(@RequestBody ShopCategory shopCategory) {
        return new R<>(shopCategoryService.updateById(shopCategory));
    }

    /**
     * 通过id删除店铺分类
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除店铺分类")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shopCategoryService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除店铺分类")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopCategoryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询店铺分类")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopCategoryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询店铺分类总记录}")
    @PostMapping("/countByShopCategoryReq")
    public R countByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq) {

        return new R<>(shopCategoryService.count(Wrappers.query(shopCategoryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopCategoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shopCategoryReq查询一条货位信息表")
    @PostMapping("/getOneByShopCategoryReq")
    public R getOneByShopCategoryReq(@RequestBody ShopCategoryReq shopCategoryReq) {

        return new R<>(shopCategoryService.getOne(Wrappers.query(shopCategoryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShopCategory> shopCategoryList) {

        return new R<>(shopCategoryService.saveOrUpdateBatch(shopCategoryList));
    }

}
