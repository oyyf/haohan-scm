/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.MerchantEmployee;
import com.haohan.cloud.scm.api.manage.req.MerchantEmployeeReq;
import com.haohan.cloud.scm.manage.service.MerchantEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 员工管理
 *
 * @author haohan
 * @date 2019-05-29 14:26:53
 */
@RestController
@AllArgsConstructor
@RequestMapping("/merchantemployee" )
@Api(value = "merchantemployee", tags = "merchantemployee管理")
public class MerchantEmployeeController {

    private final MerchantEmployeeService merchantEmployeeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param merchantEmployee 员工管理
     * @return
     */
    @GetMapping("/page" )
    public R getMerchantEmployeePage(Page page, MerchantEmployee merchantEmployee) {
        return new R<>(merchantEmployeeService.page(page, Wrappers.query(merchantEmployee)));
    }


    /**
     * 通过id查询员工管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(merchantEmployeeService.getById(id));
    }

    /**
     * 新增员工管理
     * @param merchantEmployee 员工管理
     * @return R
     */
    @SysLog("新增员工管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantemployee_add')" )
    public R save(@RequestBody MerchantEmployee merchantEmployee) {
        return new R<>(merchantEmployeeService.save(merchantEmployee));
    }

    /**
     * 修改员工管理
     * @param merchantEmployee 员工管理
     * @return R
     */
    @SysLog("修改员工管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantemployee_edit')" )
    public R updateById(@RequestBody MerchantEmployee merchantEmployee) {
        return new R<>(merchantEmployeeService.updateById(merchantEmployee));
    }

    /**
     * 通过id删除员工管理
     * @param id id
     * @return R
     */
    @SysLog("删除员工管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_merchantemployee_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(merchantEmployeeService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除员工管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_merchantemployee_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询员工管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询员工管理总记录}")
    @PostMapping("/countByMerchantEmployeeReq")
    public R countByMerchantEmployeeReq(@RequestBody MerchantEmployeeReq merchantEmployeeReq) {

        return new R<>(merchantEmployeeService.count(Wrappers.query(merchantEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据merchantEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantEmployeeReq")
    public R getOneByMerchantEmployeeReq(@RequestBody MerchantEmployeeReq merchantEmployeeReq) {

        return new R<>(merchantEmployeeService.getOne(Wrappers.query(merchantEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_merchantemployee_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<MerchantEmployee> merchantEmployeeList) {

        return new R<>(merchantEmployeeService.saveOrUpdateBatch(merchantEmployeeList));
    }


}
