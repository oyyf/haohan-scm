/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.TreeDict;
import com.haohan.cloud.scm.api.manage.req.TreeDictReq;
import com.haohan.cloud.scm.manage.service.TreeDictService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 树形字典
 *
 * @author haohan
 * @date 2019-05-29 14:28:58
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/TreeDict")
@Api(value = "treedict", tags = "treedict内部接口服务")
public class TreeDictFeignApiCtrl {

    private final TreeDictService treeDictService;


    /**
     * 通过id查询树形字典
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(treeDictService.getById(id));
    }


    /**
     * 分页查询 树形字典 列表信息
     * @param treeDictReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTreeDictPage")
    public R getTreeDictPage(@RequestBody TreeDictReq treeDictReq) {
        Page page = new Page(treeDictReq.getPageNo(), treeDictReq.getPageSize());
        TreeDict treeDict =new TreeDict();
        BeanUtil.copyProperties(treeDictReq, treeDict);

        return new R<>(treeDictService.page(page, Wrappers.query(treeDict)));
    }


    /**
     * 全量查询 树形字典 列表信息
     * @param treeDictReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTreeDictList")
    public R getTreeDictList(@RequestBody TreeDictReq treeDictReq) {
        TreeDict treeDict =new TreeDict();
        BeanUtil.copyProperties(treeDictReq, treeDict);

        return new R<>(treeDictService.list(Wrappers.query(treeDict)));
    }


    /**
     * 新增树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @Inner
    @SysLog("新增树形字典")
    @PostMapping("/add")
    public R save(@RequestBody TreeDict treeDict) {
        return new R<>(treeDictService.save(treeDict));
    }

    /**
     * 修改树形字典
     * @param treeDict 树形字典
     * @return R
     */
    @Inner
    @SysLog("修改树形字典")
    @PostMapping("/update")
    public R updateById(@RequestBody TreeDict treeDict) {
        return new R<>(treeDictService.updateById(treeDict));
    }

    /**
     * 通过id删除树形字典
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除树形字典")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(treeDictService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除树形字典")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(treeDictService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询树形字典")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(treeDictService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询树形字典总记录}")
    @PostMapping("/countByTreeDictReq")
    public R countByTreeDictReq(@RequestBody TreeDictReq treeDictReq) {

        return new R<>(treeDictService.count(Wrappers.query(treeDictReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param treeDictReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据treeDictReq查询一条货位信息表")
    @PostMapping("/getOneByTreeDictReq")
    public R getOneByTreeDictReq(@RequestBody TreeDictReq treeDictReq) {

        return new R<>(treeDictService.getOne(Wrappers.query(treeDictReq), false));
    }


    /**
     * 批量修改OR插入
     * @param treeDictList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<TreeDict> treeDictList) {

        return new R<>(treeDictService.saveOrUpdateBatch(treeDictList));
    }

}
