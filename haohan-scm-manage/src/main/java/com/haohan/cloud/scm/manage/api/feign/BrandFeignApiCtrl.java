/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Brand;
import com.haohan.cloud.scm.api.manage.req.BrandReq;
import com.haohan.cloud.scm.manage.service.BrandService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 品牌管理
 *
 * @author haohan
 * @date 2019-05-29 14:25:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Brand")
@Api(value = "brand", tags = "brand内部接口服务")
public class BrandFeignApiCtrl {

    private final BrandService brandService;


    /**
     * 通过id查询品牌管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(brandService.getById(id));
    }


    /**
     * 分页查询 品牌管理 列表信息
     * @param brandReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBrandPage")
    public R getBrandPage(@RequestBody BrandReq brandReq) {
        Page page = new Page(brandReq.getPageNo(), brandReq.getPageSize());
        Brand brand =new Brand();
        BeanUtil.copyProperties(brandReq, brand);

        return new R<>(brandService.page(page, Wrappers.query(brand)));
    }


    /**
     * 全量查询 品牌管理 列表信息
     * @param brandReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBrandList")
    public R getBrandList(@RequestBody BrandReq brandReq) {
        Brand brand =new Brand();
        BeanUtil.copyProperties(brandReq, brand);

        return new R<>(brandService.list(Wrappers.query(brand)));
    }


    /**
     * 新增品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @Inner
    @SysLog("新增品牌管理")
    @PostMapping("/add")
    public R save(@RequestBody Brand brand) {
        return new R<>(brandService.save(brand));
    }

    /**
     * 修改品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @Inner
    @SysLog("修改品牌管理")
    @PostMapping("/update")
    public R updateById(@RequestBody Brand brand) {
        return new R<>(brandService.updateById(brand));
    }

    /**
     * 通过id删除品牌管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除品牌管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(brandService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除品牌管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(brandService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询品牌管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(brandService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询品牌管理总记录}")
    @PostMapping("/countByBrandReq")
    public R countByBrandReq(@RequestBody BrandReq brandReq) {

        return new R<>(brandService.count(Wrappers.query(brandReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据brandReq查询一条货位信息表")
    @PostMapping("/getOneByBrandReq")
    public R getOneByBrandReq(@RequestBody BrandReq brandReq) {

        return new R<>(brandService.getOne(Wrappers.query(brandReq), false));
    }


    /**
     * 批量修改OR插入
     * @param brandList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Brand> brandList) {

        return new R<>(brandService.saveOrUpdateBatch(brandList));
    }

}
