/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Industry;
import com.haohan.cloud.scm.api.manage.req.IndustryReq;
import com.haohan.cloud.scm.manage.service.IndustryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 行业类型
 *
 * @author haohan
 * @date 2019-05-29 14:26:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Industry")
@Api(value = "industry", tags = "industry内部接口服务")
public class IndustryFeignApiCtrl {

    private final IndustryService industryService;


    /**
     * 通过id查询行业类型
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(industryService.getById(id));
    }


    /**
     * 分页查询 行业类型 列表信息
     * @param industryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchIndustryPage")
    public R getIndustryPage(@RequestBody IndustryReq industryReq) {
        Page page = new Page(industryReq.getPageNo(), industryReq.getPageSize());
        Industry industry =new Industry();
        BeanUtil.copyProperties(industryReq, industry);

        return new R<>(industryService.page(page, Wrappers.query(industry)));
    }


    /**
     * 全量查询 行业类型 列表信息
     * @param industryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchIndustryList")
    public R getIndustryList(@RequestBody IndustryReq industryReq) {
        Industry industry =new Industry();
        BeanUtil.copyProperties(industryReq, industry);

        return new R<>(industryService.list(Wrappers.query(industry)));
    }


    /**
     * 新增行业类型
     * @param industry 行业类型
     * @return R
     */
    @Inner
    @SysLog("新增行业类型")
    @PostMapping("/add")
    public R save(@RequestBody Industry industry) {
        return new R<>(industryService.save(industry));
    }

    /**
     * 修改行业类型
     * @param industry 行业类型
     * @return R
     */
    @Inner
    @SysLog("修改行业类型")
    @PostMapping("/update")
    public R updateById(@RequestBody Industry industry) {
        return new R<>(industryService.updateById(industry));
    }

    /**
     * 通过id删除行业类型
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除行业类型")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(industryService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除行业类型")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(industryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询行业类型")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(industryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询行业类型总记录}")
    @PostMapping("/countByIndustryReq")
    public R countByIndustryReq(@RequestBody IndustryReq industryReq) {

        return new R<>(industryService.count(Wrappers.query(industryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param industryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据industryReq查询一条货位信息表")
    @PostMapping("/getOneByIndustryReq")
    public R getOneByIndustryReq(@RequestBody IndustryReq industryReq) {

        return new R<>(industryService.getOne(Wrappers.query(industryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param industryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Industry> industryList) {

        return new R<>(industryService.saveOrUpdateBatch(industryList));
    }

}
