package com.haohan.cloud.scm.manage.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.req.MerchantFeignReq;
import com.haohan.cloud.scm.api.manage.req.merchant.QueryMerchantReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;

import java.util.List;

/**
 * @author dy
 * @date 2020/3/3
 */
public interface MerchantCoreService {

    /**
     * 分页查询
     *
     * @param page
     * @param req
     * @return
     */
    IPage<MerchantVO> findPage(Page<Merchant> page, QueryMerchantReq req);

    /**
     * 查询商家详情
     *
     * @param merchantId
     * @return
     */
    MerchantVO fetchInfo(String merchantId);

    /**
     * 平台商家
     *
     * @return
     */
    MerchantVO fetchPlatform();

    /**
     * 创建平台的商家
     *
     * @param merchant
     * @return
     */
    Merchant createMerchantWithPlatform(Merchant merchant);

    /**
     * 初始化平台商家
     *
     * @param merchant
     * @return
     */
    MerchantVO initPlatform(Merchant merchant);

    /**
     * 新增
     *
     * @param merchant
     * @return
     */
    boolean addMerchant(Merchant merchant);

    /**
     * 修改
     *
     * @param merchant
     * @return
     */
    boolean modifyMerchant(Merchant merchant);

    /**
     * 删除
     *
     * @param merchantId
     * @return
     */
    boolean deleteMerchant(String merchantId);

    /**
     * 根据merchantId 集合和 启用状态 查询列表
     *
     * @param req
     * @return
     */
    List<MerchantVO> findMerchantList(MerchantFeignReq req);
}
