/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.Brand;

/**
 * 品牌管理
 *
 * @author haohan
 * @date 2019-05-13 17:12:26
 */
public interface BrandService extends IService<Brand> {

}
