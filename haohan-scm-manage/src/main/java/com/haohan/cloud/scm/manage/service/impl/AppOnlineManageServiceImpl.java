/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.AppOnlineManage;
import com.haohan.cloud.scm.manage.mapper.AppOnlineManageMapper;
import com.haohan.cloud.scm.manage.service.AppOnlineManageService;
import org.springframework.stereotype.Service;

/**
 * 应用上线管理
 *
 * @author haohan
 * @date 2019-05-13 17:43:17
 */
@Service
public class AppOnlineManageServiceImpl extends ServiceImpl<AppOnlineManageMapper, AppOnlineManage> implements AppOnlineManageService {

}
