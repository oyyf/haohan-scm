/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;
import com.haohan.cloud.scm.api.manage.req.MerchantAppExtReq;
import com.haohan.cloud.scm.manage.service.MerchantAppExtService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-29 14:26:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/MerchantAppExt")
@Api(value = "merchantappext", tags = "merchantappext内部接口服务")
public class MerchantAppExtFeignApiCtrl {

    private final MerchantAppExtService merchantAppExtService;


    /**
     * 通过id查询商家应用扩展信息
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(merchantAppExtService.getById(id));
    }


    /**
     * 分页查询 商家应用扩展信息 列表信息
     * @param merchantAppExtReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantAppExtPage")
    public R getMerchantAppExtPage(@RequestBody MerchantAppExtReq merchantAppExtReq) {
        Page page = new Page(merchantAppExtReq.getPageNo(), merchantAppExtReq.getPageSize());
        MerchantAppExt merchantAppExt =new MerchantAppExt();
        BeanUtil.copyProperties(merchantAppExtReq, merchantAppExt);

        return new R<>(merchantAppExtService.page(page, Wrappers.query(merchantAppExt)));
    }


    /**
     * 全量查询 商家应用扩展信息 列表信息
     * @param merchantAppExtReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMerchantAppExtList")
    public R getMerchantAppExtList(@RequestBody MerchantAppExtReq merchantAppExtReq) {
        MerchantAppExt merchantAppExt =new MerchantAppExt();
        BeanUtil.copyProperties(merchantAppExtReq, merchantAppExt);

        return new R<>(merchantAppExtService.list(Wrappers.query(merchantAppExt)));
    }


    /**
     * 新增商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @Inner
    @SysLog("新增商家应用扩展信息")
    @PostMapping("/add")
    public R save(@RequestBody MerchantAppExt merchantAppExt) {
        return new R<>(merchantAppExtService.save(merchantAppExt));
    }

    /**
     * 修改商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @Inner
    @SysLog("修改商家应用扩展信息")
    @PostMapping("/update")
    public R updateById(@RequestBody MerchantAppExt merchantAppExt) {
        return new R<>(merchantAppExtService.updateById(merchantAppExt));
    }

    /**
     * 通过id删除商家应用扩展信息
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商家应用扩展信息")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(merchantAppExtService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商家应用扩展信息")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppExtService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商家应用扩展信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppExtService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商家应用扩展信息总记录}")
    @PostMapping("/countByMerchantAppExtReq")
    public R countByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq) {

        return new R<>(merchantAppExtService.count(Wrappers.query(merchantAppExtReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据merchantAppExtReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantAppExtReq")
    public R getOneByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq) {

        return new R<>(merchantAppExtService.getOne(Wrappers.query(merchantAppExtReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantAppExtList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<MerchantAppExt> merchantAppExtList) {

        return new R<>(merchantAppExtService.saveOrUpdateBatch(merchantAppExtList));
    }

}
