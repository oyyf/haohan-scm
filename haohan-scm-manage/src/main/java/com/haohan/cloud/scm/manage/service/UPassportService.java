/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.UPassport;

/**
 * 用户通行证
 *
 * @author haohan
 * @date 2019-05-13 17:27:42
 */
public interface UPassportService extends IService<UPassport> {

    UPassport fetchByTelephone(String telephone);
}
