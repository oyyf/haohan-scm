package com.haohan.cloud.scm.manage.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.dto.ShopSqlDTO;
import com.haohan.cloud.scm.api.manage.req.shop.QueryShopReq;
import com.haohan.cloud.scm.api.manage.req.shop.ShopEditReq;
import com.haohan.cloud.scm.api.manage.vo.ShopVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.manage.core.ShopManageCoreService;
import com.haohan.cloud.scm.manage.service.ShopService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/9/12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/manage/shop")
@Api(value = "ApiManageShop", tags = "ShopManage店铺管理")
public class ShopManageApiCtrl {

    private final ShopManageCoreService shopManageCoreService;
    private final ShopService shopService;


    @GetMapping("/page")
    public R<IPage<ShopVO>> findPage(Page page, @Validated QueryShopReq req) {
        ShopSqlDTO query = req.transTo();
        query.queryPage(page);
        IPage<ShopExtDTO> shopPage = shopService.findPage(req.transTo());
        IPage<ShopVO> result = new Page<>(shopPage.getCurrent(), shopPage.getSize(), shopPage.getTotal());
        result.setRecords(shopPage.getRecords().stream()
                .map(ShopVO::new)
                .collect(Collectors.toList())
        );
        return RUtil.success(result);
    }

    /**
     * 查询店铺 详情
     *
     * @return
     */
    @GetMapping("/fetchInfo")
    public R<ShopVO> fetchInfo(@Validated({SingleGroup.class}) QueryShopReq req) {
        ShopExtDTO shop = shopManageCoreService.fetchShopInfo(req.getShopId());
        return RUtil.success(new ShopVO(shop));
    }

    @SysLog("新增店铺信息")
    @PreAuthorize("@pms.hasPermission('scm_shop_add')")
    @PostMapping
    public R<Boolean> add(@RequestBody @Validated(FirstGroup.class) ShopEditReq req) {
        req.setShopId(null);
        return RUtil.success(shopManageCoreService.addShop(req));
    }

    @SysLog("修改店铺信息")
    @PreAuthorize("@pms.hasPermission('scm_shop_edit')")
    @PutMapping
    public R<Boolean> modify(@RequestBody @Validated(SecondGroup.class) ShopEditReq req) {
        return RUtil.success(shopManageCoreService.modifyShop(req));
    }

    @SysLog("删除店铺信息")
    @PreAuthorize("@pms.hasPermission('scm_shop_del')")
    @DeleteMapping("/{id}")
    public R<Boolean> delete(@PathVariable String id) {
        return RUtil.success(shopManageCoreService.deleteShop(id));
    }


}
