/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.req.ShopReq;
import com.haohan.cloud.scm.manage.service.ShopService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 店铺
 *
 * @author haohan
 * @date 2019-05-29 14:28:19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shop" )
@Api(value = "shop", tags = "shop管理")
public class ShopController {

    private final ShopService shopService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shop 店铺
     * @return
     */
    @GetMapping("/page" )
    public R getShopPage(Page page, Shop shop) {
        return new R<>(shopService.page(page, Wrappers.query(shop)));
    }


    /**
     * 通过id查询店铺
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shopService.getById(id));
    }

    /**
     * 新增店铺
     * @param shop 店铺
     * @return R
     */
    @SysLog("新增店铺" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shop_add')" )
    public R save(@RequestBody Shop shop) {
        return R.failed("不支持");
    }

    /**
     * 修改店铺
     * @param shop 店铺
     * @return R
     */
    @SysLog("修改店铺" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shop_edit')" )
    public R updateById(@RequestBody Shop shop) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除店铺
     * @param id id
     * @return R
     */
    @SysLog("删除店铺" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shop_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除店铺")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shop_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询店铺")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询店铺总记录}")
    @PostMapping("/countByShopReq")
    public R countByShopReq(@RequestBody ShopReq shopReq) {

        return new R<>(shopService.count(Wrappers.query(shopReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shopReq查询一条货位信息表")
    @PostMapping("/getOneByShopReq")
    public R getOneByShopReq(@RequestBody ShopReq shopReq) {

        return new R<>(shopService.getOne(Wrappers.query(shopReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shop_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Shop> shopList) {

        return R.failed("不支持");
    }


}
