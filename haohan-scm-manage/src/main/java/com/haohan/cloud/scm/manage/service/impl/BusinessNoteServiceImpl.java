/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.manage.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.manage.entity.BusinessNote;
import com.haohan.cloud.scm.manage.mapper.BusinessNoteMapper;
import com.haohan.cloud.scm.manage.service.BusinessNoteService;
import org.springframework.stereotype.Service;

/**
 * 商务留言
 *
 * @author haohan
 * @date 2019-05-13 17:15:46
 */
@Service
public class BusinessNoteServiceImpl extends ServiceImpl<BusinessNoteMapper, BusinessNote> implements BusinessNoteService {

}
