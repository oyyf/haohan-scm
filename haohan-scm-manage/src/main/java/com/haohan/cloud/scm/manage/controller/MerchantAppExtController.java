/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;
import com.haohan.cloud.scm.api.manage.req.MerchantAppExtReq;
import com.haohan.cloud.scm.manage.service.MerchantAppExtService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-29 14:26:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/merchantappext" )
@Api(value = "merchantappext", tags = "merchantappext管理")
public class MerchantAppExtController {

    private final MerchantAppExtService merchantAppExtService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param merchantAppExt 商家应用扩展信息
     * @return
     */
    @GetMapping("/page" )
    public R getMerchantAppExtPage(Page page, MerchantAppExt merchantAppExt) {
        return new R<>(merchantAppExtService.page(page, Wrappers.query(merchantAppExt)));
    }


    /**
     * 通过id查询商家应用扩展信息
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(merchantAppExtService.getById(id));
    }

    /**
     * 新增商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @SysLog("新增商家应用扩展信息" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantappext_add')" )
    public R save(@RequestBody MerchantAppExt merchantAppExt) {
        return new R<>(merchantAppExtService.save(merchantAppExt));
    }

    /**
     * 修改商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @SysLog("修改商家应用扩展信息" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_merchantappext_edit')" )
    public R updateById(@RequestBody MerchantAppExt merchantAppExt) {
        return new R<>(merchantAppExtService.updateById(merchantAppExt));
    }

    /**
     * 通过id删除商家应用扩展信息
     * @param id id
     * @return R
     */
    @SysLog("删除商家应用扩展信息" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_merchantappext_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(merchantAppExtService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商家应用扩展信息")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_merchantappext_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppExtService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商家应用扩展信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(merchantAppExtService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商家应用扩展信息总记录}")
    @PostMapping("/countByMerchantAppExtReq")
    public R countByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq) {

        return new R<>(merchantAppExtService.count(Wrappers.query(merchantAppExtReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据merchantAppExtReq查询一条货位信息表")
    @PostMapping("/getOneByMerchantAppExtReq")
    public R getOneByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq) {

        return new R<>(merchantAppExtService.getOne(Wrappers.query(merchantAppExtReq), false));
    }


    /**
     * 批量修改OR插入
     * @param merchantAppExtList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_merchantappext_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<MerchantAppExt> merchantAppExtList) {

        return new R<>(merchantAppExtService.saveOrUpdateBatch(merchantAppExtList));
    }


}
