/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplateExtInfo;

/**
 * 店铺模板扩展信息
 *
 * @author haohan
 * @date 2019-05-13 17:45:20
 */
public interface ShopTemplateExtInfoService extends IService<ShopTemplateExtInfo> {

}
