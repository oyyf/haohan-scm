/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.ShopTemplateExtInfo;
import com.haohan.cloud.scm.api.manage.req.ShopTemplateExtInfoReq;
import com.haohan.cloud.scm.manage.service.ShopTemplateExtInfoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 店铺模板扩展信息
 *
 * @author haohan
 * @date 2019-05-29 14:28:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShopTemplateExtInfo")
@Api(value = "shoptemplateextinfo", tags = "shoptemplateextinfo内部接口服务")
public class ShopTemplateExtInfoFeignApiCtrl {

    private final ShopTemplateExtInfoService shopTemplateExtInfoService;


    /**
     * 通过id查询店铺模板扩展信息
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shopTemplateExtInfoService.getById(id));
    }


    /**
     * 分页查询 店铺模板扩展信息 列表信息
     * @param shopTemplateExtInfoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopTemplateExtInfoPage")
    public R getShopTemplateExtInfoPage(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {
        Page page = new Page(shopTemplateExtInfoReq.getPageNo(), shopTemplateExtInfoReq.getPageSize());
        ShopTemplateExtInfo shopTemplateExtInfo =new ShopTemplateExtInfo();
        BeanUtil.copyProperties(shopTemplateExtInfoReq, shopTemplateExtInfo);

        return new R<>(shopTemplateExtInfoService.page(page, Wrappers.query(shopTemplateExtInfo)));
    }


    /**
     * 全量查询 店铺模板扩展信息 列表信息
     * @param shopTemplateExtInfoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShopTemplateExtInfoList")
    public R getShopTemplateExtInfoList(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {
        ShopTemplateExtInfo shopTemplateExtInfo =new ShopTemplateExtInfo();
        BeanUtil.copyProperties(shopTemplateExtInfoReq, shopTemplateExtInfo);

        return new R<>(shopTemplateExtInfoService.list(Wrappers.query(shopTemplateExtInfo)));
    }


    /**
     * 新增店铺模板扩展信息
     * @param shopTemplateExtInfo 店铺模板扩展信息
     * @return R
     */
    @Inner
    @SysLog("新增店铺模板扩展信息")
    @PostMapping("/add")
    public R save(@RequestBody ShopTemplateExtInfo shopTemplateExtInfo) {
        return new R<>(shopTemplateExtInfoService.save(shopTemplateExtInfo));
    }

    /**
     * 修改店铺模板扩展信息
     * @param shopTemplateExtInfo 店铺模板扩展信息
     * @return R
     */
    @Inner
    @SysLog("修改店铺模板扩展信息")
    @PostMapping("/update")
    public R updateById(@RequestBody ShopTemplateExtInfo shopTemplateExtInfo) {
        return new R<>(shopTemplateExtInfoService.updateById(shopTemplateExtInfo));
    }

    /**
     * 通过id删除店铺模板扩展信息
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除店铺模板扩展信息")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shopTemplateExtInfoService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除店铺模板扩展信息")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateExtInfoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询店铺模板扩展信息")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shopTemplateExtInfoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shopTemplateExtInfoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询店铺模板扩展信息总记录}")
    @PostMapping("/countByShopTemplateExtInfoReq")
    public R countByShopTemplateExtInfoReq(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {

        return new R<>(shopTemplateExtInfoService.count(Wrappers.query(shopTemplateExtInfoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shopTemplateExtInfoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shopTemplateExtInfoReq查询一条货位信息表")
    @PostMapping("/getOneByShopTemplateExtInfoReq")
    public R getOneByShopTemplateExtInfoReq(@RequestBody ShopTemplateExtInfoReq shopTemplateExtInfoReq) {

        return new R<>(shopTemplateExtInfoService.getOne(Wrappers.query(shopTemplateExtInfoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shopTemplateExtInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShopTemplateExtInfo> shopTemplateExtInfoList) {

        return new R<>(shopTemplateExtInfoService.saveOrUpdateBatch(shopTemplateExtInfoList));
    }

}
