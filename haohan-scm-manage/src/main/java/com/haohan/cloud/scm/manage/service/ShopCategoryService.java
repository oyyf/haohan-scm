/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.manage.entity.ShopCategory;

/**
 * 店铺分类
 *
 * @author haohan
 * @date 2019-05-13 17:39:16
 */
public interface ShopCategoryService extends IService<ShopCategory> {

}
