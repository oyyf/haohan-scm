/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.manage.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.req.UPassportReq;
import com.haohan.cloud.scm.manage.service.UPassportService;
import com.haohan.cloud.scm.manage.utils.ScmManageUtils;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 用户通行证
 *
 * @author haohan
 * @date 2019-05-29 14:29:24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/upassport" )
@Api(value = "upassport", tags = "upassport管理")
public class UPassportController {

    private final UPassportService uPassportService;
    private final ScmManageUtils scmManageUtils;

    /**
     * 分页查询
     * @param page 分页对象
     * @param uPassport 用户通行证
     * @return
     */
    @GetMapping("/page" )
    public R getUPassportPage(Page page, UPassport uPassport) {
        return new R<>(uPassportService.page(page, Wrappers.query(uPassport)));
    }


    /**
     * 通过id查询用户通行证
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(uPassportService.getById(id));
    }

    /**
     * 新增用户通行证
     * @param uPassport 用户通行证
     * @return R
     */
    @SysLog("新增用户通行证" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_upassport_add')" )
    public R save(@RequestBody UPassport uPassport) {
        return new R<>(uPassportService.save(uPassport));
    }

    /**
     * 修改用户通行证
     * @param uPassport 用户通行证
     * @return R
     */
    @SysLog("修改用户通行证" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_upassport_edit')" )
    public R updateById(@RequestBody UPassport uPassport) {
        return new R<>(uPassportService.updateById(uPassport));
    }

    /**
     * 通过id删除用户通行证
     * @param id id
     * @return R
     */
    @SysLog("删除用户通行证" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_upassport_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(uPassportService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除用户通行证")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_upassport_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(uPassportService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询用户通行证")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(uPassportService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询用户通行证总记录}")
    @PostMapping("/countByUPassportReq")
    public R countByUPassportReq(@RequestBody UPassportReq uPassportReq) {

        return new R<>(uPassportService.count(Wrappers.query(uPassportReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param uPassportReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据uPassportReq查询一条货位信息表")
    @PostMapping("/getOneByUPassportReq")
    public R getOneByUPassportReq(@RequestBody UPassportReq uPassportReq) {

        return new R<>(uPassportService.getOne(Wrappers.query(uPassportReq), false));
    }


    /**
     * 批量修改OR插入
     * @param uPassportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_upassport_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<UPassport> uPassportList) {

        return new R<>(uPassportService.saveOrUpdateBatch(uPassportList));
    }


    /**
     * 根据手机号 查询 uid
     * @param telephone 手机号
     * @return R
     */
    @PostMapping("/fetchUid")
    public R fetchUid(@RequestParam("telephone") String telephone) {
        if(StrUtil.isEmpty(telephone)){
            return R.failed("telephone不能为空");
        }
        UPassportReq uPassportReq = new UPassportReq();
        uPassportReq.setTelephone(telephone);
        UPassport uPassport = uPassportService.getOne(Wrappers.query(uPassportReq), false);
        if(null != uPassport){
            return R.ok(uPassport.getId());
        }
        return R.failed("查询失败");
    }

    /**
     * 根据手机号 查询 系统用户id
     * @param telephone 手机号
     * @return R
     */
    @PostMapping("/fetchUserId")
    public R fetchUserId(@RequestParam("telephone") String telephone) {
        if(StrUtil.isEmpty(telephone)){
            return R.failed("telephone不能为空");
        }
        return R.ok(scmManageUtils.fetchUserId(telephone));
    }

}
