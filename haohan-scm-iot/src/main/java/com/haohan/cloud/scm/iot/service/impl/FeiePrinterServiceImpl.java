/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.iot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;
import com.haohan.cloud.scm.iot.mapper.FeiePrinterMapper;
import com.haohan.cloud.scm.iot.service.FeiePrinterService;
import org.springframework.stereotype.Service;

/**
 * 飞鹅打印机管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:13
 */
@Service
public class FeiePrinterServiceImpl extends ServiceImpl<FeiePrinterMapper, FeiePrinter> implements FeiePrinterService {

}
