package com.haohan.cloud.scm.iot.api.ctrl;

import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;
import com.haohan.cloud.scm.iot.core.IotFeiePrinterService;
import com.haohan.cloud.scm.iot.service.FeiePrinterService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/8/24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/iot/feiePrinter")
@Api(value = "ApuFeiePrinter", tags = "飞鹅打印机Api管理")
public class FeiePrinterApiCtrl {

    private FeiePrinterService feiePrinterService;

    private IotFeiePrinterService iotFeiePrinterService;

    @PostMapping("/deleteById")
    @ApiOperation(value = "删除飞鹅打印终端")
    public R deleteById(@RequestBody FeiePrinter feiePrinter){
        return R.ok(feiePrinterService.removeById(feiePrinter));
    }

    @PostMapping("/editFeiePrinter")
    @ApiOperation(value = "编辑飞鹅打印终端")
    public R editCloudPrintTerminal(@RequestBody FeiePrinter feiePrinter){
        return R.ok(feiePrinterService.updateById(feiePrinter));
    }

    @PostMapping("/addFeiePrinter")
    @ApiOperation(value = "添加飞鹅打印机")
    public R addFeiePrinter(@RequestBody FeiePrinter printer){
        return R.ok(iotFeiePrinterService.addFeiePrinter(printer));
    }
}
