package com.haohan.cloud.scm.iot.api.ctrl;

import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import com.haohan.cloud.scm.iot.core.IotCloudPrintTerminalService;
import com.haohan.cloud.scm.iot.service.CloudPrintTerminalService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/8/24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/iot/cloudPrintTerminal")
@Api(value = "ApuCloudPrintTerminal", tags = "易联云打印机Api管理")
public class CloudPrintTerminalApiCtrl {

    private CloudPrintTerminalService cloudPrintTerminalService;

    private IotCloudPrintTerminalService iotCloudPrintTerminalService;

    @PostMapping("/deleteById")
    @ApiOperation(value = "删除云打印终端")
    public R deleteById(@RequestBody CloudPrintTerminal terminal){
        return R.ok(cloudPrintTerminalService.removeById(terminal));
    }

    @PostMapping("/editCloudPrintTerminal")
    @ApiOperation(value = "编辑云打印终端")
    public R editCloudPrintTerminal(@RequestBody CloudPrintTerminal terminal){
        return R.ok(cloudPrintTerminalService.updateById(terminal));
    }

    @PostMapping("/addCloudPrinter")
    @ApiOperation(value = "添加云打印机")
    public R addFeiePrinter(@RequestBody CloudPrintTerminal printer){
        return R.ok(iotCloudPrintTerminalService.addCloudPrinter(printer));
    }
}
