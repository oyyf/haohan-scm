package com.haohan.cloud.scm.iot.core;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;

/**
 * @author cx
 * @date 2019/8/24
 */
public interface IotFeiePrinterService {

    /**
     * 飞鹅打印机列表
     * @param page
     * @param feiePrinter
     * @return
     */
    IPage getFeiePrinterPage(Page page, FeiePrinter feiePrinter);

    /**
     * 新增飞鹅打印机
     * @param printer
     * @return
     */
    Boolean addFeiePrinter(FeiePrinter printer);
}

