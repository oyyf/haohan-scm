package com.haohan.cloud.scm.iot.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.Resp.CloudPrintTerminalResp;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.iot.core.IotCloudPrintTerminalService;
import com.haohan.cloud.scm.iot.service.CloudPrintTerminalService;
import com.haohan.cloud.scm.iot.utils.ScmIotUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/24
 */

@Service
@AllArgsConstructor
public class IotCloudPrintTerminalServiceImpl implements IotCloudPrintTerminalService {

    private final CloudPrintTerminalService cloudPrintTerminalService;

    private final ScmIotUtil scmIotUtil;

    /**
     * 飞鹅打印机列表
     * @param page
     * @param terminal
     * @return
     */
    @Override
    public IPage getCloudPrintPage(Page page, CloudPrintTerminal terminal) {
        IPage p = cloudPrintTerminalService.page(page, Wrappers.query(terminal));
        if(p.getRecords().isEmpty()){
            throw new EmptyDataException("数据为空");
        }
        List<CloudPrintTerminalResp> res = new ArrayList<>();
        p.getRecords().forEach(val -> {
            CloudPrintTerminal printer = BeanUtil.toBean(val, CloudPrintTerminal.class);
            Shop shop = scmIotUtil.queryShop(printer.getShopId());
            Merchant merchant = scmIotUtil.queryMerchat(printer.getMerchantId());
            CloudPrintTerminalResp resp = new CloudPrintTerminalResp();
            BeanUtil.copyProperties(printer,resp);
            resp.setMerchantName(merchant.getMerchantName());
            resp.setShopName(shop.getName());
            res.add(resp);
        });
        p.setRecords(res);
        return p;
    }

    /**
     * 新增易联云打印机
     * @param printer
     * @return
     */
    @Override
    public Boolean addCloudPrinter(CloudPrintTerminal printer) {
        if(scmIotUtil.queryShop(printer.getShopId()) == null){
            throw new EmptyDataException("店铺不存在");
        }
        if(scmIotUtil.queryMerchat(printer.getMerchantId()) == null){
            throw new EmptyDataException("商家不存在");
        }
        return cloudPrintTerminalService.save(printer);
    }
}
