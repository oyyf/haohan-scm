/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.iot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.iot.entity.TerminalManage;
import com.haohan.cloud.scm.iot.mapper.TerminalManageMapper;
import com.haohan.cloud.scm.iot.service.TerminalManageService;
import org.springframework.stereotype.Service;

/**
 * 终端设备管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:39
 */
@Service
public class TerminalManageServiceImpl extends ServiceImpl<TerminalManageMapper, TerminalManage> implements TerminalManageService {

}
