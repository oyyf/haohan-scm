package com.haohan.cloud.scm.iot.utils;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.feign.MerchantFeignService;
import com.haohan.cloud.scm.api.manage.feign.ShopFeignService;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author cx
 * @date 2019/8/24
 */

@Component
@AllArgsConstructor
public class ScmIotUtil {

    private ShopFeignService shopFeignService;

    private MerchantFeignService merchantFeignService;

    /**
     * 根据id查询shop
     * @param id
     * @return
     */
    public Shop queryShop(String id){
        R r = shopFeignService.getById(id, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r) || r.getData() == null){
            throw new EmptyDataException();
        }
        return BeanUtil.toBean(r.getData(),Shop.class);
    }

    /**
     * 根据id查询商家
     * @param id
     * @return
     */
    public Merchant queryMerchat(String id){
        R r = merchantFeignService.getById(id,SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r) || r.getData() == null){
            throw new EmptyDataException();
        }
        return BeanUtil.toBean(r.getData(),Merchant.class);
    }
}
