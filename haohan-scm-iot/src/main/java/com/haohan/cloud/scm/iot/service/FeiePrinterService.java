/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;

/**
 * 飞鹅打印机管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:13
 */
public interface FeiePrinterService extends IService<FeiePrinter> {

}
