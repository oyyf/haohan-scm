/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.entity.TerminalManage;
import com.haohan.cloud.scm.api.iot.req.TerminalManageReq;
import com.haohan.cloud.scm.iot.service.TerminalManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 终端设备管理
 *
 * @author haohan
 * @date 2019-05-29 14:14:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/terminalmanage" )
@Api(value = "terminalmanage", tags = "terminalmanage管理")
public class TerminalManageController {

    private final TerminalManageService terminalManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param terminalManage 终端设备管理
     * @return
     */
    @GetMapping("/page" )
    public R getTerminalManagePage(Page page, TerminalManage terminalManage) {
        return new R<>(terminalManageService.page(page, Wrappers.query(terminalManage)));
    }


    /**
     * 通过id查询终端设备管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(terminalManageService.getById(id));
    }

    /**
     * 新增终端设备管理
     * @param terminalManage 终端设备管理
     * @return R
     */
    @SysLog("新增终端设备管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_terminalmanage_add')" )
    public R save(@RequestBody TerminalManage terminalManage) {
        return new R<>(terminalManageService.save(terminalManage));
    }

    /**
     * 修改终端设备管理
     * @param terminalManage 终端设备管理
     * @return R
     */
    @SysLog("修改终端设备管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_terminalmanage_edit')" )
    public R updateById(@RequestBody TerminalManage terminalManage) {
        return new R<>(terminalManageService.updateById(terminalManage));
    }

    /**
     * 通过id删除终端设备管理
     * @param id id
     * @return R
     */
    @SysLog("删除终端设备管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_terminalmanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(terminalManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除终端设备管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_terminalmanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(terminalManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询终端设备管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(terminalManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param terminalManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询终端设备管理总记录}")
    @PostMapping("/countByTerminalManageReq")
    public R countByTerminalManageReq(@RequestBody TerminalManageReq terminalManageReq) {

        return new R<>(terminalManageService.count(Wrappers.query(terminalManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param terminalManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据terminalManageReq查询一条货位信息表")
    @PostMapping("/getOneByTerminalManageReq")
    public R getOneByTerminalManageReq(@RequestBody TerminalManageReq terminalManageReq) {

        return new R<>(terminalManageService.getOne(Wrappers.query(terminalManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param terminalManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_terminalmanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<TerminalManage> terminalManageList) {

        return new R<>(terminalManageService.saveOrUpdateBatch(terminalManageList));
    }


}
