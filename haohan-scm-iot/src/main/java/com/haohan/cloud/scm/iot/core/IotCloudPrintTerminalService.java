package com.haohan.cloud.scm.iot.core;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;

/**
 * @author cx
 * @date 2019/8/24
 */
public interface IotCloudPrintTerminalService {

    /**
     * 飞鹅打印机列表
     * @param page
     * @param terminal
     * @return
     */
    IPage getCloudPrintPage(Page page, CloudPrintTerminal terminal);

    /**
     * 新增易联云打印机
     * @param printer
     * @return
     */
    Boolean addCloudPrinter(CloudPrintTerminal printer);
}

