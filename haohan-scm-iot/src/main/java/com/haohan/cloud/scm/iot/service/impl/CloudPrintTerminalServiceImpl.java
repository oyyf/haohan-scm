/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.iot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import com.haohan.cloud.scm.iot.mapper.CloudPrintTerminalMapper;
import com.haohan.cloud.scm.iot.service.CloudPrintTerminalService;
import org.springframework.stereotype.Service;

/**
 * 云打印终端管理
 *
 * @author haohan
 * @date 2019-05-13 19:12:48
 */
@Service
public class CloudPrintTerminalServiceImpl extends ServiceImpl<CloudPrintTerminalMapper, CloudPrintTerminal> implements CloudPrintTerminalService {

}
