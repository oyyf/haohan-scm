/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import com.haohan.cloud.scm.api.iot.req.CloudPrintTerminalReq;
import com.haohan.cloud.scm.iot.service.CloudPrintTerminalService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 云打印终端管理
 *
 * @author haohan
 * @date 2019-05-29 14:14:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/CloudPrintTerminal")
@Api(value = "cloudprintterminal", tags = "cloudprintterminal内部接口服务")
public class CloudPrintTerminalFeignApiCtrl {

    private final CloudPrintTerminalService cloudPrintTerminalService;


    /**
     * 通过id查询云打印终端管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(cloudPrintTerminalService.getById(id));
    }


    /**
     * 分页查询 云打印终端管理 列表信息
     * @param cloudPrintTerminalReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCloudPrintTerminalPage")
    public R getCloudPrintTerminalPage(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq) {
        Page page = new Page(cloudPrintTerminalReq.getPageNo(), cloudPrintTerminalReq.getPageSize());
        CloudPrintTerminal cloudPrintTerminal =new CloudPrintTerminal();
        BeanUtil.copyProperties(cloudPrintTerminalReq, cloudPrintTerminal);

        return new R<>(cloudPrintTerminalService.page(page, Wrappers.query(cloudPrintTerminal)));
    }


    /**
     * 全量查询 云打印终端管理 列表信息
     * @param cloudPrintTerminalReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCloudPrintTerminalList")
    public R getCloudPrintTerminalList(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq) {
        CloudPrintTerminal cloudPrintTerminal =new CloudPrintTerminal();
        BeanUtil.copyProperties(cloudPrintTerminalReq, cloudPrintTerminal);

        return new R<>(cloudPrintTerminalService.list(Wrappers.query(cloudPrintTerminal)));
    }


    /**
     * 新增云打印终端管理
     * @param cloudPrintTerminal 云打印终端管理
     * @return R
     */
    @Inner
    @SysLog("新增云打印终端管理")
    @PostMapping("/add")
    public R save(@RequestBody CloudPrintTerminal cloudPrintTerminal) {
        return new R<>(cloudPrintTerminalService.save(cloudPrintTerminal));
    }

    /**
     * 修改云打印终端管理
     * @param cloudPrintTerminal 云打印终端管理
     * @return R
     */
    @Inner
    @SysLog("修改云打印终端管理")
    @PostMapping("/update")
    public R updateById(@RequestBody CloudPrintTerminal cloudPrintTerminal) {
        return new R<>(cloudPrintTerminalService.updateById(cloudPrintTerminal));
    }

    /**
     * 通过id删除云打印终端管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除云打印终端管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(cloudPrintTerminalService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除云打印终端管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(cloudPrintTerminalService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询云打印终端管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(cloudPrintTerminalService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param cloudPrintTerminalReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询云打印终端管理总记录}")
    @PostMapping("/countByCloudPrintTerminalReq")
    public R countByCloudPrintTerminalReq(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq) {

        return new R<>(cloudPrintTerminalService.count(Wrappers.query(cloudPrintTerminalReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param cloudPrintTerminalReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据cloudPrintTerminalReq查询一条货位信息表")
    @PostMapping("/getOneByCloudPrintTerminalReq")
    public R getOneByCloudPrintTerminalReq(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq) {

        return new R<>(cloudPrintTerminalService.getOne(Wrappers.query(cloudPrintTerminalReq), false));
    }


    /**
     * 批量修改OR插入
     * @param cloudPrintTerminalList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<CloudPrintTerminal> cloudPrintTerminalList) {

        return new R<>(cloudPrintTerminalService.saveOrUpdateBatch(cloudPrintTerminalList));
    }

}
