/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.iot.entity.TerminalManage;

/**
 * 终端设备管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:39
 */
public interface TerminalManageService extends IService<TerminalManage> {

}
