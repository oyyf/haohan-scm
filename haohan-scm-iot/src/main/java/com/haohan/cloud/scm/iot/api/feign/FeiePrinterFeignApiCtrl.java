/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.iot.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;
import com.haohan.cloud.scm.api.iot.req.FeiePrinterReq;
import com.haohan.cloud.scm.iot.service.FeiePrinterService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 飞鹅打印机管理
 *
 * @author haohan
 * @date 2019-05-29 14:14:17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/FeiePrinter")
@Api(value = "feieprinter", tags = "feieprinter内部接口服务")
public class FeiePrinterFeignApiCtrl {

    private final FeiePrinterService feiePrinterService;


    /**
     * 通过id查询飞鹅打印机管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(feiePrinterService.getById(id));
    }


    /**
     * 分页查询 飞鹅打印机管理 列表信息
     * @param feiePrinterReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchFeiePrinterPage")
    public R getFeiePrinterPage(@RequestBody FeiePrinterReq feiePrinterReq) {
        Page page = new Page(feiePrinterReq.getPageNo(), feiePrinterReq.getPageSize());
        FeiePrinter feiePrinter =new FeiePrinter();
        BeanUtil.copyProperties(feiePrinterReq, feiePrinter);

        return new R<>(feiePrinterService.page(page, Wrappers.query(feiePrinter)));
    }


    /**
     * 全量查询 飞鹅打印机管理 列表信息
     * @param feiePrinterReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchFeiePrinterList")
    public R getFeiePrinterList(@RequestBody FeiePrinterReq feiePrinterReq) {
        FeiePrinter feiePrinter =new FeiePrinter();
        BeanUtil.copyProperties(feiePrinterReq, feiePrinter);

        return new R<>(feiePrinterService.list(Wrappers.query(feiePrinter)));
    }


    /**
     * 新增飞鹅打印机管理
     * @param feiePrinter 飞鹅打印机管理
     * @return R
     */
    @Inner
    @SysLog("新增飞鹅打印机管理")
    @PostMapping("/add")
    public R save(@RequestBody FeiePrinter feiePrinter) {
        return new R<>(feiePrinterService.save(feiePrinter));
    }

    /**
     * 修改飞鹅打印机管理
     * @param feiePrinter 飞鹅打印机管理
     * @return R
     */
    @Inner
    @SysLog("修改飞鹅打印机管理")
    @PostMapping("/update")
    public R updateById(@RequestBody FeiePrinter feiePrinter) {
        return new R<>(feiePrinterService.updateById(feiePrinter));
    }

    /**
     * 通过id删除飞鹅打印机管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除飞鹅打印机管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(feiePrinterService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除飞鹅打印机管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(feiePrinterService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询飞鹅打印机管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(feiePrinterService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param feiePrinterReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询飞鹅打印机管理总记录}")
    @PostMapping("/countByFeiePrinterReq")
    public R countByFeiePrinterReq(@RequestBody FeiePrinterReq feiePrinterReq) {

        return new R<>(feiePrinterService.count(Wrappers.query(feiePrinterReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param feiePrinterReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据feiePrinterReq查询一条货位信息表")
    @PostMapping("/getOneByFeiePrinterReq")
    public R getOneByFeiePrinterReq(@RequestBody FeiePrinterReq feiePrinterReq) {

        return new R<>(feiePrinterService.getOne(Wrappers.query(feiePrinterReq), false));
    }


    /**
     * 批量修改OR插入
     * @param feiePrinterList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<FeiePrinter> feiePrinterList) {

        return new R<>(feiePrinterService.saveOrUpdateBatch(feiePrinterList));
    }

}
