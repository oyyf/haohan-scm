package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/6/11
 */
@Data
@ApiModel(description = "单品采购确认供应商及价格请求")
public class SinglePurchaseReq{

//    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String uId;

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" , required = true)
    private String pmId;

    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value = "采购单明细编号" , required = true)
    private String purchaseDetailSn;

    @NotNull(message = "realBuyPrice不能为空")
    @ApiModelProperty(value = "采购价格" , required = true)
    private BigDecimal realBuyPrice;

    @NotBlank(message = "supplierId不能为空")
    @ApiModelProperty(value = "供应商ID" , required = true)
    private String supplierId;

    @NotBlank(message = "payType不能为空")
    @ApiModelProperty(value = "付款方式:1对公转账2现金支付3在线支付4承兑汇票" , required = true, example = "1")
    private String payType;

    @NotBlank(message = "taskId不能为空")
    @ApiModelProperty(value = "采购任务id")
    private String taskId;

    /**
     * 揽货方式1.自提2.送货上门
     */
    @ApiModelProperty(value = "揽货方式1.自提2.送货上门")
    private String receiveType;

}
