package com.haohan.cloud.scm.api.manage.req.merchant;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/3/25
 * id查询详情 SingleGroup
 */
@Data
public class QueryMerchantReq {

    @NotBlank(message = "商家id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "商家id最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @ApiModelProperty(value = "商家启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "采购配送商家类型", notes = "商家的采购配送类型:0 普通商家  1 平台商家")
    private MerchantPdsTypeEnum pdsType;

    // 非eq

    @Length(max = 32, message = "商家名称最大长度32字符")
    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @Length(max = 32, message = "商家地址最大长度32字符")
    @ApiModelProperty(value = "商家地址")
    private String address;

    @Length(max = 32, message = "商家联系人名称最大长度32字符")
    @ApiModelProperty(value = "商家联系人名称")
    private String contact;

    @Length(max = 32, message = "商家电话最大长度32字符")
    @ApiModelProperty(value = "商家电话")
    private String telephone;

    @Length(max = 32, message = "行业名称最大长度32字符")
    @ApiModelProperty(value = "行业名称")
    private String industry;

    public Merchant tranTo() {
        // eq 参数
        Merchant merchant = new Merchant();
        merchant.setId(this.merchantId);
        merchant.setStatus(this.status);
        merchant.setPdsType(this.pdsType);
        return merchant;
    }
}
