/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;


/**
 * 车辆管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:20
 */
@Data
@TableName("scm_lms_truck_manage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "车辆管理")
public class TruckManage extends Model<TruckManage> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 车辆编号
     */
    private String truckNo;
    /**
     * 车辆品牌
     */
    private String brand;
    /**
     * 名称
     */
    private String truckName;
    /**
     * 负责人
     */
    private String principal;
    /**
     * 司机
     */
    private String driver;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 车辆载重Kg
     */
    private Integer carryWeight;
    /**
     * 车辆容积m3
     */
    private Integer carryVolume;
    /**
     * 状态
     */
    private String status;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
