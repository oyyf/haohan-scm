/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.api.opc.req.ShipperReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 发货人内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShipperFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface ShipperFeignService {


    /**
     * 通过id查询发货人
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/Shipper/{id}", method = RequestMethod.GET)
    R<Shipper> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 发货人 列表信息
     *
     * @param shipperReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Shipper/fetchShipperPage")
    R<Page<Shipper>> getShipperPage(@RequestBody ShipperReq shipperReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 发货人 列表信息
     *
     * @param shipperReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Shipper/fetchShipperList")
    R<List<Shipper>> getShipperList(@RequestBody ShipperReq shipperReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增发货人
     *
     * @param shipper 发货人
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/add")
    R<Boolean> save(@RequestBody Shipper shipper, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改发货人
     *
     * @param shipper 发货人
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/update")
    R<Boolean> updateById(@RequestBody Shipper shipper, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除发货人
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/listByIds")
    R<List<Shipper>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipperReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/countByShipperReq")
    R<Integer> countByShipperReq(@RequestBody ShipperReq shipperReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipperReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/getOneByShipperReq")
    R<Shipper> getOneByShipperReq(@RequestBody ShipperReq shipperReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shipperList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/Shipper/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<Shipper> shipperList, @RequestHeader(SecurityConstants.FROM) String from);


}
