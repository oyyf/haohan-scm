package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAuditTaskReq extends PurchaseTask {
    /**
     * 采购状态:1.待处理2.待审核3.采购中4.采购完成5.部分完成6.已关闭
     */
    private PurchaseStatusEnum purchaseStatus;

    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    private MethodTypeEnum methodType;

    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;

}
