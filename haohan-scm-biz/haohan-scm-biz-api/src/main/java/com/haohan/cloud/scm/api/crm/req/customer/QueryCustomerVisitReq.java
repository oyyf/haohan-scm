package com.haohan.cloud.scm.api.crm.req.customer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.VisitSqlDTO;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/1/3
 */
@Data
public class QueryCustomerVisitReq {

    @NotBlank(message = "客户拜访记录id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "客户拜访记录id长度最大32字符")
    @ApiModelProperty(value = "主键")
    private String visitId;

    @Length(max = 32, message = "客户编号长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @Length(max = 32, message = "拜访联系人id长度最大32字符")
    @ApiModelProperty(value = "拜访联系人id")
    private String linkmanId;

    @Length(max = 32, message = "拜访员工id长度最大32字符")
    @ApiModelProperty(value = "拜访员工id")
    private String visitEmployeeId;

    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成")
    private VisitStatusEnum visitStatus;

    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败")
    private CustomerStatusEnum customerStatus;

    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;

    // 非eq

    /**
     * 单独权限处理
     */
    @Length(max = 32, message = "app登录员工id长度最大32字符")
    @ApiModelProperty(value = "app登录员工id")
    private String employeeId;

    @Length(max = 32, message = "客户名称长度最大32字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 32, message = "拜访联系人名称长度最大32字符")
    @ApiModelProperty(value = "拜访联系人名称")
    private String linkmanName;

    @Length(max = 32, message = "拜访员工名称长度最大32字符")
    @ApiModelProperty(value = "拜访员工名称")
    private String visitEmployeeName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    // 客户联查

    /**
     * 单独处理
     */
    @ApiModelProperty(value = "销售区域")
    private String areaSn;

    @ApiModelProperty(value = "客户类型", notes = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    public VisitSqlDTO transTo(Page page) {
        VisitSqlDTO sqlDTO = new VisitSqlDTO();
        sqlDTO.queryPage(page);
        sqlDTO.setId(this.visitId);
        sqlDTO.setCustomerSn(this.customerSn);
        sqlDTO.setLinkmanId(this.linkmanId);

        sqlDTO.setEmployeeId(this.visitEmployeeId);
        sqlDTO.setVisitStatus(null == this.visitStatus ? null : this.visitStatus.getType());
        sqlDTO.setCustomerStatus(null == this.customerStatus ? null : this.customerStatus.getType());
        sqlDTO.setVisitStep(null == this.visitStep ? null : this.visitStep.getType());
        sqlDTO.setCustomerName(this.customerName);
        sqlDTO.setLinkmanName(this.linkmanName);
        sqlDTO.setEmployeeName(this.visitEmployeeName);

        sqlDTO.queryDate(startDate, endDate);
        sqlDTO.setCustomerType(null == this.customerType ? null : this.customerType.getType());
        return sqlDTO;
    }
}
