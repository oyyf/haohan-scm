package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAddPurchaseOrderReq {

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 汇总明细编号
     */
    private String summaryDetailSn;

}
