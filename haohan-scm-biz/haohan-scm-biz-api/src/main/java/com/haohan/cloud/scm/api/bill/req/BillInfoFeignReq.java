package com.haohan.cloud.scm.api.bill.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author dy
 * @date 2019/12/6
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BillInfoFeignReq extends BillInfoReq {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "审核通过标识")
    private Boolean successFlag;

    @ApiModelProperty(value = "订单编号 集合")
    private Set<String> orderSnSet;

    public BillInfoFeignReq(Page page) {
        super();
        this.current = page.getCurrent();
        this.size = page.getSize();
    }
}
