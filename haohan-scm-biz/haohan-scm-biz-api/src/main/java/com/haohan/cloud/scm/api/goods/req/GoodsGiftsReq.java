/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsGifts;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 赠品
 *
 * @author haohan
 * @date 2019-05-28 19:55:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "赠品")
public class GoodsGiftsReq extends GoodsGifts {

    private long pageSize;
    private long pageNo;




}
