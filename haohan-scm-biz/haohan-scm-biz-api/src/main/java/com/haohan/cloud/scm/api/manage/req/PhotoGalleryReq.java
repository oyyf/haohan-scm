/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 资源图片库
 *
 * @author haohan
 * @date 2019-05-28 20:37:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "资源图片库")
public class PhotoGalleryReq extends PhotoGallery {

    private long pageSize;
    private long pageNo;




}
