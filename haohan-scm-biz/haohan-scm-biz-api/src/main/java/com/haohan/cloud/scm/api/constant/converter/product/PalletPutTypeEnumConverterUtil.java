package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.PalletPutTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class PalletPutTypeEnumConverterUtil implements Converter<PalletPutTypeEnum> {
    @Override
    public PalletPutTypeEnum convert(Object o, PalletPutTypeEnum palletPutTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PalletPutTypeEnum.getByType(o.toString());
    }
}
