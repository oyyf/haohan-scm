package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditGoodsModelDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 使用的规格列表
     */
    List<GoodsModel>  useModelList;

    List<GoodsModelTotal> useTotalList;

    List<GoodsModel>  deleteModelList;

    List<GoodsModelTotal> deleteTotalList;


}
