/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import com.haohan.cloud.scm.api.crm.req.WorkReportReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 工作汇报内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WorkReportFeignService", value = ScmServiceName.SCM_CRM)
public interface WorkReportFeignService {


    /**
     * 通过id查询工作汇报
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WorkReport/{id}", method = RequestMethod.GET)
    R<WorkReport> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 工作汇报 列表信息
     *
     * @param workReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WorkReport/fetchWorkReportPage")
    R<Page<WorkReport>> getWorkReportPage(@RequestBody WorkReportReq workReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 工作汇报 列表信息
     *
     * @param workReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WorkReport/fetchWorkReportList")
    R<List<WorkReport>> getWorkReportList(@RequestBody WorkReportReq workReportReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增工作汇报
     *
     * @param workReport 工作汇报
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/add")
    R<Boolean> save(@RequestBody WorkReport workReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改工作汇报
     *
     * @param workReport 工作汇报
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/update")
    R<Boolean> updateById(@RequestBody WorkReport workReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除工作汇报
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/listByIds")
    R<List<WorkReport>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param workReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/countByWorkReportReq")
    R<Integer> countByWorkReportReq(@RequestBody WorkReportReq workReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param workReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/getOneByWorkReportReq")
    R<WorkReport> getOneByWorkReportReq(@RequestBody WorkReportReq workReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param workReportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WorkReport/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<WorkReport> workReportList, @RequestHeader(SecurityConstants.FROM) String from);


}
