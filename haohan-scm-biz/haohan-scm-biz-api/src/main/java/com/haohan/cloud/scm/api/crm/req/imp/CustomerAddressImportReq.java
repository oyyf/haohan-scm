package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.CustomerAddressImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class CustomerAddressImportReq {

    @NotEmpty(message = "客户送货地址列表不能为空")
    private List<CustomerAddressImport> deliveryAddressList;

}
