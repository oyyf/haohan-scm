package com.haohan.cloud.scm.api.sys.admin.feign;

import com.pig4cloud.pigx.admin.api.entity.SysOauthClientDetails;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author dy
 * @date 2019/12/4
 */
@FeignClient(contextId = "SysClientFeignService", value = ServiceNameConstants.UPMS_SERVICE)
public interface SysClientFeignService {

    /**
     * 通过 客户端id查询
     *
     * @param clientId 用户名
     * @param from     调用标志
     * @return R
     */
    @GetMapping("/api/feign/client/{clientId}")
    R<SysOauthClientDetails> getById(@PathVariable("clientId") String clientId, @RequestHeader(SecurityConstants.FROM) String from);


}
