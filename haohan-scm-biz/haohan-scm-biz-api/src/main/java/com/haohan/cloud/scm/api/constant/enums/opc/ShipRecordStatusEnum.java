package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/6
 * 发货记录状态: 1.待发货 2.已发货 3.已关闭
 */
@Getter
@AllArgsConstructor
public enum ShipRecordStatusEnum implements IBaseEnum {

    /**
     * 发货记录状态: 1.待发货 2.已发货 3.已关闭
     */
    wait("1", "待发货"),
    success("2", "已发货"),
    failed("3", "已关闭");

    private static final Map<String, ShipRecordStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ShipRecordStatusEnum e : ShipRecordStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ShipRecordStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
