/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.req.WarehouseAllotReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 库存调拨记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarehouseAllotFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WarehouseAllotFeignService {


    /**
     * 通过id查询库存调拨记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarehouseAllot/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 库存调拨记录 列表信息
     * @param warehouseAllotReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseAllot/fetchWarehouseAllotPage")
    R getWarehouseAllotPage(@RequestBody WarehouseAllotReq warehouseAllotReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 库存调拨记录 列表信息
     * @param warehouseAllotReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseAllot/fetchWarehouseAllotList")
    R getWarehouseAllotList(@RequestBody WarehouseAllotReq warehouseAllotReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增库存调拨记录
     * @param warehouseAllot 库存调拨记录
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/add")
    R save(@RequestBody WarehouseAllot warehouseAllot, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改库存调拨记录
     * @param warehouseAllot 库存调拨记录
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/update")
    R updateById(@RequestBody WarehouseAllot warehouseAllot, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除库存调拨记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarehouseAllot/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warehouseAllotReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/countByWarehouseAllotReq")
    R countByWarehouseAllotReq(@RequestBody WarehouseAllotReq warehouseAllotReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warehouseAllotReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/getOneByWarehouseAllotReq")
    R getOneByWarehouseAllotReq(@RequestBody WarehouseAllotReq warehouseAllotReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warehouseAllotList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllot/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarehouseAllot> warehouseAllotList, @RequestHeader(SecurityConstants.FROM) String from);


}
