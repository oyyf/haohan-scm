package com.haohan.cloud.scm.api.opc.req.payment;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author cx
 * @date 2019/8/21
 */

@Data
public class PaymentDetailReq {
    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "receivableSn不能为空")
    private String receivableSn;
}
