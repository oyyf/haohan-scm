/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.req.MarketEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 市场部员工内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MarketEmployeeFeignService", value = ScmServiceName.SCM_CRM)
public interface MarketEmployeeFeignService {


    /**
     * 通过id查询市场部员工
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/MarketEmployee/{id}", method = RequestMethod.GET)
    R<MarketEmployee> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 市场部员工 列表信息
     *
     * @param marketEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MarketEmployee/fetchMarketEmployeePage")
    R<Page<MarketEmployee>> getMarketEmployeePage(@RequestBody MarketEmployeeReq marketEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 市场部员工 列表信息
     *
     * @param marketEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MarketEmployee/fetchMarketEmployeeList")
    R<List<MarketEmployee>> getMarketEmployeeList(@RequestBody MarketEmployeeReq marketEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增市场部员工
     *
     * @param marketEmployee 市场部员工
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/add")
    R<Boolean> save(@RequestBody MarketEmployee marketEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改市场部员工
     *
     * @param marketEmployee 市场部员工
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/update")
    R<Boolean> updateById(@RequestBody MarketEmployee marketEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除市场部员工
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/listByIds")
    R<List<MarketEmployee>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param marketEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/countByMarketEmployeeReq")
    R<Integer> countByMarketEmployeeReq(@RequestBody MarketEmployeeReq marketEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param marketEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/getOneByMarketEmployeeReq")
    R<MarketEmployee> getOneByMarketEmployeeReq(@RequestBody MarketEmployeeReq marketEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param marketEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/MarketEmployee/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<MarketEmployee> marketEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);


}
