package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/28
 * 调配类型:1.采购2.库存
 */
@Getter
@AllArgsConstructor
public enum AllocateTypeEnum {

    purchase("1", "采购"),
    stock("2", "库存");

    private static final Map<String, AllocateTypeEnum> MAP = new HashMap<>(8);

    static {
        for (AllocateTypeEnum e : AllocateTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AllocateTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;

}
