package com.haohan.cloud.scm.api.order.impl;

import com.haohan.cloud.scm.api.aftersales.feign.ReturnOrderFeignService;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.order.OrderCommonService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/3/6
 */
@Service
@AllArgsConstructor
public class ReturnOrderFeignCommonServiceImpl implements OrderCommonService {

    private final ReturnOrderFeignService returnOrderFeignService;

    @Override
    public OrderInfoDTO fetchOrderInfo(String orderSn) {
        R<OrderInfoDTO> r = returnOrderFeignService.fetchOrderIfo(orderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询退货单详情失败");
        }
        return r.getData();
    }

    @Override
    public boolean updateOrderSettlement(String orderSn, PayStatusEnum status) {
        // 目前订单无支付状态
        return true;
    }

    @Override
    public void orderCompleteShip(String orderSn) {
        // 目前订单无完成发货状态
    }
}
