/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.AreaRelation;
import com.haohan.cloud.scm.api.crm.req.AreaRelationReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 区域关联关系内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AreaRelationFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface AreaRelationFeignService {


    /**
     * 通过id查询区域关联关系
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/AreaRelation/{id}", method = RequestMethod.GET)
    R<AreaRelation> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 区域关联关系 列表信息
     *
     * @param areaRelationReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/AreaRelation/fetchAreaRelationPage")
    R<Page<AreaRelation>> getAreaRelationPage(@RequestBody AreaRelationReq areaRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 区域关联关系 列表信息
     *
     * @param areaRelationReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/AreaRelation/fetchAreaRelationList")
    R<List<AreaRelation>> getAreaRelationList(@RequestBody AreaRelationReq areaRelationReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增区域关联关系
     *
     * @param areaRelation 区域关联关系
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/add")
    R<Boolean> save(@RequestBody AreaRelation areaRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改区域关联关系
     *
     * @param areaRelation 区域关联关系
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/update")
    R<Boolean> updateById(@RequestBody AreaRelation areaRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除区域关联关系
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/listByIds")
    R<List<AreaRelation>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param areaRelationReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/countByAreaRelationReq")
    R<Integer> countByAreaRelationReq(@RequestBody AreaRelationReq areaRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param areaRelationReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/getOneByAreaRelationReq")
    R<AreaRelation> getOneByAreaRelationReq(@RequestBody AreaRelationReq areaRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param areaRelationList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/AreaRelation/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<AreaRelation> areaRelationList, @RequestHeader(SecurityConstants.FROM) String from);


}
