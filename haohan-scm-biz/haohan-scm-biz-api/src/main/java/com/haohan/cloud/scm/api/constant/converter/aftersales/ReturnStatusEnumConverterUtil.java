package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;

/**
 * @author dy
 * @date 2019/8/6
 */
public class ReturnStatusEnumConverterUtil implements Converter<ReturnStatusEnum> {
    @Override
    public ReturnStatusEnum convert(Object o, ReturnStatusEnum returnTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReturnStatusEnum.getByType(o.toString());
    }
}