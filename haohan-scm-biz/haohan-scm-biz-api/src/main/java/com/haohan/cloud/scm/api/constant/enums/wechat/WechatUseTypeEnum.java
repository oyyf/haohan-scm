package com.haohan.cloud.scm.api.constant.enums.wechat;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/7/23
 * 使用类型:1.采购,2.供应
 */
@Getter
@AllArgsConstructor
public enum WechatUseTypeEnum implements IBaseEnum {

    /**
     * 使用类型:1.采购,2.供应
     */
    purchase("1", "采购"),
    supply("2", "供应");

    private static final Map<String, WechatUseTypeEnum> MAP = new HashMap<>(8);

    static {
        for (WechatUseTypeEnum e : WechatUseTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static WechatUseTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
