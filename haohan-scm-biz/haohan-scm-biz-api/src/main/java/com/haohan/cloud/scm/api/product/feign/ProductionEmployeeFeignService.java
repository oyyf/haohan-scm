/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ProductionEmployee;
import com.haohan.cloud.scm.api.product.req.ProductionEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 生产部员工表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductionEmployeeFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ProductionEmployeeFeignService {


    /**
     * 通过id查询生产部员工表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductionEmployee/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 生产部员工表 列表信息
     * @param productionEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductionEmployee/fetchProductionEmployeePage")
    R getProductionEmployeePage(@RequestBody ProductionEmployeeReq productionEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 生产部员工表 列表信息
     * @param productionEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductionEmployee/fetchProductionEmployeeList")
    R getProductionEmployeeList(@RequestBody ProductionEmployeeReq productionEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/add")
    R save(@RequestBody ProductionEmployee productionEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/update")
    R updateById(@RequestBody ProductionEmployee productionEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除生产部员工表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ProductionEmployee/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/countByProductionEmployeeReq")
    R countByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/getOneByProductionEmployeeReq")
    R getOneByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productionEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductionEmployee/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductionEmployee> productionEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);


}
