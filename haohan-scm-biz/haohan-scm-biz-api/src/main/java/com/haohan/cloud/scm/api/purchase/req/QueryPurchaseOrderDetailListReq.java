package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Data
@ApiModel(description = "根据采购单号查询采购单明细列表")
public class QueryPurchaseOrderDetailListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    @ApiModelProperty(value = "采购单号", required = true)
    private String purchaseSn;

    @ApiModelProperty(value = "采购员工通行证id", required = true)
    private String uid;

    @ApiModelProperty(value = "分页大小")
    private long size = 10;
    @ApiModelProperty(value = "分页当前页码")
    private long current = 1;
}
