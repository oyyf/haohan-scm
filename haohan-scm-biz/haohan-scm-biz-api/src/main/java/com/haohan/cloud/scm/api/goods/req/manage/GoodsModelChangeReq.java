package com.haohan.cloud.scm.api.goods.req.manage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/6/2
 * 修改库存数量
 */
@Data
public class GoodsModelChangeReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "商品规格id不能为空")
    @Length(max = 32, message = "商品规格id的长度最大32字符")
    @ApiModelProperty("商品规格id")
    private String modelId;

    @NotNull(message = "库存数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "库存数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "库存数量")
    private BigDecimal storage;


}
