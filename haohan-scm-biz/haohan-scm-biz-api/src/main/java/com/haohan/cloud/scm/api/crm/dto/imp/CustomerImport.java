package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class CustomerImport {

    // 暂不使用
//    @ApiModelProperty(value = "客户编号")
//    @Length(min = 0, max = 32, message = "客户编号的字符长度必须在0至32之间")
//    private String customerSn;

    @NotBlank(message = "客户名称不能为空")
    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @NotBlank(message = "销售区域不能为空")
    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "销售区域名称")
    private String areaName;

    @Length(min = 0, max = 32, message = "市场名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "市场名称")
    private String marketName;

    @NotNull(message = "客户类型不能为空")
    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    @NotBlank(message = "联系人姓名不能为空")
    @Length(min = 0, max = 10, message = "联系人姓名的字符长度必须在1至10之间")
    @ApiModelProperty(value = "联系人姓名")
    private String contact;

    @NotBlank(message = "联系人手机不能为空")
    @Length(min = 0, max = 20, message = "联系人手机的字符长度必须在1至20之间")
    @ApiModelProperty(value = "联系人手机")
    private String telephone;

    @Length(min = 0, max = 20, message = "座机电话的字符长度必须在0至20之间")
    @ApiModelProperty(value = "座机电话")
    private String phoneNumber;

    @Length(min = 0, max = 20, message = "客户经理的字符长度必须在0至20之间")
    @ApiModelProperty(value = "客户经理名称")
    private String directorName;

    @Length(min = 0, max = 64, message = "客户标签的字符长度必须在0至64之间")
    @ApiModelProperty(value = "客户标签")
    private String tags;

    @NotBlank(message = "省不能为空")
    @Length(min = 0, max = 20, message = "省的字符长度必须在1至20之间")
    @ApiModelProperty(value = "省")
    private String province;

    @NotBlank(message = "市不能为空")
    @Length(min = 0, max = 20, message = "市的字符长度必须在1至20之间")
    @ApiModelProperty(value = "市")
    private String city;

    @Length(min = 0, max = 20, message = "区的字符长度必须在0至20之间")
    @ApiModelProperty(value = "区")
    private String district;

    @Length(min = 0, max = 20, message = "街道、乡镇的字符长度必须在0至20之间")
    @ApiModelProperty(value = "街道、乡镇")
    private String street;

    @NotBlank(message = "详细地址不能为空")
    @Length(min = 0, max = 64, message = "详细地址的字符长度必须在1至64之间")
    @ApiModelProperty(value = "详细地址")
    private String address;

    @Length(min = 0, max = 64, message = "营业执照的字符长度必须在0至64之间")
    @ApiModelProperty(value = "营业执照编号")
    private String bizLicense;

    @Length(min = 0, max = 64, message = "营业执照名称的字符长度必须在0至64之间")
    @ApiModelProperty(value = "营业执照名称")
    private String licenseName;

    @Length(min = 0, max = 64, message = "工商注册号的字符长度必须在0至64之间")
    @ApiModelProperty(value = "工商注册号")
    private String registrationNum;

    @ApiModelProperty(value = "注册日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registrationDate;

    @Length(min = 0, max = 64, message = "客户注册全称的字符长度必须在0至64之间")
    @ApiModelProperty(value = "客户注册全称")
    private String regName;

    @Length(min = 0, max = 64, message = "经营法人名称的字符长度必须在0至64之间")
    @ApiModelProperty(value = "经营法人名称")
    private String legalName;

    @Length(min = 0, max = 64, message = "经营地址的字符长度必须在0至64之间")
    @ApiModelProperty(value = "经营地址")
    private String regAddress;

    @Length(min = 0, max = 64, message = "营业面积的字符长度必须在0至64之间")
    @ApiModelProperty(value = "经营面积")
    private String operateArea;

    @Length(min = 0, max = 64, message = "年经营流水的字符长度必须在0至64之间")
    @ApiModelProperty(value = "年经营流水")
    private String shopSale;

    @Length(min = 0, max = 255, message = "业务介绍的字符长度必须在0至255之间")
    @ApiModelProperty(value = "业务介绍")
    private String bizDesc;

    @Length(min = 0, max = 64, message = "营业时间的字符长度必须在0至64之间")
    @ApiModelProperty(value = "营业时间")
    private String serviceTime;

    @ApiModelProperty(value = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;

    @ApiModelProperty(value = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;

    @Length(min = 0, max = 32, message = "邮编的字符长度必须在0至32之间")
    @ApiModelProperty(value = "邮编")
    private String postcode;

    @Length(min = 0, max = 32, message = "传真的字符长度必须在0至32之间")
    @ApiModelProperty(value = "传真")
    private String fax;

    @Length(min = 0, max = 64, message = "网址的字符长度必须在0至64之间")
    @ApiModelProperty(value = "网址")
    private String website;

    @Length(min = 0, max = 64, message = "备注信息的字符长度必须在0至64之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public Customer transTo() {
        Customer customer = new Customer();
        customer.setCustomerName(this.customerName);

        customer.setCustomerType(this.customerType);
        customer.setContact(this.contact);
        customer.setTelephone(this.telephone);
        customer.setPhoneNumber(this.phoneNumber);

        customer.setTags(this.tags);
        customer.setProvince(this.province);
        customer.setCity(this.city);
        customer.setDistrict(this.district);
        customer.setStreet(this.street);
        customer.setAddress(this.address);
        customer.setBizLicense(this.bizLicense);
        customer.setLicenseName(this.licenseName);
        customer.setRegistrationNum(this.registrationNum);
        customer.setRegistrationDate(this.registrationDate);

        customer.setRegName(this.regName);
        customer.setLegalName(this.legalName);
        customer.setRegAddress(this.regAddress);
        customer.setOperateArea(this.operateArea);
        customer.setShopSale(this.shopSale);
        customer.setBizDesc(this.bizDesc);
        customer.setServiceTime(this.serviceTime);
        customer.setCompanyNature(this.companyNature);
        customer.setCustomerLevel(this.customerLevel);
        customer.setFax(this.fax);
        customer.setWebsite(this.website);
        customer.setRemarks(this.remarks);

        customer.setInitTime(LocalDateTime.now());
        customer.setStatus(UseStatusEnum.enabled);
        return customer;
    }
}
