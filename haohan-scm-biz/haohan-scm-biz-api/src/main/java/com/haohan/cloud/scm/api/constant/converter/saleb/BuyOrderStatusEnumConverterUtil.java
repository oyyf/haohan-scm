package com.haohan.cloud.scm.api.constant.converter.saleb;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class BuyOrderStatusEnumConverterUtil implements Converter<BuyOrderStatusEnum> {

    @Override
    public BuyOrderStatusEnum convert(Object o, BuyOrderStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BuyOrderStatusEnum.getByType(o.toString());
    }

}
