/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 结算记录   已废弃，使用bill模块
 *
 * @author haohan
 * @date 2019-05-13 20:39:12
 */
@Data
@TableName("scm_settlement_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "结算记录")
public class SettlementRecord extends Model<SettlementRecord> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 结算记录编号
     */
    @TableField(value = "settlement_id")
    private String settlementSn;
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;
    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;
    /**
     * 结算开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementBeginDate;
    /**
     * 结算结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementEndDate;
    /**
     * 付款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payDate;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 结算公司类型:采购商/供应商 (未启用)
     */
    private String companyType;
    /**
     * 结算公司id: merchantId
     */
    private String companyId;
    /**
     * 结算公司名称
     */
    private String companyName;
    /**
     * 结款人名称
     */
    private String companyOperator;
    /**
     * 结算凭证图片  修改为 图片组编号
     */
    @TableField(value = "settlement_img")
    private String groupNum;
    /**
     * 结算说明
     */
    private String settlementDesc;
    /**
     * 货款单号 不适用该值 使用结算单账单关联表 联查
     */
    private String paymentSn;
    /**
     * 经办人名称
     */
    private String operator;
    /**
     * 是否结算:0否1是
     */
    private YesNoEnum status;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
