package com.haohan.cloud.scm.api.supply.trans;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2020/4/23
 */
@UtilityClass
public class SupplyGoodsTrans {


    public static SupplierGoods initRelation(String merchantId, GoodsModelDTO platformModel, GoodsModelDTO supplyModel) {
        SupplierGoods relation = new SupplierGoods();
        relation.setSupplierMerchantId(merchantId);
        relation.setGoodsId(platformModel.getGoodsId());
        relation.setGoodsModelId(platformModel.getId());
        relation.setSupplyGoodsId(supplyModel.getGoodsId());
        relation.setSupplyModelId(supplyModel.getId());
        relation.setStatus(UseStatusEnum.enabled);
        relation.setSupplyType(SupplyTypeEnum.ordinaryRetail);
        return relation;
    }

    public static SupplierGoods initRelation(GoodsModelDTO platformModel, GoodsModel supplyModel) {
        SupplierGoods relation = new SupplierGoods();
        relation.setGoodsId(platformModel.getGoodsId());
        relation.setGoodsModelId(platformModel.getId());
        relation.setSupplyGoodsId(supplyModel.getGoodsId());
        relation.setSupplyModelId(supplyModel.getId());
        relation.setStatus(UseStatusEnum.enabled);
        relation.setSupplyType(SupplyTypeEnum.ordinaryRetail);
        return relation;
    }
}
