package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
public class PaymentParam {
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 实际采购数量
     */
    private BigDecimal buyNum;
    /**
     * 采购价
     */
    private BigDecimal dealPrice;
    /**
     * 收款人
     */
    private String companyOperator;
    /**
     * 付款方式1.协议2.现款
     */
    private PayTypeEnum payType;
    /**
     * 付款金额
     */
    private BigDecimal settlementAmount;

}
