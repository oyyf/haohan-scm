/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.Warehouse;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 仓库信息表
 *
 * @author haohan
 * @date 2019-05-28 19:50:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "仓库信息表")
public class WarehouseReq extends Warehouse {

    private long pageSize;
    private long pageNo;




}
