package com.haohan.cloud.scm.api.salec.resp;

import lombok.Data;

/**
 * @author cx
 * @date 2019/7/27
 */

@Data
public class PayDescResp {
    private String orderId;

    private String responseCode;

    private String transId;

    private String reqId;
}
