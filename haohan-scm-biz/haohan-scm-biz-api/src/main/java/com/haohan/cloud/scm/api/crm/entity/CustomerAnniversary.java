/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.market.AnniversaryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CalendarTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 客户纪念日
 *
 * @author haohan
 * @date 2019-08-30 12:00:19
 */
@Data
@TableName("crm_customer_anniversary")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户纪念日")
public class CustomerAnniversary extends Model<CustomerAnniversary> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 主题内容
     */
    @ApiModelProperty(value = "主题内容")
    private String content;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户联系人id
     */
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;
    /**
     * 客户联系人姓名
     */
    @ApiModelProperty(value = "客户联系人姓名")
    private String linkmanName;
    /**
     * 纪念日类型:1.公司2.联系人
     */
    @ApiModelProperty(value = "纪念日类型:1.公司2.联系人")
    private AnniversaryTypeEnum anniversaryType;
    /**
     * 纪念日时间
     */
    @ApiModelProperty(value = "纪念日时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime anniversaryTime;
    /**
     * 日历类型:1.公历2.农历
     */
    @ApiModelProperty(value = "日历类型:1.公历2.农历")
    private CalendarTypeEnum calendarType;
    /**
     * 市场负责人id
     */
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;
    /**
     * 市场负责人名称
     */
    @ApiModelProperty(value = "市场负责人名称")
    private String directorName;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
