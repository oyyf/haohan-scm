/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.req.SummaryOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购单汇总内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "OpsSummaryOrderFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface OpsSummaryOrderFeignService {


    /**
     * 通过id查询采购单汇总
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/OpsSummaryOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购单汇总 列表信息
     * @param opsSummaryOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OpsSummaryOrder/fetchOpsSummaryOrderPage")
    R getOpsSummaryOrderPage(@RequestBody SummaryOrderReq opsSummaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购单汇总 列表信息
     * @param opsSummaryOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OpsSummaryOrder/fetchOpsSummaryOrderList")
    R getOpsSummaryOrderList(@RequestBody SummaryOrderReq opsSummaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购单汇总
     * @param opsSummaryOrder 采购单汇总
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/add")
    R save(@RequestBody SummaryOrder opsSummaryOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购单汇总
     * @param opsSummaryOrder 采购单汇总
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/update")
    R updateById(@RequestBody SummaryOrder opsSummaryOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购单汇总
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/OpsSummaryOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param opsSummaryOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/countByOpsSummaryOrderReq")
    R countByOpsSummaryOrderReq(@RequestBody SummaryOrderReq opsSummaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param opsSummaryOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/getOneByOpsSummaryOrderReq")
    R getOneByOpsSummaryOrderReq(@RequestBody SummaryOrderReq opsSummaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param opsSummaryOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/OpsSummaryOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SummaryOrder> opsSummaryOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
