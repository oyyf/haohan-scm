/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.UserOpenPlatform;
import com.haohan.cloud.scm.api.manage.req.UserOpenPlatformReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;


/**
 * @program: haohan-fresh-scm
 * @description: 第三方开放平台用户管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "UserOpenPlatformFeignService", value = ScmServiceName.SCM_MANAGE)
public interface UserOpenPlatformFeignService {


//    /**
//     * 通过id查询第三方开放平台用户管理
//     * @param id id
//     * @return R
//     */
//    @RequestMapping(value = "/api/feign/UserOpenPlatform/{id}", method = RequestMethod.GET)
//    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 分页查询 第三方开放平台用户管理 列表信息
//     * @param userOpenPlatformReq 请求对象
//     * @return
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/fetchUserOpenPlatformPage")
//    R getUserOpenPlatformPage(@RequestBody UserOpenPlatformReq userOpenPlatformReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 全量查询 第三方开放平台用户管理 列表信息
//     * @param userOpenPlatformReq 请求对象
//     * @return
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/fetchUserOpenPlatformList")
//    R getUserOpenPlatformList(@RequestBody UserOpenPlatformReq userOpenPlatformReq, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 新增第三方开放平台用户管理
//     * @param userOpenPlatform 第三方开放平台用户管理
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/add")
//    R save(@RequestBody UserOpenPlatform userOpenPlatform, @RequestHeader(SecurityConstants.FROM) String from);

//
//    /**
//     * 修改第三方开放平台用户管理
//     * @param userOpenPlatform 第三方开放平台用户管理
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/update")
//    R updateById(@RequestBody UserOpenPlatform userOpenPlatform, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 通过id删除第三方开放平台用户管理
//     * @param id id
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/delete/{id}")
//    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//   * 删除（根据ID 批量删除)
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//    @PostMapping("/api/feign/UserOpenPlatform/batchDelete")
//    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量查询（根据IDS）
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/listByIds")
//    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     *
//     * @param userOpenPlatformReq 实体对象,可以为空
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/countByUserOpenPlatformReq")
//    R countByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询开放平台用户 (可根据openId、uid、appId查询)
     * 不限制tenantId
     *
     * @param userOpenPlatformReq 实体对象
     * @param from
     * @return R  可null
     */
    @PostMapping("/api/feign/UserOpenPlatform/getOneByUserOpenPlatformReq")
    R<UserOpenPlatform> getOneByUserOpenPlatformReq(@RequestBody UserOpenPlatformReq userOpenPlatformReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量修改OR插入
//     *
//     * @param userOpenPlatformList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @PostMapping("/api/feign/UserOpenPlatform/saveOrUpdateBatch")
//    R saveOrUpdateBatch(@RequestBody List<UserOpenPlatform> userOpenPlatformList, @RequestHeader(SecurityConstants.FROM) String from);


}
