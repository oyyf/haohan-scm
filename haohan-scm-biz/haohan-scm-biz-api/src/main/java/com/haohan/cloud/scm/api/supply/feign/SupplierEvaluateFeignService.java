/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;
import com.haohan.cloud.scm.api.supply.req.SupplierEvaluateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 供应商评价记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierEvaluateFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplierEvaluateFeignService {


    /**
     * 通过id查询供应商评价记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplierEvaluate/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应商评价记录 列表信息
     * @param supplierEvaluateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierEvaluate/fetchSupplierEvaluatePage")
    R getSupplierEvaluatePage(@RequestBody SupplierEvaluateReq supplierEvaluateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应商评价记录 列表信息
     * @param supplierEvaluateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierEvaluate/fetchSupplierEvaluateList")
    R getSupplierEvaluateList(@RequestBody SupplierEvaluateReq supplierEvaluateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应商评价记录
     * @param supplierEvaluate 供应商评价记录
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/add")
    R save(@RequestBody SupplierEvaluate supplierEvaluate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应商评价记录
     * @param supplierEvaluate 供应商评价记录
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/update")
    R updateById(@RequestBody SupplierEvaluate supplierEvaluate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应商评价记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SupplierEvaluate/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplierEvaluateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/countBySupplierEvaluateReq")
    R countBySupplierEvaluateReq(@RequestBody SupplierEvaluateReq supplierEvaluateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierEvaluateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/getOneBySupplierEvaluateReq")
    R getOneBySupplierEvaluateReq(@RequestBody SupplierEvaluateReq supplierEvaluateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param supplierEvaluateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SupplierEvaluate/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SupplierEvaluate> supplierEvaluateList, @RequestHeader(SecurityConstants.FROM) String from);


}
