/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreCategory;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品分类表
 *
 * @author haohan
 * @date 2019-06-19 17:43:14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品分类表")
public class StoreCategoryReq extends StoreCategory {

  private long pageSize;
  private long pageNo;


}
