package com.haohan.cloud.scm.api.crm.vo;

import com.pig4cloud.pigx.admin.api.entity.SysDept;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * @author dy
 * @date 2019/12/23
 */
@Data
@NoArgsConstructor
public class DeptAreaRelationVO {

    @Length(max = 64, message = "部门id长度最大64字符")
    @ApiModelProperty(value = "部门id")
    private String deptId;

    @Length(max = 64, message = "部门名称长度最大64字符")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "区域sn列表")
    private List<String> areaSnList;

    @ApiModelProperty(value = "区域数量")
    private Integer areaNum;

    public DeptAreaRelationVO(SysDept dept) {
        if (null != dept) {
            this.deptId = null != dept.getDeptId() ? dept.getDeptId().toString() : "";
            this.deptName = dept.getName();
        }
        this.areaNum = 0;
    }

}
