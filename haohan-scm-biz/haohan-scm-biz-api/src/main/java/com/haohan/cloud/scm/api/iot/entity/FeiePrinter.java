/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.iot.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 飞鹅打印机管理
 *
 * @author haohan
 * @date 2019-05-13 19:14:13
 */
@Data
@TableName("scm_feie_printer")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "飞鹅打印机管理")
public class FeiePrinter extends Model<FeiePrinter> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
   * 店铺ID
   */
    private String shopId;
    /**
   * 商家ID
   */
    private String merchantId;
    /**
   * 打印机类型
   */
    private String printerType;
    /**
   * 打印机编号
   */
    private String printerSn;
    /**
   * 打印机秘钥
   */
    private String printerKey;
    /**
   * 打印机名
   */
    private String printerName;
    /**
   * 模板类型
   */
    private String templateType;
    /**
   * 打印模板,小票样式
   */
    private String template;
    /**
   * 可打印分类
   */
    private String category;
    /**
   * 打印次数
   */
    private String times;
    /**
   * 云打印状态,是否添加至飞鹅云,0 -否 1-是
   */
    private String status;
    /**
   * 启用状态
   */
    private String useable;
    /**
   * 创建者
   */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
   * 创建时间
   */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
   * 更新时间
   */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;

}
