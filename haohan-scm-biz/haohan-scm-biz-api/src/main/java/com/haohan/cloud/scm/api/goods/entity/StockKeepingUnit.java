/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 库存商品库
 *
 * @author haohan
 * @date 2019-05-13 19:07:17
 */
@Data
@TableName("scm_cms_stock_keeping_unit")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "库存商品库")
public class StockKeepingUnit extends Model<StockKeepingUnit> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 标准商品id
     */
    private String spuId;
    /**
     * 规格属性名id集
     */
    private String attrNameIds;
    /**
     * 规格属性值id集
     */
    private String attrValueIds;
    /**
     * 商品唯一编号
     */
    private String stockGoodsSn;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 售价
     */
    private BigDecimal salePrice;
    /**
     * 库存
     */
    private String stock;
    /**
     * 单位
     */
    private String unit;
    /**
     * 规格详情,拼接所有属性值
     */
    private String attrDetail;
    /**
     * 扫码条码
     */
    private String scanCode;
    /**
     * 规格图片
     */
    private String attrPhoto;
    /**
     * 重量;单位kg
     */
    private BigDecimal weight;
    /**
     * 体积;单位立方米
     */
    private BigDecimal volume;
    /**
     * 排序
     */
    private BigDecimal sort;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
