package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.PalletTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class PalleTypeEnumConverterUtil implements Converter<PalletTypeEnum> {
    @Override
    public PalletTypeEnum convert(Object o, PalletTypeEnum palleTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PalletTypeEnum.getByType(o.toString());
    }
}
