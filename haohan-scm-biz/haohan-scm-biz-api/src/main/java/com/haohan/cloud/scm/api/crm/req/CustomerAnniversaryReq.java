/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerAnniversary;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户纪念日
 *
 * @author haohan
 * @date 2019-08-30 11:45:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户纪念日")
public class CustomerAnniversaryReq extends CustomerAnniversary {

    private long pageSize;
    private long pageNo;


}
