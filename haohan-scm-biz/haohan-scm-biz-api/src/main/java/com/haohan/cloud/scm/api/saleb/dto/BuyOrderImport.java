package com.haohan.cloud.scm.api.saleb.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/9/12
 */
@Data
public class BuyOrderImport {

    /**
     * 配送日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "配送日期不能为空")
    private LocalDateTime deliveryTime;
    /**
     * 配送批次
     */
    @NotBlank(message = "配送批次不能为空")
    @Length(min=1, max=2, message = "配送批次的字符长度必须在1至2之间")
    private String buySeq;
    /**
     * 采购商名称
     */
    @NotBlank(message = "采购商名称不能为空")
    @Length(min=1, max=20, message = "采购商名称的字符长度必须在1至20之间")
    private String buyerName;
    /**
     * 采购需求
     */
    @Length(min=0, max=100, message = "采购需求的字符长度必须在0至100之间")
    private String needNote;
    /**
     * 配送方式
     */
    @Length(min=0, max=2, message = "配送方式的字符长度必须在0至2之间")
    private String deliveryType;
    /**
     * 联系人
     */
    @Length(min=0, max=20, message = "联系人的字符长度必须在1至20之间")
    private String contact;
    /**
     * 联系电话
     */
    @Length(min=0, max=15, message = "联系电话的字符长度必须在1至15之间")
    private String telephone;
    /**
     * 配送地址
     */
    @Length(min=0, max=100, message = "配送地址的字符长度必须在1至100之间")
    private String address;
    /**
     * 运费
     */
    @Range(min=0, max=1000000, message = "运费在0至1000000之间")
    private BigDecimal shipFee;

    // 商品明细
    /**
     * 商品规格编号
     */
//    private String goodsModelSn;
    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    @Length(min=1, max=20, message = "商品名称的字符长度必须在1至20之间")
    private String goodsName;
    /**
     * 商品规格 名称
     */
    @NotBlank(message = "商品规格不能为空")
    @Length(min=1, max=20, message = "商品规格的字符长度必须在1至20之间")
    private String goodsModel;
    /**
     * 单位
     */
    @Length(min=0, max=10, message = "单位的字符长度必须在0至10之间")
    private String unit;
    /**
     * 采购价格
     */
    @NotNull(message = "采购价格不能为空")
    @Range(min=0, max=1000000, message = "采购价格在0至1000000之间")
    private BigDecimal buyPrice;
    /**
     * 采购数量  商品下单数量
     */
    @NotNull(message = "采购数量不能为空")
    @Range(min=0, max=1000000, message = "采购数量在0至1000000之间")
    private BigDecimal orderGoodsNum;
    /**
     * 采购备注  采购明细备注信息
     */
    @Length(min=0, max=100, message = "采购备注的字符长度必须在0至100之间")
    private String remarks;


}
