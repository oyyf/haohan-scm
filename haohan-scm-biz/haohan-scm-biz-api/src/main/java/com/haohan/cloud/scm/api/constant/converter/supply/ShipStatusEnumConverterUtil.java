package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.ShipStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ShipStatusEnumConverterUtil implements Converter<ShipStatusEnum> {
    @Override
    public ShipStatusEnum convert(Object o, ShipStatusEnum shipStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShipStatusEnum.getByType(o.toString());
    }
}
