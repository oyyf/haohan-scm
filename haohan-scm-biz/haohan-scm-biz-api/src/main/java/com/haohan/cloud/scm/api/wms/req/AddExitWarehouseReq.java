package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/16
 */
@Data
@ApiModel(description = "新增出库单（出库单明细）")
public class AddExitWarehouseReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

//    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseSn;

    @NotNull(message = "exitType不能为空")
    @ApiModelProperty(value = "出库单类型:1.配送2.加工", required = true)
    private ExitTypeEnum exitType;

    @NotEmpty(message = "商品参数列表不能为空")
    private List<GoodsModelReq> list;
}
