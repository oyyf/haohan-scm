package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/20
 */
@Getter
@AllArgsConstructor
public enum GradeTypeEnum implements IBaseEnum {
    /**
     * 等级:一星-五星
     */
    oneRank("1", "一星"),
    twoRank("2", "二星"),
    threeRank("3", "三星"),
    fourRank("4", "四星"),
    fiveRank("5", "五星");

    private static final Map<String, GradeTypeEnum> MAP = new HashMap<>(8);
    private static final Map<String, GradeTypeEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (GradeTypeEnum e : GradeTypeEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static GradeTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    public static GradeTypeEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
