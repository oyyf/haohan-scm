package com.haohan.cloud.scm.api.crm.req.bill;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.req.SettlementFeignReq;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/28
 * SingleGroup 带settlementSn
 */
@Data
@Api("查询应收账单(员工客户)")
public class CrmSettlementQueryReq {


    @NotBlank(message = "员工ID不能为空", groups = {SingleGroup.class, Default.class})
    @Length(max = 32, message = "员工ID最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    // eq条件

    @NotBlank(message = "结算记录编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "结算记录编号的最大长度为32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "结算记录编号")
    private String settlementSn;

    @ApiModelProperty(value = "结算状态")
    private YesNoEnum settlementStatus;

    @Length(max = 32, message = "账单编号的最大长度为32字符")
    @ApiModelProperty(value = "账单编号")
    private String billSn;

    @Length(max = 32, message = "来源订单编号最大长度32字符")
    @ApiModelProperty(value = "来源订单编号")
    private String orderSn;

    @Length(max = 32, message = "下单客户编号最大长度32字符")
    @ApiModelProperty(value = "下单客户编号", notes = "客户sn")
    private String customerSn;


    // 非eq查询条件

    @ApiModelProperty(value = "订单成交日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "订单成交日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    public SettlementFeignReq transToFeign(Page page) {
        SettlementFeignReq query = new SettlementFeignReq();
        query.setCurrent(page.getCurrent());
        query.setSize(page.getSize());
        query.setStartDate(this.startDate);
        query.setEndDate(this.endDate);

        // eq 参数 客户编号需单独处理
        query.setSettlementSn(this.settlementSn);
        query.setSettlementStatus(this.settlementStatus);
        query.setBillSn(this.billSn);
        query.setOrderSn(this.orderSn);
        return query;
    }
}
