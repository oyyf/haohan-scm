/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 供应订单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplyOrderFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplyOrderFeignService {


    /**
     * 通过id查询供应订单
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplyOrder/{id}", method = RequestMethod.GET)
    R<SupplyOrder> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应订单 列表信息
     *
     * @param supplyOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplyOrder/fetchSupplyOrderPage")
    R<Page<SupplyOrder>> getSupplyOrderPage(@RequestBody SupplyOrderReq supplyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应订单 列表信息
     *
     * @param supplyOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplyOrder/fetchSupplyOrderList")
    R<List<SupplyOrder>> getSupplyOrderList(@RequestBody SupplyOrderReq supplyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/add")
    R<Boolean> save(@RequestBody SupplyOrder supplyOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/update")
    R<Boolean> updateById(@RequestBody SupplyOrder supplyOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应订单
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/listByIds")
    R<List<SupplyOrder>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplyOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/countBySupplyOrderReq")
    R<Integer> countBySupplyOrderReq(@RequestBody SupplyOrderReq supplyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplyOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/getOneBySupplyOrderReq")
    R<SupplyOrder> getOneBySupplyOrderReq(@RequestBody SupplyOrderReq supplyOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param supplyOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SupplyOrder/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SupplyOrder> supplyOrderList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 获取订单详细
     *
     * @param orderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/SupplyOrder/orderInfo")
    R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

}
