/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProductDescription;
import com.haohan.cloud.scm.api.salec.req.StoreProductDescriptionReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductDescriptionFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductDescriptionFeignService {


    /**
     * 通过id查询
     *
     * @param productId id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/StoreProductDescription/{productId}", method = RequestMethod.GET)
    R<StoreProductDescription> getById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询  列表信息
     *
     * @param storeProductDescriptionReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/StoreProductDescription/fetchStoreProductDescriptionPage")
    R<Page<StoreProductDescription>> getStoreProductDescriptionPage(@RequestBody StoreProductDescriptionReq storeProductDescriptionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询  列表信息
     *
     * @param storeProductDescriptionReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/StoreProductDescription/fetchStoreProductDescriptionList")
    R<List<StoreProductDescription>> getStoreProductDescriptionList(@RequestBody StoreProductDescriptionReq storeProductDescriptionReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增
     *
     * @param storeProductDescription
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/add")
    R<Boolean> save(@RequestBody StoreProductDescription storeProductDescription, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改
     *
     * @param storeProductDescription
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/update")
    R<Boolean> updateById(@RequestBody StoreProductDescription storeProductDescription, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除
     *
     * @param productId id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/delete/{productId}")
    R<Boolean> removeById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/listByIds")
    R<List<StoreProductDescription>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param storeProductDescriptionReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/countByStoreProductDescriptionReq")
    R<Integer> countByStoreProductDescriptionReq(@RequestBody StoreProductDescriptionReq storeProductDescriptionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param storeProductDescriptionReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/getOneByStoreProductDescriptionReq")
    R<StoreProductDescription> getOneByStoreProductDescriptionReq(@RequestBody StoreProductDescriptionReq storeProductDescriptionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param storeProductDescriptionList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/StoreProductDescription/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProductDescription> storeProductDescriptionList, @RequestHeader(SecurityConstants.FROM) String from);


}
