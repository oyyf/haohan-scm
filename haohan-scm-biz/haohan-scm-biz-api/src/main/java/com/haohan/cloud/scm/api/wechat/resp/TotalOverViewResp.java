package com.haohan.cloud.scm.api.wechat.resp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @program: haohan-fresh-scm
 * @description: 数据汇总返回
 * @author: Simon
 * @create: 2019-07-10
 **/
@Data
@AllArgsConstructor
public class TotalOverViewResp implements Serializable {

    String prefixText;
    BigDecimal data;

}
