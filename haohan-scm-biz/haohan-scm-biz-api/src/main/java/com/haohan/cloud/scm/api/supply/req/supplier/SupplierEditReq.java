package com.haohan.cloud.scm.api.supply.req.supplier;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/6
 * 新增 firstGroup
 * 修改 secondGroup
 */
@Data
public class SupplierEditReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "供应商id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "供应商id的长度最大为32字符")
    @ApiModelProperty(value = "供应商id")
    private String id;

    @NotBlank(message = "商家ID不能为空")
    @Length(min = 0, max = 64, message = "商家ID长度必须介于 0 和 64 之间")
    private String merchantId;

    @NotBlank(message = "供应商全称不能为空")
    @Length(min = 0, max = 64, message = "供应商全称长度必须介于 0 和 64 之间")
    private String supplierName;

    @Length(min = 0, max = 64, message = "供应商简称长度必须介于 0 和 64 之间")
    private String shortName;

    @NotBlank(message = "联系人不能为空")
    @Length(min = 0, max = 64, message = "联系人长度必须介于 0 和 64 之间")
    private String contact;

    @NotBlank(message = "电话不能为空")
    @Length(min = 0, max = 15, message = "电话长度必须介于 0 和 15 之间")
    private String telephone;

    @NotBlank(message = "供应商地址不能为空")
    @Length(min = 0, max = 64, message = "供应商地址长度必须介于 0 和 64 之间")
    private String address;

    @NotNull(message = "账期不能为空")
    @ApiModelProperty(value = "账期")
    private PayPeriodEnum payPeriod;

    /**
     * 账期日：  设置账期对应的结算日，使用数字，最大2位
     */
    @Length(min = 0, max = 2, message = "账期日长度必须介于 0 和 2 之间")
    private String payDay;

    @Length(min = 0, max = 255, message = "标签长度必须介于 0 和 255 之间")
    private String tags;

    @ApiModelProperty(value = "启用状态")
    private UseStatusEnum status;

    @ApiModelProperty(value = "供应商类型")
    private PdsSupplierTypeEnum supplierType;

    @ApiModelProperty(value = "是否开启消息推送")
    private YesNoEnum needPush;

    @Length(min = 0, max = 5, message = "排序值长度必须介于 0 和 5 之间")
    private String sort;

    @ApiModelProperty(value = "评级")
    private GradeTypeEnum supplierLevel;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "供应商地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @Length(min = 0, max = 64, message = "地址定位长度在0至64之间")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @Length(min = 0, max = 32, message = "地址定位区域长度在0至32之间")
    @ApiModelProperty(value = "地址定位区域")
    private String area;

    @Length(min = 0, max = 255, message = "备注长度在0至255之间")
    private String remarks;

    // 扩展

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "角色")
    private String roleIds;

    public Supplier transTo() {
        Supplier supplier = new Supplier();
        supplier.setId(this.id);
        supplier.setMerchantId(this.merchantId);
        supplier.setSupplierName(this.supplierName);
        supplier.setShortName(this.shortName);
        supplier.setContact(this.contact);
        supplier.setTelephone(this.telephone);
        supplier.setAddress(this.address);
        supplier.setPayPeriod(this.payPeriod);
        // 账期日设置 1-31
        String payDay = StrUtil.blankToDefault(this.payDay, "1");
        int num;
        try {
            num = Math.max(1, NumberUtil.parseInt(payDay));
        } catch (Exception e) {
            num = 1;
        }
        num = Math.min(num, 31);
        supplier.setPayDay(String.valueOf(num));
        supplier.setTags(this.tags);
        supplier.setStatus(null == this.status ? UseStatusEnum.enabled : this.status);
        supplier.setSupplierType(null == this.supplierType ? PdsSupplierTypeEnum.ordinary : this.supplierType);
        supplier.setNeedPush(null == this.needPush ? YesNoEnum.no : this.needPush);
        supplier.setSort(StrUtil.blankToDefault(this.sort, "100"));
        supplier.setSupplierLevel(null == this.supplierLevel ? GradeTypeEnum.threeRank : this.supplierLevel);
        // 定位处理
        if (StrUtil.isNotEmpty(this.position)) {
            String[] post = StrUtil.split(this.position, StrUtil.COMMA);
            supplier.setLongitude(post[0]);
            supplier.setLatitude(post[1]);
        }
        supplier.setArea(this.area);
        supplier.setRemarks(this.remarks);
        return supplier;
    }
}
