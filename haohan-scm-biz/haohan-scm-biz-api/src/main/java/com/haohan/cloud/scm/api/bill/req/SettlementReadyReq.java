package com.haohan.cloud.scm.api.bill.req;

import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/12/28
 * 结算单准备/撤销结算
 * 准备结算 default
 * 撤销结算 SecondGroup
 */
@Data
public class SettlementReadyReq {

    @NotBlank(message = "结算单编号不能为空")
    @Length(max = 32, message = "结算单编号的最大长度为32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @NotNull(message = "结算金额不能为空", groups = {FirstGroup.class})
    @Digits(integer = 8, fraction = 2, message = "结算金额的整数位最大8位, 小数位最大2位")
    private BigDecimal settlementAmount;

}
