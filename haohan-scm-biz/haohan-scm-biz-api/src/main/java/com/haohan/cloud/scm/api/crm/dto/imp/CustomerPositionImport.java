package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class CustomerPositionImport {

    @NotBlank(message = "客户名称不能为空")
    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Pattern(regexp = ScmCommonConstant.LONGITUDE_PATTEN, message = "经度格式有误, 正负180度之间, 弧度数表示")
    @ApiModelProperty(value = "经度")
    private String longitude;

    @Pattern(regexp = ScmCommonConstant.LATITUDE_PATTEN, message = "纬度格式有误, 正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "纬度")
    private String latitude;


}
