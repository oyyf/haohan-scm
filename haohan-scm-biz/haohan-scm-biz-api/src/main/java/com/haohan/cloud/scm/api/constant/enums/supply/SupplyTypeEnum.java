package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/20
 * 供应类型
 */
@Getter
@AllArgsConstructor
public enum SupplyTypeEnum {
    /**
     * 供应类型:普通零售
     */
    ordinaryRetail("0", "普通零售"),
    /**
     * 供应类型:协议零售
     */
    retailAgreement("1", "协议零售"),
    /**
     * 供应类型:协议总价
     */
    agreementPrice("2", "协议总价");

    private static final Map<String, SupplyTypeEnum> MAP = new HashMap<>(8);

    static {
        for (SupplyTypeEnum e : SupplyTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SupplyTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
