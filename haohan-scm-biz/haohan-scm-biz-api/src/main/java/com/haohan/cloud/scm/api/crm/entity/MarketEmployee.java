/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 市场部员工
 *
 * @author haohan
 * @date 2019-08-30 12:00:04
 */
@Data
@TableName("crm_market_employee")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "市场部员工")
public class MarketEmployee extends Model<MarketEmployee> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 通行证ID
     */
    @ApiModelProperty(value = "通行证ID")
    private String passportId;
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    private String telephone;
    /**
     * 市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人
     */
    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;
    /**
     * 启用状态 0.未启用 1.启用 2.待审核
     */
    @ApiModelProperty(value = "启用状态 0.未启用 1.启用 2.待审核")
    private UseStatusEnum useStatus;
    /**
     * 默认分润比例
     */
    @ApiModelProperty(value = "默认分润比例")
    private BigDecimal defaultRate;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 性别:1男2女
     */
    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;
    /**
     * 职位
     */
    @ApiModelProperty(value = "职位")
    private String post;
    /**
     * 部门id
     */
    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @Length(max = 255, message = "头像图片地址的长度最大为255字符")
    @ApiModelProperty(value = "头像图片地址")
    private String avatar;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
