package com.haohan.cloud.scm.api.wechat.req;

import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/5/20
 */
@Data
public class WxAdvanceBillReq {

    @NotBlank(message = "通行证id不能为空")
    @Length(max = 64, message = "通行证id长度最大64字符")
    @ApiModelProperty(value = "通行证id")
    private String uid;

    @NotBlank(message = "订单编号不能为空")
    @Length(max = 32, message = "订单编号最大长度32字符")
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @NotNull(message = "预付金额不能为空")
    @Digits(integer = 8, fraction = 2, message = "预付金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "预付金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "账单类型")
    private BillTypeEnum billType;

    public BillInfoDTO transTo() {
        BillInfoDTO dto = new BillInfoDTO();
        dto.setOrderSn(this.orderSn);
        dto.setAdvanceAmount(this.advanceAmount);
        dto.setBillType(this.billType);
        return dto;
    }
}
