package com.haohan.cloud.scm.api.opc.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/7/16
 */
@Data
@ApiModel("生成采购单和汇总单")
public class CreatePurchaseOrderWithSummaryReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;

    @NotEmpty(message = "summaryList不能为空")
    @ApiModelProperty(value = "汇总需求信息列表",required = true)
    private List<CreateSummaryOrderReq> summaryList;


}
