/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.ProductAttrName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品库属性名
 *
 * @author haohan
 * @date 2019-05-28 19:57:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品库属性名")
public class ProductAttrNameReq extends ProductAttrName {

    private long pageSize;
    private long pageNo;




}
