/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerAddress;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户地址
 *
 * @author haohan
 * @date 2019-08-30 11:45:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户地址")
public class CustomerAddressReq extends CustomerAddress {

    private long pageSize;
    private long pageNo;


}
