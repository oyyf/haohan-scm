package com.haohan.cloud.scm.api.opc.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/3
 */
@Data
public class FormulaReq {

    /**
     * 商品ID
     */
    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;
    /**
     * 商品需求数量
     */
    @NotNull(message = "needBuyNum不能为空")
    private BigDecimal needBuyNum;

    /**
     * 采购方式类型
     */
    private String methodType;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 采购价
     */
    private BigDecimal buyPrice;
    /**
     * 竞价截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime biddingEndTime;

}
