package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/7/8
 */
@Data
@ApiModel("出库明细汇总列表结果")
public class ExitDetailInfoResp {

    private String goodsModelId;

    private BigDecimal productNumber;

    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 送货批次:0第一批1第二批
     */
    private BuySeqEnum deliverySeq;

    private List<ExitWarehouseDetail> detailList;

    private GoodsModelDTO goodsModel;


}
