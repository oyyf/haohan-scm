package com.haohan.cloud.scm.api.crm.req.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/12/17
 */
@Data
@ApiModel("修改密码")
public class CrmPasswordModifyReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "原密码不能为空")
    @Length(min = 6, max = 20, message = "原密码长度6-20字符")
    @ApiModelProperty("原密码")
    private String password;

    @NotBlank(message = "新密码不能为空")
    @Length(min = 6, max = 20, message = "新密码长度6-20字符")
    @ApiModelProperty("新密码")
    private String newPassword;

}
