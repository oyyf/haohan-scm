package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author xwx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "新增出库单 根据出库单明细")
public class CreateExitWarehouseReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotEmpty(message = "ids出库单明细编号数组不能为空")
    @ApiModelProperty(value = "出库单明细编号数组", required = true)
    private String[] ids;

    @ApiModelProperty(value = "出库单类型:1.配送2.加工")
    private String exitType;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseSn;

    @ApiModelProperty(value = "出库申请人")
    private String applicantId;

    @ApiModelProperty(value = "出库申请人名称")
    private String applicantName;
}
