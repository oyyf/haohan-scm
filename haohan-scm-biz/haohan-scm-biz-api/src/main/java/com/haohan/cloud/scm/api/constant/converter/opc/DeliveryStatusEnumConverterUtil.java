package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.DeliveryStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class DeliveryStatusEnumConverterUtil implements Converter<DeliveryStatusEnum> {
    @Override
    public DeliveryStatusEnum convert(Object o, DeliveryStatusEnum deliveryStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DeliveryStatusEnum.getByType(o.toString());
    }
}
