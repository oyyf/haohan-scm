package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.PdsSummaryStatusEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class PdsSummaryStatusEnumConverterUtil implements Converter<PdsSummaryStatusEnum> {

    @Override
    public PdsSummaryStatusEnum convert(Object o, PdsSummaryStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsSummaryStatusEnum.getByType(o.toString());
    }

}
