/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.SortingOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 分拣单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SortingOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface SortingOrderDetailFeignService {


    /**
     * 通过id查询分拣单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SortingOrderDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 分拣单明细 列表信息
     * @param sortingOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SortingOrderDetail/fetchSortingOrderDetailPage")
    R getSortingOrderDetailPage(@RequestBody SortingOrderDetailReq sortingOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 分拣单明细 列表信息
     * @param sortingOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SortingOrderDetail/fetchSortingOrderDetailList")
    R getSortingOrderDetailList(@RequestBody SortingOrderDetailReq sortingOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/add")
    R save(@RequestBody SortingOrderDetail sortingOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/update")
    R updateById(@RequestBody SortingOrderDetail sortingOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除分拣单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SortingOrderDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/countBySortingOrderDetailReq")
    R countBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/getOneBySortingOrderDetailReq")
    R getOneBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param sortingOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SortingOrderDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SortingOrderDetail> sortingOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改分拣单明细 根据B订单明细编号
     * @param sortingOrderDetail 必须 pmId/ BuyDetailSn
     */
    @PostMapping("/api/feign/SortingOrderDetail/modifyBySn")
    R<Boolean> modifySortingDetailBySn(@RequestBody SortingOrderDetail sortingOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);
}
