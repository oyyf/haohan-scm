/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.GoodsModelFeignReq;
import com.haohan.cloud.scm.api.goods.req.GoodsModelReq;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品规格内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsModelFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsModelFeignService {


    /**
     * 通过id查询商品规格 (不带平台定价)
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsModel/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改商品规格
     *
     * @param goodsModel 商品规格
     * @return R
     */
    @PostMapping("/api/feign/GoodsModel/update")
    R updateById(@RequestBody GoodsModel goodsModel, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsModel/countByGoodsModelReq")
    R countByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录  (不带平台定价)
     *
     * @param goodsModelReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsModel/getOneByGoodsModelReq")
    R getOneByGoodsModelReq(@RequestBody GoodsModelReq goodsModelReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 通过id查询商品规格   (不带平台定价)
     * 关联 goods / goodsCategory
     * 返回带 商品名称/ 商品分类名称
     *
     * @param id id
     * @return R<GoodsModelDTO>
     */
    @RequestMapping(value = "/api/feign/GoodsModel/info/{id}", method = RequestMethod.GET)
    R<GoodsModelDTO> getInfoById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据sn查询商品规格信息   (不带平台定价)
     *
     * @return 返回可为null
     */
    @RequestMapping(value = "/api/feign/GoodsModel/infoBySn/{modelSn}", method = RequestMethod.GET)
    R<GoodsModelDTO> getInfoBySn(@PathVariable("modelSn") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询商品规格信息  根据商品名称 规格名称 规格单位  (使用平台定价)
     * 优先 规格名称匹配 无时 匹配 规格单位
     * 都无 或多条匹配时 返回null
     *
     * @param query 必须shopId goodsName
     * @return
     */
    @PostMapping(value = "/api/feign/GoodsModel/fetchGoodsModelByName")
    R<GoodsModelDTO> fetchGoodsModelByName(@RequestBody GoodsModelDTO query, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 返回商品排行
     *
     * @param from
     * @return
     */
    @GetMapping(value = "/api/feign/GoodsModel/getStorageRank")
    R getStorageRank(@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询商品库存预警数量
     *
     * @param from
     * @return
     */
    @GetMapping(value = "/api/feign/GoodsModel/queryStorageWarn")
    R queryStorageWarn(@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 名称查询, 若无对应商品则新增 商品(下架状态, 分类为  未确认商品-> 导入商品)
     * (使用平台定价)
     *
     * @param goodsModelDTO goodsName / modelName /unit /price / shopId
     * @param from
     * @return
     */
    @PostMapping(value = "/api/feign/GoodsModel/fetchModelByNameOrAdd")
    R<GoodsModelDTO> fetchModelByNameOrAdd(@RequestBody GoodsModelDTO goodsModelDTO, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询分类及子分类 的商品规格id列表
     *
     * @param categoryId
     * @param from
     * @return
     */
    @RequestMapping(value = "/api/feign/GoodsModel/fetchListByCategory/{categoryId}", method = RequestMethod.GET)
    R<List<String>> fetchModelListByCategory(@PathVariable("categoryId") String categoryId, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据 moderId 带 merchantId/shopId
     * (不带平台定价)
     *
     * @param req
     * @param from
     * @return 可null
     */
    @PostMapping(value = "/api/feign/GoodsModel/fetchInfo")
    R<GoodsModelDTO> fetchInfo(@RequestBody GoodsModelFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据 modelIdSet 查询规格列表  (不带平台定价)
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping(value = "/api/feign/GoodsModel/findGoodsModelList")
    R<List<GoodsModelDTO>> findGoodsModelList(@RequestBody GoodsModelFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 规格库存扣减
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping(value = "/api/feign/GoodsModel/modelStorageSubtract")
    R<Boolean> modelStorageSubtract(@RequestBody GoodsModelFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 根据id查询商品规格信息  (使用平台定价)
     * 无typeName
     *
     * @param req 必须 modelId, 可选buyerId/pricingMerchantId, pricingDate
     * @param from
     * @return
     */
    @PostMapping(value = "/api/feign/GoodsModel/fetchModelVO")
    R<GoodsModelVO> fetchModelVO(@RequestBody GoodsModelFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
