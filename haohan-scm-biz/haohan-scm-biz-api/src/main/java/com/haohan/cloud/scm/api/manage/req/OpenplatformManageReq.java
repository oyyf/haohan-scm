/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.OpenplatformManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 开放平台应用资料管理
 *
 * @author haohan
 * @date 2019-05-28 20:35:50
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "开放平台应用资料管理")
public class OpenplatformManageReq extends OpenplatformManage {

    private long pageSize;
    private long pageNo;




}
