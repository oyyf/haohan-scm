package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.constant.enums.opc.AllocateTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.SummaryStatusEnum;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/7/16
 * 必需参数: pmId/buySeq/goodsModelId/deliveryTime/allocateType
 */
@Data
@ApiModel("创建汇总单 根据商品需求")
public class CreateSummaryOrderReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotNull(message = "buySeq不能为空")
    @ApiModelProperty(value = "配送批次", required = true)
    private BuySeqEnum buySeq;

    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id", required = true)
    private String goodsModelId;

    @NotNull(message = "deliveryTime不能为空")
    @ApiModelProperty(value = "配送时间", required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;

    @ApiModelProperty(value = "调配类型")
    private AllocateTypeEnum allocateType;

    @ApiModelProperty(value = "汇总单状态:1.待处理2.采购中3.配送中.4已完成5.已关闭")
    private SummaryStatusEnum summaryStatus;

    @ApiModelProperty(value = "汇总单主键")
    private String id;

    @ApiModelProperty(value = "需求采购数量")
    private BigDecimal needBuyNum;

    public SummaryOrder transTo(){
        SummaryOrder summaryOrder = new SummaryOrder();
        summaryOrder.setPmId(this.pmId);
        summaryOrder.setBuySeq(this.buySeq);
        summaryOrder.setGoodsModelId(this.goodsModelId);
        summaryOrder.setDeliveryTime(this.deliveryTime);
        if(null == this.allocateType){
            this.allocateType = AllocateTypeEnum.purchase;
        }
        summaryOrder.setAllocateType(this.allocateType);
        summaryOrder.setSummaryStatus(this.summaryStatus);
        summaryOrder.setId(this.id);
        summaryOrder.setNeedBuyNum(this.needBuyNum);
        return summaryOrder;
    }

}
