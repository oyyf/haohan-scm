package com.haohan.cloud.scm.api.goods.req.imp;

import com.haohan.cloud.scm.api.goods.dto.imp.GoodsImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/16
 */
@Data
public class GoodsImportReq {

    @NotBlank(message = "店铺id不能为空")
    @Length(min = 0, max = 32, message = "店铺id的字符长度必须在1至32之间")
    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @NotEmpty(message = "商品分类列表不能为空")
    private List<GoodsImport> goodsList;

}
