package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.MarketTaskTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class MarketTaskTypeEnumConverterUtil implements Converter<MarketTaskTypeEnum> {
    @Override
    public MarketTaskTypeEnum convert(Object o, MarketTaskTypeEnum marketTaskTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MarketTaskTypeEnum.getByType(o.toString());
    }
}
