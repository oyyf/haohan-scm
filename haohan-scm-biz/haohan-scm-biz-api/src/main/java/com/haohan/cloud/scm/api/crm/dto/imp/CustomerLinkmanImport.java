package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerLinkman;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class CustomerLinkmanImport {

    @NotBlank(message = "客户名称不能为空")
    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "是否主联系人")
    private YesNoEnum primaryFlag;

    @NotBlank(message = "姓名不能为空")
    @Length(min = 0, max = 10, message = "姓名的字符长度必须在1至10之间")
    @ApiModelProperty(value = "姓名")
    private String name;

    @NotBlank(message = "手机号不能为空")
    @Length(min = 8, max = 15, message = "手机号的字符长度必须在8至15之间")
    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @Length(min = 0, max = 20, message = "部门的字符长度必须在0至20之间")
    @ApiModelProperty(value = "部门")
    private String department;

    @Length(min = 0, max = 20, message = "职位的字符长度必须在0至20之间")
    @ApiModelProperty(value = "职位")
    private String position;

    @Length(min = 0, max = 64, message = "邮箱的字符长度必须在0至64之间")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Length(min = 0, max = 64, message = "联系地址的字符长度必须在0至64之间")
    @ApiModelProperty(value = "联系地址")
    private String address;

    @Length(min = 0, max = 255, message = "描述的字符长度必须在0至255之间")
    @ApiModelProperty(value = "描述")
    private String linkmanDesc;

    @Length(min = 0, max = 255, message = "备注信息的字符长度必须在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public CustomerLinkman transTo() {
        CustomerLinkman linkman = new CustomerLinkman();
        // 默认不是主要联系人
        linkman.setPrimaryFlag(null == this.primaryFlag ? YesNoEnum.no : this.primaryFlag);
        linkman.setName(this.name);
        linkman.setTelephone(this.telephone);
        // 默认男
        linkman.setSex(null == this.sex ? SexEnum.man : this.sex);
        linkman.setDepartment(this.department);
        linkman.setPosition(this.position);
        linkman.setEmail(this.email);
        linkman.setAddress(this.address);
        linkman.setLinkmanDesc(this.linkmanDesc);
        linkman.setRemarks(this.remarks);
        return linkman;
    }
}
