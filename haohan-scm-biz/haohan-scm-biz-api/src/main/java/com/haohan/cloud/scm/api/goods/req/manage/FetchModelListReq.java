package com.haohan.cloud.scm.api.goods.req.manage;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/4
 */
@Data
public class FetchModelListReq {

    @NotBlank(message = "店铺id不能为空")
    @Length(max = 32, message = "店铺Id的长度最大32字符")
    @ApiModelProperty(value = "店铺Id")
    private String shopId;

    @Length(max = 32, message = "商品id的长度最大32字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 32, message = "商品规格id的长度最大32字符")
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @Length(max = 10, message = "规格名称的长度最大10字符")
    @ApiModelProperty(value = "规格名称")
    private String modelName;

    @Length(max = 10, message = "规格单位的长度最大10字符")
    @ApiModelProperty(value = "规格单位")
    private String modelUnit;

    @Length(max = 32, message = "扫码购编码的长度最大32字符")
    @ApiModelProperty(value = "扫码购编码")
    private String modelCode;

    @Length(max = 32, message = "商品编号的长度最大32字符")
    @ApiModelProperty(value = "商品编号")
    private String goodsSn;

    @ApiModelProperty(value = "上下架状态  0否1是")
    private YesNoEnum marketableFlag;

    @Length(max = 10, message = "商品名称的长度最大10字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 32, message = "商品分类Id的长度最大32字符")
    @ApiModelProperty(value = "商品分类Id")
    private String categoryId;

    @Length(max = 10, message = "商品分类名称的长度最大10字符")
    @ApiModelProperty(value = "商品分类名称")
    private String categoryName;

    // 扩展

    @Length(max = 32, message = "采购商id的长度最大32字符")
    @ApiModelProperty("采购商id,用于联查平台商品定价")
    private String buyerId;

    @Length(max = 32, message = "平台商品定价商家id的长度最大32字符")
    @ApiModelProperty("平台商品定价商家id")
    private String pricingMerchantId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("平台商品定价查询日期")
    private LocalDate pricingDate;

    public GoodsModelDTO transTo() {
        GoodsModelDTO model = new GoodsModelDTO();
        model.setShopId(this.shopId);
        model.setGoodsId(this.goodsId);
        model.setId(this.goodsModelId);
        model.setModelName(this.modelName);
        model.setModelUnit(this.modelUnit);
        model.setModelCode(this.modelCode);
        model.setGoodsSn(this.goodsSn);
        if (null != this.marketableFlag) {
            model.setIsMarketable(Integer.valueOf(this.marketableFlag.getType()));
        }
        model.setGoodsName(this.goodsName);
        model.setGoodsCategoryId(this.categoryId);
        model.setCategoryName(this.categoryName);
        return model;
    }
}
