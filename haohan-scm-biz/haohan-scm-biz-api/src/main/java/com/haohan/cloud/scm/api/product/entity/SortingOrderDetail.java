/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-13 18:15:00
 */
@Data
@TableName("scm_pws_sorting_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分拣单明细")
public class SortingOrderDetail extends Model<SortingOrderDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 分拣单编号
     */
    private String sortingOrderSn;
    /**
     * 分拣明细编号
     */
    private String sortingDetailSn;
    /**
     * 客户采购单明细编号
     */
    private String buyDetailSn;
    /**
     * 采购商品规格id
     */
    private String goodsModelId;
    /**
     * 采购商品名称
     */
    private String goodsName;
    /**
     * 采购商品规格名称
     */
    private String goodsModelName;
    /**
     * 采购商品单位
     */
    private String goodsUnit;
    /**
     * 采购数量
     */
    private BigDecimal purchaseNumber;
    /**
     * 货品编号
     */
    private String productSn;
    /**
     * 货品名称
     */
    private String productName;
    /**
     * 货品单位
     */
    private String productUnit;
    /**
     * 货品分拣数量
     */
    private BigDecimal sortingNumber;
    /**
     * 分拣时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sortingTime;
    /**
     * 分拣人id
     */
    private String operatorId;
    /**
     * 分拣人名称
     */
    private String operatorName;
    /**
     * 分拣状态:1待分拣2.可分拣3已分拣4已完成
     */
    private SortingStatusEnum sortingStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
