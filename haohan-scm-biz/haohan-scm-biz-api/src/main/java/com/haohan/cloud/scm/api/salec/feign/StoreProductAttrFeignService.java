/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreProductAttr;
import com.haohan.cloud.scm.api.salec.req.StoreProductAttrReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品属性表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreProductAttrFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreProductAttrFeignService {


  /**
   * 通过id查询商品属性表
   *
   * @param productId id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreProductAttr/{productId}", method = RequestMethod.GET)
  R<StoreProductAttr> getById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 商品属性表 列表信息
   *
   * @param storeProductAttrReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttr/fetchStoreProductAttrPage")
  R<Page<StoreProductAttr>> getStoreProductAttrPage(@RequestBody StoreProductAttrReq storeProductAttrReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 商品属性表 列表信息
   *
   * @param storeProductAttrReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreProductAttr/fetchStoreProductAttrList")
  R<List<StoreProductAttr>> getStoreProductAttrList(@RequestBody StoreProductAttrReq storeProductAttrReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增商品属性表
   *
   * @param storeProductAttr 商品属性表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/add")
  R<Boolean> save(@RequestBody StoreProductAttr storeProductAttr, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改商品属性表
   *
   * @param storeProductAttr 商品属性表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/update")
  R<Boolean> updateById(@RequestBody StoreProductAttr storeProductAttr, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除商品属性表
   *
   * @param productId id
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/delete/{productId}")
  R<Boolean> removeById(@PathVariable("productId") Integer productId, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/listByIds")
  R<List<StoreProductAttr>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeProductAttrReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/countByStoreProductAttrReq")
  R<Integer> countByStoreProductAttrReq(@RequestBody StoreProductAttrReq storeProductAttrReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeProductAttrReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/getOneByStoreProductAttrReq")
  R<StoreProductAttr> getOneByStoreProductAttrReq(@RequestBody StoreProductAttrReq storeProductAttrReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeProductAttrList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreProductAttr/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreProductAttr> storeProductAttrList, @RequestHeader(SecurityConstants.FROM) String from);


}
