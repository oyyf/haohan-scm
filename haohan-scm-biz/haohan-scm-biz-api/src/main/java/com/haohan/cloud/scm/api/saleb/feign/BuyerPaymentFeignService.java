/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.req.payment.BuyerPaymentReq;
import com.haohan.cloud.scm.api.opc.req.payment.CountReceivableBillReq;
import com.haohan.cloud.scm.api.opc.req.payment.CreateReceivableReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;



/**
 * @program: haohan-fresh-scm
 * @description: 采购商货款统计内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BuyerPaymentFeignService", value = ScmServiceName.SCM_BIZ_SALEB)
public interface BuyerPaymentFeignService {


//    /**
//     * 通过id查询采购商货款统计
//     * @param id id
//     * @return R
//     */
//    @RequestMapping(value = "/api/feign/BuyerPayment/{id}", method = RequestMethod.GET)
//    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 分页查询 采购商货款统计 列表信息
//     * @param buyerPaymentReq 请求对象
//     * @return
//     */
//    @PostMapping("/api/feign/BuyerPayment/fetchBuyerPaymentPage")
//    R getBuyerPaymentPage(@RequestBody BuyerPaymentReq buyerPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购商货款统计 列表信息
     * @param buyerPaymentReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/BuyerPayment/fetchBuyerPaymentList")
    R getBuyerPaymentList(@RequestBody BuyerPaymentReq buyerPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 新增采购商货款统计
//     * @param buyerPayment 采购商货款统计
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/add")
//    R save(@RequestBody BuyerPayment buyerPayment, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 修改采购商货款统计
//     * @param buyerPayment 采购商货款统计
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/update")
//    R updateById(@RequestBody BuyerPayment buyerPayment, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 通过id删除采购商货款统计
//     * @param id id
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/delete/{id}")
//    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//   * 删除（根据ID 批量删除)
//   *
//   * @param idList 主键ID列表
//   * @return R
//   */
//    @PostMapping("/api/feign/BuyerPayment/batchDelete")
//    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量查询（根据IDS）
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/listByIds")
//    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     *
//     * @param buyerPaymentReq 实体对象,可以为空
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/countByBuyerPaymentReq")
//    R countByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录 (bill对应接口完成)
     *
     * @param buyerPaymentReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/BuyerPayment/getOneByBuyerPaymentReq")
    R getOneByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量修改OR插入
//     *
//     * @param buyerPaymentList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @PostMapping("/api/feign/BuyerPayment/saveOrUpdateBatch")
//    R saveOrUpdateBatch(@RequestBody List<BuyerPayment> buyerPaymentList, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 查询应收账单 是否通过审核  带商家名称
//     * @param buyerPayment 必需 pmId / BuyerPaymentId
//     * @return 不为确认状态时 返回null
//     */
//    @PostMapping("/api/feign/BuyerPayment/checkPayment")
//    R<BuyerPayment> checkPayment(@RequestBody BuyerPayment buyerPayment, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询到期的应收账单数
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/BuyerPayment/countBill")
    R<Integer> countReceivableBill(@RequestBody CountReceivableBillReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 创建应收账单 (bill对应接口完成)
     * @param req 必需:pmId/receivableSn/billType
     * @param from
     * @return
     */
    @PostMapping("/api/feign/BuyerPayment/createReceivable")
    R<BuyerPayment> createReceivable(@RequestBody CreateReceivableReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
