package com.haohan.cloud.scm.api.crm.vo.app;

import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/18
 * 客户欠款
 */
@Data
@NoArgsConstructor
public class CustomerDebtVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "待收金额")
    private BigDecimal debtAmount;

    public CustomerDebtVO(BillInfoDTO bill) {
        this.customerSn = bill.getCustomerId();
        this.customerName = bill.getCustomerName();
        this.debtAmount = bill.getBillAmount();
    }

}
