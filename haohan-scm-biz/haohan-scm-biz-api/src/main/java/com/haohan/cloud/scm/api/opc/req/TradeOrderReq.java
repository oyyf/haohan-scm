/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 交易订单
 *
 * @author haohan
 * @date 2019-05-30 10:21:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "交易订单")
public class TradeOrderReq extends TradeOrder {

    private long pageSize;
    private long pageNo;




}
