package com.haohan.cloud.scm.api.saleb.dto;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/13
 */
@Data
@ApiModel("导入的采购单")
public class BuyOrderImportDTO {


    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 采购商名称
     */
    private String buyerName;
    /**
     * 采购用户
     */
    private String buyerUid;

    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;
    /**
     * 送货批次
     */
    private BuySeqEnum buySeq;
    /**
     * 采购需求
     */
    private String needNote;

    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 配送地址
     */
    private String address;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 运费
     */
    private BigDecimal shipFee;
    /**
     * 配送方式
     */
    private DeliveryTypeEnum deliveryType;

    /**
     * 采购单明细
     */
    private List<BuyOrderDetail> detailList;


}
