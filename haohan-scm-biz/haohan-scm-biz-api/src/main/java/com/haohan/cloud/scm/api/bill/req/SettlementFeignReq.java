package com.haohan.cloud.scm.api.bill.req;

import com.haohan.cloud.scm.api.bill.dto.SettlementDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/12/28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SettlementFeignReq extends SettlementReq {

    /**
     * 分页参数 当前页
     */
    private long current = 1;
    /**
     * 分页参数 每页显示条数
     */
    private long size = 10;

    @Length(max = 32, message = "随机码的最大长度为32字符")
    @ApiModelProperty(value = "开始结算状态随机码（验证是否此次结算）")
    private String randomCode;

    @ApiModelProperty(value = "结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    public SettlementDTO transToDTO() {
        SettlementDTO settle = new SettlementDTO();
        settle.setSettlementSn(this.getSettlementSn());
        settle.setCompanyOperator(this.getCompanyOperator());
        settle.setOperatorName(this.getOperatorName());
        settle.setRandomCode(this.randomCode);
        settle.setPayType(this.getPayType());
        settle.setSettlementTime(this.settlementTime);
        settle.setSettlementDesc(this.getSettlementDesc());
        settle.setPhotoList(this.photoList);
        return settle;
    }

}
