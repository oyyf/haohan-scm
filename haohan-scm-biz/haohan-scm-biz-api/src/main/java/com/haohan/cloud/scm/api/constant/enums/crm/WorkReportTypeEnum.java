package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/27
 */
@Getter
@AllArgsConstructor
public enum WorkReportTypeEnum implements IBaseEnum {

    /**
     * 工作汇报类型: 1.日常 2.销量 3.库存
     */
    daily("1", "日常"),
    sales("2", "销量"),
    stock("3", "库存");

    private static final Map<String, WorkReportTypeEnum> MAP = new HashMap<>(8);

    static {
        for (WorkReportTypeEnum e : WorkReportTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static WorkReportTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
