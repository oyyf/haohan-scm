package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.StorageStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class StoragePlaceTypeEnumConverterUtil implements Converter<StorageStatusEnum> {
    @Override
    public StorageStatusEnum convert(Object o, StorageStatusEnum storagePlaceTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return StorageStatusEnum.getByType(o.toString());
    }
}
