/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.HomeDeliveryRecord;
import com.haohan.cloud.scm.api.crm.req.HomeDeliveryRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 送货上门服务记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "HomeDeliveryRecordFeignService", value = ScmServiceName.SCM_CRM)
public interface HomeDeliveryRecordFeignService {


    /**
     * 通过id查询送货上门服务记录表
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/HomeDeliveryRecord/{id}", method = RequestMethod.GET)
    R<HomeDeliveryRecord> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 送货上门服务记录表 列表信息
     *
     * @param homeDeliveryRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/fetchHomeDeliveryRecordPage")
    R<Page<HomeDeliveryRecord>> getHomeDeliveryRecordPage(@RequestBody HomeDeliveryRecordReq homeDeliveryRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 送货上门服务记录表 列表信息
     *
     * @param homeDeliveryRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/fetchHomeDeliveryRecordList")
    R<List<HomeDeliveryRecord>> getHomeDeliveryRecordList(@RequestBody HomeDeliveryRecordReq homeDeliveryRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增送货上门服务记录表
     *
     * @param homeDeliveryRecord 送货上门服务记录表
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/add")
    R<Boolean> save(@RequestBody HomeDeliveryRecord homeDeliveryRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改送货上门服务记录表
     *
     * @param homeDeliveryRecord 送货上门服务记录表
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/update")
    R<Boolean> updateById(@RequestBody HomeDeliveryRecord homeDeliveryRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除送货上门服务记录表
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/listByIds")
    R<List<HomeDeliveryRecord>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param homeDeliveryRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/countByHomeDeliveryRecordReq")
    R<Integer> countByHomeDeliveryRecordReq(@RequestBody HomeDeliveryRecordReq homeDeliveryRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param homeDeliveryRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/getOneByHomeDeliveryRecordReq")
    R<HomeDeliveryRecord> getOneByHomeDeliveryRecordReq(@RequestBody HomeDeliveryRecordReq homeDeliveryRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param homeDeliveryRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/HomeDeliveryRecord/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<HomeDeliveryRecord> homeDeliveryRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
