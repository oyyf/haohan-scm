/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门关系表
 *
 * @author haohan
 * @date 2019-08-30 11:59:56
 */
@Data
@TableName("crm_tree_relation")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "部门关系表")
public class TreeRelation extends Model<TreeRelation> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 祖先节点
     */
    @ApiModelProperty(value = "祖先节点")
    private String ancestor;
    /**
     * 后代节点
     */
    @ApiModelProperty(value = "后代节点")
    private String descendant;
    /**
     * 类名
     */
    @ApiModelProperty(value = "类名")
    private String className;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;

}
