/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:13:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商货款统计")
public class SupplierPaymentReq extends SupplierPayment {

    private long pageSize=10;
    private long pageNo=1;

    /**
     * 应收账单 商家id
     */
    private String merchantId;

    private String uId;

    /**
     * 应收账单 商家名称
     */
    private String merchantName;

}
