package com.haohan.cloud.scm.api.supply.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/7
 * 查询B客户采购的关联供应订单信息 订单明细
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplyRelationBuyOrderDetailVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "采购单编号")
    private String buyOrderSn;

    @ApiModelProperty(value = "采购明细编号")
    private String buyDetailSn;

    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;

    @ApiModelProperty(value = "供应订单明细编号")
    private String supplyDetailSn;

}
