package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/20
 */
@Data
@ApiModel(description = "查询请款记录详情需求")
public class PurchaseQueryLendingReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;

    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id",required = true)
    private String uId;

    @NotBlank(message = "LendingId不能为空")
    @ApiModelProperty(value = "请款记录id",required = true)
    private String lendingId;
}
