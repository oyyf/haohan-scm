/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 入库单明细
 *
 * @author haohan
 * @date 2019-05-28 19:04:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "入库单明细")
public class EnterWarehouseDetailReq extends EnterWarehouseDetail {

    private long pageSize=10;
    private long pageNo=1;
//    @NotBlank(message = "uId不能为空")
//    private String uid;
    @NotBlank(message = "pmId不能为空")
    private String pmId;


}
