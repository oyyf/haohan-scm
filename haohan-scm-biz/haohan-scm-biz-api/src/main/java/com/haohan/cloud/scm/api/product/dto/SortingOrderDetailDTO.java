package com.haohan.cloud.scm.api.product.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/7/8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分拣单明细及分拣单属性")
public class SortingOrderDetailDTO extends Model<SortingOrderDetailDTO> {
    private static final long serialVersionUID = 1L;

    // 分拣单明细属性

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 分拣单编号
     */
    private String sortingOrderSn;
    /**
     * 分拣明细编号
     */
    private String sortingDetailSn;
    /**
     * 客户采购单明细编号
     */
    private String buyDetailSn;
    /**
     * 采购商品规格id
     */
    private String goodsModelId;
    /**
     * 采购商品名称
     */
    private String goodsName;
    /**
     * 采购商品规格名称
     */
    private String goodsModelName;
    /**
     * 采购商品单位
     */
    private String goodsUnit;
    /**
     * 采购数量
     */
    private BigDecimal purchaseNumber;
    /**
     * 货品编号
     */
    private String productSn;
    /**
     * 货品名称
     */
    private String productName;
    /**
     * 货品单位
     */
    private String productUnit;
    /**
     * 货品分拣数量
     */
    private BigDecimal sortingNumber;
    /**
     * 分拣时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sortingTime;
    /**
     * 分拣人id
     */
    private String operatorId;
    /**
     * 分拣人名称
     */
    private String operatorName;
    /**
     * 分拣状态:1待分拣2.可分拣3已分拣4已完成
     */
    private String sortingStatus;
    /**
     * 备注信息
     */
    private String remarks;

    // 分拣单属性

    /**
     * 客户采购单编号
     */
    private String buyId;
    /**
     * 客户id
     */
    private String buyerId;
    /**
     * 客户名称
     */
    private String buyerName;
    /**
     * 分拣点编号
     */
    private String sortingPlaceSn;
    /**
     * 分拣点名称
     */
    private String sortingPlaceName;
    /**
     * 分拣截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadlineTime;

    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 送货批次:0第一批1第二批
     */
    private String deliverySeq;
    /**
     * 装车点编号
     */
    private String deliveryPlaceSn;
    /**
     * 装车点名称
     */
    private String deliveryPlaceName;


}
