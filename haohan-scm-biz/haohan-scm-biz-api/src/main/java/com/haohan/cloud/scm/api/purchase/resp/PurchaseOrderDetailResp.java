package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReviewTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author cx
 * @date 2019/7/17
 */

@Data
public class PurchaseOrderDetailResp {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    private String purchaseSn;


    @NotNull(message = "purchaseOrderType不能为空")
    private PurchaseOrderTypeEnum purchaseOrderType;

    @NotNull(message = "buyFinalTime不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyFinalTime;

    private PurchaseStatusEnum purchaseStatus;

    @NotNull(message = "reviewType不能为空")
    private ReviewTypeEnum reviewType;

    private String initiatorId;

    private String remarks;

    private List<PurchaseDetailCheckResp> detailList;
}
