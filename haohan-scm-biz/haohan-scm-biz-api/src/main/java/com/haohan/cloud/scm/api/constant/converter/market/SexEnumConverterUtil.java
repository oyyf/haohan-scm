package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class SexEnumConverterUtil implements Converter<SexEnum> {
    @Override
    public SexEnum convert(Object o, SexEnum sexEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SexEnum.getByType(o.toString());
    }
}
