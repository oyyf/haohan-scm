/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderReq;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 退货单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ReturnOrderFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface ReturnOrderFeignService {


    /**
     * 通过id查询退货单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ReturnOrder/{id}", method = RequestMethod.GET)
    R<ReturnOrder> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 退货单 列表信息
     * @param returnOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ReturnOrder/fetchReturnOrderPage")
    R<Page<ReturnOrder>> getReturnOrderPage(@RequestBody ReturnOrderReq returnOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 退货单 列表信息
     * @param returnOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ReturnOrder/fetchReturnOrderList")
    R<List<ReturnOrder>> getReturnOrderList(@RequestBody ReturnOrderReq returnOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增退货单
     * @param returnOrder 退货单
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/add")
    R<Boolean> save(@RequestBody ReturnOrder returnOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改退货单
     * @param returnOrder 退货单
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/update")
    R<Boolean> updateById(@RequestBody ReturnOrder returnOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除退货单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ReturnOrder/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/listByIds")
    R<List<ReturnOrder>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/countByReturnOrderReq")
    R<Integer> countByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/getOneByReturnOrderReq")
    R<ReturnOrder> getOneByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param returnOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<ReturnOrder> returnOrderList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询 可退款的退货单  根据pmId/ returnSn
     * @param req pmId/ returnSn
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrder/fetchRefund")
    R<ReturnOrder> fetchRefund(@RequestBody ReturnOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 获取订单详细
     *
     * @param orderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/ReturnOrder/orderInfo")
    R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

}
