/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesRecordReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 售后记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AfterSalesRecordFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface AfterSalesRecordFeignService {


    /**
     * 通过id查询售后记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/AfterSalesRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 售后记录 列表信息
     * @param afterSalesRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AfterSalesRecord/fetchAfterSalesRecordPage")
    R getAfterSalesRecordPage(@RequestBody AfterSalesRecordReq afterSalesRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 售后记录 列表信息
     * @param afterSalesRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AfterSalesRecord/fetchAfterSalesRecordList")
    R getAfterSalesRecordList(@RequestBody AfterSalesRecordReq afterSalesRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/add")
    R save(@RequestBody AfterSalesRecord afterSalesRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/update")
    R updateById(@RequestBody AfterSalesRecord afterSalesRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除售后记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/AfterSalesRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/countByAfterSalesRecordReq")
    R countByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/getOneByAfterSalesRecordReq")
    R getOneByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param afterSalesRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/AfterSalesRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<AfterSalesRecord> afterSalesRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
