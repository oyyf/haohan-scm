package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum SalesStatusEnum {
    /**
     * 销售机会状态:1跟踪2成功3失败4搁置5失效
     */
    tailAfter("1","跟踪"),
    succeed("2","成功"),
    failure("3","失败"),
    shelve("4","搁置"),
    loseEfficacy("5","失效");

    private static final Map<String, SalesStatusEnum> MAP = new HashMap<>(8);

    static {
        for (SalesStatusEnum e : SalesStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SalesStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
