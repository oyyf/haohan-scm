/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req.settlement;

import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 结算单账单关系表
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "结算单账单关系表")
public class SettlementRelationReq extends SettlementRelation {

    private long pageSize;
    private long pageNo;


}
