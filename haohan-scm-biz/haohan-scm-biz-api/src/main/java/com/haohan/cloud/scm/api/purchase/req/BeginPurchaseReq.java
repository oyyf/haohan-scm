package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReviewTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/8/7
 */
@Data
@ApiModel(description = "发起采购")
public class BeginPurchaseReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    @ApiModelProperty(value = "采购单编号", required = true)
    private String purchaseSn;

    /**
     * 审核方式1.不审核2.需审核
     */
    @NotNull(message = "reviewType不能为空")
    @ApiModelProperty(value = "审核方式1.不审核2.需审核", required = true)
    private ReviewTypeEnum reviewType;

    /**
     * 采购截止时间
     */
    @NotNull(message = "buyFinalTime不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyFinalTime;

    /**
     * 采购订单分类:1.采购计划2.按需采购
     */
    @NotNull(message = "purchaseOrderType不能为空")
    @ApiModelProperty(value = "采购订单分类:1.采购计划2.按需采购", required = true)
    private PurchaseOrderTypeEnum purchaseOrderType;

    @ApiModelProperty(value = "采购审核任务执行人id")
    private String transactorId;

}
