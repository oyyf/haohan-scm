package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/17
 */
@Data
@ApiModel(description = "新增供应商 售卖商品")
public class AddSupplierGoodsReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不为空")
    @ApiModelProperty(value = "采购员id" , required = true)
    private String uid;

    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id sku",required = true)
    private String goodsModelId;

    @NotBlank(message = "supplierId不为空")
    @ApiModelProperty(value = "供应商id" , required = true)
    private String supplierId;

    @ApiModelProperty(value = "启用状态:0未启用，1启用")
    private UseStatusEnum status;

    @ApiModelProperty(value = "供应类型:0普通零售1协议零售2协议总价")
    private SupplyTypeEnum supplyType;

}
