package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseOrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReviewTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/4
 */
@Data
@ApiModel(description = "直接新增采购单和采购单明细")
public class AddPurchaseOrderAndDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @Valid
    @NotNull(message = "list不能为空")
    @ApiModelProperty(value = "集合中以商品的goodsModelId和needBuyNum为一个对象", required = true)
    private List<ParamsReq> list;

    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    @NotNull(message = "methodType不能为空")
    @ApiModelProperty(value = "采购方式类型:1.竞价采购2.单品采购3.协议供应", required = true)
    private MethodTypeEnum methodType;

    /**
     * 审核方式1.不审核2.需审核
     */
    @NotNull(message = "reviewType不能为空")
    @ApiModelProperty(value = "审核方式1.不审核2.需审核", required = true)
    private ReviewTypeEnum reviewType;

    /**
     * 采购截止时间
     */
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyFinalTime;

    /**
     * 采购订单分类:1.采购计划2.按需采购
     */
    @NotNull(message = "purchaseOrderType不能为空")
    @ApiModelProperty(value = "采购订单分类:1.采购计划2.按需采购", required = true)
    private PurchaseOrderTypeEnum purchaseOrderType;


    @ApiModelProperty(value = "采购审核任务执行人id")
    private String transactorId;
}
