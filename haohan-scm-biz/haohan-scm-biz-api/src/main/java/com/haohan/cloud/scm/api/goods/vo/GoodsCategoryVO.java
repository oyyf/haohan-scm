package com.haohan.cloud.scm.api.goods.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/5/6
 * 商品分类展示及使用属性(最新)
 */
@Data
@NoArgsConstructor
public class GoodsCategoryVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
    private String categoryId;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 父级编号
     */
    private String parentId;
    /**
     * 名称
     */
    private String categoryName;
    /**
     * 排序
     */
    private BigDecimal sort;
    /**
     * logo地址
     */
    private String logo;
    /**
     * 分类类型 修改为是否展示 0否 1是 (用于分类树中是否展示)
     */
    private YesNoEnum categoryType;
    /**
     * 是否上架  0否 1是 (用于所属商品的批量上下架)
     */
    private YesNoEnum status;
    /**
     * 备注信息
     */
    private String remarks;


    public GoodsCategoryVO(GoodsCategory category) {
        BeanUtil.copyProperties(category, this);
        this.setCategoryId(category.getId());
        this.setCategoryName(category.getName());
    }
}
