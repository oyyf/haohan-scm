package com.haohan.cloud.scm.api.saleb.req.order;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.BuyDetailModifyReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/30
 */
@Data
public class BuyOrderAddReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "采购商id不能为空")
    @Length(max = 64, message = "采购商id长度最大64字符")
    @ApiModelProperty(value = "采购商id")
    private String buyerId;

    @NotNull(message = "送货日期为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("送货日期")
    private LocalDate deliveryDate;

    private BuySeqEnum buySeq;

    @Length(max = 255, message = "采购需求长度最大255字符")
    private String needNote;

    @Length(min = 0, max = 255, message = "订单备注长度在0至255之间")
    private String remarks;

    @Digits(integer = 10, fraction = 2, message = "运费的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "运费")
    private BigDecimal shipFee;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @Valid
    @NotEmpty(message = "商品明细不能为空")
    @ApiModelProperty("商品明细列表")
    private List<BuyDetailModifyReq> detailList;

    public BuyOrder transTo(Buyer buyer) {
        BuyOrder order = new BuyOrder();
        order.setPmId(buyer.getPmId());
        order.setBuyerId(this.buyerId);
        order.setBuyerName(buyer.getBuyerName());
        order.setBuyerUid(buyer.getPassportId());
        order.setBuyTime(LocalDateTime.now());
        order.setDeliveryTime(this.deliveryDate);
        order.setBuySeq(null == this.buySeq ? BuySeqEnum.first : this.buySeq);
        order.setNeedNote(this.needNote);
        // 无总价
        order.setContact(buyer.getContact());
        order.setTelephone(buyer.getTelephone());
        order.setAddress(buyer.getAddress());
//        order.setStatus(BuyOrderStatusEnum.submit);
        order.setShipFee(this.shipFee);
        order.setDeliveryType(null == this.deliveryType ? DeliveryTypeEnum.self_delivery : this.deliveryType);
//        order.setPayStatus(PayStatusEnum.wait);
        order.setRemarks(this.remarks);
        return order;
    }
}
