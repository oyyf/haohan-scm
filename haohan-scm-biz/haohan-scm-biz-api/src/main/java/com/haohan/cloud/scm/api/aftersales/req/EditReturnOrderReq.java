package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/7
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class EditReturnOrderReq extends ReturnOrder {

    private List<ReturnOrderDetail> editDetails;

    private List<ReturnOrderDetail> addDetails;

    private List<ReturnOrderDetail> delDetails;

    private String warehouseId;

    /**
     * 退货图片id列表
     */
    @ApiModelProperty(value = "退货图片id列表")
    private List<String> photoList;
}
