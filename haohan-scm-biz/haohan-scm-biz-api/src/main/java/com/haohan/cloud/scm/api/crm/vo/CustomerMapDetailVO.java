package com.haohan.cloud.scm.api.crm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
public class CustomerMapDetailVO {

    /**
     * 经纬度  position
     */
    @JsonProperty("center")
    private String position;
    /**
     * 客户编号  customerSn
     */
    @JsonProperty("id")
    private String customerSn;
    /**
     * 客户名称  customerName
     */
    @JsonProperty("name")
    private String customerName;
    /**
     * 详细地址  address
     */
    private String address;

    @ApiModelProperty(value = "座机电话")
    @JsonProperty("tel")
    private String phoneNumber;
    @ApiModelProperty(value = "客户类型:1经销商2门店")
    @JsonProperty("type")
    private CustomerTypeEnum customerType;

    /**
     * 联系人
     */
    @JsonProperty("info")
    private LinkManVO linkMan;

    public CustomerMapDetailVO(Customer customer) {
        if (null != customer) {
            this.position = customer.getPosition();
            this.customerSn = customer.getCustomerSn();
            this.customerName = customer.getCustomerName();
            this.address = customer.getAddress();
            this.phoneNumber = customer.getPhoneNumber();
            this.customerType = customer.getCustomerType();

        }
    }

}
