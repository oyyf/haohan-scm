package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum ReportTypeEnum {

    /**
     * 汇报类型1.日常汇报2.订单采购汇报
     */
    dailyReport("1","日常汇报"),
    orderPurchaseReport("2","订单采购汇报");

    private static final Map<String, ReportTypeEnum> MAP = new HashMap<>(8);

    static {
      for (ReportTypeEnum e : ReportTypeEnum.values()) {
        MAP.put(e.getType(), e);
      }
    }

    @JsonCreator
    public static ReportTypeEnum getByType(String type) {
      if (MAP.containsKey(type)) {
        return MAP.get(type);
      }
      return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
