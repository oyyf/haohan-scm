/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 订单表
 *
 * @author haohan
 * @date 2019-07-10 22:40:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "订单表")
public class StoreOrderReq extends StoreOrder {

  private long pageSize;
  private long pageNo;

  //开始创建时间
  private Integer beginCreateTime;
  //结束创建时间
  private Integer endCreateTime;


}
