package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.constant.enums.product.EnterTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/9
 */
@Data
@ApiModel(description = "根据商品创建入库单、入库单明细明细、货品信息")
public class CreateEnterWarehouseDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseSn;

    @NotNull(message = "enterType不能为空")
    @ApiModelProperty(value = "入库单类型:1.加工入库2.采购入库", required = true)
    private EnterTypeEnum enterType;

    @NotEmpty(message = "商品参数列表不能为空")
    private List<GoodsModelReq> list;
}
