/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户项目售后维护管理
 *
 * @author haohan
 * @date 2019-05-30 10:28:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户项目售后维护管理")
public class CustomerServiceManageReq extends CustomerServiceManage {

    private long pageSize;
    private long pageNo;




}
