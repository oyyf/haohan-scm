/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.api.opc.req.ReadySettlementReq;
import com.haohan.cloud.scm.api.opc.req.SettlementRecordReq;
import com.haohan.cloud.scm.api.pay.entity.OrderPayRecord;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 结算记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SettlementRecordFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface SettlementRecordFeignService {


    /**
     * 通过id查询结算记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SettlementRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 结算记录 列表信息
     * @param settlementRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SettlementRecord/fetchSettlementRecordPage")
    R getSettlementRecordPage(@RequestBody SettlementRecordReq settlementRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 结算记录 列表信息
     * @param settlementRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SettlementRecord/fetchSettlementRecordList")
    R getSettlementRecordList(@RequestBody SettlementRecordReq settlementRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/add")
    R save(@RequestBody SettlementRecord settlementRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改结算记录
     * @param settlementRecord 结算记录
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/update")
    R updateById(@RequestBody SettlementRecord settlementRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除结算记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SettlementRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/countBySettlementRecordReq")
    R countBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param settlementRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/getOneBySettlementRecordReq")
    R getOneBySettlementRecordReq(@RequestBody SettlementRecordReq settlementRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param settlementRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SettlementRecord> settlementRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 准备结算
     *
     * @param req
     * @return R
     */
    @PostMapping("/api/feign/SettlementRecord/readySettlement")
    R<SettlementRecord> readySettlement(@RequestBody ReadySettlementReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 在线支付后完成结算
     * @param payRecord
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SettlementRecord/payToSettlement")
    R<Boolean> payToSettlement(@RequestBody OrderPayRecord payRecord, @RequestHeader(SecurityConstants.FROM) String from);
}
