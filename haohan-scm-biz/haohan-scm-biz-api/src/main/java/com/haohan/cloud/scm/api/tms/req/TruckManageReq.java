/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.req;

import com.haohan.cloud.scm.api.tms.entity.TruckManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 车辆管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "车辆管理")
public class TruckManageReq extends TruckManage {

    private long pageSize;
    private long pageNo;


}
