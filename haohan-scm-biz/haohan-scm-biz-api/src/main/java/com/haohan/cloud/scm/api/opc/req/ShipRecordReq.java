/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "发货记录")
public class ShipRecordReq extends ShipRecord {

    private long pageSize;
    private long pageNo;

}
