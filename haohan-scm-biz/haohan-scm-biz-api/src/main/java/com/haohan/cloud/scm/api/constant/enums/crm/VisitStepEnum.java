package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/24
 */
@Getter
@AllArgsConstructor
public enum VisitStepEnum implements IBaseEnum {

    /**
     * 进展阶段:1.初次拜访 2.了解交流 3.深入跟进 4.达成合作 5.商务往来
     */
    first("1", "初次拜访"),
    understand("2", "了解交流"),
    follow("3", "深入跟进"),
    reach("4", "达成合作"),
    business("5", "商务往来");

    private static final Map<String, VisitStepEnum> MAP = new HashMap<>(8);

    static {
        for (VisitStepEnum e : VisitStepEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static VisitStepEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
