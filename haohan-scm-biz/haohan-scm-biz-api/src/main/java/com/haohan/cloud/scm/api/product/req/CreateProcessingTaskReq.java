package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "创建加工生产任务需求")
public class CreateProcessingTaskReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家Id" , required = true)
    private String pmId;

    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;

    @NotNull(message = "needNumber不能为空")
    @ApiModelProperty(value = "需求加工数量" , required = true)
    private BigDecimal needNumber;

    @ApiModelProperty(value = "执行人id" )
    private String transactorId;

    @ApiModelProperty(value = "发起人id")
    private String initiatorId;

    @ApiModelProperty(value = "任务内容说明")
    private String taskContent;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "任务截止时间", example = "2000-01-01 12:00:00")
    private LocalDateTime deadlineTime;
}
