/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 出库单
 *
 * @author haohan
 * @date 2019-05-28 19:07:58
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "出库单")
public class ExitWarehouseReq extends ExitWarehouse {

    private long pageSize;
    private long pageNo;




}
