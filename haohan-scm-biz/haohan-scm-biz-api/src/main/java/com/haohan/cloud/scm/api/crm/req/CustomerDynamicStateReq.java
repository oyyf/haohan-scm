/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.CustomerDynamicState;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户业务动态
 *
 * @author haohan
 * @date 2019-08-30 11:45:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户业务动态")
public class CustomerDynamicStateReq extends CustomerDynamicState {

    private long pageSize;
    private long pageNo;

    private String merchantId;

}
