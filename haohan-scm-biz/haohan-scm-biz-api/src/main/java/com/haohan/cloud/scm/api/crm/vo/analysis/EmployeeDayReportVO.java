package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/9
 */
@Data
@Api("员工每日日报数")
public class EmployeeDayReportVO {

    @ApiModelProperty(value = "日报总数")
    private Integer reportTotalNum;

    @ApiModelProperty(value = "查询开始时间")
    protected LocalDate startDate;

    @ApiModelProperty(value = "查询结束时间")
    protected LocalDate endDate;

    @ApiModelProperty(value = "日报列表(按日)")
    List<WorkReportDayVO> reportList;

}
