package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/31
 */
@Getter
@AllArgsConstructor
public enum YesNoEnum implements IBaseEnum {
    /**
     * yes_no  0.否 1.是
     */
    no("0", "否"),
    yes("1", "是");

    private static final Map<String, YesNoEnum> MAP = new HashMap<>(8);
    private static final Map<String, YesNoEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (YesNoEnum e : YesNoEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static YesNoEnum getByType(String type) {
        return MAP.get(type);
    }

    public static YesNoEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
