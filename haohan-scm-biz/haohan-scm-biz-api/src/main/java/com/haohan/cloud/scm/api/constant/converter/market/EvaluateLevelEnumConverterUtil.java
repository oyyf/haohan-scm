package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.EvaluateLevelEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class EvaluateLevelEnumConverterUtil implements Converter<EvaluateLevelEnum> {
    @Override
    public EvaluateLevelEnum convert(Object o, EvaluateLevelEnum evaluateLevelEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EvaluateLevelEnum.getByType(o.toString());
    }
}
