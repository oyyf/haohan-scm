package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseQueryPurchaseTaskReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    private String id;

}
