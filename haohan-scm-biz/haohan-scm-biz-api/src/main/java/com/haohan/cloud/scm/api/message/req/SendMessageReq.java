package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.message.dto.SendInMailMessageDTO;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2020/3/20
 * 发送消息
 */
@Data
public class SendMessageReq {

    /**
     * 消息标题(消息简述)
     */
    @NotBlank(message = "消息标题不能为空")
    @Length(max = 32, message = "消息标题最大长度32字符")
    private String title;

    /**
     * 消息内容
     */
    @NotBlank(message = "消息内容不能为空")
    @Length(max = 5000, message = "消息内容最大长度5000字符")
    private String content;

    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    @NotNull(message = "业务部门类型不能为空")
    private DepartmentTypeEnum departmentType;

    /**
     * 站内信类型:1公告2及时通信
     */
    private InMailTypeEnum inMailType;

    /**
     * 发送人uid
     */
    @Length(max = 32, message = "发送人uid最大长度32字符")
    private String senderUid;
    /**
     * 发送人名称
     */
    @Length(max = 32, message = "发送人名称最大长度32字符")
    private String senderName;

    /**
     * 业务类型:各部门的消息
     */
    private MsgActionTypeEnum msgActionType;


    public SendInMailMessageDTO transTo() {
        SendInMailMessageDTO msg = new SendInMailMessageDTO();
        msg.setTitle(this.title);
        msg.setContent(this.content);
        msg.setDepartmentType(this.departmentType);
        msg.setInMailType(this.inMailType);
        msg.setSenderUid(this.senderUid);
        msg.setSenderName(this.senderName);
        msg.setMsgActionType(this.msgActionType);
        return msg;
    }
}
