package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum EvaluateLevelEnum {
    /**
     * 考核评级:1优2.良3中4差
     */
    excellent("1","优"),
    good("2","良"),
    middle("3","中"),
    bad("4","差");

    private static final Map<String, EvaluateLevelEnum> MAP = new HashMap<>(8);

    static {
        for (EvaluateLevelEnum e : EvaluateLevelEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static EvaluateLevelEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
