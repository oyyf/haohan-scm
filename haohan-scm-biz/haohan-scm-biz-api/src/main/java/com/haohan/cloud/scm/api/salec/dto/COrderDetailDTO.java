package com.haohan.cloud.scm.api.salec.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/7/13
 */
@Data
public class COrderDetailDTO {

    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 商品id
     */
    private Integer product_id;
    /**
     * 商品规格id
     */
    private String product_attr_unique;

    /**
     * 采购数量
     */
    private BigDecimal cart_num;
    /**
     * 创建时间
     */
    private String add_time;

    /**
     * 商品信息
     */
    private ProductInfoDTO productInfo;

    /**
     * 真实价格
     */
    private BigDecimal truePrice;

    /**
     * vip真实价格
     */
    private BigDecimal vipTruePrice;
    /**
     * 真实库存
     */
    private Integer trueStock;
    /**
     * 成本价
     */
    private BigDecimal costPrice;
}
