package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "获取托盘")
public class GainPalletReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号",required = true)
    private String warehouseSn;

    @ApiModelProperty(value = "托盘名称")
    private String palletName;

    @ApiModelProperty(value = "托盘名称")
    private String palletDesc;

}
