package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.haohan.cloud.scm.api.constant.enums.market.AnniversaryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CalendarTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerAnniversary;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/2
 */
@Data
@NoArgsConstructor
public class CustomerAnniversaryVO {

    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 主题内容
     */
    @ApiModelProperty(value = "主题内容")
    private String content;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 客户联系人id
     */
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;
    /**
     * 客户联系人姓名
     */
    @ApiModelProperty(value = "客户联系人姓名")
    private String linkmanName;
    /**
     * 纪念日类型:1.公司2.联系人
     */
    @ApiModelProperty(value = "纪念日类型:1.公司2.联系人")
    private AnniversaryTypeEnum anniversaryType;
    /**
     * 纪念日时间
     */
    @ApiModelProperty(value = "纪念日时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime anniversaryTime;
    /**
     * 日历类型:1.公历2.农历
     */
    @ApiModelProperty(value = "日历类型:1.公历2.农历")
    private CalendarTypeEnum calendarType;
    /**
     * 市场负责人id
     */
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;
    /**
     * 市场负责人名称
     */
    @ApiModelProperty(value = "市场负责人名称")
    private String directorName;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "客户联系人详情")
    private LinkManVO linkManInfo;

    public CustomerAnniversaryVO(CustomerAnniversary anniversary) {
        if (null != anniversary) {
            BeanUtil.copyProperties(anniversary, this);
        }
    }

}
