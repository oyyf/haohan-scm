/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import com.haohan.cloud.scm.api.crm.req.CustomerReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户基础信息内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerFeignService {


    /**
     * 通过id查询客户基础信息
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Customer/{id}", method = RequestMethod.GET)
    R<Customer> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户基础信息 列表信息
     *
     * @param customerReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Customer/fetchCustomerPage")
    R<Page<Customer>> getCustomerPage(@RequestBody CustomerReq customerReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户基础信息 列表信息
     *
     * @param customerReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Customer/fetchCustomerList")
    R<List<Customer>> getCustomerList(@RequestBody CustomerReq customerReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户基础信息
     *
     * @param customer 客户基础信息
     * @return R
     */
    @PostMapping("/api/feign/Customer/add")
    R<Boolean> save(@RequestBody Customer customer, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户基础信息
     *
     * @param customer 客户基础信息
     * @return R
     */
    @PostMapping("/api/feign/Customer/update")
    R<Boolean> updateById(@RequestBody Customer customer, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户基础信息
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Customer/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Customer/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Customer/listByIds")
    R<List<Customer>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Customer/countByCustomerReq")
    R<Integer> countByCustomerReq(@RequestBody CustomerReq customerReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Customer/getOneByCustomerReq")
    R<Customer> getOneByCustomerReq(@RequestBody CustomerReq customerReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Customer/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<Customer> customerList, @RequestHeader(SecurityConstants.FROM) String from);


}
