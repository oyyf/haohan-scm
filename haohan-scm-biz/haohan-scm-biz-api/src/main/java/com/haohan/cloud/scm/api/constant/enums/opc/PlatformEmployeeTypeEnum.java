package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PlatformEmployeeTypeEnum {
  responsible("1","分管负责人"),
  operation("2","运营人员"),
  finance("3","财务");
  @EnumValue
  private String type;
  @JsonValue
  private String desc;
}
