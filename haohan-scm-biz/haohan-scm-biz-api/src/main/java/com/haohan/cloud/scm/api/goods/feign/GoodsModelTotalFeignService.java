/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import com.haohan.cloud.scm.api.goods.req.GoodsModelTotalReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品规格名称内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsModelTotalFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsModelTotalFeignService {


    /**
     * 通过id查询商品规格名称
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsModelTotal/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品规格名称 列表信息
     *
     * @param goodsModelTotalReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsModelTotal/fetchGoodsModelTotalPage")
    R getGoodsModelTotalPage(@RequestBody GoodsModelTotalReq goodsModelTotalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品规格名称 列表信息
     *
     * @param goodsModelTotalReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsModelTotal/fetchGoodsModelTotalList")
    R getGoodsModelTotalList(@RequestBody GoodsModelTotalReq goodsModelTotalReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品规格名称
     *
     * @param goodsModelTotal 商品规格名称
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/add")
    R save(@RequestBody GoodsModelTotal goodsModelTotal, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品规格名称
     *
     * @param goodsModelTotal 商品规格名称
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/update")
    R updateById(@RequestBody GoodsModelTotal goodsModelTotal, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品规格名称
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsModelTotalReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/countByGoodsModelTotalReq")
    R countByGoodsModelTotalReq(@RequestBody GoodsModelTotalReq goodsModelTotalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsModelTotalReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/getOneByGoodsModelTotalReq")
    R getOneByGoodsModelTotalReq(@RequestBody GoodsModelTotalReq goodsModelTotalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param goodsModelTotalList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/GoodsModelTotal/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<GoodsModelTotal> goodsModelTotalList, @RequestHeader(SecurityConstants.FROM) String from);


}
