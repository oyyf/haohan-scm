package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.WorkReportTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import com.haohan.cloud.scm.api.crm.vo.analysis.EmployeeDailyCountVO;
import com.haohan.cloud.scm.api.crm.vo.analysis.WorkReportAnalysisCountVO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/12
 */
@Data
@Api("工作日报 带图片组")
@NoArgsConstructor
public class WorkReportVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "汇报id")
    private String id;

    @ApiModelProperty(value = "汇报编号")
    private String reportSn;

    @ApiModelProperty(value = "工作汇报类型: 1.日常 2.销量 3.库存")
    private WorkReportTypeEnum reportType;

    @ApiModelProperty(value = "汇报内容")
    private String reportContent;

    @ApiModelProperty(value = "汇报位置定位")
    private String reportLocation;

    @ApiModelProperty(value = "汇报地址")
    private String reportAddress;

    @ApiModelProperty(value = "汇报人id")
    private String reportManId;

    @ApiModelProperty(value = "汇报人名称")
    private String reportMan;

    @ApiModelProperty(value = "汇报时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reportTime;

    @ApiModelProperty(value = "汇报人手机号")
    private String reportTel;

    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private ReportStatusEnum status;

    @ApiModelProperty(value = "反馈信息")
    private String feedback;

    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;

    @ApiModelProperty(value = "日报图片列表")
    private List<PhotoGallery> photoList;

    @ApiModelProperty(value = "员工日常统计数")
    private WorkReportAnalysisCountVO dailyCount;

    // 扩展

    @ApiModelProperty(value = "员工部门名称")
    private String deptName;

    @ApiModelProperty(value = "员工类型描述")
    private String employeeType;

    public WorkReportVO(WorkReport workReport) {
        BeanUtil.copyProperties(workReport, this);
    }

    public void setEmployee(MarketEmployee employee) {
        this.deptName = null == employee.getDeptName() ? "无" : employee.getDeptName();
        this.employeeType = null == employee.getEmployeeType() ? "无" : employee.getEmployeeType().getDesc();
    }
}
