/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.iot.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 云打印终端管理
 *
 * @author haohan
 * @date 2019-05-13 19:12:48
 */
@Data
@TableName("scm_cloud_print_terminal")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "云打印终端管理")
public class CloudPrintTerminal extends Model<CloudPrintTerminal> {
private static final long serialVersionUID = 1L;

    /**
   * 编号
   */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
   * 商家
   */
    private String merchantId;
    /**
   * 店铺
   */
    private String shopId;
    /**
   * 应用ID
   */
    private String clientId;
    /**
   * 打印机编号
   */
    private String machineCode;
    /**
   * 密钥
   */
    private String secret;
    /**
   * 打印机名称
   */
    private String name;
    /**
   * 状态
   */
    private String status;
    /**
   * 创建者
   */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
   * 创建时间
   */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
   * 更新时间
   */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;

}
