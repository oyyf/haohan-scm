package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/12/10
 */
@Getter
@AllArgsConstructor
public enum DeliveryGoodsStatusEnum implements IBaseEnum {

    /**
     * 配送状态 1未送，2已送，3异常
     */
    wait("1", "未送"),
    finish("2", "已送"),
    unusual("3", "异常");

    private static final Map<String, DeliveryGoodsStatusEnum> MAP = new HashMap<>(8);

    static {
        for (DeliveryGoodsStatusEnum e : DeliveryGoodsStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DeliveryGoodsStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
