package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/5
 */
@Data
@ApiModel(description = "采购任务审核——B端")
public class CheckTaskBatchReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID",required = true)
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    @ApiModelProperty(value = "采购单编号",required = true)
    private String purchaseSn;

    @NotNull(message = "purchaseStatus不能为空")
    @ApiModelProperty(value = "采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭")
    private PurchaseStatusEnum purchaseStatus;

    @NotNull(message = "detailList不能为空")
    @ApiModelProperty(value = "采购单明细列表",required = true)
    private List<DetailReq> detailList;

    @ApiModelProperty(value = "付款方式1.协议2.现款")
    private PayTypeEnum payType;

    @ApiModelProperty(value = "采购方式类型:1.竞价采购2.单品采购3.协议供应")
    private MethodTypeEnum methodType;

    @ApiModelProperty(value = "采购截止时间")
    private LocalDateTime buyFinalTime;

    @ApiModelProperty(value = "审批备注")
    private String actionDesc;


}
