package com.haohan.cloud.scm.api.constant.enums.message;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum AfterSalesStatusEnum {
    /**
     * 售后状态:1已上报2处理中3待确认4已完成
     */
    reported("1","已上报"),
    beingProcessed("2","处理中"),
    stayAffirm("3","待确认"),
    accomplish("4","已完成");

    private static final Map<String, AfterSalesStatusEnum> MAP = new HashMap<>(8);

    static {
        for (AfterSalesStatusEnum e : AfterSalesStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AfterSalesStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
