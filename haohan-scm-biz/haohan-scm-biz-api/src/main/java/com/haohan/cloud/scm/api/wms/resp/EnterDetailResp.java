package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/8/13
 */

@Data
public class EnterDetailResp extends EnterWarehouseDetail {
    private String productName;

    private BigDecimal productNumber;

    private String unit;
}
