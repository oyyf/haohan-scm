package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.SalesOrderImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class SalesOrderImportReq {

    @NotEmpty(message = "销售订单明细列表不能为空")
    private List<SalesOrderImport> detailList;

    @ApiModelProperty("是否按编号导入")
    private Boolean importType;

}
