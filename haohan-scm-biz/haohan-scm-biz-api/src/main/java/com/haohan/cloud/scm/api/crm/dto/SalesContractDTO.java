package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.market.ContractStatusEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
public class SalesContractDTO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 合同编号
     */
    @ApiModelProperty(value = "合同编号")
    private String salesContractSn;
    /**
     * 合同状态:1待签订2已签订3未签订
     */
    @ApiModelProperty(value = "合同状态:1待签订2已签订3未签订")
    private ContractStatusEnum contractStatus;
    /**
     * 合同描述
     */
    @ApiModelProperty(value = "合同描述")
    private String contractDesc;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 客户联系人id
     */
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;
    /**
     * 客户联系人姓名
     */
    @ApiModelProperty(value = "客户联系人姓名")
    private String linkmanName;
    /**
     * 销售机会id
     */
    @ApiModelProperty(value = "销售机会id")
    private String salesLeadsId;
    /**
     * 合同金额
     */
    @ApiModelProperty(value = "合同金额")
    private BigDecimal contractAmount;
    /**
     * 签约时间
     */
    @ApiModelProperty(value = "签约时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime contractTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedTime;
    /**
     * 条件条款
     */
    @ApiModelProperty(value = "条件条款")
    private String conditions;
    /**
     * 合同图片 图片组编号
     */
    @ApiModelProperty(value = "合同图片组编号")
    private String photoGroupNum;
    /**
     * 销售分润比例
     */
    @ApiModelProperty(value = "销售分润比例")
    private BigDecimal salesRate;
    /**
     * 市场负责人id
     */
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;
    /**
     * 市场负责人名称
     */
    @ApiModelProperty(value = "市场负责人名称")
    private String directorName;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 合同图片列表
     */
    @ApiModelProperty(value = "合同图片列表")
    private List<PhotoGallery> photoList;


}
