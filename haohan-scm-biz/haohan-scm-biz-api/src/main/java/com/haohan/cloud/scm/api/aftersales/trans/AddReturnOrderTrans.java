package com.haohan.cloud.scm.api.aftersales.trans;


import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.*;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;

import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/8/6
 */
public class AddReturnOrderTrans {

    public static ReturnOrderDetail initReturnOrderDetailTrans(PurchaseOrderDetail detail){
        ReturnOrderDetail d = new ReturnOrderDetail();
        d.setPmId(detail.getPmId());
        d.setGoodsAmount(detail.getRealBuyNum().multiply(detail.getBuyPrice()));
        d.setGoodsCategoryId(detail.getGoodsCategoryId());
        d.setGoodsCategoryName(detail.getGoodsCategoryName());
        d.setGoodsId(detail.getGoodsId());
        d.setGoodsImg(detail.getGoodsImg());
        d.setGoodsModelId(detail.getGoodsModelId());
        d.setGoodsName(detail.getGoodsName());
        d.setGoodsNum(detail.getRealBuyNum());
        d.setGoodsPrice(detail.getBuyPrice());
        d.setModelName(detail.getModelName());
        d.setReturnDetailSn(detail.getPurchaseDetailSn());
        d.setUnit(detail.getUnit());
        d.setReturnStatus(ReturnStatusEnum.wait);
        d.setReturnTime(LocalDateTime.now());
        return d;
    }

    public static ReturnOrderDetail initBuyOrderDetailReturn(BuyOrderDetail de, GoodsModelDTO dto){
        ReturnOrderDetail d = new ReturnOrderDetail();
        d.setPmId(de.getPmId());
        d.setGoodsAmount(de.getBuyPrice().multiply(de.getGoodsNum()));
        d.setGoodsCategoryId(dto.getGoodsCategoryId());
        d.setGoodsCategoryName(dto.getCategoryName());
        d.setGoodsPrice(de.getBuyPrice());
        d.setGoodsNum(de.getGoodsNum());
        d.setReturnTime(LocalDateTime.now());
        d.setUnit(de.getUnit());
        d.setOrderDetailSn(de.getBuyDetailSn());
        d.setReturnStatus(ReturnStatusEnum.wait);
        d.setModelName(de.getGoodsModel());
        d.setGoodsModelId(de.getGoodsModelId());
        d.setGoodsName(de.getGoodsName());
        d.setGoodsImg(de.getGoodsImg());
        d.setGoodsId(dto.getGoodsId());
        return d;
    }

    public static ProductInfo initProductInfoTrans(ReturnOrderDetail de){
        ProductInfo p = new ProductInfo();
        p.setPmId(de.getPmId());
        p.setProductQuality(ProductQialityEnum.normal);
        p.setGoodsId(de.getGoodsId());
        p.setGoodsModelId(de.getGoodsModelId());
        p.setProductNumber(de.getGoodsNum());
        p.setProductStatus(ProductStatusEnum.normal);
        // TODO
        p.setPurchasePrice(de.getGoodsPrice());
        p.setProductType(ProductTypeEnum.Return);
        p.setUnit(de.getUnit());
        p.setProductName(de.getGoodsName()+"|"+de.getModelName());
        p.setPurchaseDetailSn(de.getReturnDetailSn());
        p.setProcessingType(ProcessingTypeEnum.not_handle);
        p.setProductPlaceStatus(ProductPlaceStatusEnum.stock);
        p.setSourceProductSn(ScmCommonConstant.ORIGINAL_PRODUCT_SN);
        return p;
    }

    public static EnterWarehouseDetail initEnterWarehouseDetailsTrans(ReturnOrderDetail d,ProductInfo i){
        EnterWarehouseDetail e = new EnterWarehouseDetail();
        e.setPmId(d.getPmId());
        e.setPurchaseDetailSn(d.getReturnDetailSn());
        e.setProductNumber(i.getProductNumber());
        e.setProductSn(i.getProductSn());
        e.setEnterStatus(EnterStatusEnum.acceptance);
        e.setGoodsModelId(i.getGoodsModelId());
        e.setUnit(d.getUnit());
        e.setApplyTime(LocalDateTime.now());
        e.setStorageType(StorageTypeEnum.enterwarehouse);
        return e;
    }

    public static EnterWarehouse initEnterWarehouseTrans(ReturnOrder o){
        EnterWarehouse  warehouse = new EnterWarehouse();
        warehouse.setPurchaseSn(o.getReturnSn());
        warehouse.setEnterType(EnterTypeEnum.returnEnter);
        warehouse.setPmId(o.getPmId());
        warehouse.setApplyTime(LocalDateTime.now());
        warehouse.setEnterStatus(EnterStatusEnum.acceptance);
        warehouse.setStorageType(StorageTypeEnum.enterwarehouse);
        return warehouse;
    }
}
