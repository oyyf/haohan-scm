package com.haohan.cloud.scm.api.opc.req.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/8/6
 */
@Data
@ApiModel(description = "查询应付账单是否可审核")
public class CheckPayableReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "supplierPaymentId不能为空")
    @ApiModelProperty(value = "应付账单编号", required = true)
    private String supplierPaymentId;

}
