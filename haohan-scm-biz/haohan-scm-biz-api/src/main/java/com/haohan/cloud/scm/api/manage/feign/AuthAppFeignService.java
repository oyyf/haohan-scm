/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.AuthApp;
import com.haohan.cloud.scm.api.manage.req.AuthAppReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 授权应用管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AuthAppFeignService", value = ScmServiceName.SCM_MANAGE)
public interface AuthAppFeignService {


    /**
     * 通过id查询授权应用管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/AuthApp/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 授权应用管理 列表信息
     * @param authAppReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AuthApp/fetchAuthAppPage")
    R getAuthAppPage(@RequestBody AuthAppReq authAppReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 授权应用管理 列表信息
     * @param authAppReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AuthApp/fetchAuthAppList")
    R getAuthAppList(@RequestBody AuthAppReq authAppReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增授权应用管理
     * @param authApp 授权应用管理
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/add")
    R save(@RequestBody AuthApp authApp, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改授权应用管理
     * @param authApp 授权应用管理
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/update")
    R updateById(@RequestBody AuthApp authApp, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除授权应用管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/AuthApp/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param authAppReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/countByAuthAppReq")
    R countByAuthAppReq(@RequestBody AuthAppReq authAppReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param authAppReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/getOneByAuthAppReq")
    R getOneByAuthAppReq(@RequestBody AuthAppReq authAppReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param authAppList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/AuthApp/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<AuthApp> authAppList, @RequestHeader(SecurityConstants.FROM) String from);


}
