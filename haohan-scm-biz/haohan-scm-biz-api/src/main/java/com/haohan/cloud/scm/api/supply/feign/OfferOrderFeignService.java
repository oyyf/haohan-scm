/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.req.*;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 报价单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "OfferOrderFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface OfferOrderFeignService {


    /**
     * 通过id查询报价单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/OfferOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 报价单 列表信息
     * @param offerOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/fetchOfferOrderPage")
    R getOfferOrderPage(@RequestBody OfferOrderReq offerOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 报价单 列表信息
     * @param offerOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/fetchOfferOrderList")
    R<List<OfferOrder>> getOfferOrderList(@RequestBody OfferOrderReq offerOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增报价单
     * @param offerOrder 报价单
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/add")
    R save(@RequestBody OfferOrder offerOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改报价单
     * @param offerOrder 报价单
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/update")
    R updateById(@RequestBody OfferOrder offerOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除报价单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/OfferOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/countByOfferOrderReq")
    R countByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/getOneByOfferOrderReq")
    R<OfferOrder> getOneByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param offerOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/OfferOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<OfferOrder> offerOrderList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 初始创建 报价单
     * @param req 请求对象
     *       必需参数: supplierId / supplyPrice / offerType / goodsModelId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/addOfferOrder")
    R<OfferOrder> addOfferOrder(@RequestBody AddOfferOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询供应商货源（加筛选条件）
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/filterOfferOrder")
    R filterOfferOrder(@RequestBody FilterOfferOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货源报价
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/addSupplierOffer")
    R addSupplierOffer(@RequestBody OfferOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 供应商报价
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/affirmOffer")
    R affirmOffer(@RequestBody AffirmOfferReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改报价单
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/OfferOrder/updateOfferOrder")
    R updateOfferOrder(@RequestBody UpdateOfferOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
