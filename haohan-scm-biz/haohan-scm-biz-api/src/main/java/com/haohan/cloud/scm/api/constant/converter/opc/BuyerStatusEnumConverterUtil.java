package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.BuyerStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class BuyerStatusEnumConverterUtil implements Converter<BuyerStatusEnum> {
    @Override
    public BuyerStatusEnum convert(Object o, BuyerStatusEnum buyerStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BuyerStatusEnum.getByType(o.toString());
    }
}
