package com.haohan.cloud.scm.api.constant.converter.goods;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;

/**
 * @author dy
 * @date 2019/10/8
 */
public class GoodsFromTypeEnumConverterUtil implements Converter<GoodsFromTypeEnum> {
    @Override
    public GoodsFromTypeEnum convert(Object o, GoodsFromTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return GoodsFromTypeEnum.getByType(o.toString());
    }
}
