package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.BizfromTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class BizfromTypeEnumConverterUtil implements Converter<BizfromTypeEnum> {
    @Override
    public BizfromTypeEnum convert(Object o, BizfromTypeEnum bizfromTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BizfromTypeEnum.getByType(o.toString());
    }
}
