package com.haohan.cloud.scm.api.goods.req.manage;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/5/7
 */
@Data
public class EditModelReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "规格id的长度最大32字符")
    @ApiModelProperty(value = "规格id")
    private String modelId;

    @NotBlank(message = "规格名称不能为空")
    @Length(max = 32, message = "规格名称的长度最大32字符")
    @ApiModelProperty(value = "规格名称")
    private String modelName;

    @NotBlank(message = "规格类型名称不能为空")
    @Length(max = 10, message = "规格类型名称的长度最大10字符")
    @ApiModelProperty(value = "规格类型名称")
    private String typeName;

    @NotBlank(message = "规格商品图片地址能为空")
    @Length(max = 255, message = "规格商品图片地址的长度最大255字符")
    @ApiModelProperty(value = "规格商品图片地址")
    private String modelUrl;

    @Length(max = 32, message = "规格扫码购编码的长度最大32字符")
    @ApiModelProperty(value = "规格扫码购编码")
    private String modelCode;

    // 价格属性

    @NotNull(message = "规格市场价不能为空")
    @Digits(integer = 8, fraction = 2, message = "规格市场价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "规格市场价")
    private BigDecimal modelPrice;

    @Digits(integer = 8, fraction = 2, message = "规格虚拟价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "规格虚拟价")
    private BigDecimal virtualPrice;

    @Digits(integer = 8, fraction = 2, message = "规格参考成本价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "规格参考成本价")
    private BigDecimal costPrice;

    @NotBlank(message = "规格计量单位不能为空")
    @Length(max = 6, message = "规格计量单位的长度最大6字符")
    @ApiModelProperty(value = "规格计量单位")
    private String modelUnit;

    // 扩展

    @Digits(integer = 8, fraction = 2, message = "规格库存的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "规格库存")
    private BigDecimal modelStorage;

    @Digits(integer = 7, fraction = 3, message = "规格重量的整数位最大7位, 小数位最大3位")
    @ApiModelProperty(value = "规格重量")
    private BigDecimal weight;

    @Digits(integer = 4, fraction = 6, message = "规格体积的整数位最大4位, 小数位最大6位")
    @ApiModelProperty(value = "规格体积")
    private BigDecimal volume;

    @Range(min = 0, max = 99999, message = "规格库存预警值在0至99999之间")
    @ApiModelProperty(value = "规格库存预警值")
    private Integer stocksForewarn;


    public GoodsModel transTo() {
        // 规格名称单独处理
        GoodsModel model = new GoodsModel();
        model.setId(modelId);
        model.setModelName(this.modelName);
        model.setModelUrl(this.modelUrl);
        model.setModelCode(this.modelCode);
        model.setModelPrice(this.modelPrice);
        model.setVirtualPrice(this.virtualPrice);
        model.setCostPrice(this.costPrice);
        model.setModelUnit(this.modelUnit);
        model.setModelStorage(this.modelStorage);
        model.setWeight(this.weight);
        model.setVolume(this.volume);
        model.setStocksForewarn(this.stocksForewarn);
        return model;
    }
}
