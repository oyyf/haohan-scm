package com.haohan.cloud.scm.api.crm.req.order;

import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/9/28
 */
@Data
public class SalesDetailAddReq {

    @NotBlank(message = "商品规格id不能为空")
    @Length(min = 0, max = 32, message = "商品规格id长度在0至32之间")
    private String goodsModelId;

    /**
     * 修改后 无需传入 根据价格判断类型
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @NotNull(message = "采购数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "采购数量的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "采购数量在0至1000000之间")
    private BigDecimal goodsNum;

    @NotNull(message = "成交价格不能为空")
    @Digits(integer = 8, fraction = 2, message = "成交价格的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "成交价格在0至1000000之间")
    private BigDecimal dealPrice;

    @Length(min = 0, max = 100, message = "商品备注长度在0至100之间")
    private String remarks;

    @Digits(integer = 8, fraction = 2, message = "赠品数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "赠品数量")
    private BigDecimal giftNum;

}
