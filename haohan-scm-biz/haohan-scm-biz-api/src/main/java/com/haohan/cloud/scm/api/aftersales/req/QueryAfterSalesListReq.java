package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterSalesStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author xwx
 * @date 2019/7/22
 */
@Data
@ApiModel(description = "查询售后单列表")
public class QueryAfterSalesListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" , required = true)
    private String pmId;

    @NotNull(message = "afterSalesStatus不能为空")
    @ApiModelProperty(value = "售后状态:1已上报2处理中3待确认4已完成",required = true)
    private AfterSalesStatusEnum afterSalesStatus;

    @ApiModelProperty(value = "通行证id",required = true)
    private String uid;

    @ApiModelProperty(value = "每页个数")
    private long size = 10;

    @ApiModelProperty(value = "页数")
    private long current = 1;

}
