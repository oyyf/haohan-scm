package com.haohan.cloud.scm.api.purchase.resp;

import lombok.Data;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseModifyEmployeeResp {

    private boolean flag = false;

}
