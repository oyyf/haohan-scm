package com.haohan.cloud.scm.api.manage.req.shop;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/12
 * 新增  FirstGroup
 * 修改 SecondGroup
 */
@Data
@ApiModel(description = "店铺编辑")
public class ShopEditReq {

    @NotBlank(message = "商家id不能为空", groups = {FirstGroup.class})
    @Length(max = 32, message = "商家id最大长度32字符")
    @ApiModelProperty(value = "商家id", required = true)
    private String merchantId;

    @NotBlank(message = "店铺id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "店铺id最大长度32字符")
    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @NotBlank(message = "店铺名称不能为空", groups = {FirstGroup.class})
    @Length(max = 32, message = "店铺名称最大长度32字符")
    @ApiModelProperty(value = "店铺名称")
    private String name;

    @NotBlank(message = "店铺地址不能为空", groups = {FirstGroup.class})
    @Length(max = 64, message = "店铺地址最大长度64字符")
    @ApiModelProperty(value = "店铺地址")
    private String address;

    @NotBlank(message = "店铺负责人名称不能为空", groups = {FirstGroup.class})
    @Length(max = 10, message = "店铺负责人名称最大长度10字符")
    @ApiModelProperty(value = "店铺负责人名称")
    private String manager;

    @NotBlank(message = "店铺电话不能为空", groups = {FirstGroup.class})
    @Length(max = 20, message = "店铺电话最大长度20字符")
    @ApiModelProperty(value = "店铺电话")
    private String telephone;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @Length(max = 100, message = "营业时间最大长度100字符")
    @ApiModelProperty(value = "营业时间")
    private String onlineTime;

    @Length(max = 100, message = "店铺服务最大长度100字符")
    @ApiModelProperty(value = "店铺服务")
    private String shopService;

    @Length(max = 250, message = "店铺介绍最大长度250字符")
    @ApiModelProperty(value = "店铺介绍")
    private String shopDesc;

    @NotNull(message = "店铺启用状态不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "店铺启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @NotNull(message = "店铺等级不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "店铺等级", notes = "原系统使用字典 0 总店, 1 分店 2采购配送店")
    private ShopLevelEnum shopLevel;

    @Length(max = 32, message = "行业名称最大长度32字符")
    @ApiModelProperty(value = "行业名称")
    private String industry;

    @Length(max = 250, message = "店铺备注最大长度250字符")
    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "轮播图列表")
    private List<String> photoList;

    @ApiModelProperty(value = "店铺收款码列表")
    private List<String> payCodeList;

    @ApiModelProperty(value = "店铺二维码列表")
    private List<String> qrcodeList;

    @ApiModelProperty(value = "店铺Logo列表")
    private List<String> shopLogoList;

    public Shop transTo() {
        Shop shop = new Shop();
        shop.setMerchantId(this.merchantId);
        shop.setId(this.shopId);
        shop.setName(this.name);
        shop.setAddress(this.address);
        shop.setManager(this.manager);
        shop.setTelephone(this.telephone);

        if (StrUtil.isNotEmpty(this.position)) {
            String[] post = StrUtil.split(this.position, StrUtil.COMMA);
            shop.setMapLongitude(post[0]);
            shop.setMapLatitude(post[1]);
        }
        shop.setOnlineTime(this.onlineTime);
        shop.setShopService(this.shopService);
        shop.setShopDesc(this.shopDesc);
        shop.setStatus(this.status);
        shop.setShopLevel(this.shopLevel);
        shop.setIndustry(this.industry);
        shop.setRemarks(this.remarks);
        return shop;
    }
}
