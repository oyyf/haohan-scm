/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerLevel;
import com.haohan.cloud.scm.api.crm.req.CustomerLevelReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户组织结构内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerLevelFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerLevelFeignService {


    /**
     * 通过id查询客户组织结构
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerLevel/{id}", method = RequestMethod.GET)
    R<CustomerLevel> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户组织结构 列表信息
     *
     * @param customerLevelReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerLevel/fetchCustomerLevelPage")
    R<Page<CustomerLevel>> getCustomerLevelPage(@RequestBody CustomerLevelReq customerLevelReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户组织结构 列表信息
     *
     * @param customerLevelReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerLevel/fetchCustomerLevelList")
    R<List<CustomerLevel>> getCustomerLevelList(@RequestBody CustomerLevelReq customerLevelReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户组织结构
     *
     * @param customerLevel 客户组织结构
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/add")
    R<Boolean> save(@RequestBody CustomerLevel customerLevel, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户组织结构
     *
     * @param customerLevel 客户组织结构
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/update")
    R<Boolean> updateById(@RequestBody CustomerLevel customerLevel, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户组织结构
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/listByIds")
    R<List<CustomerLevel>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerLevelReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/countByCustomerLevelReq")
    R<Integer> countByCustomerLevelReq(@RequestBody CustomerLevelReq customerLevelReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerLevelReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/getOneByCustomerLevelReq")
    R<CustomerLevel> getOneByCustomerLevelReq(@RequestBody CustomerLevelReq customerLevelReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerLevelList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerLevel/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerLevel> customerLevelList, @RequestHeader(SecurityConstants.FROM) String from);


}
