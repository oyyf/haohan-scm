package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/8/11
 */
@Data
@ApiModel(description = "配送单批量送达")
public class ArriveBatchReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家id", required = true)
    private String pmId;

    @NotNull(message = "deliveryDate不能为空")
    @ApiModelProperty(value = "送货日期", required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @NotNull(message = "deliverySeq不能为空")
    @ApiModelProperty(value = "送货批次:0第一批1第二批", required = true)
    private BuySeqEnum deliverySeq;

}
