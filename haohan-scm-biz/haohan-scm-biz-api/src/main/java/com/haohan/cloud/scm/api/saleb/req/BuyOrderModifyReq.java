package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/9
 */
@Data
public class BuyOrderModifyReq {

    @NotBlank(message = "订单编号不能为空")
    @Length(min = 0, max = 32, message = "订单编号长度在0至32之间")
    @ApiModelProperty("订单编号")
    private String buyId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("送货日期")
    private LocalDate deliveryTime;

    private BuySeqEnum buySeq;

    @Length(min = 0, max = 255, message = "采购需求长度在0至255之间")
    private String needNote;

    @Length(min = 0, max = 255, message = "订单备注长度在0至255之间")
    private String remarks;

    @Valid
    @NotEmpty(message = "商品明细不能为空")
    @ApiModelProperty("商品明细列表")
    private List<BuyDetailModifyReq> detailList;


    public BuyOrder transTo() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setDeliveryTime(this.deliveryTime);
        buyOrder.setBuySeq(this.buySeq);
        buyOrder.setNeedNote(this.needNote);
        buyOrder.setRemarks(this.remarks);
        return buyOrder;
    }
}
