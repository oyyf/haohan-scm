package com.haohan.cloud.scm.api.crm.req.bill;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.req.BillInfoFeignReq;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/25
 */
@Data
@Api("查询应收账单(员工客户)")
public class CrmBillQueryReq {


    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    // eq条件

    @NotBlank(message = "账单编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "账单编号的最大长度为32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "账单编号")
    private String billSn;

    @ApiModelProperty(value = "账单审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "账单类型")
    private BillTypeEnum billType;

    @ApiModelProperty(value = "结算状态")
    private YesNoEnum settlementStatus;

    @Length(max = 32, message = "平台商家ID最大长度32字符")
    @ApiModelProperty(value = "平台商家ID")
    private String pmId;

    @Length(max = 32, message = "来源订单编号最大长度32字符")
    @ApiModelProperty(value = "来源订单编号")
    private String orderSn;

    @Length(max = 32, message = "下单客户编号最大长度32字符")
    @ApiModelProperty(value = "下单客户编号", notes = "采购商id、供应商id、客户sn")
    private String customerSn;

    @Length(max = 32, message = "客户商家ID最大长度32字符")
    @ApiModelProperty(value = "客户商家ID")
    private String merchantId;

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    private YesNoEnum advanceFlag;


    // 非eq查询条件

    @ApiModelProperty(value = "订单成交日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "订单成交日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    public BillInfoFeignReq transToFeign(Page page) {
        BillInfoFeignReq query = new BillInfoFeignReq();
        BeanUtil.copyProperties(this, query);
        query.setCustomerId(this.customerSn);
        query.setCurrent(page.getCurrent());
        query.setSize(page.getSize());
        return query;
    }
}
