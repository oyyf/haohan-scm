/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.Shelf;
import com.haohan.cloud.scm.api.wms.req.ShelfReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货架信息表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShelfFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface ShelfFeignService {


    /**
     * 通过id查询货架信息表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Shelf/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货架信息表 列表信息
     * @param shelfReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Shelf/fetchShelfPage")
    R getShelfPage(@RequestBody ShelfReq shelfReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货架信息表 列表信息
     * @param shelfReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Shelf/fetchShelfList")
    R getShelfList(@RequestBody ShelfReq shelfReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货架信息表
     * @param shelf 货架信息表
     * @return R
     */
    @PostMapping("/api/feign/Shelf/add")
    R save(@RequestBody Shelf shelf, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货架信息表
     * @param shelf 货架信息表
     * @return R
     */
    @PostMapping("/api/feign/Shelf/update")
    R updateById(@RequestBody Shelf shelf, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货架信息表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Shelf/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Shelf/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Shelf/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shelfReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Shelf/countByShelfReq")
    R countByShelfReq(@RequestBody ShelfReq shelfReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shelfReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Shelf/getOneByShelfReq")
    R getOneByShelfReq(@RequestBody ShelfReq shelfReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shelfList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Shelf/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Shelf> shelfList, @RequestHeader(SecurityConstants.FROM) String from);


}
