package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/6/15
 */
@Data
@ApiModel(description = "货品下架(货位、货品数量部分下架)")
public class ProductRemoveSectionReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "sourceProductSn不能为空")
    @ApiModelProperty(value = "原货品编号",required = true)
    private String sourceProductSn;

    @NotBlank(message = "subProductNum不能为空")
    @ApiModelProperty(value = "单个新货品数量")
    private BigDecimal subProductNum;

    @NotBlank(message = "operatorId不能为空")
    @ApiModelProperty(value = "操作人id")
    private String operatorId;

    @NotBlank(message = "targetPalletSn不能为空")
    @ApiModelProperty(value = "下架目标托盘编号")
    private String targetPalletSn;
}
