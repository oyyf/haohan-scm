package com.haohan.cloud.scm.api.iot.Resp;

import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import lombok.Data;

/**
 *
 * @author cx
 * @date 2019/8/24
 */

@Data
public class CloudPrintTerminalResp extends CloudPrintTerminal {

    /**
     * 店铺名称
     */
    private String shopName;

    /**
     * 商家名称
     */
    private String merchantName;
}
