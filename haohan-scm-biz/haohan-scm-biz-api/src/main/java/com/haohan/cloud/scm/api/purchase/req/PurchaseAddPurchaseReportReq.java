package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAddPurchaseReportReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;
    private String uid;

    /**
     * 汇报类型1.日常汇报2.订单采购汇报
     */
    private String reportType;
    /**
     * 汇报分类1.货品不足2.价格贵3.质量差4.其他
     */
    private String reportCategoryType;
    /**
     * 汇报状态1.已汇报2.已反馈3.已处理
     */
    private String reportStatus;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 汇报人
     */
    private String reporterId;
    /**
     * 汇报人名称
     */
    private String reporterName;
    /**
     * 汇报内容
     */
    private String reportContent;
    /**
     * 汇报时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reportTime;
    /**
     * 汇报图片
     */
    private String reportPhoto;
    /**
     * 反馈人
     */
    private String answerId;
    /**
     * 反馈人名称
     */
    private String answerName;
    /**
     * 反馈内容
     */
    private String answerContent;
    /**
     * 反馈时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime answerTime;
    /**
     * 备注信息
     */
    private String remarks;

}
