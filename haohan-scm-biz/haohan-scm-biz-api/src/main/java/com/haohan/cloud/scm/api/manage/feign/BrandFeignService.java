/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.Brand;
import com.haohan.cloud.scm.api.manage.req.BrandReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 品牌管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BrandFeignService", value = ScmServiceName.SCM_MANAGE)
public interface BrandFeignService {


    /**
     * 通过id查询品牌管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Brand/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 品牌管理 列表信息
     * @param brandReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Brand/fetchBrandPage")
    R getBrandPage(@RequestBody BrandReq brandReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 品牌管理 列表信息
     * @param brandReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Brand/fetchBrandList")
    R getBrandList(@RequestBody BrandReq brandReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @PostMapping("/api/feign/Brand/add")
    R save(@RequestBody Brand brand, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改品牌管理
     * @param brand 品牌管理
     * @return R
     */
    @PostMapping("/api/feign/Brand/update")
    R updateById(@RequestBody Brand brand, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除品牌管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Brand/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Brand/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Brand/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Brand/countByBrandReq")
    R countByBrandReq(@RequestBody BrandReq brandReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param brandReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Brand/getOneByBrandReq")
    R getOneByBrandReq(@RequestBody BrandReq brandReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param brandList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Brand/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Brand> brandList, @RequestHeader(SecurityConstants.FROM) String from);


}
