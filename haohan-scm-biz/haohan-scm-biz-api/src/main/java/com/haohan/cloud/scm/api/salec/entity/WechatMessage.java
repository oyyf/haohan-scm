/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 用户行为记录表
 *
 * @author haohan
 * @date 2020-05-21 18:46:18
 */
@Data
@TableName("eb_wechat_message")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户行为记录表")
public class WechatMessage extends Model<WechatMessage> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户行为记录id")
    @TableId(type = IdType.INPUT)
    private Integer id;

    @Length(max = 32, message = "用户openid长度最大32字符")
    @ApiModelProperty(value = "用户openid")
    private String openid;

    @Length(max = 32, message = "操作类型长度最大32字符")
    @ApiModelProperty(value = "操作类型")
    private String type;

    @Length(max = 512, message = "操作详细记录长度最大512字符")
    @ApiModelProperty(value = "操作详细记录")
    private String result;

    @ApiModelProperty(value = "操作时间")
    private Integer addTime;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
