package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ReportTypeEnumConverterUtil implements Converter<ReportTypeEnum> {
    @Override
    public ReportTypeEnum convert(Object o, ReportTypeEnum reportStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReportTypeEnum.getByType(o.toString());
    }
}
