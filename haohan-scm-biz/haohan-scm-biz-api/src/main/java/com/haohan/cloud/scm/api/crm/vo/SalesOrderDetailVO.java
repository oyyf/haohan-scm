package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/12/19
 */
@Data
@ApiModel("销售订单明细信息, 带商品信息")
@NoArgsConstructor
public class SalesOrderDetailVO {

    /**
     * 销售单编号
     */
    @ApiModelProperty(value = "销售单编号")
    private String salesOrderSn;
    /**
     * 销售单明细编号
     */
    @ApiModelProperty(value = "销售单明细编号")
    private String salesDetailSn;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 商品销售类型:1.普通2.促销品3.赠品
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;
    /**
     * 商品规格ID
     */
    @ApiModelProperty(value = "商品规格ID")
    private String goodsModelId;
    /**
     * 商品规格编号
     */
    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;
    /**
     * 商品图片
     */
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 商品规格名称
     */
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;
    /**
     * 采购数量
     */
    @ApiModelProperty(value = "采购数量")
    private BigDecimal goodsNum;
    /**
     * 市场价格
     */
    @ApiModelProperty(value = "市场价格")
    private BigDecimal marketPrice;
    /**
     * 成交价格
     */
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String unit;
    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "对应商品详情")
    private GoodsVO goodsInfo;

    @ApiModelProperty(value = "赠品明细数量")
    private BigDecimal giftNum;

    public SalesOrderDetailVO(SalesOrderDetail detail) {
        BeanUtil.copyProperties(detail, this);
        this.giftNum = BigDecimal.ZERO;
    }
}
