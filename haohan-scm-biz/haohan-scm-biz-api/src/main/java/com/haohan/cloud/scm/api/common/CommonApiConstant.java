package com.haohan.cloud.scm.api.common;

import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.api.common.req.*;
import com.haohan.cloud.scm.api.common.req.admin.PdsDateSeqBaseApiReq;
import com.haohan.cloud.scm.api.common.req.admin.PdsShipOrderApiReq;
import com.haohan.cloud.scm.api.common.req.buyer.*;
import com.haohan.cloud.scm.api.common.req.common.*;
import com.haohan.cloud.scm.api.common.req.delivery.PdsShipBuyerApiReq;
import com.haohan.cloud.scm.common.tools.http.MethodType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

/**
 * Created by zgw on 2019/5/22.
 * @author zgw
 */
@Getter
@AllArgsConstructor
public enum CommonApiConstant implements IApiEnum {

    //wiki http://wiki.corp.gh178.com/pages/viewpage.action?pageId=3048710
    Manage_buyorder_collect("采购单汇总", "/f/pds/api/admin/shortcut/collect", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    Manage_trade_confirm("交易确认", "/f/pds/api/admin/shortcut/confirm", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),

    goodsList("获取商品列表", "/f/pds/api/common/goods/fetchGoodsList", PdsBuyerGoodsReq.class, BaseResp.class, MethodType.GET),


    auth_login("管理后台登录", "/f/pds/api/auth/login", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_goods_queryList("采购商 一段时间", "/f/pds/api/buyer/goods/queryList", PdsBuyerPriceReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_initPrice("采购商 初始化复制", "/f/pds/api/buyer/goods/initPrice", PdsBuyerPriceReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_batchUpdateGoodsPrice("采购商 批量修改 采购价", "/f/pds/api/buyer/goods/batchUpdateGoodsPrice", PdsBuyerGoodsUpdateListReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_updateGoodsPrice("采购商 修改 采购价", "/f/pds/api/buyer/goods/updateGoodsPrice", PdsBuyerPriceUpdateReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_batchUpdateShelfStatus("采购商 批量上下架", "/f/pds/api/buyer/goods/batchUpdateShelfStatus", PdsApiBuyerGoodsShelfReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_collectList("获取采购商收藏商品列表", "/f/pds/api/buyer/goods/collect/list", PdsCollectGoodsListApiReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_collectAdd("采购商添加收藏", "/f/pds/api/buyer/goods/collect/add", PdsCollectGoodsApiReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_cancelCollect("采购商取消收藏", "/f/pds/api/buyer/goods/collect/cancel", PdsCollectGoodsApiReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_fetchGoodsModelInfoList("采购商商品(sku)信息列表查询", "/f/pds/api/buyer/goods/modelInfoList", PdsBuyerGoodsModelListReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_updatePriceToPmShop("批量修改平台商家店铺 商品的零售定价", "/f/pds/api/buyer/goods/shopPrice/batchUpdate", PdsUpdateShopPriceReq.class, BaseResp.class, MethodType.POST),
    buyer_goods_deletePriceBatch("批量删除采购商 采购价", "/f/pds/api/buyer/goods/buyerPrice/deleteUpdate", PdsDeletePriceBatchReq.class, BaseResp.class, MethodType.POST),
    buyer_info_queryPaymentList("采购商 货款查询", "/f/pds/api/buyer/info/queryPaymentList", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_info_totalPayment("采购商 货款合计查询 暂未使用", "/f/pds/api/buyer/info/totalPayment", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_info_paymentRecord("采购商 采购单生成货款记录", "/f/pds/api/buyer/info/paymentRecord", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_info_buyerList("采购商 列表查询", "/f/pds/api/buyer/info/buyerList", PdsBuyerReq.class, BaseResp.class, MethodType.POST),
    buyer_info_merchantList("采购商 查询商家列表", "/f/pds/api/buyer/info/merchantList", PdsBuyerReq.class, BaseResp.class, MethodType.POST),
    buyer_order_addBuyOrder("新增采购单", "/f/pds/api/buyer/order/buyOrder/add", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_modifyBuyOrder("修改采购单", "/f/pds/api/buyer/order/buyOrder/modify", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_queryBuyOrder("采购单列表 查询  带采购明细", "/f/pds/api/buyer/order/buyOrder/query", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_detailBuyOrder("采购单 带采购明细", "/f/pds/api/buyer/order/buyOrder/detail", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_confirmBuyOrder("采购商确认报价", "/f/pds/api/buyer/order/buyOrder/confirm", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_tradeGoodsList("采购商 查看采购交易商品列表 分页", "/f/pds/api/buyer/order/tradeGoods/fetchList", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_confirmTradeGoods("采购商 商品确认收货", "/f/pds/api/buyer/order/tradeGoods/confirm", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_buyOrderList("按采购商商家 统计 采购单", "/f/pds/api/buyer/order/merchant/buyOrderList", PdsBuyerOrderApiReq.class, BaseResp.class, MethodType.POST),
    buyer_order_printCompleteOrder("打印订单", "/f/pds/api/buyer/order/buyOrder/printComplete", HashMap.class, BaseResp.class, MethodType.POST),
    buyer_order_buyOrderDetailList("按采购商商家 统计 采购单明细", "/f/pds/api/buyer/order/merchant/buyOrderDetailList", PdsBuyerOrderApiReq.class, BaseResp.class, MethodType.POST),

    common_uidCheck("判断是 采购商或供应商", "/f/pds/api/common/uidCheck", HashMap.class, BaseResp.class, MethodType.POST),
    common_takeSession("处理session", "/f/pds/api/common", HashMap.class, BaseResp.class, MethodType.POST),
    common_bindTel("绑定 采购商/供应商 手机", "/f/pds/api/common/bindTel", HashMap.class, BaseResp.class, MethodType.POST),
    common_bindTel2("绑定 采购商/供应商 手机 方法2: 通过调用微信的获取手机号功能", "/f/pds/api/common/bindTel2", HashMap.class, BaseResp.class, MethodType.POST),
    common_findTelAndBind("按手机号 查找采购商/供应商表 并设置uid", "/f/pds/api/common", HashMap.class, BaseResp.class, MethodType.POST),
    common_fetchPrinterList("飞鹅打印机列表", "/f/pds/api/common/printer/fetchPrinterList", FeiePrinterReq.class, BaseResp.class, MethodType.POST),
    common_cloudPrinterList("获取易联云打印机列表", "/f/pds/api/common/printer/yiPrinterList", PdsPrinterQueryApiReq.class, BaseResp.class, MethodType.POST),
    common_textPrint("打印机设置", "/f/pds/api/common/printer/textPrint", PdsTextPrintApiReq.class, BaseResp.class, MethodType.POST),
    common_fetchAreaList("区域列表", "/f/pds/api/common/area/findList", PdsCommonAreaListReq.class, BaseResp.class, MethodType.POST),
    common_goods_fetchGoodsList("区域列表", "/f/pds/api/common/goods/fetchGoodsList", PdsBuyerGoodsReq.class, BaseResp.class, MethodType.POST),
    common_goods_fetchGoodsInfo("商品详情", "/f/pds/api/common/goods/fetchGoodsInfo", ReqGoods.class, BaseResp.class, MethodType.POST),
    common_goods_fetchCategoryList("商品分类列表", "/f/pds/api/common/goods/fetchCategoryList", HashMap.class, BaseResp.class, MethodType.POST),
    common_goods_selectTopN("获取采购商常用下单商品列表", "/f/pds/api/common/goods/topList", PdsBuyerTopNGoodsApiReq.class, BaseResp.class, MethodType.POST),
    common_goods_goodsBaseModify("商品基础信息编辑   名称/价格/图片", "/f/pds/api/common/goods/goodsBaseModify", ReqGoods.class, BaseResp.class, MethodType.JSON),
    common_goods_goodsExtModify("商品扩展信息编辑  售卖规则/赠品规则/配送规则/服务选项", "/f/pds/api/common/goods/goodsExtModify", ReqGoods.class, BaseResp.class, MethodType.POST),
    common_goods_goodsStatusModify("商品上下架  批量", "/f/pds/api/common/goods/goodsStatusModify", ReqGoods.class, BaseResp.class, MethodType.POST),
    common_goods_goodsSortModify("商品权重  排序修改", "/f/pds/api/common/goods/goodsSortModify", ReqGoods.class, BaseResp.class, MethodType.POST),
    common_goods_goodsStorageModify("商品库存  修改", "/f/pds/api/common/goods/goodsStorageModify", ReqGoods.class, BaseResp.class, MethodType.POST),
    common_goods_goodsCategoryDelete("商品分类删除  批量", "/f/pds/api/common/goods/goodsCategoryDelete", HashMap.class, BaseResp.class, MethodType.POST),
    common_goods_goodsCategoryModify("商品分类编辑", "/f/pds/api/common/goods/goodsCategoryModify", HashMap.class, BaseResp.class, MethodType.POST),
    common_goods_uploadPhoto("图片上传", "/f/pds/api/common/goods/uploadPhoto", MultipartFile.class, BaseResp.class, MethodType.POST),

    delivery_findDeliveryBuyerList("获取送货单列表", "/f/pds/api/delivery/driver/shipBuyerList", PdsShipBuyerApiReq.class, BaseResp.class, MethodType.POST),
    delivery_findShipOrderList("获取送货单明细", "/f/pds/api/delivery/driver/shipOrderDetails", PdsShipBuyerApiReq.class, BaseResp.class, MethodType.POST),
    delivery_truckLoad("装车", "/f/pds/api/delivery/operation/truckLoad", HashMap.class, BaseResp.class, MethodType.POST),
    delivery_depart("发车(司机)", "/f/pds/api/delivery/driver/depart", PdsShipOrderApiReq.class, BaseResp.class, MethodType.POST),
    delivery_shipOrderArrived("送货单送达(司机)", "/f/pds/api/delivery/driver/shipOrderArrived", HashMap.class, BaseResp.class, MethodType.POST),
    delivery_selfOrderArrived("自提单送达", "/f/pds/api/delivery/operation/selfOrderArrived", PdsAdmBaseReq.class, BaseResp.class, MethodType.POST),

    wechat_app_add("微信小程序用户关联", "/f/xiaodian/api/openuser/add",HashMap.class, BaseResp.class, MethodType.POST);

    private String desc;
    private String url;
    private Class reqClass;
    private Class respClass;
    private MethodType methodType;
}
