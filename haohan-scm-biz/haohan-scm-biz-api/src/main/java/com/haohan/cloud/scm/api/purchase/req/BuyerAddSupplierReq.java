package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "采购员新增供应商")
public class BuyerAddSupplierReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "supplierName不能为空")
    @ApiModelProperty(value = "供应商的全称",required = true)
    private String supplierName;

    @NotBlank(message = "telephone不能为空")
    @Length(min = 0, max = 11, message = "telephone长度在0-11个字符")
    private String telephone;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "采购员uid",required = true)
    private String uid;

    @NotBlank(message = "address不能为空")
    @ApiModelProperty(value = "供应商地址",required = true)
    private String address;

    @ApiModelProperty(value = "联系人")
    private String contact;




}
