/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 残次货品处理记录
 *
 * @author haohan
 * @date 2019-05-28 20:51:05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "残次货品处理记录")
public class ImperfectProductReq extends ImperfectProduct {

    private long pageSize;
    private long pageNo;




}
