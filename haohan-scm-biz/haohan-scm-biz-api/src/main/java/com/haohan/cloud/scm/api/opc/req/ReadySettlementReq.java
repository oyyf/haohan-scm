package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.req.settlement.SettlementPaymentReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/7/26
 */
@Data
@ApiModel(description = "准备结算")
public class ReadySettlementReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotEmpty(message = "paymentList不能为空")
    @ApiModelProperty(value = "结算账单列表", required = true)
    @Valid
    private List<SettlementPaymentReq> paymentList;


}
