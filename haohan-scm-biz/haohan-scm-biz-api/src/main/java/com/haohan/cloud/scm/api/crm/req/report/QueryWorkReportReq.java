package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.enums.crm.WorkReportTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/11/12
 */
@Data
@Api("工作日报查询")
public class QueryWorkReportReq {

    @Length(max = 64, message = "汇报编号的长度最大64字符")
    @ApiModelProperty(value = "汇报编号")
    private String reportSn;

    @Length(max = 64, message = "汇报人id的长度最大64字符")
    @ApiModelProperty(value = "汇报人id")
    private String reportManId;

    @ApiModelProperty(value = "工作汇报类型: 1.日常 2.销量 3.库存")
    private WorkReportTypeEnum reportType;

    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private ReportStatusEnum status;

    @Length(max = 10, message = "汇报人名称的长度最大10字符")
    @ApiModelProperty(value = "汇报人名称")
    private String reportMan;

    @Length(max = 15, message = "汇报人手机号的长度最大15字符")
    @ApiModelProperty(value = "汇报人手机号")
    private String reportTel;

    @ApiModelProperty(value = "查询汇报时间-开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "查询汇报时间-结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "查询汇报时间-开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询汇报时间-结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    public WorkReport transTo() {
        WorkReport report = new WorkReport();
        report.setReportSn(this.reportSn);
        report.setReportManId(this.reportManId);
        report.setStatus(this.status);
        report.setReportType(this.reportType);
        return report;
    }
}
