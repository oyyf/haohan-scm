/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.saleb.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-13 20:34:39
 */
@Data
@TableName("scm_ops_buy_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单")
public class BuyOrder extends Model<BuyOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 采购商名称
     */
    private String buyerName;
    /**
     * 采购用户
     */
    private String buyerUid;
    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;
    /**
     * 送货批次
     */
    private BuySeqEnum buySeq;
    /**
     * 采购需求
     */
    private String needNote;
    /**
     * 总价预估
     */
    private BigDecimal genPrice;
    /**
     * 采购总价
     */
    private BigDecimal totalPrice;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 配送地址
     */
    private String address;
    /**
     * 状态
     */
    private BuyOrderStatusEnum status;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 运费
     */
    private BigDecimal shipFee;
    /**
     * 配送方式
     */
    private DeliveryTypeEnum deliveryType;
    /**
     * 零售单号  （存放来源订单编号, 如商城订单号）
     */
    private String goodsOrderId;

    @ApiModelProperty(value = "商品种类数")
    private Integer goodsNum;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
