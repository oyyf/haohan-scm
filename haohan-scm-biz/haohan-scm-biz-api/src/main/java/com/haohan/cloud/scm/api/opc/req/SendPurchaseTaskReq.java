package com.haohan.cloud.scm.api.opc.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/4
 */
@Data
@ApiModel(description = "运营 向采购部 发采购任务")
public class SendPurchaseTaskReq {

  @NotBlank(message = "pmId不能为空")
  @ApiModelProperty(value = "平台id",required = true)
  private String pmId;
  /**
   * 采购明细编号列表
   */
  @NotNull(message = "purchaseDetailSnList不能为空")
  @ApiModelProperty(value = "采购明细编号列表",required = true)
  private List<String> purchaseDetailSnList;
  /**
   * 采购订单分类
   */
  @NotBlank(message = "purchaseOrderType采购订单分类不能为空")
  @ApiModelProperty(value = "采购订单分类",required = true)
  private String purchaseOrderType;
  /**
   *  采购截至时间
   */
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @ApiModelProperty(value = "采购截至时间")
  private LocalDateTime buyFinalTime;
  /**
   * 审核方式1.不审核2.需审核
   */
  @ApiModelProperty(value = "审核方式1.不审核2.需审核")
  private String reviewType;
  /**
   * 发起人
   */
  @ApiModelProperty(value = "发起人")
  private String initiatorId;
}
