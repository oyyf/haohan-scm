package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class CompanyNatureEnumConverterUtil implements Converter<CompanyNatureEnum> {
    @Override
    public CompanyNatureEnum convert(Object o, CompanyNatureEnum companyNatureEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return CompanyNatureEnum.getByType(o.toString());
    }
}
