package com.haohan.cloud.scm.api.goods.req.manage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/6/6
 * 平台商品定价查询使用
 */
@Data
public class GoodsPricingReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "商品店铺id的长度最大32字符")
    @ApiModelProperty("商品店铺id,用于联查平台商品定价")
    private String shopId;

    @Length(max = 32, message = "商品id的长度最大32字符")
    @ApiModelProperty("商品id,用于联查平台商品定价")
    private String goodsId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("平台商品定价查询日期")
    private LocalDate pricingDate;

    @Length(max = 32, message = "采购商id的长度最大32字符")
    @ApiModelProperty("采购商id,用于联查平台商品定价")
    private String buyerId;

    @Length(max = 32, message = "平台商品定价商家id的长度最大32字符")
    @ApiModelProperty("平台商品定价商家id")
    private String pricingMerchantId;



}
