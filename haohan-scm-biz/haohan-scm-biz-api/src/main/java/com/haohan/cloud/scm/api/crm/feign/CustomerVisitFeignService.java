/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import com.haohan.cloud.scm.api.crm.req.CustomerVisitReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户拜访记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerVisitFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerVisitFeignService {


    /**
     * 通过id查询客户拜访记录
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerVisit/{id}", method = RequestMethod.GET)
    R<CustomerVisit> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户拜访记录 列表信息
     *
     * @param customerVisitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerVisit/fetchCustomerVisitPage")
    R<Page<CustomerVisit>> getCustomerVisitPage(@RequestBody CustomerVisitReq customerVisitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户拜访记录 列表信息
     *
     * @param customerVisitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerVisit/fetchCustomerVisitList")
    R<List<CustomerVisit>> getCustomerVisitList(@RequestBody CustomerVisitReq customerVisitReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户拜访记录
     *
     * @param customerVisit 客户拜访记录
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/add")
    R<Boolean> save(@RequestBody CustomerVisit customerVisit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户拜访记录
     *
     * @param customerVisit 客户拜访记录
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/update")
    R<Boolean> updateById(@RequestBody CustomerVisit customerVisit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户拜访记录
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/listByIds")
    R<List<CustomerVisit>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerVisitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/countByCustomerVisitReq")
    R<Integer> countByCustomerVisitReq(@RequestBody CustomerVisitReq customerVisitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerVisitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/getOneByCustomerVisitReq")
    R<CustomerVisit> getOneByCustomerVisitReq(@RequestBody CustomerVisitReq customerVisitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerVisitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerVisit/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerVisit> customerVisitList, @RequestHeader(SecurityConstants.FROM) String from);


}
