package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.DeliverySeqEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class DeliverySeqEnumConverterUtil implements Converter<DeliverySeqEnum> {
    @Override
    public DeliverySeqEnum convert(Object o, DeliverySeqEnum deliverySeqEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DeliverySeqEnum.getByType(o.toString());
    }
}
