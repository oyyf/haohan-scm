package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class BuySeqEnumConverterUtil implements Converter<BuySeqEnum> {

    @Override
    public BuySeqEnum convert(Object o, BuySeqEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BuySeqEnum.getByType(o.toString());
    }

}
