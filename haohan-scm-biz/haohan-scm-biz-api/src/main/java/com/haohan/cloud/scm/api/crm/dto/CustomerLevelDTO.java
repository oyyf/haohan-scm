package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.market.LevelEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerLevel;
import com.haohan.cloud.scm.api.crm.entity.CustomerMerchantRelation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/27
 */
@Data
public class CustomerLevelDTO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 父级ID
     */
    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "父级名称")
    private String parentName;
    /**
     * 排序值
     */
    @ApiModelProperty(value = "排序值")
    private String sort;
    /**
     * 层级
     */
    @ApiModelProperty(value = "层级")
    private LevelEnum level;

    /**
     * 客户关联的商家
     */
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    public CustomerLevelDTO(CustomerLevel customerLevel, CustomerLevel parent, CustomerMerchantRelation relation) {
        if (null != customerLevel) {
            this.id = customerLevel.getId();
            this.customerSn = customerLevel.getCustomerSn();
            this.customerName = customerLevel.getCustomerName();
            this.parentId = customerLevel.getParentId();
            this.sort = customerLevel.getSort();
            this.level = customerLevel.getLevel();
        }
        if (null != parent) {
            this.parentName = parent.getCustomerName();
        }
        if (null != relation) {
            this.merchantId = relation.getMerchantId();
            this.merchantName = relation.getMerchantName();
        }
    }

}
