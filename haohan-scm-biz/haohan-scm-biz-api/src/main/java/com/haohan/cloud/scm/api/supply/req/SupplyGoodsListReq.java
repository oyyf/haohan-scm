package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "查询货源信息列表-小程序")
public class SupplyGoodsListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    @ApiModelProperty(value = "每页显示条数" )
    private long pageSize=10;

    @ApiModelProperty(value = "页码" )
    private long pageNo=1;
}
