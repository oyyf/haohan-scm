package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;

/**
 * @author dy
 * @date 2019/9/27
 */
public class PayStatusEnumConverterUtil implements Converter<PayStatusEnum> {
    @Override
    public PayStatusEnum convert(Object o, PayStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PayStatusEnum.getByType(o.toString());
    }

}