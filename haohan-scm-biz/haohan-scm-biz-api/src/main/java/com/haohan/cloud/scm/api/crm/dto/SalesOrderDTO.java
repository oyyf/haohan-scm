package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/27
 */
@Data
@NoArgsConstructor
public class SalesOrderDTO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "销售订单编号")
    private String salesOrderSn;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "供货商编号")
    private String supplierSn;

    @ApiModelProperty(value = "供货商名称")
    private String supplierName;

    @ApiModelProperty(value = "下单时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;

    @ApiModelProperty(value = "交货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    @ApiModelProperty(value = "合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "总计金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "销售单类型:1代客下单,2自主下单")
    private SalesTypeEnum salesType;

    @ApiModelProperty(value = "业务员")
    private String employeeId;

    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @ApiModelProperty(value = "收货人id")
    private String linkmanId;

    @ApiModelProperty(value = "收货人名称")
    private String linkmanName;

    @ApiModelProperty(value = "收货地址")
    private String address;

    @ApiModelProperty(value = "收货人手机号")
    private String telephone;

    @ApiModelProperty(value = "审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "货品种数")
    private Integer goodsNum;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    @ApiModelProperty(value = "外部订单号")
    private String externalSn;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "订单明细")
    private List<SalesOrderDetail> detailList;

    public SalesOrderDTO(SalesOrder salesOrder) {
        this.id = salesOrder.getId();
        this.salesOrderSn = salesOrder.getSalesOrderSn();
        this.customerSn = salesOrder.getCustomerSn();
        this.customerName = salesOrder.getCustomerName();
        this.supplierSn = salesOrder.getSupplierSn();
        this.supplierName = salesOrder.getSupplierName();
        this.orderTime = salesOrder.getOrderTime();
        this.deliveryDate = salesOrder.getDeliveryDate();
        this.sumAmount = salesOrder.getSumAmount();
        this.otherAmount = salesOrder.getOtherAmount();
        this.totalAmount = salesOrder.getTotalAmount();
        this.salesType = salesOrder.getSalesType();
        this.employeeId = salesOrder.getEmployeeId();
        this.employeeName = salesOrder.getEmployeeName();
        this.linkmanId = salesOrder.getLinkmanId();
        this.linkmanName = salesOrder.getLinkmanName();
        this.address = salesOrder.getAddress();
        this.telephone = salesOrder.getTelephone();
        this.reviewStatus = salesOrder.getReviewStatus();
        this.goodsNum = salesOrder.getGoodsNum();
        this.payStatus = salesOrder.getPayStatus();
        this.externalSn = salesOrder.getExternalSn();
        this.remarks = salesOrder.getRemarks();
    }

    public SalesOrder transTo() {
        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setId(this.id);
        salesOrder.setSalesOrderSn(this.salesOrderSn);
        salesOrder.setCustomerSn(this.customerSn);
        salesOrder.setCustomerName(this.customerName);
        salesOrder.setSupplierSn(this.supplierSn);
        salesOrder.setSupplierName(this.supplierName);
        salesOrder.setOrderTime(this.orderTime);
        salesOrder.setDeliveryDate(this.deliveryDate);
        salesOrder.setSumAmount(this.sumAmount);
        salesOrder.setOtherAmount(this.otherAmount);
        salesOrder.setTotalAmount(this.totalAmount);
        salesOrder.setSalesType(this.salesType);
        salesOrder.setEmployeeId(this.employeeId);
        salesOrder.setEmployeeName(this.employeeName);
        salesOrder.setLinkmanId(this.linkmanId);
        salesOrder.setLinkmanName(this.linkmanName);
        salesOrder.setAddress(this.address);
        salesOrder.setTelephone(this.telephone);
        salesOrder.setReviewStatus(this.reviewStatus);
        salesOrder.setGoodsNum(this.goodsNum);
        salesOrder.setPayStatus(this.payStatus);
        salesOrder.setExternalSn(this.externalSn);
        salesOrder.setRemarks(this.remarks);
        return salesOrder;
    }
}
