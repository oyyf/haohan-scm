/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 库存调拨明细记录
 *
 * @author haohan
 * @date 2019-05-28 19:51:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "库存调拨明细记录")
public class WarehouseAllotDetailReq extends WarehouseAllotDetail {

    private long pageSize;
    private long pageNo;




}
