/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 销售订单明细
 *
 * @author haohan
 * @date 2019-09-04 18:32:09
 */
@Data
@TableName("crm_sales_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售订单明细")
public class SalesOrderDetail extends Model<SalesOrderDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 销售单编号
     */
    @ApiModelProperty(value = "销售单编号")
    private String salesOrderSn;
    /**
     * 销售单明细编号
     */
    @ApiModelProperty(value = "销售单明细编号")
    private String salesDetailSn;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 商品销售类型:1.普通2.促销品3.赠品
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;
    /**
     * 商品规格ID
     */
    @ApiModelProperty(value = "商品规格ID")
    private String goodsModelId;
    /**
     * 商品规格编号
     */
    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;
    /**
     * 商品图片
     */
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 商品规格名称
     */
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;
    /**
     * 采购数量
     */
    @ApiModelProperty(value = "采购数量")
    private BigDecimal goodsNum;
    /**
     * 市场价格
     */
    @ApiModelProperty(value = "市场价格")
    private BigDecimal marketPrice;
    /**
     * 成交价格
     */
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String unit;
    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
