package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAddLendingReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    /**
     * 主键
     */
    private String id;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 申请人
     */
    private String applicantId;
    /**
     * 申请人名称
     */
    private String applicantName;
    /**
     * 申请内容
     */
    private String applicantContent;
    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 审核人
     */
    private String auditorId;
    /**
     * 审核人名称
     */
    private String auditorName;
    /**
     * 审核人意见
     */
    private String auditorOpinion;
    /**
     * 审核时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;
    /**
     * 复审人
     */
    private String reviewerId;
    /**
     * 复审人名称
     */
    private String reviewerName;
    /**
     * 复审人意见
     */
    private String reviewerOpinion;
    /**
     * 复审时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reviewTime;
    /**
     * 申请金额
     */
    private BigDecimal applyAmount;
    /**
     * 收款人
     */
    private String payee;
    /**
     * 收款人名称
     */
    private String payeeName;
    /**
     * 放款金额
     */
    private BigDecimal lendingAmount;
    /**
     * 放款人
     */
    private String lenderId;
    /**
     * 放款人名称
     */
    private String lenderName;
    /**
     * 放款时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lendingTime;
    /**
     * 放款说明
     */
    private String lengingContent;
    /**
     * 请款状态:1.已申请2.通过初审3.待复审4.审核未通过5.待放款6.已放款
     */
    private String lendingStatus;
    /**
     * 备注信息
     */
    private String remarks;

}
