package com.haohan.cloud.scm.api.constant.enums.bill;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/11/26
 */
@Getter
@AllArgsConstructor
public enum SettlementStyleEnum implements IBaseEnum {

    /**
     * 结算单类型: 1.普通, 2.汇总, 3.明细
     */
    normal("1", "普通"),
    summary("2", "汇总"),
    detail("3", "明细");

    private static final Map<String, SettlementStyleEnum> MAP = new HashMap<>(8);

    static {
        for (SettlementStyleEnum e : SettlementStyleEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SettlementStyleEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
