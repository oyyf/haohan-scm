package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/6/12
 */
@Data
@ApiModel(description = "协议供应确认供应商价格请求")
public class AgreementPurchaseReq {

  @NotBlank(message = "pmId不能为空")
  @ApiModelProperty(value = "平台商家ID" , required = true)
  private String pmId;

  @NotBlank(message = "id不能为空")
  @ApiModelProperty(value = "采购单明细ID" , required = true)
  private String id;

  @NotBlank(message = "uId不能为空")
  @ApiModelProperty(value = "用户ID" , required = true)
  private String uId;

  @ApiModelProperty(value = "采购价格" )
  private BigDecimal buyPrice;
}
