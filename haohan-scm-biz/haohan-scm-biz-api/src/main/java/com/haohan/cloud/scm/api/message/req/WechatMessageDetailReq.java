/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.WechatMessageDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息模板明细
 *
 * @author haohan
 * @date 2019-05-28 20:06:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "消息模板明细")
public class WechatMessageDetailReq extends WechatMessageDetail {

    private long pageSize;
    private long pageNo;




}
