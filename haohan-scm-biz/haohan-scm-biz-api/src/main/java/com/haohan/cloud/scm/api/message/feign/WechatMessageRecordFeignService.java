/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WechatMessageRecord;
import com.haohan.cloud.scm.api.message.req.WechatMessageRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 微信消息记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WechatMessageRecordFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WechatMessageRecordFeignService {


    /**
     * 通过id查询微信消息记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WechatMessageRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 微信消息记录表 列表信息
     * @param wechatMessageRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessageRecord/fetchWechatMessageRecordPage")
    R getWechatMessageRecordPage(@RequestBody WechatMessageRecordReq wechatMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 微信消息记录表 列表信息
     * @param wechatMessageRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessageRecord/fetchWechatMessageRecordList")
    R getWechatMessageRecordList(@RequestBody WechatMessageRecordReq wechatMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增微信消息记录表
     * @param wechatMessageRecord 微信消息记录表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/add")
    R save(@RequestBody WechatMessageRecord wechatMessageRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改微信消息记录表
     * @param wechatMessageRecord 微信消息记录表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/update")
    R updateById(@RequestBody WechatMessageRecord wechatMessageRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除微信消息记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WechatMessageRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param wechatMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/countByWechatMessageRecordReq")
    R countByWechatMessageRecordReq(@RequestBody WechatMessageRecordReq wechatMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param wechatMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/getOneByWechatMessageRecordReq")
    R getOneByWechatMessageRecordReq(@RequestBody WechatMessageRecordReq wechatMessageRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param wechatMessageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WechatMessageRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WechatMessageRecord> wechatMessageRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
