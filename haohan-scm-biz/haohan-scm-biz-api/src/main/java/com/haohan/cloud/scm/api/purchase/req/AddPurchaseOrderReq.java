package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/4
 */
@Data
public class AddPurchaseOrderReq {

  @NotBlank(message = "pmId不能为空")
  private String pmId;

  @NotEmpty(message = "purchaseDetailSn不能为空")
  List<String> purchaseDetailSn;
}
