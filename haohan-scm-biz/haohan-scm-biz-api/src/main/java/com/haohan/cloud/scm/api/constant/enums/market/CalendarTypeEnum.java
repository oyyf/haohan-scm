package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum CalendarTypeEnum implements IBaseEnum {
    /**
     * 日历类型:1.公历2.农历
     */
    gregorianCalendar("1","公历"),
    lunarCalendar("2","农历");

    private static final Map<String, CalendarTypeEnum> MAP = new HashMap<>(8);

    static {
        for (CalendarTypeEnum e : CalendarTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static CalendarTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
