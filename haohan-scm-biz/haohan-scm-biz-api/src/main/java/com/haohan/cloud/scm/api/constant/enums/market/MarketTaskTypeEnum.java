package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum MarketTaskTypeEnum {
    /**
     * 任务类别:1.售后2.收款
     */
    afterSale("1","售后"),
    proceeds("2","收款");

    private static final Map<String, MarketTaskTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MarketTaskTypeEnum e : MarketTaskTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MarketTaskTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
