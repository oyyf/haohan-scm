package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/7/8
 */
@Data
public class SummaryExitNumReq {

    @ApiModelProperty(value = "平台id")
    private String pmId;
    /**
     * 送货批次:0第一批1第二批
     */
    @ApiModelProperty(value = "送货批次", example = "0")
    private String deliverySeq;
    private String goodsModelId;
    private String exitStatus;
    private String exitType;
    /**
     * 送货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "送货时间", example = "2019-07-08")
    private LocalDate deliveryDate;
    private String warehouseSn;

    public SummaryExitNumReq copyFrom(ExitWarehouseDetail detail) {
        this.setPmId(detail.getPmId());
        if (null != detail.getDeliverySeq()) {
            this.setDeliverySeq(detail.getDeliverySeq().getType());
        }
        this.setGoodsModelId(detail.getGoodsModelId());
        if (null != detail.getExitStatus()) {
            this.setExitStatus(detail.getExitStatus().getType());
        }
        if (null != detail.getExitType()) {
            this.setExitType(detail.getExitType().getType());
        }
        this.setDeliveryDate(detail.getDeliveryDate());
        this.setWarehouseSn(detail.getWarehouseSn());
        return this;
    }

}
