package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("商品销售数据")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoodsSalesVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @ApiModelProperty(value = "单位")
    private String goodsUnit;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "回款金额")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "商品数")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private String salesGoodsType;
}
