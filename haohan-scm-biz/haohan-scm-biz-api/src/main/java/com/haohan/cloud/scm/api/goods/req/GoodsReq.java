/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.Goods;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-28 19:55:33
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品")
public class GoodsReq extends Goods {

    private long pageSize;
    private long pageNo;




}
