package com.haohan.cloud.scm.api.crm.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/7
 * 最近的市场和区域
 */
@Data
public class NearbyMarketAndAreaVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("和市场定位的距离(公里)")
    private BigDecimal distance;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @ApiModelProperty(value = "市场名称")
    private String marketName;

    @ApiModelProperty(value = "区域编码")
    private String areaSn;

    @ApiModelProperty(value = "区域名称")
    private String areaName;


}
