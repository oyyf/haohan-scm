/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.purchase.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.purchase.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-13 18:48:01
 */
@Data
@TableName("scm_pms_purchase_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购部采购单明细")
public class PurchaseOrderDetail extends Model<PurchaseOrderDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 汇总明细编号
     */
    private String summaryDetailSn;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 商品分类id
     */
    private String goodsCategoryId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品分类名称
     */
    private String goodsCategoryName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭
     */
    private PurchaseStatusEnum purchaseStatus;
    /**
     * 采购订单分类:1.采购计划2.按需采购
     */
    private PurchaseOrderTypeEnum purchaseOrderType;
    /**
     * 采购截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyFinalTime;
    /**
     * 发起人
     */
    private String initiatorId;
    /**
     * 执行人
     */
    private String transactorId;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 采购价
     */
    private BigDecimal buyPrice;
    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;
    /**
     * 实际采购数量
     */
    private BigDecimal realBuyNum;
    /**
     * 揽货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receiveTime;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 揽货方式1.自提2.送货上门
     */
    private ReceiveTypeEnum receiveType;
    /**
     * 采购执行状态 1.正常2.异常
     */
    private ActionTypeEnum actionType;
    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    private MethodTypeEnum methodType;
    /**
     * 竞价截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime biddingEndTime;
    /**
     * 报价状态:1.无需报价2.未报价3.已报价
     */
    private OfferTypeEnum offerType;
    /**
     * 请款类型1.不请款2.需请款
     */
    private LendingTypeEnum lendingType;
    /**
     * 请款人类型:1.发起人2.执行人3.供应商
     */
    private LenderTypeEnum lenderType;
    /**
     * 来源采购单明细编号
     */
    private String originalPurchaseDetailSn;
    /**
     * 中标状态:0.不开标1.已中标2.落标
     */
    private BiddingStatusEnum biddingStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */

    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
