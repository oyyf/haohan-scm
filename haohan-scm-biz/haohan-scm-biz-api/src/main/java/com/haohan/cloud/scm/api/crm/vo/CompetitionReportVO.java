package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.DataReportDTO;
import com.haohan.cloud.scm.api.crm.entity.DataReport;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/22
 * 竞品上报信息 只有一个竞品
 */
@Data
@NoArgsConstructor
public class CompetitionReportVO {

    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "上报位置定位")
    private String reportLocation;

    @ApiModelProperty(value = "上报位置地址")
    private String reportAddress;

    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @ApiModelProperty(value = "上报人名称")
    private String reportMan;

    @ApiModelProperty(value = "上报日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @ApiModelProperty(value = "上报人手机号")
    private String reportTelephone;

    @ApiModelProperty(value = "上报状态 0.待确认 1.已确认 2.不通过")
    private DataReportStatusEnum reportStatus;

    @ApiModelProperty(value = "商品总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "图片列表")
    private List<PhotoGallery> photoList;

    // 商品信息

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品单位")
    private String goodsUnit;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal tradePrice;

    @ApiModelProperty(value = "数量")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    /**
     * 竞品上报信息, 不含商品(回传上报编号)
     *
     * @param report
     */
    public CompetitionReportVO(DataReportDTO report) {
        BeanUtil.copyProperties(report, this);
    }

    public CompetitionReportVO(DataReport report) {
        BeanUtil.copyProperties(report, this);
    }

    public void copyFromDetail(DataReportDetail detail) {
        this.goodsName = detail.getGoodsName();
        this.goodsUnit = detail.getGoodsUnit();
        this.tradePrice = detail.getTradePrice();
        this.goodsNum = detail.getGoodsNum();
        this.amount = this.tradePrice.multiply(this.goodsNum);
        this.salesGoodsType = detail.getSalesGoodsType();
    }
}
