package com.haohan.cloud.scm.api.opc.req.ship;

import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/3/4
 */
@Data
public class QueryShipperReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    // 非eq

    @Length(max = 64, message = "发货人姓名长度最大64字符")
    @ApiModelProperty(value = "发货人姓名")
    private String shipperName;

    @Length(max = 64, message = "电话长度最大64字符")
    @ApiModelProperty(value = "电话")
    private String telephone;

    @Length(max = 64, message = "发货地址长度最大64字符")
    @ApiModelProperty(value = "发货地址")
    private String shipAddress;

}
