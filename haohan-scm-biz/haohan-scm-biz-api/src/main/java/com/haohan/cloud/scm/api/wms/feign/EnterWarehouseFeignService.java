/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseByOrderDetailReq;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 入库单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "EnterWarehouseFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface EnterWarehouseFeignService {


    /**
     * 通过id查询入库单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/EnterWarehouse/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 入库单 列表信息
     * @param enterWarehouseReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouse/fetchEnterWarehousePage")
    R getEnterWarehousePage(@RequestBody EnterWarehouseReq enterWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 入库单 列表信息
     * @param enterWarehouseReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouse/fetchEnterWarehouseList")
    R getEnterWarehouseList(@RequestBody EnterWarehouseReq enterWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/add")
    R save(@RequestBody EnterWarehouse enterWarehouse, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/update")
    R updateById(@RequestBody EnterWarehouse enterWarehouse, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除入库单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/EnterWarehouse/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/countByEnterWarehouseReq")
    R countByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/getOneByEnterWarehouseReq")
    R getOneByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param enterWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/EnterWarehouse/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<EnterWarehouse> enterWarehouseList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增入库单（入库单明细）根据采购单明细
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouse/enterWarehouseByOrderDetail")
    R enterWarehouseByOrderDetail(@RequestBody EnterWarehouseByOrderDetailReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询订单退货入库单详情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/EnterWarehouse/queryEnterOrderDetail")
    R queryEnterOrderDetail(@RequestBody EnterWarehouseReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
