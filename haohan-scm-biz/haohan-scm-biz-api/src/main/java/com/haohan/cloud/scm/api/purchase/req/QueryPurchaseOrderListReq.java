package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/3
 */
@Data
@ApiModel(description = "查询采购单列表")
public class QueryPurchaseOrderListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @ApiModelProperty(value = "采购员工通行证id", required = true)
    private String uid;

    @ApiModelProperty(value = "分页大小")
    private long size = 10;
    @ApiModelProperty(value = "分页当前页码")
    private long current = 1;
}
