package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/25
 */
@Getter
@AllArgsConstructor
public enum AreaTypeEnum implements IBaseEnum {

    /**
     * 地区等级： 0.全国 1.省 2.市 3.区 4.街道
     */
    country("0", "全国") {
        @Override
        public AreaTypeEnum getNext() {
            return province;
        }
    },
    province("1", "省") {
        @Override
        public AreaTypeEnum getNext() {
            return city;
        }
    },
    city("2", "市") {
        @Override
        public AreaTypeEnum getNext() {
            return district;
        }
    },
    district("3", "区") {
        @Override
        public AreaTypeEnum getNext() {
            return street;
        }
    },
    street("4", "街道");

    private static final Map<String, AreaTypeEnum> MAP = new HashMap<>(8);

    static {
        for (AreaTypeEnum e : AreaTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AreaTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;

    public AreaTypeEnum getNext() {
        return this;
    }
}
