/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */


package com.haohan.cloud.scm.api.pay.req;

import com.haohan.cloud.scm.api.pay.entity.RefundManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 退款管理
 *
 * @author haohan
 * @date 2020-06-10 19:17:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退款管理")
public class RefundManageReq extends RefundManage {

    private long pageSize;
    private long pageNo;

}
