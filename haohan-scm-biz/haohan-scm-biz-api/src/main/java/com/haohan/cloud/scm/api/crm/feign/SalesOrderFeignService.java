/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import com.haohan.cloud.scm.api.crm.req.SalesOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 销售订单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesOrderFeignService", value = ScmServiceName.SCM_CRM)
public interface SalesOrderFeignService {


    /**
     * 通过id查询销售订单
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesOrder/{id}", method = RequestMethod.GET)
    R<SalesOrder> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 销售订单 列表信息
     *
     * @param salesOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesOrder/fetchSalesOrderPage")
    R<Page<SalesOrder>> getSalesOrderPage(@RequestBody SalesOrderReq salesOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 销售订单 列表信息
     *
     * @param salesOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesOrder/fetchSalesOrderList")
    R<List<SalesOrder>> getSalesOrderList(@RequestBody SalesOrderReq salesOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增销售订单
     *
     * @param salesOrder 销售订单
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/add")
    R<Boolean> save(@RequestBody SalesOrder salesOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改销售订单
     *
     * @param salesOrder 销售订单
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/update")
    R<Boolean> updateById(@RequestBody SalesOrder salesOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除销售订单
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/listByIds")
    R<List<SalesOrder>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/countBySalesOrderReq")
    R<Integer> countBySalesOrderReq(@RequestBody SalesOrderReq salesOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/getOneBySalesOrderReq")
    R<SalesOrder> getOneBySalesOrderReq(@RequestBody SalesOrderReq salesOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SalesOrder/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesOrder> salesOrderList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 获取订单详细
     *
     * @param orderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/SalesOrder/orderInfo")
    R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改订单结算状态
     *
     * @param salesOrderReq orderSn 、payStatus
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SalesOrder/updateSettlement")
    R<Boolean> updateOrderSettlement(@RequestBody SalesOrderReq salesOrderReq, @RequestHeader(SecurityConstants.FROM) String from);
}
