/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.CostControl;
import com.haohan.cloud.scm.api.opc.req.CostControlReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 成本管控内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CostControlFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface CostControlFeignService {


    /**
     * 通过id查询成本管控
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CostControl/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 成本管控 列表信息
     * @param costControlReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CostControl/fetchCostControlPage")
    R getCostControlPage(@RequestBody CostControlReq costControlReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 成本管控 列表信息
     * @param costControlReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CostControl/fetchCostControlList")
    R getCostControlList(@RequestBody CostControlReq costControlReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增成本管控
     * @param costControl 成本管控
     * @return R
     */
    @PostMapping("/api/feign/CostControl/add")
    R save(@RequestBody CostControl costControl, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改成本管控
     * @param costControl 成本管控
     * @return R
     */
    @PostMapping("/api/feign/CostControl/update")
    R updateById(@RequestBody CostControl costControl, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除成本管控
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CostControl/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/CostControl/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CostControl/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param costControlReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CostControl/countByCostControlReq")
    R countByCostControlReq(@RequestBody CostControlReq costControlReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param costControlReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CostControl/getOneByCostControlReq")
    R getOneByCostControlReq(@RequestBody CostControlReq costControlReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param costControlList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CostControl/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<CostControl> costControlList, @RequestHeader(SecurityConstants.FROM) String from);


}
