package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum PurchaseStatusEnum implements IBaseEnum {
    /**
     * 采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭
     */
    pending("1","待处理"),
    checking("2","待审核"),
    purchasing("3","采购中"),
    stockUp("4","备货中"),
    haveSales("5","已揽货"),
    finish("6","采购完成"),
    partly_completed("7","部分完成"),
    close("8","已关闭");

    private static final Map<String, PurchaseStatusEnum> MAP = new HashMap<>(8);

    static {
        for (PurchaseStatusEnum e : PurchaseStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PurchaseStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;


}
