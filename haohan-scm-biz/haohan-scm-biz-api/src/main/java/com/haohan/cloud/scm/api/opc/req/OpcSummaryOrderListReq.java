package com.haohan.cloud.scm.api.opc.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author xwx
 * @date 2019/6/21
 */
@Data
@ApiModel(description = "返回汇总临时数据列表")
public class OpcSummaryOrderListReq {
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;
    /**
     * 送货批次
     */
    @ApiModelProperty(value = "送货批次", required = true)
    private String buySeq;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "送货日期", required = true, example = "2019-05-28")
    private LocalDate deliveryTime;

    @ApiModelProperty(value = "商品姓名")
    private String goodsName;
}
