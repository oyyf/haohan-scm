/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.bill.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-13 20:39:12
 */
@Data
@TableName("scm_settlement_account")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "结算记录")
public class SettlementAccount extends BaseEntity<SettlementAccount> {
    private static final long serialVersionUID = 1L;

    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 平台商家名称
     */
    private String pmName;
    /**
     * 结算公司id: merchantId
     */
    private String companyId;
    /**
     * 结算公司名称
     */
    private String companyName;
    /**
     * 结算订单编号
     */
    private String orderSn;
    /**
     * 结算账单编号
     */
    private String billSn;

    // 结算相关
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;
    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;
    /**
     * 结算时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 是否结算:0否1是
     */
    private YesNoEnum settlementStatus;
    /**
     * 结算单类型: 1.普通, 2.汇总, 3.明细
     */
    private SettlementStyleEnum settlementStyle;
    /**
     * 汇总结算单编号
     */
    private String summarySettlementSn;

    // 记录信息
    /**
     * 结款人名称
     */
    private String companyOperator;
    /**
     * 结算凭证图片组编号
     */
    private String groupNum;
    /**
     * 结算说明
     */
    private String settlementDesc;
    /**
     * 经办人名称
     */
    private String operatorName;

}
