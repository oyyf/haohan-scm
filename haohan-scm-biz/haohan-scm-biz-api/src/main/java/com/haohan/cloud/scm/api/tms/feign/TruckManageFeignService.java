/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.tms.entity.TruckManage;
import com.haohan.cloud.scm.api.tms.req.TruckManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 车辆管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "TruckManageFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface TruckManageFeignService {


    /**
     * 通过id查询车辆管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/TruckManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 车辆管理 列表信息
     *
     * @param truckManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TruckManage/fetchTruckManagePage")
    R getTruckManagePage(@RequestBody TruckManageReq truckManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 车辆管理 列表信息
     *
     * @param truckManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TruckManage/fetchTruckManageList")
    R getTruckManageList(@RequestBody TruckManageReq truckManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增车辆管理
     *
     * @param truckManage 车辆管理
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/add")
    R save(@RequestBody TruckManage truckManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改车辆管理
     *
     * @param truckManage 车辆管理
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/update")
    R updateById(@RequestBody TruckManage truckManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除车辆管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param truckManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/countByTruckManageReq")
    R countByTruckManageReq(@RequestBody TruckManageReq truckManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param truckManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/getOneByTruckManageReq")
    R getOneByTruckManageReq(@RequestBody TruckManageReq truckManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param truckManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/TruckManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<TruckManage> truckManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
