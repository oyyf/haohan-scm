package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.tree.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.poi.ss.formula.functions.T;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AreaTree extends TreeNode<AreaTree> {

    private String areaName;
    private String areaSn;
    private String parentName;

}
