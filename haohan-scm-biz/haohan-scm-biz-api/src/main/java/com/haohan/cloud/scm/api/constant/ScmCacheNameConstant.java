package com.haohan.cloud.scm.api.constant;

/**
 * @author dy
 * @date 2019/10/30
 * 缓存的key
 */
public interface ScmCacheNameConstant {

    // 通用

    /**
     * 定时任务执行标识
     */
    String SCM_SCHEDULED = "scm_scheduled:";

    // crm   crm_details

    /**
     * 员工通讯录(10小时过期)
     */
    String CRM_EMPLOYEE_CONTACT = "crm_details:employee_contact#36000";

    /**
     * 员工负责 区域(10小时过期)
     */
    String CRM_EMPLOYEE_AREA = "crm_details:employee_area#36000";

    /**
     * 客户层级树(10小时过期)
     */
    String CRM_CUSTOMER_TREE = "crm_details:customer_tree#36000";

    /**
     * 员工 统计 (5分钟过期)
     */
    String CRM_EMPLOYEE_STATISTICS = "crm_details:employee_customer#300";
    /**
     * 员工 拥有权限查看的客户 (10小时过期)
     */
    String CRM_EMPLOYEE_SERVICE_CUSTOMER = "crm_details:employee_service_customer#36000";
    /**
     * 员工拥有权限查看的客户  字符串(10小时过期)
     */
    String CRM_EMPLOYEE_SERVICE_CUSTOMER_STR = "crm_details:employee_service_customer_str#36000";
    /**
     * 员工 查询的所有客户 (5分钟过期)
     */
    String CRM_EMPLOYEE_CUSTOMER_LIST = "crm_details:employee_all_customer#300";
    /**
     * 员工 查询的部门员工 (10小时过期)
     */
    String CRM_EMPLOYEE_DEPT_EMPLOYEE_IDS = "crm_details:employee_dept_employee_ids#36000";

    /**
     * 附近定位（市场)  (5分钟过期)
     */
    String CRM_NEARBY_POSITION = "crm_details:nearby_position#300";

    /**
     * 查询销量上报统计数据时最小更新时间(5分钟)
     */
    String CRM_QUERY_SALES_REPORT_ANALYSIS = "crm_details:query_sales_report_analysis#300";

    /**
     * 创建销量上报统计数据时最小更新时间(1小时)
     */
    String CRM_CREATE_SALES_REPORT_ANALYSIS = "crm_details:create_sales_report_analysis#3600";

    /**
     * 查询销量上报统计数据时最小更新时间(5分钟)
     */
    String CRM_QUERY_SALES_ORDER_ANALYSIS = "crm_details:query_sales_order_analysis#300";

    // goods goods_details

    /**
     * 商品详情 (60分钟过期)
     */
    String GOODS_INFO_DETAIL = "goods_details:goods_info#3600";

    /**
     * 商品分类树(100小时过期)
     */
    String GOODS_CATEGORY_TREE = "goods_details:category_tree#360000";

    /**
     * 商品规格信息 (30分钟过期)
     */
    String GOODS_MODEL_DETAIL = "goods_details:goods_model#1800";

    // manage  manage_details

    /**
     * 商家信息(100小时过期)
     */
    String MANAGE_MERCHANT_INFO = "manage_details:merchant_info#360000";
    /**
     * 平台商家 (100小时过期)
     */
    String MANAGE_PLATFORM_MERCHANT = "manage_details:platform_merchant#360000";
    /**
     * 商家店铺 (100小时过期)
     */
    String MANAGE_MERCHANT_SHOP = "manage_details:merchant_shop#360000";

    /**
     * 使用定价的商家id (10小时过期) 和buyer有关
     */
    String MANAGE_PRICING_MERCHANT_ID = "manage_details:pricing_merchant#36000";

    /**
     * 定价的平台商家id (10小时过期) 和shop有关
     */
    String MANAGE_PRICING_PM_ID = "manage_details:pricing_pm#36000";

    // bill   bill_details

    /**
     * 结算时对比 随机码，单独设置过期时间
     */
    String SETTLEMENT_RANDOM_CODE = "bill_details:settlement:code";

    // photoGroup   photo_details
    /**
     * 图片组图片列表 (1小时过期)
     */
    String PHOTO_GROUP_LIST = "photo_details:photo_group_list#3600";

    // supply     supply_details
    /**
     * 供应商的商家店铺信息 (5分钟过期)
     */
    String SUPPLY_SUPPLIER_MERCHANT_INFO = "supply_details:supply_supplier_merchant_info#300";


    // wechat  wecaht_details

    /**
     * 微信小程序 通行证 (1小时过期)
     */
    String WECHAT_PASSPORT_INFO = "wecaht_details:passport_info#3600";
    /**
     * 微信小程序 采购商 (1小时过期)
     */
    String WECHAT_BUYER_INFO = "wecaht_details:buyer_info#3600";
    /**
     * 微信小程序 采购商常购买商品 (1小时过期)
     */
    String WECHAT_BUYER_OFTEN_GOODS = "wecaht_details:buyer_often_goods#3600";
    /**
     * 微信小程序 采购商商品列表 (1小时过期) 和goods有关
     */
    String WECHAT_BUYER_GOODS_PAGE = "wecaht_details:buyer_goods_page#3600";



}
