package com.haohan.cloud.scm.api.product.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/6
 */
@Data
public class AddShipReq {
    @NotBlank(message = "buyId不能为空")
    private String buyId;
}
