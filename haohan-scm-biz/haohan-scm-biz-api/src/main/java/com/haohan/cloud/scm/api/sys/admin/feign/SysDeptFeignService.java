package com.haohan.cloud.scm.api.sys.admin.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.admin.api.entity.SysDept;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/14
 */
@FeignClient(contextId = "SysDeptFeignService", value = ServiceNameConstants.UPMS_SERVICE)
public interface SysDeptFeignService {

    /**
     * 通过id查询
     *
     * @param id
     * @param from
     * @return
     */
    @GetMapping("/api/feign/dept/{id}")
    R<SysDept> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 列表查询
     *
     * @param dept
     * @param from
     * @return
     */
    @PostMapping("/api/feign/dept/list")
    R<List<SysDept>> fetchDeptList(@RequestBody SysDept dept, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 分页查询
     *
     * @param params
     * @param from
     * @return
     */
    @PostMapping("/api/feign/dept/page")
    R<Page<SysDept>> fetchDeptPage(@RequestBody Map<String, Object> params, @RequestHeader(SecurityConstants.FROM) String from);


}
