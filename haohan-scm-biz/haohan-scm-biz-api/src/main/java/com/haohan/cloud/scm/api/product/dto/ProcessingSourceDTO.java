package com.haohan.cloud.scm.api.product.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/6/13
 * 用于货品 拆分
 */
@Data
public class ProcessingSourceDTO {

    /**
     * 原货品
     */
    private String sourceProductSn;

    /**
     * 单个新货品数量
     */
    private BigDecimal subProductNum;

    /**
     * 多个新货品数量列表
     */
    private List<BigDecimal> subProductNumList;

    // 操作人

    // 加工时间

    // 暂存点编号


}
