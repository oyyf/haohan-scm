/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesReq;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 售后单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AfterSalesFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface AfterSalesFeignService {


    /**
     * 通过id查询售后单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/AfterSales/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 售后单 列表信息
     * @param afterSalesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AfterSales/fetchAfterSalesPage")
    R getAfterSalesPage(@RequestBody AfterSalesReq afterSalesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 售后单 列表信息
     * @param afterSalesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AfterSales/fetchAfterSalesList")
    R getAfterSalesList(@RequestBody AfterSalesReq afterSalesReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增售后单
     * @param afterSales 售后单
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/add")
    R save(@RequestBody AfterSales afterSales, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改售后单
     * @param afterSales 售后单
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/update")
    R updateById(@RequestBody AfterSales afterSales, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除售后单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/AfterSales/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param afterSalesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/countByAfterSalesReq")
    R countByAfterSalesReq(@RequestBody AfterSalesReq afterSalesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param afterSalesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/getOneByAfterSalesReq")
    R getOneByAfterSalesReq(@RequestBody AfterSalesReq afterSalesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param afterSalesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/AfterSales/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<AfterSales> afterSalesList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询售后单详情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/AfterSales/queryAfterSalesDetail")
    R queryAfterSalesDetail(@RequestBody QueryAfterSalesDetailReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
