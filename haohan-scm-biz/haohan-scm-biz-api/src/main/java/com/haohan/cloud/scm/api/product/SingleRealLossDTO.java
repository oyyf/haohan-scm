package com.haohan.cloud.scm.api.product;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "查询损耗率需求")
public class SingleRealLossDTO {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家平台id" , required = true)
    private String pmId;

    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id" , required = true)
    private String goodsModelId;

    @ApiModelProperty(value = "损耗类型:1.采购2.生产加工3.分拣4.配送5.仓储盘点6.调拨")
    private String lossType;
}
