package com.haohan.cloud.scm.api.crm.req.analysis;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2020/1/16
 * 客户按月分析
 */
@Data
public class CustomerMonthAnalysisReq {
    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @Pattern(regexp = ScmCommonConstant.MONTH_PATTEN, message = "查询开始月份格式有误, yyyy-MM表示")
    @ApiModelProperty(value = "查询开始月份", notes = "yyyy-MM")
    private String startMonth;

    @Pattern(regexp = ScmCommonConstant.MONTH_PATTEN, message = "查询结束月份格式有误, yyyy-MM表示")
    @ApiModelProperty(value = "查询结束月份", notes = "yyyy-MM")
    private String endMonth;


}
