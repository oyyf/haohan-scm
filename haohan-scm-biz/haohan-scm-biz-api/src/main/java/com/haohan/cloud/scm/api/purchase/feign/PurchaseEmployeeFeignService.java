/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.BuyerAddSupplierReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购员工管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseEmployeeFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseEmployeeFeignService {


    /**
     * 通过id查询采购员工管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseEmployee/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购员工管理 列表信息
     * @param purchaseEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseEmployee/fetchPurchaseEmployeePage")
    R getPurchaseEmployeePage(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购员工管理 列表信息
     * @param purchaseEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseEmployee/fetchPurchaseEmployeeList")
    R getPurchaseEmployeeList(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/add")
    R save(@RequestBody PurchaseEmployee purchaseEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/update")
    R updateById(@RequestBody PurchaseEmployee purchaseEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购员工管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseEmployee/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/countByPurchaseEmployeeReq")
    R countByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/getOneByPurchaseEmployeeReq")
    R getOneByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseEmployee/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseEmployee> purchaseEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 采购员工新增供应商
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseEmployee/addSupplier")
    R addSupplier(@RequestBody BuyerAddSupplierReq req, @RequestHeader(SecurityConstants.FROM) String from);

}
