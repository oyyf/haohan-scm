package com.haohan.cloud.scm.api.saleb.resp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/11
 */
@Data
public class QueryOrderSumAndPriceSumResp {

  /**
   * 排行
   */
  private Integer orderSeq;
  /**
   * 订单总数
   */
  private Integer orderSum;
  /**
   * 订单总金额
   */
  private BigDecimal moneySum;
  /**
   * 区域地址
   */
  private String area;
}
