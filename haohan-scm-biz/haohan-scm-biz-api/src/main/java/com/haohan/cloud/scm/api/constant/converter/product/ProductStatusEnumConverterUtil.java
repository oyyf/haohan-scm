package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProductStatusEnumConverterUtil implements Converter<ProductStatusEnum> {
    @Override
    public ProductStatusEnum convert(Object o, ProductStatusEnum productStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductStatusEnum.getByType(o.toString());
    }
}
