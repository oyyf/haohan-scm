package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/8/13
 */
@Data
@ApiModel("订单确认收货 交易完成")
public class ConfirmReceiveReq {

    @ApiModelProperty(value = "平台商家id", required = true)
    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @ApiModelProperty(value = "采购单编号", required = true)
    @NotBlank(message = "buyId不能为空")
    private String buyId;

    public BuyOrder transTo() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setPmId(this.pmId);
        buyOrder.setBuyId(this.buyId);
        return buyOrder;
    }
}
