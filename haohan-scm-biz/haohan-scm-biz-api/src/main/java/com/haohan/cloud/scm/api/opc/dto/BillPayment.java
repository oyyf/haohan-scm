package com.haohan.cloud.scm.api.opc.dto;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/9/19
 */
@Data
@ApiModel(description = "账单")
public class BillPayment {

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 来源订单编号 =
     */
    private String orderSn;
    /**
     * 账单归属人id(buyerId/supplierId)  =
     */
    private String customerId;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家id
     */
    private String merchantId;
    /**
     * 客户商家名称
     */
    private String merchantName;
    /**
     * 订单成交日期 =>应收订单送货日期 / 退货单申请日期  =
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dealDate;
    /**
     * 账单金额  =
     */
    private BigDecimal billAmount;
    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 账单编号 (应收、应付) =
     */
    private String billSn;
    /**
     * 账单类型: 1订单应收 2退款应收 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 结算状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;

    public BillPayment() {
    }

    public BillPayment(BuyerPayment payment) {
        this.id = payment.getId();
        this.pmId = payment.getPmId();
        this.orderSn = payment.getReceivableSn();
        this.customerId = payment.getBuyerId();
        this.customerName = payment.getCustomerName();
        this.merchantId = payment.getMerchantId();
        this.merchantName = payment.getMerchantName();
        this.dealDate = payment.getBuyDate();
        this.billAmount = payment.getBuyerPayment();
        this.settlementSn = payment.getSettlementSn();
        this.billSn = payment.getBuyerPaymentId();
        this.billType = payment.getBillType();
        this.reviewStatus = payment.getReviewStatus();
        this.status = payment.getStatus();
        this.settlementType = SettlementTypeEnum.receivable;
    }

    public BillPayment(SupplierPayment payment) {
        this.id = payment.getId();
        this.pmId = payment.getPmId();
        this.orderSn = payment.getPayableSn();
        this.customerId = payment.getSupplierId();
        this.customerName = payment.getCustomerName();
        this.merchantId = payment.getMerchantId();
        this.merchantName = payment.getMerchantName();
        this.dealDate = payment.getSupplyDate();
        this.billAmount = payment.getSupplierPayment();
        this.settlementSn = payment.getSettlementSn();
        this.billSn = payment.getSupplierPaymentId();
        this.billType = payment.getBillType();
        this.reviewStatus = payment.getReviewStatus();
        this.status = payment.getStatus();
        this.settlementType = SettlementTypeEnum.payable;
    }


}
