package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/11/15
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ReportSqlDTO extends SelfSqlDTO {

    @ApiModelProperty(value = "上报类型 0库存1销售记录")
    private String reportType;

    @ApiModelProperty(value = "上报状态0待确认1确认")
    private String reportStatus;

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品规格编号")
    private String modelSn;

    @ApiModelProperty(value = "条形码")
    private String modelCode;

    @ApiModelProperty(value = "到期日期 yyyy-MM-dd HH:mm:ss")
    private String maturityTime;

    @ApiModelProperty(value = "商品规格id列表,逗号连接")
    private String goodsModelIds;

}
