package com.haohan.cloud.scm.api.wechat.req;

import com.haohan.cloud.scm.api.saleb.req.QueryBuyOrderReq;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/5/30
 * Default 验证 uid
 * SingleGroup 验证 uid、 buyOrderSn
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxBuyOrderReq extends QueryBuyOrderReq {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "通行证id不能为空", groups = {Default.class, SingleGroup.class})
    @Length(max = 64, message = "通行证id长度最大64字符")
    private String uid;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;


}
