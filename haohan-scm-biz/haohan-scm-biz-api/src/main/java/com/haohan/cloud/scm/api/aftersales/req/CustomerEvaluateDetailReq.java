/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户服务评分明细
 *
 * @author haohan
 * @date 2019-05-30 10:25:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户服务评分明细")
public class CustomerEvaluateDetailReq extends CustomerEvaluateDetail {

    private long pageSize;
    private long pageNo;




}
