package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductionTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class ProductionTypeEnumConverterUtil implements Converter<ProductionTypeEnum> {
    @Override
    public ProductionTypeEnum convert(Object o, ProductionTypeEnum productionTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductionTypeEnum.getByType(o.toString());
    }
}
