package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/6/4
 */
@Getter
@AllArgsConstructor
public enum BuyerStatusEnum {

    /**
     * 采购商状态
     */
    wait_ship("0", "待发货"),
    wait_check("1", "待验货"),
    received("2", "已收货");

    private static final Map<String, BuyerStatusEnum> MAP = new HashMap<>(8);

    static {
        for (BuyerStatusEnum e : BuyerStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BuyerStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
