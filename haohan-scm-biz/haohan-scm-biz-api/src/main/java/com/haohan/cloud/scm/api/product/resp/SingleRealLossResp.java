package com.haohan.cloud.scm.api.product.resp;

import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "查询损耗率返回参数")
public class SingleRealLossResp {

    @ApiModelProperty(value = "商家平台id")
    private String pmId;

    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @ApiModelProperty(value = "损耗类型:1.采购2.生产加工3.分拣4.配送5.仓储盘点6.调拨")
    private LossTypeEnum lossType;

    @ApiModelProperty(value="损耗率")
    private BigDecimal lossRate;
}
