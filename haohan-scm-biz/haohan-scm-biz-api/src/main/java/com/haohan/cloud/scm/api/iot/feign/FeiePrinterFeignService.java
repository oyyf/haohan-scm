/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.iot.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.iot.entity.FeiePrinter;
import com.haohan.cloud.scm.api.iot.req.FeiePrinterReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 飞鹅打印机管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "FeiePrinterFeignService", value = ScmServiceName.SCM_IOT)
public interface FeiePrinterFeignService {


    /**
     * 通过id查询飞鹅打印机管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/FeiePrinter/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 飞鹅打印机管理 列表信息
     *
     * @param feiePrinterReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/FeiePrinter/fetchFeiePrinterPage")
    R getFeiePrinterPage(@RequestBody FeiePrinterReq feiePrinterReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 飞鹅打印机管理 列表信息
     *
     * @param feiePrinterReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/FeiePrinter/fetchFeiePrinterList")
    R getFeiePrinterList(@RequestBody FeiePrinterReq feiePrinterReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增飞鹅打印机管理
     *
     * @param feiePrinter 飞鹅打印机管理
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/add")
    R save(@RequestBody FeiePrinter feiePrinter, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改飞鹅打印机管理
     *
     * @param feiePrinter 飞鹅打印机管理
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/update")
    R updateById(@RequestBody FeiePrinter feiePrinter, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除飞鹅打印机管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param feiePrinterReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/countByFeiePrinterReq")
    R countByFeiePrinterReq(@RequestBody FeiePrinterReq feiePrinterReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param feiePrinterReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/getOneByFeiePrinterReq")
    R getOneByFeiePrinterReq(@RequestBody FeiePrinterReq feiePrinterReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param feiePrinterList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/FeiePrinter/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<FeiePrinter> feiePrinterList, @RequestHeader(SecurityConstants.FROM) String from);


}
