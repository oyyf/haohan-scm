package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.PaymentTypeEnum;

/**
 * @author dy
 * @date 2019/12/10
 */
public class PaymentTypeEnumConverterUtil implements Converter<PaymentTypeEnum> {
    @Override
    public PaymentTypeEnum convert(Object o, PaymentTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PaymentTypeEnum.getByType(o.toString());
    }

}