/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;
import com.haohan.cloud.scm.api.wms.req.WarehouseStockReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 仓库库存内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "warehouseStockFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WarehouseStockFeignService {


    /**
     * 通过id查询仓库库存
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/warehouseStock/{id}", method = RequestMethod.GET)
    R<WarehouseStock> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 仓库库存 列表信息
     * @param warehouseStockReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/warehouseStock/fetchWarehouseStockPage")
    R<Page<WarehouseStock>> getwarehouseStockPage(@RequestBody WarehouseStockReq warehouseStockReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 仓库库存 列表信息
     * @param warehouseStockReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/warehouseStock/fetchWarehouseStockList")
    R<List<WarehouseStock>> getwarehouseStockList(@RequestBody WarehouseStockReq warehouseStockReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增仓库库存
     * @param warehouseStock 仓库库存
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/add")
    R<Boolean> save(@RequestBody WarehouseStock warehouseStock, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改仓库库存
     * @param warehouseStock 仓库库存
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/update")
    R<Boolean> updateById(@RequestBody WarehouseStock warehouseStock, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除仓库库存
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/listByIds")
    R<List<WarehouseStock>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warehouseStockReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/countByWarehouseStockReq")
    R<Integer> countBywarehouseStockReq(@RequestBody WarehouseStockReq warehouseStockReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warehouseStockReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/getOneByWarehouseStockReq")
    R<WarehouseStock> getOneBywarehouseStockReq(@RequestBody WarehouseStockReq warehouseStockReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warehouseStockList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/warehouseStock/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<WarehouseStock> warehouseStockList, @RequestHeader(SecurityConstants.FROM) String from);


}

