package com.haohan.cloud.scm.api.constant.converter.saleb;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyerTypeEnum;

/**
 * @author dy
 * @date 2019/6/15
 */
public class BuyerTypeEnumConverterUtil implements Converter<BuyerTypeEnum> {

    @Override
    public BuyerTypeEnum convert(Object o, BuyerTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BuyerTypeEnum.getByType(o.toString());
    }

}
