package com.haohan.cloud.scm.api.common.req.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @program: haohan-fresh-scm
 * @description: 支付结果查询
 * @author: Simon
 * @create: 2019-07-26
 **/
@Data
public class PayQueryReq extends BasePayParams {

  @JsonProperty("trans_id")
  private String transId;

  @JsonProperty("req_id")
  private String reqId;

  @JsonProperty("wallet_order_id")
  private String walletOrderId;

}
