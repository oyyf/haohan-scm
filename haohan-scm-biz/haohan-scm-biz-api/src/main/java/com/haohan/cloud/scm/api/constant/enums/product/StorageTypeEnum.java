package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum StorageTypeEnum {
  enterwarehouse("1","入库存储"),
  delivery("2","转配送");

    private static final Map<String, StorageTypeEnum> MAP = new HashMap<>(8);

    static {
        for (StorageTypeEnum e : StorageTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static StorageTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
