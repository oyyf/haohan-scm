/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 送货明细
 *
 * @author haohan
 * @date 2019-05-28 20:53:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "送货明细")
public class ShipOrderDetailReq extends ShipOrderDetail {

    private long pageSize;
    private long pageNo;




}
