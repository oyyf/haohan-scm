package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/26
 */
@Data
public class WorkReportAnalysisCountVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("新增客户数")
    private Integer customerNum;

    @ApiModelProperty("销售订单数")
    private Integer salesOrderNum;

    @ApiModelProperty("销售订单金额")
    private BigDecimal salesOrderAmount;

    @ApiModelProperty("拜访数")
    private Integer visitNum;

    @ApiModelProperty("现场拍照数")
    private Integer pictureRecordNum;

    @ApiModelProperty("销量上报数")
    private Integer salesReportNum;

    @ApiModelProperty("销量上报金额")
    private BigDecimal salesReportAmount;

    @ApiModelProperty("库存上报数")
    private Integer stockReportNum;

    @ApiModelProperty("竞品上报数")
    private Integer competitionReportNum;

    @ApiModelProperty("竞品上报金额")
    private BigDecimal competitionReportAmount;

    public WorkReportAnalysisCountVO() {
        this.customerNum = 0;
        this.stockReportNum = 0;
        this.visitNum = 0;
        this.pictureRecordNum = 0;
        this.salesOrderNum = 0;
        this.salesOrderAmount = BigDecimal.ZERO;
        this.salesReportNum = 0;
        this.salesReportAmount = BigDecimal.ZERO;
        this.competitionReportNum = 0;
        this.competitionReportAmount = BigDecimal.ZERO;
    }

}
