package com.haohan.cloud.scm.api.constant.enums.message;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/13
 */
@Getter
@AllArgsConstructor
public enum MsgActionTypeEnum implements IBaseEnum {

    /**
     * 业务类型:各部门的消息
     * 0.系统通知
     * 51.日报  52.销售订单 53.销量上报 54.库存上报 55.竞品上报
     */
    system("0", "系统通知"),
    workReport("51", "日报"),
    salesOrder("52", "销售订单"),
    salesReport("53", "销量上报"),
    stockReport("54", "库存上报"),
    competitionReport("55", "竞品上报");

    private static final Map<String, MsgActionTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MsgActionTypeEnum e : MsgActionTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MsgActionTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
