package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterOrderTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class AfterOrderTypeEnumConverterUtil implements Converter<AfterOrderTypeEnum> {
    @Override
    public AfterOrderTypeEnum convert(Object o, AfterOrderTypeEnum afterOrderTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AfterOrderTypeEnum.getByType(o.toString());
    }
}
