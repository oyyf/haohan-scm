/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 配送规则
 *
 * @author haohan
 * @date 2019-05-13 18:47:18
 */
@Data
@TableName("scm_cms_delivery_rules")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "配送规则")
public class DeliveryRules extends Model<DeliveryRules> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 商家id
     */
    private String merchantId;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 配送方式
     */
    private String deliveryType;
    /**
     * 配送计划类型
     */
    private String deliveryPlanType;
    /**
     * 配送周期
     */
    private String deliverySchedule;
    /**
     * 配送时效
     */
    private String arriveType;
    /**
     * 每次配送数量
     */
    private Integer deliveryNum;
    /**
     * 起送数量
     */
    private Integer minNum;
    /**
     * 规则描述
     */
    private String rulesDesc;
    /**
     * 指定时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime specificDate;
    /**
     * 配送初始时间间隔
     */
    private Integer startDayNum;
    /**
     * 配送总数量
     */
    private Integer deliveryTotalNum;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @TableField(fill = FieldFill.INSERT)
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

}
