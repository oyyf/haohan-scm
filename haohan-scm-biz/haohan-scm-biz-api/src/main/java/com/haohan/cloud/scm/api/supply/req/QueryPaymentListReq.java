package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/17
 */
@Data
@ApiModel(description = "查询供应商货款列表")
public class QueryPaymentListReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    @NotBlank(message = "status不能为空")
    @ApiModelProperty(value = "状态:已结/未结",required = true)
    private YesNoEnum status;

    @ApiModelProperty(value = "每页显示条数" )
    private long pageSize=10;

    @ApiModelProperty(value = "页码" )
    private long pageNo=1;
}
