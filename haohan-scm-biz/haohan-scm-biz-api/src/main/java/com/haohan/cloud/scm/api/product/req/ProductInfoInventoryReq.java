package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class ProductInfoInventoryReq extends ProductInfo {

    private BigDecimal afterNumber;

    private LossTypeEnum lossType;
}
