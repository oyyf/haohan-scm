/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreProductDescription;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author haohan
 * @date 2020-06-20 14:52:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "")
public class StoreProductDescriptionReq extends StoreProductDescription {

    private long pageSize;
    private long pageNo;

}
