package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerAddress;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/10/12
 */
@Data
public class CustomerAddressImport {

    @NotBlank(message = "客户名称不能为空")
    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "是否默认")
    private YesNoEnum defaultFlag;

    @NotBlank(message = "省不能为空")
    @Length(min = 0, max = 20, message = "省的字符长度必须在1至20之间")
    @ApiModelProperty(value = "省")
    private String province;

    @NotBlank(message = "市不能为空")
    @Length(min = 0, max = 20, message = "市的字符长度必须在1至20之间")
    @ApiModelProperty(value = "市")
    private String city;

    @NotBlank(message = "区不能为空")
    @Length(min = 0, max = 20, message = "区的字符长度必须在1至20之间")
    @ApiModelProperty(value = "区")
    private String district;

    @Length(min = 0, max = 20, message = "街道、乡镇的字符长度必须在0至20之间")
    @ApiModelProperty(value = "街道、乡镇")
    private String street;

    @NotBlank(message = "详细地址不能为空")
    @Length(min = 0, max = 64, message = "详细地址的字符长度必须在1至64之间")
    @ApiModelProperty(value = "详细地址")
    private String address;

    @Length(min = 0, max = 20, message = "收货人名称的字符长度必须在0至20之间")
    @ApiModelProperty(value = "收货人名称")
    private String linkman;

    @NotBlank(message = "联系人手机不能为空")
    @Length(min = 8, max = 15, message = "联系人手机的字符长度必须在8至15之间")
    @ApiModelProperty(value = "联系人手机")
    private String telephone;

    @Length(min = 0, max = 20, message = "固定电话的字符长度必须在0至20之间")
    @ApiModelProperty(value = "固定电话")
    private String phone;

    @Length(min = 0, max = 255, message = "备注信息的字符长度必须在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public CustomerAddress transTo() {
        CustomerAddress customerAddress = new CustomerAddress();
        // 默认 否
        customerAddress.setDefaultFlag(null == this.defaultFlag ? YesNoEnum.no : this.defaultFlag);
        customerAddress.setProvince(this.province);
        customerAddress.setCity(this.city);
        customerAddress.setDistrict(this.district);
        customerAddress.setStreet(this.street);
        customerAddress.setAddress(this.address);
        customerAddress.setLinkman(this.linkman);
        customerAddress.setTelephone(this.telephone);
        customerAddress.setPhone(this.phone);
        customerAddress.setRemarks(this.remarks);
        return customerAddress;
    }
}
