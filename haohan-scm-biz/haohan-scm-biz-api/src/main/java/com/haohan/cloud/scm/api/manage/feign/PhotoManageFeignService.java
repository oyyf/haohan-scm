/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.PhotoManage;
import com.haohan.cloud.scm.api.manage.req.PhotoManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 图片管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PhotoManageFeignService", value = ScmServiceName.SCM_MANAGE)
public interface PhotoManageFeignService {


    /**
     * 通过id查询图片管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PhotoManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 图片管理 列表信息
     * @param photoManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoManage/fetchPhotoManagePage")
    R getPhotoManagePage(@RequestBody PhotoManageReq photoManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 图片管理 列表信息
     * @param photoManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoManage/fetchPhotoManageList")
    R getPhotoManageList(@RequestBody PhotoManageReq photoManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/add")
    R save(@RequestBody PhotoManage photoManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改图片管理
     * @param photoManage 图片管理
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/update")
    R updateById(@RequestBody PhotoManage photoManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除图片管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PhotoManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/countByPhotoManageReq")
    R countByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param photoManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/getOneByPhotoManageReq")
    R getOneByPhotoManageReq(@RequestBody PhotoManageReq photoManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param photoManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PhotoManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PhotoManage> photoManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
