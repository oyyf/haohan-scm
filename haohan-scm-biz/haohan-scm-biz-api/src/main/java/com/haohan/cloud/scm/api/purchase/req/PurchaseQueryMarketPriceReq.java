package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "查询市场行情详情需求")
public class PurchaseQueryMarketPriceReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID",required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "市场行情id",required = true)
    private String id;

    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id" , required = true)
    private String uId;

    public PurchaseQueryMarketPriceReq(String pmId, String id) {
        this.pmId = pmId;
        this.id = id;
    }

}
