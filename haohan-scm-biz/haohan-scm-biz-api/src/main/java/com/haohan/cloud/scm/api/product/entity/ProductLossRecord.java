/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-13 18:16:25
 */
@Data
@TableName("scm_pws_product_loss_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货品损耗记录表")
public class ProductLossRecord extends Model<ProductLossRecord> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 损耗记录编号
     */
    private String lossRecordSn;
    /**
     * 报损前货品编号
     */
    private String productSn;
    /**
     * 报损后货品编号
     */
    private String afterProductSn;
    /**
     * 损耗货品编号
     */
    private String lossProductSn;
    /**
     * 损耗类型:1.采购2.生产加工3.分拣4.配送5.仓储盘点6.调拨
     */
    private LossTypeEnum lossType;
    /**
     * 货品单位
     */
    private String unit;
    /**
     * 损耗前数量
     */
    private BigDecimal originalNumber;
    /**
     * 损耗后数量
     */
    private BigDecimal resultNumber;
    /**
     * 耗损数量
     */
    private BigDecimal lossNumber;
    /**
     * 损耗率
     */
    private BigDecimal lossRate;
    /**
     * 损耗记录时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime recordTime;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 加工记录编号
     */
    private String processingSn;
    /**
     * 分拣明细编号
     */
    private String sortingDetailSn;
    /**
     * 送货单明细编号
     */
    private String shipDetailSn;
    /**
     * 盘点明细编号
     */
    private String warehouseInventorySn;
    /**
     * 调拨明细编号
     */
    private String allotDetailSn;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
