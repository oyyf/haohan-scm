package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.GoodsAnalysisTypeEnum;

/**
 * @author dy
 * @date 2019/9/27
 */
public class GoodsAnalysisTypeEnumConverterUtil implements Converter<GoodsAnalysisTypeEnum> {
    @Override
    public GoodsAnalysisTypeEnum convert(Object o, GoodsAnalysisTypeEnum workReportTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return GoodsAnalysisTypeEnum.getByType(o.toString());
    }

}