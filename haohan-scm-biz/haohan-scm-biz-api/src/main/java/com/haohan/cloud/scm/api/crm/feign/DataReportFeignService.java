/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.DataReport;
import com.haohan.cloud.scm.api.crm.req.DataReportReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 数据汇报内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DataReportFeignService", value = ScmServiceName.SCM_CRM)
public interface DataReportFeignService {


    /**
     * 通过id查询数据汇报
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DataReport/{id}", method = RequestMethod.GET)
    R<DataReport> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 数据汇报 列表信息
     *
     * @param dataReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DataReport/fetchDataReportPage")
    R<Page<DataReport>> getDataReportPage(@RequestBody DataReportReq dataReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 数据汇报 列表信息
     *
     * @param dataReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DataReport/fetchDataReportList")
    R<List<DataReport>> getDataReportList(@RequestBody DataReportReq dataReportReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增数据汇报
     *
     * @param dataReport 数据汇报
     * @return R
     */
    @PostMapping("/api/feign/DataReport/add")
    R<Boolean> save(@RequestBody DataReport dataReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改数据汇报
     *
     * @param dataReport 数据汇报
     * @return R
     */
    @PostMapping("/api/feign/DataReport/update")
    R<Boolean> updateById(@RequestBody DataReport dataReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除数据汇报
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DataReport/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DataReport/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DataReport/listByIds")
    R<List<DataReport>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param dataReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DataReport/countByDataReportReq")
    R<Integer> countByDataReportReq(@RequestBody DataReportReq dataReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param dataReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DataReport/getOneByDataReportReq")
    R<DataReport> getOneByDataReportReq(@RequestBody DataReportReq dataReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param dataReportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DataReport/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<DataReport> dataReportList, @RequestHeader(SecurityConstants.FROM) String from);


}
