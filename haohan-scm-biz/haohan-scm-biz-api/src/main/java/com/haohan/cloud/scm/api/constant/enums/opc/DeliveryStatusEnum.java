package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/6/4
 */
@Getter
@AllArgsConstructor
public enum DeliveryStatusEnum {
    /**
     * 配送状态
     */
    wait_delivery("0", "待配送"),
    delivering("1", "配送中"),
    arrived("2", "已送达");

    private static final Map<String, DeliveryStatusEnum> MAP = new HashMap<>(8);

    static {
        for (DeliveryStatusEnum e : DeliveryStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DeliveryStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;

}
