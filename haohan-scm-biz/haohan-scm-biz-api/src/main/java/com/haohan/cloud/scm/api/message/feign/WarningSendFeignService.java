/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WarningSend;
import com.haohan.cloud.scm.api.message.req.WarningSendReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 预警发送记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarningSendFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WarningSendFeignService {


    /**
     * 通过id查询预警发送记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarningSend/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 预警发送记录表 列表信息
     * @param warningSendReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningSend/fetchWarningSendPage")
    R getWarningSendPage(@RequestBody WarningSendReq warningSendReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 预警发送记录表 列表信息
     * @param warningSendReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningSend/fetchWarningSendList")
    R getWarningSendList(@RequestBody WarningSendReq warningSendReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增预警发送记录表
     * @param warningSend 预警发送记录表
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/add")
    R save(@RequestBody WarningSend warningSend, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改预警发送记录表
     * @param warningSend 预警发送记录表
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/update")
    R updateById(@RequestBody WarningSend warningSend, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除预警发送记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarningSend/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warningSendReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/countByWarningSendReq")
    R countByWarningSendReq(@RequestBody WarningSendReq warningSendReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warningSendReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/getOneByWarningSendReq")
    R getOneByWarningSendReq(@RequestBody WarningSendReq warningSendReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warningSendList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarningSend/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarningSend> warningSendList, @RequestHeader(SecurityConstants.FROM) String from);


}
