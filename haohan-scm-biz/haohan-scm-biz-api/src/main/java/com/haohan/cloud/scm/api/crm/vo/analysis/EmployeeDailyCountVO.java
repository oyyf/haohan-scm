package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/1/9
 * 员工日常统计
 * 新增客户数、销量上报数、库存上报数、竞品上报数
 * 销售订单数、拜访数、现场拍照数
 */
@Data
public class EmployeeDailyCountVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("新增客户数")
    private Integer customerNum;

    @ApiModelProperty("销售订单数")
    private Integer salesOrderNum;

    @ApiModelProperty("拜访数")
    private Integer visitNum;

    @ApiModelProperty("现场拍照数")
    private Integer pictureRecordNum;

    @ApiModelProperty("销量上报数")
    private Integer salesReportNum;

    @ApiModelProperty("库存上报数")
    private Integer stockReportNum;

    @ApiModelProperty("竞品上报数")
    private Integer competitionReportNum;

    public EmployeeDailyCountVO() {
        this.customerNum = 0;
        this.salesOrderNum = 0;
        this.visitNum = 0;
        this.pictureRecordNum = 0;
        this.salesReportNum = 0;
        this.stockReportNum = 0;
        this.competitionReportNum = 0;
    }

    public EmployeeDailyCountVO(Integer customerNum, Integer salesOrderNum, Integer visitNum, Integer pictureRecordNum) {
        this.customerNum = customerNum;
        this.salesOrderNum = salesOrderNum;
        this.visitNum = visitNum;
        this.pictureRecordNum = pictureRecordNum;
    }

    public void setReportNum(Integer salesReportNum, Integer stockReportNum, Integer competitionReportNum) {
        this.salesReportNum = salesReportNum;
        this.stockReportNum = stockReportNum;
        this.competitionReportNum = competitionReportNum;
    }
}
