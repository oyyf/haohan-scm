package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/7/26
 */
@Data
@ApiModel(description = "创建应收账单")
public class CreateReceivableReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "receivableSn不能为空")
    @ApiModelProperty(value = "应收来源订单编号", required = true)
    private String receivableSn;

    @NotNull(message = "billType不能为空")
    @ApiModelProperty(value = "账单类型", required = true)
    private BillTypeEnum billType;

    public BuyerPayment transTo() {
        BuyerPayment buyerPayment = new BuyerPayment();
        buyerPayment.setPmId(this.pmId);
        buyerPayment.setReceivableSn(this.receivableSn);
        buyerPayment.setBillType(this.billType);
        return buyerPayment;
    }

}
