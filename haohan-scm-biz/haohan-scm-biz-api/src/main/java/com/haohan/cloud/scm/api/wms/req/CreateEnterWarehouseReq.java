package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author xwx
 * @date 2019/6/13
 */
@Data
@ApiModel(description = "新增入库单 根据入库单明细")
public class CreateEnterWarehouseReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id" , required = true)
    private String pmId;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号" , required = true)
    private String warehouseSn;

    @NotEmpty(message = "ids入库单明细编号的数组不能为空")
    @ApiModelProperty(value = "入库单明细编号的数组" , required = true)
    private String[] ids;

    @ApiModelProperty(value = "入库批次号")
    private String batchNumber;

    @ApiModelProperty(value = "采购单编号")
    private String purchaseSn;


}
