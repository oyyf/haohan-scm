package com.haohan.cloud.scm.api.bill.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/11/27
 */
@Data
@TableName("scm_payable_bill")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "应付账单")
public class PayableBill extends BaseBill<ReceivableBill> {
    private static final long serialVersionUID = 1L;

}