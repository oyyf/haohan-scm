package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class CustomerStatusEnumConverterUtil implements Converter<CustomerStatusEnum> {
    @Override
    public CustomerStatusEnum convert(Object o, CustomerStatusEnum customerStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return CustomerStatusEnum.getByType(o.toString());
    }
}
