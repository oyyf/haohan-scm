package com.haohan.cloud.scm.api.purchase.resp;

import lombok.Data;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseModifyPurchaseReportResp {
    private Boolean flag = false;

}
