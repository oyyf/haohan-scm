/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.message.entity.WarningTemplate;
import com.haohan.cloud.scm.api.message.req.WarningTemplateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 预警信息模板内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarningTemplateFeignService", value = ScmServiceName.SCM_BIZ_MSG)
public interface WarningTemplateFeignService {


    /**
     * 通过id查询预警信息模板
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarningTemplate/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 预警信息模板 列表信息
     * @param warningTemplateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningTemplate/fetchWarningTemplatePage")
    R getWarningTemplatePage(@RequestBody WarningTemplateReq warningTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 预警信息模板 列表信息
     * @param warningTemplateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarningTemplate/fetchWarningTemplateList")
    R getWarningTemplateList(@RequestBody WarningTemplateReq warningTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增预警信息模板
     * @param warningTemplate 预警信息模板
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/add")
    R save(@RequestBody WarningTemplate warningTemplate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改预警信息模板
     * @param warningTemplate 预警信息模板
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/update")
    R updateById(@RequestBody WarningTemplate warningTemplate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除预警信息模板
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarningTemplate/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warningTemplateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/countByWarningTemplateReq")
    R countByWarningTemplateReq(@RequestBody WarningTemplateReq warningTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warningTemplateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/getOneByWarningTemplateReq")
    R getOneByWarningTemplateReq(@RequestBody WarningTemplateReq warningTemplateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warningTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarningTemplate/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarningTemplate> warningTemplateList, @RequestHeader(SecurityConstants.FROM) String from);


}
