package com.haohan.cloud.scm.api.purchase.resp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.TaskActionTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/6
 */
@Data
public class PurchaseTaskListResp {
    //PurchaseTask中
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     *采购单编号
     */
    private String purchaseSn;
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 任务状态1.待处理2.执行中3.已完成4.未完成
     */
    private TaskStatusEnum taskStatus;
    /**
     * 任务执行方式:1.审核2.分配3.采购
     */
    private TaskActionTypeEnum taskActionType;
    /**
     * 任务截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadlineTime;

    //PurchaseOrderDetail下
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品分类名称
     */
    private String goodsCategoryName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;
    /**
     * 实际采购数量
     */
    private BigDecimal realBuyNum;
    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    private MethodTypeEnum methodType;

    /**
     * 发起人
     */
    private String initiatorName;
    /**
     * 发起人id
     */
    private String initiatorId;
    /**
     * 执行人
     */
    private String transactorName;
    /**
     * 执行人id
     */
    private String transactorId;

    /**
     * 采购状态:1.待处理2.待审核3.采购中4.备货中5.已揽货6.采购完成7.部分完成8.已关闭
     */
    private PurchaseStatusEnum purchaseStatus;

    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 采购价
     */
    private BigDecimal buyPrice;

}
