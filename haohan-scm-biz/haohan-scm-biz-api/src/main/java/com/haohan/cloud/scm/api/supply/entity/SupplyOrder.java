/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
@Data
@TableName("scm_sms_supply_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应订单")
public class SupplyOrder extends Model<SupplyOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 供应订单编号
     */
    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;
    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商id")
    private String supplierId;
    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;
    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间")
    private LocalDateTime orderTime;
    /**
     * 供应日期
     */
    @ApiModelProperty(value = "供应日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate supplyDate;
    /**
     * 需求描述
     */
    @ApiModelProperty(value = "需求描述")
    private String needNote;
    /**
     * 供应总金额
     */
    @ApiModelProperty(value = "供应总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "商品合计金额")
    private BigDecimal sumAmount;
    /**
     * 其他金额
     */
    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;
    /**
     * 联系人
     */
    @ApiModelProperty(value = "联系人")
    private String contact;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    private String telephone;
    /**
     * 供应地址
     */
    @ApiModelProperty(value = "供应地址")
    private String address;
    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private BuyOrderStatusEnum status;
    /**
     * 成交时间 (确认时间)
     */
    @ApiModelProperty(value = "成交时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 配送方式
     */
    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;
    /**
     * 货品种数
     */
    @ApiModelProperty(value = "货品种数")
    private Integer goodsNum;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
