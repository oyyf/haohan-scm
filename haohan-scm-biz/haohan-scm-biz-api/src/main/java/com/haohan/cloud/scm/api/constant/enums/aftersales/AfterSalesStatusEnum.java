package com.haohan.cloud.scm.api.constant.enums.aftersales;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum AfterSalesStatusEnum {
  report("1","已上报"),
  handle("2","处理中"),
  accept("3","待确认"),
  finish("4","已完成");

    private static final Map<String, AfterSalesStatusEnum> MAP = new HashMap<>(8);

    static {
        for (AfterSalesStatusEnum e : AfterSalesStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AfterSalesStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
