package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/3
 */
@Getter
@AllArgsConstructor
public enum VisitStatusEnum implements IBaseEnum {

    /**
     * 拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成
     */
    wait("1", "待拜访"),
    doing("2", "拜访中"),
    finish("3", "已拜访"),
    failed("4", "未完成");

    private static final Map<String, VisitStatusEnum> MAP = new HashMap<>(8);

    static {
        for (VisitStatusEnum e : VisitStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static VisitStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
