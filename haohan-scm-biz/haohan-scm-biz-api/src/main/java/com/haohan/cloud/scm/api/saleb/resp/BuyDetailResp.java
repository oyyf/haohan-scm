package com.haohan.cloud.scm.api.saleb.resp;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/8
 */

@Data
public class BuyDetailResp extends BuyOrder {
    private List<BuyOrderDetail> detailList;
}
