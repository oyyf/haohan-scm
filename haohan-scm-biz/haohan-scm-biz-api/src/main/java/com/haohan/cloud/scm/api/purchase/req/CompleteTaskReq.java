package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cx
 * @date 2019/7/24
 */

@Data
public class CompleteTaskReq {
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String uid;

    /**
     * 商户id
     */
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 采购单明细编号
     */
    @NotBlank(message = "采购单明细编号不能为空")
    private String purchaseDetailSn;

    @NotNull(message = "状态更改不能为空")
    private PurchaseStatusEnum purchaseStatus;
}
