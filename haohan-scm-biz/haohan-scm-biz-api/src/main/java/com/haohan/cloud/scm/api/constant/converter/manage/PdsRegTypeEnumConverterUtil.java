package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegTypeEnum;

/**
 * @author dy
 * @date 2019/9/12
 */
public class PdsRegTypeEnumConverterUtil implements Converter<PdsRegTypeEnum> {
    @Override
    public PdsRegTypeEnum convert(Object o, PdsRegTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsRegTypeEnum.getByType(o.toString());
    }
}
