/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderDetailReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 退货单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ReturnOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface ReturnOrderDetailFeignService {


    /**
     * 通过id查询退货单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ReturnOrderDetail/{id}", method = RequestMethod.GET)
    R<ReturnOrderDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 退货单明细 列表信息
     * @param returnOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ReturnOrderDetail/fetchReturnOrderDetailPage")
    R<Page<ReturnOrderDetail>> getReturnOrderDetailPage(@RequestBody ReturnOrderDetailReq returnOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 退货单明细 列表信息
     * @param returnOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ReturnOrderDetail/fetchReturnOrderDetailList")
    R<List<ReturnOrderDetail>> getReturnOrderDetailList(@RequestBody ReturnOrderDetailReq returnOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增退货单明细
     * @param returnOrderDetail 退货单明细
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/add")
    R<Boolean> save(@RequestBody ReturnOrderDetail returnOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改退货单明细
     * @param returnOrderDetail 退货单明细
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/update")
    R<Boolean> updateById(@RequestBody ReturnOrderDetail returnOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除退货单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ReturnOrderDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/listByIds")
    R<List<ReturnOrderDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param returnOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/countByReturnOrderDetailReq")
    R<Integer> countByReturnOrderDetailReq(@RequestBody ReturnOrderDetailReq returnOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param returnOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/getOneByReturnOrderDetailReq")
    R<ReturnOrderDetail> getOneByReturnOrderDetailReq(@RequestBody ReturnOrderDetailReq returnOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param returnOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ReturnOrderDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<ReturnOrderDetail> returnOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
