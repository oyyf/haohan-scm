package com.haohan.cloud.scm.api.constant.converter;

import lombok.AllArgsConstructor;
import org.springframework.format.FormatterRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author dy
 * @date 2019/7/3
 */
@Component
@AllArgsConstructor
public class SpringConvertConfig implements WebMvcConfigurer {

    private final SpringEnumConvertFactory springEnumConvertFactory;

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(springEnumConvertFactory);
    }


}
