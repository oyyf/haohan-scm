/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.MallUser;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 用户表
 *
 * @author haohan
 * @date 2019-07-10 22:41:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户表")
public class MallUserReq extends MallUser {

    private long pageSize;
    private long pageNo;




}
