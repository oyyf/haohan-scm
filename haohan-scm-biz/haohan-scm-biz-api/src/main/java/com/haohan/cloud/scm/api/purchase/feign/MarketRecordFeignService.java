/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.req.MarketRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyMarketPriceReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 市场行情价格记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MarketRecordFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface MarketRecordFeignService {


    /**
     * 通过id查询市场行情价格记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/MarketRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 市场行情价格记录 列表信息
     * @param marketRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MarketRecord/fetchMarketRecordPage")
    R getMarketRecordPage(@RequestBody MarketRecordReq marketRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 市场行情价格记录 列表信息
     * @param marketRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MarketRecord/fetchMarketRecordList")
    R getMarketRecordList(@RequestBody MarketRecordReq marketRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/add")
    R save(@RequestBody MarketRecord marketRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/update")
    R updateById(@RequestBody MarketRecord marketRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除市场行情价格记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/MarketRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/countByMarketRecordReq")
    R countByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/getOneByMarketRecordReq")
    R getOneByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param marketRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/MarketRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<MarketRecord> marketRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改市场行情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/MarketRecord/modifyMarketPrice")
    R modifyMarketPrice(@RequestBody PurchaseModifyMarketPriceReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
