package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/8/20
 */
@Data
@ApiModel(description = "查询应付账单列表")
public class SupplierPaymentListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @ApiModelProperty(value = "订单成交日期,查询开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;

    @ApiModelProperty(value = "订单成交日期,查询结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家名称
     */
    private String merchantName;

    /**
     * 应付来源订单编号
     */
    private String payableSn;
    /**
     * 应付货款记录编号
     */
    private String supplierPaymentId;

    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 账单类型: 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;

    public SupplierPayment transTo() {
        // 只设置 eq条件
        SupplierPayment payment = new SupplierPayment();
        payment.setPmId(this.getPmId());
        payment.setPayableSn(this.getPayableSn());
        payment.setSupplierPaymentId(this.getSupplierPaymentId());
        payment.setStatus(this.getStatus());
        payment.setBillType(this.getBillType());
        payment.setReviewStatus(this.getReviewStatus());
        return payment;
    }
}
