package com.haohan.cloud.scm.api.opc.req.settlement;

import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/9/18
 */
@Data
@ApiModel("结算的账单信息")
public class SettlementPaymentReq {

    /**
     * 账单编号
     */
    @ApiModelProperty(value = "账单编号")
    @NotBlank(message = "账单编号不能为空")
    private String paymentSn;

    /**
     * 结算账单类型:1.应收 2.应付
     */
    @ApiModelProperty(value = "结算账单类型:1.应收 2.应付")
    private SettlementTypeEnum settlementType;

}
