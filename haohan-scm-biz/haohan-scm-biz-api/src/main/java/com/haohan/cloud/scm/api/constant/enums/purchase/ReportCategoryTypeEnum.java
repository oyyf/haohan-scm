package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum ReportCategoryTypeEnum {
    /**
     * 汇报分类1.货品不足2.价格贵3.质量差4.其他
     */
    insufficientGoods("1","货品不足"),
    expensive("2","价格贵"),
    ropy("3","质量差"),
    other("4","其他");

    private static final Map<String, ReportCategoryTypeEnum> MAP = new HashMap<>(8);

    static {
      for (ReportCategoryTypeEnum e : ReportCategoryTypeEnum.values()) {
        MAP.put(e.getType(), e);
      }
    }

    @JsonCreator
    public static ReportCategoryTypeEnum getByType(String type) {
      if (MAP.containsKey(type)) {
        return MAP.get(type);
      }
      return null;
    }
    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
