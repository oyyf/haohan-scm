package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/9/23
 */
@Data
@ApiModel("查询上报列表")
public class DataReportListReq {

    @Length(max = 32, message = "上报单编码长度最大32字符")
    @ApiModelProperty(value = "上报单编码")
    private String reportSn;

    @Length(max = 32, message = "客户编码长度最大32字符")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @Length(max = 64, message = "客户名称长度最大64字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 10, message = "上报人名称长度最大10字符")
    @ApiModelProperty(value = "上报人名称")
    private String reportMan;

    @ApiModelProperty(value = "上报状态0待确认1确认")
    private DataReportStatusEnum reportStatus;

    @ApiModelProperty(value = "上报日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;

    @ApiModelProperty(value = "上报日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    // app

    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

}
