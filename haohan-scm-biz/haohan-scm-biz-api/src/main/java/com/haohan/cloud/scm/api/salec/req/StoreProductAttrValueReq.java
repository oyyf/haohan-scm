/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreProductAttrValue;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品属性值表
 *
 * @author haohan
 * @date 2019-06-19 17:42:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性值表")
public class StoreProductAttrValueReq extends StoreProductAttrValue {

  private long pageSize;
  private long pageNo;


}
