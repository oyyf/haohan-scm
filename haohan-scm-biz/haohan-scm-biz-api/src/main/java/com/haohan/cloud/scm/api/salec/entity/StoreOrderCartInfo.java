/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 订单购物详情表
 *
 * @author haohan
 * @date 2020-05-21 18:43:59
 */
@Data
@TableName("eb_store_order_cart_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "订单购物详情表")
public class StoreOrderCartInfo extends Model<StoreOrderCartInfo> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "订单id", notes = "对应订单主键")
    @TableId(type = IdType.INPUT)
    private Integer oid;

    @ApiModelProperty(value = "购物车id")
    private Integer cartId;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @Length(max = 65535, message = "购买东西的详细信息长度最大65535字符")
    @ApiModelProperty(value = "购买东西的详细信息")
    private String cartInfo;

    @Length(max = 32, message = "唯一id长度最大32字符")
    @ApiModelProperty(value = "唯一id")
    @TableField("`unique`")
    private String unique;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
