package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.PdsPayTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class PdsPayTypeEnumConverterUtil implements Converter<PdsPayTypeEnum> {
    @Override
    public PdsPayTypeEnum convert(Object o, PdsPayTypeEnum pdsPayTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsPayTypeEnum.getByType(o.toString());
    }

}
