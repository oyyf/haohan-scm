package com.haohan.cloud.scm.api.constant.converter.wechat;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.wechat.WechatUseTypeEnum;

/**
 * @author dy
 * @date 2020/5/19
 */
public class WechatUseTypeEnumConverterUtil implements Converter<WechatUseTypeEnum> {
    @Override
    public WechatUseTypeEnum convert(Object o, WechatUseTypeEnum gradeTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return WechatUseTypeEnum.getByType(o.toString());
    }

}
