package com.haohan.cloud.scm.api.constant.enums.product;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum ExitStatusEnum {
    wait("1", "待出库"),
    receive("2", "已接收"),
    not_receive("3", "未接收"),
    portion("4", "部分接收");

    private static final Map<String, ExitStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ExitStatusEnum e : ExitStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ExitStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
