package com.haohan.cloud.scm.api.supply.resp;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.supply.req.PaymentParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
public class FetchSupplierPaymentResp {
    /**
     * 供应商货款记录编号
     */
    private String supplierPaymentId;
    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 账单类型: 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    private PdsOfferTypeEnum offerType;

    /**
     * 报价单号
     */
    private String offerOrderId;
    /**
     * 账单明细
     */
    List<PaymentParam> list;

}
