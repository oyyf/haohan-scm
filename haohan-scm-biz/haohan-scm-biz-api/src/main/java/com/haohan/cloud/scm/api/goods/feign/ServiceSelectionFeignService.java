/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.ServiceSelection;
import com.haohan.cloud.scm.api.goods.req.ServiceSelectionReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 服务选项内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ServiceSelectionFeignService", value = ScmServiceName.SCM_GOODS)
public interface ServiceSelectionFeignService {


    /**
     * 通过id查询服务选项
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ServiceSelection/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 服务选项 列表信息
     *
     * @param serviceSelectionReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ServiceSelection/fetchServiceSelectionPage")
    R getServiceSelectionPage(@RequestBody ServiceSelectionReq serviceSelectionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 服务选项 列表信息
     *
     * @param serviceSelectionReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ServiceSelection/fetchServiceSelectionList")
    R getServiceSelectionList(@RequestBody ServiceSelectionReq serviceSelectionReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增服务选项
     *
     * @param serviceSelection 服务选项
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/add")
    R save(@RequestBody ServiceSelection serviceSelection, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改服务选项
     *
     * @param serviceSelection 服务选项
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/update")
    R updateById(@RequestBody ServiceSelection serviceSelection, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除服务选项
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param serviceSelectionReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/countByServiceSelectionReq")
    R countByServiceSelectionReq(@RequestBody ServiceSelectionReq serviceSelectionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param serviceSelectionReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/getOneByServiceSelectionReq")
    R getOneByServiceSelectionReq(@RequestBody ServiceSelectionReq serviceSelectionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param serviceSelectionList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ServiceSelection/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ServiceSelection> serviceSelectionList, @RequestHeader(SecurityConstants.FROM) String from);


}
