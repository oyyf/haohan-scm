package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.ShipStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/20
 */
@Data
@ApiModel(description = "查询报价单列表-小程序")
public class QueryOfferListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    @ApiModelProperty(value = "报价类型:1平台报价2市场报价3指定报价4货源报价",required = true)
    private PdsOfferTypeEnum offerType;

    @ApiModelProperty(value = "报价单状态1待报价2已报价3中标4未中标")
    private PdsOfferStatusEnum status;

    @ApiModelProperty(value = "发货状态:0不需发货1.待备货2.备货中3.已发货4.已接收5.售后中")
    private ShipStatusEnum shipStatus;

    @ApiModelProperty(value = "每页显示条数" )
    private long pageSize=10;

    @ApiModelProperty(value = "页码" )
    private long pageNo=1;
}
