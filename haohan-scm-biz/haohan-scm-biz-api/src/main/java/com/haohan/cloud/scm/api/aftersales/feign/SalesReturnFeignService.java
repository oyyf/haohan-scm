/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;
import com.haohan.cloud.scm.api.aftersales.req.SalesReturnReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 售后退货记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesReturnFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface SalesReturnFeignService {


    /**
     * 通过id查询售后退货记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesReturn/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 售后退货记录 列表信息
     * @param salesReturnReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesReturn/fetchSalesReturnPage")
    R getSalesReturnPage(@RequestBody SalesReturnReq salesReturnReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 售后退货记录 列表信息
     * @param salesReturnReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesReturn/fetchSalesReturnList")
    R getSalesReturnList(@RequestBody SalesReturnReq salesReturnReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/add")
    R save(@RequestBody SalesReturn salesReturn, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/update")
    R updateById(@RequestBody SalesReturn salesReturn, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除售后退货记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SalesReturn/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/countBySalesReturnReq")
    R countBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/getOneBySalesReturnReq")
    R getOneBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesReturnList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SalesReturn/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SalesReturn> salesReturnList, @RequestHeader(SecurityConstants.FROM) String from);


}
