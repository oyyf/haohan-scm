package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProcessingTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProcessingTypeEnumConverterUtil implements Converter<ProcessingTypeEnum> {
    @Override
    public ProcessingTypeEnum convert(Object o, ProcessingTypeEnum processingTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProcessingTypeEnum.getByType(o.toString());
    }
}
