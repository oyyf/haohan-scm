package com.haohan.cloud.scm.api.bill.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/26
 */
@Data
@ApiModel("订单商品明细-账单模块")
public class OrderDetailDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "明细编号")
    private String detailSn;

    // 商品信息
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品规格ID
     */
    private String goodsModelId;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 市场价格
     */
    private BigDecimal marketPrice;

    // 订单信息
    /**
     * 成交价格
     */
    private BigDecimal dealPrice;
    /**
     * 成交数量
     */
    private BigDecimal goodsNum;
    /**
     * 商品金额
     */
    private BigDecimal amount;


}
