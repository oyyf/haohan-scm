package com.haohan.cloud.scm.api.crm.vo.app;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import com.haohan.cloud.scm.api.opc.vo.ShipRecordDetailVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/6
 */
@Data
public class CrmShipDetailVO {

    @Length(max = 64, message = "发货单编号长度最大64字符")
    @ApiModelProperty(value = "发货单编号")
    private String shipSn;

    @Length(max = 64, message = "商品id长度最大64字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 64, message = "商品规格id长度最大64字符")
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    @Length(max = 64, message = "商品名称长度最大64字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 64, message = "规格名称长度最大64字符")
    @ApiModelProperty(value = "规格名称")
    private String modelName;

    @Length(max = 64, message = "单位长度最大64字符")
    @ApiModelProperty(value = "单位")
    private String unit;

    @Length(max = 255, message = "商品图片长度最大255字符")
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @Digits(integer = 10, fraction = 2, message = "商品数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "商品数量")
    private BigDecimal goodsNum;

    @Digits(integer = 10, fraction = 2, message = "成交价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;

    @Digits(integer = 10, fraction = 2, message = "商品金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "商品金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesType;

    public CrmShipDetailVO(ShipRecordDetail detail) {
        if (null != detail) {
            BeanUtil.copyProperties(detail, this);
            this.shipSn = detail.getShipRecordSn();
        }
    }

    public CrmShipDetailVO(ShipRecordDetailVO detail) {
        if (null != detail) {
            BeanUtil.copyProperties(detail, this);
            this.shipSn = detail.getShipRecordSn();
        }
    }
}
