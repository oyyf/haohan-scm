package com.haohan.cloud.scm.api.crm.req.report;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/9/30
 */
@Data
public class QueryStockReq {

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品规格编号")
    private String modelSn;

    @ApiModelProperty(value = "条形码")
    private String modelCode;

    @ApiModelProperty(value = "到期日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime maturityTime;

    @ApiModelProperty(value = "商品分类id")
    private String goodsCategoryId;
}
