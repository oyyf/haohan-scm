/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.WarningTemplate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 预警信息模板
 *
 * @author haohan
 * @date 2019-05-28 20:05:29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预警信息模板")
public class WarningTemplateReq extends WarningTemplate {

    private long pageSize;
    private long pageNo;




}
