package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/27
 * 汇总单状态
 */
@Getter
@AllArgsConstructor
public enum SummaryStatusEnum {

    wait("1", "待处理"),
    purchase("2", "采购中"),
    delivery("3", "配送中"),
    deal("4", "已完成"),
    cancel("5", "已关闭");

    private static final Map<String, SummaryStatusEnum> MAP = new HashMap<>(8);

    static {
        for (SummaryStatusEnum e : SummaryStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SummaryStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;
  private String desc;

}
