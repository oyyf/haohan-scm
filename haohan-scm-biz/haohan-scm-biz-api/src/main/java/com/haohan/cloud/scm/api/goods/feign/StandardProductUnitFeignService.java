/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.StandardProductUnit;
import com.haohan.cloud.scm.api.goods.req.StandardProductUnitReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 标准商品库内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StandardProductUnitFeignService", value = ScmServiceName.SCM_GOODS)
public interface StandardProductUnitFeignService {


    /**
     * 通过id查询标准商品库
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/StandardProductUnit/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 标准商品库 列表信息
     *
     * @param standardProductUnitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StandardProductUnit/fetchStandardProductUnitPage")
    R getStandardProductUnitPage(@RequestBody StandardProductUnitReq standardProductUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 标准商品库 列表信息
     *
     * @param standardProductUnitReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StandardProductUnit/fetchStandardProductUnitList")
    R getStandardProductUnitList(@RequestBody StandardProductUnitReq standardProductUnitReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增标准商品库
     *
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/add")
    R save(@RequestBody StandardProductUnit standardProductUnit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改标准商品库
     *
     * @param standardProductUnit 标准商品库
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/update")
    R updateById(@RequestBody StandardProductUnit standardProductUnit, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除标准商品库
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/countByStandardProductUnitReq")
    R countByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param standardProductUnitReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/getOneByStandardProductUnitReq")
    R getOneByStandardProductUnitReq(@RequestBody StandardProductUnitReq standardProductUnitReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param standardProductUnitList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/StandardProductUnit/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<StandardProductUnit> standardProductUnitList, @RequestHeader(SecurityConstants.FROM) String from);


}
