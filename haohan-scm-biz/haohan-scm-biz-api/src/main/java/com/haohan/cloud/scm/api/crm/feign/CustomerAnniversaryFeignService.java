/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerAnniversary;
import com.haohan.cloud.scm.api.crm.req.CustomerAnniversaryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户纪念日内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerAnniversaryFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerAnniversaryFeignService {


    /**
     * 通过id查询客户纪念日
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerAnniversary/{id}", method = RequestMethod.GET)
    R<CustomerAnniversary> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户纪念日 列表信息
     *
     * @param customerAnniversaryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerAnniversary/fetchCustomerAnniversaryPage")
    R<Page<CustomerAnniversary>> getCustomerAnniversaryPage(@RequestBody CustomerAnniversaryReq customerAnniversaryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户纪念日 列表信息
     *
     * @param customerAnniversaryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerAnniversary/fetchCustomerAnniversaryList")
    R<List<CustomerAnniversary>> getCustomerAnniversaryList(@RequestBody CustomerAnniversaryReq customerAnniversaryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户纪念日
     *
     * @param customerAnniversary 客户纪念日
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/add")
    R<Boolean> save(@RequestBody CustomerAnniversary customerAnniversary, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户纪念日
     *
     * @param customerAnniversary 客户纪念日
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/update")
    R<Boolean> updateById(@RequestBody CustomerAnniversary customerAnniversary, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户纪念日
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/listByIds")
    R<List<CustomerAnniversary>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerAnniversaryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/countByCustomerAnniversaryReq")
    R<Integer> countByCustomerAnniversaryReq(@RequestBody CustomerAnniversaryReq customerAnniversaryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerAnniversaryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/getOneByCustomerAnniversaryReq")
    R<CustomerAnniversary> getOneByCustomerAnniversaryReq(@RequestBody CustomerAnniversaryReq customerAnniversaryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerAnniversaryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerAnniversary/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerAnniversary> customerAnniversaryList, @RequestHeader(SecurityConstants.FROM) String from);


}
