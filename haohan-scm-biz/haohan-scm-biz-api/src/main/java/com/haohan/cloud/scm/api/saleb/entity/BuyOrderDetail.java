/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.saleb.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-13 20:34:19
 */
@Data
@TableName("scm_ops_buy_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单明细")
public class BuyOrderDetail extends Model<BuyOrderDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 汇总单号
     */
    @TableField(value = "smmary_buy_id")
    private String summaryBuyId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购明细编号
     */
    private String buyDetailSn;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 商品规格ID
     */
    @TableField(value = "goods_id")
    private String goodsModelId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品规格
     */
    private String goodsModel;
    /**
     * 采购数量
     */
    private BigDecimal goodsNum;
    /**
     * 市场价格
     */
    private BigDecimal marketPrice;
    /**
     * 采购价格
     */
    private BigDecimal buyPrice;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private BuyOrderStatusEnum status;
    /**
     * 零售单明细id
     */
    private String goodsOrderDetailId;
    /**
     * 商品下单数量   记录商品原始的下单数量
     */
    private BigDecimal orderGoodsNum;
    /**
     * 客户订单明细汇总状态:0未处理1已汇总2已备货
     */
    private DetailSummaryFlagEnum summaryFlag;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
