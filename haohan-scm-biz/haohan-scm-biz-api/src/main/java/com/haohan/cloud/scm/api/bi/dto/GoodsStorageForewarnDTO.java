package com.haohan.cloud.scm.api.bi.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author cx
 * @date 2019/7/10
 */
@Data
public class GoodsStorageForewarnDTO {

    private Map<String,List<String>> data;

    private List<Object> series;
}
