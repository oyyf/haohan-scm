/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.AppOnlineManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 应用上线管理
 *
 * @author haohan
 * @date 2019-05-28 20:33:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "应用上线管理")
public class AppOnlineManageReq extends AppOnlineManage {

    private long pageSize;
    private long pageNo;




}
