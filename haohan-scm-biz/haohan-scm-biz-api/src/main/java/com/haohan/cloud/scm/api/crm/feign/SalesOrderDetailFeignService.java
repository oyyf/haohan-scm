/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import com.haohan.cloud.scm.api.crm.req.SalesOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 销售订单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesOrderDetailFeignService", value = ScmServiceName.SCM_CRM)
public interface SalesOrderDetailFeignService {


    /**
     * 通过id查询销售订单明细
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesOrderDetail/{id}", method = RequestMethod.GET)
    R<SalesOrderDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 销售订单明细 列表信息
     *
     * @param salesOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesOrderDetail/fetchSalesOrderDetailPage")
    R<Page<SalesOrderDetail>> getSalesOrderDetailPage(@RequestBody SalesOrderDetailReq salesOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 销售订单明细 列表信息
     *
     * @param salesOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SalesOrderDetail/fetchSalesOrderDetailList")
    R<List<SalesOrderDetail>> getSalesOrderDetailList(@RequestBody SalesOrderDetailReq salesOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增销售订单明细
     *
     * @param salesOrderDetail 销售订单明细
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/add")
    R<Boolean> save(@RequestBody SalesOrderDetail salesOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改销售订单明细
     *
     * @param salesOrderDetail 销售订单明细
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/update")
    R<Boolean> updateById(@RequestBody SalesOrderDetail salesOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除销售订单明细
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/listByIds")
    R<List<SalesOrderDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/countBySalesOrderDetailReq")
    R<Integer> countBySalesOrderDetailReq(@RequestBody SalesOrderDetailReq salesOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/getOneBySalesOrderDetailReq")
    R<SalesOrderDetail> getOneBySalesOrderDetailReq(@RequestBody SalesOrderDetailReq salesOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SalesOrderDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesOrderDetail> salesOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
