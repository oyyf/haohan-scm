package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/20
 * 发货状态:-1不需发货 0待备货 1备货中 2已发货 4已接收 5售后中
 */
@Getter
@AllArgsConstructor
public enum ShipStatusEnum implements IBaseEnum {
    /**
     * 发货状态:不需发货
     */
    disabled("-1", "不需发货"),
    /**
     * 发货状态:待备货
     */
    stayCargo("0", "待备货"),
    /**
     * 发货状态:备货中
     */
    startCargo("1", "备货中"),
    /**
     * 发货状态:已发货
     */
    sendCargo("2", "已发货"),
    /**
     * 发货状态:已接收
     */
    receiveCargo("4", "已接收"),
    /**
     * 发货状态:售后中
     */
    afterSale("5", "售后中");

    private static final Map<String, ShipStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ShipStatusEnum e : ShipStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ShipStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;

}
