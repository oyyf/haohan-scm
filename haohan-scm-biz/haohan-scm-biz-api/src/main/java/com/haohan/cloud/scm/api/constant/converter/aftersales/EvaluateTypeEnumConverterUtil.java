package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.EvaluateTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class EvaluateTypeEnumConverterUtil implements Converter<EvaluateTypeEnum> {
    @Override
    public EvaluateTypeEnum convert(Object o, EvaluateTypeEnum evaluateTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EvaluateTypeEnum.getByType(o.toString());
    }
}
