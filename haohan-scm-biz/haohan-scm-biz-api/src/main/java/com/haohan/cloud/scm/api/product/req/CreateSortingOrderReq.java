package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/7/18
 */
@Data
@ApiModel("新增货品分拣单  根据B客户订单")
public class CreateSortingOrderReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value="平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "buyId不能为空")
    @ApiModelProperty(value="B客户订单编号", required = true)
    private String buyId;

}
