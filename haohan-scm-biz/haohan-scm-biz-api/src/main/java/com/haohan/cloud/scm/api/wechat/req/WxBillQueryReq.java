package com.haohan.cloud.scm.api.wechat.req;

import com.haohan.cloud.scm.api.bill.req.BillInfoReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/5/19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxBillQueryReq extends BillInfoReq {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "通行证id不能为空")
    private String uid;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;

}
