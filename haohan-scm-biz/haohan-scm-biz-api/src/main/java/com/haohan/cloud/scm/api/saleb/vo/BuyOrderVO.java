package com.haohan.cloud.scm.api.saleb.vo;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/28
 */
@Data
@NoArgsConstructor
public class BuyOrderVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 64, message = "采购编号长度最大64字符")
    @ApiModelProperty(value = "采购编号")
    private String buyOrderSn;

    @Length(max = 64, message = "采购商id长度最大64字符")
    @ApiModelProperty(value = "采购商id")
    private String buyerId;

    @Length(max = 64, message = "采购商名称长度最大64字符")
    @ApiModelProperty(value = "采购商名称")
    private String buyerName;

    @Length(max = 64, message = "采购用户长度最大64字符")
    @ApiModelProperty(value = "采购用户")
    private String buyerUid;

    @ApiModelProperty(value = "采购时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;

    @ApiModelProperty(value = "送货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "送货批次")
    private BuySeqEnum buySeq;

    @Length(max = 500, message = "采购需求长度最大500字符")
    @ApiModelProperty(value = "采购需求")
    private String needNote;

    @Digits(integer = 10, fraction = 2, message = "总价预估的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "总价预估")
    private BigDecimal genPrice;

    @Digits(integer = 10, fraction = 2, message = "采购总价的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "采购总价")
    private BigDecimal totalPrice;

    @Length(max = 64, message = "联系人长度最大64字符")
    @ApiModelProperty(value = "联系人")
    private String contact;

    @Length(max = 64, message = "联系电话长度最大64字符")
    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @Length(max = 500, message = "配送地址长度最大500字符")
    @ApiModelProperty(value = "配送地址")
    private String address;

    @ApiModelProperty(value = "订单状态")
    private BuyOrderStatusEnum status;

    @ApiModelProperty(value = "成交时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;

    @Digits(integer = 10, fraction = 2, message = "运费的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "运费")
    private BigDecimal shipFee;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @Length(max = 64, message = "零售单号长度最大64字符")
    @ApiModelProperty(value = "零售单号")
    private String goodsOrderId;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "商品种类数")
    private Integer goodsNum;

    @ApiModelProperty(value = "明细列表")
    private List<BuyOrderDetailVO> detailList;

    public BuyOrderVO(BuyOrder order) {
        this.buyOrderSn = order.getBuyId();
        this.buyerId = order.getBuyerId();
        this.buyerName = order.getBuyerName();
        this.buyerUid = order.getBuyerUid();
        this.buyTime = order.getBuyTime();
        this.deliveryDate = order.getDeliveryTime();
        this.buySeq = order.getBuySeq();
        this.needNote = order.getNeedNote();
        this.genPrice = order.getGenPrice();
        this.totalPrice = order.getTotalPrice();
        this.contact = order.getContact();
        this.telephone = order.getTelephone();
        this.address = order.getAddress();
        this.status = order.getStatus();
        this.dealTime = order.getDealTime();
        this.shipFee = order.getShipFee();
        this.deliveryType = order.getDeliveryType();
        this.goodsOrderId = order.getGoodsOrderId();
        this.payStatus = null == order.getPayStatus() ? PayStatusEnum.wait : order.getPayStatus();
        this.goodsNum = order.getGoodsNum();
        this.remarks = order.getRemarks();
    }
}
