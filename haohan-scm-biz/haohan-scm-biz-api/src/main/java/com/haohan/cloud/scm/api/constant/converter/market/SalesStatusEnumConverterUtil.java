package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.SalesStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class SalesStatusEnumConverterUtil implements Converter<SalesStatusEnum> {
    @Override
    public SalesStatusEnum convert(Object o, SalesStatusEnum salesStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SalesStatusEnum.getByType(o.toString());
    }
}
