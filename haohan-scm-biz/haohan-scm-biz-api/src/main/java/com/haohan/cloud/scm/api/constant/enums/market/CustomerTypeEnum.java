package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum CustomerTypeEnum implements IBaseEnum {
    /**
     * 客户类型:1经销商2门店3竞争者 4合作伙伴 5其他
     */
    dealer("1", "经销商"),
    store("2", "门店"),
    competitor("3", "竞争者"),
    partner("4", "合作伙伴"),
    rests("5", "其他");

    private static final Map<String, CustomerTypeEnum> MAP = new HashMap<>(8);
    private static final Map<String, CustomerTypeEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (CustomerTypeEnum e : CustomerTypeEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static CustomerTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    public static CustomerTypeEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
