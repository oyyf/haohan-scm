package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/7/5
 */
@Data
@ApiModel("B客户订单取消")
public class CancelBuyOrderReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id")
    private String pmId;

    @ApiModelProperty(value = "订单明细编号")
    private String buyDetailSn;

    @ApiModelProperty(value = "订单编号")
    private String buyId;

    @ApiModelProperty(value = "备注")
    private String remarks;


}
