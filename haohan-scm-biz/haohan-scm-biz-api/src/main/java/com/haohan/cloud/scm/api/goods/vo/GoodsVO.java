package com.haohan.cloud.scm.api.goods.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import com.haohan.cloud.scm.api.manage.vo.PhotoVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author dy
 * @date 2019/11/2
 * 商品展示及使用属性(最新)
 */
@Data
@NoArgsConstructor
public class GoodsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品描述")
    private String detailDesc;

    @ApiModelProperty(value = "图片地址")
    private String thumbUrl;

    @ApiModelProperty(value = "商品唯一编号")
    private String goodsSn;

    @ApiModelProperty(value = "概要描述，用于商品名称别名搜索")
    private String simpleDesc;
    /**
     * 图片组编号
     */
    private String photoGroupNum;
    /**
     * 库存数量
     */
    private BigDecimal storage;
    /**
     * 排序
     */
    private String sort;
    /**
     * 扫码购编码
     */
    private String scanCode;
    /**
     * 公共商品库通用编号
     */
    private String generalSn;

    // 状态属性
    /**
     * 是否上架 YesNoEnum  0.否 1.是
     */
    private YesNoEnum isMarketable;
    /**
     * 售卖规则标记
     */
    private YesNoEnum saleRule;
    /**
     * 服务选项标记
     */
    private YesNoEnum serviceSelection;
    /**
     * 配送规则标记
     */
    private YesNoEnum deliveryRule;
    /**
     * 赠品标记
     */
    private YesNoEnum goodsGift;
    /**
     * 商品规格标记
     */
    private YesNoEnum goodsModel;
    /**
     * 商品状态(出售中/仓库中/已售罄)
     */
    private GoodsStatusEnum goodsStatus;
    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    private GoodsFromTypeEnum goodsFrom;
    /**
     * 商品类型
     */
    private GoodsTypeEnum goodsType;
    /**
     * 是否c端销售
     */
    private YesNoEnum salecFlag;

    // 默认价格属性
    /**
     * 零售定价,单位元 (使用,市场价)
     */
    private BigDecimal marketPrice;
    /**
     * vip定价,单位元  (会员价)
     */
    private BigDecimal vipPrice;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 计量单位
     */
    private String unit;

    // 其他属性
//    /**
//     * 第三方编号/即速商品id
//     */
//    private String thirdGoodsSn;
//
//    @ApiModelProperty(value = "行业名称")
//    private String industry;
//
//    @ApiModelProperty(value = "品牌名称")
//    private String brand;
//
//    @ApiModelProperty(value = "厂家制造商")
//    private String manufacturer;

    // 商家店铺

    @ApiModelProperty(value = "店铺ID")
    private String shopId;

    @ApiModelProperty(value = "商家ID")
    private String merchantId;

    // 分类

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    // 规格

    @ApiModelProperty(value = "商品规格属性名列表的映射")
    Map<String, List<String>> attrMap;


    @ApiModelProperty(value = "商品规格列表")
    List<GoodsModelVO> modelList;

    /**
     * todo salec优化后删除
     */
    @ApiModelProperty(value = "商品规格属性名列表")
    List<GoodsModelTotal> modelTotalList;

    // 图片组图片 (轮播图)

    @ApiModelProperty(value = "图片组图片 (轮播图)")
    List<PhotoVO> photoList;

    // 扩展

    @ApiModelProperty(value = "是否收藏")
    private YesNoEnum collectionStatus;

    public GoodsVO(Goods goods) {
        BeanUtil.copyProperties(goods, this);
        this.goodsId = goods.getId();
        this.categoryId = goods.getGoodsCategoryId();
        this.collectionStatus = YesNoEnum.no;
    }

    public void copyPrice(GoodsPriceRule priceRule) {
        this.marketPrice = priceRule.getMarketPrice();
        this.vipPrice = priceRule.getVipPrice();
        this.virtualPrice = priceRule.getVirtualPrice();
        this.unit = priceRule.getUnit();
    }

}
