package com.haohan.cloud.scm.api.purchase.resp;

import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseQueryPurchaseTaskListResp {
    private List list;
    private long size;
    private long current;
    private long total;
    private long pages;

}
