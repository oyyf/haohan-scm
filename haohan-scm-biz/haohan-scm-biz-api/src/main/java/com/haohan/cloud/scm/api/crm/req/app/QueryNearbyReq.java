package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2019/10/24
 * 附近的客户及市场查询
 */
@Data
public class QueryNearbyReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @ApiModelProperty("距离")
    private Integer distance = 3;

    @Pattern(regexp = ScmCommonConstant.LONGITUDE_PATTEN, message = "经度格式有误, 正负180度之间, 弧度数表示")
    @ApiModelProperty(value = "经度")
    private String longitude;

    @Pattern(regexp = ScmCommonConstant.LATITUDE_PATTEN, message = "纬度格式有误, 正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "纬度")
    private String latitude;

}
