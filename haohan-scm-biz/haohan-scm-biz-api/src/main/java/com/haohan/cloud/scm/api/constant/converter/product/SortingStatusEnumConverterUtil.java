package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class SortingStatusEnumConverterUtil implements Converter<SortingStatusEnum> {
    @Override
    public SortingStatusEnum convert(Object o, SortingStatusEnum sortingStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SortingStatusEnum.getByType(o.toString());
    }
}
