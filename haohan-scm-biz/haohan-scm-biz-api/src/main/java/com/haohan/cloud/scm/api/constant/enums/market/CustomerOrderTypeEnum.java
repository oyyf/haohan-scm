package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum CustomerOrderTypeEnum {
    /**
     * 客户订单类型:1.B客户采购单2.B客户门店采购单3.C客户零售单
     */
    bOfferOrder("1","B客户采购单"),
    bShopOfferOrder("2","B客户门店采购单"),
    cRetailOrder("3","C客户零售单");

    private static final Map<String, CustomerOrderTypeEnum> MAP = new HashMap<>(8);

    static {
        for (CustomerOrderTypeEnum e : CustomerOrderTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static CustomerOrderTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
