package com.haohan.cloud.scm.api.crm.dto.imp;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.market.LevelEnum;
import com.haohan.cloud.scm.api.crm.dto.SalesAreaDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class SalesAreaImport {

    @NotBlank(message = "区域名称不能为空")
    @Length(min = 1, max = 32, message = "区域名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @NotBlank(message = "上级区域名称不能为空")
    @Length(min = 1, max = 32, message = "上级区域名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "上级区域名称")
    private String parentName;

    @ApiModelProperty(value = "区域等级")
    private LevelEnum areaLevel;

    @Length(min = 1, max = 32, message = "上级区域名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "排序")
    private String sort;

    @Length(min = 0, max = 64, message = "备注信息的字符长度必须在0至64之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;


    public SalesAreaDTO transTo() {
        SalesAreaDTO area = new SalesAreaDTO();
        area.setAreaName(this.areaName);
        area.setParentName(this.parentName);
        area.setAreaLevel(this.areaLevel);
        area.setSort(StrUtil.isBlank(this.sort) ? "100" : this.sort);
        area.setRemarks(this.remarks);
        return area;
    }
}
