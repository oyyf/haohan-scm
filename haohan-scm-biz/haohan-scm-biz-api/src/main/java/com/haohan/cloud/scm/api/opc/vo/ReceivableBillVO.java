package com.haohan.cloud.scm.api.opc.vo;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/7/3
 */
@Data
@Api("应收账单信息(列表展示)")
public class ReceivableBillVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 应收订单编号
     */
    private String receivableBillSn;
    /**
     * 账单类型:
     * 应收:11订单应收 12退款应收 13销售应收
     * 应付:21采购应付 22退款应付
     */
    private BillTypeEnum billType;
    /**
     * 应收来源 订单编号
     */
    private String receivableOrderSn;
    /**
     * 采购商  => 账单归属人id(buyerId/supplierId)
     */
    private String customerId;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家id
     */
    private String merchantId;
    /**
     * 客户商家名称
     */
    private String merchantName;
    /**
     * 订单成交日期 =>应收订单送货日期 / 退货单申请日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dealDate;
    /**
     * 商品数量
     */
    private String goodsNum;
    /**
     * 应收货款  = orderAmount + otherAmount
     */
    private BigDecimal totalAmount;
    /**
     * 订单货款
     */
    private BigDecimal orderAmount;
    /**
     * 其他货款
     */
    private BigDecimal otherAmount;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 结算单编号
     */
    private String settlementSn;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    // 扩展属性
    /**
     * 制单人
     */
    private String createName;


}
