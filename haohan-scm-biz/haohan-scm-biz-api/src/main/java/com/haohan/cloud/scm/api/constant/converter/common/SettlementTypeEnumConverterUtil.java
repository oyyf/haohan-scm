package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class SettlementTypeEnumConverterUtil implements Converter<SettlementTypeEnum>{
    @Override
    public SettlementTypeEnum convert(Object o, SettlementTypeEnum settlementTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SettlementTypeEnum.getByType(o.toString());
    }

}
