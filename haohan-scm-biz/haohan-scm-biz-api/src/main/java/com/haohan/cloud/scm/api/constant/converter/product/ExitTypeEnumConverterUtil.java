package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class ExitTypeEnumConverterUtil implements Converter<ExitTypeEnum> {
    @Override
    public ExitTypeEnum convert(Object o, ExitTypeEnum exitTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ExitTypeEnum.getByType(o.toString());
    }
}
