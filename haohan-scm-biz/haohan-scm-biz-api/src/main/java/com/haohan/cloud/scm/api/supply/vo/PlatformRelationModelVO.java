package com.haohan.cloud.scm.api.supply.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/28
 * 平台商品规格 关联的供应商品规格
 */
@Data
@NoArgsConstructor
public class PlatformRelationModelVO {

    @ApiModelProperty(value = "平台商品规格id")
    private String platformModelId;

    @ApiModelProperty(value = "供应商品规格列表")
    List<SupplyModelExtVO> modelList;

    public PlatformRelationModelVO(String goodsModelId) {
        this.platformModelId = goodsModelId;
    }
}
