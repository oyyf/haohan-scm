package com.haohan.cloud.scm.api.constant.converter.bill;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;

/**
 * @author dy
 * @date 2019/11/26
 */
public class OrderTypeEnumConverterUtil implements Converter<OrderTypeEnum> {
    @Override
    public OrderTypeEnum convert(Object o, OrderTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return OrderTypeEnum.getByType(o.toString());
    }

}