package com.haohan.cloud.scm.api.opc.dto;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/9/20
 */
@Data
public class BillPaymentDetail {

    // 商品信息
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品规格ID
     */
    private String goodsModelId;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 市场价格
     */
    private BigDecimal marketPrice;

    // 订单信息
    /**
     * 成交价格
     */
    private BigDecimal dealPrice;
    /**
     * 商品下单数量
     */
    private BigDecimal orderGoodsNum;
    /**
     * 成交数量
     */
    private BigDecimal goodsNum;
    /**
     * 商品金额
     */
    private BigDecimal amount;


    public BillPaymentDetail(BuyOrderDetail detail) {
//        this.goodsId = detail.getGoodsId();
        this.goodsModelId = detail.getGoodsModelId();
        this.goodsName = detail.getGoodsName();
        this.modelName = detail.getGoodsModel();
        this.unit = detail.getUnit();
        this.goodsImg = detail.getGoodsImg();
        this.marketPrice = detail.getMarketPrice();
        this.dealPrice = detail.getBuyPrice();
        this.orderGoodsNum = detail.getOrderGoodsNum();
        this.goodsNum = detail.getGoodsNum();
        if (null != this.dealPrice && null != this.goodsNum) {
            this.amount = this.dealPrice.multiply(this.goodsNum);
        } else {
            this.amount = BigDecimal.ZERO;
        }
    }

    public BillPaymentDetail(ReturnOrderDetail detail) {
        this.goodsId = detail.getGoodsId();
        this.goodsModelId = detail.getGoodsModelId();
        this.goodsName = detail.getGoodsName();
        this.modelName = detail.getModelName();
        this.unit = detail.getUnit();
        this.goodsImg = detail.getGoodsImg();
//        this.marketPrice = detail.getMarketPrice();
        this.dealPrice = detail.getGoodsPrice();
//        this.orderGoodsNum = detail.getOrderGoodsNum();
        this.goodsNum = detail.getGoodsNum();
        if (null != this.dealPrice && null != this.goodsNum) {
            this.amount = this.dealPrice.multiply(this.goodsNum);
        } else {
            this.amount = BigDecimal.ZERO;
        }
    }

    public BillPaymentDetail(OfferOrder offer) {

    }
}
