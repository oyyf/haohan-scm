package com.haohan.cloud.scm.api.salec.trans;

import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.salec.entity.StoreCategory;
import lombok.experimental.UtilityClass;

/**
 * @author dy
 * @date 2020/6/19
 */
@UtilityClass
public class StoreCategoryTrans {

    /**
     * 商品分类属性 addTime 需单独设置
     * @param cate
     * @return
     */
    public StoreCategory categoryTrans(GoodsCategory cate){
        StoreCategory category = new StoreCategory();
        category.setId(Integer.parseInt(cate.getId()));
        category.setPid(Integer.parseInt(cate.getParentId()));
        category.setCateName(cate.getName());
        category.setSort(null == cate.getSort()?100: cate.getSort().intValue());
        category.setPic(null == cate.getSort()?null:cate.getLogo());
        category.setIsShow(1);
        category.setShopId(cate.getShopId());
        return category;
    }


}
