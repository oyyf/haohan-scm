/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退货单")
public class ReturnOrderReq extends ReturnOrder {

    private long pageSize;
    private long pageNo;
}
