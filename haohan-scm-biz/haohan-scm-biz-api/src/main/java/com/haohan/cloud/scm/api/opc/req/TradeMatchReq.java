/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.TradeMatch;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 交易匹配
 *
 * @author haohan
 * @date 2019-05-30 10:21:33
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "交易匹配")
public class TradeMatchReq extends TradeMatch {

    private long pageSize;
    private long pageNo;




}
