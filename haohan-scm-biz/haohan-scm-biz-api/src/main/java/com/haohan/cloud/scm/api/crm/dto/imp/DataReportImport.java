package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.DataReportDTO;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.trans.CrmReportTrans;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @author dy
 * @date 2019/10/18
 */
@Data
public class DataReportImport {

    @Length(min = 0, max = 32, message = "客户编码的字符长度必须在0至32之间")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @Length(min = 0, max = 32, message = "客户名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @NotNull(message = "日期不能为空")
    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @NotBlank(message = "上报人不能为空")
    @Length(min = 0, max = 32, message = "上报人的字符长度必须在1至32之间")
    @ApiModelProperty(value = "上报人名称")
    private String reportMan;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @Length(min = 0, max = 32, message = "商品规格编码的字符长度必须在1至32之间")
    @ApiModelProperty(value = "商品规格编码")
    private String modelSn;

    @Length(min = 0, max = 32, message = "商品名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(min = 0, max = 32, message = "商品规格名称的字符长度必须在0至32之间")
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @NotBlank(message = "销售单位不能为空")
    @Length(min = 0, max = 10, message = "销售单位的字符长度必须在0至10之间")
    @ApiModelProperty(value = "销售单位")
    private String goodsUnit;

    @Digits(integer = 8, fraction = 2, message = "销售单价的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "销售单价的大小必须在0至1000000之间")
    @ApiModelProperty(value = "销售单价")
    private BigDecimal tradePrice;

    @NotNull(message = "销售数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "销售数量的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "销售数量的大小必须在0至1000000之间")
    @ApiModelProperty(value = "销售数量")
    private BigDecimal goodsNum;

    @ApiModelProperty(value = "销售金额")
    private BigDecimal amount;

    @Length(min = 0, max = 64, message = "商品行备注的字符长度必须在0至64之间")
    @ApiModelProperty(value = "商品行备注")
    private String remarks;

    @ApiModelProperty(value = "上报类型 0库存1销售记录")
    private DataReportEnum reportType;

    /**
     * 销售单价有误时 会抛出异常
     *
     * @param model
     * @return
     */
    public DataReportDetail initReportDetail(GoodsModelDTO model) {
        DataReportDetail detail = new DataReportDetail();
        detail.setTradePrice(this.tradePrice);
        detail.setGoodsNum(this.goodsNum);
        detail.setSalesGoodsType(this.salesGoodsType);
        detail.setRemarks(this.remarks);
        CrmReportTrans.defaultDetailInfo(detail, reportDate);
        // 商品属性
        CrmReportTrans.reportDetailCopyGoods(detail, model);
        // 根据商品销售类型处理价格
        CrmReportTrans.choicePriceByType(detail, model);
        return detail;
    }

    public DataReportDTO initDataReport(MarketEmployee reportMan) {
        DataReportDTO report = new DataReportDTO();
        report.setReportType(this.reportType);
        report.setCustomerSn(this.customerSn);
        report.setCustomerName(this.customerName);
        // 上报人
        report.setReportManId(reportMan.getId());
        report.setReportMan(reportMan.getName());
        report.setReportTelephone(reportMan.getTelephone());

        report.setReportDate(this.reportDate);
        report.setReportStatus(DataReportStatusEnum.confirm);
        report.setDetailList(new ArrayList<>());
        return report;
    }
}
