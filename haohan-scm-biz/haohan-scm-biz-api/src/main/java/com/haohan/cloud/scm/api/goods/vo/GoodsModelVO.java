package com.haohan.cloud.scm.api.goods.vo;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/2
 * 商品规格展示及使用属性(最新)
 */
@Data
@NoArgsConstructor
public class GoodsModelVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 规格id
     */
    private String modelId;
    /**
     * 商品规格唯一编号
     */
    private String goodsModelSn;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 规格类型名称
     */
    private String typeName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格商品图片地址
     */
    private String modelUrl;
    /**
     * 扩展信息  (未使用, 原定为 规格类型及名称 如:"种类:普通,尺寸:大")
     */
    private String modelInfo;
    /**
     * 扫码购编码
     */
    private String modelCode;
    /**
     * 规格组合   (原系统使用, 用于映射规格类型和名称 如:"30,32")
     */
    private String model;

    // 价格属性

    /**
     * 规格价格
     */
    private BigDecimal modelPrice;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 参考成本价
     */
    private BigDecimal costPrice;
    /**
     * 规格单位
     */
    private String modelUnit;

    /**
     * 规格库存
     */
    private BigDecimal modelStorage;

//    /**
//     * 即速应用规格ID
//     */
//    private String itemsId;


//    /**
//     * 商品规格通用编号/公共商品库通用编号
//     */
//    private String modelGeneralSn;
//    /**
//     * 第三方规格编号/即速商品id
//     */
//    private String thirdModelSn;

    /**
     * 重量
     */
    private BigDecimal weight;
    /**
     * 体积
     */
    private BigDecimal volume;
    /**
     * 库存预警值
     */
    private Integer stocksForewarn;

    public GoodsModelVO(GoodsModel model) {
        // 无 typeName goodsName
        this.modelId = model.getId();
        this.goodsModelSn= model.getGoodsModelSn();
        this.goodsId =model.getGoodsId();
        this.modelName = model.getModelName();
        this.modelUrl = model.getModelUrl();
        this.modelInfo=model.getModelInfo();
        this.modelCode=model.getModelCode();
        this.model=model.getModel();
        this.modelPrice=model.getModelPrice();
        this.virtualPrice=model.getVirtualPrice();
        this.costPrice=model.getCostPrice();
        this.modelUnit=model.getModelUnit();
        this.modelStorage=model.getModelStorage();
        this.weight=model.getWeight();
        this.volume=model.getVolume();
        this.stocksForewarn=model.getStocksForewarn();
    }
}
