package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/15
 */
@Data
@ApiModel(description = "查询出库单详情（包含货品信息）")
public class QueryExitWarehouseDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "exitWarehouseSn不能为空")
    @ApiModelProperty(value = "出库单编号", required = true)
    private String exitWarehouseSn;
}
