package com.haohan.cloud.scm.api.crm.req.order;

import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/28
 */
@Data
public class SalesOrderAddReq {

    @NotBlank(message = "客户编号不能为空")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    private String customerSn;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "交货日期不能为空")
    private LocalDate deliveryDate;

    @NotNull(message = "销售单类型不能为空")
    private SalesTypeEnum salesType;

    @Length(min = 0, max = 32, message = "业务员id长度在0至32之间")
    private String employeeId;

    @NotBlank(message = "收货地址id不能为空")
    @Length(min = 0, max = 32, message = "收货地址id长度在0至32之间")
    private String customerAddressId;

    @Digits(integer = 8, fraction = 2, message = "总计金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "总计金额在0至1000000之间")
    private BigDecimal totalAmount;

    @Digits(integer = 8, fraction = 2, message = "其他金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "其他金额在0至1000000之间")
    private BigDecimal otherAmount;

    @Length(min = 0, max = 100, message = "订单备注长度在0至100之间")
    private String remarks;

    @Length(min = 0, max = 32, message = "供货商编号长度在0至32之间")
    private String supplierSn;

    @Valid
    @NotEmpty(message = "商品明细不能为空")
    private List<SalesDetailAddReq> detailList;

    public SalesOrder transTo() {
        SalesOrder order = new SalesOrder();
        order.setCustomerSn(this.customerSn);
        order.setDeliveryDate(this.deliveryDate);
        order.setSalesType(this.salesType);
        order.setEmployeeId(this.employeeId);
        order.setTotalAmount(this.totalAmount);
        order.setOtherAmount(this.otherAmount);
        order.setRemarks(this.remarks);
        order.setSupplierSn(this.supplierSn);
        // 默认支付状态
        order.setPayStatus(PayStatusEnum.wait);
        return order;
    }
}
