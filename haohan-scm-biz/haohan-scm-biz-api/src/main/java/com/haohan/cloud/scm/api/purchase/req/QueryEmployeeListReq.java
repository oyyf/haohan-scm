package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.EmployeeTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author xwx
 * @date 2019/7/4
 */
@Data
@ApiModel(description = "查询采购员工列表")
public class QueryEmployeeListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;
    /**
     * 采购员工类型:1采购总监2采购经理3采购员
     */
    @NotNull(message = "purchaseEmployeeType不能为空")
    @ApiModelProperty(value = "采购员工类型:1采购总监2采购经理3采购员", required = true)
    private EmployeeTypeEnum purchaseEmployeeType;
}
