package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author dy
 * @date 2019/10/28
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Api("客户拜访分析")
public class VisitAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "客户编号")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    private String customerSn;

    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成")
    private VisitStatusEnum visitStatus;

    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;

    @ApiModelProperty(value = "客户sn 集合")
    private Set<String> customerSnSet;

    @ApiModelProperty(value = "拜访员工id 集合")
    private Set<String> employeeIdSet;

    public VisitAnalysisDTO(String customerSn, LocalDate startDate, LocalDate endDate) {
        this.customerSn = customerSn;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
