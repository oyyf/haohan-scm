package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ShipOrderStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ShipOrderStatusEnumConverterUtil implements Converter<ShipOrderStatusEnum> {
    @Override
    public ShipOrderStatusEnum convert(Object o, ShipOrderStatusEnum shipOrderStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShipOrderStatusEnum.getByType(o.toString());
    }
}
