package com.haohan.cloud.scm.api.common.req.admin;

/**
 * @author shenyu
 * @create 2019/1/4
 */
public class PdsBuyerApiReq extends PdsBaseApiReq {
    private String buyerMerchantId;

    public String getBuyerMerchantId() {
        return buyerMerchantId;
    }

    public void setBuyerMerchantId(String buyerMerchantId) {
        this.buyerMerchantId = buyerMerchantId;
    }
}
