package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.EnterTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class EnterTypeEnumConverterUtil implements Converter<EnterTypeEnum> {
    @Override
    public EnterTypeEnum convert(Object o, EnterTypeEnum enterTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EnterTypeEnum.getByType(o.toString());
    }
}
