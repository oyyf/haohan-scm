package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ExitStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class ExitStatusEnumConverterUtil implements Converter<ExitStatusEnum> {
    @Override
    public ExitStatusEnum convert(Object o, ExitStatusEnum exitStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ExitStatusEnum.getByType(o.toString());
    }
}
