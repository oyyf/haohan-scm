/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 微信用户表
 *
 * @author haohan
 * @date 2020-05-21 18:45:52
 */
@Data
@TableName("eb_wechat_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "微信用户表")
public class WechatUser extends Model<WechatUser> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "微信用户id")
    @TableId(type = IdType.INPUT)
    private Integer uid;

    @Length(max = 30, message = "只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段长度最大30字符")
    @ApiModelProperty(value = "只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段")
    private String unionid;

    @Length(max = 30, message = "用户的标识，对当前公众号唯一长度最大30字符")
    @ApiModelProperty(value = "用户的标识，对当前公众号唯一")
    private String openid;

    @Length(max = 32, message = "小程序唯一身份ID长度最大32字符")
    @ApiModelProperty(value = "小程序唯一身份ID")
    private String routineOpenid;

    @Length(max = 64, message = "用户的昵称长度最大64字符")
    @ApiModelProperty(value = "用户的昵称")
    private String nickname;

    @Length(max = 256, message = "用户头像长度最大256字符")
    @ApiModelProperty(value = "用户头像")
    private String headimgurl;

    @ApiModelProperty(value = "用户的性别，值为1时是男性，值为2时是女性，值为0时是未知")
    private Integer sex;

    @Length(max = 64, message = "用户所在城市长度最大64字符")
    @ApiModelProperty(value = "用户所在城市")
    private String city;

    @Length(max = 64, message = "用户的语言，简体中文为zh_CN长度最大64字符")
    @ApiModelProperty(value = "用户的语言，简体中文为zh_CN")
    private String language;

    @Length(max = 64, message = "用户所在省份长度最大64字符")
    @ApiModelProperty(value = "用户所在省份")
    private String province;

    @Length(max = 64, message = "用户所在国家长度最大64字符")
    @ApiModelProperty(value = "用户所在国家")
    private String country;

    @Length(max = 256, message = "公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注长度最大256字符")
    @ApiModelProperty(value = "公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注")
    private String remark;

    @ApiModelProperty(value = "用户所在的分组ID（兼容旧的用户分组接口）")
    private Integer groupid;

    @Length(max = 256, message = "用户被打上的标签ID列表长度最大256字符")
    @ApiModelProperty(value = "用户被打上的标签ID列表")
    private String tagidList;

    @ApiModelProperty(value = "用户是否订阅该公众号标识")
    private Integer subscribe;

    @ApiModelProperty(value = "关注公众号时间")
    private Integer subscribeTime;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "一级推荐人")
    private Integer stair;

    @ApiModelProperty(value = "二级推荐人")
    private Integer second;

    @ApiModelProperty(value = "一级推荐人订单")
    private Integer orderStair;

    @ApiModelProperty(value = "二级推荐人订单")
    private Integer orderSecond;

    @Digits(integer = 8, fraction = 2, message = "佣金的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "佣金")
    private BigDecimal nowMoney;

    @Length(max = 32, message = "小程序用户会话密匙长度最大32字符")
    @ApiModelProperty(value = "小程序用户会话密匙")
    private String sessionKey;

    @Length(max = 32, message = "用户类型长度最大32字符")
    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
