package com.haohan.cloud.scm.api.goods.req.manage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 3219/11/2
 * goodsId 和 goodsSn 需1个
 */
@Data
public class GoodsFetchInfoReq {

    @Length(max = 32, message = "商品编号的长度最大为32字符")
    @ApiModelProperty(value = "商品唯一编号")
    private String goodsSn;

    @Length(max = 32, message = "商品id的长度最大为32字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    // 扩展

    @Length(max = 32, message = "采购商id的长度最大32字符")
    @ApiModelProperty("采购商id,用于联查平台商品定价")
    private String buyerId;

    @Length(max = 32, message = "平台商品定价商家id的长度最大32字符")
    @ApiModelProperty("平台商品定价商家id")
    private String pricingMerchantId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("平台商品定价查询日期")
    private LocalDate pricingDate;

    public GoodsPricingReq transTo() {
        GoodsPricingReq req = new GoodsPricingReq();
        req.setBuyerId(this.buyerId);
        req.setPricingMerchantId(this.pricingMerchantId);
        req.setPricingDate(this.getPricingDate());
        return req;
    }
}
