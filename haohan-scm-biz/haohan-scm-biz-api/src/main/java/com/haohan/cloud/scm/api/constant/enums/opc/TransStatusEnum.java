package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/6/4
 */
@Getter
@AllArgsConstructor
public enum TransStatusEnum {
    /**
     * 交易状态
     */
    done("0", "成交"),
    after_sale("1", "售后");

    private static final Map<String, TransStatusEnum> MAP = new HashMap<>(8);

    static {
        for (TransStatusEnum e : TransStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static TransStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
