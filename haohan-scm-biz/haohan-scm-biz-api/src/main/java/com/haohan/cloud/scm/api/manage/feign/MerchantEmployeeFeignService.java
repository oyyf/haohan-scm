/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.MerchantEmployee;
import com.haohan.cloud.scm.api.manage.req.MerchantEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 员工管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MerchantEmployeeFeignService", value = ScmServiceName.SCM_MANAGE)
public interface MerchantEmployeeFeignService {


    /**
     * 通过id查询员工管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/MerchantEmployee/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 员工管理 列表信息
     * @param merchantEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantEmployee/fetchMerchantEmployeePage")
    R getMerchantEmployeePage(@RequestBody MerchantEmployeeReq merchantEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 员工管理 列表信息
     * @param merchantEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantEmployee/fetchMerchantEmployeeList")
    R getMerchantEmployeeList(@RequestBody MerchantEmployeeReq merchantEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增员工管理
     * @param merchantEmployee 员工管理
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/add")
    R save(@RequestBody MerchantEmployee merchantEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改员工管理
     * @param merchantEmployee 员工管理
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/update")
    R updateById(@RequestBody MerchantEmployee merchantEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除员工管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/MerchantEmployee/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param merchantEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/countByMerchantEmployeeReq")
    R countByMerchantEmployeeReq(@RequestBody MerchantEmployeeReq merchantEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param merchantEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/getOneByMerchantEmployeeReq")
    R getOneByMerchantEmployeeReq(@RequestBody MerchantEmployeeReq merchantEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param merchantEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/MerchantEmployee/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<MerchantEmployee> merchantEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);


}
