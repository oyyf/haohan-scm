package com.haohan.cloud.scm.api.constant.converter.saleb;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;

/**
 * @author dy
 * @date 2019/6/15
 */
public class PayPeriodEnumConverterUtil implements Converter<PayPeriodEnum> {

    @Override
    public PayPeriodEnum convert(Object o, PayPeriodEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PayPeriodEnum.getByType(o.toString());
    }

}
