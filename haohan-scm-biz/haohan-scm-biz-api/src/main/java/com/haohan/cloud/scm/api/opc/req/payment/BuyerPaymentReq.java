/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:28:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购商货款统计")
public class BuyerPaymentReq extends BuyerPayment {

    private long pageSize;
    private long pageNo;

    /**
     * 应收账单 商家id
     */
    private String merchantId;

    /**
     * 应收账单 商家名称
     */
    private String merchantName;

}
