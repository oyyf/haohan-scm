package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/9/17
 */
@Data
@ApiModel("修改采购明细价格/数量")
public class ModifyBuyOrderDetailReq {

    /**
     * 采购明细编号
     */
    @NotEmpty(message = "采购明细编号不能为空")
    private String buyDetailSn;
    /**
     * 采购价格
     */
    @Range(min = 0, max = 1000000, message = "采购价格在0至1000000之间")
    private BigDecimal buyPrice;
    /**
     * 采购数量
     */
    @Range(min = 0, max = 1000000, message = "采购数量在0至1000000之间")
    private BigDecimal goodsNum;

    /**
     * 备注信息
     */
    @Length(min = 0, max = 200, message = "备注信息长度在0至200之间")
    private String remarks;


    public BuyOrderDetail transTo() {
        BuyOrderDetail detail = new BuyOrderDetail();
        detail.setBuyDetailSn(this.buyDetailSn);
        detail.setBuyPrice(this.buyPrice);
        detail.setGoodsNum(this.goodsNum);
        detail.setRemarks(this.remarks);
        return detail;
    }
}
