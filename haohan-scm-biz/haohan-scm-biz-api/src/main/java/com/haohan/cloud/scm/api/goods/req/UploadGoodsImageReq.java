package com.haohan.cloud.scm.api.goods.req;

import lombok.Data;

import java.util.List;

/**

 *
 * @author cx
 * @date 2019/7/20
 */

@Data
public class UploadGoodsImageReq {

    /**
     * 删除的图片编号
     */
    private List<String> delImages;

    /**
     * 上传的iamge编号
     */
    private List<String> updImages;

    /**
     * 图片组编号
     */
    private String photoGroupNum;

    /**
     * 商家ID
     */
    private String merchantId;

    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品id
     */
    private String goodsSn;
}
