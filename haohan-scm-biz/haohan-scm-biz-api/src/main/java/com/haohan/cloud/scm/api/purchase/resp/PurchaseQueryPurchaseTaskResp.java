package com.haohan.cloud.scm.api.purchase.resp;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseQueryPurchaseTaskResp {

    /**
     * 发起人
     */
    private String initiatorId;
    /**
     * 发起人名称
     */
    private String initiatorName;
    /**
     * 执行人
     */
    private String transactorId;
    /**
     * 执行人名称
     */
    private String transactorName;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 任务执行方式:1.审核2.分配3.采购
     */
    private String taskActionType;
    /**
     * 任务状态1.待处理2.执行中3.已完成4.未完成
     */
    private String taskStatus;
    /**
     * 任务内容说明
     */
    private String content;
    /**
     * 任务执行备注
     */
    private String actionDesc;
    /**
     * 任务截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadlineTime;
    /**
     * 任务操作时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime actionTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer tenantId;


}
