package com.haohan.cloud.scm.api.manage.vo;

import com.haohan.cloud.scm.api.manage.entity.Merchant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/16
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MerchantShopVO extends MerchantVO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("店铺列表")
    private List<ShopVO> shopList;

    public MerchantShopVO(Merchant merchant){
        super(merchant);
    }

}
