package com.haohan.cloud.scm.api.goods;

import cn.hutool.http.HttpUtil;

import java.util.HashMap;
import java.util.Map;

public class GetGoodsListUtil implements RequestURL {
    /**
     * 获取商品列表请求
     *
     * @param
     * @return
     */
    public static String getGoodsListUtil(Map<String, Object> paramMap) {
        String result = HttpUtil.post(GET_GOODS_LIST_URL, paramMap);
        return result;
    }

    public static void main(String[] args) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("pmId", "2ec13acfd7284e02b33d7d2acbca9d13");
        paramMap.put("shopId", "19fe47f661794ddf9f1caf560d2d685d");
        paramMap.put("haohan.session.id", "83b7918f68284de59c352f78b3d1c156");
        String goodsListUtil = GetGoodsListUtil.getGoodsListUtil(paramMap);
        System.out.println(goodsListUtil);
    }

}
