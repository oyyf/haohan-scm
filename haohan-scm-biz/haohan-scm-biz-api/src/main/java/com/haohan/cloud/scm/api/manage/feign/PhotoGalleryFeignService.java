/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.req.PhotoGalleryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 资源图片库内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PhotoGalleryFeignService", value = ScmServiceName.SCM_MANAGE)
public interface PhotoGalleryFeignService {


    /**
     * 通过id查询资源图片库
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PhotoGallery/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 资源图片库 列表信息
     * @param photoGalleryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoGallery/fetchPhotoGalleryPage")
    R getPhotoGalleryPage(@RequestBody PhotoGalleryReq photoGalleryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 资源图片库 列表信息
     * @param photoGalleryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PhotoGallery/fetchPhotoGalleryList")
    R getPhotoGalleryList(@RequestBody PhotoGalleryReq photoGalleryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/add")
    R save(@RequestBody PhotoGallery photoGallery, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改资源图片库
     * @param photoGallery 资源图片库
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/update")
    R updateById(@RequestBody PhotoGallery photoGallery, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除资源图片库
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PhotoGallery/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/countByPhotoGalleryReq")
    R countByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param photoGalleryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/getOneByPhotoGalleryReq")
    R getOneByPhotoGalleryReq(@RequestBody PhotoGalleryReq photoGalleryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param photoGalleryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PhotoGallery/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PhotoGallery> photoGalleryList, @RequestHeader(SecurityConstants.FROM) String from);


}
