package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.ProductTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class ProductTypeEnumConverterUtil implements Converter<ProductTypeEnum> {
    @Override
    public ProductTypeEnum convert(Object o, ProductTypeEnum productTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ProductTypeEnum.getByType(o.toString());
    }
}
