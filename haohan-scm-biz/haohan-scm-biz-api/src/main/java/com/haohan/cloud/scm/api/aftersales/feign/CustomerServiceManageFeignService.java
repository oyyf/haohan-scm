/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;
import com.haohan.cloud.scm.api.aftersales.req.CustomerServiceManageReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 客户项目售后维护管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerServiceManageFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface CustomerServiceManageFeignService {


    /**
     * 通过id查询客户项目售后维护管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerServiceManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户项目售后维护管理 列表信息
     * @param customerServiceManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerServiceManage/fetchCustomerServiceManagePage")
    R getCustomerServiceManagePage(@RequestBody CustomerServiceManageReq customerServiceManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户项目售后维护管理 列表信息
     * @param customerServiceManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerServiceManage/fetchCustomerServiceManageList")
    R getCustomerServiceManageList(@RequestBody CustomerServiceManageReq customerServiceManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/add")
    R save(@RequestBody CustomerServiceManage customerServiceManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/update")
    R updateById(@RequestBody CustomerServiceManage customerServiceManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户项目售后维护管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/CustomerServiceManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/countByCustomerServiceManageReq")
    R countByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/getOneByCustomerServiceManageReq")
    R getOneByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerServiceManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerServiceManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<CustomerServiceManage> customerServiceManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
