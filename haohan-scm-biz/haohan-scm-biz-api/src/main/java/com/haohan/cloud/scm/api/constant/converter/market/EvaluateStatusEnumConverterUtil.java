package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.EvaluateStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class EvaluateStatusEnumConverterUtil implements Converter<EvaluateStatusEnum> {

    @Override
    public EvaluateStatusEnum convert(Object o, EvaluateStatusEnum evaluateStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EvaluateStatusEnum.getByType(o.toString());
    }
}
