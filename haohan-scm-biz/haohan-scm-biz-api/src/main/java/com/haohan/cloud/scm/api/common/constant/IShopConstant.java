package com.haohan.cloud.scm.api.common.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dy on 2018/8/28.
 */
public interface IShopConstant {

    // 店铺级别
    enum ShopLevelType {
        head("0", "总店"),
        sub("1", "子店"),
        pds("2", "采购配送店");

        private String code;
        private String desc;

        ShopLevelType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }


    // 店铺图片类型
    enum ShopPhotoType {
        logo("10", "店铺Logo"),
        payCode("11", "店铺收款二维码"),
        qrcode("12", "店铺二维码"),
        card("13", "名片二维码");

        private String code;
        private String desc;

        private static final Map<String, ShopPhotoType> MAP = new HashMap<>(8);

        static {
            for (ShopPhotoType type : ShopPhotoType.values()) {
                MAP.put(type.getCode(), type);
            }
        }

        public static ShopPhotoType getTypeByCode(String code) {
            if (MAP.containsKey(code)) {
                return MAP.get(code);
            }
            return null;
        }

        ShopPhotoType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    /**
     * 店铺认证类型
     */
    enum ShopAuthType {
        no("0", "未认证"),
        yes("1", "已认证");

        private String code;
        private String desc;
        private static final Map<String, ShopAuthType> MAP = new HashMap<>(8);

        static {
            for (ShopAuthType type : ShopAuthType.values()) {
                MAP.put(type.getCode(), type);
            }
        }

        public static ShopAuthType getTypeByCode(String code) {
            if (MAP.containsKey(code)) {
                return MAP.get(code);
            }
            return null;
        }

        ShopAuthType(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

}
