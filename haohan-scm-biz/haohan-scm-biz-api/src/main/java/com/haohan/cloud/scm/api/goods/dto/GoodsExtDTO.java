package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.goods.entity.Goods;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/9/13
 * 扩展基础属性 带价格信息
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("扩展商品属性")
public class GoodsExtDTO extends Goods {

    /**
     * 零售定价,单位元 (使用,市场价)
     */
    private BigDecimal marketPrice;
    /**
     * vip定价,单位元  (会员价)
     */
    private BigDecimal vipPrice;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 计量单位
     */
    private String unit;

}
