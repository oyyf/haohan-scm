/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.SortingOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 分拣单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SortingOrderFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface SortingOrderFeignService {


    /**
     * 通过id查询分拣单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SortingOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 分拣单 列表信息
     * @param sortingOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SortingOrder/fetchSortingOrderPage")
    R getSortingOrderPage(@RequestBody SortingOrderReq sortingOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 分拣单 列表信息
     * @param sortingOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SortingOrder/fetchSortingOrderList")
    R getSortingOrderList(@RequestBody SortingOrderReq sortingOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/add")
    R save(@RequestBody SortingOrder sortingOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/update")
    R updateById(@RequestBody SortingOrder sortingOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除分拣单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SortingOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/countBySortingOrderReq")
    R countBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/getOneBySortingOrderReq")
    R getOneBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param sortingOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SortingOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SortingOrder> sortingOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
