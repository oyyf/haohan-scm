package com.haohan.cloud.scm.api.goods.req.manage;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/7
 * 新增 FirstGroup
 * 修改 SecondGroup
 */
@Data
public class EditGoodsCategoryReq {

    @NotBlank(message = "分类id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "分类id的长度最大32字符")
    @ApiModelProperty("主键")
    private String categoryId;

    @NotBlank(message = "店铺id不能为空")
    @Length(max = 32, message = "店铺id的长度最大32字符")
    @ApiModelProperty("店铺id")
    private String shopId;

    @NotBlank(message = "分类名称不能为空")
    @Length(max = 10, message = "分类名称的长度最大10字符")
    @ApiModelProperty("分类名称")
    private String name;

    @Length(max = 32, message = "父级分类id的长度最大32字符")
    @ApiModelProperty("父级分类id")
    private String parentId;

    @Length(max = 5, message = "排序值的长度最大5字符")
    @ApiModelProperty("排序值")
    private String sort;

    @Length(max = 255, message = "分类logo图片地址的长度最大255字符")
    @ApiModelProperty("分类logo图片地址")
    private String logo;

    @ApiModelProperty("分类树中是否展示")
    private YesNoEnum categoryType;

    @ApiModelProperty("是否上架所有商品")
    private YesNoEnum status;

    @ApiModelProperty("备注")
    @Length(max = 255, message = "备注的长度最大255字符")
    private String remarks;

    public GoodsCategory transTo() {
        GoodsCategory category = new GoodsCategory();
        category.setId(this.categoryId);
        category.setShopId(this.shopId);
        category.setName(this.name);
        category.setParentId(this.parentId);
        int s = 100;
        if (StrUtil.isNotBlank(this.sort)) {
            try {
                s = Integer.parseInt(this.sort);
            } catch (Exception ignored) {
            }
        }
        category.setSort(new BigDecimal(s));
        category.setLogo(this.logo);
        category.setCategoryType(this.categoryType);
        category.setStatus(this.status);
        category.setRemarks(this.remarks);
        return category;
    }


}
