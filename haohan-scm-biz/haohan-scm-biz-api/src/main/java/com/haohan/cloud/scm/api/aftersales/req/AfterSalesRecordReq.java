/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 售后记录
 *
 * @author haohan
 * @date 2019-05-30 10:24:29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "售后记录")
public class AfterSalesRecordReq extends AfterSalesRecord {

    private long pageSize;
    private long pageNo;




}
