package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;

/**
 * @author dy
 * @date 2020/1/14
 */
public class InMailTypeEnumConverterUtil implements Converter<InMailTypeEnum> {
    @Override
    public InMailTypeEnum convert(Object o, InMailTypeEnum messageTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return InMailTypeEnum.getByType(o.toString());
    }
}
