package com.haohan.cloud.scm.api.opc.req.payment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
@ApiModel(description = "查询应付账单详情")
public class FetchSupplierPaymentReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "供应商货款id",required = true)
    private String id;
}
