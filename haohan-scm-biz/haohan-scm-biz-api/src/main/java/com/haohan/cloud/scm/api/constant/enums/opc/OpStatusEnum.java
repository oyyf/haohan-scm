package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/6/4
 */
@Getter
@AllArgsConstructor
public enum OpStatusEnum {
    /**
     * 运营状态
     */
    prepare("0", "备货中"),
    feight("1", "待揽货"),
    waitSortOut("2", "待分拣"),
    sorted("3", "已分拣"),
    truckLoad("4", "已装车");
    private static final Map<String, OpStatusEnum> MAP = new HashMap<>(8);

    static {
        for (OpStatusEnum e : OpStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static OpStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
