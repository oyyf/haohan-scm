package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.AllocateTypeEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class AllocateTypeEnumConverterUtil implements Converter<AllocateTypeEnum> {

    @Override
    public AllocateTypeEnum convert(Object o, AllocateTypeEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AllocateTypeEnum.getByType(o.toString());
    }

}
