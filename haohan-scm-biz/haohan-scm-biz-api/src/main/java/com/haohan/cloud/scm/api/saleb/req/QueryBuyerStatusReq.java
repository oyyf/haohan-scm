package com.haohan.cloud.scm.api.saleb.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author dy
 * @date 2019/9/19
 */
@Data
public class QueryBuyerStatusReq implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 平台商家id (废弃参数)
     */
    @Length(max = 64, message = "pmId最大64字符")
    private String pmId;

    /**
     * 采购商id
     */
    @NotBlank(message = "采购商id不能为空")
    @Length(max = 64, message = "采购商id最大64字符")
    private String buyerId;

}
