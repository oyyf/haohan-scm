/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.SupplierAgreement;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 协议供应商品记录
 *
 * @author haohan
 * @date 2019-05-29 13:13:43
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "协议供应商品记录")
public class SupplierAgreementReq extends SupplierAgreement {

    private long pageSize=10;
    private long pageNo=1;
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    @NotBlank(message = "uId不能为空")
    private String uid;



}
