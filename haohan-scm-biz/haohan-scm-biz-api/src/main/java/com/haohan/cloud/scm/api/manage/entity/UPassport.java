/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.manage.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegFromEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsUpassportStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 用户通行证
 *
 * @author haohan
 * @date 2019-05-13 17:27:42
 */
@Data
@TableName("scm_u_passport")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户通行证")
public class UPassport extends Model<UPassport> {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 手机
     */
    private String telephone;
    /**
     * 密码
     */
    private String password;
    /**
     * 登录盐值
     */
    private String salt;
    /**
     * 注册时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regTime;
    /**
     * 注册IP
     */
    private String regIp;
    /**
     * 注册方式 0：login_name；1：telephone；2：email 3：QQ；4：sinaweibo； 5：weixin
     */
    private PdsRegTypeEnum regType;
    /**
     * 注册类型 0 ：Web端；1： 移动端； 2： Wap端
     */
    private PdsRegFromEnum regFrom;
    /**
     * 唯一ID
     */
    private String unionId;
    /**
     * 服务ID
     */
    private String serviceId;
    /**
     * 状态 0 ：冻结  1：正常  2 待审核 3 小白用户
     */
    private PdsUpassportStatusEnum status;
    /**
     * 冻结原因
     */
    private String reason;
    /**
     * 备注
     */
    private String memo;
    /**
     * 是否测试
     */
    private YesNoEnum isTest;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
