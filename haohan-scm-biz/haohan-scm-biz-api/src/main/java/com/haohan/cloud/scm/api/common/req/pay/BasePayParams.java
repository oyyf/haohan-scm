package com.haohan.cloud.scm.api.common.req.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @program: haohan-fresh-scm
 * @description: 支付请求基础参数
 * @author: Simon
 * @create: 2019-07-26
 **/
@Data
public class BasePayParams implements Serializable {

  @JsonProperty("partner_id")
  @NotNull(message = "partnerId 不能为空")
  private String partnerId;


  @JsonProperty("pay_type")
  private String payType;


  @JsonProperty("order_id")
  @NotNull(message = "orderId 不能为空")
  private String orderId;

  @JsonProperty("order_type")
  private String orderType;

  @JsonProperty("pay_channel")
  private String payChannel;

  @JsonProperty("shop_id")
  private String shopId;


  @JsonProperty("merchant_id")
  private String merchantId;

}
