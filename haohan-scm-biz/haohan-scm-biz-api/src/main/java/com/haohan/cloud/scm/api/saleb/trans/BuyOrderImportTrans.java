package com.haohan.cloud.scm.api.saleb.trans;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/12
 */
@Data
public class BuyOrderImportTrans {


    public static final Map<String, String> HEADER_MAP = new HashMap<>(20);

    static {
        HEADER_MAP.put("配送日期", "deliveryTime");
        HEADER_MAP.put("配送批次", "buySeq");
        HEADER_MAP.put("采购商名称", "buyerName");
        HEADER_MAP.put("采购需求", "needNote");
        HEADER_MAP.put("配送方式", "deliveryType");
        HEADER_MAP.put("联系人", "contact");
        HEADER_MAP.put("联系电话", "telephone");
        HEADER_MAP.put("配送地址", "address");
        HEADER_MAP.put("运费", "shipFee");
        HEADER_MAP.put("商品名称", "goodsName");
        HEADER_MAP.put("商品规格", "goodsModel");
        HEADER_MAP.put("单位", "unit");
        HEADER_MAP.put("采购价格", "buyPrice");
        HEADER_MAP.put("采购数量", "orderGoodsNum");
        HEADER_MAP.put("采购备注", "remarks");
    }

}
