package com.haohan.cloud.scm.api.bill.trans;

import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.bill.entity.BaseBill;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import lombok.Data;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/27
 */
@Data
@UtilityClass
public class ScmBillTrans {

    /**
     * 账单类型 转换 订单类型
     *
     * @return
     */
    public OrderTypeEnum transBillOrderType(BillTypeEnum billType) {
        OrderTypeEnum orderType = null;
        switch (billType) {
            case order:
                orderType = OrderTypeEnum.buy;
                break;
            case offerBack:
            case orderBack:
                orderType = OrderTypeEnum.back;
                break;
            case sales:
                orderType = OrderTypeEnum.sales;
                break;
            case purchase:
                orderType = OrderTypeEnum.supply;
                break;
            default:
        }
        return orderType;
    }

    public static SettlementTypeEnum transSettlementType(BillTypeEnum billType) {
        SettlementTypeEnum type;
        switch (billType) {
            case order:
            case offerBack:
            case sales:
                type = SettlementTypeEnum.receivable;
                break;
            case orderBack:
            case purchase:
                type = SettlementTypeEnum.payable;
                break;
            default:
                throw new ErrorDataException("未定义的账单类型");
        }
        return type;
    }

    /**
     * 初始化预付账单 并设置预付金额
     *
     * @param bill          普通账单
     * @param advanceBill   预付账单
     * @param advanceAmount 预付金额
     * @return
     */
    public static <T extends BaseBill> void initAdvanceBillAndModifyBill(T bill, T advanceBill, BigDecimal advanceAmount) {
        advanceBill.setReviewStatus(ReviewStatusEnum.success);
        advanceBill.setBillType(bill.getBillType());
        advanceBill.setSettlementStatus(YesNoEnum.no);
        // 订单
        advanceBill.setPmId(bill.getPmId());
        advanceBill.setPmName(bill.getPmName());
        advanceBill.setOrderSn(bill.getOrderSn());
        advanceBill.setCustomerId(bill.getCustomerId());
        advanceBill.setCustomerName(bill.getCustomerName());
        advanceBill.setMerchantId(bill.getMerchantId());
        advanceBill.setMerchantName(bill.getMerchantName());
        advanceBill.setDealDate(bill.getDealDate());
        // 金额
        advanceBill.setBillAmount(advanceAmount);
        advanceBill.setAdvanceFlag(YesNoEnum.yes);
        advanceBill.setAdvanceAmount(advanceAmount);
        advanceBill.setOrderAmount(bill.getOrderAmount());

        // 修改普通账单金额
        bill.setAdvanceAmount(advanceAmount);
        bill.setBillAmount(bill.getBillAmount().subtract(advanceAmount));
    }

    /**
     * 初始化设置账单 设置订单信息
     *
     * @param order    订单
     * @param bill     账单
     * @param billType 账单类型
     * @return
     */
    public static <T extends BaseBill> void initBillByOrder(OrderInfoDTO order, T bill, BillTypeEnum billType) {
        bill.setReviewStatus(ReviewStatusEnum.wait);
        bill.setBillType(billType);
        bill.setSettlementStatus(YesNoEnum.no);
        // 订单
        bill.setPmId(order.getPmId());
        bill.setPmName(order.getPmName());
        bill.setOrderSn(order.getOrderSn());
        bill.setCustomerId(order.getCustomerId());
        bill.setCustomerName(order.getCustomerName());
        bill.setMerchantId(order.getMerchantId());
        bill.setMerchantName(order.getMerchantName());
        bill.setDealDate(order.getDealDate());
        // 金额
        bill.setBillAmount(order.getTotalAmount());
        bill.setAdvanceFlag(YesNoEnum.no);
        bill.setAdvanceAmount(BigDecimal.ZERO);
        bill.setOrderAmount(order.getTotalAmount());
    }

    public static <T extends BaseBill> SettlementAccount initSettlement(T bill) {
        SettlementAccount account = new SettlementAccount();
        account.setPmId(bill.getPmId());
        account.setPmName(bill.getPmName());
        account.setCompanyId(bill.getMerchantId());
        account.setCompanyName(bill.getMerchantName());
        account.setOrderSn(bill.getOrderSn());
        account.setBillSn(bill.getBillSn());
        account.setSettlementType(ScmBillTrans.transSettlementType(bill.getBillType()));
        account.setSettlementAmount(bill.getBillAmount());
        account.setPayType(PayTypeEnum.cash);
        account.setSettlementStatus(YesNoEnum.no);
        account.setSettlementStyle(SettlementStyleEnum.normal);
        return account;
    }

    public static SettlementAccount initSummarySettlement(SettlementAccount detail) {
        SettlementAccount summary = new SettlementAccount();
        summary.setPmId(detail.getPmId());
        summary.setPmName(detail.getPmName());
        summary.setCompanyId(detail.getCompanyId());
        summary.setCompanyName(detail.getCompanyName());
        summary.setPayType(PayTypeEnum.cash);
        summary.setSettlementStatus(YesNoEnum.no);
        summary.setSettlementStyle(SettlementStyleEnum.summary);
        return summary;
    }
}
