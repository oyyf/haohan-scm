/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.PlatformEmployee;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 平台员工管理
 *
 * @author haohan
 * @date 2019-05-30 10:23:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "平台员工管理")
public class PlatformEmployeeReq extends PlatformEmployee {

    private long pageSize;
    private long pageNo;




}
