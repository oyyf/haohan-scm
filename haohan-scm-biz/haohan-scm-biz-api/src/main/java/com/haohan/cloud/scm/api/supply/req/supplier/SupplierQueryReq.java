package com.haohan.cloud.scm.api.supply.req.supplier;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/12
 * 供应商列表查询
 */
@Data
public class SupplierQueryReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(min = 0, max = 64, message = "商家ID长度必须介于 0 和 64 之间")
    private String merchantId;

    @ApiModelProperty(value = "账期")
    private PayPeriodEnum payPeriod;

    @Length(min = 0, max = 2, message = "账期日长度必须介于 0 和 2 之间")
    private String payDay;

    @ApiModelProperty(value = "启用状态")
    private UseStatusEnum status;

    @ApiModelProperty(value = "供应商类型")
    private PdsSupplierTypeEnum supplierType;

    @ApiModelProperty(value = "是否开启消息推送")
    private YesNoEnum needPush;

    @Length(min = 0, max = 5, message = "排序值长度必须介于 0 和 5 之间")
    private String sort;

    @ApiModelProperty(value = "评级")
    private GradeTypeEnum supplierLevel;

    // 非eq

    @Length(min = 0, max = 64, message = "商家名称长度必须介于 0 和 64 之间")
    private String merchantName;

    @Length(min = 0, max = 64, message = "供应商全称长度必须介于 0 和 64 之间")
    private String supplierName;

    @Length(min = 0, max = 64, message = "供应商简称长度必须介于 0 和 64 之间")
    private String shortName;

    @Length(min = 0, max = 64, message = "联系人长度必须介于 0 和 64 之间")
    private String contact;

    @Length(min = 0, max = 15, message = "电话长度必须介于 0 和 15 之间")
    private String telephone;

    @Length(min = 0, max = 64, message = "供应商地址长度必须介于 0 和 64 之间")
    private String address;

    @Length(min = 0, max = 255, message = "标签长度必须介于 0 和 255 之间")
    private String tags;

    @ApiModelProperty(value = "地址定位区域")
    private String area;

    public Supplier transTo() {
        Supplier supplier = new Supplier();
        supplier.setMerchantId(this.merchantId);
        supplier.setPayPeriod(this.payPeriod);
        supplier.setPayDay(this.payDay);
        supplier.setStatus(this.status);
        supplier.setSupplierType(this.supplierType);
        supplier.setNeedPush(this.needPush);
        supplier.setSort(StrUtil.isEmpty(this.sort) ? null : this.sort);
        supplier.setSupplierLevel(this.supplierLevel);
        return supplier;
    }
}
