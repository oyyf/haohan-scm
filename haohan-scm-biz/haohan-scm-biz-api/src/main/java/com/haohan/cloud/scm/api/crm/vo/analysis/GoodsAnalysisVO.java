package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysisDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/16
 */
@Data
@NoArgsConstructor
public class GoodsAnalysisVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统计名称", notes = "分类,规格")
    private String label;

    @ApiModelProperty(value = "统计数量")
    private BigDecimal value;

    public GoodsAnalysisVO(SalesReportAnalysisDetail detail) {
        this.label = detail.getName();
        this.value = detail.getNum();
    }

}
