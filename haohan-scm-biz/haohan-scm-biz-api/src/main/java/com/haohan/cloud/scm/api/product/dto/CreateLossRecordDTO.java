package com.haohan.cloud.scm.api.product.dto;

import com.haohan.cloud.scm.api.constant.enums.product.LossTypeEnum;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import lombok.Data;

/**
 * @author dy
 * @date 2019/6/13
 * 用于 货品创建损耗记录
 */
@Data
public class CreateLossRecordDTO {

    /**
     * 损耗前 货品
     */
    private ProductInfo source;
    /**
     * 损耗后 正常货品
     */
    private ProductInfo normal;
    /**
     * 损耗货品
     */
    private ProductInfo loss;
    /**
     * 关联记录编号
     */
    private String relationSn;
    /**
     * 损耗类型:1.采购2.生产加工3.分拣4.配送5.仓储盘点6.调拨
     */
    private LossTypeEnum lossType;


}


