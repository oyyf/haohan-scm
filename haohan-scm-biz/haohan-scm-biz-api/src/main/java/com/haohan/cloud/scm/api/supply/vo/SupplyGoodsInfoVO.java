package com.haohan.cloud.scm.api.supply.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyRelationTypeEnum;
import com.haohan.cloud.scm.api.goods.vo.GoodsVO;
import com.haohan.cloud.scm.api.manage.vo.PhotoVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author dy
 * @date 2020/4/18
 * 供应商品信息 spu及sku信息
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SupplyGoodsInfoVO extends SupplyGoodsVO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规格列表")
    List<SupplyModelVO> modelList;

    @ApiModelProperty(value = "商品规格属性名列表的映射")
    Map<String, List<String>> attrMap;

    @ApiModelProperty(value = "图片组图片 (轮播图)")
    List<PhotoVO> photoList;

    /**
     * 未设置modelList
     *
     * @param goods
     */
    public SupplyGoodsInfoVO(GoodsVO goods) {
        BeanUtil.copyProperties(goods, this, "modelList", "isMarketable");
        this.setIsMarketable(goods.getIsMarketable() == YesNoEnum.yes ? 1 : 0);
        this.setGoodsId(goods.getGoodsId());
        this.setCategoryId(goods.getCategoryId());
        this.setRelationType(SupplyRelationTypeEnum.no);
    }

}
