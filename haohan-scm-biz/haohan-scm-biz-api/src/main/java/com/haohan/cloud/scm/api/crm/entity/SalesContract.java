/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.market.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 销售合同
 *
 * @author haohan
 * @date 2019-08-30 12:02:10
 */
@Data
@TableName("crm_sales_contract")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售合同")
public class SalesContract extends Model<SalesContract> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 合同编号
     */
    @ApiModelProperty(value = "合同编号")
    private String salesContractSn;
    /**
     * 合同状态:1待签订2已签订3未签订
     */
    @ApiModelProperty(value = "合同状态:1待签订2已签订3未签订")
    private ContractStatusEnum contractStatus;
    /**
     * 合同描述
     */
    @ApiModelProperty(value = "合同描述")
    private String contractDesc;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 客户联系人id
     */
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;
    /**
     * 客户联系人姓名
     */
    @ApiModelProperty(value = "客户联系人姓名")
    private String linkmanName;
    /**
     * 销售机会id
     */
    @ApiModelProperty(value = "销售机会id")
    private String salesLeadsId;
    /**
     * 合同金额
     */
    @ApiModelProperty(value = "合同金额")
    private BigDecimal contractAmount;
    /**
     * 签约时间
     */
    @ApiModelProperty(value = "签约时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime contractTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedTime;
    /**
     * 条件条款
     */
    @ApiModelProperty(value = "条件条款")
    private String conditions;
    /**
     * 合同图片 图片组编号
     */
    @ApiModelProperty(value = "合同图片组编号")
    private String photoGroupNum;
    /**
     * 销售分润比例
     */
    @ApiModelProperty(value = "销售分润比例")
    private BigDecimal salesRate;
    /**
     * 市场负责人id
     */
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;
    /**
     * 市场负责人名称
     */
    @ApiModelProperty(value = "市场负责人名称")
    private String directorName;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
