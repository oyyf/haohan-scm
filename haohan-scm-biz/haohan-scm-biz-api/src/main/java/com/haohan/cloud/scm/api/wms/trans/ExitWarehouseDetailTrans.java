package com.haohan.cloud.scm.api.wms.trans;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ExitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.resp.ExitWarehouseDetailResp;

import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/15
 */
public class ExitWarehouseDetailTrans {
    public static ExitWarehouseDetailResp respTrans(ExitWarehouseDetail detail, ProductInfo info){
        ExitWarehouseDetailResp resp = new ExitWarehouseDetailResp();
        resp.setProductNumber(detail.getProductNumber());
        resp.setUnit(detail.getUnit());
        resp.setProductName(info.getProductName());
        resp.setPurchasePrice(info.getPurchasePrice());
        resp.setGoodsModelId(info.getGoodsModelId());
        return resp;
    }
    public static ExitWarehouseDetail trans(ProductInfo info){
        ExitWarehouseDetail exitWarehouseDetail = new ExitWarehouseDetail();
        exitWarehouseDetail.setApplyTime(LocalDateTime.now());
        exitWarehouseDetail.setUnit(info.getUnit());
        exitWarehouseDetail.setExitStatus(ExitStatusEnum.wait);
        exitWarehouseDetail.setDeliverySeq(BuySeqEnum.first);
        return exitWarehouseDetail;
    }

    /**
     * 根据客户订单明细创建出库单
     * @param info
     * @param detail
     * @return
     */
    public static ExitWarehouseDetail createDetailByBuyOrderDetail(ProductInfo info, BuyOrderDetail detail, BuyOrder order){
        ExitWarehouseDetail warehouseDetail = new ExitWarehouseDetail();
        warehouseDetail.setPmId(detail.getPmId());
        warehouseDetail.setExitType(ExitTypeEnum.delivery);
        warehouseDetail.setExitStatus(ExitStatusEnum.wait);
        warehouseDetail.setProductNumber(detail.getGoodsNum());
        warehouseDetail.setBuyOrderDetailSn(detail.getBuyDetailSn());
        warehouseDetail.setApplyTime(LocalDateTime.now());
        warehouseDetail.setDeliveryDate(order.getDeliveryTime());
        warehouseDetail.setDeliverySeq(order.getBuySeq());
        warehouseDetail.setUnit(info.getUnit());
        warehouseDetail.setProductSn(info.getProductSn());
        warehouseDetail.setGoodsModelId(detail.getGoodsModelId());
        return warehouseDetail;
    }
}
