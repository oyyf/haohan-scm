package com.haohan.cloud.scm.api.salec.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author cx
 * @date 2019/7/27
 */

@Data
public class AfterPayUpdReq {

    /**
     * 目前订单支付回调会修改值
     */
    @NotBlank(message = "商家id不能为空")
    @Length(max = 64, message = "商家id最大长度为64字符")
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @NotBlank(message = "支付订单id不能为空")
    @Length(max = 64, message = "支付订单id最大长度为64字符")
    private String orderId;

    @NotBlank(message = "支付状态不能为空")
    @Length(max = 5, message = "支付状态最大长度为5字符")
    @ApiModelProperty(value = "支付状态")
    private String payStatus;

    @NotBlank(message = "用户id不能为空")
    @Length(max = 64, message = "用户id最大长度为64字符")
    @ApiModelProperty(value = "用户id")
    private String uid;
}
