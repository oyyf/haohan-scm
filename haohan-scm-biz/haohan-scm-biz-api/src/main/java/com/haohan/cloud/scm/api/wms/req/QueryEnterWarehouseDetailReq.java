package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/9
 */
@Data
@ApiModel(description = "查询入库单明细（包含货品信息）根据入库单号")
public class QueryEnterWarehouseDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "enterWarehouseSn不能为空")
    @ApiModelProperty(value = "入库单编号", required = true)
    private String enterWarehouseSn;
}
