package com.haohan.cloud.scm.api.supply.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/4/23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SupplySqlDTO extends SelfSqlDTO {

    @ApiModelProperty(value = "供应商id列表, 逗号连接")
    private String supplierIds;

    @ApiModelProperty(value = "商品规格id", notes = "平台商品")
    private String modelId;

    @ApiModelProperty(value = "启用状态")
    private UseStatusEnum status;

    // 字典处理(自定义sql使用)

    public String getStatus(){
        return null == this.status ? null : this.status.getType();
    }

}
