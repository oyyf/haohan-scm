package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/8
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class QueryEnterDetailResp extends EnterWarehouse {
    private List<EnterDetailResp> list;
}
