/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * 客户联系人
 *
 * @author haohan
 * @date 2019-08-30 12:02:12
 */
@Data
@TableName("crm_customer_linkman")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户联系人")
public class CustomerLinkman extends Model<CustomerLinkman> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号的字符长度必须小必须在1至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "姓名不能为空")
    @Length(min = 0, max = 10, message = "姓名的字符长度必须在1至10之间")
    @ApiModelProperty(value = "姓名")
    private String name;
    /**
     * 性别:1男2女
     */
    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @Length(min = 0, max = 20, message = "职位的字符长度必须在0至20之间")
    @ApiModelProperty(value = "职位")
    private String position;

    @Length(min = 0, max = 20, message = "部门的字符长度必须在0至20之间")
    @ApiModelProperty(value = "部门")
    private String department;

    @NotBlank(message = "手机号不能为空")
    @Length(min = 8, max = 15, message = "手机号的字符长度必须在8至15之间")
    @ApiModelProperty(value = "电话")
    private String telephone;

    @Length(min = 0, max = 64, message = "邮箱的字符长度必须在0至64之间")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Length(min = 0, max = 64, message = "联系地址的字符长度必须在0至64之间")
    @ApiModelProperty(value = "联系地址")
    private String address;

    @Length(min = 0, max = 255, message = "描述的字符长度必须在0至255之间")
    @ApiModelProperty(value = "描述")
    private String linkmanDesc;
    /**
     * 是否主联系人
     */
    @ApiModelProperty(value = "是否主联系人")
    private YesNoEnum primaryFlag;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(min = 0, max = 255, message = "备注信息的字符长度必须在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
