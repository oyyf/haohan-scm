package com.haohan.cloud.scm.api.common;

import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.api.common.entity.req.PdsOfferOrderReq;
import com.haohan.cloud.scm.api.common.entity.req.ReqOfferOrders;
import com.haohan.cloud.scm.api.common.params.PdsAdminSumBuyOrder;
import com.haohan.cloud.scm.api.common.req.admin.*;
import com.haohan.cloud.scm.api.common.req.operate.PdsAfterSaleOrderReq;
import com.haohan.cloud.scm.common.tools.http.MethodType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;

/**
 * Created by zgw on 2019/5/22.
 * @author zgw
 */
@Getter
@AllArgsConstructor
public enum AdminApiConstant implements IApiEnum{

    //wiki http://wiki.corp.gh178.com/pages/viewpage.action?pageId=3048710
    admin_bill_buyerPaymentList("获取采购商货款列表", "/f/pds/api/admin/bill/buyerPayment/findList", PdsAdminBuyerPaymentListReq.class, BaseResp.class, MethodType.POST),
    admin_bill_buyerPaymentBatchCreate("批量计算生成采购商货款", "/f/pds/api/admin/bill/buyerPayment/batchCreate", PdsAdminBuyerPaymentBatchCreateReq.class, BaseResp.class, MethodType.POST),
    admin_bill_supplierPaymentList("获取供应商货款列表", "/f/pds/api/admin/bill/supplierPayment/findList", PdsAdminSupplierPaymentListReq.class, BaseResp.class, MethodType.POST),
    admin_bill_supplierPaymentCreate("计算生成供应商货款", "/f/pds/api/admin/bill/supplierPayment/create", PdsAdminSupplierPaymentCreateReq.class, BaseResp.class, MethodType.POST),
    admin_bill_settlementRecordList("获取结算记录列表", "/f/pds/api/admin/bill/settlementRecord/findList", PdsAdminSettlementRecordListReq.class, BaseResp.class, MethodType.POST),
    admin_bill_settlementRecordEdit("新增/修改结算记录", "/f/pds/api/admin/bill/settlementRecord/edit", PdsAdminSettlementRecordEditReq.class, BaseResp.class, MethodType.POST),
    admin_bill_settlementQueryAmount("查询结算金额", "/f/pds/api/admin/bill/settlementRecord/queryAmount", PdsAdminSettlementRecordAmountReq.class, BaseResp.class, MethodType.POST),
    admin_bill_fetchCompanyList("查询结算公司(采购商/供应商_商家)", "/f/pds/api/admin/bill/settlementRecord/fetchCompanyList", PdsAdminSettlementRecordCompanyListReq.class, BaseResp.class, MethodType.POST),

    admin_common_fetchSupplierList("查询供应商列表_带goodsId时查供应价格", "/f/pds/api/admin/common/supplier/list", PdsSupplierListApiReq.class, BaseResp.class, MethodType.POST),
    admin_common_fetchBuyerList("查询采购商列表", "/f/pds/api/admin/common/buyer/list", PdsBuyerApiReq.class, BaseResp.class, MethodType.POST),
    admin_common_driverList("获取司机列表", "/f/pds/api/admin/common/driver/list", PdsBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_common_pmList("获取平台商家列表", "/f/pds/api/admin/common/pmList", null, BaseResp.class, MethodType.POST),
    admin_customer_findList("获取用户列表", "/f/pds/api/admin/customer/findList", PdsAdminCustomerListReq.class, BaseResp.class, MethodType.POST),
    admin_employee_selectList("获取员工列表", "/f/pds/api/admin/employee/list", PdsBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_employee_add("添加员工信息", "/f/pds/api/admin/employee/add", PdsEmployeeApiReq.class, BaseResp.class, MethodType.POST),
    admin_employee_update("修改员工信息", "/f/pds/api/admin/employee/update", PdsEmployeeApiReq.class, BaseResp.class, MethodType.POST),
    admin_employee_delete("删除员工信息", "/f/pds/api/admin/employee/delete", PdsEmployeeApiReq.class, BaseResp.class, MethodType.POST),

    admin_merchantShop_merchantList("获取所属商家列表", "/f/pds/api/admin/merchantShop/merchant/findList", PdsAdminMerchantListReq.class, BaseResp.class, MethodType.POST),
    admin_merchantShop_merchantEdit("所属商家编辑", "/f/pds/api/admin/merchantShop/merchant/edit", PdsAdminMerchantEditReq.class, BaseResp.class, MethodType.POST),
    admin_merchantShop_merchantDelete("所属商家删除", "/f/pds/api/admin/merchantShop/merchant/delete", PdsAdminMerchantDeleteReq.class, BaseResp.class, MethodType.POST),
    admin_merchantShop_shopList("获取所属店铺列表", "/f/pds/api/admin/merchantShop/shop/findList", PdsAdminShopListReq.class, BaseResp.class, MethodType.POST),
    admin_merchantShop_shopEdit("所属店铺编辑", "/f/pds/api/admin/merchantShop/shop/edit", PdsAdminShopEditReq.class, BaseResp.class, MethodType.POST),
    admin_merchantShop_shopDelete("所属店铺删除", "/f/pds/api/admin/merchantShop/shop/delete", PdsAdminShopDeleteReq.class, BaseResp.class, MethodType.POST),

    admin_order_fetchBuyList("获取采购单列表", "/f/pds/api/admin/order/buy/list", PdsBuyOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_fetchBuyDetailList("获取采购明细列表", "/f/pds/api/admin/order/buyDetail/list", PdsBuyOrderDetailApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_delete("删除采购明细", "/f/pds/api/admin/order/buyDetail/delete", PdsBuyOrderDetailApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_editBuyOrder("修改运费", "/f/pds/api/admin/order/editBuyOrder", PdsBuyShipFeeApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_findOfferList("获取报价单列表", "/f/pds/api/admin/order/offer/findPage", PdsOfferOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_shipOrderList("查询送货单列表", "/f/pds/api/admin/order/driver/shipList", PdsShipOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_shipDetailList("获取送货单明细", "/f/pds/api/admin/order/driver/shipDetails", PdsShipOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_findSelfOrderBuyerList("获取自提采购商列表", "/f/pds/api/admin/order/selfOrder/buyerList", PdsSelfOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_findSelfOrderList("获取自提交易单列表", "/f/pds/api/admin/order/selfOrder/orderList", PdsSelfOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_order_submitShipOrder("修改送货单备注", "/f/pds/api/admin/order/selfOrder/shipOrder/edit", HashMap.class, BaseResp.class, MethodType.POST),

    admin_roles_buyerList("获取采购商列表", "/f/pds/api/admin/roles/buyer/findList", PdsAdminBuyerListReq.class, BaseResp.class, MethodType.POST),
    admin_roles_buyerEdit("编辑采购商_新增或修改", "/f/pds/api/admin/roles/buyer/edit", PdsAdminBuyerEditReq.class, BaseResp.class, MethodType.POST),
    admin_roles_buyerDelete("采购商删除", "/f/pds/api/admin/roles/buyer/delete", PdsAdminBuyerDeleteReq.class, BaseResp.class, MethodType.POST),
    admin_roles_employeeList("获取员工列表", "/f/pds/api/admin/roles/employee/findList", PdsAdminEmployeeListReq.class, BaseResp.class, MethodType.POST),
    admin_roles_employeeEdit("编辑员工新增或修改", "/f/pds/api/admin/roles/employee/edit", PdsAdminEmployeeEditReq.class, BaseResp.class, MethodType.POST),
    admin_roles_employeeDelete("员工删除", "/f/pds/api/admin/roles/employee/delete", PdsAdminEmployeeDeleteReq.class, BaseResp.class, MethodType.POST),
    admin_roles_supplierList("获取供应商列表", "/f/pds/api/admin/roles/supplier/findList", PdsAdminSupplierListReq.class, BaseResp.class, MethodType.POST),
    admin_roles_supplierEdit("编辑供应商新增或修改", "/f/pds/api/admin/roles/supplier/edit", PdsAdminSupplierEditReq.class, BaseResp.class, MethodType.POST),
    admin_roles_supplierDelete("供应商删除", "/f/pds/api/admin/roles/supplier/delete", PdsAdminSupplierDeleteReq.class, BaseResp.class, MethodType.POST),

    admin_shortcut_collect("采购单汇总", "/f/pds/api/admin/shortcut/collect", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_shortcut_confirm("平台确认报价-采购商确认报价-交易匹配-运营揽货", "/f/pds/api/admin/shortcut/confirm", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_shortcut_finish("装车-送达", "/f/pds/api/admin/shortcut/finish", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_shortcut_goodsReceived("平台下_统一批次_全收货", "/f/pds/api/admin/shortcut/goodsReceived", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_shortcut_reset("重置数据_至汇总采购单前", "/f/pds/api/admin/shortcut/reset", PdsDataResetApiReq.class, BaseResp.class, MethodType.POST),

    admin_sortout_findList("分拣记录列表", "/f/pds/api/admin/sortout/findPage", PdsBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_sortout_confirm("确定分拣", "/f/pds/api/admin/sortout/confirm", PdsSortOutApiReq.class, BaseResp.class, MethodType.POST),
    admin_sortout_edit("修改分拣数量", "/f/pds/api/admin/sortout/edit", PdsSortOutApiReq.class, BaseResp.class, MethodType.POST),
    admin_sortout_allProcess("分拣进度(整体)", "/f/pds/api/admin/sortout/process/all", PdsDateSeqBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_sortout_buyProcess("分拣进度(按商品种类)", "/f/pds/api/admin/sortout/process/byGoods", HashMap.class, BaseResp.class, MethodType.POST),
    admin_sortout_fastSortOut("一键分拣", "/f/pds/api/admin/sortout/fastSortOut", PdsDataResetApiReq.class, BaseResp.class, MethodType.POST),

    admin_summary_fetchSummaryOrderList("查询汇总单列表", "/f/pds/api/admin/summary/findPage", PdsAdminSumBuyOrder.class, BaseResp.class, MethodType.POST),
    admin_summary_editBuyOrderDetail("修改采购单明细", "/f/pds/api/admin/summary/buy/edit", PdsApiSumBuyDetailBatchReq.class, BaseResp.class, MethodType.POST),
    admin_summary_saveOfferOrder("新增或修改报价单", "/f/pds/api/admin/summary/offer/save", PdsApiSumOfferSaveBatchReq.class, BaseResp.class, MethodType.POST),
    admin_summary_editSumOrder("修改汇总单", "/f/pds/api/admin/summary/summaryOrder/edit", PdsApiSumOrderBatchReq.class, BaseResp.class, MethodType.POST),

    admin_summary_deleteOfferOrder("删除报价单", "/f/pds/api/admin/summary/offer/delete", PdsOfferOrderApiReq.class, BaseResp.class, MethodType.POST),
    admin_summary_convert("自营平台汇总单转采购单", "/f/pds/api/admin/summary/convert", PdsDataResetApiReq.class, BaseResp.class, MethodType.POST),

    admin_truck_list("车辆列表", "/f/pds/api/admin/truck/list", PdsBaseApiReq.class, BaseResp.class, MethodType.POST),
    admin_truck_addTruck("新增车辆", "/f/pds/api/admin/truck/add", PdsTruckApiReq.class, BaseResp.class, MethodType.POST),
    admin_truck_editTruck("编辑车辆", "/f/pds/api/admin/truck/edit", PdsTruckApiReq.class, BaseResp.class, MethodType.POST),
    admin_truck_delTruck("删除车辆", "/f/pds/api/admin/truck/delete", PdsTruckApiReq.class, BaseResp.class, MethodType.POST),

    admin_preSummaryOrder("预汇总", "/f/pds/api/admin/preSummaryOrder", PdsDateSeqApiReq.class, BaseResp.class, MethodType.POST),

    admin_statical_overview("数据分析-数据概览", "/f/pds/api/admin/statistical/overview", PdsApiStatisOverViewReq.class, BaseResp.class, MethodType.POST),
    admin_statical_statisCurve("销售额曲线", "/f/pds/api/admin/statistical/statisCurve", PdsApiStatisCurveReq.class, BaseResp.class, MethodType.POST),
    admin_statical_categoryPercent("分类占比", "/f/pds/api/admin/statistical/categoryPercent", PdsApiStatisCurveReq.class, BaseResp.class, MethodType.POST),
    admin_statical_goodsTopN("单品销量排行", "/f/pds/api/admin/statistical/goodsTopN", PdsApiStatisSaleTopReq.class, BaseResp.class, MethodType.POST),
    admin_statical_orderDeal("待处理订单", "/f/pds/api/admin/statistical/orderDeal", PdsApiStatisOrderDealReq.class, BaseResp.class, MethodType.POST),
    admin_statical_orderAnalyze("采购订单数据分析", "/f/pds/api/admin/statistical/orderAnalyze", PdsApiOrderAnalyzeReq.class, BaseResp.class, MethodType.POST),
    admin_statical_buyerSaleTop("商户销量排行", "/f/pds/api/admin/statistical/buyerSaleTop", PdsApiOrderAnalyzeReq.class, BaseResp.class, MethodType.POST),
    admin_statical_saleTrend("销售趋势", "/f/pds/api/admin/statistical/saleTrend", PdsApiOrderAnalyzeReq.class, BaseResp.class, MethodType.POST),
    admin_statical_briefReport("运营简报", "/f/pds/api/admin/statistical/briefReport", PdsApiOrderAnalyzeReq.class, BaseResp.class, MethodType.POST),
    admin_statical_rangeAmount("查询_时间段内_按采购商所属商家查看每日货款金额", "/f/pds/api/admin/statistical/rangeAmount", PdsRangeAmountReq.class, BaseResp.class, MethodType.POST),

    operating_fetchSupplierList("获取已成交的供应商列表", "/f/pds/api/operating/freight/supList", PdsOfferOrderReq.class, BaseResp.class, MethodType.POST),
    operating_waitDeal("查询待揽货数量", "/f/pds/api/operating/freight/waitDealNum", PdsOfferOrderReq.class, BaseResp.class, MethodType.POST),
    operating_fetchGoodsList("供应商供货商品列表", "/f/pds/api/operating/freight/goodsList", PdsOfferOrderReq.class, BaseResp.class, MethodType.POST),
    operating_freightConfirm("揽货确认", "/f/pds/api/operating/freight/confirm", HashMap.class, BaseResp.class, MethodType.POST),
    operating_afterSale("售后", "/f/pds/api/operating/afterSale", PdsAfterSaleOrderReq.class, BaseResp.class, MethodType.POST),
    operating_enterStockApply("入库", "/f/pds/api/operating/enterStock", HashMap.class, BaseResp.class, MethodType.POST),

    operating_platform_updateBuyOrderStatus("采购单状态变更为待确认", "/f/pds/api/process/buyOrder/statusWait", HashMap.class, BaseResp.class, MethodType.POST),
    operating_platform_goodsArrived("货物送达", "/f/pds/api/process/tradeOrder/goodsArrived", HashMap.class, BaseResp.class, MethodType.POST),

    supplier_supplierOrder_queryWaitOfferList("查询待报价商品列表(采购单汇总)", "/f/pds/api/supplier/order/waitOffer/fetchList", ReqOfferOrders.class, BaseResp.class, MethodType.POST),
    supplier_supplierOrder_supplierOffer("供应商_商品报价(报价单)", "/f/pds/api/supplier/order/supplierOffer", ReqOfferOrders.class, BaseResp.class, MethodType.POST),
    supplier_supplierOrder_queryOfferOrderList("供应商报价单_列表查询", "/f/pds/api/supplier/order/offerOrder/fetchList", ReqOfferOrders.class, BaseResp.class, MethodType.POST);
    private String desc;
    private String url;
    private Class reqClass;
    private Class respClass;
    private MethodType methodType;
}
