package com.haohan.cloud.scm.api.crm.vo;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/10/23
 */
@Data
@NoArgsConstructor
@ApiModel("客户信息返回")
public class CustomerVO {

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "客户地址定位 (经度，纬度)")
    private String position;

    @ApiModelProperty(value = "是否标注定位")
    private Boolean positionFlag;

    @ApiModelProperty(value = "客户标签")
    private String tags;

    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    @ApiModelProperty(value = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;

    @ApiModelProperty(value = "客户状态", notes = "客户状态:0.未启用 1.启用 2.待审核")
    private UseStatusEnum status;

    @ApiModelProperty(value = "公司性质", notes = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;

    @ApiModelProperty(value = "客户联系人名称")
    private String contact;

    @ApiModelProperty(value = "联系手机")
    private String telephone;

    @ApiModelProperty(value = "座机电话")
    private String phoneNumber;

    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    /**
     * 需联查
     */
    @ApiModelProperty(value = "客户负责人名称")
    private String directorName;

    @ApiModelProperty("距离")
    private BigDecimal distance;

    // web端 使用属性

    @ApiModelProperty(value = "销售区域")
    private String areaSn;

    /**
     * 需联查
     */
    @ApiModelProperty(value = "销售区域名称")
    private String areaName;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    /**
     * 需联查
     */
    @ApiModelProperty(value = "市场名称")
    private String marketName;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    /**
     * 使用属性
     *
     * @param customer
     */
    public CustomerVO(Customer customer) {
        this.customerSn = customer.getCustomerSn();
        this.customerName = customer.getCustomerName();
        this.address = customer.getAddress();
        this.position = customer.getPosition();
        // 是否标注定位
        this.positionFlag = null != customer.getPosition();
        this.tags = customer.getTags();
        this.customerType = customer.getCustomerType();
        this.customerLevel = customer.getCustomerLevel();
        this.status = customer.getStatus();
        this.companyNature = customer.getCompanyNature();
        this.contact = customer.getContact();
        this.telephone = customer.getTelephone();
        this.phoneNumber = customer.getPhoneNumber();
        this.directorId = customer.getDirectorId();
        this.areaSn = customer.getAreaSn();
        this.marketSn = customer.getMarketSn();
        this.createDate = customer.getCreateDate();
    }


}
