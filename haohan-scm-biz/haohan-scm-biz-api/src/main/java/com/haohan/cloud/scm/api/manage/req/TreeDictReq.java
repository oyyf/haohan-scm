/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.TreeDict;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 树形字典
 *
 * @author haohan
 * @date 2019-05-28 20:38:58
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "树形字典")
public class TreeDictReq extends TreeDict {

    private long pageSize;
    private long pageNo;




}
