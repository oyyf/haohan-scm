package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.OpStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class OpStatusEnumConverterUtil implements Converter<OpStatusEnum> {
    @Override
    public OpStatusEnum convert(Object o, OpStatusEnum opStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return OpStatusEnum.getByType(o.toString());
    }
}
