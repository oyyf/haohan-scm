package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.MarketEmployeeImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class MarketEmployeeImportReq {

    @NotEmpty(message = "员工列表不能为空")
    private List<MarketEmployeeImport> employeeList;

}
