package com.haohan.cloud.scm.api.crm.vo.app;

import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.common.tools.util.PinYin4jUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2019/10/22
 */
@Data
@NoArgsConstructor
@Api("员工通讯录信息")
public class EmployeeVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String namePy;
    private String avatar;
    private String mobile;
    private String department;
    @ApiModelProperty(value = "市场部员工类型")
    private String type;

    public EmployeeVO(MarketEmployee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.avatar = employee.getAvatar();
        this.mobile = employee.getTelephone();
        this.department = employee.getDeptName();
        // 拼音
        if (null != this.name) {
            String py = PinYin4jUtils.fetchPinYin(this.name.charAt(0));
            if (null != py) {
                this.namePy = py;
            }
        }
        if (null == employee.getEmployeeType()) {
            this.type = "未设置";
        } else {
            this.type = employee.getEmployeeType().getDesc();
        }
    }
}
