package com.haohan.cloud.scm.api.crm.req.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/5/29
 * 客户联系人创建采购商用于登录小程序
 */
@Data
public class CustomerBuyerReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 32, message = "员工id的长度最大32字符")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号的字符长度必须小必须在1至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "客户联系人id不能为空")
    @Length(max = 32, message = "客户联系人id的长度最大32字符")
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;


}
