package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("日报数分析(部门)")
public class WorkReportCountVO {

    @ApiModelProperty(value = "员工数")
    private Integer employeeNum;

    @ApiModelProperty(value = "日报已交数")
    private Integer finishNum;

    @ApiModelProperty(value = "日报未交数")
    private Integer waitNum;

    @ApiModelProperty(value = "日报不统计数")
    private Integer otherNum;

}
