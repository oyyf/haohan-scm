package com.haohan.cloud.scm.api.wms.trans;

import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.EnterTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ShelfOperateEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.wms.entity.*;
import com.haohan.cloud.scm.api.wms.req.CreateEnterWarehouseDetailReq;
import com.haohan.cloud.scm.api.wms.req.CreateEnterWarehouseReq;
import com.haohan.cloud.scm.api.wms.req.ProductPutShelfReq;
import com.haohan.cloud.scm.api.wms.resp.EnterWarehouseDetailResp;

import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/6/13
 */
public class EnterWarehouseDetailTrans {
    /**
     * 将采购单明细的信息存入 入库单明细中
     * @param purchaseOrderDetail
     * @return
     */
    public static EnterWarehouseDetail trans(PurchaseOrderDetail purchaseOrderDetail){
        EnterWarehouseDetail enterWarehouseDetail = new EnterWarehouseDetail();
        enterWarehouseDetail.setPmId(purchaseOrderDetail.getPmId());
        enterWarehouseDetail.setSummaryOrderId(purchaseOrderDetail.getSummaryDetailSn());
        enterWarehouseDetail.setPurchaseDetailSn(purchaseOrderDetail.getPurchaseDetailSn());
        enterWarehouseDetail.setGoodsModelId(purchaseOrderDetail.getGoodsModelId());
        enterWarehouseDetail.setUnit(purchaseOrderDetail.getUnit());
        enterWarehouseDetail.setEnterStatus(EnterStatusEnum.handle);
        enterWarehouseDetail.setStorageType(StorageTypeEnum.enterwarehouse);
        enterWarehouseDetail.setApplyTime(LocalDateTime.now());
        return enterWarehouseDetail;
    }


    /**
     * 将货品信息存入 入库单明细中
     * @param productInfo
     * @return
     */
    public static EnterWarehouseDetail productInfoTrans(ProductInfo productInfo){
        EnterWarehouseDetail enterWarehouseDetail = new EnterWarehouseDetail();
        enterWarehouseDetail.setProductNumber(productInfo.getProductNumber());
        enterWarehouseDetail.setGoodsModelId(productInfo.getGoodsModelId());
        enterWarehouseDetail.setProductSn(productInfo.getProductSn());
        enterWarehouseDetail.setUnit(productInfo.getUnit());
        enterWarehouseDetail.setPmId(productInfo.getPmId());
        enterWarehouseDetail.setApplyTime(LocalDateTime.now());
        enterWarehouseDetail.setEnterStatus(EnterStatusEnum.handle);
        enterWarehouseDetail.setStorageType(StorageTypeEnum.enterwarehouse);
        return enterWarehouseDetail;
    }

    /**
     * 设置货品上架记录
     * @param productInfo
     * @param req
     * @return
     */
    public static ShelfManagement putawayTrans(ProductInfo productInfo, ProductPutShelfReq req){
        ShelfManagement shelfManagement = new ShelfManagement();
        shelfManagement.setPmId(req.getPmId());
        shelfManagement.setCellSn(req.getCellSn());
        shelfManagement.setPalletSn(req.getPalletSn());
        shelfManagement.setProductSn(req.getProductSn());
        shelfManagement.setProductName(productInfo.getProductName());
        shelfManagement.setUnit(productInfo.getUnit());
        shelfManagement.setProductNumber(productInfo.getProductNumber());
        shelfManagement.setOperateTime(LocalDateTime.now());
        shelfManagement.setShelfOperate(ShelfOperateEnum.on);
        return shelfManagement;
    }

    /**
     * 设置货品下架记录
     * @param productInfo
     * @param cell
     * @param pallet
     * @return
     */
    public static ShelfManagement removeShelfTrans(ProductInfo productInfo, Cell cell, Pallet pallet){
        ShelfManagement shelfManagement = new ShelfManagement();
        shelfManagement.setPmId(productInfo.getPmId());
        shelfManagement.setShelfOperate(ShelfOperateEnum.off);
        shelfManagement.setWarehouseSn(cell.getWarehouseSn());
        shelfManagement.setShelfSn(cell.getShelfSn());
        shelfManagement.setCellSn(cell.getCellSn());
        shelfManagement.setPalletSn(pallet.getPalletSn());
        shelfManagement.setProductSn(productInfo.getProductSn());
        shelfManagement.setProductName(productInfo.getProductName());
        shelfManagement.setUnit(productInfo.getUnit());
        shelfManagement.setOperateTime(LocalDateTime.now());
        return shelfManagement;
    }

    /**
     * 入库单信息
     * @param req
     * @return
     */
    public static EnterWarehouse enterWarehouseTrans(CreateEnterWarehouseReq req){
        EnterWarehouse enterWarehouse = new EnterWarehouse();
        enterWarehouse.setPmId(req.getPmId());
        enterWarehouse.setBatchNumber(req.getBatchNumber());
        enterWarehouse.setWarehouseSn(req.getWarehouseSn());
        enterWarehouse.setPurchaseSn(req.getPurchaseSn());
        enterWarehouse.setApplyTime(LocalDateTime.now());
        enterWarehouse.setEnterStatus(EnterStatusEnum.handle);
        enterWarehouse.setEnterType(EnterTypeEnum.machineEnter);
        enterWarehouse.setStorageType(StorageTypeEnum.enterwarehouse);
        return enterWarehouse;
    }

    public static EnterWarehouseDetail enterWarehouseDetailTrans(PurchaseOrderDetail orderDetail, ProductInfo productInfo){
        EnterWarehouseDetail detail = new EnterWarehouseDetail();
        detail.setPmId(orderDetail.getPmId());
        detail.setSummaryOrderId(orderDetail.getSummaryDetailSn());
        detail.setPurchaseDetailSn(orderDetail.getPurchaseDetailSn());
        detail.setGoodsModelId(orderDetail.getGoodsModelId());
        detail.setProductSn(productInfo.getProductSn());
        detail.setProductNumber(productInfo.getProductNumber());
        detail.setUnit(productInfo.getUnit());
        detail.setApplyTime(LocalDateTime.now());
        detail.setEnterStatus(EnterStatusEnum.handle);
        detail.setStorageType(StorageTypeEnum.enterwarehouse);
        return detail;
    }

    public static EnterWarehouseDetailResp respTrans(EnterWarehouseDetail detail,ProductInfo info){
        EnterWarehouseDetailResp resp = new EnterWarehouseDetailResp();
        resp.setProductNumber(detail.getProductNumber());
        resp.setUnit(detail.getUnit());
        resp.setProductName(info.getProductName());
        resp.setPurchasePrice(info.getPurchasePrice());
        resp.setGoodsModelId(info.getGoodsModelId());
        return resp;
    }

    public static EnterWarehouse createEnterWarehouseTrans(CreateEnterWarehouseDetailReq req){
        EnterWarehouse enterWarehouse = new EnterWarehouse();
        enterWarehouse.setPmId(req.getPmId());
        enterWarehouse.setApplyTime(LocalDateTime.now());
        enterWarehouse.setWarehouseSn(req.getWarehouseSn());
        enterWarehouse.setEnterStatus(EnterStatusEnum.handle);
        enterWarehouse.setEnterType(req.getEnterType());
        return enterWarehouse;
    }
    public static EnterWarehouseDetail createEnterWarehouseDetailTrans(String warehouseSn, GoodsModelDTO dto){
        EnterWarehouseDetail detail = new EnterWarehouseDetail();
        detail.setWarehouseSn(warehouseSn);
        detail.setGoodsModelId(dto.getId());
        detail.setUnit(dto.getModelUnit());
        detail.setApplyTime(LocalDateTime.now());
        detail.setEnterStatus(EnterStatusEnum.handle);
        detail.setStorageType(StorageTypeEnum.enterwarehouse);
        return detail;
    }


}
