package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/1/3
 */
@Data
@NoArgsConstructor
public class CustomerVisitVO {

    @ApiModelProperty(value = "主键")
    private String visitId;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 拜访地址
     */
    @ApiModelProperty(value = "拜访地址")
    private String visitAddress;
    /**
     * 拜访联系人id
     */
    @ApiModelProperty(value = "拜访联系人id")
    private String linkmanId;
    /**
     * 拜访联系人名称
     */
    @ApiModelProperty(value = "拜访联系人名称")
    private String linkmanName;
    /**
     * 拜访日期
     */
    @ApiModelProperty(value = "拜访日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate visitDate;
    /**
     * 拜访内容
     */
    @ApiModelProperty(value = "拜访内容")
    private String visitContent;
    /**
     * 进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来
     */
    @ApiModelProperty(value = "进展阶段:1初次拜访2了解交流3.深入跟进4达成合作5商务往来")
    private VisitStepEnum visitStep;
    /**
     * 拜访图片组编号
     */
    @ApiModelProperty(value = "拜访图片组编号")
    private String photoNum;
    /**
     * 拜访员工id
     */
    @ApiModelProperty(value = "拜访员工id")
    private String employeeId;
    /**
     * 拜访员工名称
     */
    @ApiModelProperty(value = "拜访员工名称")
    private String employeeName;
    /**
     * 下次回访时间
     */
    @ApiModelProperty(value = "下次回访时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime nextVisitTime;
    /**
     * 客户状态:0.无1潜在2.有意向3成交4失败
     */
    @ApiModelProperty(value = "客户状态:0.无1潜在2.有意向3成交4失败")
    private CustomerStatusEnum customerStatus;

    @ApiModelProperty(value = "拜访状态: 1.待拜访 2.拜访中 3.已拜访 4.未完成")
    private VisitStatusEnum visitStatus;
    /**
     * 拜访总结
     */
    @ApiModelProperty(value = "拜访总结")
    private String summaryContent;
    /**
     * 抵达时间
     */
    @ApiModelProperty(value = "抵达时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime arrivalTime;
    /**
     * 离开时间
     */
    @ApiModelProperty(value = "离开时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime departureTime;
    /**
     * 抵达地址定位
     */
    @ApiModelProperty(value = "抵达地址定位")
    private String arrivalPosition;

    @ApiModelProperty(value = "抵达地址")
    private String arrivalAddress;
    /**
     * 离开地址定位
     */
    @ApiModelProperty(value = "离开地址定位")
    private String departurePosition;

    @ApiModelProperty(value = "离开地址")
    private String departureAddress;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 拜访图片列表
     */
    @ApiModelProperty(value = "拜访图片列表")
    private List<PhotoGallery> photoList;

    // 客户信息扩展

    @ApiModelProperty(value = "客户类型", notes = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;

    @ApiModelProperty(value = "销售区域编码")
    private String areaSn;

    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    public CustomerVisitVO(CustomerVisit visit) {
        BeanUtil.copyProperties(visit, this);
        this.visitId = visit.getId();
    }
}
