package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseEmployeeSupplierReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;
    @NotBlank(message = "employeeId不能为空")
    private String employeeId;
    private long size = 10;
    private long current = 1;

}
