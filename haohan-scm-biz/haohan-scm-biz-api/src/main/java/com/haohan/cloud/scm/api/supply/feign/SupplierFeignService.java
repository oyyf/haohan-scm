/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 供应商内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierFeignService", value = ScmServiceName.SCM_BIZ_SUPPLY)
public interface SupplierFeignService {


    /**
     * 通过id查询供应商
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Supplier/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 供应商 列表信息
     * @param supplierReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Supplier/fetchSupplierPage")
    R getSupplierPage(@RequestBody SupplierReq supplierReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 供应商 列表信息
     * @param supplierReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Supplier/fetchSupplierList")
    R getSupplierList(@RequestBody SupplierReq supplierReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增供应商
     * @param supplier 供应商
     * @return R
     */
    @PostMapping("/api/feign/Supplier/add")
    R save(@RequestBody Supplier supplier, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改供应商
     * @param supplier 供应商
     * @return R
     */
    @PostMapping("/api/feign/Supplier/update")
    R updateById(@RequestBody Supplier supplier, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除供应商
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Supplier/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Supplier/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplierReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Supplier/countBySupplierReq")
    R<Integer> countBySupplierReq(@RequestBody SupplierReq supplierReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Supplier/getOneBySupplierReq")
    R getOneBySupplierReq(@RequestBody SupplierReq supplierReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 采购员新增供应商
     * @param supplier 供应商  必须：supplierName/telephone
     * @return R
     */
    @PostMapping("/api/feign/Supplier/addBuyerSupplier")
    R addBuyerSupplier(@RequestBody Supplier supplier, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 供应商商家名称修改
     * @param req 供应商  必须：merchantId/merchantName
     * @return R
     */
    @PostMapping("/api/feign/Supplier/updateMerchantName")
    R<Boolean> updateMerchantName(@RequestBody SupplierReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
