/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.WechatMessage;
import com.haohan.cloud.scm.api.salec.req.WechatMessageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 用户行为记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WechatMessageFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface WechatMessageFeignService {


    /**
     * 通过id查询用户行为记录表
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WechatMessage/{id}", method = RequestMethod.GET)
    R<WechatMessage> getById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 用户行为记录表 列表信息
     *
     * @param wechatMessageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessage/fetchWechatMessagePage")
    R<Page<WechatMessage>> getWechatMessagePage(@RequestBody WechatMessageReq wechatMessageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 用户行为记录表 列表信息
     *
     * @param wechatMessageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WechatMessage/fetchWechatMessageList")
    R<List<WechatMessage>> getWechatMessageList(@RequestBody WechatMessageReq wechatMessageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增用户行为记录表
     *
     * @param wechatMessage 用户行为记录表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/add")
    R<Boolean> save(@RequestBody WechatMessage wechatMessage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改用户行为记录表
     *
     * @param wechatMessage 用户行为记录表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/update")
    R<Boolean> updateById(@RequestBody WechatMessage wechatMessage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除用户行为记录表
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/listByIds")
    R<List<WechatMessage>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param wechatMessageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/countByWechatMessageReq")
    R<Integer> countByWechatMessageReq(@RequestBody WechatMessageReq wechatMessageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param wechatMessageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/getOneByWechatMessageReq")
    R<WechatMessage> getOneByWechatMessageReq(@RequestBody WechatMessageReq wechatMessageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param wechatMessageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WechatMessage/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<WechatMessage> wechatMessageList, @RequestHeader(SecurityConstants.FROM) String from);


}
