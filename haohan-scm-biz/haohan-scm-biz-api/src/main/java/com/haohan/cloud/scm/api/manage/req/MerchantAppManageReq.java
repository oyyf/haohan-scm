/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.MerchantAppManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商家应用管理
 *
 * @author haohan
 * @date 2019-05-28 20:35:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商家应用管理")
public class MerchantAppManageReq extends MerchantAppManage {

    private long pageSize;
    private long pageNo;




}
