/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 出库单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ExitWarehouseFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface ExitWarehouseFeignService {


    /**
     * 通过id查询出库单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ExitWarehouse/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 出库单 列表信息
     * @param exitWarehouseReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ExitWarehouse/fetchExitWarehousePage")
    R getExitWarehousePage(@RequestBody ExitWarehouseReq exitWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 出库单 列表信息
     * @param exitWarehouseReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ExitWarehouse/fetchExitWarehouseList")
    R getExitWarehouseList(@RequestBody ExitWarehouseReq exitWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/add")
    R save(@RequestBody ExitWarehouse exitWarehouse, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/update")
    R updateById(@RequestBody ExitWarehouse exitWarehouse, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除出库单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ExitWarehouse/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/countByExitWarehouseReq")
    R countByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/getOneByExitWarehouseReq")
    R getOneByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param exitWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ExitWarehouse/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ExitWarehouse> exitWarehouseList, @RequestHeader(SecurityConstants.FROM) String from);


}
