package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/6/19
 */
@Data
@ApiModel("采购部新增员工")
public class PurchaseAddEmployeeReq {

    /**
     * 平台商家id
     */
    @ApiModelProperty(value = "平台商家id", required = true)
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 名称
     */
    @ApiModelProperty(value = "采购员工名称", required = true)
    @NotBlank(message = "name不能为空")
    private String name;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "采购员工手机号", required = true)
    @NotBlank(message = "telephone不能为空")
    private String telephone;
    /**
     * 采购员工类型:1采购总监2采购经理3采购员
     */
    @ApiModelProperty(value = "采购员工类型:1采购总监2采购经理3采购员", required = true)
    @NotNull(message = "purchaseEmployeeType不能为空")
    private String purchaseEmployeeType;

    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;
    /**
     * 通行证ID
     */
    private String passportId;
    /**
     * 用户id
     */
    private String userId;

}
