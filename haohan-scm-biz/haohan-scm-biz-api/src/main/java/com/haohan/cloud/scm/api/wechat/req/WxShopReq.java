package com.haohan.cloud.scm.api.wechat.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/20
 */
@Data
public class WxShopReq {

    @NotBlank(message = "uid不能为空")
    @Length(max = 64, message = "通行证id长度最大64字符")
    private String uid;


}
