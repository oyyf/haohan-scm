/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;



/**
 * 仓库库存
 *
 * @author haohan
 * @date 2019-08-22 16:12:12
 */
@Data
@TableName("scm_pss_warehouse_stock")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "仓库库存")
public class WarehouseStock extends Model<WarehouseStock> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * 商家ID
     */
    @ApiModelProperty(value = "商家ID")
    private String merchantId;
    /**
     * 仓库编号
     */
    @ApiModelProperty(value = "仓库编号")
    private String warehouseId;
    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码")
    private String barCode;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码")
    private String goodsCode;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String unit;
    /**
     * 规格
     */
    @ApiModelProperty(value = "规格")
    private String attr;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal stockNum;
    /**
     * 单价
     */
    @ApiModelProperty(value = "单价")
    private BigDecimal price;
    /**
     * 金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;
    /**
     * 供应商ID
     */
    @ApiModelProperty(value = "供应商ID")
    private String supplierId;
    /**
     * 入库时间
     */
    @ApiModelProperty(value = "入库时间")
    private LocalDateTime instockTime;
    /**
     * 上次盘点时间
     */
    @ApiModelProperty(value = "上次盘点时间")
    private LocalDateTime lastInventoryTime;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
