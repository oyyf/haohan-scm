package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.crm.WorkReportTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import com.haohan.cloud.scm.api.crm.vo.analysis.EmployeeDailyCountVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/12
 * 验证：新增FirstGroup   修改SecondGroup
 */
@Data
@Api("编辑工作日报")
public class EditWorkReportReq {

    @NotBlank(message = "工作日报id不能为空", groups = {SecondGroup.class})
    private String id;

    @NotNull(message = "工作汇报类型不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "工作汇报类型: 1.日常 2.销量 3.库存")
    private WorkReportTypeEnum reportType;

    @NotBlank(message = "汇报内容不能为空", groups = {FirstGroup.class})
    @Length(max = 5000, message = "汇报内容的长度最大5000字符")
    @ApiModelProperty(value = "汇报内容")
    private String reportContent;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "汇报位置定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "汇报位置")
    private String reportLocation;

    @Length(max = 64, message = "汇报地址的长度最大64字符")
    @ApiModelProperty(value = "汇报地址")
    private String reportAddress;

    @NotBlank(message = "汇报人id不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "汇报人id")
    private String reportManId;

    @NotNull(message = "汇报状态不能为空", groups = {SecondGroup.class})
    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private ReportStatusEnum status;

    @Length(max = 255, message = "反馈信息的长度最大255字符", groups = {SecondGroup.class})
    @ApiModelProperty(value = "反馈信息")
    private String feedback;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    @ApiModelProperty(value = "员工日常统计数")
    private EmployeeDailyCountVO modifyDailyCount;

    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "app员工id")
    private String employeeId;

    public WorkReport fetchAdd() {
        WorkReport workReport = new WorkReport();
        workReport.setReportType(this.reportType);
        workReport.setReportContent(this.reportContent);
        workReport.setReportLocation(this.reportLocation);
        workReport.setReportAddress(this.reportAddress);
        workReport.setReportManId(this.reportManId);
        // 初始值
        workReport.setStatus(ReportStatusEnum.already);
        workReport.setReportTime(LocalDateTime.now());
        return workReport;
    }

    public WorkReport fetchUpdate() {
        WorkReport workReport = new WorkReport();
        workReport.setId(this.id);
        workReport.setReportType(this.reportType);
        workReport.setReportContent(this.reportContent);
        workReport.setReportLocation(this.reportLocation);
        workReport.setReportAddress(this.reportAddress);
        workReport.setStatus(this.status);
        workReport.setFeedback(this.feedback);
        return workReport;
    }


}
