package com.haohan.cloud.scm.api.crm.vo.app;

import lombok.Data;

/**
 * @author dy
 * @date 2019/10/28
 */
@Data
public class VisitAnalysisVO {

    /**
     * 统计月  yyyy-MM
     */
    private String month;
    /**
     * 次数
     */
    private Integer num;


}
