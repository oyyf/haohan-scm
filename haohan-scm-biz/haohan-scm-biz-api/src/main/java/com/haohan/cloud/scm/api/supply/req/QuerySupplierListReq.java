package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/20
 */
@Data
@ApiModel(description = "查询供应商列表")
public class QuerySupplierListReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" , required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "通行证id" , required = true)
    private String uid;

    @ApiModelProperty(value = "每页显示条数")
    private long size = 10;

    @ApiModelProperty(value = "当前页")
    private long current = 1;


}
