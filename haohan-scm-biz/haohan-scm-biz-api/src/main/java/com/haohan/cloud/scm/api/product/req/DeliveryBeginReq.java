package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/8/11
 */
@Data
@ApiModel(description = "开始配送")
public class DeliveryBeginReq {
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "shipId不能为空")
    @ApiModelProperty(value = "送货单编号", required = true)
    private String shipId;


}
