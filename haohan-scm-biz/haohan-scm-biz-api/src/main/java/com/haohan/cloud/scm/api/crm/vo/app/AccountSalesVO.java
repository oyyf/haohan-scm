package com.haohan.cloud.scm.api.crm.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
@NoArgsConstructor
public class AccountSalesVO {

    @ApiModelProperty(value = "销售金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "退货金额")
    private BigDecimal returnGoods;

    @ApiModelProperty(value = "订货金额")
    private BigDecimal advanceAmount;

    @ApiModelProperty(value = "还货金额 （退款金额）")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "已收金额")
    private BigDecimal receivableAmount;

    @ApiModelProperty(value = "待收金额")
    private BigDecimal debtAmount;

    public AccountSalesVO(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getDebtAmount() {
        // 待收金额
        if (null == this.orderAmount || null == this.receivableAmount) {
            return BigDecimal.ZERO;
        } else {
            this.debtAmount = this.orderAmount.subtract(this.receivableAmount);
        }
        return this.debtAmount;
    }

}
