package com.haohan.cloud.scm.api.supply.req.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author dy
 * @date 2020/4/29
 */
@Data
public class SupplyOrderConfirmReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "供应订单编号不能为空")
    @Length(max = 32, message = "供应订单编号的长度最大为32字符")
    private String supplySn;

    @ApiModelProperty(value = "备注信息")
    @Length(max = 125, message = "备注信息的最大长度125字符")
    private String remarks;

}
