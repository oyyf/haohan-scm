package com.haohan.cloud.scm.api.common.req.admin;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author shenyu
 * @create 2018/12/17
 */
public class PdsDataResetApiReq extends PdsBaseApiReq {
    @NotNull(message = "missing param deliveryDate")
    private Date deliveryDate;
    @NotBlank(message = "missing param buySeq")
    private String buySeq;

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getBuySeq() {
        return buySeq;
    }

    public void setBuySeq(String buySeq) {
        this.buySeq = buySeq;
    }
}
