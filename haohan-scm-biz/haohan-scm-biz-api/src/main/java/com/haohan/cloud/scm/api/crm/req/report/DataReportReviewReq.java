package com.haohan.cloud.scm.api.crm.req.report;

import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/12/17
 */
@Data
@ApiModel("数据上报确认")
public class DataReportReviewReq {

    @NotBlank(message = "员工id不能为空", groups = {SingleGroup.class})
    @Length(min = 0, max = 32, message = "员工id长度在0至32之间")
    private String employeeId;

    @NotBlank(message = "上报编号不能为空")
    @Length(min = 0, max = 32, message = "上报编号长度在0至32之间")
    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @ApiModelProperty(value = "上报类型 0.库存 1.销售记录 2.竞品")
    private DataReportEnum reportType;

    @ApiModelProperty(value = "确认标识, true为确认, false为不通过")
    private Boolean confirmFlag;

    @Length(max = 64, message = "备注信息最大长度64字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

}
