/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 消息模板明细
 *
 * @author haohan
 * @date 2019-05-13 18:34:15
 */
@Data
@TableName("scm_ms_wechat_message_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "消息模板明细")
public class WechatMessageDetail extends Model<WechatMessageDetail> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private String id;
    /**
   * 消息ID
   */
    private String msgTemplateId;
    /**
   * 字段名称
   */
    private String fieldName;
    /**
   * 字段值
   */
    private String fieldValue;
    /**
   * 字段code
   */
    private String fileldCode;
    /**
   * 字段颜色
   */
    private String fieldColor;
    /**
   * 是否加粗
   */
    private String isBold;
    /**
   * 排序
   */
    private String sort;
    /**
   * 创建者
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    private String updateBy;
    /**
   * 更新时间
   */
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;
  
}
