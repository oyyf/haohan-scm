package com.haohan.cloud.scm.api.wms.trans;

import com.haohan.cloud.scm.api.constant.enums.product.ExitStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.ExitTypeEnum;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.api.wms.req.AddExitWarehouseReq;

import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/16
 */
public class ExitWarehouseTrans {
    public static ExitWarehouse trans(AddExitWarehouseReq req){
        ExitWarehouse exitWarehouse = new ExitWarehouse();
        exitWarehouse.setPmId(req.getPmId());
        exitWarehouse.setApplyTime(LocalDateTime.now());
        exitWarehouse.setWarehouseSn(req.getWarehouseSn());
        exitWarehouse.setExitStatus(ExitStatusEnum.wait);
        exitWarehouse.setExitType(ExitTypeEnum.delivery);
        return exitWarehouse;
    }
}
