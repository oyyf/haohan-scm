package com.haohan.cloud.scm.api.crm.vo;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import com.haohan.cloud.scm.api.crm.entity.CustomerLinkman;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
public class LinkManVO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "联系地址")
    private String address;

    @ApiModelProperty(value = "电话")
    private String telephone;

    @ApiModelProperty(value = "是否主联系人")
    private YesNoEnum primaryFlag;

    public LinkManVO(CustomerLinkman linkman) {
        if (null != linkman) {
            this.id = linkman.getId();
            this.customerSn = linkman.getCustomerSn();
            this.name = linkman.getName();
            this.address = linkman.getAddress();
            this.telephone = linkman.getTelephone();
            this.primaryFlag = linkman.getPrimaryFlag();
        }
    }

    public LinkManVO(Customer customer) {
        if (null != customer) {
            this.customerSn = customer.getCustomerSn();
            this.name = customer.getContact();
            this.address = customer.getAddress();
            this.telephone = customer.getTelephone();
            this.primaryFlag = YesNoEnum.yes;
        }
    }
}
