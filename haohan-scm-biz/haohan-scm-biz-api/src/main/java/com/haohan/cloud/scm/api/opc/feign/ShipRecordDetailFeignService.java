/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import com.haohan.cloud.scm.api.opc.req.ShipRecordDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 发货记录明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShipRecordDetailFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface ShipRecordDetailFeignService {


    /**
     * 通过id查询发货记录明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShipRecordDetail/{id}", method = RequestMethod.GET)
    R<ShipRecordDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 发货记录明细 列表信息
     *
     * @param shipRecordDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecordDetail/fetchShipRecordDetailPage")
    R<Page<ShipRecordDetail>> getShipRecordDetailPage(@RequestBody ShipRecordDetailReq shipRecordDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 发货记录明细 列表信息
     *
     * @param shipRecordDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ShipRecordDetail/fetchShipRecordDetailList")
    R<List<ShipRecordDetail>> getShipRecordDetailList(@RequestBody ShipRecordDetailReq shipRecordDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增发货记录明细
     *
     * @param shipRecordDetail 发货记录明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/add")
    R<Boolean> save(@RequestBody ShipRecordDetail shipRecordDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改发货记录明细
     *
     * @param shipRecordDetail 发货记录明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/update")
    R<Boolean> updateById(@RequestBody ShipRecordDetail shipRecordDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除发货记录明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/listByIds")
    R<List<ShipRecordDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipRecordDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/countByShipRecordDetailReq")
    R<Integer> countByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipRecordDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/getOneByShipRecordDetailReq")
    R<ShipRecordDetail> getOneByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shipRecordDetailList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/ShipRecordDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<ShipRecordDetail> shipRecordDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
