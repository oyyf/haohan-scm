/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.Cell;
import com.haohan.cloud.scm.api.wms.req.CellReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货位信息表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CellFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface CellFeignService {


    /**
     * 通过id查询货位信息表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Cell/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货位信息表 列表信息
     * @param cellReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Cell/fetchCellPage")
    R getCellPage(@RequestBody CellReq cellReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货位信息表 列表信息
     * @param cellReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Cell/fetchCellList")
    R getCellList(@RequestBody CellReq cellReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @PostMapping("/api/feign/Cell/add")
    R save(@RequestBody Cell cell, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @PostMapping("/api/feign/Cell/update")
    R updateById(@RequestBody Cell cell, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货位信息表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Cell/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Cell/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Cell/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Cell/countByCellReq")
    R countByCellReq(@RequestBody CellReq cellReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Cell/getOneByCellReq")
    R getOneByCellReq(@RequestBody CellReq cellReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param cellList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Cell/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Cell> cellList, @RequestHeader(SecurityConstants.FROM) String from);


}
