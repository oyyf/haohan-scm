/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;
import com.haohan.cloud.scm.api.manage.req.MerchantAppExtReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商家应用扩展信息内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "MerchantAppExtFeignService", value = ScmServiceName.SCM_MANAGE)
public interface MerchantAppExtFeignService {


    /**
     * 通过id查询商家应用扩展信息
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/MerchantAppExt/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商家应用扩展信息 列表信息
     * @param merchantAppExtReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantAppExt/fetchMerchantAppExtPage")
    R getMerchantAppExtPage(@RequestBody MerchantAppExtReq merchantAppExtReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商家应用扩展信息 列表信息
     * @param merchantAppExtReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/MerchantAppExt/fetchMerchantAppExtList")
    R getMerchantAppExtList(@RequestBody MerchantAppExtReq merchantAppExtReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/add")
    R save(@RequestBody MerchantAppExt merchantAppExt, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商家应用扩展信息
     * @param merchantAppExt 商家应用扩展信息
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/update")
    R updateById(@RequestBody MerchantAppExt merchantAppExt, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商家应用扩展信息
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/MerchantAppExt/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/countByMerchantAppExtReq")
    R countByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param merchantAppExtReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/getOneByMerchantAppExtReq")
    R getOneByMerchantAppExtReq(@RequestBody MerchantAppExtReq merchantAppExtReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param merchantAppExtList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/MerchantAppExt/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<MerchantAppExt> merchantAppExtList, @RequestHeader(SecurityConstants.FROM) String from);


}
