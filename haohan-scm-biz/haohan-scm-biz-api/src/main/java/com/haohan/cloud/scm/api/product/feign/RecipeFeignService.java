/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.Recipe;
import com.haohan.cloud.scm.api.product.req.RecipeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商品加工配方表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "RecipeFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface RecipeFeignService {


    /**
     * 通过id查询商品加工配方表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Recipe/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品加工配方表 列表信息
     * @param recipeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Recipe/fetchRecipePage")
    R getRecipePage(@RequestBody RecipeReq recipeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品加工配方表 列表信息
     * @param recipeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Recipe/fetchRecipeList")
    R getRecipeList(@RequestBody RecipeReq recipeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @PostMapping("/api/feign/Recipe/add")
    R save(@RequestBody Recipe recipe, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @PostMapping("/api/feign/Recipe/update")
    R updateById(@RequestBody Recipe recipe, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品加工配方表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Recipe/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Recipe/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Recipe/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Recipe/countByRecipeReq")
    R countByRecipeReq(@RequestBody RecipeReq recipeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Recipe/getOneByRecipeReq")
    R getOneByRecipeReq(@RequestBody RecipeReq recipeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param recipeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Recipe/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Recipe> recipeList, @RequestHeader(SecurityConstants.FROM) String from);


}
