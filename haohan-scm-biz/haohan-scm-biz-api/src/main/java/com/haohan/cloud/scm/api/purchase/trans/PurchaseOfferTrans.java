package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.supply.req.AddOfferOrderReq;

/**
 * @author cx
 * @date 2019/6/11
 */

public class PurchaseOfferTrans {
  public static AddOfferOrderReq offerTrans(PurchaseOrderDetail pod){
    AddOfferOrderReq offerReq = new AddOfferOrderReq();
    offerReq.setOfferType(PurchaseEnumTrans.change(pod.getMethodType()).getType());
    offerReq.setPurchaseDetailSn(pod.getPurchaseDetailSn());
    offerReq.setGoodsId(pod.getGoodsId());
    offerReq.setBuyNum(pod.getNeedBuyNum());
    offerReq.setSupplyPrice(pod.getBuyPrice());
    offerReq.setSupplyImg(pod.getGoodsImg());
    offerReq.setGoodsModelId(pod.getGoodsModelId());
    return offerReq;
  }
}
