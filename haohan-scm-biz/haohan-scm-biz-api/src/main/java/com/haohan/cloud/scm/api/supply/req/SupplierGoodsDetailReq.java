package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/19
 */
@Data
@ApiModel(description = "查询供应商 售卖商品信息详情")
public class SupplierGoodsDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "supplierId不能为空")
    @ApiModelProperty(value = "供应商id",required = true)
    private String supplierId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "供应商货品id",required = true)
    private String id;

    @ApiModelProperty(value = "采购员uid" )
    private String uid;


}
