/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 短信模板
 *
 * @author haohan
 * @date 2019-05-13 18:34:19
 */
@Data
@TableName("scm_ms_short_message_template")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "短信模板")
public class ShortMessageTemplate extends Model<ShortMessageTemplate> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private String id;
    /**
   * 平台商家id
   */
    private String pmId;
    /**
   * 模板id
   */
    private String templateId;
    /**
   * 应用id
   */
    private String appId;
    /**
   * 短信模板类型:1验证码2通知3推广
   */
    private String shortTemplateType;
    /**
   * 消息类型:不同使用类型
   */
    private String shortMsgType;
    /**
   * 模板名称
   */
    private String templateName;
    /**
   * 模板内容
   */
    private String templateContent;
    /**
   * 短信签名
   */
    private String signName;
    /**
   * 模板说明
   */
    private String templateDesc;
    /**
   * 启用状态:0.未启用1.启用
   */
    private String useStatus;
    /**
   * 创建者
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    private String updateBy;
    /**
   * 更新时间
   */
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;
  
}
