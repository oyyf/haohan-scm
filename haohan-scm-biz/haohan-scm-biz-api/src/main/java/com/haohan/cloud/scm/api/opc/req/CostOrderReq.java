/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.CostOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 成本核算单
 *
 * @author haohan
 * @date 2019-05-30 10:17:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "成本核算单")
public class CostOrderReq extends CostOrder {

    private long pageSize;
    private long pageNo;




}
