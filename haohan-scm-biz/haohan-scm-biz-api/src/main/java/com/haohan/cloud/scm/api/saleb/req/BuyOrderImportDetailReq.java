package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.dto.BuyOrderImport;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/17
 */
@Data
public class BuyOrderImportDetailReq {

    @NotEmpty(message = "订单商品列表不能为空")
    @Valid
    private List<BuyOrderImport> list;

    @NotBlank(message = "采购单id不能为空")
    private String buyId;

}
