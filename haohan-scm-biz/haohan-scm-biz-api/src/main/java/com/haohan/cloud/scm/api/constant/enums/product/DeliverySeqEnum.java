package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum  DeliverySeqEnum  {

  first("0","第一批"),
  second("1","第二批");

  private static final Map<String, DeliverySeqEnum> MAP = new HashMap<>(8);

  static {
    for (DeliverySeqEnum e : DeliverySeqEnum.values()) {
      MAP.put(e.getType(), e);
    }
  }

  @JsonCreator
  public static DeliverySeqEnum getByType(String type) {
    if (MAP.containsKey(type)) {
      return MAP.get(type);
    }
    return null;
  }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
