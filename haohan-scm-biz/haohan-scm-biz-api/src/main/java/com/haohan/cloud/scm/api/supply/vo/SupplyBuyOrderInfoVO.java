package com.haohan.cloud.scm.api.supply.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/7
 * 查询供应订单对应采购单信息
 */
@Data
public class SupplyBuyOrderInfoVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "发货记录编号")
    private String shipRecordSn;

    @ApiModelProperty(value = "采购订单编号列表, 逗号连接")
    private String buyOrderSns;

    @ApiModelProperty(value = "供应订单编号对应采购明细的列表")
    private List<SupplyRelationBuyOrderDetailVO> detailList;


}
