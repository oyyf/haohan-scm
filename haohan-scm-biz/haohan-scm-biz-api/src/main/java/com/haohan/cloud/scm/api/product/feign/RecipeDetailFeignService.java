/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.RecipeDetail;
import com.haohan.cloud.scm.api.product.req.RecipeDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商品加工配方明细表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "RecipeDetailFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface RecipeDetailFeignService {


    /**
     * 通过id查询商品加工配方明细表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/RecipeDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品加工配方明细表 列表信息
     * @param recipeDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/RecipeDetail/fetchRecipeDetailPage")
    R getRecipeDetailPage(@RequestBody RecipeDetailReq recipeDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品加工配方明细表 列表信息
     * @param recipeDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/RecipeDetail/fetchRecipeDetailList")
    R getRecipeDetailList(@RequestBody RecipeDetailReq recipeDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/add")
    R save(@RequestBody RecipeDetail recipeDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/update")
    R updateById(@RequestBody RecipeDetail recipeDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品加工配方明细表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/RecipeDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/countByRecipeDetailReq")
    R countByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/getOneByRecipeDetailReq")
    R getOneByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param recipeDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/RecipeDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<RecipeDetail> recipeDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
