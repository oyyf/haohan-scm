package com.haohan.cloud.scm.api.common.req.admin;

/**
 * @author dy
 * @date 2019/1/7
 */
public class PdsSupplierListApiReq extends PdsBaseApiReq {

    private String goodsId;

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }
}
