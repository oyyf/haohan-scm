package com.haohan.cloud.scm.api.order.impl;

import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.crm.feign.SalesOrderFeignService;
import com.haohan.cloud.scm.api.crm.req.SalesOrderReq;
import com.haohan.cloud.scm.api.order.OrderCommonService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author dy
 * @date 2020/3/6
 */
@Service
@AllArgsConstructor
public class SalesOrderFeignCommonServiceImpl implements OrderCommonService {

    private final SalesOrderFeignService salesOrderFeignService;

    @Override
    public OrderInfoDTO fetchOrderInfo(String orderSn) {
        R<OrderInfoDTO> r = salesOrderFeignService.fetchOrderIfo(orderSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            throw new ErrorDataException("查询销售订单详情失败");
        }
        return r.getData();
    }

    @Override
    public boolean updateOrderSettlement(String orderSn, PayStatusEnum status) {
        SalesOrderReq req = new SalesOrderReq();
        req.setSalesOrderSn(orderSn);
        req.setPayStatus(status);
        R<Boolean> r = salesOrderFeignService.updateOrderSettlement(req, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || null == r.getData()) {
            return false;
        }
        return r.getData();
    }

    @Override
    public void orderCompleteShip(String orderSn) {
        // 目前订单无完成发货状态
    }
}
