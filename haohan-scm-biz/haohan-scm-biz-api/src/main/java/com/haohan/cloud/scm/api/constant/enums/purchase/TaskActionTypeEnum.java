package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum TaskActionTypeEnum {

    /**
     * 任务执行方式:1.审核2.分配3.采购
     */
    audit("1","审核"),
    allocation("2","分配"),
    procurement("3","采购");
    private static final Map<String, TaskActionTypeEnum> MAP = new HashMap<>(8);

    static {
      for (TaskActionTypeEnum e : TaskActionTypeEnum.values()) {
        MAP.put(e.getType(), e);
      }
    }

    @JsonCreator
    public static TaskActionTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
