package com.haohan.cloud.scm.api.crm.req.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
public class VisitCustomerCountReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 20, message = "员工id字符长度在0至20之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "查询开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

}
