/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 采购单汇总
 *
 * @author haohan
 * @date 2019-05-13 20:32:23
 */
@Data
@TableName("scm_ops_summary_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单汇总")
public class SummaryOrder extends Model<SummaryOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 汇总单号
     */
    private String summaryOrderId;
    /**
     * 商品分类id
     */
    private String goodsCategoryId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 平台报价
     */
    private BigDecimal platformPrice;
    /**
     * 采购均价
     */
    private BigDecimal buyAvgPrice;
    /**
     * 供应均价
     */
    private BigDecimal supplyAvgPrice;
    /**
     * 实际采购数量
     */
    private BigDecimal realBuyNum;
    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;
    /**
     * 最小供应量
     */
    private Integer limitSupplyNum;
    /**
     * 商家数量
     */
    private Integer buyerNum;
    /**
     * 采购日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private PdsSummaryStatusEnum status;
    /**
     * 是否生成交易单
     */
    private YesNoEnum isGenTrade;
    /**
     * 采购批次
     */
    private BuySeqEnum buySeq;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 汇总单状态:1.待处理2.采购中3.配送中.4已完成5.已关闭
     */
    private SummaryStatusEnum summaryStatus;
    /**
     * 调配类型:1.采购2.库存
     */
    private AllocateTypeEnum allocateType;
    /**
     * 汇总时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime summaryTime;
    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 商品ID(spu)
     */
    private String goodsId;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 商品规格(名称)
     */
    private String goodsModel;
    /**
     * 采购选项:1.单品2.原材料
     */
    @ApiModelProperty(value = "采购选项:1.单品2.原材料")
    private PurchaseOptionEnum purchaseOption;
    /**
     * 租户id
     */
    private Integer tenantId;

}
