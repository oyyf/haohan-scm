package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/29
 */
@Data
public class VisitFinishReq {

    @NotBlank(message = "客户拜访记录id不能为空")
    @Length(max = 32, message = "客户拜访记录id长度最大32字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @NotBlank(message = "离开地址定位不能为空")
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "离开地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "离开地址定位")
    private String departurePosition;

    @NotBlank(message = "离开地址不能为空")
    @Length(max = 64, message = "离开地址长度最大64字符")
    @ApiModelProperty(value = "离开地址")
    private String departureAddress;

    @Length(max = 255, message = "客户拜访记录id长度最大255字符")
    @ApiModelProperty(value = "拜访总结")
    private String summaryContent;

    @ApiModelProperty(value = "下次回访时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime nextVisitTime;

    @ApiModelProperty(value = "拜访图片列表")
    private List<String> photoList;

    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        visit.setId(this.id);
        visit.setDeparturePosition(this.departurePosition);
        visit.setDepartureAddress(this.departureAddress);
        visit.setSummaryContent(this.summaryContent);
        visit.setNextVisitTime(this.nextVisitTime);
        return visit;
    }

}
