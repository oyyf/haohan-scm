package com.haohan.cloud.scm.api.supply.trans;

import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;

import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/6/19
 */
public class SupplierEvaluateTrans {

    public static SupplierEvaluate trans(Supplier supplier, UPassport uPassport){

        SupplierEvaluate supplierEvaluate = new SupplierEvaluate();
        supplierEvaluate.setPmId(supplier.getPmId());
        supplierEvaluate.setSupplierId(supplier.getId());
        supplierEvaluate.setSupplierName(supplier.getSupplierName());
        supplierEvaluate.setEvaluatorId(uPassport.getId());
        //评价人名称
        supplierEvaluate.setEvaluatorName(uPassport.getLoginName());
        supplierEvaluate.setEvaluateTime(LocalDateTime.now());
        return supplierEvaluate;
    }

}
