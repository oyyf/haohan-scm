/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 库存调拨记录
 *
 * @author haohan
 * @date 2019-05-13 21:32:14
 */
@Data
@TableName("scm_pws_warehouse_allot")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "库存调拨记录")
public class WarehouseAllot extends Model<WarehouseAllot> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 库存调拨编号
     */
    private String warehouseAllotSn;
    /**
     * 调出仓库编号
     */
    private String outWarehouseSn;
    /**
     * 调出仓库名称
     */
    private String outWarehouseName;
    /**
     * 调入仓库编号
     */
    private String inWarehouseSn;
    /**
     * 调入仓库名称
     */
    private String inWarehouseName;
    /**
     * 调拨状态:1待确认2已确认3调拨中4已收货5取消
     */
    private String allotStatus;
    /**
     * 调拨时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime allotTime;
    /**
     * 申请人id
     */
    private String applicantId;
    /**
     * 申请人名称
     */
    private String applicantName;
    /**
     * 申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 确认人id
     */
    private String auditorId;
    /**
     * 确认人名称
     */
    private String auditorName;
    /**
     * 确认时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
