package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.market.MarketTypeEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
public class MarketManageDTO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 区域编码
     */
    @ApiModelProperty(value = "区域编码")
    private String areaSn;

    @ApiModelProperty(value = "区域名称")
    private String areaName;
    /**
     * 市场编码
     */
    @ApiModelProperty(value = "市场编码")
    private String marketSn;
    /**
     * 市场名称
     */
    @ApiModelProperty(value = "市场名称")
    private String marketName;
    /**
     * 市场类型
     */
    @ApiModelProperty(value = "市场类型")
    private MarketTypeEnum marketType;
    /**
     * 标签
     */
    @ApiModelProperty(value = "标签")
    private String tags;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String phone;
    /**
     * 图片组编号
     */
    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;
    /**
     * 省
     */
    @ApiModelProperty(value = "省")
    private String province;
    /**
     * 市
     */
    @ApiModelProperty(value = "市")
    private String city;
    /**
     * 区
     */
    @ApiModelProperty(value = "区")
    private String district;
    /**
     * 街道、乡镇
     */
    @ApiModelProperty(value = "街道、乡镇")
    private String street;
    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;
    /**
     * 地址定位 (经度，纬度)
     */
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    /**
     * 图片列表
     */
    @ApiModelProperty(value = "图片列表")
    private List<PhotoGallery> photoList;


}
