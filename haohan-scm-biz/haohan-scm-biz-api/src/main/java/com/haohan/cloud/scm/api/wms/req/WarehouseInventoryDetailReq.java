/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 库存盘点明细
 *
 * @author haohan
 * @date 2019-05-28 19:51:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "库存盘点明细")
public class WarehouseInventoryDetailReq extends WarehouseInventoryDetail {

    private long pageSize;
    private long pageNo;




}
