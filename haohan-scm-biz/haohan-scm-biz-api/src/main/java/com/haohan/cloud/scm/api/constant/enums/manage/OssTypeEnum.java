package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/12
 */
@Getter
@AllArgsConstructor
public enum OssTypeEnum implements IBaseEnum {

    /**
     * 云服务类型
     */
    aliyun("0", "阿里云"),
    tencent("1", "腾讯云"),
    baidu("2", "百度云");

    private static final Map<String, OssTypeEnum> MAP = new HashMap<>(8);

    static {
        for (OssTypeEnum e : OssTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static OssTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
