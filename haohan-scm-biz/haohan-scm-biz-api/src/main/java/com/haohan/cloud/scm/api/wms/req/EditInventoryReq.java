package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class EditInventoryReq extends WarehouseInventory {
    private List<WarehouseInventoryDetail> list;

    private List<WarehouseInventoryDetail> delDetails;

    private List<ProductInfoInventoryReq> addDetails;
}
