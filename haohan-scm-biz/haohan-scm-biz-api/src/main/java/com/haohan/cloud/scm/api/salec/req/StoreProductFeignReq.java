package com.haohan.cloud.scm.api.salec.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/6/8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreProductFeignReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "商品id的长度最大为32字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @ApiModelProperty(value = "上下架状态 true上架")
    private Boolean marketableFlag;


}
