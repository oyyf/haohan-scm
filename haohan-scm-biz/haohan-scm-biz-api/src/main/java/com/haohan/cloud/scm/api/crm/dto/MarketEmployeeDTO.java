package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.SexEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author dy
 * @date 2019/12/7
 */
@Data
@ApiModel(description = "员工信息")
@NoArgsConstructor
public class MarketEmployeeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("员工id")
    private String employeeId;

    @ApiModelProperty(value = "通行证ID")
    private String passportId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private MarketEmployeeTypeEnum employeeType;

    @ApiModelProperty(value = "启用状态 0.未启用 1.启用 2.待审核")
    private UseStatusEnum useStatus;

    @ApiModelProperty(value = "性别:1男2女")
    private SexEnum sex;

    @ApiModelProperty(value = "职位")
    private String post;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "头像图片地址")
    private String avatar;

    /**
     * 客户sn集合
     */
    @ApiModelProperty(value = "客户sn集合")
    private Set<String> customerSnSet;


    @ApiModelProperty(value = "角色列表")
    private List<Integer> roleList;

    @ApiModelProperty(value = "区域列表")
    private List<String> areaSnList;


    public MarketEmployeeDTO(MarketEmployee exist) {
        this.employeeId = exist.getId();
        this.passportId = exist.getPassportId();
        this.userId = exist.getUserId();
        this.name = exist.getName();
        this.telephone = exist.getTelephone();
        this.employeeType = exist.getEmployeeType();
        this.useStatus = exist.getUseStatus();
        this.sex = exist.getSex();
        this.post = exist.getPost();
        this.deptId = exist.getDeptId();
        this.deptName = exist.getDeptName();
        this.avatar = exist.getAvatar();
    }
}
