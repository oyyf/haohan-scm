/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;
import com.haohan.cloud.scm.api.product.req.ImperfectProductReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 残次货品处理记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ImperfectProductFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ImperfectProductFeignService {


    /**
     * 通过id查询残次货品处理记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ImperfectProduct/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 残次货品处理记录 列表信息
     * @param imperfectProductReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ImperfectProduct/fetchImperfectProductPage")
    R getImperfectProductPage(@RequestBody ImperfectProductReq imperfectProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 残次货品处理记录 列表信息
     * @param imperfectProductReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ImperfectProduct/fetchImperfectProductList")
    R getImperfectProductList(@RequestBody ImperfectProductReq imperfectProductReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/add")
    R save(@RequestBody ImperfectProduct imperfectProduct, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/update")
    R updateById(@RequestBody ImperfectProduct imperfectProduct, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除残次货品处理记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ImperfectProduct/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/countByImperfectProductReq")
    R countByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/getOneByImperfectProductReq")
    R getOneByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param imperfectProductList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ImperfectProduct/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ImperfectProduct> imperfectProductList, @RequestHeader(SecurityConstants.FROM) String from);


}
