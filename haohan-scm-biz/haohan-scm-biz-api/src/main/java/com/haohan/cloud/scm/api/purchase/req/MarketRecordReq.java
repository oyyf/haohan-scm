/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 市场行情价格记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "市场行情价格记录")
public class MarketRecordReq extends MarketRecord {

    private long pageSize;
    private long pageNo;

    private String goodsCategoryName;

    private String transactorName;



}
