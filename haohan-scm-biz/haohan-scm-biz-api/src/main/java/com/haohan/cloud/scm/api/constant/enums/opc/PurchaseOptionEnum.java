package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum PurchaseOptionEnum {
  single("1","单品"),
  raw("2","原材料");
    private static final Map<String, PurchaseOptionEnum> MAP = new HashMap<>(8);

    static {
        for (PurchaseOptionEnum e : PurchaseOptionEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PurchaseOptionEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }


  @EnumValue
  @JsonValue
  private String type;
  private String desc;

}
