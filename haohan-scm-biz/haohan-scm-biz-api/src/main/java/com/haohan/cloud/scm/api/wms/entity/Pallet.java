/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.PalletPutTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.PalletTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 托盘信息表
 *
 * @author haohan
 * @date 2019-05-13 21:29:29
 */
@Data
@TableName("scm_pws_pallet")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "托盘信息表")
public class Pallet extends Model<Pallet> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 仓库编号
     */
    private String warehouseSn;
    /**
     * 托盘编号
     */
    private String palletSn;
    /**
     * 托盘名称
     */
    private String palletName;
    /**
     * 描述
     */
    private String palletDesc;
    /**
     * 托盘类型:1木质2.塑料3.金属
     */
    private PalletTypeEnum palletType;
    /**
     * 放置货品编号
     */
    private String productSn;
    /**
     * 托盘放置类型:1.货位2.暂存点3.其他
     */
    private PalletPutTypeEnum palletPutType;
    /**
     * 货位编号
     */
    private String cellSn;
    /**
     * 暂存点编号
     */
    private String storagePlaceSn;
    /**
     * 启用状态:0.未启用1.启用
     */
    private UseStatusEnum useStatus;
    /**
     * 存货状态:1.空闲2.使用中
     */
    private StorageStatusEnum storageStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
