package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum ProductionTypeEnum {
  handle("1","加工"),
  delivery("2","配送"),
  storage("3","库存盘点");

    private static final Map<String, ProductionTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ProductionTypeEnum e : ProductionTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ProductionTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
