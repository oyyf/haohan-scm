package com.haohan.cloud.scm.api.manage.req.shop;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import com.haohan.cloud.scm.api.manage.dto.ShopSqlDTO;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/3/30
 * id查询详情 SingleGroup
 */
@Data
public class QueryShopReq {

    @NotBlank(message = "店铺id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "店铺id最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "店铺启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "店铺等级", notes = "原系统使用字典 0 总店, 1 分店 2采购配送店")
    private ShopLevelEnum shopLevel;

    @Length(max = 32, message = "商家id最大长度32字符")
    @ApiModelProperty(value = "商家id")
    private String merchantId;

    // 非eq

    @Length(max = 32, message = "店铺名称最大长度32字符")
    @ApiModelProperty(value = "店铺名称")
    private String name;

    @Length(max = 32, message = "店铺地址最大长度32字符")
    @ApiModelProperty(value = "店铺地址")
    private String address;

    @Length(max = 32, message = "店铺负责人名称最大长度32字符")
    @ApiModelProperty(value = "店铺负责人名称")
    private String manager;

    @Length(max = 32, message = "店铺电话最大长度32字符")
    @ApiModelProperty(value = "店铺电话")
    private String telephone;

    @Length(max = 32, message = "商家名称最大长度32字符")
    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @Length(max = 32, message = "店铺服务最大长度32字符")
    @ApiModelProperty(value = "店铺服务")
    private String shopService;

    @Length(max = 32, message = "店铺介绍最大长度32字符")
    @ApiModelProperty(value = "店铺介绍")
    private String shopDesc;

    @Length(max = 32, message = "行业名称最大长度32字符")
    @ApiModelProperty(value = "行业名称")
    private String industry;

    public ShopSqlDTO transTo() {
        ShopSqlDTO query = new ShopSqlDTO();
        query.setShopId(this.shopId);
        query.setStatus(this.status);
        query.setShopLevel(this.shopLevel);
        query.setMerchantId(this.merchantId);

        query.setName(this.name);
        query.setAddress(this.address);
        query.setManager(this.manager);
        query.setTelephone(this.telephone);
        query.setShopService(this.shopService);
        query.setShopDesc(this.shopDesc);
        query.setIndustry(this.industry);
        query.setMerchantName(this.merchantName);
        return query;
    }
}
