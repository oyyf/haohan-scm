package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.SalesStageEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class SalesStageEnumConverterUtil implements Converter<SalesStageEnum> {
    @Override
    public SalesStageEnum convert(Object o, SalesStageEnum salesStageEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SalesStageEnum.getByType(o.toString());
    }
}
