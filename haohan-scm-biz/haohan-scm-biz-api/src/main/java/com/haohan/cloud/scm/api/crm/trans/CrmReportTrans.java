package com.haohan.cloud.scm.api.crm.trans;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.ReportSqlDTO;
import com.haohan.cloud.scm.api.crm.dto.analysis.DataReportAnalysisDTO;
import com.haohan.cloud.scm.api.crm.dto.analysis.SalesReportAnalysisDTO;
import com.haohan.cloud.scm.api.crm.dto.analysis.WorkReportAnalysisDTO;
import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysis;
import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import com.haohan.cloud.scm.api.crm.req.report.DataDetailAddReq;
import com.haohan.cloud.scm.api.crm.req.report.QueryStockReq;
import com.haohan.cloud.scm.api.crm.vo.analysis.*;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/18
 */
@UtilityClass
public class CrmReportTrans {

    /**
     * 复制数据上报明细的商品信息
     *
     * @param detail
     * @param model
     */
    public void reportDetailCopyGoods(DataReportDetail detail, GoodsModelDTO model) {
        detail.setGoodsModelId(model.getId());
        detail.setGoodsName(model.getGoodsName());
        detail.setModelAttr(model.getModelName());
        detail.setGoodsUnit(model.getModelUnit());
        detail.setGoodsImg(model.getModelUrl());
        detail.setModelSn(model.getGoodsModelSn());
        detail.setModelCode(model.getModelCode());
    }

    /**
     * 默认设置 上报明细 的 保质期/生产日期/到期日期
     *
     * @param detail
     * @param startDate 生产日期
     */
    public void defaultDetailInfo(DataReportDetail detail, LocalDate startDate) {
        if (StrUtil.isBlank(detail.getExpiration())) {
            detail.setExpiration("一个月");
        }
        LocalDateTime time = detail.getProductTime();
        if (null == time) {
            time = LocalDateTime.of(startDate, LocalTime.MIN);
            detail.setProductTime(time);
        }
        if (null == detail.getMaturityTime()) {
            detail.setMaturityTime(time.plusDays(30));
        }
    }

    /**
     * 复制 数据上报明细的可修改项
     *
     * @param source
     * @param target
     */
    public void copyReportDetail(DataReportDetail source, DataReportDetail target) {
        target.setTradePrice(source.getTradePrice());
        target.setGoodsNum(source.getGoodsNum());
        target.setExpiration(source.getExpiration());
        target.setProductTime(source.getProductTime());
        target.setMaturityTime(source.getMaturityTime());
        target.setRemarks(source.getRemarks());
        target.setSalesGoodsType(source.getSalesGoodsType());
    }

    /**
     * 根据商品销售类型处理价格
     * 1.普通: 以零售价为准；
     * 2.促销品: 以填写值为准；
     * 3.赠品：价格为0。
     *
     * @param detail
     * @param model
     */
    public void choicePriceByType(DataReportDetail detail, GoodsModelDTO model) {
        BigDecimal price;
        switch (detail.getSalesGoodsType()) {
            case normal:
                detail.setTradePrice((price = model.getSalePrice()) == null ? model.getModelPrice() : price);
                break;
            case gift:
                detail.setTradePrice(BigDecimal.ZERO);
                break;
            case promotion:
            default:
        }
        if (null == detail.getTradePrice()) {
            throw new ErrorDataException(detail.getSalesGoodsType().getDesc().concat(":销售单价有误"));
        }
    }

    public static EmployeeReportVO transEmployeeReport(WorkReportAnalysisDTO entity) {
        EmployeeReportVO reportVO = new EmployeeReportVO();
        reportVO.setEmployeeId(entity.getEmployeeId());
        reportVO.setEmployeeName(entity.getEmployeeName());
        reportVO.setReportNum(entity.getReportNum());
        return reportVO;
    }

    public static EmployeeReportVO initEmployeeReport(MarketEmployee entity) {
        EmployeeReportVO reportVO = new EmployeeReportVO();
        reportVO.setEmployeeId(entity.getId());
        reportVO.setEmployeeName(entity.getName());
        reportVO.setReportNum(0);
        return reportVO;
    }

    public static ReportAnalysisVO transSalesAnalysis(DataReportAnalysisDTO entity) {
        ReportAnalysisVO analysisVO = new ReportAnalysisVO();
        analysisVO.setNum(entity.getNum());
        analysisVO.setAmount(entity.getAmount());
        analysisVO.setStartDate(entity.getStartDate());
        analysisVO.setEndDate(entity.getEndDate());
        return analysisVO;
    }

    public static CustomerSalesVO transCustomerSales(DataReportAnalysisDTO entity) {
        CustomerSalesVO customerSales = new CustomerSalesVO();
        customerSales.setCustomerSn(entity.getCustomerSn());
        customerSales.setCustomerName(entity.getCustomerName());
        customerSales.setOrderNum(entity.getNum());
        customerSales.setOrderAmount(entity.getAmount());
        return customerSales;
    }

    public static EmployeeSalesVO transEmployeeSales(DataReportAnalysisDTO entity) {
        EmployeeSalesVO employeeSales = new EmployeeSalesVO();
        employeeSales.setEmployeeName(entity.getEmployeeName());
        employeeSales.setOrderNum(entity.getNum());
        employeeSales.setOrderAmount(entity.getAmount());
        return employeeSales;
    }

    public static GoodsSalesVO transGoodsSales(DataReportAnalysisDTO entity) {
        GoodsSalesVO goodsSales = new GoodsSalesVO();
        goodsSales.setGoodsModelSn(entity.getGoodsModelSn());
        goodsSales.setModelName(entity.getModelName());
        goodsSales.setGoodsName(entity.getGoodsName());
        goodsSales.setGoodsUnit(entity.getGoodsUnit());
        goodsSales.setOrderNum(entity.getNum());
        goodsSales.setOrderAmount(entity.getAmount());
        goodsSales.setGoodsNum(entity.getGoodsNum());
        goodsSales.setSalesGoodsType(entity.getSalesGoodsType());
        return goodsSales;
    }

    public static void copyFromEmployee(MarketEmployee employee, WorkReport workReport) {
        workReport.setReportManId(employee.getId());
        workReport.setReportMan(employee.getName());
        workReport.setReportTel(employee.getTelephone());
    }

    public static void transReportSqlParams(ReportSqlDTO params, QueryStockReq req) {
        params.setCustomerSn(req.getCustomerSn());
        params.setGoodsName(req.getGoodsName());
        params.setModelSn(req.getModelSn());
        params.setModelCode(req.getModelCode());
        if (null != req.getMaturityTime()) {
            params.setMaturityTime(req.getMaturityTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
    }

    /**
     * 复制出赠品
     *
     * @param detail
     * @return
     */
    public static DataDetailAddReq copyGiftDetail(DataDetailAddReq detail) {
        DataDetailAddReq gift = new DataDetailAddReq();
        gift.setGoodsModelId(detail.getGoodsModelId());
        gift.setTradePrice(BigDecimal.ZERO);
        gift.setGoodsNum(detail.getGiftNum());
        gift.setExpiration(detail.getExpiration());
        gift.setProductTime(detail.getProductTime());
        gift.setMaturityTime(detail.getMaturityTime());
        gift.setRemarks(detail.getRemarks());
        gift.setSalesGoodsType(SalesGoodsTypeEnum.gift);
        return gift;
    }


    /**
     * 处理明细列表中的赠品
     *
     * @param detailList
     * @return
     */
    public static List<DataDetailAddReq> handleDetailListWithGift(List<DataDetailAddReq> detailList) {
        List<DataDetailAddReq> giftList = new ArrayList<>(detailList.size());
        detailList.forEach(detail -> {
            BigDecimal giftNum = null == detail.getGiftNum() ? BigDecimal.ZERO : detail.getGiftNum();
            // 只有赠品
            if (detail.getGoodsNum().compareTo(BigDecimal.ZERO) == 0) {
                detail.setGoodsNum(giftNum);
                detail.setTradePrice(BigDecimal.ZERO);
                detail.setSalesGoodsType(SalesGoodsTypeEnum.gift);
            } else if (giftNum.compareTo(BigDecimal.ZERO) > 0) {
                giftList.add(CrmReportTrans.copyGiftDetail(detail));
            }
        });
        detailList.addAll(giftList);
        return detailList;
    }

    public static SalesReportAnalysis transSalesReportAnalysis(SalesReportAnalysisDTO analysisDTO) {
        SalesReportAnalysis analysis = new SalesReportAnalysis();
        analysis.setCustomerSn(analysisDTO.getCustomerSn());
        analysis.setCustomerName(analysisDTO.getCustomerName());
        analysis.setEmployeeNames(analysisDTO.getEmployeeNames());
        analysis.setMonth(analysisDTO.getMonth());
        analysis.setTotalAmount(analysisDTO.getTotalAmount());
        analysis.setGiftAmount(analysisDTO.getGiftAmount());
        analysis.setGiftContent(analysisDTO.getGiftContent());
        return analysis;
    }
}
