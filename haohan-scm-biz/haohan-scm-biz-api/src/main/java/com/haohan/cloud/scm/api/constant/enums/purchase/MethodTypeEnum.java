package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/29
 */
@Getter
@AllArgsConstructor
public enum MethodTypeEnum {

    /**
     *  采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    bidding("1","竞价采购"),
    single("2","单品采购"),
    agreement("3","协议供应");

    private static final Map<String, MethodTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MethodTypeEnum e : MethodTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MethodTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
