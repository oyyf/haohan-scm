/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品分类
 *
 * @author haohan
 * @date 2019-05-13 18:46:41
 */
@Data
@TableName("scm_cms_goods_category")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品分类")
public class GoodsCategory extends Model<GoodsCategory> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 行业名称
     */
    private String industry;
    /**
     * 关键词
     */
    private String keywords;
    /**
     * 父级编号
     */
    private String parentId;
    /**
     * 所有父级编号
     */
    private String parentIds;
    /**
     * 名称
     */
    private String name;
    /**
     * 排序
     */
    private BigDecimal sort;
    /**
     * 描述
     */
    private String description;
    /**
     * logo地址
     */
    private String logo;
    /**
     * 分类类型 修改为是否展示 0否 1是 (用于分类树中是否展示)
     */
    private YesNoEnum categoryType;
    /**
     * 是否上架  0否 1是 (用于所属商品的批量上下架)
     */
    private YesNoEnum status;
    /**
     * 分类编号  原系统 第三方分类编号   即速分类ID (未使用)
     */
    private String categorySn;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 父分类编号/即速父级ID (未使用)
     */
    private String parentSn;
    /**
     * 商品类型 0实体商品，1虚拟商品
     */
    private String goodsType;
    /**
     * 商品分类通用编号  (未使用)
     */
    private String generalCategorySn;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息 (json串:为供应商品分类时保存平台分类Id: categoryId)
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
