package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/7/13
 */
@Data
@ApiModel("同一商品 出库 实出数量确认")
public class ConfirmRealExitNumReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id", required = true)
    private String goodsModelId;

    @NotEmpty(message = "出库单明细列表detailList不能为空")
    @ApiModelProperty(value = "出库单明细列表", required = true)
    List<DetailConfirmRealExitNumReq> detailList;

    @ApiModelProperty(value = "仓库编号")
    private String warehouseSn;

}
