package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("app登陆")
public class CrmLoginReq {

    @NotBlank(message = "clientId不能为空")
    private String clientId;

    @NotBlank(message = "clientSecret不能为空")
    private String clientSecret;

    @NotBlank(message = "用户名不能为空", groups = {FirstGroup.class})
    @Length(max = 20, message = "用户名长度最大20字符")
    @ApiModelProperty("用户名")
    private String userName;

    @NotBlank(message = "手机号不能为空", groups = {SecondGroup.class})
    @Length(max = 20, message = "手机号长度最大20字符")
    @ApiModelProperty("手机号")
    private String telephone;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6, max = 20, message = "密码长度6-20字符")
    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("随机串")
    private String randomStr;

    @ApiModelProperty("验证码")
    private String code;

    @ApiModelProperty("类型")
    private String grantType;

    @NotBlank(message = "scope不能为空")
    @ApiModelProperty("范围")
    private String scope;


}
