package com.haohan.cloud.scm.api.common.req.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 账户查询请求
 */
@Data
public class PayAccountReq extends BasePayParams {

    @JsonProperty("app_id")
    private String appId;

}
