package com.haohan.cloud.scm.api.salec.dto;

import lombok.Data;

/**
 * @author cx
 * @date 2019/7/14
 */
@Data
public class AttrInfoDTO {
    private String suk;
    private String image;
}
