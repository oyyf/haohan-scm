package com.haohan.cloud.scm.api.supply.trans;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;

/**
 * @author xwx
 * @date 2019/6/17
 */
public class SupplierGoodsTrans {

    public static SupplierGoods trans(GoodsModel goodsModel, Supplier supplier){
        SupplierGoods goods = new SupplierGoods();
        goods.setPmId(supplier.getPmId());
        goods.setSupplierId(supplier.getId());
        goods.setSupplierMerchantId(supplier.getMerchantId());
        goods.setGoodsId(goodsModel.getGoodsId());
        goods.setGoodsModelId(goodsModel.getId());
        goods.setStatus(UseStatusEnum.enabled);
        goods.setSupplyType(SupplyTypeEnum.ordinaryRetail);
        return goods;
    }
}
