/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysisDetail;
import com.haohan.cloud.scm.api.crm.req.SalesReportAnalysisDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户销量上报数据统计明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesReportAnalysisDetailFeignService", value = ScmServiceName.SCM_CRM)
public interface SalesReportAnalysisDetailFeignService {


    /**
     * 通过id查询客户销量上报数据统计明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesReportAnalysisDetail/{id}", method = RequestMethod.GET)
    R<SalesReportAnalysisDetail> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户销量上报数据统计明细 列表信息
     *
     * @param salesReportAnalysisDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/fetchSalesReportAnalysisDetailPage")
    R<Page<SalesReportAnalysisDetail>> getSalesReportAnalysisDetailPage(@RequestBody SalesReportAnalysisDetailReq salesReportAnalysisDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户销量上报数据统计明细 列表信息
     *
     * @param salesReportAnalysisDetailReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/fetchSalesReportAnalysisDetailList")
    R<List<SalesReportAnalysisDetail>> getSalesReportAnalysisDetailList(@RequestBody SalesReportAnalysisDetailReq salesReportAnalysisDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户销量上报数据统计明细
     *
     * @param salesReportAnalysisDetail 客户销量上报数据统计明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/add")
    R<Boolean> save(@RequestBody SalesReportAnalysisDetail salesReportAnalysisDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户销量上报数据统计明细
     *
     * @param salesReportAnalysisDetail 客户销量上报数据统计明细
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/update")
    R<Boolean> updateById(@RequestBody SalesReportAnalysisDetail salesReportAnalysisDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户销量上报数据统计明细
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/listByIds")
    R<List<SalesReportAnalysisDetail>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesReportAnalysisDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/countBySalesReportAnalysisDetailReq")
    R<Integer> countBySalesReportAnalysisDetailReq(@RequestBody SalesReportAnalysisDetailReq salesReportAnalysisDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesReportAnalysisDetailReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/getOneBySalesReportAnalysisDetailReq")
    R<SalesReportAnalysisDetail> getOneBySalesReportAnalysisDetailReq(@RequestBody SalesReportAnalysisDetailReq salesReportAnalysisDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesReportAnalysisDetailList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysisDetail/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesReportAnalysisDetail> salesReportAnalysisDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
