package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class DeliveryTypeEnumConverterUtil implements Converter<DeliveryTypeEnum> {
    @Override
    public DeliveryTypeEnum convert(Object o, DeliveryTypeEnum deliveryTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DeliveryTypeEnum.getByType(o.toString());
    }
}
