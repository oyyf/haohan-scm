package com.haohan.cloud.scm.api.constant.enums.saleb;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/7/5
 * 客户订单明细汇总状态
 */
@Getter
@AllArgsConstructor
public enum DetailSummaryFlagEnum implements IBaseEnum {

    /**
     * 客户订单明细汇总状态:0未处理1已汇总2已备货
     */
    wait("0", "未处理"),
    summary("1", "已汇总"),
    prepare("2", "已备货");

    private static final Map<String, DetailSummaryFlagEnum> MAP = new HashMap<>(8);

    static {
        for (DetailSummaryFlagEnum e : DetailSummaryFlagEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DetailSummaryFlagEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
