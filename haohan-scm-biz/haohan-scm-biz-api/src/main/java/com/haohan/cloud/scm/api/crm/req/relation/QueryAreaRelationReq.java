package com.haohan.cloud.scm.api.crm.req.relation;

import lombok.Data;

/**
 * @author dy
 * @date 2019/12/23
 */
@Data
public class QueryAreaRelationReq {

    private String deptId;
    private String deptName;

}
