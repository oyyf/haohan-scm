package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.WarehouseTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class WarehouseTypeEnumConverterUtil implements Converter<WarehouseTypeEnum> {
    @Override
    public WarehouseTypeEnum convert(Object o, WarehouseTypeEnum warehouseTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return WarehouseTypeEnum.getByType(o.toString());
    }
}
