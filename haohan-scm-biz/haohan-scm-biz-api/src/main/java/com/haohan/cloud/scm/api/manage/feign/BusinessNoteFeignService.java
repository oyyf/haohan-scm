/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.BusinessNote;
import com.haohan.cloud.scm.api.manage.req.BusinessNoteReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 商务留言内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BusinessNoteFeignService", value = ScmServiceName.SCM_MANAGE)
public interface BusinessNoteFeignService {


    /**
     * 通过id查询商务留言
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/BusinessNote/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商务留言 列表信息
     * @param businessNoteReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/BusinessNote/fetchBusinessNotePage")
    R getBusinessNotePage(@RequestBody BusinessNoteReq businessNoteReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商务留言 列表信息
     * @param businessNoteReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/BusinessNote/fetchBusinessNoteList")
    R getBusinessNoteList(@RequestBody BusinessNoteReq businessNoteReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商务留言
     * @param businessNote 商务留言
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/add")
    R save(@RequestBody BusinessNote businessNote, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商务留言
     * @param businessNote 商务留言
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/update")
    R updateById(@RequestBody BusinessNote businessNote, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商务留言
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/BusinessNote/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param businessNoteReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/countByBusinessNoteReq")
    R countByBusinessNoteReq(@RequestBody BusinessNoteReq businessNoteReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param businessNoteReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/getOneByBusinessNoteReq")
    R getOneByBusinessNoteReq(@RequestBody BusinessNoteReq businessNoteReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param businessNoteList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/BusinessNote/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<BusinessNote> businessNoteList, @RequestHeader(SecurityConstants.FROM) String from);


}
