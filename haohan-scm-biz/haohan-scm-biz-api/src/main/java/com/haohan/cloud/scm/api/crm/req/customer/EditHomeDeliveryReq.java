package com.haohan.cloud.scm.api.crm.req.customer;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.MemberTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PaymentTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.HomeDeliveryRecord;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/10
 * 新增 FirstGroup  编辑SecondGroup
 */
@Data
public class EditHomeDeliveryReq {

    @NotBlank(message = "id不能为空", groups = {SecondGroup.class})
    @Length(max = 64, message = "主键的最大长度为64字符")
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 客户编码
     */
    @NotBlank(message = "客户不能为空")
    @Length(max = 64, message = "客户编码的最大长度为64字符")
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 联系人id
     */
    @NotBlank(message = "联系人不能为空")
    @Length(max = 64, message = "联系人id的最大长度为64字符")
    @ApiModelProperty(value = "联系人id")
    private String linkmanId;
    /**
     * 配件类型 1普通件 2急件
     */
    @NotNull(message = "配件类型不能为空")
    @ApiModelProperty(value = "配件类型 1普通件 2急件")
    private DeliveryGoodsTypeEnum goodsType;
    /**
     * 件数
     */
    @NotNull(message = "件数不能为空")
    @Digits(integer = 10, fraction = 0, message = "件数的整数位最大10位, 小数位最大0位")
    @ApiModelProperty(value = "件数")
    private Integer goodsNum;
    /**
     * 重量 , 单位kg
     */
    @NotNull(message = "重量不能为空")
    @Digits(integer = 8, fraction = 2, message = "重量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "重量 , 单位kg")
    private BigDecimal weight;
    /**
     * 预约配送时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预约配送时间")
    private LocalDateTime appointmentTime;
    /**
     * 配送时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "配送时间")
    private LocalDateTime deliveryTime;
    /**
     * 配送物品 （自定义标签）
     */
    @Length(max = 64, message = "配送物品的最大长度为64字符")
    @ApiModelProperty(value = "配送物品 （自定义标签）")
    private String goodsTags;
    /**
     * 配送状态 1未送，2已送，3异常
     */
    @ApiModelProperty(value = "配送状态 1未送，2已送，3异常")
    private DeliveryGoodsStatusEnum deliveryStatus;
    /**
     * 会员类型 1普通，2年卡
     */
    @ApiModelProperty(value = "会员类型（1普通，2年卡）")
    private MemberTypeEnum memberType;
    /**
     * 付款方式 1.现金，2.微信转账，3.线上支付
     */
    @ApiModelProperty(value = "付款方式 1.现金，2.微信转账，3.线上支付")
    private PaymentTypeEnum paymentType;
    /**
     * 付款金额（数字, 单位元）
     */
    @NotNull(message = "付款金额不能为空")
    @Digits(integer = 8, fraction = 2, message = "重量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "付款金额（数字, 单位元）")
    private BigDecimal amount;
    /**
     * 其他服务 （自定义标签）
     */
    @Length(max = 64, message = "其他服务的最大长度为64字符")
    @ApiModelProperty(value = "其他服务 （自定义标签）")
    private String otherServices;
    /**
     * 补充说明
     */
    @Length(max = 255, message = "补充说明的最大长度为255字符")
    @ApiModelProperty(value = "补充说明")
    private String description;
    /**
     * 订单号
     */
    @Length(max = 64, message = "订单号的最大长度为64字符")
    @ApiModelProperty(value = "订单号")
    private String orderSn;
    /**
     * 订单类型
     */
    @Length(max = 5, message = "订单类型的最大长度为5字符")
    @ApiModelProperty(value = "订单类型")
    private String orderTye;
    /**
     * 第三方订单号
     */
    @Length(max = 64, message = "第三方订单号的最大长度为64字符")
    @ApiModelProperty(value = "第三方订单号")
    private String thirdOrderSn;

    /**
     * 备注信息
     */
    @Length(max = 255, message = "备注信息的最大长度为255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;


    public HomeDeliveryRecord transTo() {
        HomeDeliveryRecord record = new HomeDeliveryRecord();
        BeanUtil.copyProperties(this, record);
        return record;
    }
}
