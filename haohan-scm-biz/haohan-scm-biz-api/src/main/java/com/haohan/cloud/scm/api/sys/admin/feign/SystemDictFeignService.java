package com.haohan.cloud.scm.api.sys.admin.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pig4cloud.pigx.admin.api.entity.SysDictItem;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author dy
 * @program: haohan-fresh-scm
 * @description: 系统用户内部接口服务
 **/
@FeignClient(contextId = "systemDictFeignService", value = ServiceNameConstants.UPMS_SERVICE)
public interface SystemDictFeignService {

    /**
     * 通过字典类型 查找字典项列表
     *
     * @param type 类型
     * @return 同类型字典
     */
    @GetMapping("/api/feign/dict/type/{type}")
    R<List<SysDictItem>> fetchDictByType(@PathVariable("type") String type, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 分页查询 字典项
     *
     * @return
     */
    @PostMapping("/api/feign/dict/item/page")
    R<IPage<SysDictItem>> fetchSysDictItemPage(@RequestBody Map<String, Object> params, @RequestHeader(SecurityConstants.FROM) String from);

}
