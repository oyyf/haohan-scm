package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.DataReportImport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/18
 */
@Data
public class SalesReportImportReq {

    @NotEmpty(message = "上报信息列表不能为空")
    private List<DataReportImport> dataList;

    @ApiModelProperty("是否按编号导入")
    private Boolean importType;
}
