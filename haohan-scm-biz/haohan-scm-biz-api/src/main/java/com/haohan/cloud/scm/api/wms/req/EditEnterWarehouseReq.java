package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/13
 */

@Data
public class EditEnterWarehouseReq extends EnterWarehouse {

    private List<EnterWarehouseDetail> list;

    private List<EnterWarehouseDetail> delList;

    private List<GoodsModelDTONumReq> addList;
}
