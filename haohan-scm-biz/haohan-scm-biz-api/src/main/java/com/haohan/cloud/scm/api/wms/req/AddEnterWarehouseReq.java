package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/5
 */
@Data
@ApiModel(description = "新增入库单 根据采购单明细")
public class AddEnterWarehouseReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseDetailSn不能为空")
    @ApiModelProperty(value = "采购单明细编号", required = true)
    private String purchaseDetailSn;
}
