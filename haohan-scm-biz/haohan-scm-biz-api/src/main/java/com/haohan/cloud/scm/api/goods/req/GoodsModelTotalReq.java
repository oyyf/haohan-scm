/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品规格名称
 *
 * @author haohan
 * @date 2019-05-28 19:54:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品规格名称")
public class GoodsModelTotalReq extends GoodsModelTotal {

    private long pageSize;
    private long pageNo;




}
