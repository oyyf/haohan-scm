package com.haohan.cloud.scm.api.crm.req.bill;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/3/11
 */
@Data
public class CrmSettlementReadyReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "结算单编号不能为空")
    @Length(max = 32, message = "结算单编号的最大长度为32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @NotNull(message = "结算金额不能为空")
    @Digits(integer = 8, fraction = 2, message = "结算金额的整数位最大8位, 小数位最大2位")
    private BigDecimal settlementAmount;

}
