package com.haohan.cloud.scm.api.bill.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/12/26
 */
@Data
@NoArgsConstructor
public class SettlementInfoVO {

    /**
     * 结算单编号
     */
    private String settlementSn;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 平台商家名称
     */
    private String pmName;
    /**
     * 结算公司id: merchantId
     */
    private String companyId;
    /**
     * 结算公司名称
     */
    private String companyName;
    /**
     * 结算订单编号
     */
    private String orderSn;
    /**
     * 结算账单编号
     */
    private String billSn;
    /**
     * 账单日期 （结算单创建日期）
     */
    private LocalDate billDate;

    // 结算相关
    /**
     * 结算类型: 应收/应付
     */
    private SettlementTypeEnum settlementType;
    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;
    /**
     * 结算时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    private PayTypeEnum payType;
    /**
     * 是否结算:0否1是
     */
    private YesNoEnum settlementStatus;
    /**
     * 结算单类型: 1.普通, 2.汇总, 3.明细
     */
    private SettlementStyleEnum settlementStyle;
    /**
     * 汇总结算单编号
     */
    private String summarySettlementSn;

    // 记录信息
    /**
     * 结款人名称
     */
    private String companyOperator;
    /**
     * 结算凭证图片组编号
     */
    private String groupNum;
    /**
     * 结算说明
     */
    private String settlementDesc;
    /**
     * 经办人名称
     */
    private String operatorName;
    /**
     * 备注信息
     */
    private String remarks;

    // 扩展信息

    @ApiModelProperty(value = "图片列表")
    private List<PhotoGallery> photoList;

    /**
     * 是否初始开始结算（true:无随机码,初始开始结算;false:有随机码,等待完成支付）
     */
    private Boolean settleFlag;

    /**
     * 开始结算状态随机码（验证是否此次结算）
     */
    private String randomCode;

    public SettlementInfoVO(SettlementAccount settlement) {
        BeanUtil.copyProperties(settlement, this);
        this.billDate = null != settlement.getCreateDate() ? settlement.getCreateDate().toLocalDate() : null;
    }


}
