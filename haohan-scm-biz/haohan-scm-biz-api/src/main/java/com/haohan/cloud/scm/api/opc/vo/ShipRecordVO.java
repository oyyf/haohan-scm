package com.haohan.cloud.scm.api.opc.vo;

import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipRecordStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2020/1/6
 */
@Data
@NoArgsConstructor
public class ShipRecordVO {

    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @Length(max = 64, message = "平台商家id长度最大64字符")
    @ApiModelProperty(value = "平台商家id")
    private String pmId;

    @Length(max = 64, message = "平台商家名称长度最大64字符")
    @ApiModelProperty(value = "平台商家名称")
    private String pmName;

    @Length(max = 64, message = "发货记录编号长度最大64字符")
    @ApiModelProperty(value = "发货记录编号")
    private String shipRecordSn;

    @Length(max = 64, message = "客户ID长度最大64字符")
    @ApiModelProperty(value = "客户ID", notes = "对应商家id")
    private String customerId;

    @Length(max = 64, message = "客户名称长度最大64字符")
    @ApiModelProperty(value = "客户名称", notes = "对应商家名称")
    private String customerName;

    @Length(max = 64, message = "收货人名称长度最大64字符")
    @ApiModelProperty(value = "收货人名称")
    private String receiverName;

    @Length(max = 64, message = "收货人电话长度最大64字符")
    @ApiModelProperty(value = "收货人电话")
    private String receiverTelephone;

    @Length(max = 64, message = "收货人地址长度最大64字符")
    @ApiModelProperty(value = "收货人地址")
    private String receiverAddress;

    @ApiModelProperty(value = "交货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @Length(max = 64, message = "发货人长度最大64字符")
    @ApiModelProperty(value = "发货人")
    private String shipperName;

    @Length(max = 64, message = "发货人电话长度最大64字符")
    @ApiModelProperty(value = "发货人电话")
    private String shipperTelephone;

    @Length(max = 64, message = "发货地址长度最大64字符")
    @ApiModelProperty(value = "发货地址")
    private String shipAddress;

    @ApiModelProperty(value = "发货时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime shipTime;

    @ApiModelProperty(value = "发货状态: 1.待发货 2.已发货 3.已关闭")
    private ShipRecordStatusEnum shipStatus;

    @ApiModelProperty(value = "发货方式 1.第三方物流2.自配送")
    private ShipTypeEnum shipType;

    @Length(max = 64, message = "物流单号长度最大64字符")
    @ApiModelProperty(value = "物流单号")
    private String logisticsSn;

    @Length(max = 64, message = "物流公司长度最大64字符")
    @ApiModelProperty(value = "物流公司")
    private String logisticsName;

    @Length(max = 64, message = "订单编号长度最大64字符")
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "订单类型： 1.采购订单、2.供应订单、3.退货订单、4.销售订单")
    private OrderTypeEnum orderType;

    @ApiModelProperty(value = "下单时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;

    @Digits(integer = 10, fraction = 2, message = "订单总金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "订单总金额")
    private BigDecimal totalAmount;

    @Digits(integer = 10, fraction = 2, message = "订单其他金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "订单其他金额")
    private BigDecimal otherAmount;

    @Digits(integer = 10, fraction = 2, message = "订单商品合计金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "订单商品合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "订单商品种类数")
    private Integer goodsNum;

    // 明细

    @ApiModelProperty(value = "发货记录商品明细列表")
    private List<ShipRecordDetailVO> detailList;

    public ShipRecordVO(ShipRecord ship) {
        this.id = ship.getId();
        this.pmId = ship.getPmId();
        this.pmName = ship.getPmName();
        this.shipRecordSn = ship.getShipRecordSn();
        this.customerId = ship.getCustomerId();
        this.customerName = ship.getCustomerName();
        this.receiverName = ship.getReceiverName();
        this.receiverTelephone = ship.getReceiverTelephone();
        this.receiverAddress = ship.getReceiverAddress();
        this.deliveryDate = ship.getDeliveryDate();
        this.shipperName = ship.getShipperName();
        this.shipperTelephone = ship.getShipperTelephone();
        this.shipAddress = ship.getShipAddress();
        this.shipTime = ship.getShipTime();
        this.shipStatus = ship.getShipStatus();
        this.shipType = ship.getShipType();
        this.logisticsSn = ship.getLogisticsSn();
        this.logisticsName = ship.getLogisticsName();
        this.orderSn = ship.getOrderSn();
        this.orderType = ship.getOrderType();
        this.orderTime = ship.getOrderTime();
        this.totalAmount = ship.getTotalAmount();
        this.otherAmount = ship.getOtherAmount();
        this.sumAmount = ship.getSumAmount();
        this.goodsNum = ship.getGoodsNum();
    }
}
