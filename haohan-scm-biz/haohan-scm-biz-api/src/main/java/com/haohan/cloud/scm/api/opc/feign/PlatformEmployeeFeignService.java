/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.PlatformEmployee;
import com.haohan.cloud.scm.api.opc.req.PlatformEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 平台员工管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PlatformEmployeeFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface PlatformEmployeeFeignService {


    /**
     * 通过id查询平台员工管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PlatformEmployee/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 平台员工管理 列表信息
     * @param platformEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PlatformEmployee/fetchPlatformEmployeePage")
    R getPlatformEmployeePage(@RequestBody PlatformEmployeeReq platformEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 平台员工管理 列表信息
     * @param platformEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PlatformEmployee/fetchPlatformEmployeeList")
    R getPlatformEmployeeList(@RequestBody PlatformEmployeeReq platformEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增平台员工管理
     * @param platformEmployee 平台员工管理
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/add")
    R save(@RequestBody PlatformEmployee platformEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改平台员工管理
     * @param platformEmployee 平台员工管理
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/update")
    R updateById(@RequestBody PlatformEmployee platformEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除平台员工管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PlatformEmployee/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param platformEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/countByPlatformEmployeeReq")
    R countByPlatformEmployeeReq(@RequestBody PlatformEmployeeReq platformEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param platformEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/getOneByPlatformEmployeeReq")
    R getOneByPlatformEmployeeReq(@RequestBody PlatformEmployeeReq platformEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param platformEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PlatformEmployee/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PlatformEmployee> platformEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);


}
