package com.haohan.cloud.scm.api.opc.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author：cx 当库存不满足时采购发送的请求
 * 2019-6-3
 */
@Data
@ApiModel(description = "库存不满足时  运营 发起需求采购")
public class NotSatisfiedReq {
    /**
     * 平台商家ID
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID",required = true)
    private String pmId;
    /**
     * 汇总单ID
     */
    @NotBlank(message = "summaryOrderId不能为空")
    @ApiModelProperty(value = "汇总单ID",required = true)
    private String summaryOrderId;
    /**
     * 需求采购商品信息
     */
    @NotEmpty(message = "purchaseInfo不能为空")
    @ApiModelProperty(value = "需求采购商品信息",required = true)
    private List<FormulaReq> purchaseInfo;
    /**
     * 发起人Id
     */
    @ApiModelProperty(value = "发起人Id")
    private String initiatorId;
}
