package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.purchase.vo.PurchaseEmployeeGoodsVO;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseEmployeeGoodsResp {
    List<PurchaseEmployeeGoodsVO> list;
    private long size;
    private long current;
    private long total;
    private long pages;
}
