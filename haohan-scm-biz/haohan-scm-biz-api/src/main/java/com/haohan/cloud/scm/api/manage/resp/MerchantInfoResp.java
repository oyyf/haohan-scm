package com.haohan.cloud.scm.api.manage.resp;

import lombok.Data;

/**
 * @author cx
 * @date 2019/8/14
 */

@Data
public class MerchantInfoResp {

    private String pdsUrl = "/pds";

    private String pmId ;

    private String shopId;

    private String pmName;

    private Boolean isSelfPm = false;

    private Boolean isPds = false;
}
