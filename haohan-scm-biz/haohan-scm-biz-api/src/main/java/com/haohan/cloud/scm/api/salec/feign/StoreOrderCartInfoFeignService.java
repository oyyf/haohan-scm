/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreOrderCartInfo;
import com.haohan.cloud.scm.api.salec.req.StoreOrderCartInfoReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 订单购物详情表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreOrderCartInfoFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface StoreOrderCartInfoFeignService {


    /**
     * 通过id查询订单购物详情表
     * @param oid id
     * @return R
     */
    @RequestMapping(value = "/api/feign/StoreOrderCartInfo/{oid}", method = RequestMethod.GET)
    R<StoreOrderCartInfo> getById(@PathVariable("oid") Integer oid, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 订单购物详情表 列表信息
     * @param storeOrderCartInfoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/fetchStoreOrderCartInfoPage")
    R<Page<StoreOrderCartInfo>> getStoreOrderCartInfoPage(@RequestBody StoreOrderCartInfoReq storeOrderCartInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 订单购物详情表 列表信息
     * @param storeOrderCartInfoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/fetchStoreOrderCartInfoList")
    R<List<StoreOrderCartInfo>> getStoreOrderCartInfoList(@RequestBody StoreOrderCartInfoReq storeOrderCartInfoReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增订单购物详情表
     * @param storeOrderCartInfo 订单购物详情表
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/add")
    R<Boolean> save(@RequestBody StoreOrderCartInfo storeOrderCartInfo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改订单购物详情表
     * @param storeOrderCartInfo 订单购物详情表
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/update")
    R<Boolean> updateById(@RequestBody StoreOrderCartInfo storeOrderCartInfo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除订单购物详情表
     * @param oid id
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/delete/{oid}")
    R<Boolean> removeById(@PathVariable("oid") Integer oid, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/StoreOrderCartInfo/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/listByIds")
    R<List<StoreOrderCartInfo>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param storeOrderCartInfoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/countByStoreOrderCartInfoReq")
    R<Integer> countByStoreOrderCartInfoReq(@RequestBody StoreOrderCartInfoReq storeOrderCartInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param storeOrderCartInfoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/getOneByStoreOrderCartInfoReq")
    R<StoreOrderCartInfo> getOneByStoreOrderCartInfoReq(@RequestBody StoreOrderCartInfoReq storeOrderCartInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param storeOrderCartInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/StoreOrderCartInfo/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreOrderCartInfo> storeOrderCartInfoList, @RequestHeader(SecurityConstants.FROM) String from);


}
