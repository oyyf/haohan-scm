package com.haohan.cloud.scm.api.common.entity.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/6/25
 */
@Data
@ApiModel(description = "图片上传返回")
public class UploadPhotoResp {

    @ApiModelProperty(value = "图片地址")
    private String photoUrl;
    @ApiModelProperty(value = "图片资源id")
    private String photoGalleryId;

}
