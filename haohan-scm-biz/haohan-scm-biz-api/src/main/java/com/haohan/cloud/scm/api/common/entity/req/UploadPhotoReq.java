package com.haohan.cloud.scm.api.common.entity.req;

import com.haohan.cloud.scm.api.constant.enums.manage.PhotoTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/6/25
 */
@Data
@ApiModel(description = "图片上传请求参数")
public class UploadPhotoReq {

    @NotNull(message = "图片文件file不能为空")
    @ApiModelProperty(value = "图片文件", required = true)
    private MultipartFile file;

    @NotNull(message = "图片类型不能为空")
    @ApiModelProperty(value = "图片类型", required = true)
    private PhotoTypeEnum type;

}
