package com.haohan.cloud.scm.api.saleb.trans;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.bill.dto.OrderDetailDTO;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DynamicTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;
import com.haohan.cloud.scm.api.crm.req.CustomerDynamicStateReq;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.BuyDetailModifyReq;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/5/28
 */
@Data
public class SalebTrans {

    public static SummaryOrder transToSummaryOrder(BuyOrderDetailDTO detailDTO, SummaryOrder summaryOrder) {
        summaryOrder.setGoodsModelId(detailDTO.getGoodsModelId());
        summaryOrder.setBuySeq(BuySeqEnum.getByType(detailDTO.getBuySeq()));
        summaryOrder.setDeliveryTime(detailDTO.getDeliveryTime());
        summaryOrder.setNeedBuyNum(detailDTO.getGoodsNum());
        return summaryOrder;
    }

    public static BuyOrderDetailDTO transToBuyOrderDetailDTO(BuyOrderDetailDTO detailDTO, SalebSummaryGoodsNumReq req) {
        detailDTO.setPmId(req.getPmId());
        detailDTO.setBuySeq(req.getBuySeq());
        detailDTO.setDeliveryTime(req.getDeliveryTime());
        detailDTO.setSummaryFlag(req.getSummaryFlag());
        detailDTO.setStatus(req.getStatus());
        detailDTO.setGoodsModelId(req.getGoodsModelId());
        return detailDTO;
    }

    /**
     * (迁移后废弃)
     * @param detailReq
     * @param model
     * @param buyOrder
     * @return
     */
    public static BuyOrderDetail initOrderDetail(BuyDetailModifyReq detailReq, GoodsModelDTO model, BuyOrder buyOrder) {
        BuyOrderDetail detail = new BuyOrderDetail();
        if (null != buyOrder) {
            detail.setPmId(buyOrder.getPmId());
            detail.setBuyId(buyOrder.getBuyId());
            detail.setBuyerId(buyOrder.getBuyerId());
        }
        detail.setGoodsModelId(model.getId());
        detail.setGoodsImg(model.getModelUrl());
        detail.setGoodsName(model.getGoodsName());
        detail.setGoodsModel(model.getModelName());
        detail.setMarketPrice(model.getModelPrice());
        detail.setUnit(model.getModelUnit());

        detail.setGoodsNum(detailReq.getGoodsNum());
        detail.setBuyPrice(detailReq.getBuyPrice());
        detail.setOrderGoodsNum(detailReq.getGoodsNum());
        detail.setRemarks(detailReq.getRemarks());

        detail.setStatus(BuyOrderStatusEnum.submit);
        detail.setSummaryFlag(DetailSummaryFlagEnum.wait);
        return detail;
    }

    public static BuyOrderDetail initOrderDetail(BuyDetailModifyReq detailReq, GoodsModelVO model) {
        BuyOrderDetail detail = new BuyOrderDetail();
        detail.setGoodsModelId(model.getModelId());
        detail.setGoodsImg(model.getModelUrl());
        detail.setGoodsName(model.getGoodsName());
        detail.setGoodsModel(model.getModelName());
        detail.setMarketPrice(model.getModelPrice());
        detail.setUnit(model.getModelUnit());

        detail.setGoodsNum(detailReq.getGoodsNum());
        detail.setBuyPrice(detailReq.getBuyPrice());
        detail.setOrderGoodsNum(detailReq.getGoodsNum());
        detail.setRemarks(detailReq.getRemarks());

        detail.setStatus(BuyOrderStatusEnum.submit);
        detail.setSummaryFlag(DetailSummaryFlagEnum.wait);
        return detail;
    }

    public static OrderInfoDTO transToOrderInfo(BuyOrder order, List<BuyOrderDetail> detailList) {
        // 缺 pmName、merchantId、merchantName
        OrderInfoDTO orderInfo = new OrderInfoDTO();
        orderInfo.setPmId(order.getPmId());
        orderInfo.setOrderSn(order.getBuyId());
        orderInfo.setOrderType(OrderTypeEnum.buy);
        orderInfo.setPayStatus(null == order.getPayStatus() ? PayStatusEnum.wait : order.getPayStatus());
        // 订单状态
        orderInfo.setOrderStatus(statusTrans(order.getStatus()));
        orderInfo.setCustomerId(order.getBuyerId());
        orderInfo.setCustomerName(order.getBuyerName());
        orderInfo.setContact(order.getContact());
        orderInfo.setTelephone(order.getTelephone());
        orderInfo.setAddress(order.getAddress());
        orderInfo.setDealDate(order.getDeliveryTime());
        orderInfo.setOrderTime(order.getBuyTime());
        BigDecimal total = order.getTotalPrice();
        if (null == total) {
            total = null == order.getGenPrice() ? BigDecimal.ZERO : order.getGenPrice();
        }
        BigDecimal other = null == order.getShipFee() ? BigDecimal.ZERO : order.getShipFee();
        orderInfo.setTotalAmount(total);
        orderInfo.setSumAmount(total.subtract(other));
        orderInfo.setOtherAmount(other);
        orderInfo.setGoodsNum(detailList.size());
        orderInfo.setRemarks(order.getRemarks());
        // 明细
        orderInfo.setDetailList(detailList.stream()
                .filter(item-> item.getStatus() != BuyOrderStatusEnum.cancel)
                .map(item -> {
                    OrderDetailDTO detail = new OrderDetailDTO();
                    detail.setDetailSn(item.getBuyDetailSn());
                    // 缺 goodsId
                    detail.setGoodsModelId(item.getGoodsModelId());
                    detail.setGoodsName(item.getGoodsName());
                    detail.setModelName(item.getGoodsModel());
                    detail.setUnit(item.getUnit());
                    detail.setGoodsImg(item.getGoodsImg());
                    detail.setMarketPrice(item.getMarketPrice());
                    detail.setDealPrice(item.getBuyPrice());
                    detail.setGoodsNum(item.getGoodsNum());
                    detail.setAmount(item.getBuyPrice().multiply(item.getGoodsNum()));
                    return detail;
                }).collect(Collectors.toList()));
        return orderInfo;
    }

    /**
     * 采购单状态转换订单状态
     *
     * @param status
     * @return
     */
    public static OrderStatusEnum statusTrans(BuyOrderStatusEnum status) {
        OrderStatusEnum result;
        switch (status) {
            case submit:
                result = OrderStatusEnum.submit;
                break;
            case success:
                result = OrderStatusEnum.success;
                break;
            case cancel:
                result = OrderStatusEnum.cancel;
                break;
            case wait:
            case arrive:
            case delivery:
                result = OrderStatusEnum.wait;
                break;
            default:
                throw new ErrorDataException("未定义采购单状态转换订单状态时错误");
        }
        return result;
    }

    private static Set<BuyOrderStatusEnum> buyOrderStatusEnumSet;

    /**
     * 获取采购订单对应订单状态wait的值
     *
     * @return
     */
    public static Set<BuyOrderStatusEnum> waitStatus() {
        if (null == buyOrderStatusEnumSet) {
            buyOrderStatusEnumSet = new HashSet<>(8);
            buyOrderStatusEnumSet.add(BuyOrderStatusEnum.wait);
            buyOrderStatusEnumSet.add(BuyOrderStatusEnum.delivery);
            buyOrderStatusEnumSet.add(BuyOrderStatusEnum.arrive);
        }
        return buyOrderStatusEnumSet;
    }

    public static BuyOrderStatusEnum statusTrans(OrderStatusEnum status) {
        BuyOrderStatusEnum result;
        switch (status) {
            case submit:
                result = BuyOrderStatusEnum.submit;
                break;
            case success:
                result = BuyOrderStatusEnum.success;
                break;
            case cancel:
                result = BuyOrderStatusEnum.cancel;
                break;
            case wait:
                // 需要单独处理
                result = BuyOrderStatusEnum.wait;
                break;
            default:
                throw new ErrorDataException("未定义采购单状态转换订单状态时错误");
        }
        return result;
    }

    public static CustomerDynamicStateReq transToDynamic(BuyOrder order, Buyer buyer, boolean addFlag) {
        CustomerDynamicStateReq req = new CustomerDynamicStateReq();
        req.setMerchantId(buyer.getMerchantId());
        req.setSourceSn(order.getBuyId());
        req.setStateType(DynamicTypeEnum.buy);
        String edit = addFlag ? "新增" : "修改";
        String content = StrUtil.format("{}{}:客户采购{}个品种, 总金额{}元", edit, req.getSourceSn(), order.getGoodsNum(), order.getTotalPrice());
        req.setContent(content);
        req.setTelephone(order.getTelephone());
        req.setReportMan(order.getContact());
        return req;
    }
}
