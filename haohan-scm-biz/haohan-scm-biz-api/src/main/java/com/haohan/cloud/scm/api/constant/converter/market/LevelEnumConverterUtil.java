package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.LevelEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class LevelEnumConverterUtil implements Converter<LevelEnum> {
    @Override
    public LevelEnum convert(Object o, LevelEnum customerLevelEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return LevelEnum.getByType(o.toString());
    }
}
