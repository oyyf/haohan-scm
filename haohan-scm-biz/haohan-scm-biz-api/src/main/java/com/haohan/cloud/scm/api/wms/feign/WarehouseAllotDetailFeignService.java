/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import com.haohan.cloud.scm.api.wms.req.WarehouseAllotDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 库存调拨明细记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarehouseAllotDetailFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WarehouseAllotDetailFeignService {


    /**
     * 通过id查询库存调拨明细记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarehouseAllotDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 库存调拨明细记录 列表信息
     * @param warehouseAllotDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/fetchWarehouseAllotDetailPage")
    R getWarehouseAllotDetailPage(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 库存调拨明细记录 列表信息
     * @param warehouseAllotDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/fetchWarehouseAllotDetailList")
    R getWarehouseAllotDetailList(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/add")
    R save(@RequestBody WarehouseAllotDetail warehouseAllotDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/update")
    R updateById(@RequestBody WarehouseAllotDetail warehouseAllotDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除库存调拨明细记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarehouseAllotDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/countByWarehouseAllotDetailReq")
    R countByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/getOneByWarehouseAllotDetailReq")
    R getOneByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warehouseAllotDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarehouseAllotDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarehouseAllotDetail> warehouseAllotDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
