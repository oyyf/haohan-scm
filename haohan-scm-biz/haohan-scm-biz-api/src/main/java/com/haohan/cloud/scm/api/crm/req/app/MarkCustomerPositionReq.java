package com.haohan.cloud.scm.api.crm.req.app;

import cn.hutool.http.HtmlUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2019/11/15
 */
@Data
@Api("客户定位标注")
public class MarkCustomerPositionReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 32, message = "员工id的长度最大32字符")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @NotBlank(message = "客户编号不能为空")
    @ApiModelProperty(value = "客户编号")
    @Length(max = 32, message = "客户编号的长度最大32字符")
    private String customerSn;

    @NotBlank(message = "客户地址定位不能为空")
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "客户地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "客户地址定位 (经度，纬度)")
    private String position;

    @NotBlank(message = "省不能为空")
    @Length(max = 20, message = "省的长度最大20字符")
    @ApiModelProperty(value = "省")
    private String province;

    @NotBlank(message = "市不能为空")
    @Length(max = 20, message = "市的长度最大20字符")
    @ApiModelProperty(value = "市")
    private String city;

    @Length(max = 20, message = "区的长度最大20字符")
    @ApiModelProperty(value = "区")
    private String district;

    @Length(max = 20, message = "街道、乡镇的长度最大20字符")
    @ApiModelProperty(value = "街道、乡镇")
    private String street;

    @Length(max = 64, message = "详细地址的长度最大64字符")
    @ApiModelProperty(value = "详细地址")
    private String address;

    public Customer transTo() {
        Customer customer = new Customer();
        customer.setCustomerSn(this.customerSn);
        customer.setPosition(this.position);
        customer.setProvince(this.province);
        customer.setCity(this.city);
        customer.setDistrict(this.district);
        customer.setStreet(this.street);
        // 地址中字符转义
        customer.setAddress(HtmlUtil.unescape(this.address));
        return customer;
    }
}
