package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.MarketManageImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/19
 */
@Data
public class MarketManageImportReq {

    @NotEmpty(message = "市场列表不能为空")
    private List<MarketManageImport> marketList;

}
