/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreOrderCartInfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 订单购物详情表
 *
 * @author haohan
 * @date 2019-07-15 09:04:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "订单购物详情表")
public class StoreOrderCartInfoReq extends StoreOrderCartInfo {

    private long pageSize;
    private long pageNo;




}
