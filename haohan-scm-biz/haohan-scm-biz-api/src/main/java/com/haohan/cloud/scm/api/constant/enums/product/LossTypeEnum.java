package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum LossTypeEnum implements IBaseEnum {
  purchase("1","采购"),
  handle("2","生产加工"),
  select("3","分拣"),
  delivery("4","配送"),
  storage("5","仓储盘点"),
  adjust("6","拨调"),
  loss("7","总损耗");

    private static final Map<String, LossTypeEnum> MAP = new HashMap<>(8);

    static {
        for (LossTypeEnum e : LossTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static LossTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;

}
