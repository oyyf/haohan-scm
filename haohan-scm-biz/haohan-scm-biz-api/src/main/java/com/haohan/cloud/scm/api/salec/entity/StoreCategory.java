/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

/**
 * 商品分类表
 *
 * @author haohan
 * @date 2020-05-21 18:45:09
 */
@Data
@TableName("eb_store_category")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品分类表")
public class StoreCategory extends Model<StoreCategory> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "商品分类表ID")
    @TableId(type = IdType.INPUT)
    private Integer id;

    @ApiModelProperty(value = "父id")
    private Integer pid;

    @Length(max = 100, message = "分类名称长度最大100字符")
    @ApiModelProperty(value = "分类名称")
    private String cateName;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @Length(max = 128, message = "图标长度最大128字符")
    @ApiModelProperty(value = "图标")
    private String pic;

    @ApiModelProperty(value = "是否显示", notes = "小程序")
    private Integer isShow;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
