package com.haohan.cloud.scm.api.message.dto;

import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MessageTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2020/1/14
 * 发送消息
 */
@Data
public class SendInMailMessageDTO {

    /**
     * 消息标题(消息简述)
     */
    private String title;
    /**
     * 消息内容
     */
    private String content;
    /**
     * 站内信类型:1公告2及时通信
     */
    private InMailTypeEnum inMailType;

    /**
     * 发送人uid
     */
    private String senderUid;
    /**
     * 发送人名称
     */
    private String senderName;
    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    private DepartmentTypeEnum departmentType;
    /**
     * 业务类型:各部门的消息
     */
    private MsgActionTypeEnum msgActionType;

    /**
     * 请求参数  (json 详情查询 编号 querySn, 解析对应MsgInfoVo的actionSn)
     */
    private String reqParams;

    public InMailRecord transToInMail(LocalDateTime sendTime) {
        InMailRecord msg = new InMailRecord();
        msg.setTitle(this.title);
        msg.setContent(this.content);
        msg.setInMailType(this.inMailType);
        msg.setSenderUid(this.senderUid);
        msg.setSenderName(this.senderName);
        msg.setDepartmentType(this.departmentType);
        msg.setMsgActionType(this.msgActionType);
        msg.setReqParams(this.reqParams);
        msg.setSendTime(sendTime);
        return msg;
    }

    public MessageRecord transToMsg(LocalDateTime sendTime) {
        MessageRecord msg = new MessageRecord();
        msg.setMessageType(MessageTypeEnum.inMail);
        msg.setDepartmentType(this.departmentType);
        msg.setMsgActionType(this.msgActionType);

        msg.setSenderUid(this.senderUid);
        msg.setSenderName(this.senderName);
        msg.setSendTime(sendTime);
        msg.setTitle(this.title);
        return msg;
    }
}
