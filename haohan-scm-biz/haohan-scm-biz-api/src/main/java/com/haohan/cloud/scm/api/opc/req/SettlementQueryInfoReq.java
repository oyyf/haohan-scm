package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/9/11
 */
@Data
@ApiModel(description = "查询结算详情")
public class SettlementQueryInfoReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "settlementSn不能为空")
    @ApiModelProperty(value = "结算记录编号", required = true)
    private String settlementSn;

    public SettlementRecord transTo() {
        SettlementRecord settlementRecord = new SettlementRecord();
        settlementRecord.setPmId(this.pmId);
        settlementRecord.setSettlementSn(this.settlementSn);
        return  settlementRecord;
    }
}
