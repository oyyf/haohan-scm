/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.DynamicTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 客户业务动态
 *
 * @author haohan
 * @date 2019-08-30 12:00:17
 */
@Data
@TableName("crm_customer_dynamic_state")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户业务动态")
public class CustomerDynamicState extends Model<CustomerDynamicState> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 动态编号
     */
    @ApiModelProperty(value = "动态编号")
    private String stateSn;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 来源编号
     */
    @ApiModelProperty(value = "来源编号")
    private String sourceSn;
    /**
     * 动态类型 (1.销售订单 2.销量上报 3.库存上报 )
     */
    @ApiModelProperty(value = "动态类型")
    private DynamicTypeEnum stateType;
    /**
     * 内容
     */
    @ApiModelProperty(value = "内容")
    private String content;
    /**
     * 联系电话 上报人
     */
    @ApiModelProperty(value = "联系电话")
    private String telephone;
    /**
     * 业务上报人名称
     */
    @ApiModelProperty(value = "业务上报人名称")
    private String reportMan;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
