package com.haohan.cloud.scm.api.purchase.resp;

import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseQueryLendingListResp {
    private List list;
    private long size;
    private long current;
    private long total;
    private long pages;
}
