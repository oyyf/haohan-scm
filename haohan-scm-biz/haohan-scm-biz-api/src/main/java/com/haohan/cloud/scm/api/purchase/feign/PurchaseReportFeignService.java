/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAnswerPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseReportReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购汇报内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseReportFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseReportFeignService {


    /**
     * 通过id查询采购汇报
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseReport/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购汇报 列表信息
     * @param purchaseReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseReport/fetchPurchaseReportPage")
    R getPurchaseReportPage(@RequestBody PurchaseReportReq purchaseReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购汇报 列表信息
     * @param purchaseReportReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseReport/fetchPurchaseReportList")
    R getPurchaseReportList(@RequestBody PurchaseReportReq purchaseReportReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购汇报
     * @param purchaseReport 采购汇报
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/add")
    R save(@RequestBody PurchaseReport purchaseReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购汇报
     * @param purchaseReport 采购汇报
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/update")
    R updateById(@RequestBody PurchaseReport purchaseReport, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购汇报
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseReport/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/countByPurchaseReportReq")
    R countByPurchaseReportReq(@RequestBody PurchaseReportReq purchaseReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseReportReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/getOneByPurchaseReportReq")
    R getOneByPurchaseReportReq(@RequestBody PurchaseReportReq purchaseReportReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseReportList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseReport/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseReport> purchaseReportList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改 采购汇报
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseReport/modifyPurchaseReport")
    R modifyPurchaseReport(@RequestBody PurchaseModifyPurchaseReportReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 回复采购汇报
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/PurchaseReport/answerPurchaseReport")
    R answerPurchaseReport(@RequestBody PurchaseAnswerPurchaseReportReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
