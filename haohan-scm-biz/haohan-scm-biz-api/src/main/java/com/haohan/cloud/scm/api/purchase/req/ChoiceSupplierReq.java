package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/5/28.
 */
@Data
@ApiModel("确认供应商")
public class ChoiceSupplierReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

//    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id" , required = true)
    private String uId;

    @ApiModelProperty(value = "采购明细id")
    private String id;

    @ApiModelProperty(value = "采购明细编号")
    private String purchaseDetailSn;

    @NotBlank(message = "offerOrderId不能为空")
    @ApiModelProperty(value = "报价单编号", required = true)
    private String offerOrderId;


}
