/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.UserOpenPlatform;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 第三方开放平台用户管理
 *
 * @author haohan
 * @date 2019-05-28 20:40:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "第三方开放平台用户管理")
public class UserOpenPlatformReq extends UserOpenPlatform {

    private long pageSize;
    private long pageNo;




}
