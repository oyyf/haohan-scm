package com.haohan.cloud.scm.api.manage.dto;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.vo.PhotoVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("扩展店铺属性")
@NoArgsConstructor
public class ShopExtDTO extends Shop {

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @ApiModelProperty(value = "轮播图列表")
    private List<PhotoVO> photoList;

    @ApiModelProperty(value = "店铺收款码列表")
    private List<PhotoVO> payCodeList;

    @ApiModelProperty(value = "店铺二维码列表")
    private List<PhotoVO> qrcodeList;

    @ApiModelProperty(value = "店铺Logo列表")
    private List<PhotoVO> shopLogoList;

    public ShopExtDTO(Shop shop) {
        BeanUtil.copyProperties(shop, this);
    }

}
