/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.TreeRelation;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 部门关系表
 *
 * @author haohan
 * @date 2019-08-30 11:44:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "部门关系表")
public class TreeRelationReq extends TreeRelation {

    private long pageSize;
    private long pageNo;


}
