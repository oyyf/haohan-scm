package com.haohan.cloud.scm.api.saleb.req;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/10/9
 */
@Data
public class BuyDetailModifyReq {

    @NotBlank(message = "商品规格id不能为空")
    @Length(min = 0, max = 32, message = "商品规格id长度在0至32之间")
    private String goodsModelId;

    @NotNull(message = "采购数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "采购数量的整数位最大8位, 小数位最大2位")
    private BigDecimal goodsNum;

    @Digits(integer = 8, fraction = 2, message = "采购价格的整数位最大8位, 小数位最大2位")
    private BigDecimal buyPrice;

    @Length(min = 0, max = 100, message = "商品备注长度在0至100之间")
    private String remarks;

}
