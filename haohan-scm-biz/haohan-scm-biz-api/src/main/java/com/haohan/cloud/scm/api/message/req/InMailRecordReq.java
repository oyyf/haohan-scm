/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-28 20:03:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信记录表")
public class InMailRecordReq extends InMailRecord {

    private long pageSize;
    private long pageNo;




}
