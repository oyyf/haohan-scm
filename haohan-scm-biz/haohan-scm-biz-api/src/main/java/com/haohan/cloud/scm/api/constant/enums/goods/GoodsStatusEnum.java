package com.haohan.cloud.scm.api.constant.enums.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/8
 */
@Getter
@AllArgsConstructor
public enum GoodsStatusEnum implements IBaseEnum {

    /**
     * 商品状态: 0 出售中 1 仓库中 2 已售罄
     */
    sale("0", "出售中"),
    stock("1", "仓库中"),
    takeout("2", "已售罄");

    private static final Map<String, GoodsStatusEnum> MAP = new HashMap<>(8);

    static {
        for (GoodsStatusEnum e : GoodsStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static GoodsStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
