/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.WorkReportTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 工作汇报
 *
 * @author haohan
 * @date 2019-08-30 12:01:58
 */
@Data
@TableName("crm_work_report")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "工作汇报")
public class WorkReport extends Model<WorkReport> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 汇报编号
     */
    @ApiModelProperty(value = "汇报编号")
    private String reportSn;
    /**
     * 工作汇报类型: 1.日常 2.销量 3.库存
     */
    @ApiModelProperty(value = "工作汇报类型: 1.日常 2.销量 3.库存")
    private WorkReportTypeEnum reportType;
    /**
     * 汇报内容
     */
    @ApiModelProperty(value = "汇报内容")
    private String reportContent;
    /**
     * 汇报位置 定位(经纬度)
     */
    @ApiModelProperty(value = "汇报位置")
    private String reportLocation;

    @Length(max = 64, message = "汇报地址的长度最大64字符")
    @ApiModelProperty(value = "汇报地址")
    private String reportAddress;
    /**
     * 汇报人id(员工)
     */
    @ApiModelProperty(value = "汇报人")
    private String reportManId;
    /**
     * 汇报人名称(员工)
     */
    @ApiModelProperty(value = "汇报人")
    private String reportMan;
    /**
     * 汇报时间
     */
    @ApiModelProperty(value = "汇报时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reportTime;
    /**
     * 汇报人手机号
     */
    @ApiModelProperty(value = "汇报人手机号")
    private String reportTel;
    /**
     * 汇报状态1.已汇报2.已反馈3.已处理 report_status
     */
    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private ReportStatusEnum status;

    @Length(max = 255, message = "反馈信息的长度最大255字符")
    @ApiModelProperty(value = "反馈信息")
    private String feedback;

    @Length(max = 64, message = "图片组编号的长度最大64字符")
    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息 (json存放员工日常统计数据 EmployeeDailyCountVO)
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
