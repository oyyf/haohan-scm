package com.haohan.cloud.scm.api.bill.req;

import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @author dy
 * @date 2019/12/28
 * SingleGroup  settlementSn必须
 */
@Data
public class SettlementReq {

    @NotNull(message = "结算单编号", groups = {SingleGroup.class})
    @Length(max = 32, message = "结算单编号最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @ApiModelProperty(value = "结算状态")
    private YesNoEnum settlementStatus;

    @ApiModelProperty(value = "付款方式:1对公转账2现金支付3在线支付4承兑汇票")
    private PayTypeEnum payType;

    @Length(max = 32, message = "平台商家ID最大长度32字符")
    @ApiModelProperty(value = "平台商家ID")
    private String pmId;

    @Length(max = 32, message = "结算公司id最大长度32字符")
    @ApiModelProperty(value = "结算公司id")
    private String companyId;

    @Length(max = 32, message = "来源订单编号最大长度32字符")
    @ApiModelProperty(value = "来源订单编号")
    private String orderSn;

    @Length(max = 32, message = "账单编号最大长度32字符")
    @ApiModelProperty(value = "账单编号(应收、应付)")
    private String billSn;

    /**
     * 结算类型: 应收/应付
     */
    @ApiModelProperty(value = "结算类型(应收、应付)")
    private SettlementTypeEnum settlementType;

    /**
     * 结算单类型: 1.普通, 2.汇总, 3.明细
     */
    @ApiModelProperty(value = "结算单类型: 1.普通, 2.汇总, 3.明细")
    private SettlementStyleEnum settlementStyle;

    /**
     * 汇总结算单编号
     */
    @Length(max = 32, message = "汇总结算单编号最大长度32字符")
    @ApiModelProperty(value = "汇总结算单编号")
    private String summarySettlementSn;

    // 非eq查询条件

    @Length(max = 32, message = "平台商家名称最大长度32字符")
    @ApiModelProperty(value = "平台商家名称")
    private String pmName;

    @Length(max = 32, message = "结算公司名称最大长度32字符")
    @ApiModelProperty(value = "结算公司名称")
    private String companyName;

    @Length(max = 32, message = "结款人名称的最大长度为32字符")
    @ApiModelProperty(value = "结款人名称")
    private String companyOperator;

    @Length(max = 255, message = "结算说明的最大长度为255字符")
    @ApiModelProperty(value = "结算说明")
    private String settlementDesc;

    @Length(max = 32, message = "经办人名称的最大长度为32字符")
    @ApiModelProperty(value = "经办人名称")
    private String operatorName;


    @ApiModelProperty(value = "结算日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "结算日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    /**
     * 结算金额
     */
    private BigDecimal settlementAmount;

    // feign使用

    @ApiModelProperty(value = "结算公司id 集合", notes = "采购商id、供应商id、客户sn 对应商家")
    private Set<String> companyIdSet;

    @ApiModelProperty(value = "账单编号列表")
    private List<String> billSnList;

    public SettlementAccount transTo() {
        SettlementAccount account = new SettlementAccount();
        account.setSettlementSn(this.settlementSn);
        account.setSettlementStatus(this.settlementStatus);
        account.setPayType(this.payType);
        account.setPmId(this.pmId);
        account.setCompanyId(this.companyId);
        account.setOrderSn(this.orderSn);
        account.setBillSn(this.billSn);
        account.setSettlementType(this.settlementType);
        account.setSettlementStyle(this.settlementStyle);
        account.setSummarySettlementSn(this.summarySettlementSn);
        return account;
    }
}
