/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.AreaRelation;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 区域关联关系
 *
 * @author haohan
 * @date 2019-12-21 15:35:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "区域关联关系")
public class AreaRelationReq extends AreaRelation {

    private long pageSize;
    private long pageNo;

}
