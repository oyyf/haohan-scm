/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-28 19:08:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "出库单明细")
public class ExitWarehouseDetailReq extends ExitWarehouseDetail {

    private long pageSize;
    private long pageNo;




}
