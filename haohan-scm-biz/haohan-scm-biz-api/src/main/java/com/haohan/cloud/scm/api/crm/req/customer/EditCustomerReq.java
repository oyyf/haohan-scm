package com.haohan.cloud.scm.api.crm.req.customer;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CompanyNatureEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.Customer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/23
 */
@Data
public class EditCustomerReq {

    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;

    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @NotBlank(message = "客户名称不能为空")
    @ApiModelProperty(value = "客户名称", required = true)
    @Length(min = 0, max = 32, message = "客户名称长度在0至32之间")
    private String customerName;
    /**
     * 销售区域编号
     */
    @NotBlank(message = "销售区域不能为空")
    @ApiModelProperty(value = "销售区域")
    @Length(min = 0, max = 32, message = "销售区域编号长度在0至32之间")
    private String areaSn;

    @Length(min = 0, max = 32, message = "市场编码长度在0至32之间")
    @ApiModelProperty(value = "市场编码")
    private String marketSn;
    /**
     * 客户联系人名称
     */
    @NotBlank(message = "联系人姓名不能为空")
    @ApiModelProperty(value = "客户联系人")
    @Length(min = 0, max = 32, message = "客户联系人名称长度在0至32之间")
    private String contact;
    /**
     * 联系人手机
     */
    @NotBlank(message = "联系人手机不能为空")
    @ApiModelProperty(value = "联系人手机")
    @Length(min = 0, max = 32, message = "联系人手机长度在0至32之间")
    private String telephone;
    /**
     * 座机电话
     */
    @ApiModelProperty(value = "座机电话")
    @Length(min = 0, max = 32, message = "座机电话长度在0至32之间")
    private String phoneNumber;
    /**
     * 省
     */
    @NotBlank(message = "省不能为空")
    @ApiModelProperty(value = "省")
    @Length(min = 0, max = 32, message = "省长度在0至32之间")
    private String province;
    /**
     * 市
     */
    @NotBlank(message = "市不能为空")
    @ApiModelProperty(value = "市")
    @Length(min = 0, max = 32, message = "市长度在0至32之间")
    private String city;
    /**
     * 区 (某些市下未划分区)
     */
    @ApiModelProperty(value = "区")
    @Length(min = 0, max = 32, message = "区长度在0至32之间")
    private String district;
    /**
     * 街道、乡镇
     */
    @ApiModelProperty(value = "街道、乡镇")
    @Length(min = 0, max = 32, message = "街道、乡镇长度在0至32之间")
    private String street;
    /**
     * 详细地址
     */
    @NotBlank(message = "详细地址不能为空")
    @ApiModelProperty(value = "详细地址")
    @Length(min = 0, max = 64, message = "详细地址长度在0至64之间")
    private String address;
    /**
     * 客户地址定位 (经度，纬度)
     */
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "位置定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "客户地址定位 (经度，纬度)")
    private String position;
    /**
     * 客户标签
     */
    @ApiModelProperty(value = "客户标签")
    @Length(min = 0, max = 64, message = "客户标签长度在0至64之间")
    private String tags;
    /**
     * 邮编
     */
    @ApiModelProperty(value = "邮编")
    @Length(min = 0, max = 32, message = "邮编长度在0至32之间")
    private String postcode;
    /**
     * 传真
     */
    @ApiModelProperty(value = "传真")
    @Length(min = 0, max = 32, message = "传真长度在0至32之间")
    private String fax;
    /**
     * 网址
     */
    @ApiModelProperty(value = "网址")
    @Length(min = 0, max = 64, message = "网址长度在0至64之间")
    private String website;
    /**
     * 营业时间描述
     */
    @ApiModelProperty(value = "营业时间描述")
    @Length(min = 0, max = 64, message = "营业时间描述长度在0至64之间")
    private String serviceTime;
    /**
     * 门头照图片组编号
     */
    @ApiModelProperty(value = "门头照图片组编号")
    @Length(min = 0, max = 32, message = "门头照图片组编号长度在0至32之间")
    private String photoGroupNum;
    /**
     * 经营面积
     */
    @ApiModelProperty(value = "经营面积")
    @Length(min = 0, max = 64, message = "经营面积长度在0至64之间")
    private String operateArea;
    /**
     * 年经营流水
     */
    @ApiModelProperty(value = "年经营流水")
    @Length(min = 0, max = 64, message = "年经营流水长度在0至64之间")
    private String shopSale;
    /**
     * 业务介绍
     */
    @ApiModelProperty(value = "业务介绍")
    @Length(min = 0, max = 500, message = "业务介绍长度在0至500之间")
    private String bizDesc;
    /**
     * 营业执照图片地址
     */
    @ApiModelProperty(value = "营业执照编号")
    @Length(min = 0, max = 64, message = "营业执照编号长度在0至64之间")
    private String bizLicense;
    /**
     * 营业执照名称
     */
    @ApiModelProperty(value = "营业执照名称")
    @Length(min = 0, max = 64, message = "营业执照名称长度在0至64之间")
    private String licenseName;

    @Length(min = 0, max = 64, message = "营业执照图片组编号长度在0至64之间")
    @ApiModelProperty(value = "营业执照图片组编号")
    private String licenseGroupNum;
    /**
     * 工商注册号
     */
    @ApiModelProperty(value = "工商注册号")
    @Length(min = 0, max = 64, message = "工商注册号长度在0至64之间")
    private String registrationNum;
    /**
     * 注册日期
     */
    @ApiModelProperty(value = "注册日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate registrationDate;
    /**
     * 客户注册全称
     */
    @ApiModelProperty(value = "客户注册全称")
    @Length(min = 0, max = 64, message = "客户注册全称长度在0至64之间")
    private String regName;
    /**
     * 经营法人名称
     */
    @ApiModelProperty(value = "经营法人名称")
    @Length(min = 0, max = 32, message = "经营法人名称长度在0至32之间")
    private String legalName;
    /**
     * 经营地址
     */
    @ApiModelProperty(value = "经营地址")
    @Length(min = 0, max = 64, message = "经营地址长度在0至64之间")
    private String regAddress;
//    /**
//     * 收录时间
//     */
//    @ApiModelProperty(value = "收录时间")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime initTime;
    /**
     * 客户状态
     */
    @ApiModelProperty(value = "客户状态")
    private UseStatusEnum status;
    /**
     * 客户类型:1经销商2门店
     */
    @ApiModelProperty(value = "客户类型:1经销商2门店")
    private CustomerTypeEnum customerType;
    /**
     * 客户负责人id
     */
    @ApiModelProperty(value = "客户负责人id")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String directorId;
    /**
     * 公司性质:1.个体2.民营3.国有4.其他
     */
    @ApiModelProperty(value = "公司性质:1.个体2.民营3.国有4.其他")
    private CompanyNatureEnum companyNature;
    /**
     * 客户级别:1星-5星
     */
    @ApiModelProperty(value = "客户级别:1星-5星")
    private GradeTypeEnum customerLevel;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    @ApiModelProperty(value = "门头照图片列表")
    private List<String> photoList;

    @ApiModelProperty(value = "营业执照图片列表")
    private List<String> licensePhotoList;

    @ApiModelProperty(value = "客户层级父Id")
    private String parentLevelId;

    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    public Customer transTo() {
        Customer customer = new Customer();
        BeanUtil.copyProperties(this, customer);
        if (null == customer.getDirectorId()) {
            customer.setDirectorId("");
        }
        if (null == customer.getMarketSn()) {
            customer.setMarketSn("");
        }
        return customer;
    }
}
