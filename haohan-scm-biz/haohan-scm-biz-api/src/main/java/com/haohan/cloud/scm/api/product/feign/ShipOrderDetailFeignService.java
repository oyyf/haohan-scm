/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import com.haohan.cloud.scm.api.product.req.ShipOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 送货明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShipOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ShipOrderDetailFeignService {


    /**
     * 通过id查询送货明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ShipOrderDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 送货明细 列表信息
     * @param shipOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShipOrderDetail/fetchShipOrderDetailPage")
    R getShipOrderDetailPage(@RequestBody ShipOrderDetailReq shipOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 送货明细 列表信息
     * @param shipOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ShipOrderDetail/fetchShipOrderDetailList")
    R getShipOrderDetailList(@RequestBody ShipOrderDetailReq shipOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/add")
    R save(@RequestBody ShipOrderDetail shipOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/update")
    R updateById(@RequestBody ShipOrderDetail shipOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除送货明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ShipOrderDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/countByShipOrderDetailReq")
    R countByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/getOneByShipOrderDetailReq")
    R getOneByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shipOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ShipOrderDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ShipOrderDetail> shipOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
