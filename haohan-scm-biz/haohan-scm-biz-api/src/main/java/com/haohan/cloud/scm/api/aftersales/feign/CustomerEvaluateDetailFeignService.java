/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.feign;

import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;
import com.haohan.cloud.scm.api.aftersales.req.CustomerEvaluateDetailReq;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 客户服务评分明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerEvaluateDetailFeignService", value = ScmServiceName.SCM_BIZ_AFTERSALES)
public interface CustomerEvaluateDetailFeignService {


    /**
     * 通过id查询客户服务评分明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerEvaluateDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户服务评分明细 列表信息
     * @param customerEvaluateDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/fetchCustomerEvaluateDetailPage")
    R getCustomerEvaluateDetailPage(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户服务评分明细 列表信息
     * @param customerEvaluateDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/fetchCustomerEvaluateDetailList")
    R getCustomerEvaluateDetailList(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户服务评分明细
     * @param customerEvaluateDetail 客户服务评分明细
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/add")
    R save(@RequestBody CustomerEvaluateDetail customerEvaluateDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户服务评分明细
     * @param customerEvaluateDetail 客户服务评分明细
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/update")
    R updateById(@RequestBody CustomerEvaluateDetail customerEvaluateDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户服务评分明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/CustomerEvaluateDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerEvaluateDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/countByCustomerEvaluateDetailReq")
    R countByCustomerEvaluateDetailReq(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerEvaluateDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/getOneByCustomerEvaluateDetailReq")
    R getOneByCustomerEvaluateDetailReq(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerEvaluateDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerEvaluateDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<CustomerEvaluateDetail> customerEvaluateDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
