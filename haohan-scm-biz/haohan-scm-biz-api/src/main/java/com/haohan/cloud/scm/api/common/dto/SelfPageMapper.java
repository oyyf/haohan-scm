package com.haohan.cloud.scm.api.common.dto;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2020/3/30
 * 自定义分页查询 , 需查询总数和列表的sql语句
 */
public interface SelfPageMapper {

    /**
     * 查询总数 (需自定义sql语句)
     *
     * @param query
     * @param <T>
     * @return
     */
    <T extends SelfSqlDTO> Integer queryCount(T query);

    /**
     * 分页查询返回列表  (需自定义sql语句)
     *
     * @param query
     * @param <E>
     * @param <T>
     * @return
     */
    <E, T extends SelfSqlDTO> List<E> queryPage(T query);

    /**
     * 自定义分页查询
     *
     * @param query      自定义查询参数
     * @param baseMapper 使用的mapper实例
     * @param <E>        返回结果类型
     * @param <T>        自定义查询参数类型
     * @param <S>        mapper实例类型
     * @return
     */
    static <E, T extends SelfSqlDTO, S extends SelfPageMapper> IPage<E> findPage(T query, S baseMapper) {
        Integer total = baseMapper.queryCount(query);
        IPage<E> page = new Page<>(query.getCurrent(), query.getSize(), total);
        List<E> list;
        // 总数大于查询页数偏移
        if (total > query.getOffset()) {
            list = baseMapper.queryPage(query);
        } else {
            list = new ArrayList<>();
        }
        page.setRecords(list);
        return page;
    }

}
