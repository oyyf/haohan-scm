package com.haohan.cloud.scm.api.product.dto;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/6/17
 */
@Data
@ApiModel(description = "货品组合需求")
public class ProductPakingResultDTO {

    @ApiModelProperty(value = "原材料损耗记录列表")
    private List<ProductLossRecord> sourceLossRecordList;

    @ApiModelProperty(value = "货品加工记录列表")
    private List<ProductProcessing> productProcessingsList;

    @ApiModelProperty(value = "成品货品信息记录记录")
    private ProductInfo productInfo;
}
