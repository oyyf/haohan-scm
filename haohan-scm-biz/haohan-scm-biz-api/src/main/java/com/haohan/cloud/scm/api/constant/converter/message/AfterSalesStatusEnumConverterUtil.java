package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.AfterSalesStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class AfterSalesStatusEnumConverterUtil implements Converter<AfterSalesStatusEnum> {
    @Override
    public AfterSalesStatusEnum convert(Object o, AfterSalesStatusEnum afterSalesStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AfterSalesStatusEnum.getByType(o.toString());
    }
}
