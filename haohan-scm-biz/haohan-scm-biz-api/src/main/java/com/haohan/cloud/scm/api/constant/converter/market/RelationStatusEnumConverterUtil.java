package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.RelationStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class RelationStatusEnumConverterUtil implements Converter<RelationStatusEnum> {
    @Override
    public RelationStatusEnum convert(Object o, RelationStatusEnum relationStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return RelationStatusEnum.getByType(o.toString());
    }
}
