/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.GoodsPromotion;
import com.haohan.cloud.scm.api.goods.req.GoodsPromotionReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品促销库内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsPromotionFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsPromotionFeignService {


    /**
     * 通过id查询商品促销库
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsPromotion/{id}", method = RequestMethod.GET)
    R<GoodsPromotion> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品促销库 列表信息
     *
     * @param goodsPromotionReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsPromotion/fetchGoodsPromotionPage")
    R<Page<GoodsPromotion>> getGoodsPromotionPage(@RequestBody GoodsPromotionReq goodsPromotionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品促销库 列表信息
     *
     * @param goodsPromotionReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsPromotion/fetchGoodsPromotionList")
    R<List<GoodsPromotion>> getGoodsPromotionList(@RequestBody GoodsPromotionReq goodsPromotionReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品促销库
     *
     * @param goodsPromotion 商品促销库
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/add")
    R<Boolean> save(@RequestBody GoodsPromotion goodsPromotion, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品促销库
     *
     * @param goodsPromotion 商品促销库
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/update")
    R<Boolean> updateById(@RequestBody GoodsPromotion goodsPromotion, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品促销库
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/listByIds")
    R<List<GoodsPromotion>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsPromotionReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/countByGoodsPromotionReq")
    R<Integer> countByGoodsPromotionReq(@RequestBody GoodsPromotionReq goodsPromotionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsPromotionReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/getOneByGoodsPromotionReq")
    R<GoodsPromotion> getOneByGoodsPromotionReq(@RequestBody GoodsPromotionReq goodsPromotionReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param goodsPromotionList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/GoodsPromotion/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<GoodsPromotion> goodsPromotionList, @RequestHeader(SecurityConstants.FROM) String from);


}
