package com.haohan.cloud.scm.api.supply.vo;

import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/5/7
 * 查询B客户采购的关联供应订单信息
 * 供应订单编号
 * 支付状态
 */
@Data
public class SupplyRelationBuyOrderVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "是否已向供应商下过单")
    private Boolean status;

    @ApiModelProperty(value = "采购订单支付状态", notes = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    @ApiModelProperty(value = "供应订单编号列表, 逗号连接")
    private String supplySns;

    @ApiModelProperty(value = "供应订单编号对应采购明细的列表")
    private List<SupplyRelationBuyOrderDetailVO> detailList;


}
