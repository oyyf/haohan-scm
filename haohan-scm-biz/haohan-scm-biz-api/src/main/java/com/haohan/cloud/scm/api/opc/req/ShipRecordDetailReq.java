/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "发货记录明细")
public class ShipRecordDetailReq extends ShipRecordDetail {

    private long pageSize;
    private long pageNo;

}
