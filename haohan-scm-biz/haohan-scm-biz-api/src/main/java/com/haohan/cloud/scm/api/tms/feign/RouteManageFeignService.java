/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.tms.entity.RouteManage;
import com.haohan.cloud.scm.api.tms.req.RouteManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 路线管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "RouteManageFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface RouteManageFeignService {


    /**
     * 通过id查询路线管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/RouteManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 路线管理 列表信息
     * @param routeManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/RouteManage/fetchRouteManagePage")
    R getRouteManagePage(@RequestBody RouteManageReq routeManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 路线管理 列表信息
     * @param routeManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/RouteManage/fetchRouteManageList")
    R getRouteManageList(@RequestBody RouteManageReq routeManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/add")
    R save(@RequestBody RouteManage routeManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/update")
    R updateById(@RequestBody RouteManage routeManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除路线管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/RouteManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/countByRouteManageReq")
    R countByRouteManageReq(@RequestBody RouteManageReq routeManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/getOneByRouteManageReq")
    R getOneByRouteManageReq(@RequestBody RouteManageReq routeManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param routeManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/RouteManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<RouteManage> routeManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
