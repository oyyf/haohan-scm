/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.dto.CustomerMerchantDTO;
import com.haohan.cloud.scm.api.crm.entity.CustomerMerchantRelation;
import com.haohan.cloud.scm.api.crm.req.CustomerMerchantRelationReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户商家关系内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerMerchantRelationFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerMerchantRelationFeignService {


    /**
     * 通过id查询客户商家关系
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerMerchantRelation/{id}", method = RequestMethod.GET)
    R<CustomerMerchantRelation> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户商家关系 列表信息
     *
     * @param customerMerchantRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/fetchCustomerMerchantRelationPage")
    R<Page<CustomerMerchantRelation>> getCustomerMerchantRelationPage(@RequestBody CustomerMerchantRelationReq customerMerchantRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户商家关系 列表信息
     *
     * @param customerMerchantRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/fetchCustomerMerchantRelationList")
    R<List<CustomerMerchantRelation>> getCustomerMerchantRelationList(@RequestBody CustomerMerchantRelationReq customerMerchantRelationReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户商家关系
     *
     * @param customerMerchantRelation 客户商家关系
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/add")
    R<Boolean> save(@RequestBody CustomerMerchantRelation customerMerchantRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户商家关系
     *
     * @param customerMerchantRelation 客户商家关系
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/update")
    R<Boolean> updateById(@RequestBody CustomerMerchantRelation customerMerchantRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户商家关系
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/listByIds")
    R<List<CustomerMerchantRelation>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerMerchantRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/countByCustomerMerchantRelationReq")
    R<Integer> countByCustomerMerchantRelationReq(@RequestBody CustomerMerchantRelationReq customerMerchantRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerMerchantRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/getOneByCustomerMerchantRelationReq")
    R<CustomerMerchantRelation> getOneByCustomerMerchantRelationReq(@RequestBody CustomerMerchantRelationReq customerMerchantRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerMerchantRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerMerchantRelation> customerMerchantRelationList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 客户商家获取 没有时创建
     *  根据customerId 或sn 查询
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/fetchCustomerMerchant")
    R<CustomerMerchantDTO> fetchCustomerMerchant(@RequestBody CustomerMerchantDTO req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 修改客户相关商家名称
     * @param customerMerchantRelationReq
     * @param from
     * @return
     */
    @PostMapping("/api/feign/CustomerMerchantRelation/updateMerchantName")
    R<Boolean> updateMerchantName(@RequestBody CustomerMerchantRelationReq customerMerchantRelationReq, @RequestHeader(SecurityConstants.FROM) String from);
}
