package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/11
 */
@Data
@ApiModel(description = "查询区域对应的订单总数和订单总金额")
public class CountBuyOrderReq {
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    /**
     * 起始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

}
