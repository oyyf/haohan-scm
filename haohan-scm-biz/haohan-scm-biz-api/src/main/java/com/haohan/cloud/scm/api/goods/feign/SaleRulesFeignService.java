/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.SaleRules;
import com.haohan.cloud.scm.api.goods.req.SaleRulesReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 售卖规则内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SaleRulesFeignService", value = ScmServiceName.SCM_GOODS)
public interface SaleRulesFeignService {


    /**
     * 通过id查询售卖规则
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SaleRules/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 售卖规则 列表信息
     *
     * @param saleRulesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SaleRules/fetchSaleRulesPage")
    R getSaleRulesPage(@RequestBody SaleRulesReq saleRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 售卖规则 列表信息
     *
     * @param saleRulesReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SaleRules/fetchSaleRulesList")
    R getSaleRulesList(@RequestBody SaleRulesReq saleRulesReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增售卖规则
     *
     * @param saleRules 售卖规则
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/add")
    R save(@RequestBody SaleRules saleRules, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改售卖规则
     *
     * @param saleRules 售卖规则
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/update")
    R updateById(@RequestBody SaleRules saleRules, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除售卖规则
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/countBySaleRulesReq")
    R countBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param saleRulesReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/getOneBySaleRulesReq")
    R getOneBySaleRulesReq(@RequestBody SaleRulesReq saleRulesReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param saleRulesList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SaleRules/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SaleRules> saleRulesList, @RequestHeader(SecurityConstants.FROM) String from);


}
