package com.haohan.cloud.scm.api.constant.enums.message;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum DepartmentTypeEnum implements IBaseEnum {
    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    supply("1", "供应"),
    purchase("2", "采购"),
    production("3", "生产"),
    logistics("4", "物流"),
    bazaar("5", "市场"),
    platform("6", "平台"),
    shop("7", "门店"),
    client("8", "客户");

    private static final Map<String, DepartmentTypeEnum> MAP = new HashMap<>(12);

    static {
        for (DepartmentTypeEnum e : DepartmentTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DepartmentTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
