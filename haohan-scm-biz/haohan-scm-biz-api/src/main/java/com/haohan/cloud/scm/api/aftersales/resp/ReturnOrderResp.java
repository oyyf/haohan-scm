package com.haohan.cloud.scm.api.aftersales.resp;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/7/31
 */
@Data
public class ReturnOrderResp extends ReturnOrder {

    /**
     * 应退金额
     */
    @ApiModelProperty(value = "应退金额")
    private BigDecimal returnAmount;
}
