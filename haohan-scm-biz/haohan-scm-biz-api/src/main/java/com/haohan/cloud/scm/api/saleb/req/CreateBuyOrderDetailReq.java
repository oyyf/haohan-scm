package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author cx
 * @date 2019/7/8
 */
@Data
public class CreateBuyOrderDetailReq {

    @NotNull(message = "buyOrder不能为空")
    private BuyOrder buyOrder;

    @NotEmpty(message = "商品列表不能为空")
    private List<GoodsModel> list;
}
