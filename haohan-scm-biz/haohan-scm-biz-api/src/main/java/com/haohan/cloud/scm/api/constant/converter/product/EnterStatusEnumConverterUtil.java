package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class EnterStatusEnumConverterUtil implements Converter<EnterStatusEnum> {
    @Override
    public EnterStatusEnum convert(Object o, EnterStatusEnum enterStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EnterStatusEnum.getByType(o.toString());
    }
}
