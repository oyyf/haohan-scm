package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author cx
 * @date 2019/8/16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class MarketRecordResp extends MarketRecord {

    private String cateName;

    private String address;

    private String phone;
}
