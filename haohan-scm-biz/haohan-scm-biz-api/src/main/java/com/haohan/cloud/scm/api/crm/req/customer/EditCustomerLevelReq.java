package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.crm.entity.CustomerLevel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/9/23
 */
@Data
public class EditCustomerLevelReq {

    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;

    @NotBlank(message = "客户编号不能为空")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    @ApiModelProperty(value = "客户编号", required = true)
    private String customerSn;

    @Length(min = 0, max = 32, message = "父级ID长度在0至32之间")
    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @Length(min = 0, max = 5, message = "排序值长度在0至5之间")
    @ApiModelProperty(value = "排序值")
    private String sort;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    public CustomerLevel transTo() {
        CustomerLevel level = new CustomerLevel();
        level.setId(this.id);
        level.setCustomerSn(this.customerSn);
        level.setParentId(this.parentId);
        level.setSort(this.sort);
        return level;
    }
}
