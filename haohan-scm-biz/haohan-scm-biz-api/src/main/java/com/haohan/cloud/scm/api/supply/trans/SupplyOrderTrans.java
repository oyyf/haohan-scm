package com.haohan.cloud.scm.api.supply.trans;

import com.haohan.cloud.scm.api.bill.dto.OrderDetailDTO;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.ShipStatusEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderDetailVO;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import com.haohan.cloud.scm.api.supply.vo.SupplyBuyOrderInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyRelationBuyOrderVO;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2020/4/21
 */
@UtilityClass
public class SupplyOrderTrans {


    public static void copySupplier(SupplyOrder order, Supplier supplier) {
        order.setSupplierId(supplier.getId());
        order.setSupplierName(supplier.getSupplierName());
        order.setContact(supplier.getContact());
        order.setTelephone(supplier.getTelephone());
        order.setAddress(supplier.getAddress());
    }

    public static OfferOrder createOfferOrder(BuyOrderDetailVO detail, Supplier supplier) {
        OfferOrder offer = new OfferOrder();
        offer.setAskOrderId(detail.getBuyDetailSn());
        offer.setOfferType(PdsOfferTypeEnum.platformOffer);
        offer.setSupplierId(supplier.getId());
        offer.setSupplierName(supplier.getSupplierName());
        // 订单中无goodsId
        offer.setGoodsId("");
        offer.setGoodsModelId(detail.getModelId());
        offer.setGoodsName(detail.getGoodsName());
        offer.setModelName(detail.getModelName());
        offer.setUnit(detail.getUnit());
        offer.setGoodsImg(detail.getGoodsImg());
        offer.setBuyNum(detail.getGoodsNum());
        offer.setSupplyNum(detail.getGoodsNum());
        // 价格状态
        offer.setSupplyPrice(detail.getBuyPrice());
        offer.setStatus(PdsOfferStatusEnum.bidding);
        offer.setShipStatus(ShipStatusEnum.stayCargo);
        offer.setDealPrice(detail.getBuyPrice());
        offer.setOtherAmount(BigDecimal.ZERO);
        offer.setTotalAmount(detail.getBuyPrice().multiply(detail.getGoodsNum()));
        offer.setReceiveType(ReceiveTypeEnum.delivery);
        // 其他属性
        offer.setBuyDetailSn(detail.getBuyDetailSn());
        offer.setRemarks("根据客户采购订单明细创建报价单");
        return offer;
    }

    public static SupplyOrderDetail initDetailByOffer(OfferOrder offer, GoodsModelDTO supplyModel) {
        SupplyOrderDetail detail = new SupplyOrderDetail();
        detail.setSupplierId(offer.getSupplierId());
        detail.setGoodsId(supplyModel.getGoodsId());
        detail.setGoodsModelId(supplyModel.getId());
        detail.setGoodsModelSn(supplyModel.getGoodsModelSn());
        detail.setGoodsImg(supplyModel.getModelUrl());
        detail.setGoodsName(supplyModel.getGoodsName());
        detail.setModelName(supplyModel.getModelName());
        detail.setGoodsNum(offer.getBuyNum());
        detail.setMarketPrice(null == offer.getSupplyPrice() ? supplyModel.getModelPrice() : offer.getSupplyPrice());
        detail.setDealPrice(supplyModel.getModelPrice());
        detail.setUnit(supplyModel.getModelUnit());
        detail.setAmount(offer.getTotalAmount());
        return detail;
    }

    public static OrderInfoDTO transToOrderInfoBySupply(SupplyOrder order, List<SupplyOrderDetail> detailList) {
        OrderInfoDTO orderInfo = new OrderInfoDTO();
        orderInfo.setOrderSn(order.getSupplySn());
        orderInfo.setOrderType(OrderTypeEnum.supply);
        // 默认未付
        orderInfo.setPayStatus(PayStatusEnum.wait);
        // 订单状态 暂只设置 取消和已下单
        orderInfo.setOrderStatus(order.getStatus() == BuyOrderStatusEnum.cancel ? OrderStatusEnum.cancel : OrderStatusEnum.submit);
        orderInfo.setContact(order.getContact());
        orderInfo.setTelephone(order.getTelephone());
        orderInfo.setAddress(order.getAddress());
        LocalDateTime deal = null == order.getDealTime() ? order.getOrderTime() : order.getDealTime();
        orderInfo.setDealDate(deal.toLocalDate());
        orderInfo.setOrderTime(order.getOrderTime());
        orderInfo.setTotalAmount(order.getTotalAmount());
        orderInfo.setSumAmount(order.getSumAmount());
        orderInfo.setOtherAmount(order.getOtherAmount());
        orderInfo.setGoodsNum(order.getGoodsNum());
        orderInfo.setRemarks(order.getRemarks());
        // 明细
        orderInfo.setDetailList(detailList.stream()
                .map(item -> {
                    OrderDetailDTO detail = new OrderDetailDTO();
                    detail.setDetailSn(item.getSupplyDetailSn());
                    detail.setGoodsId(item.getGoodsId());
                    detail.setGoodsModelId(item.getGoodsModelId());
                    detail.setGoodsName(item.getGoodsName());
                    detail.setModelName(item.getModelName());
                    detail.setUnit(item.getUnit());
                    detail.setGoodsImg(item.getGoodsImg());
                    detail.setMarketPrice(item.getMarketPrice());
                    detail.setDealPrice(item.getDealPrice());
                    detail.setGoodsNum(item.getGoodsNum());
                    detail.setAmount(item.getAmount());
                    return detail;
                }).collect(Collectors.toList()));
        return orderInfo;
    }

    public static SupplyRelationBuyOrderVO initSupplyRelationBuyOrder(PayStatusEnum payStatus) {
        SupplyRelationBuyOrderVO relation = new SupplyRelationBuyOrderVO();
        relation.setStatus(false);
        relation.setPayStatus(payStatus);
        relation.setSupplySns("");
        relation.setDetailList(new ArrayList<>(4));
        return relation;
    }


    public static SupplyBuyOrderInfoVO initSupplyBuyOrderInfo(ShipRecord shipRecord) {
        SupplyBuyOrderInfoVO info = new SupplyBuyOrderInfoVO();
        info.setShipRecordSn(null == shipRecord ? "" : shipRecord.getShipRecordSn());
        info.setBuyOrderSns("");
        info.setDetailList(new ArrayList<>(4));
        return info;
    }
}
