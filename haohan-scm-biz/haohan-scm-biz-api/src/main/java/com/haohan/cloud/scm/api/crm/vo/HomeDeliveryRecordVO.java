package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.MemberTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PaymentTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.HomeDeliveryRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/10
 */
@Data
@NoArgsConstructor
public class HomeDeliveryRecordVO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 联系人id
     */
    @ApiModelProperty(value = "联系人id")
    private String linkmanId;
    /**
     * 联系人名称
     */
    @ApiModelProperty(value = "联系人名称")
    private String linkmanName;
    /**
     * 配件类型 1普通件 2急件
     */
    @ApiModelProperty(value = "配件类型 1普通件 2急件")
    private DeliveryGoodsTypeEnum goodsType;
    /**
     * 件数
     */
    @ApiModelProperty(value = "件数")
    private Integer goodsNum;
    /**
     * 重量 , 单位kg
     */
    @ApiModelProperty(value = "重量 , 单位kg")
    private BigDecimal weight;
    /**
     * 预约配送时间
     */
    @ApiModelProperty(value = "预约配送时间")
    private LocalDateTime appointmentTime;
    /**
     * 配送时间
     */
    @ApiModelProperty(value = "配送时间")
    private LocalDateTime deliveryTime;
    /**
     * 配送物品 （自定义标签）
     */
    @ApiModelProperty(value = "配送物品 （自定义标签）")
    private String goodsTags;
    /**
     * 配送状态 1未送，2已送，3异常
     */
    @ApiModelProperty(value = "配送状态 1未送，2已送，3异常")
    private DeliveryGoodsStatusEnum deliveryStatus;
    /**
     * 会员类型 1普通，2年卡
     */
    @ApiModelProperty(value = "会员类型（1普通，2年卡）")
    private MemberTypeEnum memberType;
    /**
     * 付款方式 1.现金，2.微信转账，3.线上支付
     */
    @ApiModelProperty(value = "付款方式 1.现金，2.微信转账，3.线上支付")
    private PaymentTypeEnum paymentType;
    /**
     * 付款金额（数字, 单位元）
     */
    @ApiModelProperty(value = "付款金额（数字, 单位元）")
    private BigDecimal amount;
    /**
     * 其他服务 （自定义标签）
     */
    @ApiModelProperty(value = "其他服务 （自定义标签）")
    private String otherServices;
    /**
     * 补充说明
     */
    @ApiModelProperty(value = "补充说明")
    private String description;
    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String orderSn;
    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型")
    private String orderTye;
    /**
     * 第三方订单号
     */
    @ApiModelProperty(value = "第三方订单号")
    private String thirdOrderSn;

    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public HomeDeliveryRecordVO(HomeDeliveryRecord record) {
        BeanUtil.copyProperties(record, this);
    }


}
