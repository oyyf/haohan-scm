package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.TaskActionTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@ApiModel(description = "采购员工任务列表查询请求")
public class PurchasePageReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @ApiModelProperty(value = "采购员工通行证id", required = true)
    private String uid;
    /**
     * 任务状态1.待处理2.执行中3.已完成4.未完成
     */
    @ApiModelProperty(value = "任务状态1.待处理2.执行中3.已完成4.未完成",dataType = "String",example = "1")
    private TaskStatusEnum taskStatus;

    @ApiModelProperty(value = "采购单编号")
    private String purchaseSn;

    @ApiModelProperty(value = "采购单明细编号")
    private String purchaseDetailSn;

    @ApiModelProperty(value = "任务执行方式:1.审核2.分配3.采购")
    private TaskActionTypeEnum taskActionType;

    @ApiModelProperty(value = "执行人名称")
    private String transactorName;

    @ApiModelProperty(value = "任务截止时间")
    private LocalDateTime deadlineTime;

    @ApiModelProperty(value = "分页大小")
    private long size = 10;
    @ApiModelProperty(value = "分页当前页码")
    private long current = 1;
}
