package com.haohan.cloud.scm.api.opc.req.ship;

import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipRecordStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author dy
 * @date 2020/1/6
 */
@Data
public class QueryShipRecordReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "发货记录编号不能为空", groups = {SingleGroup.class})
    @Length(max = 64, message = "发货记录编号长度最大64字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "发货记录编号")
    private String shipRecordSn;

    @Length(max = 64, message = "订单编号长度最大64字符")
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @Length(max = 64, message = "物流单号长度最大64字符")
    @ApiModelProperty(value = "物流单号")
    private String logisticsSn;

    @Length(max = 64, message = "客户ID长度最大64字符")
    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @ApiModelProperty(value = "交货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "发货记录状态: 1.待发货 2.已发货 3.已关闭")
    private ShipRecordStatusEnum shipStatus;

    @ApiModelProperty(value = "发货方式 1.第三方物流2.自配送")
    private ShipTypeEnum shipType;

    @ApiModelProperty(value = "订单类型： 1.采购订单、2.供应订单、3.退货订单、4.销售订单")
    private OrderTypeEnum orderType;

    // 非eq

    @Length(max = 64, message = "客户名称长度最大64字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 64, message = "收货人名称长度最大64字符")
    @ApiModelProperty(value = "收货人名称")
    private String receiverName;

    @Length(max = 64, message = "发货人名称长度最大64字符")
    @ApiModelProperty(value = "发货人")
    private String shipperName;

    @Length(max = 64, message = "物流公司长度最大64字符")
    @ApiModelProperty(value = "物流公司")
    private String logisticsName;

    @ApiModelProperty(value = "查询发货日期-开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询发货日期-结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty(value = "订单编号集合")
    private Set<String> orderSnSet;

    public ShipRecord transTo() {
        ShipRecord ship = new ShipRecord();
        // eq条件
        ship.setShipRecordSn(this.shipRecordSn);
        ship.setOrderSn(this.orderSn);
        ship.setLogisticsSn(this.logisticsSn);
        ship.setCustomerId(this.customerId);
        ship.setDeliveryDate(this.deliveryDate);
        ship.setShipStatus(this.shipStatus);
        ship.setShipType(this.shipType);
        ship.setOrderType(this.orderType);
        return ship;
    }
}
