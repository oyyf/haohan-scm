/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.EnterTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 入库单
 *
 * @author haohan
 * @date 2019-05-13 21:28:49
 */
@Data
@TableName("scm_pws_enter_warehouse")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "入库单")
public class EnterWarehouse extends Model<EnterWarehouse> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 入库单编号
     */
    private String enterWarehouseSn;
    /**
     * 入库批次号
     */
    private String batchNumber;
    /**
     * 入库申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 仓库编号
     */
    private String warehouseSn;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 入库状态:1待处理2验收中3已验收4.售后待处理
     */
    private EnterStatusEnum enterStatus;
    /**
     * 入库单类型:1.加工入库2.采购入库
     */
    private EnterTypeEnum enterType;
    /**
     * 货品处理类型:1.入库存储2.转配送
     */
    private StorageTypeEnum storageType;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

  public EnterWarehouse(String enterWarehouseSn) {
    this.setEnterWarehouseSn(enterWarehouseSn);
  }

  public EnterWarehouse() {

  }
}
