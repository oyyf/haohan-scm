package com.haohan.cloud.scm.api.salec.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author cx
 * @date 2019/6/21
 */
@Data
@ApiModel(description = "对应属性值下的商品信息")
public class ValueDTO {

    @ApiModelProperty(value = "此商品的属性信息")
    private Map<String,String> detail;

    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @ApiModelProperty(value = "销售价")
    private BigDecimal price;

    @ApiModelProperty(value = "销量")
    private Integer sales = 100;

    @ApiModelProperty(value = "商品图片")
    private String pic;

    private Boolean check = false;

}
