/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.WarningRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 预警记录表
 *
 * @author haohan
 * @date 2019-05-29 13:49:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预警记录表")
public class WarningRecordReq extends WarningRecord {

    private long pageSize;
    private long pageNo;




}
