package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.LendingStatusEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class LendingStatusEnumConverterUtil implements Converter<LendingStatusEnum> {
    @Override
    public LendingStatusEnum convert(Object o, LendingStatusEnum lendingStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return LendingStatusEnum.getByType(o.toString());
    }
}
