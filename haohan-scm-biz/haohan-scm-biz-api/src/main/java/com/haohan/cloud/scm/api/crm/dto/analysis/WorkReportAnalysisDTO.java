package com.haohan.cloud.scm.api.crm.dto.analysis;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Api("工作日报分析")
public class WorkReportAnalysisDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "员工名称")
    private String employeeName;

    @ApiModelProperty(value = "汇报员工id列表,逗号分隔")
    private String reportManIds;

    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private String status;

    @ApiModelProperty(value = "工作汇报类型: 1.日常 2.销量 3.库存")
    private String reportType;

    @ApiModelProperty(value = "日报数")
    private Integer reportNum;

    @ApiModelProperty(value = "员工数")
    private Integer employeeNum;


}
