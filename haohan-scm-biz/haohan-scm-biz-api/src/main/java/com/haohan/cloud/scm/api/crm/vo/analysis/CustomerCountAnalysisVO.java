package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("新增客户数统计")
public class CustomerCountAnalysisVO {

    /**
     * 统计时间  yyyy-MM / yyyy-MM-dd
     */
    private String date;
    /**
     * 次数
     */
    private Integer num;


}
