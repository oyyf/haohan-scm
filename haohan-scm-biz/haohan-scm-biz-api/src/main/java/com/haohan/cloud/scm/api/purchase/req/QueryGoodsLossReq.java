package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class QueryGoodsLossReq {

  @NotBlank(message = "summaryOrderId不能为空")
  private String summaryOrderId;



}
