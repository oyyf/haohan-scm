package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/8/11
 */
@Data
@ApiModel(description = "修改B订单及明细的状态")
public class UpdateBuyOrderStatusReq {

    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    /**
     * 采购编号
     */
    @NotBlank(message = "buyId不能为空")
    @ApiModelProperty(value = "采购编号", required = true)
    private String buyId;

    /**
     * 状态
     */
    @NotNull(message = "status不能为空")
    @ApiModelProperty(value = "状态", required = true)
    private BuyOrderStatusEnum status;

    @NotEmpty(message = "detailList不能为空")
    @ApiModelProperty(value = "明细列表,必需buyDetailSn/status,可选goodsNum", required = true)
    private List<BuyOrderDetail> detailList;

    /**
     * 运费
     */
    @ApiModelProperty(value = "运费")
    private BigDecimal shipFee;
}
