package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/20
 */
@Data
@ApiModel(description = "查询报价单详情-小程序")
public class QueryOfferDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "供应商uid",required = true)
    private String uid;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "报价单主键",required = true)
    private String id;

    @ApiModelProperty(value = "报价类型",required = true)
    private PdsOfferTypeEnum offerType;
}
