package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.crm.entity.CustomerVisit;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/29
 */
@Data
public class ArrivalVisitReq {

    @NotBlank(message = "客户拜访记录id不能为空")
    @Length(max = 32, message = "客户拜访记录id长度最大32字符")
    @ApiModelProperty(value = "主键")
    private String id;

    @NotBlank(message = "抵达地址定位不能为空")
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "抵达地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "抵达地址定位")
    private String arrivalPosition;

    @NotBlank(message = "抵达地址不能为空")
    @Length(max = 64, message = "抵达地址长度最大64字符")
    @ApiModelProperty(value = "抵达地址")
    private String arrivalAddress;

    @ApiModelProperty(value = "拜访图片列表")
    private List<String> photoList;

    public CustomerVisit transTo() {
        CustomerVisit visit = new CustomerVisit();
        visit.setId(this.id);
        visit.setArrivalPosition(this.arrivalPosition);
        visit.setArrivalAddress(this.arrivalAddress);
        return visit;
    }

}
