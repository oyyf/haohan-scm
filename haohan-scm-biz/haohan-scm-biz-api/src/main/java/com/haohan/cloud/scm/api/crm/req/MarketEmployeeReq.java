/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 市场部员工
 *
 * @author haohan
 * @date 2019-08-30 11:45:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "市场部员工")
public class MarketEmployeeReq extends MarketEmployee {

    private long pageSize;
    private long pageNo;


}
