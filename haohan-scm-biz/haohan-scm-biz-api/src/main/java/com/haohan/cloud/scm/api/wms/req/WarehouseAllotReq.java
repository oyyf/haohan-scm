/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 库存调拨记录
 *
 * @author haohan
 * @date 2019-05-28 19:51:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "库存调拨记录")
public class WarehouseAllotReq extends WarehouseAllot {

    private long pageSize;
    private long pageNo;




}
