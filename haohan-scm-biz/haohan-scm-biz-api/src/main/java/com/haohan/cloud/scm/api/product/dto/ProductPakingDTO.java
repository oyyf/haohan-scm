package com.haohan.cloud.scm.api.product.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/14
 */
@Data
@ApiModel(description = "货品组合 (按配方 合并为一个 原始货品)需求")
public class ProductPakingDTO {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家平台id",required = true)
    private String pmId;

    @NotNull(message = "sourceProductSn不能为空")
    @ApiModelProperty(value = "原材料货品编号列表", required = true)
    private List<String> sourceProductSns;

    @NotBlank(message= "subProductNum不能为空")
    @ApiModelProperty(value = "货品加工数量", required = true)
    private BigDecimal subProductNum;

    @NotBlank(message = "recipeSn不能为空")
    @ApiModelProperty(value = "商品配方编号" , required = true)
    private String recipeSn;

    @ApiModelProperty(value = "操作员id")
    private String operatorId;

}
