/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;
import com.haohan.cloud.scm.api.product.req.ProductLossRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货品损耗记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductLossRecordFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ProductLossRecordFeignService {


    /**
     * 通过id查询货品损耗记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductLossRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货品损耗记录表 列表信息
     * @param productLossRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductLossRecord/fetchProductLossRecordPage")
    R getProductLossRecordPage(@RequestBody ProductLossRecordReq productLossRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货品损耗记录表 列表信息
     * @param productLossRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductLossRecord/fetchProductLossRecordList")
    R getProductLossRecordList(@RequestBody ProductLossRecordReq productLossRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/add")
    R save(@RequestBody ProductLossRecord productLossRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/update")
    R updateById(@RequestBody ProductLossRecord productLossRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货品损耗记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ProductLossRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/countByProductLossRecordReq")
    R countByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/getOneByProductLossRecordReq")
    R getOneByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productLossRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductLossRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductLossRecord> productLossRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增报损记录
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ProductLossRecordaddLossRecord")
    R addLossRecord(@RequestBody ProductInfoInventoryReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
