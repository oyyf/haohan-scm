package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;

/**
 * @author dy
 * @date 2019/9/27
 */
public class DataReportStatusEnumConverterUtil implements Converter<DataReportStatusEnum> {
    @Override
    public DataReportStatusEnum convert(Object o, DataReportStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DataReportStatusEnum.getByType(o.toString());
    }

}