package com.haohan.cloud.scm.api.aftersales.resp;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *
 * @author cx
 * @date 2019/8/6
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class QueryReturnOrderDetailResp extends ReturnOrder {

    private List<ReturnOrderDetail> detailList;
    /**
     * 退货图片 列表
     */
    private List<PhotoGallery> photoList;

}
