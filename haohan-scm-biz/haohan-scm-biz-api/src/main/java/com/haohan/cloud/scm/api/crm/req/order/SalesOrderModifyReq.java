package com.haohan.cloud.scm.api.crm.req.order;

import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/28
 */
@Data
public class SalesOrderModifyReq {

    @NotBlank(message = "订单编号不能为空")
    @Length(min = 0, max = 32, message = "订单编号长度在0至32之间")
    private String salesOrderSn;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("交货日期")
    private LocalDate deliveryDate;

    @ApiModelProperty("销售单类型")
    private SalesTypeEnum salesType;

    @Length(min = 0, max = 32, message = "业务员id长度在0至32之间")
    @ApiModelProperty("业务员id")
    private String employeeId;

    @Length(min = 0, max = 32, message = "收货地址id长度在0至32之间")
    private String customerAddressId;

    @Digits(integer = 8, fraction = 2, message = "总计金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "总计金额在0至1000000之间")
    private BigDecimal totalAmount;

    @Digits(integer = 8, fraction = 2, message = "其他金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "其他金额在0至1000000之间")
    private BigDecimal otherAmount;

    @ApiModelProperty("支付状态")
    private PayStatusEnum payStatus;

    @Length(min = 0, max = 100, message = "订单备注长度在0至100之间")
    private String remarks;

    @Length(min = 0, max = 32, message = "供货商编号长度在0至32之间")
    private String supplierSn;

    @Valid
    @NotEmpty(message = "商品明细不能为空")
    private List<SalesDetailAddReq> detailList;

    public SalesOrder transTo() {
        SalesOrder order = new SalesOrder();
        order.setDeliveryDate(this.deliveryDate);
        order.setSalesType(this.salesType);
        order.setEmployeeId(this.employeeId);
        order.setTotalAmount(this.totalAmount);
        order.setOtherAmount(this.otherAmount);
        order.setPayStatus(this.payStatus);
        order.setRemarks(this.remarks);
        order.setSupplierSn(this.supplierSn);
        return order;
    }

}
