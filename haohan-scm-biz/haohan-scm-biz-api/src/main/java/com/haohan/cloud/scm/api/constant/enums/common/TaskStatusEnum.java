package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum TaskStatusEnum implements IBaseEnum {
    /**
     * 任务状态1.待处理2.执行中3.已完成4.未完成
     */
    wait("1", "待处理"),
    doing("2", "执行中"),
    finish("3", "已完成"),
    failed("4", "未完成");
    private static final Map<String, TaskStatusEnum> MAP = new HashMap<>(8);

    static {
        for (TaskStatusEnum e : TaskStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static TaskStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
