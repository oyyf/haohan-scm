/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 订单表
 *
 * @author haohan
 * @date 2020-05-21 18:44:43
 */
@Data
@TableName("eb_store_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "订单表")
public class StoreOrder extends Model<StoreOrder> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "订单ID")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @Length(max = 32, message = "订单号长度最大32字符")
    @ApiModelProperty(value = "订单号")
    private String orderId;

    @ApiModelProperty(value = "用户id", notes = "WechatUser的主键")
    private Integer uid;

    @Length(max = 32, message = "用户姓名长度最大32字符")
    @ApiModelProperty(value = "用户姓名")
    private String realName;

    @Length(max = 18, message = "用户电话长度最大18字符")
    @ApiModelProperty(value = "用户电话")
    private String userPhone;

    @Length(max = 100, message = "详细地址长度最大100字符")
    @ApiModelProperty(value = "详细地址")
    private String userAddress;

    @Length(max = 256, message = "购物车id长度最大256字符")
    @ApiModelProperty(value = "购物车id")
    private String cartId;

    @Digits(integer = 8, fraction = 2, message = "运费金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "运费金额")
    private BigDecimal freightPrice;

    @ApiModelProperty(value = "订单商品总数")
    private Integer totalNum;

    @Digits(integer = 8, fraction = 2, message = "订单总价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "订单总价")
    private BigDecimal totalPrice;

    @Digits(integer = 8, fraction = 2, message = "邮费的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "邮费")
    private BigDecimal totalPostage;

    @Digits(integer = 8, fraction = 2, message = "实际支付金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "实际支付金额")
    private BigDecimal payPrice;

    @Digits(integer = 8, fraction = 2, message = "支付邮费的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "支付邮费")
    private BigDecimal payPostage;

    @Digits(integer = 8, fraction = 2, message = "抵扣金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "抵扣金额")
    private BigDecimal deductionPrice;

    @ApiModelProperty(value = "优惠券id")
    private Integer couponId;

    @Digits(integer = 8, fraction = 2, message = "优惠券金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "优惠券金额")
    private BigDecimal couponPrice;

    @ApiModelProperty(value = "支付状态", notes = "0否1是")
    private Integer paid;

    @ApiModelProperty(value = "支付时间")
    private Integer payTime;

    @Length(max = 32, message = "支付方式长度最大32字符")
    @ApiModelProperty(value = "支付方式")
    private String payType;

    @ApiModelProperty(value = "创建时间")
    private Integer addTime;

    @ApiModelProperty(value = "订单状态（-1 : 申请退款 -2 : 退货成功 0：待发货；1：待收货；2：已收货；3：待评价；-1：已退款）")
    private Integer status;

    @ApiModelProperty(value = "0 未退款 1 申请中 2 已退款")
    private Integer refundStatus;

    @Length(max = 255, message = "退款图片长度最大255字符")
    @ApiModelProperty(value = "退款图片")
    private String refundReasonWapImg;

    @Length(max = 255, message = "退款用户说明长度最大255字符")
    @ApiModelProperty(value = "退款用户说明")
    private String refundReasonWapExplain;

    @ApiModelProperty(value = "退款时间")
    private Integer refundReasonTime;

    @Length(max = 255, message = "前台退款原因长度最大255字符")
    @ApiModelProperty(value = "前台退款原因")
    private String refundReasonWap;

    @Length(max = 255, message = "不退款的理由长度最大255字符")
    @ApiModelProperty(value = "不退款的理由")
    private String refundReason;

    @Digits(integer = 8, fraction = 2, message = "退款金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundPrice;

    @Length(max = 64, message = "快递名称/送货人姓名长度最大64字符")
    @ApiModelProperty(value = "快递名称/送货人姓名")
    private String deliveryName;

    @Length(max = 32, message = "发货类型长度最大32字符")
    @ApiModelProperty(value = "发货类型")
    private String deliveryType;

    @Length(max = 64, message = "快递单号/手机号长度最大64字符")
    @ApiModelProperty(value = "快递单号/手机号")
    private String deliveryId;

    @Digits(integer = 8, fraction = 2, message = "消费赚取积分的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "消费赚取积分")
    private BigDecimal gainIntegral;

    @Digits(integer = 8, fraction = 2, message = "使用积分的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "使用积分")
    private BigDecimal useIntegral;

    @Digits(integer = 8, fraction = 2, message = "给用户退了多少积分的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "给用户退了多少积分")
    private BigDecimal backIntegral;

    @Length(max = 512, message = "备注长度最大512字符")
    @ApiModelProperty(value = "备注")
    private String mark;

    @TableLogic
    @ApiModelProperty(value = "是否删除")
    private Integer isDel;

    @Length(max = 32, message = "唯一id(md5加密)类似id长度最大32字符")
    @ApiModelProperty(value = "唯一id(md5加密)类似id")
    @TableField("`unique`")
    private String unique;

    @Length(max = 512, message = "管理员备注长度最大512字符")
    @ApiModelProperty(value = "管理员备注")
    private String remark;

    @ApiModelProperty(value = "商户ID")
    private Integer merId;

    @ApiModelProperty(value = "")
    private Integer isMerCheck;

    @ApiModelProperty(value = "拼团商品id0一般商品")
    private Integer combinationId;

    @ApiModelProperty(value = "拼团id 0没有拼团")
    private Integer pinkId;

    @Digits(integer = 8, fraction = 2, message = "成本价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @ApiModelProperty(value = "秒杀商品ID")
    private Integer seckillId;

    @ApiModelProperty(value = "砍价id")
    private Integer bargainId;

    @Length(max = 12, message = "核销码长度最大12字符")
    @ApiModelProperty(value = "核销码")
    private String verifyCode;

    @ApiModelProperty(value = "门店id")
    private Integer storeId;

    @ApiModelProperty(value = "配送方式 1=快递 ，2=门店自提")
    private Integer shippingType;

    @ApiModelProperty(value = "店员id")
    private Integer clerkId;

    @ApiModelProperty(value = "支付渠道(0微信公众号1微信小程序)")
    private Integer isChannel;

    @ApiModelProperty(value = "消息提醒")
    private Integer isRemind;

    @ApiModelProperty(value = "后台是否删除")
    private Integer isSystemDel;

    @ApiModelProperty(value = "是否已同步到后台, 0否1是")
    private String isScm;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
