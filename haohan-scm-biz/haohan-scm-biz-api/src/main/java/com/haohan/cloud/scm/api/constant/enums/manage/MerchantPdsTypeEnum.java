package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/14
 */
@Getter
@AllArgsConstructor
public enum MerchantPdsTypeEnum implements IBaseEnum {

    /**
     * 商家的采购配送类型:0 普通商家 1 平台商家  9 商城用户
     */
    general("0", "普通商家"),
    platform("1", "平台商家"),
    mall("9", " 商城用户");

    private static final Map<String, MerchantPdsTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MerchantPdsTypeEnum e : MerchantPdsTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MerchantPdsTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
