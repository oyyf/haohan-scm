package com.haohan.cloud.scm.api.goods.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author cx
 * @date 2019/6/24
 */
@Data
@ApiModel(description = "获取商品信息需求")
public class GoodsInfoReq {

    @NotNull(message = "goodsId不能为空")
    @ApiModelProperty(value = "商品id")
    private List<String> goodsIds;

    private String shopId;
}
