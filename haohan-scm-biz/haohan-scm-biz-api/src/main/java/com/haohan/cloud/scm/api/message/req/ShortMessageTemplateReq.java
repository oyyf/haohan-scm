/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.ShortMessageTemplate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 短信模板
 *
 * @author haohan
 * @date 2019-05-28 20:19:08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "短信模板")
public class ShortMessageTemplateReq extends ShortMessageTemplate {

    private long pageSize;
    private long pageNo;




}
