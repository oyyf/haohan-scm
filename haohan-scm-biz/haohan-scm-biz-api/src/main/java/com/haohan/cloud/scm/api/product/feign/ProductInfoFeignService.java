/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.dto.ProcessingResultDTO;
import com.haohan.cloud.scm.api.product.dto.ProcessingSourceDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.CreateSortingOrderReq;
import com.haohan.cloud.scm.api.product.req.ProductInfoReq;
import com.haohan.cloud.scm.api.product.req.ProductMatchSortingReq;
import com.haohan.cloud.scm.api.product.req.QueryGoodsStorageReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 货品信息记录表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductInfoFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ProductInfoFeignService {


    /**
     * 通过id查询货品信息记录表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductInfo/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 货品信息记录表 列表信息
     * @param productInfoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductInfo/fetchProductInfoPage")
    R getProductInfoPage(@RequestBody ProductInfoReq productInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 货品信息记录表 列表信息 按货品数量 降序
     * @param productInfoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductInfo/fetchProductInfoList")
    R getProductInfoList(@RequestBody ProductInfoReq productInfoReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/add")
    R save(@RequestBody ProductInfo productInfo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/update")
    R updateById(@RequestBody ProductInfo productInfo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除货品信息记录表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ProductInfo/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/countByProductInfoReq")
    R countByProductInfoReq(@RequestBody ProductInfoReq productInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/getOneByProductInfoReq")
    R getOneByProductInfoReq(@RequestBody ProductInfoReq productInfoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductInfo/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductInfo> productInfoList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 货品报损/去皮 (拆分为2个 状态为：一个正常 一个损耗)
     * 货品未保存
     * @param sourceDTO
     *          必须参数: sourceProductSn / subProductNum
     * @param from
     * @return ProcessingResultDTO
     *          返回结果: subProduct / lossProduct / sourceProduct
     */
    @PostMapping("/api/feign/ProductInfo/productPeeling")
    R<ProcessingResultDTO> productPeeling(@RequestBody ProcessingSourceDTO sourceDTO, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询库存余量
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ProductInfo/queryGoodsStorage")
    R<ProductInfo> queryGoodsStorage(@RequestBody QueryGoodsStorageReq req,@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 创建货品信息 根据采购单明细
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ProductInfo/addProductInfo")
    R<ProductInfo> addProductInfo(@RequestBody PurchaseQueryOrderDetailReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增货品分拣单  根据B客户订单
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/ProductInfo/createSortingOrder")
    R<SortingOrder> createSortingOrder(@RequestBody CreateSortingOrderReq req, @RequestHeader(SecurityConstants.FROM) String from);

     /**
     * 根据 采购入库货品 匹配分拣
     * @param from
     * @param req pmId / realBuyNum / purchaseDetailSn
     * @return 返回修改后的货品信息
     */
    @PostMapping("/api/feign/ProductInfo/enterProductMatchSorting")
    R<ProductInfo> enterProductMatchSorting(@RequestBody ProductMatchSortingReq req, @RequestHeader(SecurityConstants.FROM) String from);


}
