package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @author xwx
 * @date 2019/7/27
 */
@Data
@ApiModel(description = "供应商报价")
public class AffirmOfferReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "uid不能为空")
    @ApiModelProperty(value = "通行证id",required = true)
    private String uid;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "报价单id",required = true)
    private String id;

    @NotBlank(message = "supplyPrice不能为空")
    @ApiModelProperty(value = "供应商报价",required = true)
    private BigDecimal supplyPrice;

    @NotBlank(message = "receiveType不能为空")
    @ApiModelProperty(value = "揽货方式1.自提2.送货上门",required = true)
    private ReceiveTypeEnum receiveType;

    @ApiModelProperty(value = "备注信息",required = true)
    private String remarks;


}
