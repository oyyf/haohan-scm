package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.EmployeeTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class EmployeeTypeEnumConverterUtil implements Converter<EmployeeTypeEnum> {
    @Override
    public EmployeeTypeEnum convert(Object o, EmployeeTypeEnum employeeTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return EmployeeTypeEnum.getByType(o.toString());
    }

}
