/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.entity;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;



/**
 * 退货单明细
 *
 * @author haohan
 * @date 2019-07-30 15:55:55
 */
@Data
@TableName("scm_ass_return_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "退货单明细")
public class ReturnOrderDetail extends Model<ReturnOrderDetail> {
    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 商家ID
     */
    @ApiModelProperty(value = "商家ID")
    private String pmId;
    /**
     * 退货单编号
     */
    @ApiModelProperty(value = "退货单编号")
    private String returnSn;
    /**
     * 退货单明细编号
     */
    @ApiModelProperty(value = "退货单明细编号")
    private String returnDetailSn;
    /**
     * 订单明细编号
     */
    @ApiModelProperty(value = "订单明细编号")
    private String orderDetailSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID")
    private String goodsId;
    /**
     * 商品规格ID
     */
    @ApiModelProperty(value = "商品规格ID")
    private String goodsModelId;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 商品图片
     */
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;
    /**
     * 商品分类ID
     */
    @ApiModelProperty(value = "商品分类ID")
    private String goodsCategoryId;
    /**
     * 商品分类名称
     */
    @ApiModelProperty(value = "商品分类名称")
    private String goodsCategoryName;
    /**
     * 规格名称
     */
    @ApiModelProperty(value = "规格名称")
    private String modelName;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String unit;
    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    private BigDecimal goodsNum;
    /**
     * 商品单价
     */
    @ApiModelProperty(value = "商品单价")
    private BigDecimal goodsPrice;
    /**
     * 商品总价
     */
    @ApiModelProperty(value = "商品总价")
    private BigDecimal goodsAmount;
    /**
     * 退货状态1:待审核2:审核不通过3:审核通过4:退货中5:已退货6:已完成
     */
    @ApiModelProperty(value = "退货状态1:待审核2:审核不通过3:审核通过4:退货中5:已退货6:已完成")
    private ReturnStatusEnum returnStatus;
    /**
     * 退货时间
     */
    @ApiModelProperty(value = "退货时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime returnTime;

    /**
     * 收货数量
     */
    private BigDecimal receiptNum;
    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @ApiModelProperty(value = "删除标记")
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;

}
