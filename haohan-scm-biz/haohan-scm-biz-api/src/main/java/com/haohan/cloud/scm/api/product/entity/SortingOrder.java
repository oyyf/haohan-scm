/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-13 18:15:05
 */
@Data
@TableName("scm_pws_sorting_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分拣单")
public class SortingOrder extends Model<SortingOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 分拣单编号
     */
    private String sortingOrderSn;
    /**
     * 客户采购单编号
     */
    private String buyId;
    /**
     * 客户id
     */
    private String buyerId;
    /**
     * 客户名称
     */
    private String buyerName;
    /**
     * 分拣点编号
     */
    private String sortingPlaceSn;
    /**
     * 分拣点名称
     */
    private String sortingPlaceName;
    /**
     * 分拣截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deadlineTime;
    /**
     * 分拣状态:1待分拣2.可分拣3已分拣4已完成
     */
    private SortingStatusEnum sortingStatus;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 送货批次:0第一批1第二批
     */
    private BuySeqEnum deliverySeq;
    /**
     * 装车点编号
     */
    private String deliveryPlaceSn;
    /**
     * 装车点名称
     */
    private String deliveryPlaceName;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
