package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ReceiveTypeEnumConverterUtil implements Converter<ReceiveTypeEnum> {
    @Override
    public ReceiveTypeEnum convert(Object o, ReceiveTypeEnum receiveTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReceiveTypeEnum.getByType(o.toString());
    }
}
