package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskListResp;

/**
 * @author cx
 * @date 2019/6/6
 */
public class PurchaseTaskListTrans {
  public static PurchaseTaskListResp getPurchaseTaskListReq(PurchaseTask task, PurchaseOrderDetail detail){
    PurchaseTaskListResp resp = new PurchaseTaskListResp();
    resp.setId(task.getId());
    resp.setPmId(task.getPmId());
    resp.setPurchaseSn(detail.getPurchaseSn());
    resp.setPurchaseDetailSn(detail.getPurchaseDetailSn());
    resp.setDeadlineTime(task.getDeadlineTime());
    resp.setTaskStatus(task.getTaskStatus());
    resp.setTaskActionType(task.getTaskActionType());
    resp.setGoodsImg(detail.getGoodsImg());
    resp.setGoodsCategoryName(detail.getGoodsCategoryName());
    resp.setGoodsName(detail.getGoodsName());
    resp.setUnit(detail.getUnit());
    resp.setNeedBuyNum(detail.getNeedBuyNum());
    resp.setMethodType(detail.getMethodType());
    resp.setModelName(detail.getModelName());
    resp.setTransactorId(task.getTransactorId());
    resp.setTransactorName(task.getTransactorName());
    resp.setPurchaseStatus(detail.getPurchaseStatus());
    resp.setRealBuyNum(detail.getRealBuyNum());
    resp.setMarketPrice(detail.getMarketPrice());
    resp.setBuyPrice(detail.getBuyPrice());
    return resp;
  }
}
