package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/27
 */
@Getter
@AllArgsConstructor
public enum DataReportStatusEnum implements IBaseEnum {

    /**
     * 上报状态 0.待确认 1.已确认 2.不通过
     */
    wait("0", "待确认"),
    confirm("1", "已确认"),
    failed("2", "不通过");

    private static final Map<String, DataReportStatusEnum> MAP = new HashMap<>(8);

    static {
        for (DataReportStatusEnum e : DataReportStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DataReportStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
