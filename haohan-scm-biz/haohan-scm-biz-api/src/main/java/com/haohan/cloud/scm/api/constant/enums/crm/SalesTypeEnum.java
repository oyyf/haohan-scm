package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/8/31
 */
@Getter
@AllArgsConstructor
public enum SalesTypeEnum implements IBaseEnum {

    /**
     * 销售单类型:1代客下单,2自主下单
     */
    help("1", "代客下单"),
    self("2", "自主下单");

    private static final Map<String, SalesTypeEnum> MAP = new HashMap<>(8);

    static {
        for (SalesTypeEnum e : SalesTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SalesTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
