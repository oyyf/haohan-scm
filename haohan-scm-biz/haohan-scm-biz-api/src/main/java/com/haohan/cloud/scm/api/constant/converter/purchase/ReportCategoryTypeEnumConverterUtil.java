package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReportCategoryTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class ReportCategoryTypeEnumConverterUtil implements Converter<ReportCategoryTypeEnum> {
    @Override
    public ReportCategoryTypeEnum convert(Object o, ReportCategoryTypeEnum reportCategoryTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReportCategoryTypeEnum.getByType(o.toString());
    }
}
