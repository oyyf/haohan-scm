package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/8/5
 */
@Getter
@AllArgsConstructor
public enum ReviewStatusEnum implements IBaseEnum {
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    wait("1", "待审核"),
    failed("2", "审核不通过"),
    success("3", "审核通过");

    private static final Map<String, ReviewStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ReviewStatusEnum e : ReviewStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ReviewStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
