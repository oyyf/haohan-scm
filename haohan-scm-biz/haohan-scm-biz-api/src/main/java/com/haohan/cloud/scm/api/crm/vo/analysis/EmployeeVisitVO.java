package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/1/3
 */
@Data
public class EmployeeVisitVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "拜访总数")
    private Integer totalNum;

    @ApiModelProperty(value = "今日拜访数")
    private Integer todayNum;


    public EmployeeVisitVO(Integer totalNum, Integer todayNum) {
        this.totalNum = null == totalNum ? 0 : totalNum;
        this.todayNum = null == todayNum ? 0 : todayNum;
    }
}
