package com.haohan.cloud.scm.api.manage.trans;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegFromEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsRegTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.PdsUpassportStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/6/10
 */
@Data
public class UpassportTrans {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    /**
     * 初始化 设置状态
     *
     * @param uPassport
     * @return
     */
    public static UPassport initUpassport(UPassport uPassport) {
        uPassport.setId(null);
        // 密码无时初始
        String password = uPassport.getPassword();
        password = StrUtil.isEmpty(password) ? ScmCommonConstant.PASSWORD : password;
        uPassport.setPassword(ENCODER.encode(password));
        uPassport.setRegTime(LocalDateTime.now());
        if (null == uPassport.getRegType()) {
            uPassport.setRegType(PdsRegTypeEnum.telephone);
        }
        if (null == uPassport.getRegFrom()) {
            uPassport.setRegFrom(PdsRegFromEnum.web);
        }
        if (null == uPassport.getStatus()) {
            uPassport.setStatus(PdsUpassportStatusEnum.enable);
        }
        if (null == uPassport.getIsTest()) {
            uPassport.setIsTest(YesNoEnum.no);
        }
        return uPassport;
    }
}
