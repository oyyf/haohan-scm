/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 平台商品定价
 *
 * @author haohan
 * @date 2019-05-30 10:19:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "平台商品定价")
public class PlatformGoodsPriceReq extends PlatformGoodsPrice {

    private long pageSize;
    private long pageNo;




}
