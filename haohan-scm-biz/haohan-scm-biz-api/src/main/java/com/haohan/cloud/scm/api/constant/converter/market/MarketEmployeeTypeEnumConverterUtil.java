package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.MarketEmployeeTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class MarketEmployeeTypeEnumConverterUtil implements Converter<MarketEmployeeTypeEnum> {
    @Override
    public MarketEmployeeTypeEnum convert(Object o, MarketEmployeeTypeEnum marketEmployeeTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MarketEmployeeTypeEnum.getByType(o.toString());
    }
}
