package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum DeliverySeqEnum {
    /**
     * 送货批次:0第一批1第二批
     */
    batch("0","第一批"),
    nikeAirGriffey("1","第二批");

    private static final Map<String, DeliverySeqEnum> MAP = new HashMap<>(8);

    static {
        for (DeliverySeqEnum e : DeliverySeqEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DeliverySeqEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
