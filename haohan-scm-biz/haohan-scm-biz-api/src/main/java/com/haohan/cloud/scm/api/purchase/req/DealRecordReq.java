/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 账户扣减记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "账户扣减记录")
public class DealRecordReq extends DealRecord {

    private long pageSize;
    private long pageNo;




}
