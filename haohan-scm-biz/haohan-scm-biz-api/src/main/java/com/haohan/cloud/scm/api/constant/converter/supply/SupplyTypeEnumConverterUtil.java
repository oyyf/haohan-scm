package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class SupplyTypeEnumConverterUtil implements Converter<SupplyTypeEnum> {
    @Override
    public SupplyTypeEnum convert(Object o, SupplyTypeEnum supplyTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SupplyTypeEnum.getByType(o.toString());
    }
}
