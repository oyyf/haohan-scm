package com.haohan.cloud.scm.api.wechat.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/8/20
 */

@Data
public class QeuryStorageWarnReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "uid不能为空")
    private String uid;

    @NotBlank(message = "supplierId不能为空")
    private String supplierId;
}
