package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.CustomerImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class CustomerImportReq {

    @NotEmpty(message = "客户列表不能为空")
    private List<CustomerImport> customerList;

}
