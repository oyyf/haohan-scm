package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "查询市场详情列表请求")
public class PurchaseQueryMarketPriceListReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" , required = true)
    private String pmId;

    //TODO 暂时为不能为空
    @NotBlank(message = "goodsCategoryId不能为空")
    @ApiModelProperty(value = "商品分类id",required = true)
    private String goodsCategoryId;

    @ApiModelProperty(value = "用户id",required = true)
    private String uid;

    @ApiModelProperty(value = "供应商评级：1星-5星")
    private String supplierLevel;

    @ApiModelProperty(value = "每页个数")
    private long size = 10;

    @ApiModelProperty(value = "页数")
    private long current = 1;

}
