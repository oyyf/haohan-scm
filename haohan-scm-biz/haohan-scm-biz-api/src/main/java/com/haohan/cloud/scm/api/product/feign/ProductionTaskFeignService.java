/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.ProductionTaskReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 生产任务表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductionTaskFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface ProductionTaskFeignService {


    /**
     * 通过id查询生产任务表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductionTask/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 生产任务表 列表信息
     * @param productionTaskReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductionTask/fetchProductionTaskPage")
    R getProductionTaskPage(@RequestBody ProductionTaskReq productionTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 生产任务表 列表信息
     * @param productionTaskReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductionTask/fetchProductionTaskList")
    R getProductionTaskList(@RequestBody ProductionTaskReq productionTaskReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/add")
    R save(@RequestBody ProductionTask productionTask, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/update")
    R updateById(@RequestBody ProductionTask productionTask, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除生产任务表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/ProductionTask/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/countByProductionTaskReq")
    R countByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/getOneByProductionTaskReq")
    R getOneByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productionTaskList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductionTask/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductionTask> productionTaskList, @RequestHeader(SecurityConstants.FROM) String from);


}
