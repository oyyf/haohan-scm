/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.purchase.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 采购汇报
 *
 * @author haohan
 * @date 2019-05-13 18:52:53
 */
@Data
@TableName("scm_pms_purchase_report")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购汇报")
public class PurchaseReport extends Model<PurchaseReport> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 汇报类型1.日常汇报2.订单采购汇报
     */
    private String reportType;
    /**
     * 汇报分类1.货品不足2.价格贵3.质量差4.其他
     */
    private String reportCategoryType;
    /**
     * 汇报状态1.已汇报2.已反馈3.已处理
     */
    private String reportStatus;
    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 汇报人
     */
    private String reporterId;
    /**
     * 汇报人名称
     */
    private String reporterName;
    /**
     * 汇报内容
     */
    private String reportContent;
    /**
     * 汇报时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reportTime;
    /**
     * 汇报图片
     */
    private String reportPhoto;
    /**
     * 反馈人
     */
    private String answerId;
    /**
     * 反馈人名称
     */
    private String answerName;
    /**
     * 反馈内容
     */
    private String answerContent;
    /**
     * 反馈时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime answerTime;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
