package com.haohan.cloud.scm.api.bill.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "账单基础属性")
public abstract class BaseBill<T extends Model> extends BaseEntity<T> {
    private static final long serialVersionUID = 1L;

    /**
     * 账单编号
     */
    protected String billSn;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    protected ReviewStatusEnum reviewStatus;
    /**
     * 账单类型:
     * 应收:11订单应收 12退货应收 13销售应收
     * 应付:21采购应付 22退货应付
     */
    protected BillTypeEnum billType;
    /**
     * 结算单编号
     */
    protected String settlementSn;
    /**
     * 结算状态 是否支付 0否1是
     */
    protected YesNoEnum settlementStatus;

    // 订单相关
    /**
     * 平台商家ID
     */
    protected String pmId;
    /**
     * 平台商家名称
     */
    protected String pmName;
    /**
     * 来源订单编号
     */
    protected String orderSn;

    @ApiModelProperty(value = "下单客户id", notes = "采购商、供应商")
    protected String customerId;

    @ApiModelProperty(value = "下单客户名称", notes = "采购商、供应商")
    protected String customerName;

    @ApiModelProperty(value = "客户商家id")
    protected String merchantId;

    @ApiModelProperty(value = "客户商家名称")
    protected String merchantName;

    @ApiModelProperty(value = "订单成交日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected LocalDate dealDate;

    // 金额相关

    @ApiModelProperty(value = "账单金额", notes = "用于结算的金额")
    protected BigDecimal billAmount;

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    protected YesNoEnum advanceFlag;

    @ApiModelProperty(value = "预付金额")
    protected BigDecimal advanceAmount;

    @ApiModelProperty(value = "订单金额", notes = "对应订单总金额")
    private BigDecimal orderAmount;

}
