package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum ReportStatusEnum implements IBaseEnum {

    /**
     * 汇报状态1.已汇报2.已反馈3.已处理
     */
    already("1", "已汇报"),
    feedback("2", "已反馈"),
    processed("3", "已处理");
    private static final Map<String, ReportStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ReportStatusEnum e : ReportStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ReportStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
