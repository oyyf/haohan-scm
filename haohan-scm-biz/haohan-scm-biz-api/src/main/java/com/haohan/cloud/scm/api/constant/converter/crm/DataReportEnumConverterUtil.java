package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;

/**
 * @author dy
 * @date 2019/9/27
 */
public class DataReportEnumConverterUtil implements Converter<DataReportEnum> {
    @Override
    public DataReportEnum convert(Object o, DataReportEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DataReportEnum.getByType(o.toString());
    }

}