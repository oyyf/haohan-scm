package com.haohan.cloud.scm.api.supply.req.supplier;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/16
 */
@Data
public class SupplierMerchantInfoReq {

    @NotBlank(message = "供应商id不能为空")
    @Length(max = 32, message = "供应商id长度最大32字符")
    @ApiModelProperty("供应商id")
    private String supplierId;

    @Length(max = 32, message = "供应商id长度最大32字符")
    @ApiModelProperty("供应商userId")
    private String userId;

}
