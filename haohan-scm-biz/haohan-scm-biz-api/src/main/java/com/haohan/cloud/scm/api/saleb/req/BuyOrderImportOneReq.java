package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/9/17
 */
@Data
@ApiModel("导入1个B客户订单")
public class BuyOrderImportOneReq {

    @NotNull(message = "excel文件不能为空")
    private MultipartFile file;
    @NotEmpty(message = "采购商不能为空")
    private String buyerId;

    /**
     * 配送日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "配送日期不能为空")
    private LocalDate deliveryTime;

    /**
     * 配送批次
     */
    @NotNull(message = "配送批次不能为空")
    private BuySeqEnum buySeq;

    /**
     * 配送方式
     */
    private String deliveryType;

    /**
     * 采购需求
     */
    @Length(min=0, max=100, message = "采购需求的字符长度必须在0至100之间")
    private String needNote;



}
