/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.req;

import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 物流配送
 *
 * @author haohan
 * @date 2019-06-05 09:10:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "物流配送")
public class DeliveryFlowReq extends DeliveryFlow {

    private long pageSize;
    private long pageNo;


}
