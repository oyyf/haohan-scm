package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/27
 * 原汇总单状态
 */
@Getter
@AllArgsConstructor
public enum PdsSummaryStatusEnum {

    /**
     * 原汇总单状态0待报价1已报价2平台确认3成交4取消
     */
    wait("0", "待报价"),
    offered("1", "已报价"),
    confirm("2", "平台确认"),
    deal("3", "成交"),
    cancel("4", "取消");

    private static final Map<String, PdsSummaryStatusEnum> MAP = new HashMap<>(8);

    static {
        for (PdsSummaryStatusEnum e : PdsSummaryStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsSummaryStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;
  private String desc;

}
