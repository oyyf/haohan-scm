package com.haohan.cloud.scm.api.common.req.admin;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author shenyu
 * @create 2018/12/14
 */
public class PdsSortOutApiReq extends PdsBaseApiReq {
    @NotBlank(message = "missing param tradeId")
    private String tradeId;
    @NotNull(message = "missing param sortOutNum")
    private BigDecimal sortOutNum;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public BigDecimal getSortOutNum() {
        return sortOutNum;
    }

    public void setSortOutNum(BigDecimal sortOutNum) {
        this.sortOutNum = sortOutNum;
    }
}
