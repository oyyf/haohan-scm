/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 货位信息表
 *
 * @author haohan
 * @date 2019-05-13 21:26:37
 */
@Data
@TableName("scm_pws_cell")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货位信息表")
public class Cell extends Model<Cell> {
  private static final long serialVersionUID = 1L;

  /**
   * 主键
   */
  @TableId(type = IdType.INPUT)
  private String id;
  /**
   * 平台商家id
   */
  private String pmId;
  /**
   * 仓库编号
   */
  private String warehouseSn;
  /**
   * 货架编号
   */
  private String shelfSn;
  /**
   * 货位编号
   */
  private String cellSn;
  /**
   * 货位名称
   */
  private String cellName;
  /**
   * 描述
   */
  private String cellDesc;
  /**
   * 货架行号
   */
  private String shelfRow;
  /**
   * 货架列号
   */
  private String shelfColumn;
  /**
   * 启用状态:0.未启用1.启用
   */
  private UseStatusEnum useStatus;
  /**
   * 存货状态:1.空闲2.使用中
   */
  private StorageStatusEnum storageStatus;
  /**
   * 创建者
   */
  @TableField(fill = FieldFill.INSERT)
  private String createBy;
  /**
   * 创建时间
   */
  @TableField(fill = FieldFill.INSERT)
  @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createDate;
  /**
   * 更新者
   */
  @TableField(fill = FieldFill.UPDATE)
  private String updateBy;
  /**
   * 更新时间
   */
  @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @TableField(fill = FieldFill.UPDATE)
  private LocalDateTime updateDate;
  /**
   * 备注信息
   */
  private String remarks;
  /**
   * 删除标记
   */
  @TableLogic
  @TableField(fill = FieldFill.INSERT)
  private String delFlag;
  /**
   * 租户id
   */
  private Integer tenantId;

}
