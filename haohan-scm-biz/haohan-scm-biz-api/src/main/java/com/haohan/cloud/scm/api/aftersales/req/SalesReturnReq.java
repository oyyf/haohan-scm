/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-30 10:28:49
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "售后退货记录")
public class SalesReturnReq extends SalesReturn {

    private long pageSize;
    private long pageNo;




}
