package com.haohan.cloud.scm.api.constant.enums.bill;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/5/11
 * 订单状态
 */
@Getter
@AllArgsConstructor
public enum OrderStatusEnum implements IBaseEnum {

    /**
     * 订单状态： 1.已下单、2.待确认、3.成交、4.取消
     */
    submit("1", "已下单"),
    /**
     * 指平台处理至客户收货 这段状态内
     */
    wait("2", "待确认"),
    success("3", "成交"),
    cancel("4", "取消");

    private static final Map<String, OrderStatusEnum> MAP = new HashMap<>(8);

    static {
        for (OrderStatusEnum e : OrderStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static OrderStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
