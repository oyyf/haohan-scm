package com.haohan.cloud.scm.api.crm.req.customer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/7
 */
@Data
public class CheckCustomerExistReq {

    @Length(max = 32, message = "客户名称的长度最大32字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 20, message = "联系人手机的长度最大20字符")
    @ApiModelProperty(value = "联系人手机")
    private String telephone;

}
