/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-28 20:54:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "分拣单")
public class SortingOrderReq extends SortingOrder {

    private long pageSize;
    private long pageNo;




}
