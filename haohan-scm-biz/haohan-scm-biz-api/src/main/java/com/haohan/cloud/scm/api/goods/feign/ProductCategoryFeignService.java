/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.ProductCategory;
import com.haohan.cloud.scm.api.goods.req.ProductCategoryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品库分类内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ProductCategoryFeignService", value = ScmServiceName.SCM_GOODS)
public interface ProductCategoryFeignService {


    /**
     * 通过id查询商品库分类
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/ProductCategory/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 商品库分类 列表信息
     *
     * @param productCategoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductCategory/fetchProductCategoryPage")
    R getProductCategoryPage(@RequestBody ProductCategoryReq productCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品库分类 列表信息
     *
     * @param productCategoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/ProductCategory/fetchProductCategoryList")
    R getProductCategoryList(@RequestBody ProductCategoryReq productCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增商品库分类
     *
     * @param productCategory 商品库分类
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/add")
    R save(@RequestBody ProductCategory productCategory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改商品库分类
     *
     * @param productCategory 商品库分类
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/update")
    R updateById(@RequestBody ProductCategory productCategory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除商品库分类
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/countByProductCategoryReq")
    R countByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param productCategoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/getOneByProductCategoryReq")
    R getOneByProductCategoryReq(@RequestBody ProductCategoryReq productCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param productCategoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/ProductCategory/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<ProductCategory> productCategoryList, @RequestHeader(SecurityConstants.FROM) String from);


}
