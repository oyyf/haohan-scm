package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/21
 */

@Data
public class AddReturnOrderByOfferReq {

    private List<OfferOrder> list;

    private String pmId;

    private BigDecimal otherAmount;
}
