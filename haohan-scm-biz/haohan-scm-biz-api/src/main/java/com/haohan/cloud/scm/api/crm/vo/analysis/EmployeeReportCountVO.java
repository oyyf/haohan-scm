package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("员工日报数分析(部门)")
public class EmployeeReportCountVO {

    @ApiModelProperty(value = "当前部门员工数")
    private Integer employeeNum;

    @ApiModelProperty(value = "日报已交员工数")
    private Integer finishNum;

    @ApiModelProperty(value = "日报未交员工数")
    private Integer waitNum;

    @ApiModelProperty(value = "当前部门员工日报数列表")
    List<EmployeeReportVO> currentList;

}
