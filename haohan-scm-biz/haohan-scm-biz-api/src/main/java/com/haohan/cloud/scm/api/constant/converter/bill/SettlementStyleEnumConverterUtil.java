package com.haohan.cloud.scm.api.constant.converter.bill;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;

/**
 * @author dy
 * @date 2019/11/26
 */
public class SettlementStyleEnumConverterUtil implements Converter<SettlementStyleEnum> {
    @Override
    public SettlementStyleEnum convert(Object o, SettlementStyleEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SettlementStyleEnum.getByType(o.toString());
    }

}