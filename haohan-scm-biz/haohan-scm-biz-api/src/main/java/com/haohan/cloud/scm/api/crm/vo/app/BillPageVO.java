package com.haohan.cloud.scm.api.crm.vo.app;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BillPageVO<T> extends Page<T> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总计欠款")
    private BigDecimal totalDebt;

    public BillPageVO(long total, BigDecimal totalDebt){
        super();
        this.setTotal(total);
        this.totalDebt = totalDebt;
    }

    public void setPage(long current, long size) {
        this.setCurrent(current);
        this.setSize(size);
    }

    public void copyProperty(BillPageVO entity) {
        this.setCurrent(entity.getCurrent());
        this.setSize(entity.getSize());
        this.setTotal(entity.getTotal());
        this.setTotalDebt(entity.getTotalDebt());
    }

}
