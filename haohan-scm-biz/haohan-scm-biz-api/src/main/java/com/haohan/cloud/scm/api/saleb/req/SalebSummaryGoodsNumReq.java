package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/5/28
 */
@Data
@ApiModel(description = "查看单商品购买需求")
public class SalebSummaryGoodsNumReq {
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;
    /**
     * 送货批次
     */
    @NotBlank(message = "buySeq不能为空")
    @ApiModelProperty(value = "送货批次", required = true)
    private String buySeq;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "deliveryTime不能为空")
    @ApiModelProperty(value = "送货日期", required = true, example = "2019-05-28")
    private LocalDate deliveryTime;
    /**
     * 是否已汇总 0否1是
     */
    @ApiModelProperty(value = "是否已汇总:0否1是", example = "0")
    private String summaryFlag;
    /**
     * 采购单状态：1.已下单2.待确认3.成交4.取消
     */
    @ApiModelProperty(value = "采购单状态1.已下单2.待确认3.成交4.取消", example = "1")
    private String status;
    /**
     * 商品规格id
     */
//    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

}
