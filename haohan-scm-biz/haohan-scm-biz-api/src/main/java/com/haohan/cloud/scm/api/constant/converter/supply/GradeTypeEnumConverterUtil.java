package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class GradeTypeEnumConverterUtil implements Converter<GradeTypeEnum> {
    @Override
    public GradeTypeEnum convert(Object o, GradeTypeEnum gradeTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return GradeTypeEnum.getByType(o.toString());
    }

}
