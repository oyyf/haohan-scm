package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/5/16
 */
@Data
public class PurchaseEmployeeBillReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;
    @NotBlank(message = "employeeId不能为空")
    private String employeeId;
    private long size = 10;
    private long current = 1;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDealTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDealTime;

}
