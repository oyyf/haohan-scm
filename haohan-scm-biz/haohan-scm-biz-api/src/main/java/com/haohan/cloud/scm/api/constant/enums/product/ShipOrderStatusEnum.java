package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/27
 */
@Getter
@AllArgsConstructor
public enum ShipOrderStatusEnum implements IBaseEnum {
    /**
     * 配送状态:1.待配送 2.配送中 3.已送达
     */
    wait("1","待配送"),
    delivery("2","配送中"),
    success("3","已送达");

    private static final Map<String, ShipOrderStatusEnum> MAP = new HashMap<>(8);

    static {
        for (ShipOrderStatusEnum e : ShipOrderStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ShipOrderStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
