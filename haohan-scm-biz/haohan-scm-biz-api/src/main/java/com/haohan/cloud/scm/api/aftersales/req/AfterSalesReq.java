/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 售后单
 *
 * @author haohan
 * @date 2019-05-30 10:24:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "售后单")
public class AfterSalesReq extends AfterSales {

    private long pageSize;
    private long pageNo;




}
