/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.WechatMessageRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 微信消息记录表
 *
 * @author haohan
 * @date 2019-05-28 20:06:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "微信消息记录表")
public class WechatMessageRecordReq extends WechatMessageRecord {

    private long pageSize;
    private long pageNo;




}
