package com.haohan.cloud.scm.api.saleb.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/7/8
 */
@Data
@ApiModel("触发修改采购单状态为wait")
public class BuyOrderChangeStatusReq {

    @ApiModelProperty(value = "平台商家id", required = true)
    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @ApiModelProperty(value = "间隔时间", required = true)
    @NotNull(message = "minute不能为空")
    private Integer minute;


}
