/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.MerchantAppExt;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商家应用扩展信息
 *
 * @author haohan
 * @date 2019-05-28 20:35:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商家应用扩展信息")
public class MerchantAppExtReq extends MerchantAppExt {

    private long pageSize;
    private long pageNo;




}
