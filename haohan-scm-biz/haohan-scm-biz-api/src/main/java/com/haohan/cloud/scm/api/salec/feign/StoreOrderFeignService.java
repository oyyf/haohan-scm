/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.StoreOrder;
import com.haohan.cloud.scm.api.salec.req.StoreOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: C端订单表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "StoreOrderFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface StoreOrderFeignService {


  /**
   * 通过id查询订单表
   *
   * @param id id
   * @return R
   */
  @RequestMapping(value = "/api/feign/StoreOrder/{id}", method = RequestMethod.GET)
  R<StoreOrder> getById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 分页查询 订单表 列表信息
   *
   * @param storeOrderReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreOrder/fetchStoreOrderPage")
  R<Page<StoreOrder>> getStoreOrderPage(@RequestBody StoreOrderReq storeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 全量查询 订单表 列表信息
   *
   * @param storeOrderReq 请求对象
   * @return
   */
  @PostMapping("/api/feign/StoreOrder/fetchStoreOrderList")
  R<List<StoreOrder>> getStoreOrderList(@RequestBody StoreOrderReq storeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 新增订单表
   *
   * @param storeOrder 订单表
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/add")
  R<Boolean> save(@RequestBody StoreOrder storeOrder, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 修改订单表
   *
   * @param storeOrder 订单表
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/update")
  R<Boolean> updateById(@RequestBody StoreOrder storeOrder, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 通过id删除订单表
   *
   * @param id id
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/delete/{id}")
  R<Boolean> removeById(@PathVariable("id") Integer id, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/batchDelete")
  R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量查询（根据IDS）
   *
   * @param idList 主键ID列表
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/listByIds")
  R<List<StoreOrder>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据 Wrapper 条件，查询总记录数
   *
   * @param storeOrderReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/countByStoreOrderReq")
  R<Integer> countByStoreOrderReq(@RequestBody StoreOrderReq storeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 根据对象条件，查询一条记录
   *
   * @param storeOrderReq 实体对象,可以为空
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/getOneByStoreOrderReq")
  R<StoreOrder> getOneByStoreOrderReq(@RequestBody StoreOrderReq storeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 批量修改OR插入
   *
   * @param storeOrderList 实体对象集合 大小不超过1000条数据
   * @return R
   */
  @PostMapping("/api/feign/StoreOrder/saveOrUpdateBatch")
  R<Boolean> saveOrUpdateBatch(@RequestBody List<StoreOrder> storeOrderList, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 查询订单列表
   *
   * @param storeOrderReq
   * @param from
   * @return List<OrderMonitorResp>
   */
  @PostMapping("/api/feign/StoreOrder/fetchStoreOrderByTime")
  R<List> getStoreOrderByTime(@RequestBody StoreOrderReq storeOrderReq, @RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 订单支付后更新订单状态
     * @param req
     * @param from
     * @return
     */
  @PostMapping("/api/feign/StoreOrder/afterPayOrderUpdOrderStatus")
  R<Boolean> afterPayOrderUpdOrderStatus(@RequestBody StoreOrderReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
