package com.haohan.cloud.scm.api.saleb.req.buyer;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/12
 */
@Data
public class BuyerEditReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "采购商id不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "采购商id的长度最大为32字符")
    @ApiModelProperty(value = "采购商id")
    private String id;

    @NotBlank(message = "商家ID不能为空")
    @Length(min = 0, max = 64, message = "商家ID长度必须介于 0 和 64 之间")
    private String merchantId;

    @NotBlank(message = "采购商全称不能为空")
    @Length(min = 0, max = 64, message = "采购商全称长度必须介于 0 和 64 之间")
    private String buyerName;

    @Length(min = 0, max = 64, message = "采购商简称长度必须介于 0 和 64 之间")
    private String shortName;

    @NotBlank(message = "联系人不能为空")
    @Length(min = 0, max = 64, message = "联系人长度必须介于 0 和 64 之间")
    private String contact;

    @NotBlank(message = "电话不能为空")
    @Length(min = 0, max = 15, message = "电话长度必须介于 0 和 15 之间")
    private String telephone;

    @NotBlank(message = "采购商地址不能为空")
    @Length(min = 0, max = 64, message = "采购商地址长度必须介于 0 和 64 之间")
    private String address;

    @NotNull(message = "账期不能为空")
    @ApiModelProperty(value = "账期")
    private PayPeriodEnum payPeriod;

    /**
     * 账期日：  设置账期对应的结算日，使用数字，最大2位
     */
    @Length(min = 0, max = 2, message = "账期日长度必须介于 0 和 2 之间")
    private String payDay;

    @ApiModelProperty(value = "启用状态")
    private UseStatusEnum status;

    @ApiModelProperty(value = "采购商类型")
    private BuyerTypeEnum buyerType;

    @ApiModelProperty(value = "是否开启消息推送")
    private YesNoEnum needPush;

    @Length(min = 0, max = 5, message = "排序值长度必须介于 0 和 5 之间")
    private String sort;

    @ApiModelProperty(value = "是否限制下单")
    private YesNoEnum needConfirmation;

    @ApiModelProperty(value = "是否需下单支付")
    private YesNoEnum needPay;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "采购商地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @Length(min = 0, max = 64, message = "地址定位长度在0至64之间")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @Length(min = 0, max = 32, message = "地址定位区域长度在0至32之间")
    @ApiModelProperty(value = "地址定位区域")
    private String area;

    @Length(min = 0, max = 255, message = "备注长度在0至255之间")
    private String remarks;

    // 扩展

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "角色")
    private String roleIds;

    public Buyer transTo() {
        Buyer buyer = new Buyer();
        buyer.setId(this.id);
        buyer.setMerchantId(this.merchantId);
        buyer.setBuyerName(this.buyerName);
        buyer.setShortName(this.shortName);
        buyer.setContact(this.contact);
        buyer.setTelephone(this.telephone);
        buyer.setAddress(this.address);
        buyer.setPayPeriod(this.payPeriod);
        // 账期日设置 1-31
        String payDay = StrUtil.blankToDefault(this.payDay, "1");
        int num;
        try {
            num = Math.max(1, NumberUtil.parseInt(payDay));
        } catch (Exception e) {
            num = 1;
        }
        num = Math.min(num, 31);
        buyer.setPayDay(String.valueOf(num));
        buyer.setStatus(null == this.status ? UseStatusEnum.enabled : this.status);
        buyer.setBuyerType(null == this.buyerType ? BuyerTypeEnum.employee : this.buyerType);
        buyer.setNeedPush(null == this.needPush ? YesNoEnum.no : this.needPush);
        buyer.setSort(StrUtil.blankToDefault(this.sort, "100"));
        buyer.setNeedConfirmation(null == this.needConfirmation ? YesNoEnum.no : this.needConfirmation);
        buyer.setNeedPay(null == this.needPay ? YesNoEnum.yes : this.needPay);
        // 定位处理
        if (StrUtil.isNotEmpty(this.position)) {
            String[] post = StrUtil.split(this.position, StrUtil.COMMA);
            buyer.setLongitude(post[0]);
            buyer.setLatitude(post[1]);
        }
        buyer.setArea(this.area);
        buyer.setRemarks(this.remarks);
        return buyer;
    }


}
