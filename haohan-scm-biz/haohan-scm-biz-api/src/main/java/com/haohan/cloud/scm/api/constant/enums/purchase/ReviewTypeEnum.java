package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum ReviewTypeEnum implements IBaseEnum {

    no_audit("1","不审核"),
    need_audit("2","需审核");

    private static final Map<String, ReviewTypeEnum> MAP = new HashMap<>(8);

    static {
      for (ReviewTypeEnum e : ReviewTypeEnum.values()) {
        MAP.put(e.getType(), e);
      }
    }

    @JsonCreator
    public static ReviewTypeEnum getByType(String type) {
      if (MAP.containsKey(type)) {
        return MAP.get(type);
      }
      return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
