package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class PayTypeEnumConverterUtil implements Converter<PayTypeEnum> {
    @Override
    public PayTypeEnum convert(Object o, PayTypeEnum payTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PayTypeEnum.getByType(o.toString());
    }
}
