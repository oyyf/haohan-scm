package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.constant.enums.goods.ModelAttrNameEnum;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/30
 */
@Data
@NoArgsConstructor
public class GoodsModelDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 规格价格
     */
    private BigDecimal modelPrice;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 规格单位
     */
    private String modelUnit;
    /**
     * 规格库存
     */
    private BigDecimal modelStorage;
    /**
     * 规格商品图片地址
     */
    private String modelUrl;
    /**
     * 扩展信息
     */
    private String modelInfo;
    /**
     * 扫码购编码
     */
    private String modelCode;
    /**
     * 规格组合
     */
    private String model;
//    /**
//     * 即速应用规格ID
//     */
//    private String itemsId;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 商品规格唯一编号
     */
    private String goodsModelSn;
    /**
     * 商品规格通用编号/公共商品库通用编号
     */
    private String modelGeneralSn;
//    /**
//     * 第三方规格编号/即速商品id
//     */
//    private String thirdModelSn;
    /**
     * 参考成本价
     */
    private BigDecimal costPrice;
    /**
     * 重量
     */
    private BigDecimal weight;
    /**
     * 体积
     */
    private BigDecimal volume;
    /**
     * 库存预警值
     */
    private Integer stocksForewarn;
    /**
     * 备注信息
     */
    private String remarks;

    // 导入时使用

    @ApiModelProperty(value = "规格属性  属性名：属性值")
    private Map<ModelAttrNameEnum, String> modelInfoMap;

// 商品信息

    /**
     * 商品唯一编号
     */
    private String goodsSn;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品描述
     */
    private String detailDesc;
    /**
     * 概要描述
     */
    private String simpleDesc;
    /**
     * 商品品牌ID
     */
    private String brandId;
    /**
     * 是否上架 YesNoEnum  0.否 1.是
     */
    private Integer isMarketable;

// 商品分类

    /**
     * 商品分类
     */
    private String goodsCategoryId;
    /**
     * 商品分类名称
     */
    private String categoryName;

    // 店铺
    /**
     * 零售店铺Id
     */
    private String shopId;

    private String merchantId;

    /**
     * 促销价
     */
    private BigDecimal salePrice;

    public GoodsModelDTO(GoodsModel model){
        this.id = model.getId();
        this.goodsId = model.getGoodsId();
        this.modelPrice = model.getModelPrice();
        this.modelName = model.getModelName();
        this.modelUnit = model.getModelUnit();
        this.modelStorage = model.getModelStorage();
        this.modelUrl = model.getModelUrl();
        this.modelInfo = model.getModelInfo();
        this.modelCode = model.getModelCode();
        this.model = model.getModel();
//        this.itemsId = model.getItemsId();
        this.virtualPrice = model.getVirtualPrice();
        this.goodsModelSn = model.getGoodsModelSn();
        this.modelGeneralSn = model.getModelGeneralSn();
//        this.thirdModelSn = model.getThirdModelSn();
        this.costPrice = model.getCostPrice();
        this.weight = model.getWeight();
        this.volume = model.getVolume();
        this.stocksForewarn = model.getStocksForewarn();
    }

    public GoodsModel transTo() {
        GoodsModel model = new GoodsModel();
        model.setId(this.id);
        model.setGoodsId(this.goodsId);
        model.setModelPrice(this.modelPrice);
        model.setModelName(this.modelName);
        model.setModelUnit(this.modelUnit);
        model.setModelStorage(this.modelStorage);
        model.setModelUrl(this.modelUrl);
        model.setModelInfo(this.modelInfo);
        model.setModelCode(this.modelCode);
        model.setModel(this.model);
//        model.setItemsId(this.itemsId);
        model.setVirtualPrice(this.virtualPrice);
        model.setGoodsModelSn(this.goodsModelSn);
        model.setModelGeneralSn(this.modelGeneralSn);
//        model.setThirdModelSn(this.thirdModelSn);
        model.setCostPrice(this.costPrice);
        model.setWeight(this.weight);
        model.setVolume(this.volume);
        model.setStocksForewarn(this.stocksForewarn);
        return model;
    }

    public GoodsModelVO transToVO() {
        GoodsModelVO model = new GoodsModelVO();
        model.setModelId(this.id);
        model.setGoodsId(this.goodsId);
        model.setModelPrice(this.modelPrice);
        model.setModelName(this.modelName);
        model.setGoodsName(this.goodsName);
        model.setModelUnit(this.modelUnit);
        model.setModelStorage(this.modelStorage);
        model.setModelUrl(this.modelUrl);
        model.setModelInfo(this.modelInfo);
        model.setModelCode(this.modelCode);
        model.setModel(this.model);
//        model.setItemsId(this.itemsId);
        model.setVirtualPrice(this.virtualPrice);
        model.setGoodsModelSn(this.goodsModelSn);
//        model.setModelGeneralSn(this.modelGeneralSn);
//        model.setThirdModelSn(this.thirdModelSn);
        model.setCostPrice(this.costPrice);
        model.setWeight(this.weight);
        model.setVolume(this.volume);
        model.setStocksForewarn(this.stocksForewarn);
        return model;
    }
}
