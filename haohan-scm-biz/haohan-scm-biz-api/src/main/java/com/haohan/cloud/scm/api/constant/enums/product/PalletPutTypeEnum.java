package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum PalletPutTypeEnum {
  good_location("1","货位"),
  temporary("2","暂存点"),
  other("3","其他");

    private static final Map<String, PalletPutTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PalletPutTypeEnum e : PalletPutTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PalletPutTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

  @EnumValue
  @JsonValue
  private String type;

  private String desc;
}
