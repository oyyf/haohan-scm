package com.haohan.cloud.scm.api.constant.converter.supply;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class PdsOfferTypeEnumConverterUtil implements Converter<PdsOfferTypeEnum> {
    @Override
    public PdsOfferTypeEnum convert(Object o, PdsOfferTypeEnum pdsOfferTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PdsOfferTypeEnum.getByType(o.toString());
    }
}
