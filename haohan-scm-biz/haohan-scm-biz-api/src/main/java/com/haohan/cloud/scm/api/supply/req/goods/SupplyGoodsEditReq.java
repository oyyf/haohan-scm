package com.haohan.cloud.scm.api.supply.req.goods;

import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/18
 * 供应商品编辑
 * 取消关联: secondGroup
 */
@Data
public class SupplyGoodsEditReq {

    @ApiModelProperty(value = "供应商店铺id")
    private String shopId;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @NotBlank(message = "平台商品id不能为空", groups = {FirstGroup.class})
    @Length(max = 32, message = "平台商品id的最大长度为32字符")
    @ApiModelProperty(value = "平台商品id")
    private String platformGoodsId;

    @NotBlank(message = "平台商品id不能为空", groups = {FirstGroup.class})
    @Length(max = 32, message = "平台商品规格id的最大长度为32字符")
    @ApiModelProperty(value = "平台商品规格id")
    private String platformModelId;

    @NotBlank(message = "供应商品id不能为空")
    @Length(max = 32, message = "供应商品id的最大长度为32字符")
    @ApiModelProperty(value = "供应商品id")
    private String supplyGoodsId;

    @NotBlank(message = "平台商品id不能为空")
    @Length(max = 32, message = "供应商品规格id的最大长度为32字符")
    @ApiModelProperty(value = "供应商品规格id")
    private String supplyModelId;

}
