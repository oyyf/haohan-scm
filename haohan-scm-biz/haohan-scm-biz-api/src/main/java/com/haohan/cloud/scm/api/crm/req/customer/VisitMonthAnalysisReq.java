package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2019/10/28
 */
@Data
public class VisitMonthAnalysisReq {

    @NotBlank(message = "客户编号不能为空")
    @ApiModelProperty(value = "客户编号")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    private String customerSn;

    @Pattern(regexp = ScmCommonConstant.MONTH_PATTEN, message = "查询开始月份格式有误, yyyy-MM表示")
    @ApiModelProperty(value = "查询开始月份", notes = "yyyy-MM")
    private String startDate;

    @Pattern(regexp = ScmCommonConstant.MONTH_PATTEN, message = "查询结束月份格式有误, yyyy-MM表示")
    @ApiModelProperty(value = "查询结束月份", notes = "yyyy-MM")
    private String endDate;

}
