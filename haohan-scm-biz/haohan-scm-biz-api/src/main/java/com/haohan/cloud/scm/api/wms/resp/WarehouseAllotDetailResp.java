package com.haohan.cloud.scm.api.wms.resp;

import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class WarehouseAllotDetailResp extends WarehouseAllot {

    private List<WarehouseAllotDetail> details;
}
