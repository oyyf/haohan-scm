/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 数据汇报明细
 *
 * @author haohan
 * @date 2019-09-04 18:31:37
 */
@Data
@TableName("crm_data_report_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "数据汇报明细")
public class DataReportDetail extends Model<DataReportDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 上报编号
     */
    @ApiModelProperty(value = "上报编号")
    private String reportSn;
    /**
     * 商品规格id
     */
    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;
    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    /**
     * 规格属性
     */
    @ApiModelProperty(value = "规格属性")
    private String modelAttr;
    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String goodsUnit;
    /**
     * 商品图片
     */
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;
    /**
     * 商品规格编号
     */
    @ApiModelProperty(value = "商品规格编号")
    private String modelSn;
    /**
     * 条形码
     */
    @ApiModelProperty(value = "条形码")
    private String modelCode;
    /**
     * 批发价
     */
    @ApiModelProperty(value = "批发价")
    private BigDecimal tradePrice;
    /**
     * 保质期
     */
    @ApiModelProperty(value = "保质期")
    private String expiration;
    /**
     * 生产日期
     */
    @ApiModelProperty(value = "生产日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime productTime;
    /**
     * 到期日期
     */
    @ApiModelProperty(value = "到期日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime maturityTime;
    /**
     * 数量
     */
    @ApiModelProperty(value = "数量")
    private BigDecimal goodsNum;
    /**
     * 商品销售类型:1.普通2.促销品3.赠品
     */
    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
