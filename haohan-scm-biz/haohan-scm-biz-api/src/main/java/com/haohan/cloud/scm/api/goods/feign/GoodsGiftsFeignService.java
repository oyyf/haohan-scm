/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.GoodsGifts;
import com.haohan.cloud.scm.api.goods.req.GoodsGiftsReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 赠品内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsGiftsFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsGiftsFeignService {


    /**
     * 通过id查询赠品
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsGifts/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 赠品 列表信息
     *
     * @param goodsGiftsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsGifts/fetchGoodsGiftsPage")
    R getGoodsGiftsPage(@RequestBody GoodsGiftsReq goodsGiftsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 赠品 列表信息
     *
     * @param goodsGiftsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsGifts/fetchGoodsGiftsList")
    R getGoodsGiftsList(@RequestBody GoodsGiftsReq goodsGiftsReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增赠品
     *
     * @param goodsGifts 赠品
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/add")
    R save(@RequestBody GoodsGifts goodsGifts, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改赠品
     *
     * @param goodsGifts 赠品
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/update")
    R updateById(@RequestBody GoodsGifts goodsGifts, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除赠品
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/countByGoodsGiftsReq")
    R countByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsGiftsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/getOneByGoodsGiftsReq")
    R getOneByGoodsGiftsReq(@RequestBody GoodsGiftsReq goodsGiftsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param goodsGiftsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/GoodsGifts/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<GoodsGifts> goodsGiftsList, @RequestHeader(SecurityConstants.FROM) String from);


}
