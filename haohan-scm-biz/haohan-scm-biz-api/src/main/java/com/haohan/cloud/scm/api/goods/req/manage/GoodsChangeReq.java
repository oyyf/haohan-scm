package com.haohan.cloud.scm.api.goods.req.manage;

import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.ThreeGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/5/14
 * FirstGroup  修改上下架状态
 * SecondGroup  修改排序值
 * ThreeGroup  修改库存数量
 */
@Data
public class GoodsChangeReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "商品id不能为空")
    @Length(max = 32, message = "商品id的长度最大32字符")
    @ApiModelProperty("主键")
    private String goodsId;

    @NotNull(message = "上下架状态不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "上下架状态")
    private Boolean marketable;

    @NotNull(message = "排序值不能为空", groups = {SecondGroup.class})
    @Range(min = 0, max = 99999, message = "排序值在0至99999之间")
    @ApiModelProperty(value = "排序值")
    private Integer sort;

    @NotNull(message = "库存数量不能为空", groups = {ThreeGroup.class})
    @Digits(integer = 8, fraction = 2, message = "库存数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "库存数量")
    private BigDecimal storage;

}
