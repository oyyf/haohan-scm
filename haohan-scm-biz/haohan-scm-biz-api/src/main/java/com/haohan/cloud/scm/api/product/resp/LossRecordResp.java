package com.haohan.cloud.scm.api.product.resp;

import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import lombok.Data;

/**
 * @author cx
 * @date 2019/8/14
 */

@Data
public class LossRecordResp extends ProductLossRecord {

    private String prodcutName;
}
