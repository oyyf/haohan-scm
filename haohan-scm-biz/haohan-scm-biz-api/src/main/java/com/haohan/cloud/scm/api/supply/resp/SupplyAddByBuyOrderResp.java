package com.haohan.cloud.scm.api.supply.resp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2020/4/27
 */
@Data
public class SupplyAddByBuyOrderResp {

    @ApiModelProperty(value = "采购单编号")
    private String buyOrderSn;

    @ApiModelProperty(value = "操作结果状态", notes = "true 成功")
    private Boolean status;

    @ApiModelProperty(value = "操作结果信息")
    private String msg;

}
