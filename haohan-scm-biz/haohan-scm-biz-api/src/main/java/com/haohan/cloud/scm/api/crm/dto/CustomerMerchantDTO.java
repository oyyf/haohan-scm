package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.crm.entity.CustomerMerchantRelation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2019/12/7
 */
@Data
@ApiModel(value = "crm客户商家信息")
@NoArgsConstructor
public class CustomerMerchantDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 客户id
     */
    @ApiModelProperty(value = "客户id")
    private String customerId;
    /**
     * 客户编号
     */
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;
    /**
     * 商家id
     */
    @ApiModelProperty(value = "商家id")
    private String merchantId;
    /**
     * 商家名称
     */
    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @ApiModelProperty(value = "平台商家ID")
    private String pmId;

    @ApiModelProperty(value = "平台商家名称")
    private String pmName;

    public CustomerMerchantDTO(CustomerMerchantRelation relation) {
        if (null != relation) {
            this.customerId = relation.getCustomerId();
            this.customerSn = relation.getCustomerSn();
            this.customerName = relation.getCustomerName();
            this.merchantId = relation.getMerchantId();
            this.merchantName = relation.getMerchantName();
        }
    }
}
