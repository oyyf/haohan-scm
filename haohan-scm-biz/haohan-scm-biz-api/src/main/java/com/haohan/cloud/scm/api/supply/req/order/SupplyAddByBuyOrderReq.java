package com.haohan.cloud.scm.api.supply.req.order;

import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author dy
 * @date 2020/4/26
 * 据采购单向供应商下单
 * SingleGroup  用于批量下单
 */
@Data
public class SupplyAddByBuyOrderReq {

    @NotBlank(message = "采购单编号不能为空")
    @Length(max = 32, message = "采购单编号的长度最大为32字符")
    @ApiModelProperty(value = "采购单编号")
    private String buyOrderSn;

    @NotEmpty(message = "采购单编号列表不能为空", groups = {SingleGroup.class})
    @ApiModelProperty(value = "采购单编号列表")
    private List<String> buyOrderSnList;

}
