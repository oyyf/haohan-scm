/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 送货单
 *
 * @author haohan
 * @date 2019-05-28 20:53:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "送货单")
public class ShipOrderReq extends ShipOrder {

    private long pageSize;
    private long pageNo;




}
