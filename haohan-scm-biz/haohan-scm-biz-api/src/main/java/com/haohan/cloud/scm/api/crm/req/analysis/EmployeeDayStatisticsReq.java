package com.haohan.cloud.scm.api.crm.req.analysis;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/9
 */
@Data
@ApiModel("员工单日数据统计、员工日常统计查询")
public class EmployeeDayStatisticsReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 20, message = "员工id字符长度在0至20之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "查询日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate queryDate;

}
