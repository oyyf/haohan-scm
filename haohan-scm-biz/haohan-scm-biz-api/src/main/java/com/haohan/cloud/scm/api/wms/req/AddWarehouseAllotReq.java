package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class AddWarehouseAllotReq extends WarehouseAllot {

    private List<ProductInfo> infos;
}
