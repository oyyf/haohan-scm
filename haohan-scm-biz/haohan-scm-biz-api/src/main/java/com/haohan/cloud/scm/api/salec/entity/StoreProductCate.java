/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品分类辅助表
 *
 * @author haohan
 * @date 2020-05-21 18:41:47
 */
@Data
@TableName("eb_store_product_cate")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品分类辅助表")
public class StoreProductCate extends Model<StoreProductCate> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键", notes = "多租户不同的数据库")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "分类id")
    private Integer cateId;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
