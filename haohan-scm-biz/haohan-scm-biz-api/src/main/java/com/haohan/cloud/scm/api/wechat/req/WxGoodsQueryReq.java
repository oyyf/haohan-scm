package com.haohan.cloud.scm.api.wechat.req;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.api.goods.req.manage.GoodsListReq;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/5/23
 * default 验证uid、shopId
 * SingleGroup 验证uid、goodsId
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WxGoodsQueryReq extends GoodsListReq {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "通行证id不能为空", groups = {Default.class, SingleGroup.class})
    @Length(max = 64, message = "通行证id长度最大64字符")
    private String uid;

    @ApiModelProperty(value = "分类树是否全展示")
    private Boolean allShow;

    @ApiModelProperty(value = "分页参数 当前页")
    private long current = 1;

    @ApiModelProperty(value = "分页参数 每页显示条数")
    private long size = 10;

    public GoodsFeignReq transToQuery() {
        GoodsFeignReq query = new GoodsFeignReq();
        BeanUtil.copyProperties(this, query);
        return query;
    }
}
