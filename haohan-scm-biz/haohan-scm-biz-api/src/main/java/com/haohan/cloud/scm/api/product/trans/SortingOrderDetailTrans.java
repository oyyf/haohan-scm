package com.haohan.cloud.scm.api.product.trans;

import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;

/**
 * @author cx
 * @date 2019/7/6
 */
public class SortingOrderDetailTrans {
    public static SortingOrderDetail createSortingOrderDetailTrans(BuyOrderDetail detail, ProductInfo info){
        SortingOrderDetail order = new SortingOrderDetail();
        order.setPmId(detail.getPmId());
        order.setBuyDetailSn(detail.getBuyDetailSn());
        order.setGoodsModelId(detail.getGoodsModelId());
        order.setGoodsName(detail.getGoodsName());
        order.setGoodsModelName(detail.getGoodsModel());
        order.setGoodsUnit(detail.getUnit());
        order.setPurchaseNumber(detail.getGoodsNum());

        String productName = info.getProductName();
        productName = (null == productName) ? detail.getGoodsName() + "|" + detail.getGoodsModel() : productName;
        String unit = info.getUnit();
        unit = (null == unit) ? detail.getUnit() : unit;
        order.setProductSn(info.getProductSn());
        order.setProductName(productName);
        order.setProductUnit(unit);

        order.setSortingStatus(SortingStatusEnum.wait);
        return order;
    }

    /**
     *
     */
    public static ShipOrderDetail createShipDetail(SortingOrderDetail detail){
        ShipOrderDetail shipDetail = new ShipOrderDetail();
        shipDetail.setPmId(detail.getPmId());
        shipDetail.setGoodsName(detail.getGoodsName());
        shipDetail.setGoodsNum(detail.getSortingNumber());
        shipDetail.setBuyDetailSn(detail.getBuyDetailSn());
        return shipDetail;
    }
}
