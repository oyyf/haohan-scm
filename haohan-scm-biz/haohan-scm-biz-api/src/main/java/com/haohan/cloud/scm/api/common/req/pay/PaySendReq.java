package com.haohan.cloud.scm.api.common.req.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zgw
 * @date 2018/5/28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PaySendReq extends BasePayParams {


  @JsonProperty("device_id")
  private String deviceId;

  @JsonProperty("order_desc")
  private String orderDesc;

  @JsonProperty("goods_name")
  private String goodsName;

  @JsonProperty("order_info")
  private OrderPayParamsExt orderInfo;

  @JsonProperty("order_detail")
  private List<GoodsOrderDetail> orderDetail;

  @JsonProperty("order_amount")
  private String orderAmount;

  @JsonProperty("buyer_id")
  private String buyerId;

  @JsonProperty("app_id")
  private String appId;

  @JsonProperty("auth_code")
  private String authCode;

  @JsonProperty("uuid")
  private String uuid;

  @JsonProperty("open_id")
  private String openId;

  @JsonProperty("notify_url")
  private String partnerNotifyUrl;

  private String uid;

}
