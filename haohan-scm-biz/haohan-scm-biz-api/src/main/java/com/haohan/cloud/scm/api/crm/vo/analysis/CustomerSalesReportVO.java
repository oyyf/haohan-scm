package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.haohan.cloud.scm.api.crm.dto.analysis.SalesReportAnalysisDTO;
import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysis;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2020/1/16
 * 客户销量上报分类统计
 */
@Data
@NoArgsConstructor
public class CustomerSalesReportVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "市场人员(所有上报员工名称)")
    private String employeeNames;

    @ApiModelProperty(value = "月份(yyyy-MM)")
    private String month;

    @ApiModelProperty(value = "销售金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "促销品(赠品总金额)")
    private BigDecimal giftAmount;

    @ApiModelProperty(value = "促销内容(赠送商品及数量)")
    private String giftContent;

    @ApiModelProperty(value = "分类统计明细列表")
    private List<GoodsAnalysisVO> detailList;


    public CustomerSalesReportVO(SalesReportAnalysis analysis) {
        this.employeeNames = analysis.getEmployeeNames();
        this.month = analysis.getMonth();
        this.totalAmount = analysis.getTotalAmount();
        this.giftAmount = analysis.getGiftAmount();
        this.giftContent = analysis.getGiftContent();
    }

    public CustomerSalesReportVO(SalesReportAnalysisDTO analysis) {
        this.employeeNames = analysis.getEmployeeNames();
        this.month = analysis.getMonth();
        this.totalAmount = analysis.getTotalAmount();
        this.giftAmount = analysis.getGiftAmount();
        this.giftContent = analysis.getGiftContent();
    }
}
