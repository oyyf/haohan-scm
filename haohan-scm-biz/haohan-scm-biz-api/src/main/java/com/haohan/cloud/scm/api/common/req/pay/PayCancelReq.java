package com.haohan.cloud.scm.api.common.req.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @program: haohan-fresh-scm
 * @description: 支付取消请求
 * @author: Simon
 * @create: 2019-07-26
 **/
@Data
public class PayCancelReq extends BasePayParams {

  @JsonProperty("org_trans_id")
  private String orgTransId;

  @JsonProperty("org_req_id")
  private String orgReqId;

}
