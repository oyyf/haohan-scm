/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.Shipper;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "发货人")
public class ShipperReq extends Shipper {

    private long pageSize;
    private long pageNo;

}
