/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 入库单
 *
 * @author haohan
 * @date 2019-05-28 19:04:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "入库单")
public class EnterWarehouseReq extends EnterWarehouse {

    private long pageSize;
    private long pageNo;




}
