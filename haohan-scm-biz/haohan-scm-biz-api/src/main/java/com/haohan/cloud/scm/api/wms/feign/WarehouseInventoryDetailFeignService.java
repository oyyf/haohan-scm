/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import com.haohan.cloud.scm.api.wms.req.WarehouseInventoryDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 库存盘点明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarehouseInventoryDetailFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WarehouseInventoryDetailFeignService {


    /**
     * 通过id查询库存盘点明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarehouseInventoryDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 库存盘点明细 列表信息
     * @param warehouseInventoryDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/fetchWarehouseInventoryDetailPage")
    R getWarehouseInventoryDetailPage(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 库存盘点明细 列表信息
     * @param warehouseInventoryDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/fetchWarehouseInventoryDetailList")
    R getWarehouseInventoryDetailList(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增库存盘点明细
     * @param warehouseInventoryDetail 库存盘点明细
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/add")
    R save(@RequestBody WarehouseInventoryDetail warehouseInventoryDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改库存盘点明细
     * @param warehouseInventoryDetail 库存盘点明细
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/update")
    R updateById(@RequestBody WarehouseInventoryDetail warehouseInventoryDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除库存盘点明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarehouseInventoryDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warehouseInventoryDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/countByWarehouseInventoryDetailReq")
    R countByWarehouseInventoryDetailReq(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warehouseInventoryDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/getOneByWarehouseInventoryDetailReq")
    R getOneByWarehouseInventoryDetailReq(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warehouseInventoryDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventoryDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarehouseInventoryDetail> warehouseInventoryDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
