package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/8/19
 */
@Data
@ApiModel(description = "查询应收账单列表")
public class BuyerPaymentListReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id",required = true)
    private String pmId;

    @ApiModelProperty(value = "订单成交日期,查询开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate beginDate;

    @ApiModelProperty(value = "订单成交日期,查询结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家名称
     */
    private String merchantName;

    /**
     * 应收来源订单编号
     */
    private String receivableSn;
    /**
     * 应收货款记录编号
     */
    private String buyerPaymentId;

    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 账单类型: 1订单应收 2退款应收
     */
    private BillTypeEnum billType;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;

    public BuyerPayment transTo(){
        // 只设置 eq条件
        BuyerPayment payment = new BuyerPayment();
        payment.setPmId(this.getPmId());
        payment.setReceivableSn(this.getReceivableSn());
        payment.setBuyerPaymentId(this.getBuyerPaymentId());
        payment.setStatus(this.getStatus());
        payment.setBillType(this.getBillType());
        payment.setReviewStatus(this.getReviewStatus());
        return payment;
    }
}
