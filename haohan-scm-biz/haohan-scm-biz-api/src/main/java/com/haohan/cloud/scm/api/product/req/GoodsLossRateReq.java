/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 单品损耗率
 *
 * @author haohan
 * @date 2019-05-28 20:50:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "单品损耗率")
public class GoodsLossRateReq extends GoodsLossRate {

    private long pageSize;
    private long pageNo;




}
