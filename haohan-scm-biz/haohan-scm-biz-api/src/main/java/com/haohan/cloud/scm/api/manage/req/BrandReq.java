/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.Brand;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 品牌管理
 *
 * @author haohan
 * @date 2019-05-28 20:33:48
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "品牌管理")
public class BrandReq extends Brand {

    private long pageSize;
    private long pageNo;




}
