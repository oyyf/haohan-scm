package com.haohan.cloud.scm.api.crm.req.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("客户数按类型统计")
public class CustomerTypeCountReq {

    @ApiModelProperty(value = "客户负责人id")
    @Length(max = 32, message = "客户负责人id长度最大32字符")
    private String directorId;

}
