package com.haohan.cloud.scm.api.supply.trans;

import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;

/**
 * @author dy
 * @date 2019/6/6
 */
public class OfferOrderTrans {

    public static OfferOrder copyFromGoodsModelDTO(OfferOrder order, GoodsModelDTO goodsModel){
        order.setGoodsModelId(goodsModel.getId());
        // 兼容原系统
        order.setGoodsId(goodsModel.getId());
        order.setModelName(goodsModel.getModelName());
        order.setGoodsName(goodsModel.getGoodsName());
        order.setGoodsImg(goodsModel.getModelUrl());
        order.setUnit(goodsModel.getModelUnit());
        return order;
    }

}
