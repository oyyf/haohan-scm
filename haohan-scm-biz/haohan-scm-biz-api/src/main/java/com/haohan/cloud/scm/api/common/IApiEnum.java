package com.haohan.cloud.scm.api.common;

import com.haohan.cloud.scm.common.tools.http.MethodType;

import java.io.Serializable;

/**
 * @program: haohan-fresh-scm
 * @description: 接口参数枚举
 * @author: Simon
 * @create: 2019-07-10
 **/
public interface IApiEnum extends Serializable {

   MethodType getMethodType();

   String getUrl();

}
