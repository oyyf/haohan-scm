package com.haohan.cloud.scm.api.wms.resp;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/7/8
 */
@Data
@ApiModel("查询待出库列表结果")
public class QueryWaitExitResp {

    private List<ExitDetailInfoResp> list;

}
