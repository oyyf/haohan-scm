/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购汇报
 *
 * @author haohan
 * @date 2019-05-29 13:35:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购汇报")
public class PurchaseReportReq extends PurchaseReport {

    private long pageSize;
    private long pageNo;




}
