package com.haohan.cloud.scm.api.crm.req.market;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.market.ContractStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.SalesContract;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/26
 */
@Data
public class EditSalesContractReq {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;

    /**
     * 合同状态:1待签订2已签订3未签订
     */
    @NotNull(message = "合同状态不能为空")
    @ApiModelProperty(value = "合同状态:1待签订2已签订3未签订")
    private ContractStatusEnum contractStatus;
    /**
     * 合同描述
     */
    @ApiModelProperty(value = "合同描述")
    @Length(min = 0, max = 64, message = "合同描述长度在0至64之间")
    private String contractDesc;
    /**
     * 客户编号
     */
    @NotNull(message = "客户编号不能为空")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    @Length(min = 0, max = 32, message = "客户名称长度在0至32之间")
    private String customerName;
    /**
     * 客户联系人id
     */
    @Length(min = 0, max = 32, message = "客户联系人id长度在0至32之间")
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;
    /**
     * 客户联系人姓名
     */
    @ApiModelProperty(value = "客户联系人姓名")
    @Length(min = 0, max = 32, message = "客户联系人姓名长度在0至32之间")
    private String linkmanName;
    /**
     * 销售机会id
     */
    @ApiModelProperty(value = "销售机会id")
    @Length(min = 0, max = 32, message = "销售机会id长度在0至32之间")
    private String salesLeadsId;
    /**
     * 合同金额
     */
    @NotNull(message = "合同金额不能为空")
    @Digits(integer = 8, fraction = 2, message = "合同金额的整数位最大8位, 小数位最大2位")
    @Range(min = 0, max = 1000000, message = "合同金额在0至1000000之间")
    @ApiModelProperty(value = "合同金额")
    private BigDecimal contractAmount;
    /**
     * 签约时间
     */
    @ApiModelProperty(value = "签约时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime contractTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedTime;
    /**
     * 条件条款
     */
    @ApiModelProperty(value = "条件条款")
    @Length(min = 0, max = 65535, message = "条件条款长度在0至65535之间")
    private String conditions;
    /**
     * 合同图片 图片组编号
     */
    @ApiModelProperty(value = "合同图片组编号")
    @Length(min = 0, max = 32, message = "合同图片组编号长度在0至32之间")
    private String photoGroupNum;
    /**
     * 销售分润比例
     */
    @ApiModelProperty(value = "销售分润比例")
    private BigDecimal salesRate;
    /**
     * 市场负责人id
     */
    @ApiModelProperty(value = "市场负责人id")
    @Length(min = 0, max = 32, message = "市场负责人id长度在0至32之间")
    private String directorId;
    /**
     * 市场负责人名称
     */
    @ApiModelProperty(value = "市场负责人名称")
    @Length(min = 0, max = 32, message = "市场负责人名称长度在0至32之间")
    private String directorName;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    @ApiModelProperty(value = "合同图片列表")
    private List<String> photoList;

    public SalesContract transTo() {
        SalesContract contract = new SalesContract();
        BeanUtil.copyProperties(this, contract);
        return contract;
    }

}
