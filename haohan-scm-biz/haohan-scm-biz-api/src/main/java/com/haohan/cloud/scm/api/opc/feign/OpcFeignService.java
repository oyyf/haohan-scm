package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author dy
 * @date 2019/5/27
 */
@FeignClient(contextId = "OpcFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface OpcFeignService {

    /**
     * 查询 汇总单
     * 通过 id summaryOrderId
     *
     * @param summaryOrder
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/opc/summaryOrder/queryOne")
    R queryOneSummaryOrder(@RequestBody SummaryOrder summaryOrder, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询 汇总单列表
     * 通过 时间 批次 等
     *
     * @param summaryOrder
     * @param from
     * @return
     */
    @PostMapping("/api/feign/opc/summaryOrder/queryList")
    R queryListSummaryOrder(@RequestBody SummaryOrder summaryOrder, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询交易单
     * 通过 id tradeId
     *
     * @param tradeOrder
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/opc/tradeOrder/queryOne")
    R queryOneTradeOrder(@RequestBody TradeOrder tradeOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询交易单列表
     * 通过 buyId 等
     *
     * @param tradeOrder
     * @param from
     * @return
     */
    @PostMapping("/api/feign/opc/tradeOrder/queryList")
    R queryListTradeOrder(@RequestBody TradeOrder tradeOrder, @RequestHeader(SecurityConstants.FROM) String from);

}
