/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.entity.PlatformGoodsPrice;
import com.haohan.cloud.scm.api.goods.req.PlatformGoodsPriceReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 平台商品定价内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PlatformGoodsPriceFeignService", value = ScmServiceName.SCM_GOODS)
public interface PlatformGoodsPriceFeignService {


    /**
     * 通过id查询平台商品定价
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PlatformGoodsPrice/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 平台商品定价 列表信息
     * @param platformGoodsPriceReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/fetchPlatformGoodsPricePage")
    R getPlatformGoodsPricePage(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 平台商品定价 列表信息
     * @param platformGoodsPriceReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/fetchPlatformGoodsPriceList")
    R getPlatformGoodsPriceList(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增平台商品定价
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/add")
    R save(@RequestBody PlatformGoodsPrice platformGoodsPrice, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改平台商品定价
     * @param platformGoodsPrice 平台商品定价
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/update")
    R updateById(@RequestBody PlatformGoodsPrice platformGoodsPrice, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除平台商品定价
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PlatformGoodsPrice/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/countByPlatformGoodsPriceReq")
    R countByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param platformGoodsPriceReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/getOneByPlatformGoodsPriceReq")
    R getOneByPlatformGoodsPriceReq(@RequestBody PlatformGoodsPriceReq platformGoodsPriceReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param platformGoodsPriceList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PlatformGoodsPrice/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PlatformGoodsPrice> platformGoodsPriceList, @RequestHeader(SecurityConstants.FROM) String from);


}
