package com.haohan.cloud.scm.api.crm.req.analysis;

import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("新增客户数日统计")
public class CustomerAnalysisReq {

    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @Length(max = 32, message = "客户负责人id长度最大32字符")
    @ApiModelProperty(value = "客户负责人id")
    private String directorId;

    @NotBlank(message = "客户编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "客户编号长度最大32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "查询开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty(value = "是否只查询app员工自己")
    private boolean selfFlag;

}
