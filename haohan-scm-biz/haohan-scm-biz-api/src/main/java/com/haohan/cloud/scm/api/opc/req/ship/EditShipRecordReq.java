package com.haohan.cloud.scm.api.opc.req.ship;

import com.haohan.cloud.scm.api.constant.enums.opc.ShipTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author dy
 * @date 2020/3/4
 * SingleGroup  开始发货操作
 */
@Data
public class EditShipRecordReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "发货记录编号不能为空", groups = {SingleGroup.class})
    @Length(max = 64, message = "发货记录编号长度最大64字符")
    @ApiModelProperty(value = "发货记录编号")
    private String shipRecordSn;

    @NotBlank(message = "发货人id不能为空", groups = {SingleGroup.class})
    @Length(max = 64, message = "发货人id长度最大64字符")
    @ApiModelProperty(value = "发货人id")
    private String shipperId;

    @NotNull(message = "发货人id不能为空", groups = {SingleGroup.class})
    @ApiModelProperty(value = "发货方式 1.第三方物流2.自配送")
    private ShipTypeEnum shipType;

    @Length(max = 64, message = "物流单号长度最大64字符")
    @ApiModelProperty(value = "物流单号")
    private String logisticsSn;

    @Length(max = 64, message = "物流公司长度最大64字符")
    @ApiModelProperty(value = "物流公司")
    private String logisticsName;

    public ShipRecord copyToEdit() {
        ShipRecord shipRecord = new ShipRecord();
        shipRecord.setShipType(this.shipType);
        shipRecord.setLogisticsSn(this.logisticsSn);
        shipRecord.setLogisticsName(this.logisticsName);
        return shipRecord;
    }
}
