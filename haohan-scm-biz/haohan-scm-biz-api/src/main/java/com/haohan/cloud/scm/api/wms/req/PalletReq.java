/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.Pallet;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 托盘信息表
 *
 * @author haohan
 * @date 2019-05-28 19:12:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "托盘信息表")
public class PalletReq extends Pallet {

    private long pageSize;
    private long pageNo;




}
