package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum MarketEmployeeTypeEnum implements IBaseEnum {
    /**
     * 市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人
     */
    marketingDirector("1", "市场总监"),
    regionalManager("2", "区域经理"),
    businessManager("3", "业务经理"),
    communityPartner("4", "社区合伙人");

    private static final Map<String, MarketEmployeeTypeEnum> MAP = new HashMap<>(8);
    private static final Map<String, MarketEmployeeTypeEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (MarketEmployeeTypeEnum e : MarketEmployeeTypeEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static MarketEmployeeTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    public static MarketEmployeeTypeEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
