package com.haohan.cloud.scm.api.wechat.vo;

import com.haohan.cloud.scm.api.manage.entity.UPassport;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/27
 * 小程序通行证信息
 */
@Data
@NoArgsConstructor
public class WxPassportVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "通行证id")
    private String uid;

    @ApiModelProperty(value = "通行证登录名称")
    private String name;

    public WxPassportVO(UPassport passport) {
        this.uid = passport.getId();
        this.name = passport.getLoginName();
    }
}
