package com.haohan.cloud.scm.api.common;

import cn.hutool.json.JSONUtil;
import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.common.tools.util.ApiCallUtil;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by zgw on 2019/5/22.
 */

@Service
@Configuration
public class BaseApiService {


  @Value("${pds.api.pdsUrl}")
  @Getter
  private String pdsUrl;

  @Value("${pds.api.payUrl}")
  @Getter
  private String payUrl;

  @Autowired
  private ApiCallUtil apiCallUtil;

  public BaseResp call(IApiEnum api, Object reqClass) {

    return apiCallUtil.apiInvoke(api.getMethodType(), pdsUrl.concat(api.getUrl()), reqClass);

  }


  public BaseResp callPay(IApiEnum api, Object reqClass) {

    return apiCallUtil.apiInvoke(api.getMethodType(), payUrl.concat(api.getUrl()), reqClass);

  }

  public BaseResp call(CommonApiConstant api, Object reqClass, HashMap<String, Object> header) {

    return apiCallUtil.apiInvoke(api.getMethodType(), pdsUrl.concat(api.getUrl()), reqClass, header);

  }

  public <Resp> Resp callResp(IApiEnum api, Class reqClass, Class<Resp> respClass) {

    BaseResp resp = call(api, reqClass);
    if (resp.isSuccess()) {
      return JSONUtil.toBean(resp.getExt().toString(), respClass);
    }
    return null;
  }


}


