package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("客户销售数据")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerSalesVO {

    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "回款金额")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "订单数")
    private Integer orderNum;

}
