/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import com.haohan.cloud.scm.api.purchase.req.LendingRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 放款申请记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "LendingRecordFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface LendingRecordFeignService {


    /**
     * 通过id查询放款申请记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/LendingRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 放款申请记录 列表信息
     * @param lendingRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/LendingRecord/fetchLendingRecordPage")
    R getLendingRecordPage(@RequestBody LendingRecordReq lendingRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 放款申请记录 列表信息
     * @param lendingRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/LendingRecord/fetchLendingRecordList")
    R getLendingRecordList(@RequestBody LendingRecordReq lendingRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/add")
    R save(@RequestBody LendingRecord lendingRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/update")
    R updateById(@RequestBody LendingRecord lendingRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除放款申请记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/LendingRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/countByLendingRecordReq")
    R countByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/getOneByLendingRecordReq")
    R getOneByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param lendingRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/LendingRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<LendingRecord> lendingRecordList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 请款记录列表
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/LendingRecord/queryLendingList")
    R queryLendingList(@RequestBody PurchaseQueryLendingListReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 请款记录详情
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/LendingRecord/queryLendingRecord")
    R queryLendingRecord(@RequestBody PurchaseQueryLendingReq req,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 请款审核
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/LendingRecord/auditLending")
    R auditLending(@RequestBody PurchaseAuditLendingReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
