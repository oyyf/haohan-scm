package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */

@Data
public class EditWarehouseAllotReq extends WarehouseAllot {

    private List<WarehouseAllotDetail> list;

    private List<WarehouseAllotDetail> delList;

    private List<ProductInfo> addList;
}
