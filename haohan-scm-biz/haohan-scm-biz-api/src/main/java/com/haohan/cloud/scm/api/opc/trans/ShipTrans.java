package com.haohan.cloud.scm.api.opc.trans;

import com.haohan.cloud.scm.api.bill.dto.OrderDetailDTO;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.ShipRecordStatusEnum;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import lombok.Data;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/3/4
 */
@Data
@UtilityClass
public class ShipTrans {

    public static ShipRecord initShipRecord(OrderInfoDTO order) {
        ShipRecord ship = new ShipRecord();
        ship.setPmId(order.getPmId());
        ship.setPmName(order.getPmName());
        // 发货单的客户对应的订单中的商家
        ship.setCustomerId(order.getMerchantId());
        ship.setCustomerName(order.getMerchantName());
        ship.setReceiverName(order.getContact());
        ship.setReceiverTelephone(order.getTelephone());
        ship.setReceiverAddress(order.getAddress());
        ship.setDeliveryDate(order.getDealDate());

        ship.setShipStatus(ShipRecordStatusEnum.wait);
        ship.setOrderSn(order.getOrderSn());
        ship.setOrderType(order.getOrderType());
        ship.setOrderTime(order.getOrderTime());
        ship.setTotalAmount(order.getTotalAmount());
        ship.setOtherAmount(order.getOtherAmount());
        ship.setSumAmount(order.getSumAmount());
        ship.setGoodsNum(order.getGoodsNum());
        ship.setRemarks(order.getRemarks());
        return ship;
    }

    public static ShipRecordDetail copyFromOrderDetail(OrderDetailDTO orderDetail) {
        ShipRecordDetail detail = new ShipRecordDetail();
        detail.setGoodsId(orderDetail.getGoodsId());
        detail.setGoodsModelId(orderDetail.getGoodsModelId());
        detail.setGoodsName(orderDetail.getGoodsName());
        detail.setModelName(orderDetail.getModelName());
        detail.setUnit(orderDetail.getUnit());
        detail.setGoodsImg(orderDetail.getGoodsImg());
        detail.setGoodsNum(orderDetail.getGoodsNum());
        detail.setDealPrice(orderDetail.getDealPrice());
        detail.setAmount(orderDetail.getAmount());
        detail.setSalesType(BigDecimal.ZERO.compareTo(orderDetail.getDealPrice()) == 0 ? SalesGoodsTypeEnum.gift : SalesGoodsTypeEnum.normal);
        return detail;
    }
}
