package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/20
 * 报价单状态
 */
@Getter
@AllArgsConstructor
public enum PdsOfferStatusEnum implements IBaseEnum {
    /**
     * 报价单状态: 待报价
     */
    stayOffer("1", "待报价"),
    /**
     * 报价单状态: 已报价
     */
    alreadyOffer("2", "已报价"),
    /**
     * 报价单状态: 中标
     */
    bidding("3", "中标"),
    /**
     * 报价单状态: 未中标
     */
    notBidding("4", "未中标");

    private static final Map<String, PdsOfferStatusEnum> MAP = new HashMap<>(8);

    static {
        for (PdsOfferStatusEnum e : PdsOfferStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PdsOfferStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
