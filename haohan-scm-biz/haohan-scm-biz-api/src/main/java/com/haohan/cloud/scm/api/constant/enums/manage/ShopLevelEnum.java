package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/14
 */
@Getter
@AllArgsConstructor
public enum ShopLevelEnum implements IBaseEnum {

    /**
     * 店铺等级 0 总店, 1 分店 2采购配送店
     */
    head("0","总店"),
    sub("1","子店"),
    pds("2","采购配送店")
    ;

    private static final Map<String, ShopLevelEnum> MAP = new HashMap<>(8);

    static {
        for (ShopLevelEnum e : ShopLevelEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ShopLevelEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
