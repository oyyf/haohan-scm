package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAnswerPurchaseReportReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    private String uid;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "采购汇报id",required = true)
    private String id;
    /**
     * 反馈人
     */
    private String answerId;
    /**
     * 反馈人名称
     */
    private String answerName;
    /**
     * 反馈内容
     */
    private String answerContent;
    /**
     * 反馈时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime answerTime;

}
