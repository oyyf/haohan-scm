package com.haohan.cloud.scm.api.crm.trans;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.crm.dto.AreaTree;
import com.haohan.cloud.scm.api.crm.entity.SalesArea;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2020/1/15
 */
@UtilityClass
public class CrmAreaTrans {


    /**
     * 区域列表转为 区域树节点列表
     *
     * @param areaList
     * @return
     */
    public List<AreaTree> fetchAreaNodeList(List<SalesArea> areaList) {
        return areaList.stream()
                // 正常数据父id不为自己
                .filter(area -> !area.getId().equals(area.getParentId()))
                // 节点排序 升序
                .sorted((c1, c2) -> StrUtil.compare(c1.getSort(), c2.getSort(), false))
                .map(area -> {
                    AreaTree node = new AreaTree();
                    node.setId(area.getId());
                    node.setParentId(area.getParentId());
                    node.setAreaName(area.getAreaName());
                    node.setAreaSn(area.getAreaSn());
                    node.setParentName(area.getParentName());
                    return node;
                }).collect(Collectors.toList());
    }

}
