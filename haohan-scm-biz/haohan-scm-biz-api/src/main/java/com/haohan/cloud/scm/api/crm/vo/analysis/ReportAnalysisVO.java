package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/11
 */
@Data
@Api("数据上报分析结果")
public class ReportAnalysisVO {

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "查询开始时间")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束时间")
    private LocalDate endDate;

}
