package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class UseStatusEnumConverterUtil implements Converter<UseStatusEnum>{
    @Override
    public UseStatusEnum convert(Object o, UseStatusEnum useStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return UseStatusEnum.getByType(o.toString());
    }

}
