package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/16
 * 商品分析统计类型
 */
@Getter
@AllArgsConstructor
public enum GoodsAnalysisTypeEnum implements IBaseEnum {

    /**
     * 商品分析统计类型:  1.规格2.分类
     */
    sku("1", "规格"),
    category("2", "分类");

    private static final Map<String, GoodsAnalysisTypeEnum> MAP = new HashMap<>(8);

    static {
        for (GoodsAnalysisTypeEnum e : GoodsAnalysisTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static GoodsAnalysisTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
