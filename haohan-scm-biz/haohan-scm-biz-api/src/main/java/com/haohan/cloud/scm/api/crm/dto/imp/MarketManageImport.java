package com.haohan.cloud.scm.api.crm.dto.imp;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.market.MarketTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.MarketManage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author dy
 * @date 2019/10/19
 */
@Data
public class MarketManageImport {

    @NotBlank(message = "市场名称不能为空")
    @Length(min = 0, max = 32, message = "市场名称的字符长度必须在1至32之间")
    @ApiModelProperty(value = "市场名称")
    private String marketName;

    @NotBlank(message = "销售区域不能为空")
    @Length(min = 0, max = 32, message = "销售区域的字符长度必须在1至32之间")
    @ApiModelProperty(value = "销售区域")
    private String areaName;

    @ApiModelProperty(value = "市场类型")
    private MarketTypeEnum marketType;

    @Length(min = 0, max = 64, message = "描述的字符长度必须在0至64之间")
    @ApiModelProperty(value = "标签")
    private String tags;

    @Length(min = 0, max = 255, message = "描述的字符长度必须在0至255之间")
    @ApiModelProperty(value = "描述")
    private String description;

    @Length(min = 0, max = 20, message = "电话的字符长度必须在0至20之间")
    @ApiModelProperty(value = "电话")
    private String phone;

    @NotBlank(message = "省不能为空")
    @Length(min = 0, max = 20, message = "省的字符长度必须在1至20之间")
    @ApiModelProperty(value = "省")
    private String province;

    @NotBlank(message = "市不能为空")
    @Length(min = 0, max = 20, message = "市的字符长度必须在1至20之间")
    @ApiModelProperty(value = "市")
    private String city;

    @Length(min = 0, max = 20, message = "区的字符长度必须在1至20之间")
    @ApiModelProperty(value = "区")
    private String district;

    @Length(min = 0, max = 20, message = "街道、乡镇的字符长度必须在0至20之间")
    @ApiModelProperty(value = "街道、乡镇")
    private String street;

    @NotBlank(message = "详细地址不能为空")
    @Length(min = 0, max = 64, message = "详细地址的字符长度必须在1至64之间")
    @ApiModelProperty(value = "详细地址")
    private String address;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @Length(min = 0, max = 255, message = "备注信息的字符长度必须在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public MarketManage transTo() {
        MarketManage market = new MarketManage();
        market.setMarketName(this.marketName);
        market.setMarketType(null == this.marketType ? MarketTypeEnum.one : this.marketType);
        market.setTags(this.tags);
        market.setDescription(this.description);
        market.setPhone(this.phone);
        market.setProvince(this.province);
        market.setCity(this.city);
        market.setDistrict(this.district);
        market.setStreet(this.street);
        market.setAddress(this.address);
        market.setPosition(this.position);
        market.setRemarks(this.remarks);
        return market;
    }
}
