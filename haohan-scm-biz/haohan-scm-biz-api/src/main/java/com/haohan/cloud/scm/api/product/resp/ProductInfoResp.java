package com.haohan.cloud.scm.api.product.resp;

import lombok.Data;

/**
 * @author xwx
 * @date 2019/5/27
 */
@Data
public class ProductInfoResp {
    private boolean flag = false;
}
