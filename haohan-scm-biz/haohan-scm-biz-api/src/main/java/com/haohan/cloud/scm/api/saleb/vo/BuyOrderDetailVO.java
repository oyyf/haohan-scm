package com.haohan.cloud.scm.api.saleb.vo;

import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/28
 */
@Data
@NoArgsConstructor
public class BuyOrderDetailVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 64, message = "汇总单号长度最大64字符")
    @ApiModelProperty(value = "汇总单号")
    private String summaryBuySn;

    @Length(max = 64, message = "采购编号长度最大64字符")
    @ApiModelProperty(value = "采购编号")
    private String buyOrderSn;

    @Length(max = 64, message = "采购明细编号长度最大64字符")
    @ApiModelProperty(value = "采购明细编号")
    private String buyDetailSn;

    @Length(max = 64, message = "采购商长度最大64字符")
    @ApiModelProperty(value = "采购商")
    private String buyerId;

    @Length(max = 64, message = "商品规格ID长度最大64字符")
    @ApiModelProperty(value = "商品规格ID")
    private String modelId;

    @Length(max = 500, message = "商品图片长度最大500字符")
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @Length(max = 64, message = "商品名称长度最大64字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 64, message = "商品规格名称长度最大64字符")
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @Digits(integer = 10, fraction = 2, message = "采购数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "采购数量")
    private BigDecimal goodsNum;

    @Digits(integer = 10, fraction = 2, message = "市场价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "市场价格")
    private BigDecimal marketPrice;

    @Digits(integer = 10, fraction = 2, message = "采购价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "采购价格")
    private BigDecimal buyPrice;

    @Length(max = 64, message = "单位长度最大64字符")
    @ApiModelProperty(value = "单位")
    private String unit;

    @ApiModelProperty(value = "状态")
    private BuyOrderStatusEnum status;

    @Length(max = 64, message = "零售单明细id长度最大64字符")
    @ApiModelProperty(value = "零售单明细id")
    private String goodsOrderDetailId;

    @Digits(integer = 10, fraction = 2, message = "商品下单数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "商品下单数量")
    private BigDecimal orderGoodsNum;

    @ApiModelProperty(value = "客户订单明细汇总状态:0未处理1已汇总2已备货")
    private DetailSummaryFlagEnum summaryFlag;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    public BuyOrderDetailVO(BuyOrderDetail detail) {
        this.summaryBuySn = detail.getSummaryBuyId();
        this.buyOrderSn = detail.getBuyId();
        this.buyDetailSn = detail.getBuyDetailSn();
        this.buyerId = detail.getBuyerId();
        this.modelId = detail.getGoodsModelId();
        this.goodsImg = detail.getGoodsImg();
        this.goodsName = detail.getGoodsName();
        this.modelName = detail.getGoodsModel();
        this.goodsNum = detail.getGoodsNum();
        this.marketPrice = detail.getMarketPrice();
        this.buyPrice = detail.getBuyPrice();
        this.unit = detail.getUnit();
        this.status = detail.getStatus();
        this.goodsOrderDetailId = detail.getGoodsOrderDetailId();
        this.orderGoodsNum = detail.getOrderGoodsNum();
        this.summaryFlag = detail.getSummaryFlag();
        this.remarks = detail.getRemarks();
    }

}
