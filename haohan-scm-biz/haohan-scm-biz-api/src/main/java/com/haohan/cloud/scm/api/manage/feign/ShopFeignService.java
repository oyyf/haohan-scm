/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.dto.ShopExtDTO;
import com.haohan.cloud.scm.api.manage.entity.Shop;
import com.haohan.cloud.scm.api.manage.req.ShopReq;
import com.haohan.cloud.scm.api.manage.vo.MerchantShopVO;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 店铺内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "ShopFeignService", value = ScmServiceName.SCM_MANAGE)
public interface ShopFeignService {


    /**
     * 通过id查询店铺
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Shop/{id}", method = RequestMethod.GET)
    R<Shop> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 店铺 列表信息
     * @param shopReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Shop/fetchShopPage")
    R getShopPage(@RequestBody ShopReq shopReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 店铺 列表信息
     * @param shopReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Shop/fetchShopList")
    R getShopList(@RequestBody ShopReq shopReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增店铺
     * @param shop 店铺
     * @return R
     */
    @PostMapping("/api/feign/Shop/add")
    R save(@RequestBody Shop shop, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改店铺
     * @param shop 店铺
     * @return R
     */
    @PostMapping("/api/feign/Shop/update")
    R updateById(@RequestBody Shop shop, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除店铺
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Shop/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Shop/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Shop/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Shop/countByShopReq")
    R countByShopReq(@RequestBody ShopReq shopReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shopReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Shop/getOneByShopReq")
    R getOneByShopReq(@RequestBody ShopReq shopReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param shopList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Shop/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Shop> shopList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 获取平台商家的采购配送店铺
     *
     * @param pmId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Shop/fetchPurchaseShop")
    R<ShopExtDTO> fetchPurchaseShop(@RequestParam(value = "pmId", required = false) String pmId, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询租户默认店铺
     * @param from
     * @return
     */
    @GetMapping("/api/feign/Shop/fetchDefaultShop")
    R<ShopExtDTO> fetchDefaultShop(@RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 获取商家店铺
     *
     * @param pmId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/Shop/fetchMerchantShop")
    R<MerchantShopVO> fetchMerchantShop(@RequestParam(value = "merchantId") String pmId, @RequestHeader(SecurityConstants.FROM) String from);

}
