package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/8/1
 */
@Data
@ApiModel(description = "批量创建应收账单")
public class CreateReceivableBatchReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotNull(message = "deliveryDate不能为空")
    @ApiModelProperty(value = "送货日期", required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @NotNull(message = "buySeq不能为空")
    @ApiModelProperty(value = "送货批次", required = true)
    private BuySeqEnum buySeq;

    public BuyOrder transTo() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setPmId(this.pmId);
        buyOrder.setDeliveryTime(deliveryDate);
        buyOrder.setBuySeq(this.buySeq);
        return buyOrder;
    }
}
