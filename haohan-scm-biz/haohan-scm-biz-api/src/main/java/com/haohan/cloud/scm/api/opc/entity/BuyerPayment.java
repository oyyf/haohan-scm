/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 应收账单   已废弃，使用bill模块
 *
 * @author haohan
 * @date 2019-05-13 20:35:09
 */
@Data
@TableName("scm_buyer_payment")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "应收账单")
public class BuyerPayment extends Model<BuyerPayment> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 采购编号 原系统使用 目前废弃
     */
    private String buyId;

    /**
     * 应收来源订单编号
     */
    private String receivableSn;
    /**
     * 采购商  => 账单归属人id(buyerId/supplierId)
     */
    private String buyerId;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家id
     */
    private String merchantId;
    /**
     * 客户商家名称
     */
    private String merchantName;
    /**
     * 订单成交日期 =>应收订单送货日期 / 退货单申请日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate buyDate;
    /**
     * 商品数量
     */
    private String goodsNum;
    /**
     * 应收货款
     */
    private BigDecimal buyerPayment;
    /**
     * 售后货款
     */
    private BigDecimal afterSalePayment;
    /**
     * 状态 是否结算 0否1是
     */
    private YesNoEnum status;
    /**
     * 运费
     */
    private BigDecimal shipFee;
    /**
     * 结算单编号
     */
    @TableField(value = "service_id")
    private String settlementSn;
    /**
     * 应收货款记录编号
     */
    private String buyerPaymentId;
    /**
     * 账单类型: 1订单应收 2退款应收
     */
    private BillTypeEnum billType;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
