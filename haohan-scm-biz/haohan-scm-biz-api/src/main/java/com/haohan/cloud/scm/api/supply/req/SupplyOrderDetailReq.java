/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应订单明细")
public class SupplyOrderDetailReq extends SupplyOrderDetail {

    private long pageSize;
    private long pageNo;

    @ApiModelProperty("商品规格id集合")
    private Set<String> modeIdSet;
}
