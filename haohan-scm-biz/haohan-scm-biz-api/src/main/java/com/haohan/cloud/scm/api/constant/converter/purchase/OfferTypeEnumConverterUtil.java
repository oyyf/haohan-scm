package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.OfferTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class OfferTypeEnumConverterUtil implements Converter<OfferTypeEnum> {
    @Override
    public OfferTypeEnum convert(Object o, OfferTypeEnum offerTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return OfferTypeEnum.getByType(o.toString());
    }
}
