package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/6
 */
@Data
@ApiModel(description = "新增入库单（入库单明细）根据采购单明细")
public class EnterWarehouseByOrderDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id", required = true)
    private String pmId;

    @NotBlank(message = "purchaseSn不能为空")
    @ApiModelProperty(value = "采购单编号", required = true)
    private String purchaseSn;

    @ApiModelProperty(value = "入库单号", required = true)
    private String enterWarehouseSn;

}
