/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.iot.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.iot.entity.CloudPrintTerminal;
import com.haohan.cloud.scm.api.iot.req.CloudPrintTerminalReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 云打印终端管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CloudPrintTerminalFeignService", value = ScmServiceName.SCM_IOT)
public interface CloudPrintTerminalFeignService {


    /**
     * 通过id查询云打印终端管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CloudPrintTerminal/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 云打印终端管理 列表信息
     *
     * @param cloudPrintTerminalReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CloudPrintTerminal/fetchCloudPrintTerminalPage")
    R getCloudPrintTerminalPage(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 云打印终端管理 列表信息
     *
     * @param cloudPrintTerminalReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CloudPrintTerminal/fetchCloudPrintTerminalList")
    R getCloudPrintTerminalList(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增云打印终端管理
     *
     * @param cloudPrintTerminal 云打印终端管理
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/add")
    R save(@RequestBody CloudPrintTerminal cloudPrintTerminal, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改云打印终端管理
     *
     * @param cloudPrintTerminal 云打印终端管理
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/update")
    R updateById(@RequestBody CloudPrintTerminal cloudPrintTerminal, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除云打印终端管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param cloudPrintTerminalReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/countByCloudPrintTerminalReq")
    R countByCloudPrintTerminalReq(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param cloudPrintTerminalReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/getOneByCloudPrintTerminalReq")
    R getOneByCloudPrintTerminalReq(@RequestBody CloudPrintTerminalReq cloudPrintTerminalReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param cloudPrintTerminalList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CloudPrintTerminal/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<CloudPrintTerminal> cloudPrintTerminalList, @RequestHeader(SecurityConstants.FROM) String from);


}
