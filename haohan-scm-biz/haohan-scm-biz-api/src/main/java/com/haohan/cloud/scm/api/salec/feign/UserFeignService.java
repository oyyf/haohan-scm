/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.salec.entity.MallUser;
import com.haohan.cloud.scm.api.salec.req.AfterPayUpdReq;
import com.haohan.cloud.scm.api.salec.req.CountUserSumReq;
import com.haohan.cloud.scm.api.salec.req.MallUserReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 用户表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "UserFeignService", value = ScmServiceName.SCM_BIZ_SALEC)
public interface UserFeignService {


    /**
     * 通过id查询用户表
     *
     * @param uid id
     * @return R
     */
    @RequestMapping(value = "/api/feign/User/{uid}", method = RequestMethod.GET)
    R<MallUser> getById(@PathVariable("uid") Integer uid, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 用户表 列表信息
     *
     * @param userReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/User/fetchUserPage")
    R<Page<MallUser>> getUserPage(@RequestBody MallUserReq userReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 用户表 列表信息
     *
     * @param userReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/User/fetchUserList")
    R<List<MallUser>> getUserList(@RequestBody MallUserReq userReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增用户表
     *
     * @param user 用户表
     * @return R
     */
    @PostMapping("/api/feign/User/add")
    R<Boolean> save(@RequestBody MallUser user, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改用户表
     *
     * @param user 用户表
     * @return R
     */
    @PostMapping("/api/feign/User/update")
    R<Boolean> updateById(@RequestBody MallUser user, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除用户表
     *
     * @param uid id
     * @return R
     */
    @PostMapping("/api/feign/User/delete/{uid}")
    R<Boolean> removeById(@PathVariable("uid") Integer uid, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/User/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/User/listByIds")
    R<List<MallUser>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param userReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/User/countByUserReq")
    R<Integer> countByUserReq(@RequestBody MallUserReq userReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param userReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/User/getOneByUserReq")
    R<MallUser> getOneByUserReq(@RequestBody MallUserReq userReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param userList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/User/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<MallUser> userList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 查询一段时间范围内用户数
     *
     * @param countUserSumReq
     * @param from
     * @return
     */
    @PostMapping("/api/feign/User/countRangeUserNum")
    R countRangeUserNum(@RequestBody CountUserSumReq countUserSumReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 支付后根据状态更新余额
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/User/orderPayUpdBalance")
    R orderPayUpdBalance(@RequestBody AfterPayUpdReq req,@RequestHeader(SecurityConstants.FROM)String from);
}
