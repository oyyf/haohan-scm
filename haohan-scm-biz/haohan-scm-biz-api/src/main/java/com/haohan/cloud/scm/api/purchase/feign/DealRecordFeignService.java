/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import com.haohan.cloud.scm.api.purchase.req.DealRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 账户扣减记录内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DealRecordFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface DealRecordFeignService {


    /**
     * 通过id查询账户扣减记录
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DealRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 账户扣减记录 列表信息
     * @param dealRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DealRecord/fetchDealRecordPage")
    R getDealRecordPage(@RequestBody DealRecordReq dealRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 账户扣减记录 列表信息
     * @param dealRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DealRecord/fetchDealRecordList")
    R getDealRecordList(@RequestBody DealRecordReq dealRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/add")
    R save(@RequestBody DealRecord dealRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/update")
    R updateById(@RequestBody DealRecord dealRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除账户扣减记录
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/DealRecord/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/countByDealRecordReq")
    R countByDealRecordReq(@RequestBody DealRecordReq dealRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/getOneByDealRecordReq")
    R getOneByDealRecordReq(@RequestBody DealRecordReq dealRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param dealRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DealRecord/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<DealRecord> dealRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
