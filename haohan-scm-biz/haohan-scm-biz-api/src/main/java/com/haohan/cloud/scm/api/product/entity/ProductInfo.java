/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.product.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.product.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 货品信息记录表1
 *
 * @author haohan
 * @date 2019-05-13 18:21:39
 */
@Data
@TableName("scm_pws_product_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货品信息记录表1")
public class ProductInfo extends Model<ProductInfo> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 商品id,spu
     */
    private String goodsId;
    /**
     * 商品规格id,sku
     */
    private String goodsModelId;
    /**
     * 货品编号
     */
    private String productSn;
    /**
     * 货品名称
     */
    private String productName;
    /**
     * 货品单位
     */
    private String unit;
    /**
     * 货品数量
     */
    private BigDecimal productNumber;
    /**
     * 货品状态1.正常2.损耗3.分装
     */
    private ProductStatusEnum productStatus;
    /**
     * 货品质量1.正常2.残次品3.废品
     */
    private ProductQialityEnum productQuality;
    /**
     * 货品位置状态:1.采购部2.生产部3.物流部4.门店5.客户
     */
    private ProductPlaceStatusEnum productPlaceStatus;
    /**
     * 货品类型:1.采购货品2.供应货品3.加工货品
     */
    private ProductTypeEnum productType;
    /**
     * 供应商id
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 入库单号
     */
    private String enterWarehouseSn;
    /**
     * 采购单明细编号
     */
    @ApiModelProperty(value = "采购单明细编号")
    private String purchaseDetailSn;
    /**
     * 采购价格/成本价
     */
    private BigDecimal purchasePrice;
    /**
     * 初始采购时间/生产日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime purchaseTime;
    /**
     * 货品有效期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime validityTime;
    /**
     * 加工操作类型:0不加工1去皮2.拆分3配方
     */
    private ProcessingTypeEnum processingType;
    /**
     * 来源货品编号
     */
    private String sourceProductSn;
    /**
     * 货品描述
     */
    private String productDesc;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
