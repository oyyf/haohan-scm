package com.haohan.cloud.scm.api.crm.req.picture;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/12/30
 */
@Data
public class QueryPictureRecordReq {


    @ApiModelProperty(value = "客户编号")
    @Length(max = 32, message = "客户编号的最大长度为32字符")
    private String customerSn;

    @ApiModelProperty(value = "拍照员工id")
    @Length(max = 32, message = "拍照员工id的最大长度为32字符")
    private String employeeId;

    // 非eq
    
    @ApiModelProperty(value = "客户名称")
    @Length(max = 32, message = "客户名称的最大长度为32字符")
    private String customerName;
    
    @ApiModelProperty(value = "拍照员工姓名")
    @Length(max = 32, message = "拍照员工姓名的最大长度为32字符")
    private String employeeName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期-开始日期")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期-结束日期")
    private LocalDate endDate;


}
