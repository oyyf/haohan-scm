/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.goods.dto.GoodsCategoryTree;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.req.GoodsCategoryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 商品分类内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsCategoryFeignService", value = ScmServiceName.SCM_GOODS)
public interface GoodsCategoryFeignService {


    /**
     * 通过id查询商品分类
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsCategory/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 分页查询 商品分类 列表信息
//     *
//     * @param goodsCategoryReq 请求对象
//     * @return
//     */
//    @PostMapping("/api/feign/GoodsCategory/fetchGoodsCategoryPage")
//    R getGoodsCategoryPage(@RequestBody GoodsCategoryReq goodsCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 商品分类 列表信息
     *
     * @param goodsCategoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsCategory/fetchGoodsCategoryList")
    R<List<GoodsCategory>> getGoodsCategoryList(@RequestBody GoodsCategoryReq goodsCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 新增商品分类
//     *
//     * @param goodsCategory 商品分类
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/add")
//    R save(@RequestBody GoodsCategory goodsCategory, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 修改商品分类
//     *
//     * @param goodsCategory 商品分类
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/update")
//    R updateById(@RequestBody GoodsCategory goodsCategory, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 通过id删除商品分类
//     *
//     * @param id id
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/delete/{id}")
//    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

//    /**
//     * 删除（根据ID 批量删除)
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/batchDelete")
//    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量查询（根据IDS）
//     *
//     * @param idList 主键ID列表
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/listByIds")
//    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 根据 Wrapper 条件，查询总记录数
//     *
//     * @param goodsCategoryReq 实体对象,可以为空
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/countByGoodsCategoryReq")
//    R countByGoodsCategoryReq(@RequestBody GoodsCategoryReq goodsCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 根据对象条件，查询一条记录
//     *
//     * @param goodsCategoryReq 实体对象,可以为空
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/getOneByGoodsCategoryReq")
//    R getOneByGoodsCategoryReq(@RequestBody GoodsCategoryReq goodsCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);


//    /**
//     * 批量修改OR插入
//     *
//     * @param goodsCategoryList 实体对象集合 大小不超过1000条数据
//     * @return R
//     */
//    @PostMapping("/api/feign/GoodsCategory/saveOrUpdateBatch")
//    R saveOrUpdateBatch(@RequestBody List<GoodsCategory> goodsCategoryList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 分类树查询
     *
     * @param goodsCategoryReq shopId必须
     * @param from
     * @return
     */
    @PostMapping("/api/feign/GoodsCategory/findCategoryTree")
    R<List<GoodsCategoryTree>> findCategoryTree(@RequestBody GoodsCategoryReq goodsCategoryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询分类及所有父级
     *
     * @param categoryId
     * @param shopId 验证分类的shopId
     * @param from
     * @return
     */
    @GetMapping("/api/feign/GoodsCategory/findAllParentWithSelfById")
    R<List<GoodsCategory>> findAllParentWithSelfById(@RequestParam("categoryId") String categoryId, @RequestParam("shopId") String shopId, @RequestHeader(SecurityConstants.FROM) String from);

}
