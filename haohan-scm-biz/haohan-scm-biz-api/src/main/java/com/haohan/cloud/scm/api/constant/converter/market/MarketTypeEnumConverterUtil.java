package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.MarketTypeEnum;

/**
 * @author dy
 * @date 2019/9/26
 */
public class MarketTypeEnumConverterUtil implements Converter<MarketTypeEnum> {
    @Override
    public MarketTypeEnum convert(Object o, MarketTypeEnum marketTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MarketTypeEnum.getByType(o.toString());
    }
}