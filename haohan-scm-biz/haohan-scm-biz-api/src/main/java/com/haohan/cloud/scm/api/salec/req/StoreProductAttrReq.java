/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreProductAttr;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品属性表
 *
 * @author haohan
 * @date 2019-06-19 17:42:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性表")
public class StoreProductAttrReq extends StoreProductAttr {

  private long pageSize;
  private long pageNo;


}
