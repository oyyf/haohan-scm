package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.BusinessTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class BusinessTypeEnumConverterUtil implements Converter<BusinessTypeEnum> {
    @Override
    public BusinessTypeEnum convert(Object o, BusinessTypeEnum businessTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return BusinessTypeEnum.getByType(o.toString());
    }
}
