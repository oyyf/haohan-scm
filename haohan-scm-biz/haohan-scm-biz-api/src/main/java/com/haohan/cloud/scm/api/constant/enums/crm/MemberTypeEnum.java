package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/12/10
 */
@Getter
@AllArgsConstructor
public enum MemberTypeEnum implements IBaseEnum {

    /**
     * 会员类型 1普通，2年卡
     */
    normal("1", "普通"),
    year("2", "年卡");

    private static final Map<String, MemberTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MemberTypeEnum e : MemberTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MemberTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
