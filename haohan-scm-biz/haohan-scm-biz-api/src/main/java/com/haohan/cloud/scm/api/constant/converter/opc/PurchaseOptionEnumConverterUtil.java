package com.haohan.cloud.scm.api.constant.converter.opc;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.opc.PurchaseOptionEnum;

/**
 * @author dy
 * @date 2019/6/1
 */
public class PurchaseOptionEnumConverterUtil implements Converter<PurchaseOptionEnum> {

    @Override
    public PurchaseOptionEnum convert(Object o, PurchaseOptionEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PurchaseOptionEnum.getByType(o.toString());
    }

}
