package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
@Api("app查询经营情况")
public class BusinessAppVO {

    @ApiModelProperty(value = "客户数")
    private Integer customerNum;

    @ApiModelProperty(value = "市场数")
    private Integer marketNum;

    @ApiModelProperty(value = "客户订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "销售金额")
    private BigDecimal salesAmount;

    @ApiModelProperty(value = "应收客户款")
    private BigDecimal receivableAmount;

    @ApiModelProperty(value = "预收客户款")
    private BigDecimal advanceAmount;

}
