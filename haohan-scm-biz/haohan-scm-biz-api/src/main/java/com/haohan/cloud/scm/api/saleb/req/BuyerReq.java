/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-29 13:28:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购商")
public class BuyerReq extends Buyer {

    private long pageSize;
    private long pageNo;


}
