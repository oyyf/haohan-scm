/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购员工管理商品
 *
 * @author haohan
 * @date 2019-05-29 13:34:42
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购员工管理商品")
public class PurchaseGoodsReq extends PurchaseGoods {

    private long pageSize;
    private long pageNo;




}
