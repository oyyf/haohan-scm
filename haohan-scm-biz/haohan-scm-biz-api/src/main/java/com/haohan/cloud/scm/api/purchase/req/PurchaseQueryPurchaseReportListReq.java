package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseQueryPurchaseReportListReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "pmId不能为空",required = true)
    private String pmId;

    @ApiModelProperty(value = "uId不能为空",required = true)
    private String uId;

    @ApiModelProperty(value = "汇报分类1.货品不足2.价格贵3.质量差4.其他")
    private String reportCategoryType;

    @ApiModelProperty(value = "汇报状态1.已汇报2.已反馈3.已处理")
    private String reportStatus;

    private long size = 10;
    private long current = 1;

}
