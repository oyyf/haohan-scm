package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterTypeEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class AfterTypeEnumConverterUtil implements Converter<AfterTypeEnum> {
    @Override
    public AfterTypeEnum convert(Object o, AfterTypeEnum afterTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AfterTypeEnum.getByType(o.toString());
    }
}
