package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.common.tree.TreeNode;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/4
 * 商品分类树形节点
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsCategoryTree extends TreeNode<GoodsCategoryTree> {

    @ApiModelProperty(value = "分类名称")
    private String name;

    @ApiModelProperty(value = "店铺ID")
    private String shopId;

    @ApiModelProperty(value = "排序")
    private BigDecimal sort;

    @ApiModelProperty(value = "logo地址")
    private String logo;

    @ApiModelProperty(value = "是否展示 0否 1是")
    private YesNoEnum categoryType;

    @ApiModelProperty(value = "是否上架  0否 1是")
    private YesNoEnum status;
}
