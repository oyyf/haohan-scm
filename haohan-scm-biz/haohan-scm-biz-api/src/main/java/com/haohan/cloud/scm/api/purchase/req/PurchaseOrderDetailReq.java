/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:35:25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购部采购单明细")
public class PurchaseOrderDetailReq extends PurchaseOrderDetail {

    private long pageSize=10;
    private long pageNo=1;
//    @NotBlank(message = "uId不能为空")
//    private String uid;
    @NotBlank(message = "pmId不能为空")
    private String pmId;



}
