package com.haohan.cloud.scm.api.constant.enums.salec;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cx
 * @date 2019/7/16
 */

@Getter
@AllArgsConstructor
public enum  UserTypeEnum {

    /**
     * 等级:1级-5级
     */

    salec("0","C端客户"),
    Saleb("1","B端客户");

    private static final Map<String, UserTypeEnum> MAP = new HashMap<>(8);

    static {
        for (UserTypeEnum e : UserTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static UserTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
