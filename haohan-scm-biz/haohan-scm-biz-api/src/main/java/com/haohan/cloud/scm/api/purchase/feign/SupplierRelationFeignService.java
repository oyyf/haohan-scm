/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.SupplierRelation;
import com.haohan.cloud.scm.api.purchase.req.SupplierRelationReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购员工管理供应商内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SupplierRelationFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface SupplierRelationFeignService {


    /**
     * 通过id查询采购员工管理供应商
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SupplierRelation/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购员工管理供应商 列表信息
     * @param supplierRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierRelation/fetchSupplierRelationPage")
    R getSupplierRelationPage(@RequestBody SupplierRelationReq supplierRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购员工管理供应商 列表信息
     * @param supplierRelationReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SupplierRelation/fetchSupplierRelationList")
    R getSupplierRelationList(@RequestBody SupplierRelationReq supplierRelationReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/add")
    R save(@RequestBody SupplierRelation supplierRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/update")
    R updateById(@RequestBody SupplierRelation supplierRelation, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购员工管理供应商
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SupplierRelation/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/countBySupplierRelationReq")
    R countBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/getOneBySupplierRelationReq")
    R getOneBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param supplierRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SupplierRelation/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SupplierRelation> supplierRelationList, @RequestHeader(SecurityConstants.FROM) String from);


}
