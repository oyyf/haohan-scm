package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/15
 */
@Data
@ApiModel(description = "货品下架(货位、货品数量全下)")
public class ProductRemoveShelfReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "productSn不能为空")
    @ApiModelProperty(value = "货品编号",required = true)
    private String productSn;

    @NotBlank(message = "operatorId不能为空")
    @ApiModelProperty(value = "操作人id")
    private String operatorId;
}
