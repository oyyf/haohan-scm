package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/31
 */
@Getter
@AllArgsConstructor
public enum PurchaseOrderTypeEnum implements IBaseEnum {
    /**
     * 采购订单分类:1.采购计划2.按需采购
     */
    purchasePlan("1","采购计划"),
    materialsNeeded("2","按需采购");

    private static final Map<String, PurchaseOrderTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PurchaseOrderTypeEnum e : PurchaseOrderTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PurchaseOrderTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
