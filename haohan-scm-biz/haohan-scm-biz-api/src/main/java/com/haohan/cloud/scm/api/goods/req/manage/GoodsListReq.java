package com.haohan.cloud.scm.api.goods.req.manage;

import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.entity.Goods;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/2
 * default 验证shopId
 * SingleGroup 验证goodsId
 */
@Data
public class GoodsListReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "店铺id不能为空")
    @Length(max = 32, message = "店铺id的长度最大32字符")
    @ApiModelProperty("店铺id")
    private String shopId;

    @NotBlank(message = "商品id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "商品id的长度最大为32字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 10, message = "商品名称的长度最大10字符")
    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("商品状态: 0 出售中 1 仓库中 2 已售罄")
    private GoodsStatusEnum goodsStatus;

    @ApiModelProperty("商品类型 0实体商品，1虚拟商品 2.产品服务 3.竞品")
    private GoodsTypeEnum goodsType;

    @ApiModelProperty("是否c端销售")
    private YesNoEnum salecFlag;

    @ApiModelProperty(value = "上下架状态  0否1是")
    private YesNoEnum marketableFlag;

    @Length(max = 32, message = "商品分类id的长度最大32字符")
    @ApiModelProperty("商品分类id")
    private String categoryId;

    @Length(max = 32, message = "商品编号的长度最大32字符")
    @ApiModelProperty("商品编号")
    private String goodsSn;

    @Length(max = 32, message = "扫码购编码的长度最大32字符")
    @ApiModelProperty("扫码购编码")
    private String scanCode;

    // 扩展

    @Length(max = 32, message = "采购商id的长度最大32字符")
    @ApiModelProperty("采购商id,用于联查平台商品定价")
    private String buyerId;

    @Length(max = 32, message = "平台商品定价商家id的长度最大32字符")
    @ApiModelProperty("平台商品定价商家id")
    private String pricingMerchantId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty("平台商品定价查询日期")
    private LocalDate pricingDate;

    // feign 使用

    @ApiModelProperty(value = "商品id")
    private List<String> goodsIds;

    @ApiModelProperty(value = "商品规格id")
    private List<String> modelIds;

    @ApiModelProperty(value = "商品分类id")
    private List<String> categoryIds;

    public Goods transTo() {
        Goods goods = new Goods();
        goods.setShopId(this.shopId);
        goods.setId(this.goodsId);
        goods.setGoodsName(this.goodsName);
        goods.setGoodsStatus(this.goodsStatus);
        if (null != this.marketableFlag) {
            goods.setIsMarketable(Integer.valueOf(this.marketableFlag.getType()));
        }
        goods.setGoodsType(this.goodsType);
        goods.setSalecFlag(this.salecFlag);
        goods.setGoodsCategoryId(this.categoryId);
        goods.setGoodsSn(this.goodsSn);
        goods.setScanCode(this.scanCode);
        return goods;
    }

    public GoodsPricingReq transToPricingReq() {
        GoodsPricingReq req = new GoodsPricingReq();
        req.setPricingMerchantId(this.pricingMerchantId);
        req.setBuyerId(this.buyerId);
        req.setPricingDate(this.pricingDate);
        return req;
    }
}
