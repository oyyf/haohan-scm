/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.Cell;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 货位信息表
 *
 * @author haohan
 * @date 2019-05-29 09:04:45
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "货位信息表")
public class CellReq extends Cell {

    private long pageSize;
    private long pageNo;




}
