package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
public class SalesAnalysisVO {

    @ApiModelProperty(value = "订单数")
    private Integer orderNum;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "回款金额")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "查询开始时间")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束时间")
    private LocalDate endDate;

}
