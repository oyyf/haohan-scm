package com.haohan.cloud.scm.api.crm.vo.app;

import com.haohan.cloud.scm.api.crm.entity.MarketEmployee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("市场部员工信息")
@NoArgsConstructor
public class MarketEmployeeVO {

    @ApiModelProperty("员工id")
    private String employeeId;

    @ApiModelProperty(value = "通行证ID")
    private String passportId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "市场部员工类型:1市场总监2区域经理3业务经理4.社区合伙人")
    private String employeeType;

    @ApiModelProperty(value = "启用状态 0.未启用 1.启用 2.待审核")
    private String useStatus;

    @ApiModelProperty(value = "性别:1男2女")
    private String sex;

    @ApiModelProperty(value = "职位")
    private String post;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "头像图片地址")
    private String avatar;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    public MarketEmployeeVO(MarketEmployee employee) {
        this.employeeId = employee.getId();
        this.passportId = employee.getPassportId();
        this.name = employee.getName();
        this.telephone = employee.getTelephone();
        if (null != employee.getEmployeeType()) {
            this.employeeType = employee.getEmployeeType().getDesc();
        }
        if (null != employee.getUseStatus()) {
            this.useStatus = employee.getUseStatus().getDesc();
        }
        if (null != employee.getSex()) {
            this.sex = employee.getSex().getDesc();
        }
        this.post = employee.getPost();
        this.deptName = employee.getDeptName();
        this.avatar = employee.getAvatar();
        this.createDate = employee.getCreateDate();
    }
}
