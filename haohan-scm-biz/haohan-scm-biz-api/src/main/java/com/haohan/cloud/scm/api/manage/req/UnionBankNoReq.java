/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 联行号
 *
 * @author haohan
 * @date 2019-05-28 20:39:40
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "联行号")
public class UnionBankNoReq extends UnionBankNo {

    private long pageSize;
    private long pageNo;




}
