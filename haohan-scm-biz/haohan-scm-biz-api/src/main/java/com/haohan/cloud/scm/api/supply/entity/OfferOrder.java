/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.supply.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.ShipStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-13 17:14:26
 */
@Data
@TableName("scm_sms_offer_order")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "报价单")
public class OfferOrder extends Model<OfferOrder> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 报价用户
     */
    private String offerUid;
    /**
     * 报价单号
     */
    private String offerOrderId;
    /**
     * 询价单号
     */
    private String askOrderId;
    /**
     * 报价类型:1平台报价2市场报价3指定报价4货源报价
     */
    private PdsOfferTypeEnum offerType;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 商品分类
     */
    private String goodsCategoryId;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 单位
     */
    private String unit;
    /**
     * 采购数量
     */
    private BigDecimal buyNum;
    /**
     * 数量 (最大供应数量)
     */
    private BigDecimal supplyNum;
    /**
     * 供应商报价
     */
    private BigDecimal supplyPrice;
    /**
     * 实物图片
     */
    private String supplyImg;
    /**
     * 供应说明
     */
    private String supplyDesc;
    /**
     * 报价单状态1待报价2已报价3中标4未中标
     */
    private PdsOfferStatusEnum status;
    /**
     * 发货状态:-1不需发货 0待备货 1备货中 2已发货 4已接收 5售后中
     */
    private ShipStatusEnum shipStatus;
    /**
     * 询价时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime askPriceTime;
    /**
     * 报价时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime offerPriceTime;
    /**
     * 起售数量
     */
    private BigDecimal minSaleNum;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 成交单价
     */
    private BigDecimal dealPrice;
    /**
     * 备货日期 （送货日期）
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate prepareDate;
    /**
     * 采购批次
     */
    private BuySeqEnum buySeq;

    /**
     * 其他费用
     */
    private BigDecimal otherAmount;
    /**
     * 总费用
     */
    private BigDecimal totalAmount;
    /**
     * 发货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deliveryTime;
    /**
     * 收货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receiveTime;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 货源有效期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime validityTime;
    /**
     * 揽货方式1.自提2.送货上门
     */
    private ReceiveTypeEnum receiveType;
    /**
     * 供应单编号
     */
    private String supplySn;
    /**
     * 供应单明细编号
     */
    private String supplyDetailSn;
    /**
     * 客户采购明细编号 (用于判断采购明细是否已有报价单,只有一个明细编号)
     */
    @TableField(value = "buy_detail_sns")
    private String buyDetailSn;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
