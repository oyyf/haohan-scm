package com.haohan.cloud.scm.api.product.dto;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import lombok.Data;

import java.util.List;

/**
 * @author dy
 * @date 2019/6/13
 */
@Data
public class ProcessingResultDTO {

    /**
     * 单个新货品
     */
    private ProductInfo subProduct;

    /**
     * 多个新货品 列表
     */
    private List<ProductInfo> subProductList;

    /**
     * 损耗货品
     */
    private ProductInfo lossProduct;

    /**
     * 原货品
     */
    private ProductInfo sourceProduct;

    /**
     * 加工记录
     */
//    private ProductProcessing processingRecord;

     /**
     * 损耗记录
     */
//    private ProductLossRecord lossRecord;




}
