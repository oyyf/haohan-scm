package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/27
 */
@Getter
@AllArgsConstructor
public enum DataReportEnum implements IBaseEnum {

    /**
     * 上报类型 0.库存 1.销量 2.竞品
     */
    stock("0", "库存"),
    sales("1", "销量"),
    competition("2", "竞品");

    private static final Map<String, DataReportEnum> MAP = new HashMap<>(8);
    private static final Map<String, DataReportEnum> DESC_MAP = new HashMap<>(8);

    static {
        for (DataReportEnum e : DataReportEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    @JsonCreator
    public static DataReportEnum getByType(String type) {
        return MAP.get(type);
    }

    public static DataReportEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
