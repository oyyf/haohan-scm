package com.haohan.cloud.scm.api.saleb.req;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/9/17
 */
@Data
public class BuyOrderImportOneDetailReq {

    @NotNull(message = "excel文件不能为空")
    private MultipartFile file;

    @NotEmpty(message = "采购单id不能为空")
    private String buyId;

}
