package com.haohan.cloud.scm.api.constant.enums.supply;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/4/17
 */
@Getter
@AllArgsConstructor
public enum SupplyRelationTypeEnum implements IBaseEnum {

    /**
     * 关联类型: 0.未关联 1.关联商家 2.关联供应人
     */
    no("0", "未关联"),
    merchant("1", "关联商家"),
    supplier("2", "关联供应人");

    private static final Map<String, SupplyRelationTypeEnum> MAP = new HashMap<>(8);

    static {
        for (SupplyRelationTypeEnum e : SupplyRelationTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SupplyRelationTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
