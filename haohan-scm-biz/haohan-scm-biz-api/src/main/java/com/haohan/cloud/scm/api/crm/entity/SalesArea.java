/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.market.LevelEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 区域管理
 *
 * @author haohan
 * @date 2019-08-30 12:02:00
 */
@Data
@TableName("crm_area")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "区域管理")
public class SalesArea extends Model<SalesArea> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 区域编码
     */
    @ApiModelProperty(value = "区域编码")
    private String areaSn;
    /**
     * 区域名称
     */
    @ApiModelProperty(value = "区域名称")
    private String areaName;
    /**
     * 区域层级 ： 1级-5级
     */
    @ApiModelProperty(value = "等级 0:父级")
    private LevelEnum areaLevel;
    /**
     * 归属部门  (该属性取消, 新增关联关系表 多对多)
     */
    @ApiModelProperty(value = "归属部门")
    private String belongDept;
    /**
     * 父ID
     */
    @ApiModelProperty(value = "父ID")
    private String parentId;
    /**
     * 父区域名称
     */
    @ApiModelProperty(value = "父区域名称")
    private String parentName;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private String sort;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
