package com.haohan.cloud.scm.api.goods.dto;

import lombok.Data;


/**
 * @author cx
 * @date 2019/7/10
 */
@Data
public class GoodsStorageRankDTO {
    private String goodsName;

    private Integer modelStorage;
}
