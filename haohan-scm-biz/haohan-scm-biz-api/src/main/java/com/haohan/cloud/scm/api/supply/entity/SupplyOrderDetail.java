/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
@Data
@TableName("scm_sms_supply_order_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应订单明细")
public class SupplyOrderDetail extends Model<SupplyOrderDetail> {
    private static final long serialVersionUID = 1L;


    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "供应订单编号长度最大64字符")
    @ApiModelProperty(value = "供应订单编号")
    private String supplySn;

    @Length(max = 64, message = "供应订单明细编号长度最大64字符")
    @ApiModelProperty(value = "供应订单明细编号")
    private String supplyDetailSn;

    @Length(max = 64, message = "供应商id长度最大64字符")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @Length(max = 64, message = "商品id长度最大64字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @Length(max = 64, message = "商品规格ID长度最大64字符")
    @ApiModelProperty(value = "商品规格ID")
    private String goodsModelId;

    @Length(max = 64, message = "商品规格编号长度最大64字符")
    @ApiModelProperty(value = "商品规格编号")
    private String goodsModelSn;

    @Length(max = 500, message = "商品图片长度最大500字符")
    @ApiModelProperty(value = "商品图片")
    private String goodsImg;

    @Length(max = 64, message = "商品名称长度最大64字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 64, message = "商品规格名称长度最大64字符")
    @ApiModelProperty(value = "商品规格名称")
    private String modelName;

    @Digits(integer = 10, fraction = 2, message = "采购数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "采购数量")
    private BigDecimal goodsNum;

    @Digits(integer = 10, fraction = 2, message = "市场价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "市场价格")
    private BigDecimal marketPrice;

    @Digits(integer = 10, fraction = 2, message = "成交价格的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "成交价格")
    private BigDecimal dealPrice;

    @Length(max = 64, message = "单位长度最大64字符")
    @ApiModelProperty(value = "单位")
    private String unit;

    @Digits(integer = 10, fraction = 2, message = "金额的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @Length(max = 64, message = "创建者长度最大64字符")
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @Length(max = 64, message = "更新者长度最大64字符")
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(max = 1, message = "删除标记长度最大1字符")
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
