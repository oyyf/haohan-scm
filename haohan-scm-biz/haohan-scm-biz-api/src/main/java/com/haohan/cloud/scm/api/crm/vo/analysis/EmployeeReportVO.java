package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("员工日报")
@JsonIgnoreProperties(value = {"employeeId"})
public class EmployeeReportVO {

    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "员工名称")
    private String employeeName;

    @ApiModelProperty(value = "日报数")
    private Integer reportNum;

}
