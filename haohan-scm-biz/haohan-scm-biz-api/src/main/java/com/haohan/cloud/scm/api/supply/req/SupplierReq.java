/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.Supplier;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-29 13:13:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "供应商")
public class SupplierReq extends Supplier {

    private long pageSize=10;
    private long pageNo=1;
    @NotBlank(message = "uId不能为空")
    private String uid;
    @NotBlank(message = "pmId不能为空")
    private String pmId;



}
