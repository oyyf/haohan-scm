package com.haohan.cloud.scm.api.salec.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/20
 */
@Data
@ApiModel(description = "新增商品需求")
public class CreateStoreProductReq {

    @NotBlank(message = "goodsModelSn不能为空")
    @ApiModelProperty(value = "商品规格唯一编号",required = true)
    private String goodsModelSn;
}
