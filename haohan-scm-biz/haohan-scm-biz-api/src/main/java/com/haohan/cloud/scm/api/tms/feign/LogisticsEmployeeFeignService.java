/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.tms.entity.LogisticsEmployee;
import com.haohan.cloud.scm.api.tms.req.LogisticsEmployeeReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 物流部员工管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "LogisticsEmployeeFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface LogisticsEmployeeFeignService {


    /**
     * 通过id查询物流部员工管理
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/LogisticsEmployee/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 物流部员工管理 列表信息
     *
     * @param logisticsEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/LogisticsEmployee/fetchLogisticsEmployeePage")
    R getLogisticsEmployeePage(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 物流部员工管理 列表信息
     *
     * @param logisticsEmployeeReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/LogisticsEmployee/fetchLogisticsEmployeeList")
    R getLogisticsEmployeeList(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增物流部员工管理
     *
     * @param logisticsEmployee 物流部员工管理
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/add")
    R save(@RequestBody LogisticsEmployee logisticsEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改物流部员工管理
     *
     * @param logisticsEmployee 物流部员工管理
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/update")
    R updateById(@RequestBody LogisticsEmployee logisticsEmployee, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除物流部员工管理
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param logisticsEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/countByLogisticsEmployeeReq")
    R countByLogisticsEmployeeReq(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param logisticsEmployeeReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/getOneByLogisticsEmployeeReq")
    R getOneByLogisticsEmployeeReq(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param logisticsEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/LogisticsEmployee/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<LogisticsEmployee> logisticsEmployeeList, @RequestHeader(SecurityConstants.FROM) String from);


}
