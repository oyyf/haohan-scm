/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.PictureRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户现场图片
 *
 * @author haohan
 * @date 2019-09-29 15:18:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户现场图片")
public class PictureRecordReq extends PictureRecord {

    private long pageSize;
    private long pageNo;


}
