package com.haohan.cloud.scm.api.bill.dto;

import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/26
 * 订单详情  通用订单属性(部分属性在转换时缺少)
 */
@Data
@ApiModel("订单详情")
public class OrderInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "订单类型", notes = "订单类型： 1.采购订单、2.供应订单、3.退货订单、4.销售订单")
    private OrderTypeEnum orderType;

    @ApiModelProperty(value = "订单状态", notes = "订单状态： 1.已下单、2.待确认、3.成交、4.取消")
    private OrderStatusEnum orderStatus;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    @ApiModelProperty(value = "平台商家ID")
    private String pmId;

    @ApiModelProperty(value = "平台商家名称")
    private String pmName;

    @ApiModelProperty(value = "下单客户id", notes = "采购商、供应商")
    private String customerId;

    @ApiModelProperty(value = "下单客户名称", notes = "采购商、供应商")
    private String customerName;

    /**
     * 对客户采购单、销售订单 来说：客户商家 即 下单客户的商家
     * 对供应订单 来说：客户商家 即 供应商商家
     * 商家是用于账单创建及结算的公司
     */
    @ApiModelProperty(value = "客户商家id")
    private String merchantId;

    @ApiModelProperty(value = "客户商家名称")
    private String merchantName;

    @ApiModelProperty(value = "客户联系人名称")
    private String contact;

    @ApiModelProperty(value = "客户联系电话")
    private String telephone;

    @ApiModelProperty(value = "客户地址")
    private String address;

    @ApiModelProperty(value = "订单成交日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dealDate;

    @ApiModelProperty(value = "下单时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderTime;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "订单商品合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "订单其他金额")
    private BigDecimal otherAmount;

    @ApiModelProperty(value = "订单商品种类数")
    private Integer goodsNum;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "订单商品明细列表")
    private List<OrderDetailDTO> detailList;


}
