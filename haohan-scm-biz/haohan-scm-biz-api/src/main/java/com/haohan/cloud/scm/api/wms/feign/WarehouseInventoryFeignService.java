/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.req.WarehouseInventoryReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 库存盘点内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "WarehouseInventoryFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WarehouseInventoryFeignService {


    /**
     * 通过id查询库存盘点
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/WarehouseInventory/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 库存盘点 列表信息
     * @param warehouseInventoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseInventory/fetchWarehouseInventoryPage")
    R getWarehouseInventoryPage(@RequestBody WarehouseInventoryReq warehouseInventoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 库存盘点 列表信息
     * @param warehouseInventoryReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/WarehouseInventory/fetchWarehouseInventoryList")
    R getWarehouseInventoryList(@RequestBody WarehouseInventoryReq warehouseInventoryReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增库存盘点
     * @param warehouseInventory 库存盘点
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/add")
    R save(@RequestBody WarehouseInventory warehouseInventory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改库存盘点
     * @param warehouseInventory 库存盘点
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/update")
    R updateById(@RequestBody WarehouseInventory warehouseInventory, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除库存盘点
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/WarehouseInventory/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param warehouseInventoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/countByWarehouseInventoryReq")
    R countByWarehouseInventoryReq(@RequestBody WarehouseInventoryReq warehouseInventoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param warehouseInventoryReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/getOneByWarehouseInventoryReq")
    R getOneByWarehouseInventoryReq(@RequestBody WarehouseInventoryReq warehouseInventoryReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param warehouseInventoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/WarehouseInventory/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<WarehouseInventory> warehouseInventoryList, @RequestHeader(SecurityConstants.FROM) String from);


}
