/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.AppOnlineManage;
import com.haohan.cloud.scm.api.manage.req.AppOnlineManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 应用上线管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "AppOnlineManageFeignService", value = ScmServiceName.SCM_MANAGE)
public interface AppOnlineManageFeignService {


    /**
     * 通过id查询应用上线管理
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/AppOnlineManage/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 应用上线管理 列表信息
     * @param appOnlineManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AppOnlineManage/fetchAppOnlineManagePage")
    R getAppOnlineManagePage(@RequestBody AppOnlineManageReq appOnlineManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 应用上线管理 列表信息
     * @param appOnlineManageReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/AppOnlineManage/fetchAppOnlineManageList")
    R getAppOnlineManageList(@RequestBody AppOnlineManageReq appOnlineManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增应用上线管理
     * @param appOnlineManage 应用上线管理
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/add")
    R save(@RequestBody AppOnlineManage appOnlineManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改应用上线管理
     * @param appOnlineManage 应用上线管理
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/update")
    R updateById(@RequestBody AppOnlineManage appOnlineManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除应用上线管理
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/AppOnlineManage/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param appOnlineManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/countByAppOnlineManageReq")
    R countByAppOnlineManageReq(@RequestBody AppOnlineManageReq appOnlineManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param appOnlineManageReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/getOneByAppOnlineManageReq")
    R getOneByAppOnlineManageReq(@RequestBody AppOnlineManageReq appOnlineManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param appOnlineManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/AppOnlineManage/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<AppOnlineManage> appOnlineManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
