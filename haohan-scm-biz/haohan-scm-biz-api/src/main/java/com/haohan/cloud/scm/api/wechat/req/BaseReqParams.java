package com.haohan.cloud.scm.api.wechat.req;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: haohan-fresh-scm
 * @description: 基础请求参数
 * @author: Simon
 * @create: 2019-07-12
 **/
@Data
public class BaseReqParams implements Serializable {

    /**
     * 权限值CODE
     */
    private String authCode;

    /**
     * 后端自行兼容各种前端业务定义
     **/
    private String type;

    private String startDate;

    private String endDate;


}
