package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.AreaTypeEnum;

/**
 * @author dy
 * @date 2019/9/25
 */
public class AreaTypeEnumConverterUtil implements Converter<AreaTypeEnum> {
    @Override
    public AreaTypeEnum convert(Object o, AreaTypeEnum areaTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AreaTypeEnum.getByType(o.toString());
    }
}
