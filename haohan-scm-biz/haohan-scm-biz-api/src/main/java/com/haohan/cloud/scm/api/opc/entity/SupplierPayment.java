/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.opc.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 应付账单   已废弃，使用bill模块
 *
 * @author haohan
 * @date 2019-05-13 17:45:06
 */
@Data
@TableName("scm_sms_supplier_payment")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "应付账单")
public class SupplierPayment extends Model<SupplierPayment> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家ID
     */
    private String pmId;
    /**
     * 询价单号
     */
    private String askOrderId;
    /**
     * 应付来源订单编号
     */
    private String payableSn;
    /**
     * 供应商  => 账单归属人id(buyerId/supplierId)
     */
    private String supplierId;
    /**
     * 客户名称 => 采购商/供应商 名称
     */
    private String customerName;
    /**
     * 客户商家id
     */
    private String merchantId;
    /**
     * 客户商家名称
     */
    private String merchantName;
    /**
     * 订单成交日期 => 供应订单备货日期 /  退货单申请日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate supplyDate;
    /**
     * 商品数量
     */
    private String goodsNum;
    /**
     * 应付货款
     */
    private BigDecimal supplierPayment;
    /**
     * 售后货款
     */
    private BigDecimal afterSalePayment;
    /**
     * 结算状态:已结/未结
     */
    private YesNoEnum status;
    /**
     * 账单类型: 3采购应付 4退款应付
     */
    private BillTypeEnum billType;
    /**
     * 结算单编号
     */
    @TableField(value = "service_id")
    private String settlementSn;
    /**
     * 应付货款记录编号
     */
    private String supplierPaymentId;
    /**
     * 账单审核状态: 1.待审核2.审核不通过3.审核通过
     */
    private ReviewStatusEnum reviewStatus;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
