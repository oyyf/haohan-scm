/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.entity.StoreProduct;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品表
 *
 * @author haohan
 * @date 2019-06-19 17:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品表")
public class StoreProductReq extends StoreProduct {

  private long pageSize;
  private long pageNo;


}
