/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsPriceRule;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-28 19:57:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "定价规则/市场价/销售价/VIP价格")
public class GoodsPriceRuleReq extends GoodsPriceRule {

    private long pageSize;
    private long pageNo;




}
