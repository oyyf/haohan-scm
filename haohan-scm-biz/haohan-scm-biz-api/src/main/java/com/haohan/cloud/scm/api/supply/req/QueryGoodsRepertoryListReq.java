package com.haohan.cloud.scm.api.supply.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "供应商品库存信息列表")
public class QueryGoodsRepertoryListReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台id",required = true)
    private String pmId;

    @NotBlank(message = "supplierId不能为空")
    @ApiModelProperty(value = "供应商id",required = true)
    private String supplierId;

    @ApiModelProperty(value = "每页显示条数" )
    private long pageSize=10;

    @ApiModelProperty(value = "页码" )
    private long pageNo=1;

}
