package com.haohan.cloud.scm.api.constant.enums.purchase;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum PayTypeEnum implements IBaseEnum {
    /**
     * 付款方式:1对公转账2现金支付3在线支付4承兑汇票
     */
    transfer("1","对公转账"),
    cash("2","现金支付"),
    online("3","在线支付"),
    acceptance("4","承兑汇票"),
    ;

    private static final Map<String, PayTypeEnum> MAP = new HashMap<>(8);

    static {
        for (PayTypeEnum e : PayTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PayTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;
}
