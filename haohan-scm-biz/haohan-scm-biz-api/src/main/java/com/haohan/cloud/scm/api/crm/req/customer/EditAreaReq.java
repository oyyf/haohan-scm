package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.crm.entity.SalesArea;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/9/25
 */
@Data
public class EditAreaReq {

    @ApiModelProperty(value = "主键")
    @Length(min = 0, max = 32, message = "主键长度在0至32之间")
    private String id;

    @ApiModelProperty(value = "区域名称", required = true)
    @NotBlank(message = "区域名称不能为空")
    @Length(min = 0, max = 32, message = "区域名称长度在0至32之间")
    private String areaName;

    @ApiModelProperty(value = "归属部门ID")
    @Length(min = 0, max = 32, message = "归属部门ID长度在0至32之间")
    private String belongDept;

    @Length(min = 0, max = 32, message = "父级ID长度在0至32之间")
    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @Length(min = 0, max = 5, message = "排序值长度在0至5之间")
    @ApiModelProperty(value = "排序值")
    private String sort;

    @ApiModelProperty(value = "备注信息")
    @Length(min = 0, max = 255, message = "备注信息长度在0至255之间")
    private String remarks;

    public SalesArea transTo() {
        SalesArea area = new SalesArea();
        area.setId(this.id);
        area.setAreaName(this.areaName);
        area.setBelongDept(this.belongDept);
        area.setParentId(this.parentId);
        area.setSort(this.sort);
        return area;
    }

}
