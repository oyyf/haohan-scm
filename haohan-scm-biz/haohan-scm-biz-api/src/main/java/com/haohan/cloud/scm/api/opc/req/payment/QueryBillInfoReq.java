package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.opc.dto.BillPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/17
 */
@Data
@ApiModel(description = "查询账单详情")
public class QueryBillInfoReq {
    @NotBlank(message = "平台商家id不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "账单编号不能为空")
    @ApiModelProperty(value = "账单编号", required = true)
    private String billSn;

    @ApiModelProperty(value = "结算类型：1.应收 2.应付", required = true)
    private SettlementTypeEnum settlementType;

    public BillPayment transTo() {
        BillPayment bill = new BillPayment();
        bill.setPmId(this.pmId);
        bill.setBillSn(this.billSn);
        bill.setSettlementType(this.settlementType);
        return bill;
    }

}
