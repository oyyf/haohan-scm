/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.pay.feign;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.pay.entity.RefundManage;
import com.haohan.cloud.scm.api.pay.req.RefundManageReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 退款管理内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "RefundManageFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface RefundManageFeignService {


    /**
     * 通过id查询退款管理
     * @param id id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/RefundManage/{id}", method = RequestMethod.GET)
    R<RefundManage> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 退款管理 列表信息
     * @param refundManageReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/RefundManage/fetchRefundManagePage")
    R<Page<RefundManage>> getRefundManagePage(@RequestBody RefundManageReq refundManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 退款管理 列表信息
     * @param refundManageReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/RefundManage/fetchRefundManageList")
    R<List<RefundManage>> getRefundManageList(@RequestBody RefundManageReq refundManageReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增退款管理
     * @param refundManage 退款管理
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/add")
    R<Boolean> save(@RequestBody RefundManage refundManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改退款管理
     * @param refundManage 退款管理
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/update")
    R<Boolean> updateById(@RequestBody RefundManage refundManage, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除退款管理
     * @param id id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @param from
   * @return R
   */
    @PostMapping("/api/feign/RefundManage/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/listByIds")
    R<List<RefundManage>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param refundManageReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/countByRefundManageReq")
    R<Integer> countByRefundManageReq(@RequestBody RefundManageReq refundManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param refundManageReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/getOneByRefundManageReq")
    R<RefundManage> getOneByRefundManageReq(@RequestBody RefundManageReq refundManageReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param refundManageList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/RefundManage/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<RefundManage> refundManageList, @RequestHeader(SecurityConstants.FROM) String from);


}
