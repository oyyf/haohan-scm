package com.haohan.cloud.scm.api.bill.req;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.bill.dto.SettlementDTO;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/12/28
 * 结算单准备结算
 */
@Data
public class SettlementFinishReq {

    @NotBlank(message = "结算单编号不能为空")
    @Length(max = 32, message = "结算单编号的最大长度为32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @NotBlank(message = "结款人名称不能为空")
    @Length(max = 32, message = "结款人名称的最大长度为32字符")
    @ApiModelProperty(value = "结款人名称")
    private String companyOperator;

    @NotBlank(message = "经办人名称不能为空")
    @Length(max = 32, message = "经办人名称的最大长度为32字符")
    @ApiModelProperty(value = "经办人名称")
    private String operatorName;

    @NotBlank(message = "随机码不能为空")
    @Length(max = 32, message = "随机码的最大长度为32字符")
    @ApiModelProperty(value = "开始结算状态随机码（验证是否此次结算）")
    private String randomCode;

    // 可选项

    @ApiModelProperty(value = "付款方式:1对公转账2现金支付3在线支付4承兑汇票")
    private PayTypeEnum payType;

    @ApiModelProperty(value = "结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;

    @Length(max = 255, message = "结算说明的最大长度为255字符")
    @ApiModelProperty(value = "结算说明")
    private String settlementDesc;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    public SettlementDTO transTo() {
        SettlementDTO settlementDTO = new SettlementDTO();
        BeanUtil.copyProperties(this, settlementDTO);
        return settlementDTO;
    }
}
