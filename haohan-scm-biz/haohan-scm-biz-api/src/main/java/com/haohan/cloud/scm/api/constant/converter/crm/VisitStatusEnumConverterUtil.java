package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStatusEnum;

/**
 * @author dy
 * @date 2020/1/3
 */
public class VisitStatusEnumConverterUtil implements Converter<VisitStatusEnum> {
    @Override
    public VisitStatusEnum convert(Object o, VisitStatusEnum visitStepEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return VisitStatusEnum.getByType(o.toString());
    }

}
