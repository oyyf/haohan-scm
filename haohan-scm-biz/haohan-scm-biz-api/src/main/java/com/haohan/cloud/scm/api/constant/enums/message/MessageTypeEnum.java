package com.haohan.cloud.scm.api.constant.enums.message;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum MessageTypeEnum implements IBaseEnum {
    /**
     * 消息类型:1微信2站内信3短信
     */
    weChat("1", "微信"),
    inMail("2", "站内信"),
    note("3", "短信");

    private static final Map<String, MessageTypeEnum> MAP = new HashMap<>(8);

    static {
        for (MessageTypeEnum e : MessageTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MessageTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
