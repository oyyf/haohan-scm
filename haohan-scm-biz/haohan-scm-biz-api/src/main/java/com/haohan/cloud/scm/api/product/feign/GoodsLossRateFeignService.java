/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import com.haohan.cloud.scm.api.product.req.GoodsLossRateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 单品损耗率内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "GoodsLossRateFeignService", value = ScmServiceName.SCM_BIZ_PRODUCT)
public interface GoodsLossRateFeignService {


    /**
     * 通过id查询单品损耗率
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/GoodsLossRate/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 单品损耗率 列表信息
     * @param goodsLossRateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsLossRate/fetchGoodsLossRatePage")
    R getGoodsLossRatePage(@RequestBody GoodsLossRateReq goodsLossRateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 单品损耗率 列表信息
     * @param goodsLossRateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/GoodsLossRate/fetchGoodsLossRateList")
    R getGoodsLossRateList(@RequestBody GoodsLossRateReq goodsLossRateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/add")
    R save(@RequestBody GoodsLossRate goodsLossRate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/update")
    R updateById(@RequestBody GoodsLossRate goodsLossRate, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除单品损耗率
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/GoodsLossRate/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/countByGoodsLossRateReq")
    R countByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/getOneByGoodsLossRateReq")
    R getOneByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param goodsLossRateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/GoodsLossRate/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<GoodsLossRate> goodsLossRateList, @RequestHeader(SecurityConstants.FROM) String from);


}
