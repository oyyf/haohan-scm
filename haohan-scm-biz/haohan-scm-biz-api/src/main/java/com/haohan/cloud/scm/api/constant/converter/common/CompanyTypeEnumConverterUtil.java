package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.CompanyTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class CompanyTypeEnumConverterUtil implements Converter<CompanyTypeEnum> {
    @Override
    public CompanyTypeEnum convert(Object o, CompanyTypeEnum companyTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return CompanyTypeEnum.getByType(o.toString());
    }
}
