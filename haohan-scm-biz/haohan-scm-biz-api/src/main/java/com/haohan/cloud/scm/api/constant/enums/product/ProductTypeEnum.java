package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum ProductTypeEnum {

    purchase("1", "采购货品"),
    supply("2", "供应货品"),
    handle("3", "加工货品"),
    Return("4","退货货品");

    private static final Map<String, ProductTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ProductTypeEnum e : ProductTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ProductTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
