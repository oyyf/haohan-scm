package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.VisitStepEnum;

/**
 * @author dy
 * @date 2019/9/24
 */
public class VisitStepEnumConverterUtil implements Converter<VisitStepEnum> {
    @Override
    public VisitStepEnum convert(Object o, VisitStepEnum visitStepEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return VisitStepEnum.getByType(o.toString());
    }

}
