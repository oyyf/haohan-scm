/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 预警记录表
 *
 * @author haohan
 * @date 2019-05-13 18:23:48
 */
@Data
@TableName("scm_ews_warning_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预警记录表")
public class WarningRecord extends Model<WarningRecord> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private String id;
    /**
   * 平台商家id
   */
    private String pmId;
    /**
   * 预警记录编号
   */
    private String warningSn;
    /**
   * 使用模板编号
   */
    private String templateSn;
    /**
   * 预警类型:各部门预警
   */
    private String warningType;
    /**
   * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
   */
    private String departmentType;
    /**
   * 预警内容
   */
    private String content;
    /**
   * 触发时间
   */
    private LocalDateTime triggerTime;
    /**
   * 创建者
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    private String updateBy;
    /**
   * 更新时间
   */
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;
  
}
