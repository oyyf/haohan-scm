package com.haohan.cloud.scm.api.crm.req.bill;

import com.haohan.cloud.scm.api.bill.req.SettlementFeignReq;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/12/28
 * FirstGroup  完成普通结算
 */
@Data
@Api("结算编辑")
public class CrmSettlementEditReq {

    @NotBlank(message = "结算单编号不能为空")
    @Length(max = 32, message = "结算单编号的最大长度为32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @NotBlank(message = "结款人名称不能为空")
    @Length(max = 32, message = "结款人名称的最大长度为32字符")
    @ApiModelProperty(value = "结款人名称")
    private String companyOperator;

    @NotBlank(message = "结算员工id不能为空")
    @Length(max = 32, message = "结算员工id的最大长度为32字符")
    @ApiModelProperty(value = "结算员工id(经办人)")
    private String employeeId;

    @NotBlank(message = "随机码不能为空")
    @Length(max = 32, message = "随机码的最大长度为32字符")
    @ApiModelProperty(value = "开始结算状态随机码（验证是否此次结算）")
    private String randomCode;

    // 可选项

    @ApiModelProperty(value = "付款方式:1对公转账2现金支付3在线支付4承兑汇票")
    private PayTypeEnum payType;

    @ApiModelProperty(value = "结算时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime settlementTime;

    @Length(max = 255, message = "结算说明的最大长度为255字符")
    @ApiModelProperty(value = "结算说明")
    private String settlementDesc;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;

    public SettlementFeignReq transToFeign() {
        SettlementFeignReq req  = new SettlementFeignReq();
        req.setSettlementSn(this.settlementSn);
        req.setCompanyOperator(this.companyOperator);
        req.setRandomCode(this.randomCode);
        req.setPayType(this.payType);
        req.setSettlementTime(this.settlementTime);
        req.setSettlementDesc(this.settlementDesc);
        req.setPhotoList(this.photoList);
        return req;
    }
}
