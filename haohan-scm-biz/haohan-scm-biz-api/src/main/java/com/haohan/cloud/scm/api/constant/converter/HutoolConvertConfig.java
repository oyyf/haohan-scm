package com.haohan.cloud.scm.api.constant.converter;

import cn.hutool.core.convert.ConverterRegistry;
import com.haohan.cloud.scm.api.constant.converter.aftersales.AfterSalesStatusEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.aftersales.*;
import com.haohan.cloud.scm.api.constant.converter.bill.OrderStatusEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.bill.OrderTypeEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.bill.SettlementStyleEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.common.*;
import com.haohan.cloud.scm.api.constant.converter.crm.*;
import com.haohan.cloud.scm.api.constant.converter.goods.GoodsFromTypeEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.goods.GoodsStatusEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.goods.GoodsTypeEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.goods.ModelAttrNameEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.manage.*;
import com.haohan.cloud.scm.api.constant.converter.market.DeliverySeqEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.market.*;
import com.haohan.cloud.scm.api.constant.converter.message.*;
import com.haohan.cloud.scm.api.constant.converter.opc.SupplierStatusEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.converter.opc.*;
import com.haohan.cloud.scm.api.constant.converter.product.*;
import com.haohan.cloud.scm.api.constant.converter.purchase.*;
import com.haohan.cloud.scm.api.constant.converter.saleb.*;
import com.haohan.cloud.scm.api.constant.converter.supply.*;
import com.haohan.cloud.scm.api.constant.converter.wechat.WechatUseTypeEnumConverterUtil;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterSalesStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.aftersales.*;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.SettlementStyleEnum;
import com.haohan.cloud.scm.api.constant.enums.common.*;
import com.haohan.cloud.scm.api.constant.enums.crm.*;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.ModelAttrNameEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.*;
import com.haohan.cloud.scm.api.constant.enums.market.*;
import com.haohan.cloud.scm.api.constant.enums.message.*;
import com.haohan.cloud.scm.api.constant.enums.opc.*;
import com.haohan.cloud.scm.api.constant.enums.product.DeliverySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.*;
import com.haohan.cloud.scm.api.constant.enums.purchase.*;
import com.haohan.cloud.scm.api.constant.enums.saleb.*;
import com.haohan.cloud.scm.api.constant.enums.supply.*;
import com.haohan.cloud.scm.api.constant.enums.wechat.WechatUseTypeEnum;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/6/3
 */
@Configuration
public class HutoolConvertConfig {

    public HutoolConvertConfig() {
        ConverterRegistry converterRegistry = ConverterRegistry.getInstance();

        converterRegistry.putCustom(LocalDateTime.class, LocalDateTimeConverterUtil.class);
        // opc
        converterRegistry.putCustom(YesNoEnum.class, YesNoEnumConverterUtil.class);
        converterRegistry.putCustom(PdsSummaryStatusEnum.class, PdsSummaryStatusEnumConverterUtil.class);
        converterRegistry.putCustom(BuySeqEnum.class, BuySeqEnumConverterUtil.class);
        converterRegistry.putCustom(SummaryStatusEnum.class, SummaryStatusEnumConverterUtil.class);
        converterRegistry.putCustom(AllocateTypeEnum.class, AllocateTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PurchaseOptionEnum.class, PurchaseOptionEnumConverterUtil.class);
        converterRegistry.putCustom(BuyerStatusEnum.class, BuyerStatusEnumConverterUtil.class);
        converterRegistry.putCustom(DeliveryStatusEnum.class, DeliveryStatusEnumConverterUtil.class);
        converterRegistry.putCustom(OpStatusEnum.class, OpStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SupplierStatusEnum.class, SupplierStatusEnumConverterUtil.class);
        converterRegistry.putCustom(TransStatusEnum.class, TransStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ShipRecordStatusEnum.class, ShipRecordStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ShipTypeEnum.class, ShipTypeEnumConverterUtil.class);

        // supply
        converterRegistry.putCustom(GradeTypeEnum.class, GradeTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PdsOfferStatusEnum.class, PdsOfferStatusEnumConverterUtil.class);
        converterRegistry.putCustom(PdsOfferTypeEnum.class, PdsOfferTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PdsSupplierTypeEnum.class, PdsSupplierTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ShipStatusEnum.class, ShipStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SupplierStatusEnum.class, SupplierStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SupplyRelationTypeEnum.class, SupplyRelationTypeEnumConverterUtil.class);
        converterRegistry.putCustom(SupplyTypeEnum.class, SupplyTypeEnumConverterUtil.class);


        // saleb
        converterRegistry.putCustom(BuyOrderStatusEnum.class, BuyOrderStatusEnumConverterUtil.class);
        converterRegistry.putCustom(PayPeriodEnum.class, PayPeriodEnumConverterUtil.class);
        converterRegistry.putCustom(BuyerTypeEnum.class, BuyerTypeEnumConverterUtil.class);
        converterRegistry.putCustom(BillTypeEnum.class, BillTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DetailSummaryFlagEnum.class, DetailSummaryFlagEnumConverterUtil.class);

        // purchase
        converterRegistry.putCustom(PurchaseStatusEnum.class, PurchaseStatusEnumConverterUtil.class);
        converterRegistry.putCustom(PurchaseOrderTypeEnum.class, PurchaseOrderTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PayTypeEnum.class, PayTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReceiveTypeEnum.class, ReceiveTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ActionTypeEnum.class, ActionTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MethodTypeEnum.class, MethodTypeEnumConverterUtil.class);
        converterRegistry.putCustom(OfferTypeEnum.class, OfferTypeEnumConverterUtil.class);
        converterRegistry.putCustom(BiddingStatusEnum.class, BiddingStatusEnumConverterUtil.class);
        converterRegistry.putCustom(LendingTypeEnum.class, LendingTypeEnumConverterUtil.class);
        converterRegistry.putCustom(LenderTypeEnum.class, LenderTypeEnumConverterUtil.class);
        converterRegistry.putCustom(EmployeeTypeEnum.class, EmployeeTypeEnumConverterUtil.class);
        converterRegistry.putCustom(TaskActionTypeEnum.class, TaskActionTypeEnumConverterUtil.class);
        converterRegistry.putCustom(TaskStatusEnum.class, TaskStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ReportTypeEnum.class, ReportTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReviewTypeEnum.class, ReviewTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReportStatusEnum.class, ReportStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ReportCategoryTypeEnum.class, ReportCategoryTypeEnumConverterUtil.class);
        converterRegistry.putCustom(LendingStatusEnum.class, LendingStatusEnumConverterUtil.class);
        //product
        converterRegistry.putCustom(ProductStatusEnum.class, ProductStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ProductQialityEnum.class, ProductQialityEnumConverterUtil.class);
        converterRegistry.putCustom(ProductTypeEnum.class, ProductTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ProcessingTypeEnum.class, ProcessingTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ProductPlaceStatusEnum.class, ProductPlaceStatusEnumConverterUtil.class);
        converterRegistry.putCustom(LossTypeEnum.class, LossTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ProductionEmployeeTypeEnum.class, ProductionEmployeeTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ShipOrderStatusEnum.class, ShipOrderStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SortingStatusEnum.class, SortingStatusEnumConverterUtil.class);
        converterRegistry.putCustom(DeliverySeqEnum.class, DeliverySeqEnumConverterUtil.class);
        converterRegistry.putCustom(AllotStatusEnum.class, AllotStatusEnumConverterUtil.class);
        converterRegistry.putCustom(EnterStatusEnum.class, EnterStatusEnumConverterUtil.class);
        converterRegistry.putCustom(EnterTypeEnum.class, EnterTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ExitStatusEnum.class, ExitStatusEnumConverterUtil.class);
        converterRegistry.putCustom(ExitTypeEnum.class, ExitTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PalletPutTypeEnum.class, PalletPutTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PalletTypeEnum.class, PalleTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ProductionTypeEnum.class, ProductionTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ShelfOperateEnum.class, ShelfOperateEnumConverterUtil.class);
        converterRegistry.putCustom(StorageStatusEnum.class, StoragePlaceTypeEnumConverterUtil.class);
        converterRegistry.putCustom(StorageTypeEnum.class, StorageTypeEnumConverterUtil.class);
        converterRegistry.putCustom(WarehouseTypeEnum.class, WarehouseTypeEnumConverterUtil.class);

        //afterSales
        converterRegistry.putCustom(AfterOrderTypeEnum.class, AfterOrderTypeEnumConverterUtil.class);
        converterRegistry.putCustom(AfterSalesStatusEnum.class, AfterSalesStatusEnumConverterUtil.class);
        converterRegistry.putCustom(AfterTypeEnum.class, AfterTypeEnumConverterUtil.class);
        converterRegistry.putCustom(EvaluateTypeEnum.class, EvaluateTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReturnTypeEnum.class, ReturnTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReturnStatusEnum.class, ReturnStatusEnumConverterUtil.class);

        //common
        converterRegistry.putCustom(BizfromTypeEnum.class, BizfromTypeEnumConverterUtil.class);
        converterRegistry.putCustom(UseStatusEnum.class, UseStatusEnumConverterUtil.class);
        converterRegistry.putCustom(DeliveryTypeEnum.class, DeliveryTypeEnumConverterUtil.class);
        converterRegistry.putCustom(BusinessTypeEnum.class, BusinessTypeEnumConverterUtil.class);
        converterRegistry.putCustom(CompanyTypeEnum.class, CompanyTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DealTypeEnum.class, DealTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PdsPayTypeEnum.class, PdsPayTypeEnumConverterUtil.class);
        converterRegistry.putCustom(SettlementTypeEnum.class, SettlementTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ReviewStatusEnum.class, ReviewStatusEnumConverterUtil.class);

        //market
        converterRegistry.putCustom(AnniversaryTypeEnum.class, AnniversaryTypeEnumConverterUtil.class);
        converterRegistry.putCustom(AreaTypeEnum.class, AreaTypeEnumConverterUtil.class);
        converterRegistry.putCustom(CalendarTypeEnum.class, CalendarTypeEnumConverterUtil.class);
        converterRegistry.putCustom(CompanyNatureEnum.class, CompanyNatureEnumConverterUtil.class);
        converterRegistry.putCustom(ContractStatusEnum.class, ContractStatusEnumConverterUtil.class);
        converterRegistry.putCustom(CustomerOrderTypeEnum.class, CustomerOrderTypeEnumConverterUtil.class);
        converterRegistry.putCustom(CustomerStatusEnum.class, CustomerStatusEnumConverterUtil.class);
        converterRegistry.putCustom(CustomerTypeEnum.class, CustomerTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DeliverySeqEnum.class, DeliverySeqEnumConverterUtil.class);
        converterRegistry.putCustom(EvaluateLevelEnum.class, EvaluateLevelEnumConverterUtil.class);
        converterRegistry.putCustom(EvaluateStatusEnum.class, EvaluateStatusEnumConverterUtil.class);
        converterRegistry.putCustom(LevelEnum.class, LevelEnumConverterUtil.class);
        converterRegistry.putCustom(MarketEmployeeTypeEnum.class, MarketEmployeeTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MarketTaskTypeEnum.class, MarketTaskTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MarketTypeEnum.class, MarketTypeEnumConverterUtil.class);
        converterRegistry.putCustom(RelationStatusEnum.class, RelationStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SalesProbabilityEnum.class, SalesProbabilityEnumConverterUtil.class);
        converterRegistry.putCustom(SalesStageEnum.class, SalesStageEnumConverterUtil.class);
        converterRegistry.putCustom(SalesStatusEnum.class, SalesStatusEnumConverterUtil.class);
        converterRegistry.putCustom(SexEnum.class, SexEnumConverterUtil.class);

        //message
        converterRegistry.putCustom(AfterSalesStatusEnum.class, AfterSalesStatusEnumConverterUtil.class);
        converterRegistry.putCustom(DepartmentTypeEnum.class, DepartmentTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MessageTypeEnum.class, MessageTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ShortTemplateTypeEnum.class, ShortTemplateTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MsgActionTypeEnum.class, MsgActionTypeEnumConverterUtil.class);
        converterRegistry.putCustom(InMailTypeEnum.class, InMailTypeEnumConverterUtil.class);

        // crm
        converterRegistry.putCustom(SalesTypeEnum.class, SalesTypeEnumConverterUtil.class);
        converterRegistry.putCustom(SalesGoodsTypeEnum.class, SalesGoodsTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DynamicTypeEnum.class, DynamicTypeEnumConverterUtil.class);
        converterRegistry.putCustom(VisitStepEnum.class, VisitStepEnumConverterUtil.class);
        converterRegistry.putCustom(VisitStatusEnum.class, VisitStatusEnumConverterUtil.class);
        converterRegistry.putCustom(DataReportEnum.class, DataReportEnumConverterUtil.class);
        converterRegistry.putCustom(DataReportStatusEnum.class, DataReportStatusEnumConverterUtil.class);
        converterRegistry.putCustom(PayStatusEnum.class, PayStatusEnumConverterUtil.class);
        converterRegistry.putCustom(WorkReportTypeEnum.class, WorkReportTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DeliveryGoodsTypeEnum.class, DeliveryGoodsTypeEnumConverterUtil.class);
        converterRegistry.putCustom(DeliveryGoodsStatusEnum.class, DeliveryGoodsStatusEnumConverterUtil.class);
        converterRegistry.putCustom(MemberTypeEnum.class, MemberTypeEnumConverterUtil.class);
        converterRegistry.putCustom(GoodsAnalysisTypeEnum.class, GoodsAnalysisTypeEnumConverterUtil.class);

        // manage
        converterRegistry.putCustom(MerchantPdsTypeEnum.class, MerchantPdsTypeEnumConverterUtil.class);
        converterRegistry.putCustom(MerchantStatusEnum.class, MerchantStatusEnumConverterUtil.class);
        converterRegistry.putCustom(OssTypeEnum.class, OssTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PdsRegFromEnum.class, PdsRegFromEnumConverterUtil.class);
        converterRegistry.putCustom(PdsRegTypeEnum.class, PdsRegTypeEnumConverterUtil.class);
        converterRegistry.putCustom(PdsUpassportStatusEnum.class, PdsUpassportStatusEnumConverterUtil.class);
        converterRegistry.putCustom(PhotoTypeEnum.class, PhotoTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ShopLevelEnum.class, ShopLevelEnumConverterUtil.class);

        // goods
        converterRegistry.putCustom(GoodsStatusEnum.class, GoodsStatusEnumConverterUtil.class);
        converterRegistry.putCustom(GoodsFromTypeEnum.class, GoodsFromTypeEnumConverterUtil.class);
        converterRegistry.putCustom(GoodsTypeEnum.class, GoodsTypeEnumConverterUtil.class);
        converterRegistry.putCustom(ModelAttrNameEnum.class, ModelAttrNameEnumConverterUtil.class);

        // bill
        converterRegistry.putCustom(OrderStatusEnum.class, OrderStatusEnumConverterUtil.class);
        converterRegistry.putCustom(OrderTypeEnum.class, OrderTypeEnumConverterUtil.class);
        converterRegistry.putCustom(SettlementStyleEnum.class, SettlementStyleEnumConverterUtil.class);

        // wechat
        converterRegistry.putCustom(WechatUseTypeEnum.class, WechatUseTypeEnumConverterUtil.class);


    }
}
