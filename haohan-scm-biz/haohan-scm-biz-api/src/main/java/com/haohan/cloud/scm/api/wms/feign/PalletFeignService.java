/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.entity.Pallet;
import com.haohan.cloud.scm.api.wms.req.PalletReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 托盘信息表内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PalletFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface PalletFeignService {


    /**
     * 通过id查询托盘信息表
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/Pallet/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 托盘信息表 列表信息
     * @param palletReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Pallet/fetchPalletPage")
    R getPalletPage(@RequestBody PalletReq palletReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 托盘信息表 列表信息
     * @param palletReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/Pallet/fetchPalletList")
    R getPalletList(@RequestBody PalletReq palletReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增托盘信息表
     * @param pallet 托盘信息表
     * @return R
     */
    @PostMapping("/api/feign/Pallet/add")
    R save(@RequestBody Pallet pallet, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改托盘信息表
     * @param pallet 托盘信息表
     * @return R
     */
    @PostMapping("/api/feign/Pallet/update")
    R updateById(@RequestBody Pallet pallet, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除托盘信息表
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/Pallet/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/Pallet/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/Pallet/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param palletReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Pallet/countByPalletReq")
    R countByPalletReq(@RequestBody PalletReq palletReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param palletReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/Pallet/getOneByPalletReq")
    R getOneByPalletReq(@RequestBody PalletReq palletReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param palletList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/Pallet/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<Pallet> palletList, @RequestHeader(SecurityConstants.FROM) String from);


}
