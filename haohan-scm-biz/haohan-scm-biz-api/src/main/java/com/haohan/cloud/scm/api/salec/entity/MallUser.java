/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 用户表
 *
 * @author haohan
 * @date 2020-05-21 18:46:48
 */
@Data
@TableName("eb_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "用户表")
public class MallUser extends Model<MallUser> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户id")
    @TableId(type = IdType.INPUT)
    private Integer uid;

    @Length(max = 32, message = "用户账号长度最大32字符")
    @ApiModelProperty(value = "用户账号")
    private String account;

    @Length(max = 32, message = "用户密码长度最大32字符")
    @ApiModelProperty(value = "用户密码")
    private String pwd;

    @Length(max = 25, message = "真实姓名长度最大25字符")
    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "生日")
    private Integer birthday;

    @Length(max = 20, message = "身份证号码长度最大20字符")
    @ApiModelProperty(value = "身份证号码")
    private String cardId;

    @Length(max = 255, message = "用户备注长度最大255字符")
    @ApiModelProperty(value = "用户备注")
    private String mark;

    @ApiModelProperty(value = "合伙人id")
    private Integer partnerId;

    @ApiModelProperty(value = "用户分组id")
    private Integer groupId;

    @Length(max = 60, message = "用户昵称长度最大60字符")
    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    @Length(max = 256, message = "用户头像长度最大256字符")
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @Length(max = 15, message = "手机号码长度最大15字符")
    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @Length(max = 16, message = "添加ip长度最大16字符")
    @ApiModelProperty(value = "添加ip")
    private String addIp;

    @ApiModelProperty(value = "最后一次登录时间")
    private Integer lastTime;

    @Length(max = 16, message = "最后一次登录ip长度最大16字符")
    @ApiModelProperty(value = "最后一次登录ip")
    private String lastIp;

    @Digits(integer = 8, fraction = 2, message = "用户余额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "用户余额")
    private BigDecimal nowMoney;

    @Digits(integer = 8, fraction = 2, message = "佣金金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "佣金金额")
    private BigDecimal brokeragePrice;

    @Digits(integer = 8, fraction = 2, message = "用户剩余积分的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "用户剩余积分")
    private BigDecimal integral;

    @ApiModelProperty(value = "连续签到天数")
    private Integer signNum;

    @ApiModelProperty(value = "1为正常，0为禁止")
    private Integer status;

    @ApiModelProperty(value = "等级")
    private Integer level;

    @ApiModelProperty(value = "推广元id")
    private Integer spreadUid;

    @ApiModelProperty(value = "推广员关联时间")
    private Integer spreadTime;

    @Length(max = 32, message = "用户类型长度最大32字符")
    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "是否为推广员")
    private Integer isPromoter;

    @ApiModelProperty(value = "用户购买次数")
    private Integer payCount;

    @ApiModelProperty(value = "下级人数")
    private Integer spreadCount;

    @ApiModelProperty(value = "清理会员时间")
    private Integer cleanTime;

    @Length(max = 255, message = "详细地址长度最大255字符")
    @ApiModelProperty(value = "详细地址")
    private String addres;

    @ApiModelProperty(value = "管理员编号")
    private Integer adminid;

    @Length(max = 36, message = "用户登陆类型，h5,wechat,routine长度最大36字符")
    @ApiModelProperty(value = "用户登陆类型，h5,wechat,routine")
    private String loginType;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
