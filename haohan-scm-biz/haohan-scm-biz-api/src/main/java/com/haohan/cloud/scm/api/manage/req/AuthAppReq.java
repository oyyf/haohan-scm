/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.AuthApp;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 授权应用管理
 *
 * @author haohan
 * @date 2019-05-28 20:33:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "授权应用管理")
public class AuthAppReq extends AuthApp {

    private long pageSize;
    private long pageNo;




}
