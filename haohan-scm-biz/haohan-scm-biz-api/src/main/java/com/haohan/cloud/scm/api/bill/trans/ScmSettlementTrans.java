package com.haohan.cloud.scm.api.bill.trans;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.bill.dto.SettlementDTO;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import lombok.Data;
import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/12/26
 */
@Data
@UtilityClass
public class ScmSettlementTrans {

    public SettlementAccount copyFinishProperty(SettlementDTO settlement) {
        SettlementAccount account = new SettlementAccount();
        account.setCompanyOperator(settlement.getCompanyOperator());
        account.setOperatorName(settlement.getOperatorName());
        if (null != settlement.getPayType()) {
            account.setPayType(settlement.getPayType());
        }
        LocalDateTime settlementTime = settlement.getSettlementTime();
        if (null == settlementTime) {
            settlementTime = LocalDateTime.now();
        }
        account.setSettlementTime(settlementTime);
        if (StrUtil.isNotEmpty(settlement.getSettlementDesc())) {
            account.setSettlementDesc(settlement.getSettlementDesc());
        }
        return account;
    }


}
