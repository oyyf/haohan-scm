/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysis;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 客户销量上报数据统计
 *
 * @author haohan
 * @date 2020-01-16 12:08:35
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户销量上报数据统计")
public class SalesReportAnalysisReq extends SalesReportAnalysis {

    private long pageSize;
    private long pageNo;

}
