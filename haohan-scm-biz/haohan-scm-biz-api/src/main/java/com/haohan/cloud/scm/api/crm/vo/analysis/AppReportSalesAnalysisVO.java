package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/11
 */
@Data
@Api("销量上报数据统计")
public class AppReportSalesAnalysisVO {

    @ApiModelProperty("今日")
    private ReportAnalysisVO today;

    @ApiModelProperty("本月")
    private ReportAnalysisVO currentMonth;

}
