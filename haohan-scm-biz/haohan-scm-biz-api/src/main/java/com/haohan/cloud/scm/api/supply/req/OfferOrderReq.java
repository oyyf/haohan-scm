/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-29 13:10:55
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "报价单")
public class OfferOrderReq extends OfferOrder {

    private long pageSize = 10;
    private long pageNo =1;
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    @NotBlank(message = "uId不能为空")
    private String uid;

}
