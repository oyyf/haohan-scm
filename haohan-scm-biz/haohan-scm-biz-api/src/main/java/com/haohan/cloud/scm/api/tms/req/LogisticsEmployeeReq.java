/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.tms.req;

import com.haohan.cloud.scm.api.tms.entity.LogisticsEmployee;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 物流部员工管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "物流部员工管理")
public class LogisticsEmployeeReq extends LogisticsEmployee {

    private long pageSize;
    private long pageNo;


}
