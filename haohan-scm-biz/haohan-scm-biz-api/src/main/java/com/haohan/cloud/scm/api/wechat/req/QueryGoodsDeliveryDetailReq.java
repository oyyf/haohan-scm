package com.haohan.cloud.scm.api.wechat.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/7/24
 */

@Data
public class QueryGoodsDeliveryDetailReq {
    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空")
    private String uid;

    /**
     * 商户id
     */
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 采购单明细编号
     */
    @NotBlank(message = "采购单明细编号不能为空")
    private String purchaseDetailSn;
}
