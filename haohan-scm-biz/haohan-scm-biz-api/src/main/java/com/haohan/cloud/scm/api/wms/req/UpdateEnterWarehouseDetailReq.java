package com.haohan.cloud.scm.api.wms.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/16
 */
@Data
@ApiModel(description = "修改入库单明细和货品信息")
public class UpdateEnterWarehouseDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "enterWarehouseSn不能为空")
    @ApiModelProperty(value = "入库单号", required = true)
    private String enterWarehouseSn;

    @NotBlank(message = "warehouseSn不能为空")
    @ApiModelProperty(value = "仓库编号", required = true)
    private String warehouseSn;

    @NotNull(message = "list不能为空")
    private List<ParamReq> list;
}
