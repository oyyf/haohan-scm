/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:29:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购单明细")
public class BuyOrderDetailReq extends BuyOrderDetail {

    private long pageSize;
    private long pageNo;

    @ApiModelProperty("商品规格id集合")
    private Set<String> modeIdSet;

}
