/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.pay.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.pay.entity.OrderPayRecord;
import com.haohan.cloud.scm.api.pay.req.OrderPayRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 支付订单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "OrderPayRecordFeignService", value = ScmServiceName.SCM_PAY)
public interface OrderPayRecordFeignService {


    /**
     * 通过id查询支付订单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/OrderPayRecord/{id}", method = RequestMethod.GET)
    R<OrderPayRecord> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 支付订单 列表信息
     * @param orderPayRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OrderPayRecord/fetchOrderPayRecordPage")
    R<Page<OrderPayRecord>> getOrderPayRecordPage(@RequestBody OrderPayRecordReq orderPayRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 支付订单 列表信息
     * @param orderPayRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/OrderPayRecord/fetchOrderPayRecordList")
    R<List<OrderPayRecord>> getOrderPayRecordList(@RequestBody OrderPayRecordReq orderPayRecordReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增支付订单
     * @param orderPayRecord 支付订单
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/add")
    R<Boolean> save(@RequestBody OrderPayRecord orderPayRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改支付订单
     * @param orderPayRecord 支付订单
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/update")
    R<Boolean> updateById(@RequestBody OrderPayRecord orderPayRecord, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除支付订单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/OrderPayRecord/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/listByIds")
    R<List<OrderPayRecord>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param orderPayRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/countByOrderPayRecordReq")
    R<Integer> countByOrderPayRecordReq(@RequestBody OrderPayRecordReq orderPayRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param orderPayRecordReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/getOneByOrderPayRecordReq")
    R<OrderPayRecord> getOneByOrderPayRecordReq(@RequestBody OrderPayRecordReq orderPayRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param orderPayRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/OrderPayRecord/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<OrderPayRecord> orderPayRecordList, @RequestHeader(SecurityConstants.FROM) String from);


}
