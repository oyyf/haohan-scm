package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/1/15
 *  SecondGroup app员工 查询其他员工区域
 *  FirstGroup  app员工 查询负责员工列表
 */
@Data
public class QueryEmployeeManageReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "需查询的员工ID不能为空", groups = {SecondGroup.class})
    @Length(max = 32, message = "需查询的员工ID最大长度32字符", groups = {SecondGroup.class})
    @ApiModelProperty(value = "需查询的员工ID")
    private String queryEmployeeId;

}
