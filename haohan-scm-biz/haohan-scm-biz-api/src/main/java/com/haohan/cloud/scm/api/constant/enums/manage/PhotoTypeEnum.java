package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/12
 * 图片类型:00:门店照片,01:商户照片,02:协议文件,03:商品照片,04:餐饮许可证,05:营业执照,06:结算卡或对公账户,07:身份证正反面,
 * 08:账单图片,09:退货图片,10:留影图片,11:上报图片,12:员工图片
 */
@Getter
@AllArgsConstructor
public enum PhotoTypeEnum implements IBaseEnum {

    /**
     * crm:客户门头照   manage:店铺轮播图、logo、二维码
     */
    ShopPhotos("00", "门店照片"),
    MerchantPhotos("01", "商户照片"),
    /**
     * crm:销售合同
     */
    ProtocolFiles("02", "协议文件"),
    /**
     * goods:零售商品
     */
    productPhotos("03", "商品照片"),
    cateringLicense("04", "餐饮许可证"),
    /**
     * crm:客户
     */
    license("05", "营业执照"),
    bankCardPhotos("06", "结算卡或对公账户"),
    idCardPhotos("07", "身份证正反面"),
    /**
     * opc:结算账单
     */
    billPhotos("08", "账单图片"),
    /**
     * afterSales:售后退货
     */
    back("09", "退货图片"),
    /**
     * crm:现场拍照、客户拜访记录、市场
     */
    memento("10", "留影图片"),
    /**
     * crm: 工作日报、数据上报
     */
    report("11", "上报图片"),
    /**
     * crm: 员工头像
     */
    employee("12", "员工图片");

    private static final Map<String, PhotoTypeEnum> MAP = new HashMap<>(16);

    static {
        for (PhotoTypeEnum e : PhotoTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static PhotoTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
