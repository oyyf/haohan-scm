package com.haohan.cloud.scm.api.aftersales.req;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/7
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class AddBuyReturnOrderReq extends BuyOrder {

    private List<BuyOrderDetail> detailList;

    private String returnType;

    private BigDecimal otherAmount;

    private String returnReason;

    /**
     * 退货图片id列表
     */
    @ApiModelProperty(value = "退货图片id列表")
    private List<String> photoList;
}