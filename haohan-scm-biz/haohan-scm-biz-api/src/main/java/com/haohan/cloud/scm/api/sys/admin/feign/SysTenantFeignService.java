package com.haohan.cloud.scm.api.sys.admin.feign;

import com.pig4cloud.pigx.admin.api.entity.SysTenant;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.constant.ServiceNameConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @author dy
 * @date 2020/1/17
 */
@FeignClient(contextId = "SysTenantFeignService", value = ServiceNameConstants.UPMS_SERVICE)
public interface SysTenantFeignService {

    /**
     * 查询全部有效的租户
     *
     * @param from
     * @return
     */
    @GetMapping("/api/feign/tenant/list")
    R<List<SysTenant>> normalList(@RequestHeader(SecurityConstants.FROM) String from);


}
