/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.DealCostDetail;
import com.haohan.cloud.scm.api.opc.req.DealCostDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 交易成本明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "DealCostDetailFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface DealCostDetailFeignService {


    /**
     * 通过id查询交易成本明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/DealCostDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 交易成本明细 列表信息
     * @param dealCostDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DealCostDetail/fetchDealCostDetailPage")
    R getDealCostDetailPage(@RequestBody DealCostDetailReq dealCostDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 交易成本明细 列表信息
     * @param dealCostDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/DealCostDetail/fetchDealCostDetailList")
    R getDealCostDetailList(@RequestBody DealCostDetailReq dealCostDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/add")
    R save(@RequestBody DealCostDetail dealCostDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/update")
    R updateById(@RequestBody DealCostDetail dealCostDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除交易成本明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/DealCostDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/countByDealCostDetailReq")
    R countByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/getOneByDealCostDetailReq")
    R getOneByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param dealCostDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/DealCostDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<DealCostDetail> dealCostDetailList, @RequestHeader(SecurityConstants.FROM) String from);


}
