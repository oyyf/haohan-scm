/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品
 *
 * @author haohan
 * @date 2019-05-13 18:46:46
 */
@Data
@TableName("scm_cms_goods")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品")
public class Goods extends Model<Goods> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 商品分类
     */
    private String goodsCategoryId;
    /**
     * 商品唯一编号
     */
    private String goodsSn;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品描述
     */
    private String detailDesc;
    /**
     * 概要描述  用于商品名称别名搜索
     */
    private String simpleDesc;
    /**
     * 商品品牌ID
     */
    private String brandId;
    /**
     * 售卖规则标记
     */
    private YesNoEnum saleRule;
    /**
     * 图片组编号
     */
    private String photoGroupNum;
    /**
     * 缩略图地址
     */
    private String thumbUrl;
    /**
     * 是否上架 YesNoEnum  0.否 1.是
     */
    private Integer isMarketable;
    /**
     * 库存数量
     */
    private BigDecimal storage;
    /**
     * 服务选项标记
     */
    private YesNoEnum serviceSelection;
    /**
     * 配送规则标记
     */
    private YesNoEnum deliveryRule;
    /**
     * 赠品标记
     */
    private YesNoEnum goodsGift;
    /**
     * 商品规格标记 (默认至少1个规格, yes表示有多个规格)
     */
    private YesNoEnum goodsModel;
    /**
     * 商品状态(出售中/仓库中/已售罄)
     */
    private GoodsStatusEnum goodsStatus;
    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    private GoodsFromTypeEnum goodsFrom;
    /**
     * 排序
     */
    private String sort;
    /**
     * 商品类型
     */
    private GoodsTypeEnum goodsType;
    /**
     * 扫码购编码
     */
    private String scanCode;
    /**
     * 第三方编号/即速商品id (未使用)
     */
    private String thirdGoodsSn;
    /**
     * 公共商品库通用编号  (未使用)
     */
    private String generalSn;
    /**
     * 是否c端销售
     */
    private YesNoEnum salecFlag;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
