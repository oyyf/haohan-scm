package com.haohan.cloud.scm.api.constant.enums.opc;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/1/6
 * 发货方式:1.第三方物流 2.自配送
 */
@Getter
@AllArgsConstructor
public enum ShipTypeEnum implements IBaseEnum {

    /**
     * 发货方式:1.第三方物流 2.自配送
     */
    third("1", "第三方物流"),
    self("2", "自配送");

    private static final Map<String, ShipTypeEnum> MAP = new HashMap<>(8);

    static {
        for (ShipTypeEnum e : ShipTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static ShipTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
