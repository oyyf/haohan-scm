package com.haohan.cloud.scm.api.crm.vo.app;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/22
 */
@Data
public class ContactVO implements Serializable {

    private String title;
    private List<EmployeeVO> userList;

}
