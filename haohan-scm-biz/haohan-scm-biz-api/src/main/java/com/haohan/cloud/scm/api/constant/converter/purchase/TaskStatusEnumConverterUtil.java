package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;

/**
 * @author cx
 * @date 2019/6/4
 */
public class TaskStatusEnumConverterUtil implements Converter<TaskStatusEnum> {
    @Override
    public TaskStatusEnum convert(Object o, TaskStatusEnum reviewTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return TaskStatusEnum.getByType(o.toString());
    }
}
