package com.haohan.cloud.scm.api.crm.req.analysis;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.analysis.DataReportAnalysisDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/11
 */
@Data
@Api("数据上报相关统计分析")
public class ReportAnalysisReq {

    @NotBlank(message = "员工id不能为空")
    @Length(max = 32, message = "员工id字符长度在0至32之间")
    @ApiModelProperty(value = "员工id")
    private String employeeId;

    @ApiModelProperty(value = "查询开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Length(min = 0, max = 32, message = "区域编码长度在0至32之间")
    @ApiModelProperty(value = "区域编码")
    private String areaSn;

    @Length(max = 32, message = "市场编码字符长度在0至32之间")
    @ApiModelProperty(value = "市场编码")
    private String marketSn;

    @ApiModelProperty(value = "商品销售类型:1.普通2.特价3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @ApiModelProperty(value = "是否只查询app员工自己")
    private boolean selfFlag;

    @Length(max = 32, message = "数据上报人id字符长度在0至32之间")
    @ApiModelProperty(value = "数据上报人id")
    private String reportManId;

    public DataReportAnalysisDTO transToDTO(Page page) {
        DataReportAnalysisDTO analysisDTO = new DataReportAnalysisDTO();
        analysisDTO.queryPage(page);
        analysisDTO.setEmployeeId(this.getEmployeeId());
        analysisDTO.queryDate(this.getStartDate(), this.getEndDate());
        analysisDTO.setAreaSn(this.areaSn);
        analysisDTO.setMarketSn(this.marketSn);
        analysisDTO.setSalesGoodsType(this.getSalesGoodsType());
        analysisDTO.setReportManId(this.reportManId);
        return analysisDTO;
    }
}
