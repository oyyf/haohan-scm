package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/18
 */
@Data
@ApiModel(description = "查看商品库存需求")
public class QueryGoodsStorageReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家id",required = true)
    private String pmId;

    @NotBlank(message = "goodsModelId不能为空")
    @ApiModelProperty(value = "商品规格id" , required = true)
    private String goodsModelId;
}
