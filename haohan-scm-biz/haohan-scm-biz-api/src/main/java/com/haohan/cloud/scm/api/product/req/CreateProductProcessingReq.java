package com.haohan.cloud.scm.api.product.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author cx
 * @date 2019/6/14
 */
@Data
@ApiModel(description = "创建货品加工记录需求")
public class CreateProductProcessingReq {

    @NotBlank(message = "productSn不能为空")
    @ApiModelProperty(value = "货品编号" , required = true)
    private String productSn;

    @NotBlank(message = "subProductNum")
    @ApiModelProperty(value = "货品数量" , required = true)
    private BigDecimal subProductNum;

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家平台id" , required= true)
    private String pmId;

    @ApiModelProperty(value = "操作人id")
    private String operatorId;

    @ApiModelProperty(value = "暂存点编号")
    private String storagePlaceSn;

    @ApiModelProperty(value = "加工时间")
    private LocalDateTime processingTime;
}


