package com.haohan.cloud.scm.api.opc.dto;

import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BillPaymentInfoDTO extends BillPayment {

    /**
     * 其他金额   （账单金额 = 其他金额+明细合计金额）
     */
    private BigDecimal otherAmount;

    /**
     * 账单明细列表
     */
    private List<BillPaymentDetail> detailList;

    public BillPaymentInfoDTO(BuyerPayment payment) {
        super(payment);
    }

    public BillPaymentInfoDTO(SupplierPayment payment) {
        super(payment);
    }
}
