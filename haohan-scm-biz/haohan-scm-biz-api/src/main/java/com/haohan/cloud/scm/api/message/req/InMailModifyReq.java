package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MessageTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2020/3/21
 */
@Data
public class InMailModifyReq {

    @NotBlank(message = "消息编号不能为空")
    @Length(max = 32, message = "消息编号最大长度32字符")
    private String messageSn;

    @Length(max = 32, message = "消息标题最大长度32字符")
    private String title;

    @Length(max = 5000, message = "消息内容最大长度5000字符")
    private String content;

    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    private DepartmentTypeEnum departmentType;

    /**
     * 消息类型:1微信2站内信3短信
     */
    private MessageTypeEnum messageType;
    /**
     * 业务类型:各部门的消息
     */
    private MsgActionTypeEnum msgActionType;

    /**
     * 站内信类型:1公告2及时通信
     */
    private InMailTypeEnum inMailType;
    /**
     * 发送时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sendTime;
    /**
     * 发送人uid
     */
    @Length(max = 32, message = "发送人uid最大长度32字符")
    private String senderUid;
    /**
     * 发送人名称
     */
    @Length(max = 32, message = "发送人名称最大长度32字符")
    private String senderName;
    /**
     * 请求参数  (json 详情查询 编号)
     */
    @Length(max = 1000, message = "请求参数最大长度1000字符")
    private String reqParams;

    public InMailRecord tranToInMail() {
        InMailRecord msg = new InMailRecord();
        msg.setTitle(this.title);
        msg.setContent(this.content);
        msg.setInMailType(this.inMailType);
        msg.setSenderUid(this.senderUid);
        msg.setSenderName(this.senderName);
        msg.setDepartmentType(this.departmentType);
        msg.setMsgActionType(this.msgActionType);
        msg.setReqParams(this.reqParams);
        msg.setSendTime(sendTime);
        return msg;
    }

    public MessageRecord tranToMessage() {
        MessageRecord msg = new MessageRecord();
        msg.setMessageType(MessageTypeEnum.inMail);
        msg.setDepartmentType(this.departmentType);
        msg.setMsgActionType(this.msgActionType);

        msg.setSenderUid(this.senderUid);
        msg.setSenderName(this.senderName);
        msg.setSendTime(sendTime);
        msg.setTitle(this.title);
        return msg;
    }
}
