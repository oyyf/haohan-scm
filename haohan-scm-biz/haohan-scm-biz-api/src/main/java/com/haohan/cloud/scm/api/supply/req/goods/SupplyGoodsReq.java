package com.haohan.cloud.scm.api.supply.req.goods;

import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.goods.req.GoodsFeignReq;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2020/4/17
 */
@Data
public class SupplyGoodsReq {

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    @Length(max = 32, message = "商品分类id的最大长度为32字符")
    @ApiModelProperty(value = "商品分类id")
    private String categoryId;

    @ApiModelProperty(value = "商品状态: 0 出售中 1 仓库中 2 已售罄")
    private GoodsStatusEnum goodsStatus;

    @ApiModelProperty(value = "上下架状态")
    private YesNoEnum marketableFlag;

    @Length(max = 32, message = "扫码购编号的最大长度为32字符")
    @ApiModelProperty(value = "扫码购编号")
    private String scanCode;


    @NotBlank(message = "商品id不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "商品id的最大长度为32字符")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    // 非eq 参数

    @Length(max = 10, message = "商品名称的最大长度为10字符")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @Length(max = 32, message = "商品编号的最大长度为32字符")
    @ApiModelProperty(value = "商品编号")
    private String goodsSn;


    public GoodsFeignReq tranTo() {
        GoodsFeignReq req = new GoodsFeignReq();
        req.setShopId(this.shopId);
        req.setCategoryId(this.categoryId);
        req.setGoodsStatus(this.goodsStatus);
        req.setMarketableFlag(this.marketableFlag);
        req.setScanCode(this.scanCode);
        req.setGoodsName(this.goodsName);
        req.setGoodsSn(this.goodsSn);
        return req;
    }
}
