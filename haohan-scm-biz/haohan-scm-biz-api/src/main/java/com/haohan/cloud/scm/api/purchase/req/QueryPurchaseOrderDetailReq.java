package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Data
@ApiModel(description = "查询采购单明细")
public class QueryPurchaseOrderDetailReq {
    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "采购单明细id", required = true)
    private String id;

    @ApiModelProperty(value = "采购员工通行证id", required = true)
    private String uId;
}
