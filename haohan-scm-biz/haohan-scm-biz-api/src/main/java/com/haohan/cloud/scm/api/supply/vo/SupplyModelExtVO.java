package com.haohan.cloud.scm.api.supply.vo;

import com.haohan.cloud.scm.api.constant.enums.supply.SupplyRelationTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author dy
 * @date 2020/4/28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SupplyModelExtVO extends SupplyModelVO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "供应商家名称")
    private String merchantName;

    @ApiModelProperty(value = "供应商店铺id")
    private String shopId;

    public SupplyModelExtVO(GoodsModelDTO model) {
        super(model);
        this.shopId = model.getShopId();
        this.setRelationType(SupplyRelationTypeEnum.merchant);
    }
}
