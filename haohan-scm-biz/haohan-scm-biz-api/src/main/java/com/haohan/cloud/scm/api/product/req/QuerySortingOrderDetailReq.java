package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.product.SortingStatusEnum;
import com.haohan.cloud.scm.api.product.dto.SortingOrderDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

/**
 * @author cx
 * @date 2019/7/8
 */
@Data
@ApiModel("查询分拣单明细列表")
public class QuerySortingOrderDetailReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家ID" ,required = true)
    private String pmId;

    @ApiModelProperty(value = "送货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "送货批次:0第一批1第二批")
    private BuySeqEnum deliverySeq;

    @ApiModelProperty(value = "客户id")
    private String buyerId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "分拣状态:1待分拣2.可分拣3已分拣4已完成")
    private SortingStatusEnum sortingStatus;

    @ApiModelProperty(value = "商品规格id")
    private String goodsModelId;

    public SortingOrderDetailDTO transToDTO() {
        SortingOrderDetailDTO dto = new SortingOrderDetailDTO();
        dto.setPmId(this.pmId);
        dto.setDeliveryDate(this.deliveryDate);
        if(null != this.deliverySeq){
            dto.setDeliverySeq(this.deliverySeq.getType());
        }
        dto.setBuyerId(this.buyerId);
        dto.setGoodsName(this.goodsName);
        if(null != this.sortingStatus){
            dto.setSortingStatus(this.sortingStatus.getType());
        }
        dto.setGoodsModelId(this.goodsModelId);
        return dto;
    }
}
