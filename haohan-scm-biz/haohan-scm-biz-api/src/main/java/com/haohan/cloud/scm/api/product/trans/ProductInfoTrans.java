package com.haohan.cloud.scm.api.product.trans;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.product.*;
import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;

import java.math.BigDecimal;

/**
 * @program: haohan-fresh-scm
 * @descriptioni:方法转换
 * @author: Simon
 * @create: 2019-05-28
 **/
public class ProductInfoTrans {


    /**
     * 根据采购单明细 初始设置 货品信息
     * @param orderDetail
     * @return
     */
    public static ProductInfo trans(PurchaseOrderDetail orderDetail) {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setPmId(orderDetail.getPmId());
        productInfo.setGoodsId(orderDetail.getGoodsId());
        productInfo.setGoodsModelId(orderDetail.getGoodsModelId());
        productInfo.setProductName(orderDetail.getGoodsName() + "|" + orderDetail.getModelName());
        productInfo.setUnit(orderDetail.getUnit());
        productInfo.setProductNumber(orderDetail.getNeedBuyNum());
        productInfo.setProductStatus(ProductStatusEnum.normal);
        productInfo.setProductQuality(ProductQialityEnum.normal);
        productInfo.setProductPlaceStatus(ProductPlaceStatusEnum.purchase);
        productInfo.setSupplierId(orderDetail.getSupplierId());
        productInfo.setSupplierName(orderDetail.getSupplierName());
        productInfo.setPurchaseDetailSn(orderDetail.getPurchaseDetailSn());
        productInfo.setPurchasePrice(orderDetail.getBuyPrice());
        productInfo.setPurchaseTime(orderDetail.getReceiveTime());

        // 货品类型 协议供应
        ProductTypeEnum productType = ProductTypeEnum.purchase;
        if(orderDetail.getMethodType() == MethodTypeEnum.agreement){
            productType = ProductTypeEnum.supply;
        }
        productInfo.setProductType(productType);
        productInfo.setProcessingType(ProcessingTypeEnum.not_handle);
        // 来源货品编号设置  此时为初始货品
        productInfo.setSourceProductSn(ScmCommonConstant.ORIGINAL_PRODUCT_SN);
        return productInfo;
    }

    /**
     * 根据原货品 创建新货品
     *   id / productSn 此时为null
     * @param source 原货品
     * @param num 数量
     * @return
     */
    public static ProductInfo initFromSource(ProductInfo source, BigDecimal num) {
        ProductInfo productInfo = new ProductInfo();
        // 属性从原货品复制
        String[] ignoreProperties = {"id", "productSn", "productNumber", "sourceProductSn",
                "productStatus", "productQuality", "processingType",
                "createBy", "createDate", "updateBy", "updateDate", "remarks", "delFlag", "tenantId"};
        BeanUtil.copyProperties(source, productInfo, ignoreProperties);
        productInfo.setProductNumber(num);
        // 来源货品编号设置   使用初始货品的编号(其来源货品编号为0)
        String sourceSn = source.getProductSn();
        if(!StrUtil.equals(sourceSn, ScmCommonConstant.ORIGINAL_PRODUCT_SN)){
            sourceSn = source.getSourceProductSn();
        }
        productInfo.setSourceProductSn(sourceSn);
        productInfo.setProductStatus(ProductStatusEnum.normal);
        productInfo.setProductQuality(ProductQialityEnum.normal);
        productInfo.setProcessingType(ProcessingTypeEnum.not_handle);
        return productInfo;
    }

    /**
     * 根据 商品信息初始化
     *   缺 pmId
     * @param dto
     * @return
     */
    public static ProductInfo createProductInfo(GoodsModelDTO dto){
        ProductInfo info = new ProductInfo();
        info.setGoodsId(dto.getGoodsId());
        info.setGoodsModelId(dto.getId());
        info.setProductName(dto.getGoodsName() + "|" + dto.getModelName());
        info.setUnit(dto.getModelUnit());
        info.setProductStatus(ProductStatusEnum.normal);
        info.setProductQuality(ProductQialityEnum.normal);
        info.setProductPlaceStatus(ProductPlaceStatusEnum.stock);
        info.setProductType(ProductTypeEnum.purchase);
        info.setPurchasePrice(dto.getModelPrice());

        return info;

    }

}
