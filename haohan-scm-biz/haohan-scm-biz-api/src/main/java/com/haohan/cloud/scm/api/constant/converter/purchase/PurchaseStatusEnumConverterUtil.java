package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.PurchaseStatusEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class PurchaseStatusEnumConverterUtil implements Converter<PurchaseStatusEnum> {
    @Override
    public PurchaseStatusEnum convert(Object o, PurchaseStatusEnum purchaseStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return PurchaseStatusEnum.getByType(o.toString());
    }
}
