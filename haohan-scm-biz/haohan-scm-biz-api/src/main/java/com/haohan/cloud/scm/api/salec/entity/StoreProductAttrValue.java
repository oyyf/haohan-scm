/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.salec.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * 商品属性值表
 *
 * @author haohan
 * @date 2020-05-21 18:42:21
 */
@Data
@TableName("eb_store_product_attr_value")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品属性值表")
public class StoreProductAttrValue extends Model<StoreProductAttrValue> {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value = "主键", notes = "对应商品规格id")
    private String id;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    /**
     * 规格名称
     */
    @Length(max = 128, message = "商品属性索引值 (attr_value|attr_value[|....])长度最大128字符")
    @ApiModelProperty(value = "商品属性索引值 (attr_value|attr_value[|....])")
    private String suk;

    @ApiModelProperty(value = "属性对应的库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sales;

    @Digits(integer = 8, fraction = 2, message = "属性金额的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "属性金额")
    private BigDecimal price;

    @Length(max = 128, message = "图片长度最大128字符")
    @ApiModelProperty(value = "图片")
    private String image;

    @Length(max = 8, message = "唯一值长度最大8字符")
    @ApiModelProperty(value = "唯一值")
    @TableField("`unique`")
    private String unique;

    @Digits(integer = 8, fraction = 2, message = "成本价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @Length(max = 50, message = "商品条码长度最大50字符")
    @ApiModelProperty(value = "商品条码")
    private String barCode;

    @Digits(integer = 8, fraction = 2, message = "原价的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "原价")
    private BigDecimal otPrice;

    @Digits(integer = 8, fraction = 2, message = "重量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "重量")
    private BigDecimal weight;

    @Digits(integer = 8, fraction = 2, message = "体积的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "体积")
    private BigDecimal volume;

    @Digits(integer = 8, fraction = 2, message = "一级返佣的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "一级返佣")
    private BigDecimal brokerage;

    @Digits(integer = 8, fraction = 2, message = "二级返佣的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "二级返佣")
    private BigDecimal brokerageTwo;

    @ApiModelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @ApiModelProperty(value = "活动限购数量")
    private Integer quota;

    @ApiModelProperty(value = "活动限购数量显示")
    private Integer quotaShow;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
