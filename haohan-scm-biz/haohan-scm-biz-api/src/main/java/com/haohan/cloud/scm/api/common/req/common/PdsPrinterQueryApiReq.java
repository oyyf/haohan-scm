package com.haohan.cloud.scm.api.common.req.common;

import com.haohan.cloud.scm.api.common.req.admin.PdsBaseApiReq;

import javax.validation.constraints.NotBlank;

/**
 * @author shenyu
 * @create 2018/12/17
 */
public class PdsPrinterQueryApiReq extends PdsBaseApiReq {
    @NotBlank(message = "shopId不能为空")
    private String shopId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
