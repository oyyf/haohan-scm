package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseAddPurchaseOrderDetailReq {

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 汇总单号
     */
    private String summaryOrderId;
}
