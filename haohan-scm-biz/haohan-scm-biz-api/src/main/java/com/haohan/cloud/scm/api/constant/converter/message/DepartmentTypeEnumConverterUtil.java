package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class DepartmentTypeEnumConverterUtil implements Converter<DepartmentTypeEnum> {
    @Override
    public DepartmentTypeEnum convert(Object o, DepartmentTypeEnum departmentTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DepartmentTypeEnum.getByType(o.toString());
    }
}
