package com.haohan.cloud.scm.api.constant.enums.crm;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/24
 */
@Getter
@AllArgsConstructor
public enum DynamicTypeEnum implements IBaseEnum {

    /**
     * 动态类型 1.销售订单 2.销量上报 3.库存上报 4.竞品上报 5.客户采购
     */
    order("1", "销售订单"),
    sales("2", "销量上报"),
    stock("3", "库存上报"),
    competition("4", "竞品上报"),
    buy("5", "客户采购");

    private static final Map<String, DynamicTypeEnum> MAP = new HashMap<>(8);

    static {
        for (DynamicTypeEnum e : DynamicTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static DynamicTypeEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
