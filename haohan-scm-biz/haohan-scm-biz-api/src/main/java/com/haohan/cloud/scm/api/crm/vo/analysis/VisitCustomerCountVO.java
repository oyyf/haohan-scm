package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dy
 * @date 2019/11/7
 */
@Data
@Api("客户拜访数统计")
public class VisitCustomerCountVO {

    @ApiModelProperty(value = "客户总数")
    private Integer customerNum;

    @ApiModelProperty(value = "拜访客户数")
    private Integer visitNum;

    @ApiModelProperty(value = "未拜访客户数")
    private Integer waitNum;

}
