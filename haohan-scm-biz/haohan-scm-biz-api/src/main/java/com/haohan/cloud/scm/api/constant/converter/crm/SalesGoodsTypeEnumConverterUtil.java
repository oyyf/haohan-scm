package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;

/**
 * @author dy
 * @date 2019/8/31
 */
public class SalesGoodsTypeEnumConverterUtil implements Converter<SalesGoodsTypeEnum> {
    @Override
    public SalesGoodsTypeEnum convert(Object o, SalesGoodsTypeEnum useStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return SalesGoodsTypeEnum.getByType(o.toString());
    }

}
