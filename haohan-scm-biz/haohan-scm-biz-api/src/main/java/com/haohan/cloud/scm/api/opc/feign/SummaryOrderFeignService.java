/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.req.SummaryOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购单汇总内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SummaryOrderFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface SummaryOrderFeignService {


    /**
     * 通过id查询采购单汇总
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SummaryOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购单汇总 列表信息
     * @param summaryOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SummaryOrder/fetchSummaryOrderPage")
    R getSummaryOrderPage(@RequestBody SummaryOrderReq summaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购单汇总 列表信息
     * @param summaryOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SummaryOrder/fetchSummaryOrderList")
    R getSummaryOrderList(@RequestBody SummaryOrderReq summaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/add")
    R save(@RequestBody SummaryOrder summaryOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/update")
    R updateById(@RequestBody SummaryOrder summaryOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购单汇总
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/SummaryOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/countBySummaryOrderReq")
    R countBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/getOneBySummaryOrderReq")
    R getOneBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param summaryOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/SummaryOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<SummaryOrder> summaryOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
