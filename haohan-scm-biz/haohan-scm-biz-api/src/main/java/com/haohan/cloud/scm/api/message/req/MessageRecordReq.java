/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 消息发送记录
 *
 * @author haohan
 * @date 2019-05-28 20:03:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "消息发送记录")
public class MessageRecordReq extends MessageRecord {

    private long pageSize;
    private long pageNo;




}
