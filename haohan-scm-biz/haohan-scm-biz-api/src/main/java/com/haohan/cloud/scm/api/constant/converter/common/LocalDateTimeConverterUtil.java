package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author dy
 * @date 2019/6/4
 */
public class LocalDateTimeConverterUtil implements Converter<LocalDateTime> {
    @Override
    public LocalDateTime convert(Object o, LocalDateTime useStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(o.toString(), df);
    }
}
