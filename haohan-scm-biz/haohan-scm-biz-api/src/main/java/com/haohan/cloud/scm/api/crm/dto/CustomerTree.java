package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.common.tree.TreeNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/9/27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerTree extends TreeNode<CustomerTree> {

    /**
     * 客户编码
     */
    @ApiModelProperty(value = "客户编码")
    private String customerSn;
    /**
     * 客户名称
     */
    @ApiModelProperty(value = "客户名称")
    private String customerName;

}
