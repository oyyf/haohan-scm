package com.haohan.cloud.scm.api.crm.dto;

import com.haohan.cloud.scm.api.crm.entity.SalesArea;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SalesAreaDTO extends SalesArea {

    private SalesArea parentArea;

    public SalesArea transTo() {
        SalesArea area = new SalesArea();
        if (null != this.parentArea) {
            area.setParentId(this.parentArea.getId());
            area.setParentName(this.parentArea.getAreaName());
            area.setAreaLevel(this.parentArea.getAreaLevel().getNext());
        } else {
            area.setParentId(null);
        }
        area.setId(this.getId());
        area.setAreaSn(this.getAreaSn());
        area.setAreaName(this.getAreaName());
        area.setBelongDept(this.getBelongDept());
        area.setSort(this.getSort());
        return area;
    }

}
