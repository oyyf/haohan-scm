/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;
import com.haohan.cloud.scm.api.purchase.req.PurchaseGoodsReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购员工管理商品内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "PurchaseGoodsFeignService", value = ScmServiceName.SCM_BIZ_PURCHASE)
public interface PurchaseGoodsFeignService {


    /**
     * 通过id查询采购员工管理商品
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/PurchaseGoods/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购员工管理商品 列表信息
     * @param purchaseGoodsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseGoods/fetchPurchaseGoodsPage")
    R getPurchaseGoodsPage(@RequestBody PurchaseGoodsReq purchaseGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购员工管理商品 列表信息
     * @param purchaseGoodsReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/PurchaseGoods/fetchPurchaseGoodsList")
    R getPurchaseGoodsList(@RequestBody PurchaseGoodsReq purchaseGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/add")
    R save(@RequestBody PurchaseGoods purchaseGoods, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/update")
    R updateById(@RequestBody PurchaseGoods purchaseGoods, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购员工管理商品
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/PurchaseGoods/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/countByPurchaseGoodsReq")
    R countByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/getOneByPurchaseGoodsReq")
    R getOneByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param purchaseGoodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/PurchaseGoods/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<PurchaseGoods> purchaseGoodsList, @RequestHeader(SecurityConstants.FROM) String from);


}
