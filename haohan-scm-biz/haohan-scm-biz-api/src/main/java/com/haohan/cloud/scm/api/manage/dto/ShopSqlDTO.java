package com.haohan.cloud.scm.api.manage.dto;

import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2020/3/30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShopSqlDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "店铺启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "店铺等级", notes = "原系统使用字典 0 总店, 1 分店 2采购配送店")
    private ShopLevelEnum shopLevel;

    @ApiModelProperty(value = "商家id")
    private String merchantId;

    // 非eq

    @ApiModelProperty(value = "店铺名称")
    private String name;

    @ApiModelProperty(value = "店铺地址")
    private String address;

    @ApiModelProperty(value = "店铺负责人名称")
    private String manager;

    @ApiModelProperty(value = "店铺电话")
    private String telephone;

    @ApiModelProperty(value = "店铺服务")
    private String shopService;

    @ApiModelProperty(value = "店铺介绍")
    private String shopDesc;

    @ApiModelProperty(value = "行业名称")
    private String industry;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    // 字典处理(自定义sql使用)

    public String getShopLevel(){
        return null == this.shopLevel ? null : this.shopLevel.getType();
    }

    public String getStatus(){
        return null == this.status ? null : this.status.getType();
    }
}
