/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.DataReportDetail;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 数据汇报明细
 *
 * @author haohan
 * @date 2019-08-30 11:45:13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "数据汇报明细")
public class DataReportDetailReq extends DataReportDetail {

    private long pageSize;
    private long pageNo;


}
