package com.haohan.cloud.scm.api.constant.converter.goods;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.goods.ModelAttrNameEnum;

/**
 * @author dy
 * @date 2019/10/17
 */
public class ModelAttrNameEnumConverterUtil implements Converter<ModelAttrNameEnum> {
    @Override
    public ModelAttrNameEnum convert(Object o, ModelAttrNameEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ModelAttrNameEnum.getByType(o.toString());
    }
}
