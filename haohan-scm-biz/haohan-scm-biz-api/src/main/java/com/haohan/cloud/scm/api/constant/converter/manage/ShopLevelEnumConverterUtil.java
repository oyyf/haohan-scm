package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.ShopLevelEnum;

/**
 * @author dy
 * @date 2019/9/14
 */
public class ShopLevelEnumConverterUtil implements Converter<ShopLevelEnum> {
    @Override
    public ShopLevelEnum convert(Object o, ShopLevelEnum shopLevelEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShopLevelEnum.getByType(o.toString());
    }
}