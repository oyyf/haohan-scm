package com.haohan.cloud.scm.api.wechat.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/7/23
 */
@Data
@ApiModel("微信小程序登录")
public class WechatLoginReq {

    @ApiModelProperty(value = "通行证id", required = true)
    @NotBlank(message = "通行证uid不能为空")
    private String uid;

//    @ApiModelProperty(value = "平台商家id")
//    private String pmId;

}
