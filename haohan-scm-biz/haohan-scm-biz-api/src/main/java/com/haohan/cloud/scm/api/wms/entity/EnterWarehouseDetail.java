/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.wms.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.product.StorageTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 入库单明细
 *
 * @author haohan
 * @date 2019-05-13 21:28:43
 */
@Data
@TableName("scm_pws_enter_warehouse_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "入库单明细")
public class EnterWarehouseDetail extends Model<EnterWarehouseDetail> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 入库单编号
     */
    private String enterWarehouseSn;
    /**
     * 入库单明细编号
     */
    private String enterWarehouseDetailSn;
    /**
     * 仓库编号
     */
    private String warehouseSn;
    /**
     * 货架编号
     */
    private String shelfSn;
    /**
     * 货位编号
     */
    private String cellSn;
    /**
     * 托盘编号
     */
    private String palletSn;
    /**
     * 汇总单编号
     */
    private String summaryOrderId;
    /**
     * 采购单明细编号
     */
    private String purchaseDetailSn;
    /**
     * 入库商品规格id
     */
    private String goodsModelId;
    /**
     * 入库货品编号
     */
    private String productSn;
    /**
     * 入库货品数量
     */
    private BigDecimal productNumber;
    /**
     * 货品单位
     */
    private String unit;
    /**
     * 入库申请人
     */
    private String applicantId;
    /**
     * 申请人名称
     */
    private String applicantName;
    /**
     * 入库申请时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applyTime;
    /**
     * 验收人
     */
    private String auditorId;
    /**
     * 验收人名称
     */
    private String auditorName;
    /**
     * 验收时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;
    /**
     * 入库状态:1待处理2验收中3已验收4.售后待处理
     */
    private EnterStatusEnum enterStatus;
    /**
     * 货品处理类型:1.入库存储2.转配送
     */
    private StorageTypeEnum storageType;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
