package com.haohan.cloud.scm.api.opc.req;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/7/26
 */
@Data
@ApiModel(description = "完成结算")
public class CompleteSettlementReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "settlementSn不能为空")
    @ApiModelProperty(value = "结算记录编号", required = true)
    private String settlementSn;

    @NotNull(message = "settlementAmount")
    @ApiModelProperty(value = "结算金额", required = true)
    private BigDecimal settlementAmount;

    @NotNull(message = "payType不能为空")
    @ApiModelProperty(value = "支付方式", required = true)
    private PayTypeEnum payType;

    @NotNull(message = "payDate不能为空")
    @ApiModelProperty(value = "付款时间", required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payDate;

    @NotBlank(message = "companyOperator不能为空")
    @ApiModelProperty(value = "结款人名称", required = true)
    @Length(min = 1, max = 20, message = "companyOperator长度为1-20字符")
    private String companyOperator;

    @NotBlank(message = "operator不能为空")
    @ApiModelProperty(value = "经办人名称", required = true)
    @Length(min = 1, max = 20, message = "operator长度为1-20字符")
    private String operator;

    @ApiModelProperty(value = "结算说明")
    @Length(min = 0, max = 500, message = "settlementDesc长度需小于500字符")
    private String settlementDesc;

    @ApiModelProperty(value = "结算凭证图片组编号")
    @Length(min = 0, max = 500, message = "groupNum长度需小于500字符")
    private String groupNum;

    @NotEmpty(message = "photoIdList不能为空")
    @ApiModelProperty(value = "结算凭证图片资源id列表", required = true)
    private List<String> photoIdList;

    public SettlementRecord transTo() {
        SettlementRecord record = new SettlementRecord();
        BeanUtil.copyProperties(this,record);
        return record;
    }
}
