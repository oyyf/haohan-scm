package com.haohan.cloud.scm.api.supply.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsFromTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyRelationTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsExtDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/4/17
 * 供应商品 对 spu
 */
@Data
@NoArgsConstructor
public class SupplyGoodsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品id")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    @ApiModelProperty(value = "商品描述")
    private String detailDesc;

    @ApiModelProperty(value = "图片地址")
    private String thumbUrl;

    @ApiModelProperty(value = "商品唯一编号")
    private String goodsSn;
    /**
     * 概要描述  用于商品名称别名搜索
     */
    private String simpleDesc;
    /**
     * 库存数量
     */
    private Integer storage;
    /**
     * 排序
     */
    private String sort;
    /**
     * 扫码购编码
     */
    private String scanCode;
    /**
     * 公共商品库通用编号
     */
    private String generalSn;

    // 状态属性
    /**
     * 是否上架 YesNoEnum  0.否 1.是
     */
    private Integer isMarketable;
    /**
     * 商品状态(出售中/仓库中/已售罄)
     */
    private GoodsStatusEnum goodsStatus;
    /**
     * 商品来源 0.小店平台 1.即速应用
     */
    private GoodsFromTypeEnum goodsFrom;
    /**
     * 商品类型
     */
    private GoodsTypeEnum goodsType;
    /**
     * 是否c端销售
     */
    private YesNoEnum salecFlag;

    // 默认价格属性
    /**
     * 零售定价,单位元 (使用,市场价)
     */
    private BigDecimal marketPrice;
    /**
     * vip定价,单位元  (会员价)
     */
    private BigDecimal vipPrice;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 计量单位
     */
    private String unit;

    // 其他属性

    @ApiModelProperty(value = "行业名称")
    private String industry;

    @ApiModelProperty(value = "品牌名称")
    private String brand;

    @ApiModelProperty(value = "厂家制造商")
    private String manufacturer;

    // 商家店铺

    @ApiModelProperty(value = "店铺ID")
    private String shopId;

    @ApiModelProperty(value = "商家ID")
    private String merchantId;

    // 分类

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    // 扩展属性

    @ApiModelProperty(value = "关联类型", notes = "关联类型: 0.未关联 1.关联商家 2.关联供应人")
    private SupplyRelationTypeEnum relationType;

    public SupplyGoodsVO(GoodsExtDTO goods) {
        BeanUtil.copyProperties(goods, this);
        this.goodsId = goods.getId();
        this.categoryId = goods.getGoodsCategoryId();
        this.relationType = SupplyRelationTypeEnum.no;
    }
}
