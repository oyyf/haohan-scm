package com.haohan.cloud.scm.api.wms.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.wms.req.CellReq;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseDetailReq;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @program: haohan-fresh-scm
 * @description: 仓储内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "wmsFeignService", value = ScmServiceName.SCM_BIZ_WMS)
public interface WmsFeignService {

  /**
   * 获取货位信息
   * @param cellSn 货位编号
   * @param from
   * @return
   */
  @GetMapping("/api/feign/cell/{cellSn}")
  R getByCellSn(@PathVariable("cellSn") String cellSn, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 获取货位信息列表
   * @param cellReq 货位请求参数
   * @param from
   * @return
   */
  @GetMapping("/api/feign/cell/fetchCellList")
  R fetchCellList(@RequestBody CellReq cellReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 获取入库单信息
   * @param enterWarehouseSn 入库单编号
   * @param from
   * @return
   */
  @GetMapping("/api/feign/enterwarehouse/{enterWarehouseSn}")
  R getByEnterWarehouseSn(@PathVariable("enterWarehouseSn") String enterWarehouseSn, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 获取入库单信息列表
   * @param enterWarehouseReq 入库请单求参数
   * @param from
   * @return
   */
  @PostMapping("/api/feign/enterwarehouse/fetchEnterWarehouseList")
  R fetchEnterWarehouseList(@RequestBody EnterWarehouseReq enterWarehouseReq, @RequestHeader(SecurityConstants.FROM) String from);


  /**
   * 获取入库单明细信息
   * @param enterWarehouseDetailSn 入库单编号
   * @param from
   * @return
   */
  @GetMapping("/api/feign/enterwarehousedetail/{enterWarehouseDetailSn}")
  R getByEnterWarehouseDetailSn(@PathVariable("enterWarehouseDetailSn") String enterWarehouseDetailSn, @RequestHeader(SecurityConstants.FROM) String from);

  /**
   * 获取入库单明细信息列表
   * @param enterWarehouseDetailReq 入库请单求参数
   * @param from
   * @return
   */
  @PostMapping("/api/feign/enterwarehousedetail/fetchEnterWarehouseDetailList")
  R fetchEnterWarehouseDetailList(@RequestBody EnterWarehouseDetailReq enterWarehouseDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


}
