package com.haohan.cloud.scm.api.salec.req;

import com.haohan.cloud.scm.api.salec.dto.COrderDetailDTO;
import lombok.Data;

import java.util.List;

/**
 * @author cx
 * @date 2019/7/13
 */
@Data
public class CreateBuyOrderDetailReq {

    private List<COrderDetailDTO> list;

    private String pmId;

    private String buyId;

    private String buyerId;

}
