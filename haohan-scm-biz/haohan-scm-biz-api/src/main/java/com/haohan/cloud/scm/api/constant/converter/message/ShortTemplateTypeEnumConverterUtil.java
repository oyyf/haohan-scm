package com.haohan.cloud.scm.api.constant.converter.message;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.message.ShortTemplateTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class ShortTemplateTypeEnumConverterUtil implements Converter<ShortTemplateTypeEnum> {
    @Override
    public ShortTemplateTypeEnum convert(Object o, ShortTemplateTypeEnum shortTemplateTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ShortTemplateTypeEnum.getByType(o.toString());
    }
}
