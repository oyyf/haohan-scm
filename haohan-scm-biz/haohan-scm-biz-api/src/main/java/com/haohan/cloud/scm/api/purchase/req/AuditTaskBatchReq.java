package com.haohan.cloud.scm.api.purchase.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author cx
 * @date 2019/6/10
 */
@Data
@ApiModel(description = "总监批量通过采购任务需求")
public class AuditTaskBatchReq {

  @NotBlank(message = "id不能为空")
  @ApiModelProperty(value = "采购任务ID" , required = true)
  private String id;

  @NotBlank(message = "uId不能为空")
  @ApiModelProperty(value ="发起人ID" , required = true)
  private String uId;

  @NotBlank(message = "pmId不能为空")
  @ApiModelProperty(value = "平台商家ID",required = true)
  private String pmId;

  @ApiModelProperty(value = "审批备注")
  private String actionDesc;
}
