package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.DeliveryGoodsStatusEnum;

/**
 * @author dy
 * @date 2019/12/10
 */
public class DeliveryGoodsStatusEnumConverterUtil implements Converter<DeliveryGoodsStatusEnum> {
    @Override
    public DeliveryGoodsStatusEnum convert(Object o, DeliveryGoodsStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DeliveryGoodsStatusEnum.getByType(o.toString());
    }

}