package com.haohan.cloud.scm.api.crm.vo;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.DataReportStatusEnum;
import com.haohan.cloud.scm.api.crm.entity.DataReport;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author dy
 * @date 2019/12/7
 */
@Data
@ApiModel("数据上报信息, 带图片列表、明细列表(带商品信息)")
@NoArgsConstructor
public class DataReportVO {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "上报编号")
    private String reportSn;

    @ApiModelProperty(value = "上报类型 0库存1销售记录")
    private DataReportEnum reportType;

    @ApiModelProperty(value = "客户编码")
    private String customerSn;

    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @ApiModelProperty(value = "图片组编号")
    private String photoGroupNum;

    @ApiModelProperty(value = "上报位置定位")
    private String reportLocation;

    @ApiModelProperty(value = "上报位置地址")
    private String reportAddress;

    @ApiModelProperty(value = "上报人id(员工)")
    private String reportManId;

    @ApiModelProperty(value = "上报人名称")
    private String reportMan;

    @ApiModelProperty(value = "上报日期/销售日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate reportDate;

    @ApiModelProperty(value = "上报人手机号")
    private String reportTelephone;

    @ApiModelProperty(value = "上报状态 0.待确认 1.已确认 2.不通过")
    private DataReportStatusEnum reportStatus;

    @ApiModelProperty(value = "操作人id")
    private String operatorId;

    @ApiModelProperty(value = "操作人")
    private String operatorName;

    @ApiModelProperty(value = "商品总数")
    private Integer goodsTotalNum;

    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    @ApiModelProperty(value = "商品合计金额")
    private BigDecimal sumAmount;

    @ApiModelProperty(value = "商品总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @ApiModelProperty(value = "图片列表")
    private List<PhotoGallery> photoList;

    @ApiModelProperty(value = "上报明细")
    private List<DataReportDetailVO> detailList;

    public DataReportVO(DataReport report) {
        BeanUtil.copyProperties(report, this);
    }
}
