package com.haohan.cloud.scm.api.constant.converter.manage;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;

/**
 * @author dy
 * @date 2019/9/14
 */
public class MerchantPdsTypeEnumConverterUtil implements Converter<MerchantPdsTypeEnum> {
    @Override
    public MerchantPdsTypeEnum convert(Object o, MerchantPdsTypeEnum merchantPdsTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MerchantPdsTypeEnum.getByType(o.toString());
    }
}