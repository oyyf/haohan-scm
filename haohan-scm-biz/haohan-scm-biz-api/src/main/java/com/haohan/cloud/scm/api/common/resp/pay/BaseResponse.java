package com.haohan.cloud.scm.api.common.resp.pay;

import com.haohan.cloud.framework.constant.RespStatus;
import com.haohan.cloud.framework.utils.JacksonUtils;
import org.apache.commons.lang3.StringUtils;

public class BaseResponse {

	private String action;
	private String responseCode;
	private String errorMsg;

	public BaseResponse() {
	}

	public BaseResponse(String responseCode, String errorMsg) {
		this.responseCode = responseCode;
		this.errorMsg = errorMsg;
	}

	public static BaseResponse error(String errorMsg){
		BaseResponse error =  new BaseResponse(RespStatus.ERROR.getCode().toString(),errorMsg);
		return error;

	}

	public boolean isSuccess(){

		return StringUtils.equals(XmBankRespStatus.SUCCESS.getResponseCode(),this.responseCode);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String toJson(){
		return JacksonUtils.toJson(this);
	}

}
