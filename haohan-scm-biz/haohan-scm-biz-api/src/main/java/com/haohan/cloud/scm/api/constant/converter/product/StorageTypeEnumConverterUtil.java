package com.haohan.cloud.scm.api.constant.converter.product;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.product.StorageTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class StorageTypeEnumConverterUtil implements Converter<StorageTypeEnum> {
    @Override
    public StorageTypeEnum convert(Object o, StorageTypeEnum storageTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return StorageTypeEnum.getByType(o.toString());
    }
}
