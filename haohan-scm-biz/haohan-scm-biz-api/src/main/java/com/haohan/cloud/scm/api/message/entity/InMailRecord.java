/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.InMailTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-13 18:34:31
 */
@Data
@TableName("scm_ms_in_mail_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "站内信记录表")
public class InMailRecord extends Model<InMailRecord> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 平台商家id (暂未使用)
     */
    private String pmId;
    /**
     * 站内信编号
     */
    private String inMailSn;
    /**
     * 标题(消息简述)
     */
    private String title;
    /**
     * 站内信内容
     */
    private String content;
    /**
     * 跳转链接  (暂未使用)
     */
    private String linkUrl;
    /**
     * 站内信类型:1公告2及时通信
     */
    private InMailTypeEnum inMailType;
    /**
     * 发送人uid
     */
    private String senderUid;
    /**
     * 发送人名称
     */
    private String senderName;
    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    private DepartmentTypeEnum departmentType;
    /**
     * 消息状态:1待发送2已发送3已查看  (暂未使用)
     */
    private String messageStatus;
    /**
     * 业务类型:各部门的消息
     */
    private MsgActionTypeEnum msgActionType;
    /**
     * 接收人uid  (暂未使用)
     */
    private String receiverUid;
    /**
     * 接收人名称  (暂未使用)
     */
    private String receiverName;
    /**
     * 发送时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sendTime;
    /**
     * 接收时间  (暂未使用)
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receiveTime;
    /**
     * 请求参数  (json 详情查询 编号)
     */
    private String reqParams;
    /**
     * 返回参数   (暂未使用)
     */
    private String respParams;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
