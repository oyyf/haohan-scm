package com.haohan.cloud.scm.api.constant.enums.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/8
 */
@Getter
@AllArgsConstructor
public enum GoodsTypeEnum implements IBaseEnum {

    /**
     * 商品类型 0实体商品，1虚拟商品 2.产品服务 3.竞品
     */
    physical("0", "实体商品"),
    virtual("1", "虚拟商品"),
    productServ("2", "产品服务"),
    competition("3", "竞品");

    private static final Map<String, GoodsTypeEnum> MAP = new HashMap<>(8);

    static {
        for (GoodsTypeEnum e : GoodsTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static GoodsTypeEnum getByType(String type) {
            return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
