/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.req;

import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-30 10:19:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "结算记录")
public class SettlementRecordReq extends SettlementRecord {

    private long pageSize;
    private long pageNo;




}
