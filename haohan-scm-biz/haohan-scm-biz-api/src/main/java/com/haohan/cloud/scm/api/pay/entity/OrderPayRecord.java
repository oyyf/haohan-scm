/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.pay.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * 支付订单
 *
 * @author haohan
 * @date 2019-07-25 21:31:10
 */
@Data
@TableName("scm_order_pay_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "支付订单")
public class OrderPayRecord extends Model<OrderPayRecord> {
  private static final long serialVersionUID = 1L;

  /**
   * 主键
   */
  @ApiModelProperty(value = "主键")
  @TableId(type = IdType.INPUT)
  private String id;
  /**
   * 商户ID
   */
  @ApiModelProperty(value = "商户ID")
  private String merchantId;
  /**
   * 商户名称
   */
  @ApiModelProperty(value = "商户名称")
  private String merchantName;
  /**
   * 店铺ID
   */
  @ApiModelProperty(value = "店铺ID")
  private String shopId;
  /**
   * 店铺名称
   */
  @ApiModelProperty(value = "店铺名称")
  private String shopName;
  /**
   * 商户订单编号，20个字符以内  自定义支付唯一单号（结算单号-n）
   */
  @ApiModelProperty(value = "商户订单编号，20个字符以内")
  private String orderId;

    @ApiModelProperty(value = "结算单编号")
  private String settlementSn;
  /**
   * 订单类型
   */
  @ApiModelProperty(value = "订单类型")
  private String orderType;
  /**
   * 用户请求IP
   */
  @ApiModelProperty(value = "用户请求IP")
  private String clientIp;
  /**
   * 支付流水号16位
   */
  @ApiModelProperty(value = "支付流水号16位")
  private String requestId;
  /**
   * 第三方平台订单id
   */
  @ApiModelProperty(value = "第三方平台订单id")
  private String transId;
  /**
   * 商户编号
   */
  @ApiModelProperty(value = "商户编号")
  private String partnerId;
  /**
   * 商品名称
   */
  @ApiModelProperty(value = "商品名称")
  private String goodsName;
  /**
   * 订单提交日期
   */
  @ApiModelProperty(value = "订单提交日期")
  private LocalDateTime orderTime;
  /**
   * 订单金额
   */
  @ApiModelProperty(value = "订单金额")
  private BigDecimal orderAmount;
  /**
   * 授权码
   */
  @ApiModelProperty(value = "授权码")
  private String authCode;
  /**
   * 支付渠道
   */
  @ApiModelProperty(value = "支付渠道")
  private String payChannel;
  /**
   * 支付方式
   */
  @ApiModelProperty(value = "支付方式")
  private String payType;
  /**
   * 是否支持信用卡0不支持，1支持
   */
  @ApiModelProperty(value = "是否支持信用卡0不支持，1支持")
  private Integer limitPay;
  /**
   * 返回码
   */
  @ApiModelProperty(value = "返回码")
  private String respCode;
  /**
   * 返回码信息描述
   */
  @ApiModelProperty(value = "返回码信息描述")
  private String respDesc;
  /**
   * 返回时间
   */
  @ApiModelProperty(value = "返回时间")
  private LocalDateTime respTime;
  /**
   * 商户订单二维码
   */
  @ApiModelProperty(value = "商户订单二维码")
  private String orderQrcode;
  /**
   * 用户标示openid
   */
  @ApiModelProperty(value = "用户标示openid")
  private String openid;
  /**
   * 微信公众帐号ID
   */
  @ApiModelProperty(value = "微信公众帐号ID")
  private String appid;
  /**
   * 预支付订单号
   */
  @ApiModelProperty(value = "预支付订单号")
  private String prepayId;
  /**
   * 签名
   */
  @ApiModelProperty(value = "签名")
  private String paySign;
  /**
   * 时间戳
   */
  @ApiModelProperty(value = "时间戳")
  private String timeStamp;
  /**
   * 随机字符串
   */
  @ApiModelProperty(value = "随机字符串")
  private String noncestr;
  /**
   * 支付状态
   */
  @ApiModelProperty(value = "支付状态")
  private String payStatus;
  /**
   * 订单信息
   */
  @ApiModelProperty(value = "订单信息")
  private String orderInfo;
  /**
   * 渠道商支付回调地址
   */
  @ApiModelProperty(value = "渠道商支付回调地址")
  private String partnerNotifyUrl;
  /**
   * 支付信息
   */
  @ApiModelProperty(value = "支付信息")
  private String payInfo;
  /**
   * 设备编号
   */
  @ApiModelProperty(value = "设备编号")
  private String deviceId;
  /**
   * 手续费
   */
  @ApiModelProperty(value = "手续费")
  private BigDecimal fee;
  /**
   * 费率
   */
  @ApiModelProperty(value = "费率")
  private BigDecimal rate;
  /**
   * 创建者
   */
  @ApiModelProperty(value = "创建者")
  @TableField(fill = FieldFill.INSERT)
  private String createBy;
  /**
   * 创建时间
   */
  @TableField(fill = FieldFill.INSERT)
  @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createDate;
  /**
   * 更新者
   */
  @ApiModelProperty(value = "更新者")
  @TableField(fill = FieldFill.UPDATE)
  private String updateBy;
  /**
   * 更新时间
   */
  @TableField(fill = FieldFill.UPDATE)
  @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime updateDate;
  /**
   * 备注信息
   */
  @ApiModelProperty(value = "备注信息")
  private String remarks;
  /**
   * 删除标记
   */
  @ApiModelProperty(value = "删除标记")
  @TableField(fill = FieldFill.INSERT)
  private String delFlag;
  /**
   * 交易用户id
   */
  @ApiModelProperty(value = "交易用户id")
  private String buyerId;
  /**
   * 订单明细
   */
  @ApiModelProperty(value = "订单明细")
  private String orderDetail;
  /**
   * 渠道编号
   */
  @ApiModelProperty(value = "渠道编号")
  private String partnerNum;
  /**
   * 租户id
   */
  @ApiModelProperty(value = "租户id")
  private Integer tenantId;

}
