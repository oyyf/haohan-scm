package com.haohan.cloud.scm.api.saleb.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/28
 */
@Data
public class BuyOrderDetailDTO {

    // 明细属性

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 汇总单号
     */
    private String summaryBuyId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购明细编号
     */
    private String buyDetailSn;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 商品规格ID
     */
    private String goodsModelId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品规格 名称
     */
    private String goodsModel;
    /**
     * 采购数量
     */
    private BigDecimal goodsNum;
    /**
     * 市场价格
     */
    private BigDecimal marketPrice;
    /**
     * 采购价格
     */
    private BigDecimal buyPrice;
    /**
     * 单位
     */
    private String unit;
    /**
     * 状态
     */
    private String status;
    /**
     * 零售单明细id
     */
    private String goodsOrderDetailId;
    /**
     * 商品下单数量
     */
    private BigDecimal orderGoodsNum;
    /**
     * 汇总状态:0未处理1已汇总2已备货
     */
    private String summaryFlag;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 租户id
     */
    private Integer tenantId;

    // 采购单 属性

    /**
     * 采购用户
     */
    private String buyerUid;
    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;
    /**
     * 送货批次
     */
    private String buySeq;

    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 配送方式
     */
    private String deliveryType;
    /**
     * 零售单号
     */
    private String goodsOrderId;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String telephone;
    /**
     * 配送地址
     */
    private String address;

    // 添加属性

    /**
     * 商品id  feign查询接口返回值无此项
     */
    private String goodsId;
}
