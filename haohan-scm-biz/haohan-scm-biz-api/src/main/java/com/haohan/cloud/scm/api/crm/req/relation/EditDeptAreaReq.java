package com.haohan.cloud.scm.api.crm.req.relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

/**
 * @author dy
 * @date 2019/12/23
 */
@Data
public class EditDeptAreaReq {

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "区域编号列表")
    private Set<String> areaSnList;

}
