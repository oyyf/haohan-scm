package com.haohan.cloud.scm.api.opc.trans;

import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.dto.BillPayment;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;
import lombok.Data;

/**
 * @author dy
 * @date 2019/9/19
 */
@Data
public class SettlementTrans {

    private SettlementTrans() {
    }

    public static SettlementRelation initRelation(SettlementRecord settlementRecord, BillPayment billPayment) {
        SettlementRelation relation = new SettlementRelation();
        relation.setSettlementId(settlementRecord.getId());
        relation.setSettlementSn(settlementRecord.getSettlementSn());
        relation.setPaymentId(billPayment.getId());
        relation.setPaymentSn(billPayment.getBillSn());

        relation.setSettlementType(fetchType(billPayment.getBillType()));
        return relation;

    }

    /**
     * 根据账单类型 返回结算类型
     *
     * @param billType
     * @return
     */
    public static SettlementTypeEnum fetchType(BillTypeEnum billType) {
        if (billType == null) {
            return null;
        }
        if (billType == BillTypeEnum.order || billType == BillTypeEnum.offerBack) {
            return SettlementTypeEnum.receivable;
        }
        return SettlementTypeEnum.payable;
    }

    /**
     * 完成结算设置
     *
     * @param id               主键
     * @param settlementRecord 必需:settlementId /settlementAmount / payType/
     *                         / payDate / GroupNum/companyOperator/operator
     *                         可选:settlementDesc
     * @return
     */
    public static SettlementRecord completeSettlementTrans(String id, SettlementRecord settlementRecord) {
        SettlementRecord result = new SettlementRecord();
        result.setId(id);
        result.setSettlementAmount(settlementRecord.getSettlementAmount());
        result.setPayType(settlementRecord.getPayType());
        result.setPayDate(settlementRecord.getPayDate());
        result.setGroupNum(settlementRecord.getGroupNum());
        result.setCompanyOperator(settlementRecord.getCompanyOperator());
        result.setOperator(settlementRecord.getOperator());
        result.setSettlementDesc(settlementRecord.getSettlementDesc());
        result.setStatus(YesNoEnum.yes);
        return result;
    }

}
