package com.haohan.cloud.scm.api.goods.dto;

import com.haohan.cloud.scm.api.goods.entity.GoodsModelTotal;
import com.haohan.cloud.scm.api.goods.vo.GoodsModelVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author cx
 * @date 2019/6/21
 * salec使用
 */
@Data
@ApiModel(description = "商品信息")
public class GoodsInfoDTO {

    @ApiModelProperty(value = "商品Id")
    private Integer id;

    @ApiModelProperty(value = "商户id")
    private Integer merId;

    @ApiModelProperty(value = "店铺id")
    private String shopId;

    @ApiModelProperty(value = "商品图片地址")
    private String image;


    @ApiModelProperty(value = "轮播图")
    private String sliderImage;

    @ApiModelProperty(value = "商品名称")
    private String storeName;

    @ApiModelProperty(value = "商品简介")
    private String storeInfo;

    @ApiModelProperty(value = "商品描述")
    private String description;

    @ApiModelProperty(value = "分类Id")
    private String cateId;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal price;

    @ApiModelProperty(value = "会员价格")
    private BigDecimal vipPrice;

    @ApiModelProperty(value = "市场价格")
    private BigDecimal otPrice;

    @ApiModelProperty(value = "单位名称")
    private String unitName;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "添加时间")
    private Integer addTime;

    @ApiModelProperty(value = "成本价")
    private BigDecimal cost;

    @ApiModelProperty(value = "品牌id")
    private Integer brand;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

    @ApiModelProperty(value="是否上架")
    private Integer isDel;

    @ApiModelProperty(value = "规格信息")
    List<GoodsModelVO> modelInfo;

    @ApiModelProperty(value = "商品规格名称列表")
    List<GoodsModelTotal> goodsModelTotals;


}
