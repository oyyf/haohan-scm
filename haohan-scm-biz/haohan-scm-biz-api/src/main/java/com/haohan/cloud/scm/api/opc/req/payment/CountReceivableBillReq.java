package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/9/20
 */
@Data
public class CountReceivableBillReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    @NotBlank(message = "buyerId不能为空")
    private String buyerId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "endDate不能为空")
    private LocalDate endDate;

    private YesNoEnum type;

}
