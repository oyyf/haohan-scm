/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.manage.entity.UnionBankNo;
import com.haohan.cloud.scm.api.manage.req.UnionBankNoReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 联行号内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "UnionBankNoFeignService", value = ScmServiceName.SCM_MANAGE)
public interface UnionBankNoFeignService {


    /**
     * 通过id查询联行号
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/UnionBankNo/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 联行号 列表信息
     * @param unionBankNoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/UnionBankNo/fetchUnionBankNoPage")
    R getUnionBankNoPage(@RequestBody UnionBankNoReq unionBankNoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 联行号 列表信息
     * @param unionBankNoReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/UnionBankNo/fetchUnionBankNoList")
    R getUnionBankNoList(@RequestBody UnionBankNoReq unionBankNoReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/add")
    R save(@RequestBody UnionBankNo unionBankNo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改联行号
     * @param unionBankNo 联行号
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/update")
    R updateById(@RequestBody UnionBankNo unionBankNo, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除联行号
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/UnionBankNo/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/countByUnionBankNoReq")
    R countByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param unionBankNoReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/getOneByUnionBankNoReq")
    R getOneByUnionBankNoReq(@RequestBody UnionBankNoReq unionBankNoReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param unionBankNoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/UnionBankNo/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<UnionBankNo> unionBankNoList, @RequestHeader(SecurityConstants.FROM) String from);


}
