/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.SalesReportAnalysis;
import com.haohan.cloud.scm.api.crm.req.SalesReportAnalysisReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户销量上报数据统计内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "SalesReportAnalysisFeignService", value = ScmServiceName.SCM_CRM)
public interface SalesReportAnalysisFeignService {


    /**
     * 通过id查询客户销量上报数据统计
     *
     * @param id   id
     * @param from
     * @return R
     */
    @RequestMapping(value = "/api/feign/SalesReportAnalysis/{id}", method = RequestMethod.GET)
    R<SalesReportAnalysis> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户销量上报数据统计 列表信息
     *
     * @param salesReportAnalysisReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SalesReportAnalysis/fetchSalesReportAnalysisPage")
    R<Page<SalesReportAnalysis>> getSalesReportAnalysisPage(@RequestBody SalesReportAnalysisReq salesReportAnalysisReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户销量上报数据统计 列表信息
     *
     * @param salesReportAnalysisReq 请求对象
     * @param from
     * @return
     */
    @PostMapping("/api/feign/SalesReportAnalysis/fetchSalesReportAnalysisList")
    R<List<SalesReportAnalysis>> getSalesReportAnalysisList(@RequestBody SalesReportAnalysisReq salesReportAnalysisReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户销量上报数据统计
     *
     * @param salesReportAnalysis 客户销量上报数据统计
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/add")
    R<Boolean> save(@RequestBody SalesReportAnalysis salesReportAnalysis, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户销量上报数据统计
     *
     * @param salesReportAnalysis 客户销量上报数据统计
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/update")
    R<Boolean> updateById(@RequestBody SalesReportAnalysis salesReportAnalysis, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户销量上报数据统计
     *
     * @param id   id
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/listByIds")
    R<List<SalesReportAnalysis>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param salesReportAnalysisReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/countBySalesReportAnalysisReq")
    R<Integer> countBySalesReportAnalysisReq(@RequestBody SalesReportAnalysisReq salesReportAnalysisReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param salesReportAnalysisReq 实体对象,可以为空
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/getOneBySalesReportAnalysisReq")
    R<SalesReportAnalysis> getOneBySalesReportAnalysisReq(@RequestBody SalesReportAnalysisReq salesReportAnalysisReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param salesReportAnalysisList 实体对象集合 大小不超过1000条数据
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/SalesReportAnalysis/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<SalesReportAnalysis> salesReportAnalysisList, @RequestHeader(SecurityConstants.FROM) String from);


}
