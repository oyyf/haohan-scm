package com.haohan.cloud.scm.api.salec.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cx
 * @date 2019/7/14
 */
@Data
public class ProductInfoDTO {
    private Integer id;
    private String image;
    private BigDecimal price;
    private BigDecimal ot_price;
    private BigDecimal vip_price;
    private BigDecimal postage;
    private String cate_id;
    private String store_name;
    private String unit_name;
    /**
     * 规格信息
     */
    private AttrInfoDTO attrInfo;
}
