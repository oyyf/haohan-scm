package com.haohan.cloud.scm.api.purchase.trans;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.GradeTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import com.haohan.cloud.scm.api.purchase.req.BuyerAddSupplierReq;
import com.haohan.cloud.scm.api.supply.entity.Supplier;

/**
 * @author xwx
 * @date 2019/6/18
 */
public class AddSupplierTrans {

    public static Supplier trans(BuyerAddSupplierReq req){
        Supplier supplier = new Supplier();
        supplier.setPmId(req.getPmId());
        supplier.setSupplierName(req.getSupplierName());
        supplier.setContact(req.getContact());
        supplier.setTelephone(req.getTelephone());
        supplier.setStatus(UseStatusEnum.stayAudit);
        supplier.setSupplierType(PdsSupplierTypeEnum.ordinary);
        supplier.setNeedPush(YesNoEnum.no);
        supplier.setSupplierLevel(GradeTypeEnum.threeRank);

        return supplier;
    }
}
