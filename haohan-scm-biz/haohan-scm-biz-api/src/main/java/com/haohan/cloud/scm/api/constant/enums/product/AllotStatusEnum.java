package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum AllotStatusEnum {

    not_accept("1", "待确认"),
    accept("2", "已确认"),
    adjust("3", "拨调中"),
    receipt("4", "已收货"),
    cancel("5", "取消");

    private static final Map<String, AllotStatusEnum> MAP = new HashMap<>(8);

    static {
        for (AllotStatusEnum e : AllotStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static AllotStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
