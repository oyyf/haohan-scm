/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

/**
 * 客户地址
 *
 * @author haohan
 * @date 2019-09-04 18:31:18
 */
@Data
@TableName("crm_customer_address")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户地址")
public class CustomerAddress extends Model<CustomerAddress> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @NotBlank(message = "客户编号不能为空")
    @Length(max = 32, message = "客户编号的字符长度必须小必须在1至32之间")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;
    /**
     * 联系人id
     */
    @Length(min = 0, max = 32, message = "联系人id的字符长度必须在0至32之间")
    @ApiModelProperty(value = "联系人id")
    private String linkmanId;

    @NotBlank(message = "联系人名称不能为空")
    @Length(min = 0, max = 20, message = "联系人名称的字符长度必须在1至20之间")
    @ApiModelProperty(value = "联系人名称")
    private String linkman;

    @Length(min = 0, max = 20, message = "固定电话的字符长度必须在0至20之间")
    @ApiModelProperty(value = "固定电话")
    private String phone;

    @NotBlank(message = "省不能为空")
    @Length(min = 0, max = 20, message = "省的字符长度必须在1至20之间")
    @ApiModelProperty(value = "省")
    private String province;

    @NotBlank(message = "市不能为空")
    @Length(min = 0, max = 20, message = "市的字符长度必须在1至20之间")
    @ApiModelProperty(value = "市")
    private String city;

    @Length(min = 0, max = 20, message = "区的字符长度必须在1至20之间")
    @ApiModelProperty(value = "区")
    private String district;

    @Length(min = 0, max = 20, message = "街道、乡镇的字符长度必须在0至20之间")
    @ApiModelProperty(value = "街道、乡镇")
    private String street;

    @NotBlank(message = "详细地址不能为空")
    @Length(min = 0, max = 64, message = "详细地址的字符长度必须在1至64之间")
    @ApiModelProperty(value = "送货地址")
    private String address;

    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    private String position;

    @NotBlank(message = "联系人手机不能为空")
    @Length(min = 8, max = 15, message = "联系人手机的字符长度必须在8至15之间")
    @ApiModelProperty(value = "联系电话")
    private String telephone;
    /**
     * 是否默认
     */
    @ApiModelProperty(value = "是否默认")
    private YesNoEnum defaultFlag;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @Length(min = 0, max = 255, message = "备注信息的字符长度必须在0至255之间")
    @ApiModelProperty(value = "备注信息")
    private String remarks;
    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
