package com.haohan.cloud.scm.api.crm.req.bill;

import com.haohan.cloud.scm.api.bill.req.BillManageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author dy
 * @date 2019/12/31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CrmBillReviewReq extends BillManageReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

}
