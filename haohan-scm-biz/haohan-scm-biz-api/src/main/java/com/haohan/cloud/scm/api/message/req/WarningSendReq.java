/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.message.req;

import com.haohan.cloud.scm.api.message.entity.WarningSend;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 预警发送记录表
 *
 * @author haohan
 * @date 2019-05-28 20:05:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "预警发送记录表")
public class WarningSendReq extends WarningSend {

    private long pageSize;
    private long pageNo;




}
