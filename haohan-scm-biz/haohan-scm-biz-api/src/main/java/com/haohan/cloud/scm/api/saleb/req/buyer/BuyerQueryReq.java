package com.haohan.cloud.scm.api.saleb.req.buyer;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyerTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.PayPeriodEnum;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/5/18
 */
@Data
public class BuyerQueryReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(min = 0, max = 64, message = "商家ID长度必须介于 0 和 64 之间")
    @ApiModelProperty(value = "商家ID不能为空")
    private String merchantId;

    @ApiModelProperty(value = "账期")
    private PayPeriodEnum payPeriod;

    @Length(min = 0, max = 2, message = "账期日长度必须介于 0 和 2 之间")
    private String payDay;

    @ApiModelProperty(value = "启用状态")
    private UseStatusEnum status;

    @ApiModelProperty(value = "采购商类型")
    private BuyerTypeEnum buyerType;

    @ApiModelProperty(value = "是否开启消息推送")
    private YesNoEnum needPush;

    @Length(min = 0, max = 5, message = "排序值长度必须介于 0 和 5 之间")
    private String sort;

    @ApiModelProperty(value = "是否限制下单")
    private YesNoEnum needConfirmation;

    @ApiModelProperty(value = "是否需下单支付")
    private YesNoEnum needPay;

    // 非eq

    @Length(min = 0, max = 64, message = "商家名称长度必须介于 0 和 64 之间")
    private String merchantName;

    @Length(min = 0, max = 64, message = "采购商全称长度必须介于 0 和 64 之间")
    private String buyerName;

    @Length(min = 0, max = 64, message = "采购商简称长度必须介于 0 和 64 之间")
    private String shortName;

    @Length(min = 0, max = 64, message = "联系人长度必须介于 0 和 64 之间")
    private String contact;

    @Length(min = 0, max = 15, message = "电话长度必须介于 0 和 15 之间")
    private String telephone;

    @Length(min = 0, max = 64, message = "采购商地址长度必须介于 0 和 64 之间")
    private String address;

    @ApiModelProperty(value = "地址定位区域")
    private String area;

    public Buyer transTo() {
        Buyer buyer = new Buyer();
        buyer.setMerchantId(this.merchantId);
        buyer.setPayPeriod(this.payPeriod);
        buyer.setPayDay(this.payDay);
        buyer.setStatus(this.status);
        buyer.setBuyerType(this.buyerType);
        buyer.setNeedPush(this.needPush);
        buyer.setSort(StrUtil.isEmpty(this.sort) ? null : this.sort);
        buyer.setNeedConfirmation(this.needConfirmation);
        buyer.setNeedPay(this.needPay);
        return buyer;
    }


}
