package com.haohan.cloud.scm.api.supply.req;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsSupplierTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author xwx
 * @date 2019/6/12
 */
@Data
@ApiModel(description = "新增供应商")
public class AddSupplierReq {

    /**
     * 平台商家ID
     */
    @NotBlank(message = "pmId不能为空")
    private String pmId;
    /**
     * 全称
     */
    @NotBlank(message = "supplierName不能为空")
    private String supplierName;
    /**
     * 联系电话
     */
    @NotBlank(message = "telephone不能为空")
    @Length(min = 0, max = 11, message = "telephone长度在0-11个字符")
    private String telephone;
    /**
     * 通行证ID
     */
    private String passportId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 供应商名称
     */
    private String shortName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 微信
     */
    private String wechatId;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 账期
     */
    private String payPeriod;
    /**
     * 供应商地址
     */
    private String address;
    /**
     * 标签
     */
    private String tags;
    /**
     * 是否启用
     */
    private UseStatusEnum status;
    /**
     * 供应商类型
     */
    private PdsSupplierTypeEnum supplierType;
    /**
     * 是否开启消息推送
     */
    private YesNoEnum needPush;
}
