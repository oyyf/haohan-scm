package com.haohan.cloud.scm.api.crm.req.app;

import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/1/14
 * SingleGroup  查询详情
 */
@Data
public class CrmMsgQueryReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "消息编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "消息编号最大长度32字符")
    @ApiModelProperty(value = "消息编号")
    private String messageSn;

    @ApiModelProperty(value = "查询发出日期-开始日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询发出日期-结束日期", notes = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty(value = "业务类型:0.系统通知 51.日报 52.销售订单 53.销量上报 54.库存上报 55.竞品上报")
    private MsgActionTypeEnum msgActionType;

    @Length(max = 32, message = "消息标题最大长度32字符")
    @ApiModelProperty(value = "消息标题")
    private String title;

}
