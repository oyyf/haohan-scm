/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品规格
 *
 * @author haohan
 * @date 2019-05-13 18:46:34
 */
@Data
@TableName("scm_cms_goods_model")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品规格")
public class GoodsModel extends Model<GoodsModel> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 规格价格
     */
    private BigDecimal modelPrice;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 规格单位
     */
    private String modelUnit;
    /**
     * 规格库存
     */
    private BigDecimal modelStorage;
    /**
     * 规格商品图片地址
     */
    private String modelUrl;
    /**
     * 扩展信息 (未使用, 原定为 规格类型及名称 如:"种类:普通,尺寸:大")
     */
    private String modelInfo;
    /**
     * 扫码购编码
     */
    private String modelCode;
    /**
     * 规格组合  (原系统使用, 用于映射规格类型和名称 如:"30,32")
     */
    private String model;
    /**
     * 即速应用规格ID
     */
    private String itemsId;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 商品规格唯一编号
     */
    private String goodsModelSn;
    /**
     * 商品规格通用编号/公共商品库通用编号
     */
    private String modelGeneralSn;
    /**
     * 第三方规格编号/即速商品id
     */
    private String thirdModelSn;
    /**
     * 参考成本价
     */
    private BigDecimal costPrice;
    /**
     * 重量
     */
    private BigDecimal weight;
    /**
     * 体积
     */
    private BigDecimal volume;
    /**
     * 库存预警值
     */
    @ApiModelProperty(value = "库存预警值")
    private Integer stocksForewarn;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
