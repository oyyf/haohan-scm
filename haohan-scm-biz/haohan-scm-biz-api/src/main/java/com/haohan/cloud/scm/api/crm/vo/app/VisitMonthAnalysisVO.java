package com.haohan.cloud.scm.api.crm.vo.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/28
 */
@Data
public class VisitMonthAnalysisVO {

    @ApiModelProperty(value = "客户编号")
    @Length(min = 0, max = 32, message = "客户编号长度在0至32之间")
    private String customerSn;

    @ApiModelProperty(value = "查询开始日期")
    @DateTimeFormat(pattern = "yyyy-MM")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期")
    @DateTimeFormat(pattern = "yyyy-MM")
    private LocalDate endDate;

    @ApiModelProperty("分析结果列表")
    private List<VisitAnalysisVO> analysisList;


}
