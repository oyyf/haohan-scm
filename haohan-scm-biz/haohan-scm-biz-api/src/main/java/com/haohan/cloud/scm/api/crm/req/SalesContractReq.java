/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.SalesContract;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 销售合同
 *
 * @author haohan
 * @date 2019-08-30 11:46:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "销售合同")
public class SalesContractReq extends SalesContract {

    private long pageSize;
    private long pageNo;


}
