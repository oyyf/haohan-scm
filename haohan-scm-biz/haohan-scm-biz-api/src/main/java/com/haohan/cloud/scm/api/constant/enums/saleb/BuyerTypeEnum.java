package com.haohan.cloud.scm.api.constant.enums.saleb;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/6/15
 */
@Getter
@AllArgsConstructor
public enum BuyerTypeEnum implements IBaseEnum {

    /**
     * 采购商 类型:1员工2老板3运营4自营
     */
    employee("1", "员工"),
    boss("2", "老板"),
    operator("3", "运营"),
    self("4", "自营");

    private static final Map<String, BuyerTypeEnum> MAP = new HashMap<>(8);

    static {
        for (BuyerTypeEnum e : BuyerTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BuyerTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
