package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/8/5
 */
@Data
@ApiModel(description = "批量创建应付账单")
public class CreatePayableBatchReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotNull(message = "deliveryDate不能为空")
    @ApiModelProperty(value = "送货日期", required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "送货批次", required = true)
    private BuySeqEnum buySeq;

    public SupplyOrder transTo() {
        SupplyOrder supplyOrder = new SupplyOrder();
        supplyOrder.setSupplyDate(this.deliveryDate);
        return supplyOrder;
    }
}
