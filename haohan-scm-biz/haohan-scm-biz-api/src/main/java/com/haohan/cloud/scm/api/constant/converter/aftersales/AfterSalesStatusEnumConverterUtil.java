package com.haohan.cloud.scm.api.constant.converter.aftersales;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterSalesStatusEnum;

/**
 * @author xwx
 * @date 2019/6/4
 */
public class AfterSalesStatusEnumConverterUtil implements Converter<AfterSalesStatusEnum> {
    @Override
    public AfterSalesStatusEnum convert(Object o, AfterSalesStatusEnum afterSalesStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return AfterSalesStatusEnum.getByType(o.toString());
    }
}
