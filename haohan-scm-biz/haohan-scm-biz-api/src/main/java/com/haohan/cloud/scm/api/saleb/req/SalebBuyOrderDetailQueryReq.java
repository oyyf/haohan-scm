package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
public class SalebBuyOrderDetailQueryReq {
    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购明细编号
     */
    private String buyDetailSn;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 商品规格ID
     */
    private String goodsModelId;
    /**
     * 状态
     */
    private String status;
    /**
     * 零售单明细id
     */
    private String goodsOrderDetailId;
    /**
     * 汇总状态:0未处理1已汇总2已备货
     */
    private String summaryFlag;
    /**
     * 送货批次
     */
    private String buySeq;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryTime;

    public SalebBuyOrderDetailQueryReq copyFromSummaryOrder(SummaryOrder summaryOrder){
        this.setPmId(summaryOrder.getPmId());
        this.setBuySeq(summaryOrder.getBuySeq().getType());
        this.setGoodsModelId(summaryOrder.getGoodsModelId());
        this.setDeliveryTime(summaryOrder.getDeliveryTime());
        return this;
    }

}
