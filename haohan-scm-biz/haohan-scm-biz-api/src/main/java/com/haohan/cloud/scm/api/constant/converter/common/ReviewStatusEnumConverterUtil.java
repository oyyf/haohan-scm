package com.haohan.cloud.scm.api.constant.converter.common;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;

/**
 * @author dy
 * @date 2019/8/5
 */
public class ReviewStatusEnumConverterUtil implements Converter<ReviewStatusEnum> {
    @Override
    public ReviewStatusEnum convert(Object o, ReviewStatusEnum reviewStatusEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return ReviewStatusEnum.getByType(o.toString());
    }

}