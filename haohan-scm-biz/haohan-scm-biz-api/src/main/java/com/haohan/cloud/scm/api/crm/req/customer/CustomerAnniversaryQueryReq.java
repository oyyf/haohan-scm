package com.haohan.cloud.scm.api.crm.req.customer;

import com.haohan.cloud.scm.api.constant.enums.market.AnniversaryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.market.CalendarTypeEnum;
import com.haohan.cloud.scm.api.crm.entity.CustomerAnniversary;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @author dy
 * @date 2019/11/30
 */
@Data
@ApiModel(value = "客户纪念查询")
public class CustomerAnniversaryQueryReq {

    @Length(max = 32, message = "主题内容的长度最大32字符")
    @ApiModelProperty(value = "主题内容")
    private String content;

    @Length(max = 32, message = "客户编号的长度最大32字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @Length(max = 32, message = "客户名称的长度最大32字符")
    @ApiModelProperty(value = "客户名称")
    private String customerName;

    @Length(max = 32, message = "客户联系人id的长度最大32字符")
    @ApiModelProperty(value = "客户联系人id")
    private String linkmanId;

    @Length(max = 32, message = "客户联系人名称的长度最大32字符")
    @ApiModelProperty(value = "客户联系人名称")
    private String linkmanName;

    @ApiModelProperty(value = "纪念日类型:1.公司2.联系人")
    private AnniversaryTypeEnum anniversaryType;

    @ApiModelProperty(value = "日历类型:1.公历2.农历")
    private CalendarTypeEnum calendarType;

    @Length(max = 32, message = "市场负责人id的长度最大32字符")
    @ApiModelProperty(value = "市场负责人id")
    private String directorId;

    @Length(max = 32, message = "市场负责人名称的长度最大32字符")
    @ApiModelProperty(value = "市场负责人名称")
    private String directorName;

    @ApiModelProperty(value = "纪念日日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "纪念日日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    public CustomerAnniversary transTo() {
        CustomerAnniversary anniversary = new CustomerAnniversary();
        // 只要eq属性
        anniversary.setCustomerSn(this.customerSn);
        anniversary.setLinkmanId(this.linkmanId);
        anniversary.setAnniversaryType(this.anniversaryType);
        anniversary.setCalendarType(this.calendarType);
        anniversary.setDirectorId(this.directorId);
        return anniversary;
    }
}
