package com.haohan.cloud.scm.api.purchase.trans;


import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.TaskActionTypeEnum;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskDetailResp;
import lombok.Data;

/**
 * @author cx
 * @date 2019/6/6
 */
@Data
public class PurchaseTaskDetailTrans {

  public static PurchaseTaskDetailResp getDetail(PurchaseTask task, PurchaseOrderDetail detail){
    PurchaseTaskDetailResp resp = new PurchaseTaskDetailResp();
    resp.setId(task.getId());
    resp.setPmId(task.getPmId());
    resp.setMethodType(detail.getMethodType());
    resp.setGoodsCategoryName(detail.getGoodsCategoryName());
    resp.setGoodsName(detail.getGoodsName());
    resp.setNeedBuyNum(detail.getNeedBuyNum());
    resp.setGoodsModelName(detail.getModelName());
    resp.setGoodsImg(detail.getGoodsImg());
    resp.setBuyPrice(detail.getBuyPrice());
    resp.setPurchaseStatus(task.getTaskStatus());
    resp.setActionTime(task.getActionTime());
    resp.setDeadlineTime(task.getDeadlineTime());
    return resp;
  }

  public static PurchaseTask purchaseTaskTrans(PurchaseTask task, PurchaseEmployee employee){
    PurchaseTask newTask = new PurchaseTask();
    newTask.setTransactorId(employee.getId());
    newTask.setTransactorName(employee.getName());
    newTask.setPurchaseSn(task.getPurchaseSn());
    newTask.setPurchaseDetailSn(task.getPurchaseDetailSn());
    newTask.setPmId(task.getPmId());
    newTask.setTaskStatus(TaskStatusEnum.wait);
//    newTask.setInitiatorId(employee.getUserId());
//    newTask.setInitiatorName(employee.getName());
    newTask.setTaskActionType(TaskActionTypeEnum.procurement);
    newTask.setDeadlineTime(task.getDeadlineTime());
    return  newTask;
  }


}
