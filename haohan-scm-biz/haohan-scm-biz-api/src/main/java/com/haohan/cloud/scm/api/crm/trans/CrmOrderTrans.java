package com.haohan.cloud.scm.api.crm.trans;

import com.haohan.cloud.scm.api.bill.dto.OrderDetailDTO;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import com.haohan.cloud.scm.api.crm.dto.CustomerMerchantDTO;
import com.haohan.cloud.scm.api.crm.dto.analysis.SalesOrderAnalysisDTO;
import com.haohan.cloud.scm.api.crm.entity.SalesOrder;
import com.haohan.cloud.scm.api.crm.entity.SalesOrderDetail;
import com.haohan.cloud.scm.api.crm.req.order.SalesDetailAddReq;
import com.haohan.cloud.scm.api.crm.vo.analysis.CustomerSalesVO;
import com.haohan.cloud.scm.api.crm.vo.analysis.EmployeeSalesVO;
import com.haohan.cloud.scm.api.crm.vo.analysis.GoodsSalesVO;
import com.haohan.cloud.scm.api.crm.vo.analysis.SalesAnalysisVO;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2019/10/18
 */
@UtilityClass
public class CrmOrderTrans {

    /**
     * 初始化销售明细
     *
     * @param detailReq
     * @param model
     * @return
     */
    public SalesOrderDetail initOrderDetail(SalesDetailAddReq detailReq, GoodsModelDTO model) {
        SalesOrderDetail detail = new SalesOrderDetail();
        detail.setGoodsNum(detailReq.getGoodsNum());
        detail.setSalesGoodsType(detailReq.getSalesGoodsType());
        detail.setRemarks(detailReq.getRemarks());

        detail.setGoodsModelId(model.getId());
        detail.setGoodsModelSn(model.getGoodsModelSn());
        detail.setGoodsImg(model.getModelUrl());
        detail.setGoodsName(model.getGoodsName());
        detail.setModelName(model.getModelName());
        detail.setMarketPrice(model.getModelPrice());
        detail.setUnit(model.getModelUnit());
        return detail;
    }

    public static SalesAnalysisVO transSalesAnalysis(SalesOrderAnalysisDTO sales) {
        SalesAnalysisVO result = new SalesAnalysisVO();
        result.setOrderNum(sales.getOrderNum());
        result.setOrderAmount(sales.getSalesAmount());
        result.setStartDate(sales.getStartDate());
        result.setEndDate(sales.getEndDate());
        return result;
    }

    public static CustomerSalesVO transCustomerSales(SalesOrderAnalysisDTO entity) {
        CustomerSalesVO customerSales = new CustomerSalesVO();
        customerSales.setCustomerSn(entity.getCustomerSn());
        customerSales.setCustomerName(entity.getCustomerName());
        customerSales.setOrderNum(entity.getOrderNum());
        customerSales.setOrderAmount(entity.getSalesAmount());
        return customerSales;
    }

    public static EmployeeSalesVO transEmployeeSales(SalesOrderAnalysisDTO entity) {
        EmployeeSalesVO employeeSales = new EmployeeSalesVO();
        employeeSales.setEmployeeName(entity.getEmployeeName());
        employeeSales.setOrderNum(entity.getOrderNum());
        employeeSales.setOrderAmount(entity.getSalesAmount());
        return employeeSales;
    }

    public static GoodsSalesVO transGoodsSales(SalesOrderAnalysisDTO entity) {
        GoodsSalesVO goodsSales = new GoodsSalesVO();
        goodsSales.setGoodsModelSn(entity.getGoodsModelSn());
        goodsSales.setGoodsName(entity.getGoodsName());
        goodsSales.setModelName(entity.getModelName());
        goodsSales.setGoodsUnit(entity.getUnit());
        goodsSales.setOrderNum(entity.getOrderNum());
        goodsSales.setOrderAmount(entity.getSalesAmount());
        goodsSales.setGoodsNum(entity.getGoodsNum());
        goodsSales.setSalesGoodsType(entity.getSalesGoodsType());
        return goodsSales;
    }

    public static OrderInfoDTO transToOrderInfo(SalesOrder order, List<SalesOrderDetail> detailList, CustomerMerchantDTO customerMerchantInfo) {
        if (null == order || null == customerMerchantInfo) {
            throw new ErrorDataException("转换需要订单及商家信息");
        }
        OrderInfoDTO orderInfo = new OrderInfoDTO();
        orderInfo.setOrderSn(order.getSalesOrderSn());
        orderInfo.setOrderType(OrderTypeEnum.sales);
        orderInfo.setPayStatus(null == order.getPayStatus() ? PayStatusEnum.wait : order.getPayStatus());
        // 订单状态 暂只设置 取消和已下单
        orderInfo.setOrderStatus(order.getReviewStatus() == ReviewStatusEnum.failed ? OrderStatusEnum.cancel : OrderStatusEnum.submit);
        orderInfo.setCustomerId(order.getCustomerSn());
        orderInfo.setCustomerName(order.getCustomerName());
        orderInfo.setContact(order.getLinkmanName());
        orderInfo.setTelephone(order.getTelephone());
        orderInfo.setAddress(order.getAddress());
        orderInfo.setDealDate(order.getDeliveryDate());
        orderInfo.setOrderTime(order.getOrderTime());
        orderInfo.setTotalAmount(order.getTotalAmount());
        orderInfo.setSumAmount(order.getSumAmount());
        orderInfo.setOtherAmount(order.getOtherAmount());
        orderInfo.setGoodsNum(order.getGoodsNum());
        orderInfo.setRemarks(order.getRemarks());
        //  pmId、pmName、merchantId、merchantName
        orderInfo.setPmId(customerMerchantInfo.getPmId());
        orderInfo.setPmName(customerMerchantInfo.getPmName());
        orderInfo.setMerchantId(customerMerchantInfo.getMerchantId());
        orderInfo.setMerchantName(customerMerchantInfo.getMerchantName());
        // 明细
        orderInfo.setDetailList(detailList.stream()
                .map(item -> {
                    OrderDetailDTO detail = new OrderDetailDTO();
                    detail.setDetailSn(item.getSalesDetailSn());
                    // 缺 goodsId
                    detail.setGoodsModelId(item.getGoodsModelId());
                    detail.setGoodsName(item.getGoodsName());
                    detail.setModelName(item.getModelName());
                    detail.setUnit(item.getUnit());
                    detail.setGoodsImg(item.getGoodsImg());
                    detail.setMarketPrice(item.getMarketPrice());
                    detail.setDealPrice(item.getDealPrice());
                    detail.setGoodsNum(item.getGoodsNum());
                    detail.setAmount(item.getAmount());
                    return detail;
                }).collect(Collectors.toList()));
        return orderInfo;
    }

    /**
     * 根据 商品销售类型设置价格
     *
     * @param salesGoodsType 商品销售类型
     * @param tradePrice     促销品 交易价
     * @param model          商品规格信息
     * @return
     */
    public BigDecimal fetchPriceByType(SalesGoodsTypeEnum salesGoodsType, BigDecimal tradePrice, GoodsModelDTO model) {
        BigDecimal price = model.getSalePrice();
        price = (null == price) ? model.getModelPrice() : price;
        switch (salesGoodsType) {
            case promotion:
                price = (null == tradePrice) ? price : tradePrice;
                break;
            case gift:
                price = BigDecimal.ZERO;
                break;
            case normal:
            default:
        }
        return price;
    }

    /**
     * 根据价格获取商品销售类型
     *
     * @param tradePrice
     * @param model
     * @return
     */
    public static SalesGoodsTypeEnum fetchTypeByPrice(BigDecimal tradePrice, GoodsModelDTO model) {
        BigDecimal price = model.getModelPrice();
        if (tradePrice.compareTo(BigDecimal.ZERO) == 0) {
            return SalesGoodsTypeEnum.gift;
        }
        if (tradePrice.compareTo(price) == 0) {
            return SalesGoodsTypeEnum.normal;
        }
        return SalesGoodsTypeEnum.promotion;
    }

    /**
     * 处理明细列表中的赠品
     *
     * @param detailList
     * @return
     */
    public static List<SalesDetailAddReq> handleDetailListWithGift(List<SalesDetailAddReq> detailList) {
        List<SalesDetailAddReq> giftList = new ArrayList<>(detailList.size());
        detailList.forEach(detail -> {
            BigDecimal giftNum = null == detail.getGiftNum() ? BigDecimal.ZERO : detail.getGiftNum();
            // 只有赠品
            if (detail.getGoodsNum().compareTo(BigDecimal.ZERO) == 0) {
                detail.setGoodsNum(giftNum);
                detail.setDealPrice(BigDecimal.ZERO);
                detail.setSalesGoodsType(SalesGoodsTypeEnum.gift);
            } else if (giftNum.compareTo(BigDecimal.ZERO) > 0) {
                giftList.add(CrmOrderTrans.copyGiftDetail(detail));
            }
        });
        detailList.addAll(giftList);
        return detailList;
    }

    private static SalesDetailAddReq copyGiftDetail(SalesDetailAddReq detail) {
        SalesDetailAddReq gift = new SalesDetailAddReq();
        gift.setGoodsModelId(detail.getGoodsModelId());
        gift.setDealPrice(BigDecimal.ZERO);
        gift.setGoodsNum(detail.getGiftNum());
        gift.setRemarks(detail.getRemarks());
        gift.setSalesGoodsType(SalesGoodsTypeEnum.gift);
        return gift;
    }
}
