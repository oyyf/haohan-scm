/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.crm.GoodsAnalysisTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.SalesGoodsTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 客户销量上报数据统计明细
 *
 * @author haohan
 * @date 2020-01-16 12:08:43
 */
@Data
@TableName("crm_sales_report_analysis_detail")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户销量上报数据统计明细")
public class SalesReportAnalysisDetail extends Model<SalesReportAnalysisDetail> {
    private static final long serialVersionUID = 1L;


    @Length(max = 64, message = "主键长度最大64字符")
    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.INPUT)
    private String id;

    @Length(max = 64, message = "统计id长度最大64字符")
    @ApiModelProperty(value = "统计id")
    private String analysisId;

    @Length(max = 64, message = "统计项名称长度最大64字符")
    @ApiModelProperty(value = "统计项名称")
    private String name;

    @Digits(integer = 10, fraction = 2, message = "统计项数量的整数位最大10位, 小数位最大2位")
    @ApiModelProperty(value = "统计项数量")
    private BigDecimal num;

    @ApiModelProperty(value = "统计项类型(1.规格2.分类)")
    private GoodsAnalysisTypeEnum type;

    @ApiModelProperty(value = "商品销售类型:1.普通2.促销品3.赠品")
    private SalesGoodsTypeEnum salesGoodsType;

    @Length(max = 64, message = "客户编号长度最大64字符")
    @ApiModelProperty(value = "客户编号")
    private String customerSn;

    @Length(max = 64, message = "月份长度最大64字符")
    @ApiModelProperty(value = "月份")
    private String month;

    @Length(max = 64, message = "创建者长度最大64字符")
    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;

    @Length(max = 64, message = "更新者长度最大64字符")
    @ApiModelProperty(value = "更新者")
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @Length(max = 255, message = "备注信息长度最大255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Length(max = 1, message = "删除标记长度最大1字符")
    @ApiModelProperty(value = "删除标记")
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private Integer tenantId;

}
