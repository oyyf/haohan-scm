/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 放款申请记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "放款申请记录")
public class LendingRecordReq extends LendingRecord {

    private long pageSize;
    private long pageNo;




}
