package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.LendingStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
@ApiModel(description = "请款审核需求")
public class PurchaseAuditLendingReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "商家平台id", required = true)
    private String pmId;

    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "请款记录id", required = true)
    private String id;

    @NotNull(message = "lendingStatus不能为空")
    @ApiModelProperty(value = "请款状态:1.已申请2.通过初审3.待复审4.审核未通过5.待放款6.已放款",required = true)
    private LendingStatusEnum lendingStatus;

    @NotBlank(message = "uId不能为空")
    @ApiModelProperty(value = "用户id", required = true)
    private String uId;

    @ApiModelProperty(value = "审核人id")
    private String auditorId;

    @ApiModelProperty(value = "审核人名称")
    private String auditorName;

    @ApiModelProperty(value = "审核人姓名")
    private String auditorOpinion;

    @ApiModelProperty(value = "审核时间",example = "2019-01-01 12:00:00")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;

    @ApiModelProperty(value = "复审人id")
    private String reviewerId;

    @ApiModelProperty(value = "复审人名称")
    private String reviewerName;

    @ApiModelProperty(value = "复审人意见")
    private String reviewerOpinion;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime reviewTime;

}
