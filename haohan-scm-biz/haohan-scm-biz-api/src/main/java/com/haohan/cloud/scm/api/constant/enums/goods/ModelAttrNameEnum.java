package com.haohan.cloud.scm.api.constant.enums.goods;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/10/17
 */
@Getter
@AllArgsConstructor
public enum ModelAttrNameEnum implements IBaseEnum {

    /**
     * 商品规格类型 对应公共商品库 商品属性名
     */
    yardage("1", "尺码"),
    colour("2", "颜色"),
    taste("3", "口味"),
    capacity("4", "容量"),
    group("5", "套餐"),
    kind("6", "种类"),
    size("7", "尺寸"),
    weight("8", "重量"),
    model("9", "型号"),
    style("10", "款式");

    private static final Map<String, ModelAttrNameEnum> MAP = new HashMap<>(16);
    private static final Map<String, ModelAttrNameEnum> DESC_MAP = new HashMap<>(16);

    static {
        for (ModelAttrNameEnum e : ModelAttrNameEnum.values()) {
            MAP.put(e.getType(), e);
            DESC_MAP.put(e.getDesc(), e);
        }
    }

    public static ModelAttrNameEnum getByDesc(String desc) {
        return DESC_MAP.get(desc);
    }

    @JsonCreator
    public static ModelAttrNameEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
