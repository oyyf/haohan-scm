/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 商品分类
 *
 * @author haohan
 * @date 2019-05-28 19:55:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "商品分类")
public class GoodsCategoryReq extends GoodsCategory {

    @ApiModelProperty(value = "分类树是否全展示")
    private boolean allShow;

    private long pageSize;
    private long pageNo;
}
