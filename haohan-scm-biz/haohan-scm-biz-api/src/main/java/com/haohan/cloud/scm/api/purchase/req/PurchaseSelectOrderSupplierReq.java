package com.haohan.cloud.scm.api.purchase.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/17
 */
@Data
public class PurchaseSelectOrderSupplierReq {

    @NotBlank(message = "pmId不能为空")
    private String pmId;

    private String id;

    /**
     * 采购单编号
     */
    private String purchaseSn;
    /**
     * 采购明细编号
     */
    private String purchaseDetailSn;
    /**
     * 汇总明细编号
     */
    private String summaryDetailSn;
    /**
     * 商品id
     */
    private String goodsId;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 商品分类id
     */
    private String goodsCategoryId;
    /**
     * 商品图片
     */
    private String goodsImg;
    /**
     * 商品分类名称
     */
    private String goodsCategoryName;
    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 规格名称
     */
    private String modelName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 采购状态:1.待处理2.待审核3.采购中4.采购完成5.部分完成6.已关闭
     */
    private String purchaseStatus;
    /**
     * 采购订单分类:1.采购计划2.按需采购
     */
    private String purchaseOrderType;
    /**
     * 采购截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyFinalTime;
    /**
     * 发起人
     */
    private String initiatorId;
    /**
     * 执行人
     */
    private String transactorId;
    /**
     * 供应商
     */
    private String supplierId;
    /**
     * 供应商名称
     */
    private String supplierName;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 采购价
     */
    private BigDecimal buyPrice;
    /**
     * 需求采购数量
     */
    private BigDecimal needBuyNum;
    /**
     * 实际采购数量
     */
    private BigDecimal realBuyNum;
    /**
     * 揽货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receiveTime;
    /**
     * 付款方式1.协议2.现款
     */
    private String payType;
    /**
     * 揽货方式1.自提2.送货上门
     */
    private String receiveType;
    /**
     * 采购执行状态1.正常2.异常
     */
    private String actionType;
    /**
     * 采购方式类型:1.竞价采购2.单品采购3.协议供应
     */
    private String methodType;
    /**
     * 竞价截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime biddingEndTime;
    /**
     * 报价状态:1.无需报价2.未报价3.已报价
     */
    private String offerType;
    /**
     * 请款类型1.不请款2.需请款
     */
    private String lendingType;
    /**
     * 请款人类型:1.发起人2.执行人3.供应商
     */
    private String lenderType;
    /**
     * 来源采购单明细编号
     */
    private String originalPurchaseDetailSn;
    /**
     * 中标状态:0.不开标1.已中标2.落标
     */
    private String biddingStatus;
    /**
     * 备注信息
     */
    private String remarks;
}
