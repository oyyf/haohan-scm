package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.LendingTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/8/8
 */
@Data
@ApiModel(description = "揽货确认")
public class ConfirmGoodsReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "taskId不能为空")
    @ApiModelProperty(value = "采购任务id", required = true)
    private String taskId;

    @NotNull(message = "realBuyNum不能为空")
    @ApiModelProperty(value = "实际揽货数量", required = true)
    private BigDecimal realBuyNum;

    @ApiModelProperty(value = "请款类型1.不请款2.需请款")
    private LendingTypeEnum lendingType;

    @ApiModelProperty(value = "付款方式1.协议2.现款")
    private PayTypeEnum payType;

    @ApiModelProperty(value = "揽货方式1.自提2.送货上门")
    private ReceiveTypeEnum receiveType;

    @ApiModelProperty(value = "任务执行备注")
    private String actionDesc;

    @ApiModelProperty(value = "请款记录编号")
    private String lendingSn;

    @ApiModelProperty(value = "其他费用")
    private BigDecimal otherAmount;
}
