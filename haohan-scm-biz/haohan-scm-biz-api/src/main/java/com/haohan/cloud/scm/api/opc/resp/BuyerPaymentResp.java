package com.haohan.cloud.scm.api.opc.resp;

import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dy
 * @date 2019/7/3
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BuyerPaymentResp extends BuyerPayment {

    private static final long serialVersionUID = 1L;

    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 制单人
     */
    private String createName;

    /**
     * 应收账单 商家id
     */
    private String merchantId;

    /**
     * 应收账单 商家名称
     */
    private String merchantName;

}
