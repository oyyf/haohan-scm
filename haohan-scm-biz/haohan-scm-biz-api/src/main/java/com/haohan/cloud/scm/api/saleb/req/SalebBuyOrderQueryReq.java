package com.haohan.cloud.scm.api.saleb.req;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
public class SalebBuyOrderQueryReq {

    /**
     * 主键
     */
    private String id;
    /**
     * 平台商家id
     */
    private String pmId;
    /**
     * 采购编号
     */
    private String buyId;
    /**
     * 采购商
     */
    private String buyerId;
    /**
     * 采购用户
     */
    private String buyerUid;
    /**
     * 采购时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime buyTime;
    /**
     * 送货日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deliveryTime;
    /**
     * 送货批次
     */
    private String buySeq;
    /**
     * 状态
     */
    private String status;
    /**
     * 成交时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dealTime;
    /**
     * 配送方式
     */
    private String deliveryType;
    /**
     * 零售单号
     */
    private String goodsOrderId;
    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 商品规格id
     */
    private String goodsModelId;
    /**
     * 是否已汇总 0否1是
     */
    private String summaryFlag;

}
