/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerUpdateLog;
import com.haohan.cloud.scm.api.crm.req.CustomerUpdateLogReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户修改日志内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerUpdateLogFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerUpdateLogFeignService {


    /**
     * 通过id查询客户修改日志
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerUpdateLog/{id}", method = RequestMethod.GET)
    R<CustomerUpdateLog> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户修改日志 列表信息
     *
     * @param customerUpdateLogReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerUpdateLog/fetchCustomerUpdateLogPage")
    R<Page<CustomerUpdateLog>> getCustomerUpdateLogPage(@RequestBody CustomerUpdateLogReq customerUpdateLogReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户修改日志 列表信息
     *
     * @param customerUpdateLogReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerUpdateLog/fetchCustomerUpdateLogList")
    R<List<CustomerUpdateLog>> getCustomerUpdateLogList(@RequestBody CustomerUpdateLogReq customerUpdateLogReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增客户修改日志
     *
     * @param customerUpdateLog 客户修改日志
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/add")
    R<Boolean> save(@RequestBody CustomerUpdateLog customerUpdateLog, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改客户修改日志
     *
     * @param customerUpdateLog 客户修改日志
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/update")
    R<Boolean> updateById(@RequestBody CustomerUpdateLog customerUpdateLog, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除客户修改日志
     *
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/delete/{id}")
    R<Boolean> removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/batchDelete")
    R<Boolean> removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/listByIds")
    R<List<CustomerUpdateLog>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerUpdateLogReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/countByCustomerUpdateLogReq")
    R<Integer> countByCustomerUpdateLogReq(@RequestBody CustomerUpdateLogReq customerUpdateLogReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerUpdateLogReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/getOneByCustomerUpdateLogReq")
    R<CustomerUpdateLog> getOneByCustomerUpdateLogReq(@RequestBody CustomerUpdateLogReq customerUpdateLogReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param customerUpdateLogList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/CustomerUpdateLog/saveOrUpdateBatch")
    R<Boolean> saveOrUpdateBatch(@RequestBody List<CustomerUpdateLog> customerUpdateLogList, @RequestHeader(SecurityConstants.FROM) String from);


}
