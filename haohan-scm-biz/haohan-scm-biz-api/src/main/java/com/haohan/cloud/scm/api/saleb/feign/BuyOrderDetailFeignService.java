/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.saleb.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderDetailReq;
import com.haohan.cloud.scm.api.saleb.req.QuerySummaryOrderDetailReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 采购单明细内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "BuyOrderDetailFeignService", value = ScmServiceName.SCM_BIZ_SALEB)
public interface BuyOrderDetailFeignService {


    /**
     * 通过id查询采购单明细
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/BuyOrderDetail/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 采购单明细 列表信息
     * @param buyOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/BuyOrderDetail/fetchBuyOrderDetailPage")
    R getBuyOrderDetailPage(@RequestBody BuyOrderDetailReq buyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 采购单明细 列表信息
     * @param buyOrderDetailReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/BuyOrderDetail/fetchBuyOrderDetailList")
    R<List<BuyOrderDetail>> getBuyOrderDetailList(@RequestBody BuyOrderDetailReq buyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/add")
    R save(@RequestBody BuyOrderDetail buyOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/update")
    R updateById(@RequestBody BuyOrderDetail buyOrderDetail, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除采购单明细
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/BuyOrderDetail/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param buyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/countByBuyOrderDetailReq")
    R<Integer> countByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq buyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param buyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/getOneByBuyOrderDetailReq")
    R<BuyOrderDetail> getOneByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq buyOrderDetailReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param buyOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/BuyOrderDetail/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<BuyOrderDetail> buyOrderDetailList, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询商品毛利
     * @param pmId
     * @param from
     * @return
     */
    @PostMapping("/api/feign/BuyOrderDetail/queryGoodsProfit")
    R queryGoodsProfit(@RequestBody String pmId,@RequestHeader(SecurityConstants.FROM)String from);

    /**
     * 查询汇总单对应订单明细(汇总临时数据)
     * @param req
     * @param form
     * @return
     */
    @PostMapping("/api/feign/BuyOrderDetail/queryWaitSummaryOrderDetail")
    R queryWaitSummaryOrderDetail(@RequestBody QuerySummaryOrderDetailReq req,@RequestHeader(SecurityConstants.FROM)String form);
}
