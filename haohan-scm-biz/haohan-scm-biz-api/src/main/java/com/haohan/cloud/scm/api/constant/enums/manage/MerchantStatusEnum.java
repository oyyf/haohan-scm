package com.haohan.cloud.scm.api.constant.enums.manage;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2019/9/24
 */
@Getter
@AllArgsConstructor
public enum MerchantStatusEnum implements IBaseEnum {

    /**
     * 商家状态 2启用  0 待审核 -1 停用  (原系统使用)
     */
    disabled("-1", "停用"),
    stayAudit("0", "待审核"),
    enabled("2", "启用");

    private static final Map<String, MerchantStatusEnum> MAP = new HashMap<>(8);

    static {
        for (MerchantStatusEnum e : MerchantStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static MerchantStatusEnum getByType(String type) {
        return MAP.get(type);
    }

    @EnumValue
    @JsonValue
    private String type;
    private String desc;
}
