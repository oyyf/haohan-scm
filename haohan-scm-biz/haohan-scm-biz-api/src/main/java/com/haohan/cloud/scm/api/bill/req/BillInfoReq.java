package com.haohan.cloud.scm.api.bill.req;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.bill.dto.BillSqlDTO;
import com.haohan.cloud.scm.api.bill.entity.BaseBill;
import com.haohan.cloud.scm.api.constant.enums.common.ReviewStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author dy
 * @date 2019/11/26
 */
@Data
@ApiModel("账单查询")
public class BillInfoReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @Length(max = 32, message = "账单编号最大长度32字符")
    @ApiModelProperty(value = "账单编号(应收、应付)")
    private String billSn;

    @ApiModelProperty(value = "账单审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum reviewStatus;

    @ApiModelProperty(value = "排除的账单审核状态: 1.待审核2.审核不通过3.审核通过")
    private ReviewStatusEnum excludeStatus;

    @ApiModelProperty(value = "账单类型")
    private BillTypeEnum billType;

    @Length(max = 32, message = "结算单编号最大长度32字符")
    @ApiModelProperty(value = "结算单编号")
    private String settlementSn;

    @ApiModelProperty(value = "结算状态")
    private YesNoEnum settlementStatus;

    @Length(max = 32, message = "平台商家ID最大长度32字符")
    @ApiModelProperty(value = "平台商家ID")
    private String pmId;

    @Length(max = 32, message = "来源订单编号最大长度32字符")
    @ApiModelProperty(value = "来源订单编号")
    private String orderSn;

    @Length(max = 32, message = "下单客户ID最大长度32字符")
    @ApiModelProperty(value = "下单客户ID", notes = "采购商id、供应商id、客户sn")
    private String customerId;

    @Length(max = 32, message = "客户商家ID最大长度32字符")
    @ApiModelProperty(value = "客户商家ID")
    private String merchantId;

    @ApiModelProperty(value = "预付标志", notes = "是否预付账单")
    private YesNoEnum advanceFlag;

    // 非eq查询条件

    @Length(max = 10, message = "下单客户名称最大长度10字符")
    @ApiModelProperty(value = "下单客户名称", notes = "采购商、供应商、客户")
    private String customerName;

    @Length(max = 10, message = "客户商家名称最大长度10字符")
    @ApiModelProperty(value = "客户商家名称")
    private String merchantName;

    @ApiModelProperty(value = "订单成交日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "订单成交日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    // feign使用

    @ApiModelProperty(value = "下单客户ID 集合", notes = "采购商id、供应商id、客户sn")
    private Set<String> customerIdSet;


    public <T extends BaseBill> void copyToBill(T bill) {
        // eq参数
        bill.setBillSn(this.billSn);
        bill.setReviewStatus(this.reviewStatus);
        bill.setBillType(this.billType);
        bill.setSettlementSn(this.settlementSn);
        bill.setSettlementStatus(this.settlementStatus);
        bill.setPmId(this.pmId);
        bill.setOrderSn(this.orderSn);
        bill.setCustomerId(this.customerId);
        bill.setMerchantId(this.merchantId);
        bill.setAdvanceFlag(this.advanceFlag);
    }

    public BillSqlDTO transTo() {
        BillSqlDTO query = new BillSqlDTO();
        // eq参数
        query.setBillSn(this.billSn);
        query.setBillType(this.billType == null ? null : this.billType.getType());
        query.setReviewStatus(this.reviewStatus == null ? null : this.reviewStatus.getType());
        query.setExcludeStatus(this.excludeStatus == null ? null : this.excludeStatus.getType());
        query.setSettlementStatus(this.settlementStatus == null ? null : this.settlementStatus.getType());
        query.setAdvanceFlag(this.advanceFlag == null ? null : this.advanceFlag.getType());
        query.setCustomerId(this.customerId);
        // 非eq
        query.queryDate(this.startDate, this.endDate);
        if (CollUtil.isNotEmpty(this.customerIdSet)) {
            query.setCustomerIds(CollUtil.join(this.customerIdSet, StrUtil.COMMA));
        }
        return query;
    }
}
