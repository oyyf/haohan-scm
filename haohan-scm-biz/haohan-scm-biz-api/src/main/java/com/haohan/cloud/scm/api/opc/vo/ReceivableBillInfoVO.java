package com.haohan.cloud.scm.api.opc.vo;

import com.haohan.cloud.scm.api.opc.dto.BillPaymentDetail;
import io.swagger.annotations.Api;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Api("应收账单详情")
public class ReceivableBillInfoVO extends ReceivableBillVO {
    private static final long serialVersionUID = 1L;

    /**
     * 账单明细列表
     */
    private List<BillPaymentDetail> detailList;

}
