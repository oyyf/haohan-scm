/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.StoragePlace;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 暂存点信息表
 *
 * @author haohan
 * @date 2019-05-28 19:50:27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "暂存点信息表")
public class StoragePlaceReq extends StoragePlace {

    private long pageSize;
    private long pageNo;




}
