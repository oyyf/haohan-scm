package com.haohan.cloud.scm.api.manage.vo;

import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/3/31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhotoVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("图片id")
    private String id;

    @ApiModelProperty("图片地址")
    private String picUrl;

    public PhotoVO(PhotoGallery photo) {
        this.id = photo.getId();
        this.picUrl = photo.getPicUrl();
    }

}
