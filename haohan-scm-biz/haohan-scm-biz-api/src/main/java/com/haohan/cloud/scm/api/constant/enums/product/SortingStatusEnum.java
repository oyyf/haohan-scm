package com.haohan.cloud.scm.api.constant.enums.product;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum SortingStatusEnum implements IBaseEnum {

    /**
     * 分拣状态:1待分拣2.可分拣3已分拣4已完成
     */
    wait("1", "待分拣"),
    sorting("2", "可分拣"),
    selected("3", "已分拣"),
    finish("4", "已完成");

    private static final Map<String, SortingStatusEnum> MAP = new HashMap<>(8);

    static {
        for (SortingStatusEnum e : SortingStatusEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static SortingStatusEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    @EnumValue
    @JsonValue
    private String type;

    private String desc;
}
