/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.req;

import com.haohan.cloud.scm.api.crm.entity.WorkReport;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 工作汇报
 *
 * @author haohan
 * @date 2019-08-30 11:45:47
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "工作汇报")
public class WorkReportReq extends WorkReport {

    private long pageSize;
    private long pageNo;


}
