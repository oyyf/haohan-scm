package com.haohan.cloud.scm.api.constant;

/**
 * Created by zgw on 2019/5/21.
 * 业务服务名
 */
public interface ScmServiceName {

  /**
   * <!-- 内部采购系统-->
   * <module>haohan-scm-biz-purchase</module>
   * <!-- 供应系统-->
   * <module>haohan-scm-biz-supply</module>
   * <!-- 生产系统-->
   * <module>haohan-scm-biz-product</module>
   * <!-- 市场销售系统-->
   * <module>haohan-scm-biz-market</module>
   * <!-- B客户系统-->
   * <module>haohan-scm-biz-saleB</module>
   * <!-- C客户系统-->
   * <module>haohan-scm-biz-salec</module>
   * <!-- 运营管理系统-->
   * <module>haohan-scm-biz-opc</module>
   * <!-- 售后系统-->
   * <module>haohan-scm-biz-aftersales</module>
   * <!-- 仓储管理-->
   * <module>haohan-scm-biz-wms</module>
   * <!-- 运输管理-->
   * <module>haohan-scm-biz-tms</module>
   * <!-- 订单管理-->
   * <module>haohan-scm-biz-oms</module>
   */

//  String SCM_BIZ_CORE = "haohan-scm-biz-core";
//
//  String SCM_BIZ_PURCHASE = "haohan-scm-biz-purchase";
//
//  String SCM_BIZ_SUPPLY = "haohan-scm-biz-supply";
//
//  String SCM_BIZ_PRODUCT = "haohan-scm-biz-product";
//
//  String SCM_BIZ_MARKET = "haohan-scm-biz-market";
//
//  String SCM_BIZ_SALEB = "haohan-scm-biz-saleb";
//
//  String SCM_BIZ_SALEC = "haohan-scm-biz-salec";
//
//  String SCM_BIZ_OPC = "haohan-scm-biz-opc";
//
//  String SCM_BIZ_AFTERSALES = "haohan-scm-biz-aftersales";
//
//  String SCM_BIZ_WMS = "haohan-scm-biz-wms";
//
//  String SCM_BIZ_TMS = "haohan-scm-biz-tms";
//
//  String SCM_BIZ_OMS = "haohan-scm-biz-oms";
//
//  String SCM_BIZ_MSG = "haohan-scm-biz-message";


  String PreServiceName = "core";

  String SCM_BIZ_CORE = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_PURCHASE = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_SUPPLY = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_PRODUCT = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_MARKET = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_SALEB = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_SALEC = "haohan-scm-biz-salec";

  String SCM_BIZ_OPC = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_AFTERSALES = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_WMS = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_TMS = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_OMS = "haohan-scm-biz-"+PreServiceName;

  String SCM_BIZ_MSG = "haohan-scm-biz-"+PreServiceName;


  /**
   * <module>haohan-scm-common</module>
   * <module>haohan-scm-manage</module>
   * <module>haohan-scm-goods</module>
   * <module>haohan-scm-oms</module>
   * <module>haohan-scm-pay</module>
   * <module>haohan-scm-wms</module>
   * <module>haohan-scm-tms</module>
   * <module>haohan-scm-retail</module>
   * <module>haohan-scm-bi</module>
   * <module>haohan-scm-iot</module>
   **/

//  String SCM_COMMON = "haohan-scm-common";
//  String SCM_MANAGE = "haohan-scm-manage";
//  String SCM_GOODS = "haohan-scm-goods";
//
//  String SCM_OMS = "haohan-scm-oms";
//  String SCM_PAY = "haohan-scm-pay";
//  String SCM_WMS = "haohan-scm-wms";
//  String SCM_TMS = "haohan-scm-tms";
//  String SCM_RETAIL = "haohan-scm-retail";
//  String SCM_BI = "haohan-scm-bi";
//  String SCM_IOT = "haohan-scm-iot";

   String manageServiceName = "manage";

  String SCM_COMMON = "haohan-scm-"+manageServiceName;
  String SCM_MANAGE = "haohan-scm-"+manageServiceName;
  String SCM_GOODS = "haohan-scm-"+manageServiceName;
  String SCM_IOT = "haohan-scm-"+manageServiceName;
  String SCM_PAY = "haohan-scm-"+manageServiceName;
  String SCM_BILL = "haohan-scm-"+manageServiceName;

  // 单独启动
  String SCM_CRM = "haohan-scm-crm";

  //以下为产品模块暂时不启用
  String SCM_OMS = "haohan-scm-oms";
  String SCM_WMS = "haohan-scm-wms";
  String SCM_TMS = "haohan-scm-tms";
  String SCM_RETAIL = "haohan-scm-retail";
  String SCM_BI = "haohan-scm-bi";


}
