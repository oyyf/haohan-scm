package com.haohan.cloud.scm.api.crm.req.imp;

import com.haohan.cloud.scm.api.crm.dto.imp.SalesAreaImport;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2019/10/11
 */
@Data
public class SalesAreaImportReq {

    @NotEmpty(message = "销售区域列表不能为空")
    private List<SalesAreaImport> areaList;

}
