/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.opc.feign;

import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.TradeOrderReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * @program: haohan-fresh-scm
 * @description: 交易订单内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "TradeOrderFeignService", value = ScmServiceName.SCM_BIZ_OPC)
public interface TradeOrderFeignService {


    /**
     * 通过id查询交易订单
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/TradeOrder/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 交易订单 列表信息
     * @param tradeOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TradeOrder/fetchTradeOrderPage")
    R getTradeOrderPage(@RequestBody TradeOrderReq tradeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 交易订单 列表信息
     * @param tradeOrderReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/TradeOrder/fetchTradeOrderList")
    R getTradeOrderList(@RequestBody TradeOrderReq tradeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 新增交易订单
     * @param tradeOrder 交易订单
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/add")
    R save(@RequestBody TradeOrder tradeOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 修改交易订单
     * @param tradeOrder 交易订单
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/update")
    R updateById(@RequestBody TradeOrder tradeOrder, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 通过id删除交易订单
     * @param id id
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/delete/{id}")
    R removeById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);

    /**
   * 删除（根据ID 批量删除)
   *
   * @param idList 主键ID列表
   * @return R
   */
    @PostMapping("/api/feign/TradeOrder/batchDelete")
    R removeByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/listByIds")
    R listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/countByTradeOrderReq")
    R countByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/getOneByTradeOrderReq")
    R getOneByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 批量修改OR插入
     *
     * @param tradeOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @PostMapping("/api/feign/TradeOrder/saveOrUpdateBatch")
    R saveOrUpdateBatch(@RequestBody List<TradeOrder> tradeOrderList, @RequestHeader(SecurityConstants.FROM) String from);


}
