package com.haohan.cloud.scm.api.opc.req.payment;

import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author dy
 * @date 2019/7/26
 */
@Data
@ApiModel(description = "创建应付账单")
public class CreatePayableReq {

    @NotBlank(message = "pmId不能为空")
    @ApiModelProperty(value = "平台商家id", required = true)
    private String pmId;

    @NotBlank(message = "payableSn不能为空")
    @ApiModelProperty(value = "应付来源订单编号", required = true)
    private String payableSn;

    @NotNull(message = "billType不能为空")
    @ApiModelProperty(value = "账单类型", required = true)
    private BillTypeEnum billType;

    public SupplierPayment transTo() {
        SupplierPayment supplierPayment = new SupplierPayment();
        supplierPayment.setPmId(this.pmId);
        supplierPayment.setPayableSn(this.payableSn);
        supplierPayment.setBillType(this.billType);
        return supplierPayment;
    }


}
