package com.haohan.cloud.scm.api.salec.dto;

import com.haohan.cloud.scm.api.saleb.req.BuyDetailModifyReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dy
 * @date 2020/6/17
 * 迁移后使用, 订单购物车属性转换
 */
@Data
public class StoreOrderDetailDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "商品规格id不能为空")
    @Length(min = 0, max = 32, message = "商品规格id长度在0至32之间")
    @ApiModelProperty(value = "商品规格id")
//    private String goodsModelId;
    private String product_attr_unique;

    @NotNull(message = "采购数量不能为空")
    @Digits(integer = 8, fraction = 2, message = "采购数量的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "采购数量")
//    private BigDecimal goodsNum;
    private BigDecimal cart_num;

    @NotNull(message = "采购价格不能为空")
    @Digits(integer = 8, fraction = 2, message = "采购价格的整数位最大8位, 小数位最大2位")
    @ApiModelProperty(value = "采购价格")
//    private BigDecimal buyPrice;
    private BigDecimal truePrice;

    public BuyDetailModifyReq transTo() {
        BuyDetailModifyReq detail =   new BuyDetailModifyReq();
        detail.setBuyPrice(this.getTruePrice());
        detail.setGoodsModelId(this.getProduct_attr_unique());
        detail.setGoodsNum(this.getCart_num());
//        detail.setRemarks();
        return detail;
    }
}
