package com.haohan.cloud.scm.api.bill.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author dy
 * @date 2020/3/7
 */
@Data
public class SummarySettlementReq {

    @ApiModelProperty("账单编号列表")
    @NotEmpty(message = "账单编号列表不能为空")
    List<String> billSnList;


}
