package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.ReceiveTypeEnum;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author xwx
 * @date 2019/7/13
 */
@Data
public class ParamsReq {
    /**
     * 商品规格id
     */
    @NotBlank(message = "goodsModelId不能为空")
    private String goodsModelId;
    /**
     * 需求采购数量
     */
    @NotNull(message = "needBuyNum不能为空")
    private BigDecimal needBuyNum;
    /**
     * 汇总单号
     */
    private String summaryOrderId;
    /**
     * 供应商id
     */
    private String supplierId;

    /**
     * 采购价
     */
    private BigDecimal buyPrice;

    /**
     * 支付类型
     */
    private PayTypeEnum payType;

    /**
     * 揽货方式
     */
    private ReceiveTypeEnum receiveType;

    /**
     * 采购方式类型
     */
    private MethodTypeEnum methodType;
    /**
     * 竞价截止时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime biddingEndTime;
}
