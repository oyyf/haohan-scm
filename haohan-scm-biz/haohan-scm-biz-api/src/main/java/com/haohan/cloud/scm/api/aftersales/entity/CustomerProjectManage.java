/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.aftersales.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.PayTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * 客户项目管理
 *
 * @author haohan
 * @date 2019-05-13 20:13:33
 */
@Data
@TableName("scm_customer_project_manage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "客户项目管理")
public class CustomerProjectManage extends Model<CustomerProjectManage> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 季度
     */
    private String quarter;
    /**
     * 签约时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime signTime;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 区域
     */
    private String area;
    /**
     * 服务产品
     */
    private String serviceProduct;
    /**
     * 服务版本
     */
    private String serviceType;
    /**
     * 开通功能
     */
    private String serviceList;
    /**
     * 支付方式
     */
    private PayTypeEnum payType;
    /**
     * 支付时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payTime;
    /**
     * 支付金额
     */
    private String payAmount;
    /**
     * 业务专管员
     */
    private String bizUser;
    /**
     * 运营专管员
     */
    private String opUser;
    /**
     * 技术负责人
     */
    private String techUser;
    /**
     * 财务核对人
     */
    private String financeUser;
    /**
     * 项目情况
     */
    private String projectInfo;
    /**
     * 其他说明
     */
    private String projectDesc;
    /**
     * 项目阶段
     */
    private String projectStep;
    /**
     * 上线状态
     */
    private YesNoEnum onlineStatus;
    /**
     * 上线时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime onlineTime;
    /**
     * 商家资源库ID
     */
    private String merchantDatabase;
    /**
     * 商家ID
     */
    private String merchant;
    /**
     * 商家应用ID
     */
    private String merchantApp;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
