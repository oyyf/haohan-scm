/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 仓库库存
 *
 * @author haohan
 * @date 2019-08-22 16:12:12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "仓库库存")
public class WarehouseStockReq extends WarehouseStock {

    private long pageSize;
    private long pageNo;




}
