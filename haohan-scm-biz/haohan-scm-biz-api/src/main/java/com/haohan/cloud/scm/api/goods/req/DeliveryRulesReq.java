/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.goods.req;

import com.haohan.cloud.scm.api.goods.entity.DeliveryRules;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 配送规则
 *
 * @author haohan
 * @date 2019-05-28 19:56:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "配送规则")
public class DeliveryRulesReq extends DeliveryRules {

    private long pageSize;
    private long pageNo;




}
