/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.bill.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.bill.req.SettlementFeignReq;
import com.haohan.cloud.scm.api.bill.vo.SettlementInfoVO;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.opc.req.SettlementRecordReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


/**
 * @author dy
 * @program: haohan-fresh-scm
 * @description: 结算单内部接口服务
 * @date 2019/11/26
 **/
@FeignClient(contextId = "SettlementFeignService", value = ScmServiceName.SCM_BILL)
public interface SettlementFeignService {


    /**
     * 通过id查询结算记录  暂不使用
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/SettlementRecord/{id}", method = RequestMethod.GET)
    R getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 结算记录 列表信息  暂不使用
     *
     * @param settlementRecordReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/SettlementRecord/fetchSettlementRecordPage")
    R getSettlementRecordPage(@RequestBody SettlementRecordReq settlementRecordReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 在线支付后完成结算
     *
     * @param req  结算必须参数
     *             settlementSn
     *             companyOperator
     *             settlementTime
     * @param from
     * @return
     */
    @PostMapping("/api/feign/settlement/payToSettlement")
    R<Boolean> payToSettlement(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 准备结算
     *
     * @param req
     * @param from
     * @return R
     */
    @PostMapping("/api/feign/settlement/ready")
    R<SettlementInfoVO> readySettlement(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 线上准备结算, 多账单合并后准备, 单个账单准备
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/settlement/readyOnline")
    R<SettlementInfoVO> readyOnlineSettlement(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 结算单完成收款结算
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/settlement/finish")
    R<Boolean> finishSettlement(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 结算单分页查询
     *
     * @param req
     * @param from
     * @return
     */
    @PostMapping("/api/feign/settlement/page")
    R<Page<SettlementInfoVO>> findPage(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 结算单详情
     *
     * @param settlementSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/settlement/fetchInfo")
    R<SettlementInfoVO> fetchSettlementInfo(@RequestParam("settlementSn") String settlementSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 结算单信息
     *
     * @param settlementSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/settlement/fetchOne")
    R<SettlementAccount> fetchSettlementAccount(@RequestParam("settlementSn") String settlementSn, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 查询订单的支付状态(默认未付)
     *
     * @param orderSn
     * @param from
     * @return
     */
    @GetMapping("/api/feign/settlement/orderPayStatus")
    R<PayStatusEnum> queryOrderPayStatus(@RequestParam("orderSn") String orderSn, @RequestHeader(SecurityConstants.FROM) String from);

    @PostMapping("/api/feign/settlement/updateMerchantName")
    R<Boolean> updateMerchantName(@RequestBody SettlementFeignReq req, @RequestHeader(SecurityConstants.FROM) String from);
}
