package com.haohan.cloud.scm.api.crm.vo.analysis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/11/8
 */
@Data
@Api("业务员销售数据")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value = {"employeeId"})
public class EmployeeSalesVO {

    @ApiModelProperty(value = "业务员")
    private String employeeId;

    @ApiModelProperty(value = "业务员名称")
    private String employeeName;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderAmount;

    @ApiModelProperty(value = "回款金额")
    private BigDecimal backAmount;

    @ApiModelProperty(value = "订单数")
    private Integer orderNum;

}
