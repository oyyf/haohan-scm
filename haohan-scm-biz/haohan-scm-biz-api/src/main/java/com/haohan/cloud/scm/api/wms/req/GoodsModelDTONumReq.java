package com.haohan.cloud.scm.api.wms.req;

import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author cx
 * @date 2019/8/12
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsModelDTONumReq extends GoodsModelDTO {
    private Integer exitNum;
}
