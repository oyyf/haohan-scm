/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.product.req;

import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-28 20:52:02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "生产任务表")
public class ProductionTaskReq extends ProductionTask {

    private long pageSize;
    private long pageNo;




}
