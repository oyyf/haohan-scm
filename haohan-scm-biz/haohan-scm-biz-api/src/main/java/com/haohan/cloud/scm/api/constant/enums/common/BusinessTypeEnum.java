package com.haohan.cloud.scm.api.constant.enums.common;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum BusinessTypeEnum {
    /**
     * 商务类型  ： 1：商务合作、2：问题咨询、3：代理加盟
     */
    businessCooperation("1","商务合作"),
    consultingWith("2","问题咨询"),
    agent("3","代理加盟");

    private static final Map<String, BusinessTypeEnum> MAP = new HashMap<>(8);

    static {
        for (BusinessTypeEnum e : BusinessTypeEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static BusinessTypeEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */

    private String desc;
}
