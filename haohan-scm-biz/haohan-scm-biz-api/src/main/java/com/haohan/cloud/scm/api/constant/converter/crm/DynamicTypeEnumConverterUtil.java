package com.haohan.cloud.scm.api.constant.converter.crm;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.crm.DynamicTypeEnum;

/**
 * @author dy
 * @date 2019/9/24
 */
public class DynamicTypeEnumConverterUtil implements Converter<DynamicTypeEnum> {
    @Override
    public DynamicTypeEnum convert(Object o, DynamicTypeEnum dynamicTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return DynamicTypeEnum.getByType(o.toString());
    }

}
