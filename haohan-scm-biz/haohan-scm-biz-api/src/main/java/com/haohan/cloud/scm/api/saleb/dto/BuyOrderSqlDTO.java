package com.haohan.cloud.scm.api.saleb.dto;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.common.dto.SelfSqlDTO;
import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2020/5/16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BuyOrderSqlDTO extends SelfSqlDTO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "采购编号")
    private String buyId;

    @ApiModelProperty(value = "采购商")
    private String buyerId;

    @ApiModelProperty(value = "送货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "送货批次")
    private BuySeqEnum buySeq;

    @ApiModelProperty(value = "订单状态")
    private BuyOrderStatusEnum status;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    @ApiModelProperty(value = "采购商名称")
    private String buyerName;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @ApiModelProperty(value = "配送地址")
    private String address;

    @ApiModelProperty(value = "采购商家名称")
    private String merchantName;

    @ApiModelProperty(value = "采购商家id")
    private String merchantId;

    @ApiModelProperty(value = "订单状态集合")
    private Set<BuyOrderStatusEnum> statusSet;

    @ApiModelProperty(value = "采购商家id 集合")
    private Set<String> merchantIdSet;

    // 字典处理(自定义sql使用)

    public String getBuySeq() {
        return null == this.buySeq ? null : this.buySeq.getType();
    }

    public String getStatus() {
        return null == this.status ? null : this.status.getType();
    }

    public String getDeliveryType() {
        return null == this.deliveryType ? null : this.deliveryType.getType();
    }

    public String getPayStatus() {
        return null == this.payStatus ? null : this.payStatus.getType();
    }

    public String getStatusSet() {
        return null == this.statusSet || this.statusSet.isEmpty() ? null : this.statusSet.stream().map(BuyOrderStatusEnum::getType).collect(Collectors.joining(StrUtil.COMMA));
    }

    public String getMerchantIdSet() {
        return null == this.merchantIdSet || this.merchantIdSet.isEmpty() ? null : CollUtil.join(this.merchantIdSet, StrUtil.COMMA);
    }

}
