package com.haohan.cloud.scm.api.saleb.req;

import com.haohan.cloud.scm.api.constant.enums.common.DeliveryTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.BuySeqEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author dy
 * @date 2020/4/28
 * SingleGroup 验证 buyOrderSn
 */
@Data
public class QueryBuyOrderReq implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "采购编号不能为空", groups = {SingleGroup.class})
    @Length(max = 64, message = "采购编号长度最大64字符", groups = {Default.class, SingleGroup.class})
    @ApiModelProperty(value = "采购编号")
    private String buyOrderSn;

    @Length(max = 64, message = "采购商长度最大64字符")
    @ApiModelProperty(value = "采购商")
    private String buyerId;

    @ApiModelProperty(value = "送货日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;

    @ApiModelProperty(value = "送货批次")
    private BuySeqEnum buySeq;

    @ApiModelProperty(value = "订单状态")
    private BuyOrderStatusEnum status;

    @ApiModelProperty(value = "配送方式")
    private DeliveryTypeEnum deliveryType;

    @ApiModelProperty(value = "订单支付状态:0未付,1已付,2部分支付")
    private PayStatusEnum payStatus;

    // 非eq

    @Length(max = 64, message = "采购商名称长度最大64字符")
    @ApiModelProperty(value = "采购商名称")
    private String buyerName;

    @Length(max = 64, message = "联系人长度最大64字符")
    @ApiModelProperty(value = "联系人")
    private String contact;

    @Length(max = 64, message = "联系电话长度最大64字符")
    @ApiModelProperty(value = "联系电话")
    private String telephone;

    @Length(max = 500, message = "配送地址长度最大500字符")
    @ApiModelProperty(value = "配送地址")
    private String address;

    @ApiModelProperty(value = "下单日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "下单日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty(value = "订单状态集合")
    private Set<BuyOrderStatusEnum> statusSet;

    // feign使用

    @ApiModelProperty(value = "采购商家名称")
    private String merchantName;

    @ApiModelProperty(value = "采购商家id")
    private String merchantId;

    @ApiModelProperty(value = "采购商家id 集合")
    private Set<String> merchantIdSet;

    public BuyOrder transTo() {
        // eq条件
        BuyOrder order = new BuyOrder();
        order.setBuyId(this.buyOrderSn);
        order.setBuyerId(this.buyerId);
        order.setDeliveryTime(this.deliveryDate);
        order.setBuySeq(this.buySeq);
        order.setStatus(this.status);
        order.setDeliveryType(this.deliveryType);
        return order;
    }

    public BuyOrderSqlDTO transToSql() {
        BuyOrderSqlDTO sqlDTO = new BuyOrderSqlDTO();
        sqlDTO.setBuyId(this.buyOrderSn);
        sqlDTO.setBuyerId(this.buyerId);
        sqlDTO.setDeliveryDate(this.deliveryDate);
        sqlDTO.setBuySeq(this.buySeq);
        sqlDTO.setStatus(this.status);
        sqlDTO.setDeliveryType(this.deliveryType);
        sqlDTO.setPayStatus(this.payStatus);

        sqlDTO.setBuyerName(this.buyerName);
        sqlDTO.setContact(this.contact);
        sqlDTO.setTelephone(this.telephone);
        sqlDTO.setAddress(this.address);
        sqlDTO.setStartDate(this.startDate);
        sqlDTO.setEndDate(this.endDate);
        sqlDTO.setStatusSet(this.statusSet);
        sqlDTO.setMerchantName(this.merchantName);
        sqlDTO.setMerchantId(this.merchantId);
        sqlDTO.setMerchantIdSet(this.merchantIdSet);
        return sqlDTO;
    }
}
