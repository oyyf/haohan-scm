package com.haohan.cloud.scm.api.purchase.resp;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import lombok.Data;

/**
 * @author cx
 * @date 2019/8/5
 */

@Data
public class PurchaseDetailCheckResp extends PurchaseOrderDetail {
    private String transactorName;
}
