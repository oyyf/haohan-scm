package com.haohan.cloud.scm.api.purchase.vo;

import lombok.Data;

/**
 * @author dy
 * @date 2019/5/18
 */
@Data
public class PurchaseEmployeeGoodsVO {

    /**
     * 主键
     */
    private String id;
    /**
     * 员工id
     */
    private String employeeId;
    /**
     * 商品id;spu
     */
    private String goodsId;
    /**
     * 启用状态:0.未启用1.启用
     */
    private String useStatus;


    /**
     * 商品唯一编号
     */
    private String goodsSn;

    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品名称
     */
    private String goodsCategoryName;
    /**
     * 缩略图地址
     */
    private String thumbUrl;
    /**
     * 0:YES 1:NO
     */
    private Integer isMarketable;


}
