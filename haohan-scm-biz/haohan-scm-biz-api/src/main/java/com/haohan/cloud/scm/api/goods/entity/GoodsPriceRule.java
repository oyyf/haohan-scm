/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.goods.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 定价规则/市场价/销售价/VIP价格
 *
 * @author haohan
 * @date 2019-05-13 18:46:24
 */
@Data
@TableName("scm_cms_goods_price_rule")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "定价规则/市场价/销售价/VIP价格")
public class GoodsPriceRule extends Model<GoodsPriceRule> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)
    private String id;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 商家ID
     */
    private String merchantId;
    /**
     * 商品ID
     */
    private String goodsId;
    /**
     * 市场价/销售价  (描述)
     */
    private String ruleDesc;
    /**
     * 批发定价,单位元
     */
    private BigDecimal wholesalePrice;
    /**
     * vip定价,单位元  (会员价)
     */
    private BigDecimal vipPrice;
    /**
     * 零售定价,单位元 (使用,市场价)
     */
    private BigDecimal marketPrice;
    /**
     * 计量单位
     */
    private String unit;
    /**
     * 启用状态 0否1是  (未使用)
     */
    private Integer status;
    /**
     * 虚拟价格
     */
    private BigDecimal virtualPrice;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新者
     */
    @TableField(fill = FieldFill.UPDATE)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", example = "2019-05-21 13:23:12")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 删除标记
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private String delFlag;
    /**
     * 租户id
     */
    private Integer tenantId;

}
