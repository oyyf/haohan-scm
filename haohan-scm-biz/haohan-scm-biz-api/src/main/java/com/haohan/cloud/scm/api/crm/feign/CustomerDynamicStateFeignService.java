/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.crm.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.ScmServiceName;
import com.haohan.cloud.scm.api.crm.entity.CustomerDynamicState;
import com.haohan.cloud.scm.api.crm.req.CustomerDynamicStateReq;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @program: haohan-fresh-scm
 * @description: 客户业务动态内部接口服务
 * @author: Simon
 * @create: 2019-05-28
 **/
@FeignClient(contextId = "CustomerDynamicStateFeignService", value = ScmServiceName.SCM_CRM)
public interface CustomerDynamicStateFeignService {


    /**
     * 通过id查询客户业务动态
     *
     * @param id id
     * @return R
     */
    @RequestMapping(value = "/api/feign/CustomerDynamicState/{id}", method = RequestMethod.GET)
    R<CustomerDynamicState> getById(@PathVariable("id") String id, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 分页查询 客户业务动态 列表信息
     *
     * @param customerDynamicStateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerDynamicState/fetchCustomerDynamicStatePage")
    R<Page<CustomerDynamicState>> getCustomerDynamicStatePage(@RequestBody CustomerDynamicStateReq customerDynamicStateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 全量查询 客户业务动态 列表信息
     *
     * @param customerDynamicStateReq 请求对象
     * @return
     */
    @PostMapping("/api/feign/CustomerDynamicState/fetchCustomerDynamicStateList")
    R<List<CustomerDynamicState>> getCustomerDynamicStateList(@RequestBody CustomerDynamicStateReq customerDynamicStateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @PostMapping("/api/feign/CustomerDynamicState/listByIds")
    R<List<CustomerDynamicState>> listByIds(@RequestBody List<String> idList, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param customerDynamicStateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerDynamicState/countByCustomerDynamicStateReq")
    R<Integer> countByCustomerDynamicStateReq(@RequestBody CustomerDynamicStateReq customerDynamicStateReq, @RequestHeader(SecurityConstants.FROM) String from);


    /**
     * 根据对象条件，查询一条记录
     *
     * @param customerDynamicStateReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/api/feign/CustomerDynamicState/getOneByCustomerDynamicStateReq")
    R<CustomerDynamicState> getOneByCustomerDynamicStateReq(@RequestBody CustomerDynamicStateReq customerDynamicStateReq, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 创建客户采购的动态记录
     *
     * @param customerDynamicStateReq
     * @param from
     * @return
     */
    @PostMapping("/api/feign/CustomerDynamicState/createCustomerBuyDynamic")
    R<Boolean> createCustomerBuyDynamic(@RequestBody CustomerDynamicStateReq customerDynamicStateReq, @RequestHeader(SecurityConstants.FROM) String from);


}
