/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.manage.req;

import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 图片组管理
 *
 * @author haohan
 * @date 2019-05-28 20:37:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "图片组管理")
public class PhotoGroupManageReq extends PhotoGroupManage {

    private long pageSize;
    private long pageNo;




}
