package com.haohan.cloud.scm.api.supply.dto;

import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.SupplyTypeEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/4/28
 * 对 SupplierGoods 扩展
 */
@Data
public class SupplyGoodsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 供应商商家id
     */
    private String supplierMerchantId;
    /**
     * 商品id;spu  (平台)
     */
    private String goodsId;
    /**
     * 商品规格id;sku  (平台)
     */
    private String goodsModelId;
    /**
     * 供应商商品id
     */
    private String supplyGoodsId;
    /**
     * 供应商商品规格id;sku
     */
    private String supplyModelId;
    /**
     * 状态  启用状态 0.未启用 1.启用 2.待审核
     */
    private UseStatusEnum status;
    /**
     * 供应类型:0普通零售1协议零售2协议总价  (scm流程使用)
     */
    private SupplyTypeEnum supplyType;


    // 扩展
    /**
     * 供应商商家名称
     */
    private String merchantName;

}
