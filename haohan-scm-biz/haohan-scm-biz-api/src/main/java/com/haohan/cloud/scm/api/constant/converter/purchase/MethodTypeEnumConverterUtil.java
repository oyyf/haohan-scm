package com.haohan.cloud.scm.api.constant.converter.purchase;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.purchase.MethodTypeEnum;

/**
 * @author xwx
 * @date 2019/6/3
 */
public class MethodTypeEnumConverterUtil implements Converter<MethodTypeEnum> {
    @Override
    public MethodTypeEnum convert(Object o, MethodTypeEnum methodTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return MethodTypeEnum.getByType(o.toString());
    }
}
