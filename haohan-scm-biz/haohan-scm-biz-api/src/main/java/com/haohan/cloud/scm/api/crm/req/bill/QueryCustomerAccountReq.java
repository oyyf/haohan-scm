package com.haohan.cloud.scm.api.crm.req.bill;

import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/1/18
 * FirstGroup 必须时间
 */
@Data
public class QueryCustomerAccountReq {

    @NotBlank(message = "员工ID不能为空")
    @Length(max = 32, message = "员工ID最大长度32字符")
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    @NotBlank(message = "下单客户编号不能为空")
    @Length(max = 32, message = "下单客户编号最大长度32字符")
    @ApiModelProperty(value = "下单客户编号", notes = "客户sn")
    private String customerSn;

    @NotNull(message = "查询开始日期不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "订单成交日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = "查询结束日期不能为空", groups = {FirstGroup.class})
    @ApiModelProperty(value = "订单成交日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;
}
