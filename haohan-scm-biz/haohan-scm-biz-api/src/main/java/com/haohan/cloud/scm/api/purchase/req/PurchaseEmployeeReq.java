/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.api.purchase.req;

import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 采购员工管理
 *
 * @author haohan
 * @date 2019-05-29 13:34:34
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "采购员工管理")
public class PurchaseEmployeeReq extends PurchaseEmployee {

    private long pageSize;
    private long pageNo;




}
