package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author dy
 * @date 2020/1/18
 */
@Data
@NoArgsConstructor
@ApiModel("客户销量上报按月统计")
public class CustomerSalesReportCountVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "每月客户销量上报统计")
    private List<CustomerSalesReportVO> records;

    @ApiModelProperty(value = "统计商品分类名称")
    private List<String> labelList;


}
