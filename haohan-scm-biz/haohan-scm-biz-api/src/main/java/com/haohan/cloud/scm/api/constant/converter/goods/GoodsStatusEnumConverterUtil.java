package com.haohan.cloud.scm.api.constant.converter.goods;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.goods.GoodsStatusEnum;

/**
 * @author dy
 * @date 2019/10/8
 */
public class GoodsStatusEnumConverterUtil implements Converter<GoodsStatusEnum> {
    @Override
    public GoodsStatusEnum convert(Object o, GoodsStatusEnum e) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return GoodsStatusEnum.getByType(o.toString());
    }
}
