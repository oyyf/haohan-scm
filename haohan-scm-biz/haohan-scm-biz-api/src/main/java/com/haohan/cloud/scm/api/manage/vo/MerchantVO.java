package com.haohan.cloud.scm.api.manage.vo;

import com.haohan.cloud.scm.api.constant.enums.manage.MerchantPdsTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.manage.MerchantStatusEnum;
import com.haohan.cloud.scm.api.manage.entity.Merchant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author dy
 * @date 2020/3/25
 */
@Data
@NoArgsConstructor
public class MerchantVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商家id")
    private String merchantId;

    @ApiModelProperty(value = "商家名称")
    private String merchantName;

    @ApiModelProperty(value = "商家地址")
    private String address;

    @ApiModelProperty(value = "商家联系人")
    private String contact;

    @ApiModelProperty(value = "商家电话")
    private String telephone;

    @ApiModelProperty(value = "所属行业")
    private String industry;

    @ApiModelProperty(value = "规模")
    private String scale;

    @ApiModelProperty(value = "业务介绍")
    private String bizDesc;

    @ApiModelProperty(value = "商家启用状态", notes = "原系统使用字典 2启用  0 待审核 -1 停用")
    private MerchantStatusEnum status;

    @ApiModelProperty(value = "商家类型", notes = "商家的采购配送类型:0 普通商家  1 平台商家")
    private MerchantPdsTypeEnum pdsType;

    @ApiModelProperty(value = "备注")
    private String remarks;

    public MerchantVO(Merchant merchant) {
        this.merchantId = merchant.getId();
        this.merchantName = merchant.getMerchantName();
        this.address = merchant.getAddress();
        this.contact = merchant.getContact();
        this.telephone = merchant.getTelephone();
        this.industry = merchant.getIndustry();
        this.scale = merchant.getScale();
        this.bizDesc = merchant.getBizDesc();
        this.status = merchant.getStatus();
        this.pdsType = merchant.getPdsType();
        this.remarks = merchant.getRemarks();
    }
}
