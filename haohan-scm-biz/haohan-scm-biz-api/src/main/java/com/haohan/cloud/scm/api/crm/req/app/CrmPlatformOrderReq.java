package com.haohan.cloud.scm.api.crm.req.app;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.bill.OrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderFeignReq;
import com.haohan.cloud.scm.api.saleb.trans.SalebTrans;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.time.LocalDate;

/**
 * @author dy
 * @date 2020/5/16
 * 查询平台订单
 * SingleGroup 查询详情sn
 */
@Data
public class CrmPlatformOrderReq {


    @NotBlank(message = "员工ID不能为空", groups = {SingleGroup.class, Default.class})
    @Length(max = 32, message = "员工ID最大长度32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "app登陆员工")
    private String employeeId;

    // eq条件

    @NotBlank(message = "订单编号不能为空", groups = {SingleGroup.class})
    @Length(max = 32, message = "订单编号的最大长度为32字符", groups = {SingleGroup.class, Default.class})
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @Length(max = 32, message = "下单客户编号最大长度32字符")
    @ApiModelProperty(value = "下单客户编号", notes = "客户sn")
    private String customerSn;

    @ApiModelProperty(value = "订单状态", notes = "订单状态： 1.已下单、2.待确认、3.成交、4.取消")
    private OrderStatusEnum orderStatus;

    // 非eq查询条件

    @ApiModelProperty(value = "客户商家名称")
    private String merchantName;

    @ApiModelProperty(value = "客户联系人名称")
    private String contact;

    @ApiModelProperty(value = "客户联系电话")
    private String telephone;

    @ApiModelProperty(value = "客户地址")
    private String address;

    @ApiModelProperty(value = "下单日期-开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "下单日期-结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;


    public BuyOrderFeignReq transToFeign(Page page) {
        BuyOrderFeignReq query = new BuyOrderFeignReq();
        query.setCurrent(page.getCurrent());
        query.setSize(page.getSize());
        query.setStartDate(this.startDate);
        query.setEndDate(this.endDate);

        query.setMerchantName(this.merchantName);
        query.setContact(this.contact);
        query.setTelephone(this.telephone);
        query.setAddress(this.address);

        // eq 参数 客户编号需单独处理
        query.setBuyOrderSn(this.orderSn);
        // 订单状态
        if (this.orderStatus == OrderStatusEnum.wait) {
            query.setStatusSet(SalebTrans.waitStatus());
        } else if(null != this.orderStatus ){
            query.setStatus(SalebTrans.statusTrans(this.orderStatus));
        }
        return query;
    }
}
