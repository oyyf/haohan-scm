package com.haohan.cloud.scm.api.message.vo;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.haohan.cloud.scm.api.constant.enums.message.DepartmentTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MessageTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.message.MsgActionTypeEnum;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author dy
 * @date 2020/1/14
 */
@Data
@NoArgsConstructor
public class MsgInfoVO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 消息编号
     */
    private String messageSn;
    /**
     * 消息标题(消息简述)
     */
    private String title;

    /**
     * 消息类型:1微信2站内信3短信
     */
    private MessageTypeEnum messageType;
    /**
     * 业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户
     */
    private DepartmentTypeEnum departmentType;
    /**
     * 业务类型:各部门的消息
     */
    private MsgActionTypeEnum msgActionType;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;
    /**
     * 发送人uid
     */
    private String senderUid;
    /**
     * 发送人名称
     */
    private String senderName;

    // 详情查询时

    /**
     * 消息内容
     */
    private String content;
    /**
     * 业务编号
     */
    private String actionSn;

    /**
     * 请求参数  (json 详情查询 编号)
     */
    private String reqParams;

    public MsgInfoVO(MessageRecord msg) {
        this.messageSn = msg.getMessageSn();
        this.title = msg.getTitle();
        this.messageType = msg.getMessageType();
        this.departmentType = msg.getDepartmentType();
        this.msgActionType = msg.getMsgActionType();
        this.sendTime = msg.getSendTime();
        this.senderUid = msg.getSenderUid();
        this.senderName = msg.getSenderName();
    }

    public MsgInfoVO(InMailRecord msg) {
        this.messageSn = msg.getInMailSn();
        this.title = msg.getTitle();
        this.messageType = MessageTypeEnum.inMail;
        this.departmentType = msg.getDepartmentType();
        this.msgActionType = msg.getMsgActionType();
        this.sendTime = msg.getSendTime();
        this.senderUid = msg.getSenderUid();
        this.senderName = msg.getSenderName();
        this.content = msg.getContent();
        this.reqParams = HtmlUtil.unescape(msg.getReqParams());
        if (StrUtil.isNotEmpty(this.reqParams)) {
            Object obj = null;
            try {
                JSONObject jsonObj = JSONUtil.parseObj(this.reqParams);
                obj = jsonObj.get("querySn");
            } catch (Exception ignored) {
            }
            this.actionSn = null == obj ? null : obj.toString();
        }
    }

}
