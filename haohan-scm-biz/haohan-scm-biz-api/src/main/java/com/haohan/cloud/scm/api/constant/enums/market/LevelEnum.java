package com.haohan.cloud.scm.api.constant.enums.market;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.haohan.cloud.scm.api.constant.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xwx
 * @date 2019/5/30
 */
@Getter
@AllArgsConstructor
public enum LevelEnum implements IBaseEnum {
    /**
     * 层级:一级-五级
     */
    oneStar("1", "一级") {
        @Override
        public LevelEnum getNext() {
            return twoStar;
        }
    },
    twoStar("2", "二级") {
        @Override
        public LevelEnum getNext() {
            return threeStar;
        }
    },
    threeStar("3", "三级") {
        @Override
        public LevelEnum getNext() {
            return fourStar;
        }
    },
    fourStar("4", "四级") {
        @Override
        public LevelEnum getNext() {
            return fiveStar;
        }
    },
    fiveStar("5", "五级");

    private static final Map<String, LevelEnum> MAP = new HashMap<>(8);

    static {
        for (LevelEnum e : LevelEnum.values()) {
            MAP.put(e.getType(), e);
        }
    }

    @JsonCreator
    public static LevelEnum getByType(String type) {
        if (MAP.containsKey(type)) {
            return MAP.get(type);
        }
        return null;
    }

    /**
     * 类型
     */
    @EnumValue
    @JsonValue
    private String type;
    /**
     * 描述
     */
    private String desc;

    public LevelEnum getNext() {
        return this;
    }

}
