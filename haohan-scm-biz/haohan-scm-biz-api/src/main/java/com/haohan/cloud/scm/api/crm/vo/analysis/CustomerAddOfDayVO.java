package com.haohan.cloud.scm.api.crm.vo.analysis;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2019/11/6
 */
@Data
@Api("新增客户数日统计")
public class CustomerAddOfDayVO {

    @ApiModelProperty(value = "客户负责人id")
    @Length(max = 32, message = "客户负责人id长度最大32字符")
    private String directorId;

    @ApiModelProperty(value = "查询开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty(value = "查询结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @ApiModelProperty("分析结果列表")
    private List<CustomerCountAnalysisVO> analysisList;

}
