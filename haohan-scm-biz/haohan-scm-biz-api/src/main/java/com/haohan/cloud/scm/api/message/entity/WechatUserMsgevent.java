/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.api.message.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 微信用户消息事件
 *
 * @author haohan
 * @date 2019-05-13 18:34:03
 */
@Data
@TableName("scm_ms_wechat_user_msgevent")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "微信用户消息事件")
public class WechatUserMsgevent extends Model<WechatUserMsgevent> {
private static final long serialVersionUID = 1L;

    /**
   * 主键
   */
    @TableId
    private String id;
    /**
   * 消息ID
   */
    private String msgId;
    /**
   * 授权微信名称
   */
    private String openWxName;
    /**
   * 授权微信ID
   */
    private String openWxId;
    /**
   * 微信ID
   */
    private String wxId;
    /**
   * 微信名称
   */
    private String wxName;
    /**
   * 微信类型
   */
    private String wxType;
    /**
   * 用户通行证ID
   */
    private String passportUid;
    /**
   * 用户ID
   */
    private String openUid;
    /**
   * 用户昵称
   */
    private String nickName;
    /**
   * 用户头像
   */
    private String albumUrl;
    /**
   * 用户openid
   */
    private String openId;
    /**
   * 用户unionid
   */
    private String unionId;
    /**
   * 消息类型
   */
    private String msgType;
    /**
   * 消息名称
   */
    private String msgName;
    /**
   * 消息内容
   */
    private String msgContent;
    /**
   * 发送时间
   */
    private LocalDateTime sendTime;
    /**
   * 完整消息包
   */
    private String fullMsgPkg;
    /**
   * 创建者
   */
    private String createBy;
    /**
   * 创建时间
   */
    private LocalDateTime createDate;
    /**
   * 更新者
   */
    private String updateBy;
    /**
   * 更新时间
   */
    private LocalDateTime updateDate;
    /**
   * 备注信息
   */
    private String remarks;
    /**
   * 删除标记
   */
    private String delFlag;
    /**
   * 租户id
   */
    private Integer tenantId;
  
}
