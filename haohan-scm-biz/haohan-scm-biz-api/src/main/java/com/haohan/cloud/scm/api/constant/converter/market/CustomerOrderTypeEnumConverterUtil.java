package com.haohan.cloud.scm.api.constant.converter.market;

import cn.hutool.core.convert.Converter;
import com.haohan.cloud.scm.api.constant.enums.market.CustomerOrderTypeEnum;

/**
 * @author xwx
 * @date 2019/6/5
 */
public class CustomerOrderTypeEnumConverterUtil implements Converter<CustomerOrderTypeEnum> {
    @Override
    public CustomerOrderTypeEnum convert(Object o, CustomerOrderTypeEnum customerOrderTypeEnum) throws IllegalArgumentException {
        if (null == o) {
            return null;
        }
        return CustomerOrderTypeEnum.getByType(o.toString());
    }
}
