package com.haohan.cloud.scm.api.crm.req.picture;

import com.haohan.cloud.scm.api.constant.ScmCommonConstant;
import com.haohan.cloud.scm.api.crm.entity.PictureRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/29
 */
@Data
public class EditPictureReq {


    @ApiModelProperty(value = "主键")
    @Length(max = 32, message = "主键的最大长度为32字符")
    private String id;

    @NotBlank(message = "客户编号不能为空")
    @ApiModelProperty(value = "客户编号")
    @Length(max = 32, message = "客户编号的最大长度为32字符")
    private String customerSn;

    @ApiModelProperty(value = "拍照员工id")
    @Length(max = 32, message = "拍照员工id的最大长度为32字符")
    private String employeeId;

    @ApiModelProperty(value = "图片组编号")
    @Length(max = 32, message = "图片组编号的最大长度为32字符")
    private String groupNum;

    @ApiModelProperty(value = "地址")
    @Length(max = 64, message = "地址的最大长度为64字符")
    private String address;

    @ApiModelProperty(value = "地址定位 (经度，纬度)")
    @Pattern(regexp = ScmCommonConstant.POSITION_PATTEN, message = "地址定位, 经纬度格式有误, 经度正负180度之间, 纬度正负90度之间, 弧度数表示")
    private String position;

    @Length(max = 255, message = "备注信息的最大长度为255字符")
    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "图片列表")
    private List<String> photoList;


    public PictureRecord transTo() {
        PictureRecord picture = new PictureRecord();
        picture.setId(this.id);
        picture.setCustomerSn(this.getCustomerSn());
        picture.setEmployeeId(this.employeeId);
        picture.setGroupNum(this.groupNum);
        picture.setAddress(this.address);
        picture.setPosition(this.position);
        picture.setRemarks(this.remarks);
        return picture;
    }
}
