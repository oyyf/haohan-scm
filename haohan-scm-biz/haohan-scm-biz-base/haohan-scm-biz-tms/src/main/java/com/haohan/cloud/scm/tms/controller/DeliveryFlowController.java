/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;
import com.haohan.cloud.scm.api.tms.req.DeliveryFlowReq;
import com.haohan.cloud.scm.tms.service.DeliveryFlowService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 物流配送
 *
 * @author haohan
 * @date 2019-06-05 09:26:15
 */
@RestController
@AllArgsConstructor
@RequestMapping("/deliveryflow" )
@Api(value = "deliveryflow", tags = "deliveryflow管理")
public class DeliveryFlowController {

    private final  DeliveryFlowService deliveryFlowService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param deliveryFlow 物流配送
     * @return
     */
    @GetMapping("/page" )
    public R getDeliveryFlowPage(Page page, DeliveryFlow deliveryFlow) {
        return new R<>(deliveryFlowService.page(page, Wrappers.query(deliveryFlow)));
    }


    /**
     * 通过id查询物流配送
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(deliveryFlowService.getById(id));
    }

    /**
     * 新增物流配送
     * @param deliveryFlow 物流配送
     * @return R
     */
    @SysLog("新增物流配送" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('tms_deliveryflow_add')" )
    public R save(@RequestBody DeliveryFlow deliveryFlow) {
        return new R<>(deliveryFlowService.save(deliveryFlow));
    }

    /**
     * 修改物流配送
     * @param deliveryFlow 物流配送
     * @return R
     */
    @SysLog("修改物流配送" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('tms_deliveryflow_edit')" )
    public R updateById(@RequestBody DeliveryFlow deliveryFlow) {
        return new R<>(deliveryFlowService.updateById(deliveryFlow));
    }

    /**
     * 通过id删除物流配送
     * @param id id
     * @return R
     */
    @SysLog("删除物流配送" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('tms_deliveryflow_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(deliveryFlowService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除物流配送")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('tms_deliveryflow_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(deliveryFlowService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询物流配送")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(deliveryFlowService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param deliveryFlowReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询物流配送总记录}")
    @PostMapping("/countByDeliveryFlowReq")
    public R countByDeliveryFlowReq(@RequestBody DeliveryFlowReq deliveryFlowReq) {

        return new R<>(deliveryFlowService.count(Wrappers.query(deliveryFlowReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param deliveryFlowReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据deliveryFlowReq查询一条货位信息表")
    @PostMapping("/getOneByDeliveryFlowReq")
    public R getOneByDeliveryFlowReq(@RequestBody DeliveryFlowReq deliveryFlowReq) {

        return new R<>(deliveryFlowService.getOne(Wrappers.query(deliveryFlowReq), false));
    }


    /**
     * 批量修改OR插入
     * @param deliveryFlowList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('tms_deliveryflow_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<DeliveryFlow> deliveryFlowList) {

        return new R<>(deliveryFlowService.saveOrUpdateBatch(deliveryFlowList));
    }


}
