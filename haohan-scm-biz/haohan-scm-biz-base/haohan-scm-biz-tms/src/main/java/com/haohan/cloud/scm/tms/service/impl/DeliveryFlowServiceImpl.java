/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.tms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;
import com.haohan.cloud.scm.tms.mapper.DeliveryFlowMapper;
import com.haohan.cloud.scm.tms.service.DeliveryFlowService;
import org.springframework.stereotype.Service;

/**
 * 物流配送
 *
 * @author haohan
 * @date 2019-06-05 09:26:15
 */
@Service
public class DeliveryFlowServiceImpl extends ServiceImpl<DeliveryFlowMapper, DeliveryFlow> implements DeliveryFlowService {

}
