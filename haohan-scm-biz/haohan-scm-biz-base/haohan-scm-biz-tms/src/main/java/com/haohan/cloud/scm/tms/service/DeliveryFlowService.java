/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;

/**
 * 物流配送
 *
 * @author haohan
 * @date 2019-06-05 09:26:15
 */
public interface DeliveryFlowService extends IService<DeliveryFlow> {

}
