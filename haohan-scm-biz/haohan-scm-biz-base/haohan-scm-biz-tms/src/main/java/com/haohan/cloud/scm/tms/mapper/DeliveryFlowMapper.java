/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.tms.entity.DeliveryFlow;


/**
 * 物流配送
 *
 * @author haohan
 * @date 2019-06-05 09:26:15
 */
public interface DeliveryFlowMapper extends BaseMapper<DeliveryFlow> {

}
