/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.tms.entity.LogisticsEmployee;
import com.haohan.cloud.scm.api.tms.req.LogisticsEmployeeReq;
import com.haohan.cloud.scm.tms.service.LogisticsEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 物流部员工管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/LogisticsEmployee")
@Api(value = "logisticsemployee", tags = "logisticsemployee内部接口服务")
public class LogisticsEmployeeFeignApiCtrl {

    private final LogisticsEmployeeService logisticsEmployeeService;


    /**
     * 通过id查询物流部员工管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(logisticsEmployeeService.getById(id));
    }


    /**
     * 分页查询 物流部员工管理 列表信息
     * @param logisticsEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchLogisticsEmployeePage")
    public R getLogisticsEmployeePage(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq) {
        Page page = new Page(logisticsEmployeeReq.getPageNo(), logisticsEmployeeReq.getPageSize());
        LogisticsEmployee logisticsEmployee =new LogisticsEmployee();
        BeanUtil.copyProperties(logisticsEmployeeReq, logisticsEmployee);

        return new R<>(logisticsEmployeeService.page(page, Wrappers.query(logisticsEmployee)));
    }


    /**
     * 全量查询 物流部员工管理 列表信息
     * @param logisticsEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchLogisticsEmployeeList")
    public R getLogisticsEmployeeList(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq) {
        LogisticsEmployee logisticsEmployee =new LogisticsEmployee();
        BeanUtil.copyProperties(logisticsEmployeeReq, logisticsEmployee);

        return new R<>(logisticsEmployeeService.list(Wrappers.query(logisticsEmployee)));
    }


    /**
     * 新增物流部员工管理
     * @param logisticsEmployee 物流部员工管理
     * @return R
     */
    @Inner
    @SysLog("新增物流部员工管理")
    @PostMapping("/add")
    public R save(@RequestBody LogisticsEmployee logisticsEmployee) {
        return new R<>(logisticsEmployeeService.save(logisticsEmployee));
    }

    /**
     * 修改物流部员工管理
     * @param logisticsEmployee 物流部员工管理
     * @return R
     */
    @Inner
    @SysLog("修改物流部员工管理")
    @PostMapping("/update")
    public R updateById(@RequestBody LogisticsEmployee logisticsEmployee) {
        return new R<>(logisticsEmployeeService.updateById(logisticsEmployee));
    }

    /**
     * 通过id删除物流部员工管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除物流部员工管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(logisticsEmployeeService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除物流部员工管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(logisticsEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询物流部员工管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(logisticsEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param logisticsEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询物流部员工管理总记录}")
    @PostMapping("/countByLogisticsEmployeeReq")
    public R countByLogisticsEmployeeReq(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq) {

        return new R<>(logisticsEmployeeService.count(Wrappers.query(logisticsEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param logisticsEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据logisticsEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByLogisticsEmployeeReq")
    public R getOneByLogisticsEmployeeReq(@RequestBody LogisticsEmployeeReq logisticsEmployeeReq) {

        return new R<>(logisticsEmployeeService.getOne(Wrappers.query(logisticsEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param logisticsEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<LogisticsEmployee> logisticsEmployeeList) {

        return new R<>(logisticsEmployeeService.saveOrUpdateBatch(logisticsEmployeeList));
    }

}
