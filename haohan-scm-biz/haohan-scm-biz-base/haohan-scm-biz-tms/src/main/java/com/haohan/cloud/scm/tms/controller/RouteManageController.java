/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.tms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.tms.entity.RouteManage;
import com.haohan.cloud.scm.api.tms.req.RouteManageReq;
import com.haohan.cloud.scm.tms.service.RouteManageService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 路线管理
 *
 * @author haohan
 * @date 2019-06-05 09:25:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/routemanage" )
@Api(value = "routemanage", tags = "routemanage管理")
public class RouteManageController {

    private final  RouteManageService routeManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param routeManage 路线管理
     * @return
     */
    @GetMapping("/page" )
    public R getRouteManagePage(Page page, RouteManage routeManage) {
        return new R<>(routeManageService.page(page, Wrappers.query(routeManage)));
    }


    /**
     * 通过id查询路线管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(routeManageService.getById(id));
    }

    /**
     * 新增路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @SysLog("新增路线管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('tms_routemanage_add')" )
    public R save(@RequestBody RouteManage routeManage) {
        return new R<>(routeManageService.save(routeManage));
    }

    /**
     * 修改路线管理
     * @param routeManage 路线管理
     * @return R
     */
    @SysLog("修改路线管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('tms_routemanage_edit')" )
    public R updateById(@RequestBody RouteManage routeManage) {
        return new R<>(routeManageService.updateById(routeManage));
    }

    /**
     * 通过id删除路线管理
     * @param id id
     * @return R
     */
    @SysLog("删除路线管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('tms_routemanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(routeManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除路线管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('tms_routemanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(routeManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询路线管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(routeManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询路线管理总记录}")
    @PostMapping("/countByRouteManageReq")
    public R countByRouteManageReq(@RequestBody RouteManageReq routeManageReq) {

        return new R<>(routeManageService.count(Wrappers.query(routeManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param routeManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据routeManageReq查询一条货位信息表")
    @PostMapping("/getOneByRouteManageReq")
    public R getOneByRouteManageReq(@RequestBody RouteManageReq routeManageReq) {

        return new R<>(routeManageService.getOne(Wrappers.query(routeManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param routeManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('tms_routemanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<RouteManage> routeManageList) {

        return new R<>(routeManageService.saveOrUpdateBatch(routeManageList));
    }


}
