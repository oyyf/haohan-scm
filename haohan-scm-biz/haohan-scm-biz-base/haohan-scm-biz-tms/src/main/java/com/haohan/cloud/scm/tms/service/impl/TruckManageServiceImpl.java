/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.tms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.tms.entity.TruckManage;
import com.haohan.cloud.scm.tms.mapper.TruckManageMapper;
import com.haohan.cloud.scm.tms.service.TruckManageService;
import org.springframework.stereotype.Service;

/**
 * 车辆管理
 *
 * @author haohan
 * @date 2019-06-05 09:26:20
 */
@Service
public class TruckManageServiceImpl extends ServiceImpl<TruckManageMapper, TruckManage> implements TruckManageService {

}
