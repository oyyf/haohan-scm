/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.entity.ProductionEmployee;

/**
 * 生产部员工表
 *
 * @author haohan
 * @date 2019-05-13 18:16:14
 */
public interface ProductionEmployeeService extends IService<ProductionEmployee> {

}
