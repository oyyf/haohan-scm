/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ProductLossRecordMapper;
import com.haohan.cloud.scm.product.service.ProductLossRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-13 18:16:25
 */
@Service
public class ProductLossRecordServiceImpl extends ServiceImpl<ProductLossRecordMapper, ProductLossRecord> implements ProductLossRecordService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Autowired
    @Lazy
    private ProductLossRecordMapper productLossRecordMapper;

    @Override
    public boolean save(ProductLossRecord entity) {
        if (StrUtil.isEmpty(entity.getLossRecordSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ProductLossRecord.class, NumberPrefixConstant.PRODUCT_LOSS_RECORD_SN_PRE);
            entity.setLossRecordSn(sn);
        }
        return super.save(entity);
    }

    /**
     * 查询单品损耗率
     * @param dto
     * @return
     */
    @Override
    public BigDecimal queryLossRate(SingleRealLossDTO dto) {
        BigDecimal bigDecimal = productLossRecordMapper.queryLossByGoodsModelId(dto);
        return bigDecimal;
    }
}
