/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;
import com.haohan.cloud.scm.api.product.req.ImperfectProductReq;
import com.haohan.cloud.scm.product.service.ImperfectProductService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 残次货品处理记录
 *
 * @author haohan
 * @date 2019-05-29 14:45:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ImperfectProduct")
@Api(value = "imperfectproduct", tags = "imperfectproduct内部接口服务")
public class ImperfectProductFeignApiCtrl {

    private final ImperfectProductService imperfectProductService;


    /**
     * 通过id查询残次货品处理记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(imperfectProductService.getById(id));
    }


    /**
     * 分页查询 残次货品处理记录 列表信息
     * @param imperfectProductReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchImperfectProductPage")
    public R getImperfectProductPage(@RequestBody ImperfectProductReq imperfectProductReq) {
        Page page = new Page(imperfectProductReq.getPageNo(), imperfectProductReq.getPageSize());
        ImperfectProduct imperfectProduct =new ImperfectProduct();
        BeanUtil.copyProperties(imperfectProductReq, imperfectProduct);

        return new R<>(imperfectProductService.page(page, Wrappers.query(imperfectProduct)));
    }


    /**
     * 全量查询 残次货品处理记录 列表信息
     * @param imperfectProductReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchImperfectProductList")
    public R getImperfectProductList(@RequestBody ImperfectProductReq imperfectProductReq) {
        ImperfectProduct imperfectProduct =new ImperfectProduct();
        BeanUtil.copyProperties(imperfectProductReq, imperfectProduct);

        return new R<>(imperfectProductService.list(Wrappers.query(imperfectProduct)));
    }


    /**
     * 新增残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @Inner
    @SysLog("新增残次货品处理记录")
    @PostMapping("/add")
    public R save(@RequestBody ImperfectProduct imperfectProduct) {
        return new R<>(imperfectProductService.save(imperfectProduct));
    }

    /**
     * 修改残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @Inner
    @SysLog("修改残次货品处理记录")
    @PostMapping("/update")
    public R updateById(@RequestBody ImperfectProduct imperfectProduct) {
        return new R<>(imperfectProductService.updateById(imperfectProduct));
    }

    /**
     * 通过id删除残次货品处理记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除残次货品处理记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(imperfectProductService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除残次货品处理记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(imperfectProductService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询残次货品处理记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(imperfectProductService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询残次货品处理记录总记录}")
    @PostMapping("/countByImperfectProductReq")
    public R countByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq) {

        return new R<>(imperfectProductService.count(Wrappers.query(imperfectProductReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据imperfectProductReq查询一条货位信息表")
    @PostMapping("/getOneByImperfectProductReq")
    public R getOneByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq) {

        return new R<>(imperfectProductService.getOne(Wrappers.query(imperfectProductReq), false));
    }


    /**
     * 批量修改OR插入
     * @param imperfectProductList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ImperfectProduct> imperfectProductList) {

        return new R<>(imperfectProductService.saveOrUpdateBatch(imperfectProductList));
    }

}
