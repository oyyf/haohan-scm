package com.haohan.cloud.scm.product.api.ctrl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.SortingBatchReq;
import com.haohan.cloud.scm.api.product.req.SortingFinishReq;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.product.core.IProductSortingOrderService;
import com.haohan.cloud.scm.product.core.ScmSortingOrderService;
import com.haohan.cloud.scm.product.service.SortingOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xwx
 * @date 2019/7/18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/sortingOrder")
@Api(value = "ApiSortingOrder", tags = "SortingOrder分拣单")
public class SortingOrderApiCtrl {
    private final ScmSortingOrderService scmSortingOrderService;
    private final IProductSortingOrderService productSortingOrderService;
    private final SortingOrderService sortingOrderService;

    /**
     * 分拣单操作 - 一键分拣
     *
     * @param req
     * @return
     */
    @PostMapping("/sortingBatch")
    @ApiOperation(value = "分拣单操作 - 一键分拣")
    public R<Boolean> sortingBatch(@Validated SortingBatchReq req) {
        return R.ok(scmSortingOrderService.sortingBatch(req));
    }

    /**
     * 分拣单批量确认完成
     *
     * @param req
     * @return
     */
    @PostMapping("/finishBatch")
    @ApiOperation(value = "分拣单批量确认完成")
    public R finishBatch(@Validated SortingBatchReq req) {
        QueryWrapper<SortingOrder> query = new QueryWrapper<>();
        query.lambda()
                .eq(SortingOrder::getPmId, req.getPmId())
                .eq(SortingOrder::getDeliverySeq, req.getDeliverySeq())
                .eq(SortingOrder::getDeliveryDate, req.getDeliveryDate());
        List<SortingOrder> orderList = sortingOrderService.list(query);
        if (CollUtil.isEmpty(orderList)) {
            return R.failed("当前时间批次无分拣单");
        }
        int success = 0;
        int failed = 0;
        for (SortingOrder sortingOrder : orderList) {
            try {
                productSortingOrderService.confirmSortingFinish(sortingOrder);
                success++;
            } catch (Exception e) {
                failed++;
            }
        }
        if (success == 0) {
            return R.failed("当前时间批次无可完成的分拣单");
        }
        return R.ok("当前时间批次确认完成分拣单数:" + success + ", 未完成分拣单数:" + failed);
    }

    /**
     * 单个 分拣单 确认完成
     *
     * @param req
     * @return
     */
    @PostMapping("/finish")
    @ApiOperation(value = "单个分拣单确认完成 ")
    public R finish(@Validated SortingFinishReq req) {
        try {
            QueryWrapper<SortingOrder> query = new QueryWrapper<>();
            query.lambda()
                    .eq(SortingOrder::getPmId, req.getPmId())
                    .eq(SortingOrder::getSortingOrderSn, req.getSortingOrderSn());
            SortingOrder sortingOrder = sortingOrderService.getOne(query);
            if (null == sortingOrder) {
                throw new ErrorDataException("找不到该分拣单");
            }
            productSortingOrderService.confirmSortingFinish(sortingOrder);
            return R.ok(true);
        } catch (Exception e) {
            return R.failed(e.getMessage());
        }
    }

}
