/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.RecipeDetail;
import com.haohan.cloud.scm.api.product.req.RecipeDetailReq;
import com.haohan.cloud.scm.product.service.RecipeDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品加工配方明细表
 *
 * @author haohan
 * @date 2019-05-29 14:46:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/recipedetail" )
@Api(value = "recipedetail", tags = "recipedetail管理")
public class RecipeDetailController {

    private final RecipeDetailService recipeDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param recipeDetail 商品加工配方明细表
     * @return
     */
    @GetMapping("/page" )
    public R getRecipeDetailPage(Page page, RecipeDetail recipeDetail) {
        return new R<>(recipeDetailService.page(page, Wrappers.query(recipeDetail)));
    }


    /**
     * 通过id查询商品加工配方明细表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(recipeDetailService.getById(id));
    }

    /**
     * 新增商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @SysLog("新增商品加工配方明细表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_recipedetail_add')" )
    public R save(@RequestBody RecipeDetail recipeDetail) {
        return new R<>(recipeDetailService.save(recipeDetail));
    }

    /**
     * 修改商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @SysLog("修改商品加工配方明细表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_recipedetail_edit')" )
    public R updateById(@RequestBody RecipeDetail recipeDetail) {
        return new R<>(recipeDetailService.updateById(recipeDetail));
    }

    /**
     * 通过id删除商品加工配方明细表
     * @param id id
     * @return R
     */
    @SysLog("删除商品加工配方明细表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_recipedetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(recipeDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品加工配方明细表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_recipedetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(recipeDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品加工配方明细表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(recipeDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品加工配方明细表总记录}")
    @PostMapping("/countByRecipeDetailReq")
    public R countByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq) {

        return new R<>(recipeDetailService.count(Wrappers.query(recipeDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据recipeDetailReq查询一条货位信息表")
    @PostMapping("/getOneByRecipeDetailReq")
    public R getOneByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq) {

        return new R<>(recipeDetailService.getOne(Wrappers.query(recipeDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param recipeDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_recipedetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<RecipeDetail> recipeDetailList) {

        return new R<>(recipeDetailService.saveOrUpdateBatch(recipeDetailList));
    }


}
