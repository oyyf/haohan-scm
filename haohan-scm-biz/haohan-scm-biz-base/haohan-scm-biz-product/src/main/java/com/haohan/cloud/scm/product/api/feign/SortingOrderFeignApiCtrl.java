/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.SortingOrderReq;
import com.haohan.cloud.scm.product.service.SortingOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-29 14:47:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SortingOrder")
@Api(value = "sortingorder", tags = "sortingorder内部接口服务")
public class SortingOrderFeignApiCtrl {

    private final SortingOrderService sortingOrderService;


    /**
     * 通过id查询分拣单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(sortingOrderService.getById(id));
    }


    /**
     * 分页查询 分拣单 列表信息
     * @param sortingOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSortingOrderPage")
    public R getSortingOrderPage(@RequestBody SortingOrderReq sortingOrderReq) {
        Page page = new Page(sortingOrderReq.getPageNo(), sortingOrderReq.getPageSize());
        SortingOrder sortingOrder =new SortingOrder();
        BeanUtil.copyProperties(sortingOrderReq, sortingOrder);

        return new R<>(sortingOrderService.page(page, Wrappers.query(sortingOrder)));
    }


    /**
     * 全量查询 分拣单 列表信息
     * @param sortingOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSortingOrderList")
    public R getSortingOrderList(@RequestBody SortingOrderReq sortingOrderReq) {
        SortingOrder sortingOrder =new SortingOrder();
        BeanUtil.copyProperties(sortingOrderReq, sortingOrder);

        return new R<>(sortingOrderService.list(Wrappers.query(sortingOrder)));
    }


    /**
     * 新增分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @Inner
    @SysLog("新增分拣单")
    @PostMapping("/add")
    public R save(@RequestBody SortingOrder sortingOrder) {
        return new R<>(sortingOrderService.save(sortingOrder));
    }

    /**
     * 修改分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @Inner
    @SysLog("修改分拣单")
    @PostMapping("/update")
    public R updateById(@RequestBody SortingOrder sortingOrder) {
        return new R<>(sortingOrderService.updateById(sortingOrder));
    }

    /**
     * 通过id删除分拣单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除分拣单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(sortingOrderService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除分拣单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询分拣单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询分拣单总记录}")
    @PostMapping("/countBySortingOrderReq")
    public R countBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq) {

        return new R<>(sortingOrderService.count(Wrappers.query(sortingOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据sortingOrderReq查询一条货位信息表")
    @PostMapping("/getOneBySortingOrderReq")
    public R getOneBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq) {

        return new R<>(sortingOrderService.getOne(Wrappers.query(sortingOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param sortingOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SortingOrder> sortingOrderList) {

        return new R<>(sortingOrderService.saveOrUpdateBatch(sortingOrderList));
    }

}
