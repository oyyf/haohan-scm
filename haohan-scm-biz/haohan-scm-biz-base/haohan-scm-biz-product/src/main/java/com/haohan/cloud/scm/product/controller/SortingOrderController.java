/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.SortingOrderReq;
import com.haohan.cloud.scm.product.service.SortingOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-29 14:47:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sortingorder" )
@Api(value = "sortingorder", tags = "sortingorder管理")
public class SortingOrderController {

    private final SortingOrderService sortingOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sortingOrder 分拣单
     * @return
     */
    @GetMapping("/page" )
    public R getSortingOrderPage(Page page, SortingOrder sortingOrder) {
        return new R<>(sortingOrderService.page(page, Wrappers.query(sortingOrder)));
    }


    /**
     * 通过id查询分拣单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(sortingOrderService.getById(id));
    }

    /**
     * 新增分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @SysLog("新增分拣单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_sortingorder_add')" )
    public R save(@RequestBody SortingOrder sortingOrder) {
        return new R<>(sortingOrderService.save(sortingOrder));
    }

    /**
     * 修改分拣单
     * @param sortingOrder 分拣单
     * @return R
     */
    @SysLog("修改分拣单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_sortingorder_edit')" )
    public R updateById(@RequestBody SortingOrder sortingOrder) {
        return new R<>(sortingOrderService.updateById(sortingOrder));
    }

    /**
     * 通过id删除分拣单
     * @param id id
     * @return R
     */
    @SysLog("删除分拣单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_sortingorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(sortingOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除分拣单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_sortingorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询分拣单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询分拣单总记录}")
    @PostMapping("/countBySortingOrderReq")
    public R countBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq) {

        return new R<>(sortingOrderService.count(Wrappers.query(sortingOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param sortingOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据sortingOrderReq查询一条货位信息表")
    @PostMapping("/getOneBySortingOrderReq")
    public R getOneBySortingOrderReq(@RequestBody SortingOrderReq sortingOrderReq) {

        return new R<>(sortingOrderService.getOne(Wrappers.query(sortingOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param sortingOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_sortingorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SortingOrder> sortingOrderList) {

        return new R<>(sortingOrderService.saveOrUpdateBatch(sortingOrderList));
    }


}
