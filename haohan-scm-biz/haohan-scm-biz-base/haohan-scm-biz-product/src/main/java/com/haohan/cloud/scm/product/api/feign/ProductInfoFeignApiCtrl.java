/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.dto.ProcessingResultDTO;
import com.haohan.cloud.scm.api.product.dto.ProcessingSourceDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.CreateSortingOrderReq;
import com.haohan.cloud.scm.api.product.req.ProductInfoReq;
import com.haohan.cloud.scm.api.product.req.ProductMatchSortingReq;
import com.haohan.cloud.scm.api.product.req.QueryGoodsStorageReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryOrderDetailReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.product.core.IProductInfoService;
import com.haohan.cloud.scm.product.core.IProductSortingOrderService;
import com.haohan.cloud.scm.product.service.ProductInfoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货品信息记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductInfo")
@Api(value = "productinfo", tags = "productinfo内部接口服务")
public class ProductInfoFeignApiCtrl {

    private final ProductInfoService productInfoService;
    private final IProductInfoService iProductInfoService;
    private final IProductSortingOrderService iProductSortingOrderService;


    /**
     * 通过id查询货品信息记录表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productInfoService.getById(id));
    }


    /**
     * 分页查询 货品信息记录表 列表信息
     * @param productInfoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductInfoPage")
    public R getProductInfoPage(@RequestBody ProductInfoReq productInfoReq) {
        Page page = new Page(productInfoReq.getPageNo(), productInfoReq.getPageSize());
        ProductInfo productInfo =new ProductInfo();
        BeanUtil.copyProperties(productInfoReq, productInfo);

        return new R<>(productInfoService.page(page, Wrappers.query(productInfo)));
    }


    /**
     * 全量查询 货品信息记录表 列表信息 按货品数量 降序
     * @param productInfoReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductInfoList")
    public R getProductInfoList(@RequestBody ProductInfoReq productInfoReq) {
        ProductInfo productInfo =new ProductInfo();
        BeanUtil.copyProperties(productInfoReq, productInfo);
        // 按货品数量 降序
        return new R<>(productInfoService.list(Wrappers.query(productInfo).orderByDesc("product_number")));
    }


    /**
     * 新增货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @Inner
    @SysLog("新增货品信息记录表")
    @PostMapping("/add")
    public R save(@RequestBody ProductInfo productInfo) {
        return new R<>(productInfoService.save(productInfo));
    }

    /**
     * 修改货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @Inner
    @SysLog("修改货品信息记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductInfo productInfo) {
        return new R<>(productInfoService.updateById(productInfo));
    }

    /**
     * 通过id删除货品信息记录表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货品信息记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productInfoService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货品信息记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productInfoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货品信息记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productInfoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货品信息记录表总记录}")
    @PostMapping("/countByProductInfoReq")
    public R countByProductInfoReq(@RequestBody ProductInfoReq productInfoReq) {

        return new R<>(productInfoService.count(Wrappers.query(productInfoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productInfoReq查询一条货位信息表")
    @PostMapping("/getOneByProductInfoReq")
    public R getOneByProductInfoReq(@RequestBody ProductInfoReq productInfoReq) {

        return new R<>(productInfoService.getOne(Wrappers.query(productInfoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductInfo> productInfoList) {

        return new R<>(productInfoService.saveOrUpdateBatch(productInfoList));
    }

    /**
     * 货品报损/去皮 (拆分为2个 状态为：一个正常 一个损耗)
     * 货品未保存
     * @param sourceDTO
     *          必须参数: sourceProductSn / subProductNum
     * @return ProcessingResultDTO
     *          返回结果: subProduct / lossProduct / sourceProduct
     */
    @Inner
    @SysLog("将货品拆分为两个新货品")
    @PostMapping("/productPeeling")
    public R<ProcessingResultDTO> productPeeling(@RequestBody ProcessingSourceDTO sourceDTO) {
        ProcessingResultDTO resultDTO;
        try {
            resultDTO = iProductInfoService.productPeeling(sourceDTO);
        }catch (Exception e){
            R r = RUtil.error();
            r.setMsg("货品拆分失败");
            return r;
        }
        return new R<>(resultDTO);
    }

    /**
     * 查询商品库存
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryGoodsStorage")
    public R<ProductInfo> queryGoodsStorage(@RequestBody QueryGoodsStorageReq req){
        return new R(iProductInfoService.queryGoodsStorage(req));
    }

    /**
     * 创建货品信息 根据采购单明细
     * @param req
     * @return
     */
    @Inner
    @SysLog("创建货品信息 根据采购单明细")
    @PostMapping("/addProductInfo")
    public R<ProductInfo> addProductInfo(@RequestBody PurchaseQueryOrderDetailReq req) {
        ProductInfo productInfo = iProductInfoService.addProductInfo(req);
        return new R<>(productInfo);
    }

    /**
     * 新增货品分拣单  根据B客户订单
     * @param req
     * @return
     */
    @Inner
    @SysLog("新增货品分拣单 根据B客户订单")
    @PostMapping("/createSortingOrder")
    public R<SortingOrder> createSortingOrder(@RequestBody CreateSortingOrderReq req){
        return new R<>(iProductSortingOrderService.createSortingOrder(req));
    }

    /**
     * 根据 采购入库货品 匹配分拣
     * @param req pmId / realBuyNum / purchaseDetailSn
     * @return 返回修改后的货品信息
     */
    @Inner
    @SysLog("根据采购入库货品 匹配分拣")
    @PostMapping("/enterProductMatchSorting")
    public R<ProductInfo> enterProductMatchSorting(@RequestBody ProductMatchSortingReq req) {
        return new R<>(iProductInfoService.enterProductMatchSorting(req));
    }

}
