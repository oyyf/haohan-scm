/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.SortingOrderDetailReq;
import com.haohan.cloud.scm.product.service.SortingOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-29 14:46:59
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SortingOrderDetail")
@Api(value = "sortingorderdetail", tags = "sortingorderdetail内部接口服务")
public class SortingOrderDetailFeignApiCtrl {

    private final SortingOrderDetailService sortingOrderDetailService;


    /**
     * 通过id查询分拣单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(sortingOrderDetailService.getById(id));
    }


    /**
     * 分页查询 分拣单明细 列表信息
     * @param sortingOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSortingOrderDetailPage")
    public R getSortingOrderDetailPage(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {
        Page page = new Page(sortingOrderDetailReq.getPageNo(), sortingOrderDetailReq.getPageSize());
        SortingOrderDetail sortingOrderDetail =new SortingOrderDetail();
        BeanUtil.copyProperties(sortingOrderDetailReq, sortingOrderDetail);

        return new R<>(sortingOrderDetailService.page(page, Wrappers.query(sortingOrderDetail)));
    }


    /**
     * 全量查询 分拣单明细 列表信息
     * @param sortingOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSortingOrderDetailList")
    public R getSortingOrderDetailList(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {
        SortingOrderDetail sortingOrderDetail =new SortingOrderDetail();
        BeanUtil.copyProperties(sortingOrderDetailReq, sortingOrderDetail);

        return new R<>(sortingOrderDetailService.list(Wrappers.query(sortingOrderDetail)));
    }


    /**
     * 新增分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @Inner
    @SysLog("新增分拣单明细")
    @PostMapping("/add")
    public R save(@RequestBody SortingOrderDetail sortingOrderDetail) {
        return new R<>(sortingOrderDetailService.save(sortingOrderDetail));
    }

    /**
     * 修改分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @Inner
    @SysLog("修改分拣单明细")
    @PostMapping("/update")
    public R updateById(@RequestBody SortingOrderDetail sortingOrderDetail) {
        return new R<>(sortingOrderDetailService.updateById(sortingOrderDetail));
    }

    /**
     * 通过id删除分拣单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除分拣单明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(sortingOrderDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除分拣单明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询分拣单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询分拣单明细总记录}")
    @PostMapping("/countBySortingOrderDetailReq")
    public R countBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {

        return new R<>(sortingOrderDetailService.count(Wrappers.query(sortingOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据sortingOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneBySortingOrderDetailReq")
    public R getOneBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {

        return new R<>(sortingOrderDetailService.getOne(Wrappers.query(sortingOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param sortingOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SortingOrderDetail> sortingOrderDetailList) {

        return new R<>(sortingOrderDetailService.saveOrUpdateBatch(sortingOrderDetailList));
    }

    /**
     * 修改分拣单明细 根据B订单明细编号
     * @param sortingOrderDetail 必须 pmId/ BuyDetailSn
     * @return R
     */
    @Inner
    @SysLog("修改分拣单明细 根据B订单明细编号")
    @PostMapping("/modifyBySn")
    public R<Boolean> modifySortingDetailBySn(@RequestBody SortingOrderDetail sortingOrderDetail) {
        UpdateWrapper<SortingOrderDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda()
                .eq(SortingOrderDetail::getPmId, sortingOrderDetail.getPmId())
                .eq(SortingOrderDetail::getBuyDetailSn, sortingOrderDetail.getBuyDetailSn());
        sortingOrderDetail.setPmId(null);
        sortingOrderDetail.setBuyDetailSn(null);
        sortingOrderDetail.setTenantId(null);
        return new R<>(sortingOrderDetailService.update(sortingOrderDetail, updateWrapper));
    }

}
