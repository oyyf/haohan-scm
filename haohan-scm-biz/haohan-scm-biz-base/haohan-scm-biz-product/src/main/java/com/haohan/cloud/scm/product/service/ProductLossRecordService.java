/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;

import java.math.BigDecimal;

/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-13 18:16:25
 */
public interface ProductLossRecordService extends IService<ProductLossRecord> {

    /**
     * 查询单品损耗率
     * @param dto
     * @return
     */
    BigDecimal queryLossRate(SingleRealLossDTO dto);
}
