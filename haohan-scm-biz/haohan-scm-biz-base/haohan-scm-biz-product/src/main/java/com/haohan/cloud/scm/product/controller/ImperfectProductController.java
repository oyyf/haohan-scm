/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ImperfectProduct;
import com.haohan.cloud.scm.api.product.req.ImperfectProductReq;
import com.haohan.cloud.scm.product.service.ImperfectProductService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 残次货品处理记录
 *
 * @author haohan
 * @date 2019-05-29 14:45:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/imperfectproduct" )
@Api(value = "imperfectproduct", tags = "imperfectproduct管理")
public class ImperfectProductController {

    private final ImperfectProductService imperfectProductService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param imperfectProduct 残次货品处理记录
     * @return
     */
    @GetMapping("/page" )
    public R getImperfectProductPage(Page page, ImperfectProduct imperfectProduct) {
        return new R<>(imperfectProductService.page(page, Wrappers.query(imperfectProduct)));
    }


    /**
     * 通过id查询残次货品处理记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(imperfectProductService.getById(id));
    }

    /**
     * 新增残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @SysLog("新增残次货品处理记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_imperfectproduct_add')" )
    public R save(@RequestBody ImperfectProduct imperfectProduct) {
        return new R<>(imperfectProductService.save(imperfectProduct));
    }

    /**
     * 修改残次货品处理记录
     * @param imperfectProduct 残次货品处理记录
     * @return R
     */
    @SysLog("修改残次货品处理记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_imperfectproduct_edit')" )
    public R updateById(@RequestBody ImperfectProduct imperfectProduct) {
        return new R<>(imperfectProductService.updateById(imperfectProduct));
    }

    /**
     * 通过id删除残次货品处理记录
     * @param id id
     * @return R
     */
    @SysLog("删除残次货品处理记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_imperfectproduct_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(imperfectProductService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除残次货品处理记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_imperfectproduct_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(imperfectProductService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询残次货品处理记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(imperfectProductService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询残次货品处理记录总记录}")
    @PostMapping("/countByImperfectProductReq")
    public R countByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq) {

        return new R<>(imperfectProductService.count(Wrappers.query(imperfectProductReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param imperfectProductReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据imperfectProductReq查询一条货位信息表")
    @PostMapping("/getOneByImperfectProductReq")
    public R getOneByImperfectProductReq(@RequestBody ImperfectProductReq imperfectProductReq) {

        return new R<>(imperfectProductService.getOne(Wrappers.query(imperfectProductReq), false));
    }


    /**
     * 批量修改OR插入
     * @param imperfectProductList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_imperfectproduct_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ImperfectProduct> imperfectProductList) {

        return new R<>(imperfectProductService.saveOrUpdateBatch(imperfectProductList));
    }


}
