/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.RecipeDetail;
import com.haohan.cloud.scm.api.product.req.RecipeDetailReq;
import com.haohan.cloud.scm.product.service.RecipeDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 商品加工配方明细表
 *
 * @author haohan
 * @date 2019-05-29 14:46:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/RecipeDetail")
@Api(value = "recipedetail", tags = "recipedetail内部接口服务")
public class RecipeDetailFeignApiCtrl {

    private final RecipeDetailService recipeDetailService;


    /**
     * 通过id查询商品加工配方明细表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(recipeDetailService.getById(id));
    }


    /**
     * 分页查询 商品加工配方明细表 列表信息
     * @param recipeDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRecipeDetailPage")
    public R getRecipeDetailPage(@RequestBody RecipeDetailReq recipeDetailReq) {
        Page page = new Page(recipeDetailReq.getPageNo(), recipeDetailReq.getPageSize());
        RecipeDetail recipeDetail =new RecipeDetail();
        BeanUtil.copyProperties(recipeDetailReq, recipeDetail);

        return new R<>(recipeDetailService.page(page, Wrappers.query(recipeDetail)));
    }


    /**
     * 全量查询 商品加工配方明细表 列表信息
     * @param recipeDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchRecipeDetailList")
    public R getRecipeDetailList(@RequestBody RecipeDetailReq recipeDetailReq) {
        RecipeDetail recipeDetail =new RecipeDetail();
        BeanUtil.copyProperties(recipeDetailReq, recipeDetail);

        return new R<>(recipeDetailService.list(Wrappers.query(recipeDetail)));
    }


    /**
     * 新增商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @Inner
    @SysLog("新增商品加工配方明细表")
    @PostMapping("/add")
    public R save(@RequestBody RecipeDetail recipeDetail) {
        return new R<>(recipeDetailService.save(recipeDetail));
    }

    /**
     * 修改商品加工配方明细表
     * @param recipeDetail 商品加工配方明细表
     * @return R
     */
    @Inner
    @SysLog("修改商品加工配方明细表")
    @PostMapping("/update")
    public R updateById(@RequestBody RecipeDetail recipeDetail) {
        return new R<>(recipeDetailService.updateById(recipeDetail));
    }

    /**
     * 通过id删除商品加工配方明细表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除商品加工配方明细表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(recipeDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除商品加工配方明细表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(recipeDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询商品加工配方明细表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(recipeDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询商品加工配方明细表总记录}")
    @PostMapping("/countByRecipeDetailReq")
    public R countByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq) {

        return new R<>(recipeDetailService.count(Wrappers.query(recipeDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param recipeDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据recipeDetailReq查询一条货位信息表")
    @PostMapping("/getOneByRecipeDetailReq")
    public R getOneByRecipeDetailReq(@RequestBody RecipeDetailReq recipeDetailReq) {

        return new R<>(recipeDetailService.getOne(Wrappers.query(recipeDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param recipeDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<RecipeDetail> recipeDetailList) {

        return new R<>(recipeDetailService.saveOrUpdateBatch(recipeDetailList));
    }

}
