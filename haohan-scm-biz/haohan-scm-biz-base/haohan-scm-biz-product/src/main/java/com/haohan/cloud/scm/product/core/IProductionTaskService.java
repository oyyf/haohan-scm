package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.CreateDeliveryTaskReq;
import com.haohan.cloud.scm.api.product.req.CreateProcessingTaskReq;
import com.haohan.cloud.scm.api.product.resp.SingleRealLossResp;

/**
 * @author cx
 * @date 2019/6/14
 */
public interface IProductionTaskService {
    /**
     * 新增 生产部任务(任务类别: 加工)  根据 汇总商品需求/预计需求
     *
     * @param req
     * @return
     */
    ProductionTask createProductProcessingTask(CreateProcessingTaskReq req);

    /**
     * 新增 生产部任务(任务类别: 配送) 根据 汇总商品需求
     *
     * @param req
     * @return
     */
    ProductionTask createProductDeliveryTask(CreateDeliveryTaskReq req);

    /**
     * 查询统计 单品实际损耗率
     *
     * @param dto
     * @return
     */
    SingleRealLossResp querySingleRealLoss(SingleRealLossDTO dto);
}
