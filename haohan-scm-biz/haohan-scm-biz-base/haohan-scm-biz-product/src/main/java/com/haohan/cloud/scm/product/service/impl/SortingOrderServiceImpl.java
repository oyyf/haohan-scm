/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.SortingOrderMapper;
import com.haohan.cloud.scm.product.service.SortingOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-13 18:15:05
 */
@Service
public class SortingOrderServiceImpl extends ServiceImpl<SortingOrderMapper, SortingOrder> implements SortingOrderService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SortingOrder entity) {
        if (StrUtil.isEmpty(entity.getSortingOrderSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(SortingOrder.class, NumberPrefixConstant.SORTING_ORDER_SN_PRE);
            entity.setSortingOrderSn(sn);
        }
        return super.save(entity);
    }


}
