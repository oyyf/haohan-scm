/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ShipOrderDetailMapper;
import com.haohan.cloud.scm.product.service.ShipOrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 送货明细
 *
 * @author haohan
 * @date 2019-05-13 18:15:11
 */
@Service
public class ShipOrderDetailServiceImpl extends ServiceImpl<ShipOrderDetailMapper, ShipOrderDetail> implements ShipOrderDetailService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ShipOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getShipDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ShipOrderDetail.class, NumberPrefixConstant.SHIP_ORDER_DETAIL_SN_PRE);
            entity.setShipDetailSn(sn);
        }
        return super.save(entity);
    }
}
