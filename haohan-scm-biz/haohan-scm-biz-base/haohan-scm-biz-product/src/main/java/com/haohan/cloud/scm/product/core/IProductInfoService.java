package com.haohan.cloud.scm.product.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.dto.CreateLossRecordDTO;
import com.haohan.cloud.scm.api.product.dto.ProcessingResultDTO;
import com.haohan.cloud.scm.api.product.dto.ProcessingSourceDTO;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductMatchSortingReq;
import com.haohan.cloud.scm.api.product.req.QueryGoodsStorageReq;
import com.haohan.cloud.scm.api.product.req.UpdateInventoryReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryOrderDetailReq;

/**
 * @author xwx
 * @date 2019/5/27
 */
public interface IProductInfoService {

    /**
     * 根据采购部采购单明细  创建 货品信息
     * @param req
     * @return
     */
    ProductInfo addProductInfo(PurchaseQueryOrderDetailReq req);

    /**
     * 更新库存余量
     * @param req
     * @return
     */
    Boolean updateInventory(UpdateInventoryReq req);


    /**
     * 货品报损/去皮 (拆分为2个 状态为：一个正常 一个损耗)
     * 货品未保存
     * @param sourceDTO
     *          必须参数: sourceProductSn / subProductNum
     * @return ProcessingResultDTO
     *          返回结果: subProduct / lossProduct / sourceProduct
     */
    ProcessingResultDTO productPeeling(ProcessingSourceDTO sourceDTO);

    /**
     * 货品分装 (拆分为多个正常 可有一个耗损)
     * 货品未保存
     * @param sourceDTO
     *          必须参数: sourceProductSn / subProductNumList
     * @return ProcessingResultDTO
     *          返回结果: subProductList / sourceProduct / lossProduct(无损耗时为null)
     */
    ProcessingResultDTO productSplitting(ProcessingSourceDTO sourceDTO);

    /**
     *  货品组合 (按配方 合并为一个 原始货品)
     * 需生成货品加工记录
     * @param sourceDTO
     * @return
     */
    ProcessingResultDTO productPacking(ProcessingSourceDTO sourceDTO);

    /**
     * 生成货品损耗记录
     * ProductLossRecord 已保存
     * @param createDTO
     *          必须参数： source / normal / loss / relationSn / lossType
     * @return 损耗记录
     */
    ProductLossRecord createLossRecord(CreateLossRecordDTO createDTO);

    /**
     * 查询商品库存
     * @param req
     * @return
     */
    ProductInfo queryGoodsStorage(QueryGoodsStorageReq req);

    /**
     * 根据 采购入库货品 匹配分拣
     * @param req pmId / realBuyNum / purchaseDetailSn
     * @return 返回修改后的货品信息
     */
    ProductInfo enterProductMatchSorting(ProductMatchSortingReq req);

    /**
     * 查询货品库存列表
     * @param page
     * @param req
     * @return
     */
    IPage queryStockNumList(Page page, ProductInfo req);
}
