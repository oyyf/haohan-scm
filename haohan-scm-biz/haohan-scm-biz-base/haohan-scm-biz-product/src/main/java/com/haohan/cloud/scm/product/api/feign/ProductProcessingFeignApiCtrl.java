/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.req.ProductProcessingReq;
import com.haohan.cloud.scm.product.service.ProductProcessingService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货品加工记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductProcessing")
@Api(value = "productprocessing", tags = "productprocessing内部接口服务")
public class ProductProcessingFeignApiCtrl {

    private final ProductProcessingService productProcessingService;


    /**
     * 通过id查询货品加工记录表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productProcessingService.getById(id));
    }


    /**
     * 分页查询 货品加工记录表 列表信息
     * @param productProcessingReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductProcessingPage")
    public R getProductProcessingPage(@RequestBody ProductProcessingReq productProcessingReq) {
        Page page = new Page(productProcessingReq.getPageNo(), productProcessingReq.getPageSize());
        ProductProcessing productProcessing =new ProductProcessing();
        BeanUtil.copyProperties(productProcessingReq, productProcessing);

        return new R<>(productProcessingService.page(page, Wrappers.query(productProcessing)));
    }


    /**
     * 全量查询 货品加工记录表 列表信息
     * @param productProcessingReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductProcessingList")
    public R getProductProcessingList(@RequestBody ProductProcessingReq productProcessingReq) {
        ProductProcessing productProcessing =new ProductProcessing();
        BeanUtil.copyProperties(productProcessingReq, productProcessing);

        return new R<>(productProcessingService.list(Wrappers.query(productProcessing)));
    }


    /**
     * 新增货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @Inner
    @SysLog("新增货品加工记录表")
    @PostMapping("/add")
    public R save(@RequestBody ProductProcessing productProcessing) {
        return new R<>(productProcessingService.save(productProcessing));
    }

    /**
     * 修改货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @Inner
    @SysLog("修改货品加工记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductProcessing productProcessing) {
        return new R<>(productProcessingService.updateById(productProcessing));
    }

    /**
     * 通过id删除货品加工记录表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货品加工记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productProcessingService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货品加工记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productProcessingService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货品加工记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productProcessingService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货品加工记录表总记录}")
    @PostMapping("/countByProductProcessingReq")
    public R countByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq) {

        return new R<>(productProcessingService.count(Wrappers.query(productProcessingReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productProcessingReq查询一条货位信息表")
    @PostMapping("/getOneByProductProcessingReq")
    public R getOneByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq) {

        return new R<>(productProcessingService.getOne(Wrappers.query(productProcessingReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productProcessingList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductProcessing> productProcessingList) {

        return new R<>(productProcessingService.saveOrUpdateBatch(productProcessingList));
    }

}
