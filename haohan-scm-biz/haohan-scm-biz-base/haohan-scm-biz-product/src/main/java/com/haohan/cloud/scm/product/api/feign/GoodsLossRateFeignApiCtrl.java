/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import com.haohan.cloud.scm.api.product.req.GoodsLossRateReq;
import com.haohan.cloud.scm.product.service.GoodsLossRateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 单品损耗率
 *
 * @author haohan
 * @date 2019-05-29 14:44:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/GoodsLossRate")
@Api(value = "goodslossrate", tags = "goodslossrate内部接口服务")
public class GoodsLossRateFeignApiCtrl {

    private final GoodsLossRateService goodsLossRateService;


    /**
     * 通过id查询单品损耗率
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(goodsLossRateService.getById(id));
    }


    /**
     * 分页查询 单品损耗率 列表信息
     * @param goodsLossRateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsLossRatePage")
    public R getGoodsLossRatePage(@RequestBody GoodsLossRateReq goodsLossRateReq) {
        Page page = new Page(goodsLossRateReq.getPageNo(), goodsLossRateReq.getPageSize());
        GoodsLossRate goodsLossRate =new GoodsLossRate();
        BeanUtil.copyProperties(goodsLossRateReq, goodsLossRate);

        return new R<>(goodsLossRateService.page(page, Wrappers.query(goodsLossRate)));
    }


    /**
     * 全量查询 单品损耗率 列表信息
     * @param goodsLossRateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchGoodsLossRateList")
    public R getGoodsLossRateList(@RequestBody GoodsLossRateReq goodsLossRateReq) {
        GoodsLossRate goodsLossRate =new GoodsLossRate();
        BeanUtil.copyProperties(goodsLossRateReq, goodsLossRate);

        return new R<>(goodsLossRateService.list(Wrappers.query(goodsLossRate)));
    }


    /**
     * 新增单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @Inner
    @SysLog("新增单品损耗率")
    @PostMapping("/add")
    public R save(@RequestBody GoodsLossRate goodsLossRate) {
        return new R<>(goodsLossRateService.save(goodsLossRate));
    }

    /**
     * 修改单品损耗率
     * @param goodsLossRate 单品损耗率
     * @return R
     */
    @Inner
    @SysLog("修改单品损耗率")
    @PostMapping("/update")
    public R updateById(@RequestBody GoodsLossRate goodsLossRate) {
        return new R<>(goodsLossRateService.updateById(goodsLossRate));
    }

    /**
     * 通过id删除单品损耗率
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除单品损耗率")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(goodsLossRateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除单品损耗率")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(goodsLossRateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询单品损耗率")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(goodsLossRateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询单品损耗率总记录}")
    @PostMapping("/countByGoodsLossRateReq")
    public R countByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq) {

        return new R<>(goodsLossRateService.count(Wrappers.query(goodsLossRateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param goodsLossRateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据goodsLossRateReq查询一条货位信息表")
    @PostMapping("/getOneByGoodsLossRateReq")
    public R getOneByGoodsLossRateReq(@RequestBody GoodsLossRateReq goodsLossRateReq) {

        return new R<>(goodsLossRateService.getOne(Wrappers.query(goodsLossRateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param goodsLossRateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<GoodsLossRate> goodsLossRateList) {

        return new R<>(goodsLossRateService.saveOrUpdateBatch(goodsLossRateList));
    }

}
