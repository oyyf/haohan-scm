/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;

/**
 * 分拣单
 *
 * @author haohan
 * @date 2019-05-13 18:15:05
 */
public interface SortingOrderMapper extends BaseMapper<SortingOrder> {

}
