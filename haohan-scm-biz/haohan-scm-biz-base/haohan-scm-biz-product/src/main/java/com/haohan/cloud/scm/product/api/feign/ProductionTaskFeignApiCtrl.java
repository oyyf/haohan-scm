/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.product.req.ProductionTaskReq;
import com.haohan.cloud.scm.product.service.ProductionTaskService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-29 14:45:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ProductionTask")
@Api(value = "productiontask", tags = "productiontask内部接口服务")
public class ProductionTaskFeignApiCtrl {

    private final ProductionTaskService productionTaskService;


    /**
     * 通过id查询生产任务表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(productionTaskService.getById(id));
    }


    /**
     * 分页查询 生产任务表 列表信息
     * @param productionTaskReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductionTaskPage")
    public R getProductionTaskPage(@RequestBody ProductionTaskReq productionTaskReq) {
        Page page = new Page(productionTaskReq.getPageNo(), productionTaskReq.getPageSize());
        ProductionTask productionTask =new ProductionTask();
        BeanUtil.copyProperties(productionTaskReq, productionTask);

        return new R<>(productionTaskService.page(page, Wrappers.query(productionTask)));
    }


    /**
     * 全量查询 生产任务表 列表信息
     * @param productionTaskReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchProductionTaskList")
    public R getProductionTaskList(@RequestBody ProductionTaskReq productionTaskReq) {
        ProductionTask productionTask =new ProductionTask();
        BeanUtil.copyProperties(productionTaskReq, productionTask);

        return new R<>(productionTaskService.list(Wrappers.query(productionTask)));
    }


    /**
     * 新增生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @Inner
    @SysLog("新增生产任务表")
    @PostMapping("/add")
    public R save(@RequestBody ProductionTask productionTask) {
        return new R<>(productionTaskService.save(productionTask));
    }

    /**
     * 修改生产任务表
     * @param productionTask 生产任务表
     * @return R
     */
    @Inner
    @SysLog("修改生产任务表")
    @PostMapping("/update")
    public R updateById(@RequestBody ProductionTask productionTask) {
        return new R<>(productionTaskService.updateById(productionTask));
    }

    /**
     * 通过id删除生产任务表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除生产任务表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(productionTaskService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除生产任务表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productionTaskService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询生产任务表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productionTaskService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询生产任务表总记录}")
    @PostMapping("/countByProductionTaskReq")
    public R countByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq) {

        return new R<>(productionTaskService.count(Wrappers.query(productionTaskReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productionTaskReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据productionTaskReq查询一条货位信息表")
    @PostMapping("/getOneByProductionTaskReq")
    public R getOneByProductionTaskReq(@RequestBody ProductionTaskReq productionTaskReq) {

        return new R<>(productionTaskService.getOne(Wrappers.query(productionTaskReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productionTaskList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ProductionTask> productionTaskList) {

        return new R<>(productionTaskService.saveOrUpdateBatch(productionTaskList));
    }

}
