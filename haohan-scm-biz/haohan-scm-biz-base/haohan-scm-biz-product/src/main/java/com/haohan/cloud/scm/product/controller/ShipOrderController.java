/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.req.ShipOrderReq;
import com.haohan.cloud.scm.product.service.ShipOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 送货单
 *
 * @author haohan
 * @date 2019-05-29 14:46:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shiporder" )
@Api(value = "shiporder", tags = "shiporder管理")
public class ShipOrderController {

    private final ShipOrderService shipOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shipOrder 送货单
     * @return
     */
    @GetMapping("/page" )
    public R getShipOrderPage(Page page, ShipOrder shipOrder) {
        return new R<>(shipOrderService.page(page, Wrappers.query(shipOrder).orderByDesc("create_date")));
    }


    /**
     * 通过id查询送货单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shipOrderService.getById(id));
    }

    /**
     * 新增送货单
     * @param shipOrder 送货单
     * @return R
     */
    @SysLog("新增送货单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shiporder_add')" )
    public R save(@RequestBody ShipOrder shipOrder) {
        return new R<>(shipOrderService.save(shipOrder));
    }

    /**
     * 修改送货单
     * @param shipOrder 送货单
     * @return R
     */
    @SysLog("修改送货单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shiporder_edit')" )
    public R updateById(@RequestBody ShipOrder shipOrder) {
        return new R<>(shipOrderService.updateById(shipOrder));
    }

    /**
     * 通过id删除送货单
     * @param id id
     * @return R
     */
    @SysLog("删除送货单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shiporder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(shipOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除送货单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shiporder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询送货单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询送货单总记录}")
    @PostMapping("/countByShipOrderReq")
    public R countByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq) {

        return new R<>(shipOrderService.count(Wrappers.query(shipOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shipOrderReq查询一条货位信息表")
    @PostMapping("/getOneByShipOrderReq")
    public R getOneByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq) {

        return new R<>(shipOrderService.getOne(Wrappers.query(shipOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shipOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shiporder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ShipOrder> shipOrderList) {

        return new R<>(shipOrderService.saveOrUpdateBatch(shipOrderList));
    }


}
