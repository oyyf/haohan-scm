/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.req.ProductProcessingReq;
import com.haohan.cloud.scm.product.service.ProductProcessingService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 货品加工记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productprocessing" )
@Api(value = "productprocessing", tags = "productprocessing管理")
public class ProductProcessingController {

    private final ProductProcessingService productProcessingService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productProcessing 货品加工记录表
     * @return
     */
    @GetMapping("/page" )
    public R getProductProcessingPage(Page page, ProductProcessing productProcessing) {
        return new R<>(productProcessingService.page(page, Wrappers.query(productProcessing)));
    }


    /**
     * 通过id查询货品加工记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productProcessingService.getById(id));
    }

    /**
     * 新增货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @SysLog("新增货品加工记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productprocessing_add')" )
    public R save(@RequestBody ProductProcessing productProcessing) {
        return new R<>(productProcessingService.save(productProcessing));
    }

    /**
     * 修改货品加工记录表
     * @param productProcessing 货品加工记录表
     * @return R
     */
    @SysLog("修改货品加工记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productprocessing_edit')" )
    public R updateById(@RequestBody ProductProcessing productProcessing) {
        return new R<>(productProcessingService.updateById(productProcessing));
    }

    /**
     * 通过id删除货品加工记录表
     * @param id id
     * @return R
     */
    @SysLog("删除货品加工记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productprocessing_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productProcessingService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除货品加工记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productprocessing_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productProcessingService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询货品加工记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productProcessingService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询货品加工记录表总记录}")
    @PostMapping("/countByProductProcessingReq")
    public R countByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq) {

        return new R<>(productProcessingService.count(Wrappers.query(productProcessingReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productProcessingReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productProcessingReq查询一条货位信息表")
    @PostMapping("/getOneByProductProcessingReq")
    public R getOneByProductProcessingReq(@RequestBody ProductProcessingReq productProcessingReq) {

        return new R<>(productProcessingService.getOne(Wrappers.query(productProcessingReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productProcessingList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productprocessing_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductProcessing> productProcessingList) {

        return new R<>(productProcessingService.saveOrUpdateBatch(productProcessingList));
    }


}
