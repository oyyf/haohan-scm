/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ProductionTaskMapper;
import com.haohan.cloud.scm.product.service.ProductionTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-13 18:16:02
 */
@Service
public class ProductionTaskServiceImpl extends ServiceImpl<ProductionTaskMapper, ProductionTask> implements ProductionTaskService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ProductionTask entity) {
        if (StrUtil.isEmpty(entity.getProductionTaskSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ProductionTask.class, NumberPrefixConstant.PRODUCTION_TASK_SN_PRE);
            entity.setProductionTaskSn(sn);
        }
        return super.save(entity);
    }
}
