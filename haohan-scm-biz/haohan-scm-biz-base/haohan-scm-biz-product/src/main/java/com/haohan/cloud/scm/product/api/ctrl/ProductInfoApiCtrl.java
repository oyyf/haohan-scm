package com.haohan.cloud.scm.product.api.ctrl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.product.ProductPlaceStatusEnum;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.req.CreateSortingOrderReq;
import com.haohan.cloud.scm.api.product.req.QueryGoodsStorageReq;
import com.haohan.cloud.scm.api.product.req.UpdateInventoryReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryOrderDetailReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.product.core.IProductInfoService;
import com.haohan.cloud.scm.product.core.IProductSortingOrderService;
import com.haohan.cloud.scm.product.service.ProductInfoService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/5/27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/productInfo")
@Api(value = "ApiProductInfo", tags = "productInfo根据采购部采购单明细  创建货品信息")
public class ProductInfoApiCtrl {

    @Autowired
    @Lazy
    private IProductInfoService productInfoService;

    @Autowired
    @Lazy
    private IProductInfoService iProductInfoService;

    @Autowired
    @Lazy
    private IProductSortingOrderService iProductSortingOrderService;

    private final ProductInfoService productInfo;

    /**
     * 创建货品信息
     * @param req
     * @return
     */
    @PostMapping("/addProductInfo")
    public R<ProductInfo> addProductInfo(@Validated PurchaseQueryOrderDetailReq req) {
        ProductInfo productInfo = productInfoService.addProductInfo(req);
        return new R<>(productInfo);
    }

    /**
    * 商品库存更新  根据goodsModelId
    * @param req
    * @return
    */
    @PostMapping("/updateInventory")
    public R updateInventory(@Validated UpdateInventoryReq req) {
        return new R(iProductInfoService.updateInventory(req));
    }

    @PostMapping("/queryGoodsStorage")
    @ApiOperation(value = "查询库存余量" , notes = "查询库存余量并更新goodsModel库存")
    public R<ProductInfo> queryGoodsStorage(@Validated QueryGoodsStorageReq req){
        return new R(iProductInfoService.queryGoodsStorage(req));
    }

    /**
     * 新增货品分拣单  根据B客户订单
     * @param req
     * @return
     */
    @PostMapping("/createSortingOrder")
    public R<SortingOrder> createSortingOrder(@Validated @RequestBody CreateSortingOrderReq req){
        return new R<>(iProductSortingOrderService.createSortingOrder(req));
    }

    @GetMapping("/queryStockNumList")
    @ApiOperation(value = "查询商品库存")
    public R queryStockNumList(Page page,ProductInfo info){
        return new R(iProductInfoService.queryStockNumList(page,info));
    }

    @GetMapping("/queryStockNumDetail")
    @ApiOperation(value = "查询商品库存记录详情")
    public R queryStockNumDetail(ProductInfo info){
        if(ObjectUtil.isNull(info)){
            throw new EmptyDataException("库存记录不能为空");
        }
        info.setProductPlaceStatus(ProductPlaceStatusEnum.stock);
        return new R(productInfo.list(Wrappers.query(info)));
    }
  }
