/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductionEmployee;
import com.haohan.cloud.scm.api.product.req.ProductionEmployeeReq;
import com.haohan.cloud.scm.product.service.ProductionEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 生产部员工表
 *
 * @author haohan
 * @date 2019-05-29 14:45:20
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productionemployee" )
@Api(value = "productionemployee", tags = "productionemployee管理")
public class ProductionEmployeeController {

    private final ProductionEmployeeService productionEmployeeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productionEmployee 生产部员工表
     * @return
     */
    @GetMapping("/page" )
    public R getProductionEmployeePage(Page page, ProductionEmployee productionEmployee) {
        return new R<>(productionEmployeeService.page(page, Wrappers.query(productionEmployee)));
    }


    /**
     * 通过id查询生产部员工表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productionEmployeeService.getById(id));
    }

    /**
     * 新增生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @SysLog("新增生产部员工表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productionemployee_add')" )
    public R save(@RequestBody ProductionEmployee productionEmployee) {
        return new R<>(productionEmployeeService.save(productionEmployee));
    }

    /**
     * 修改生产部员工表
     * @param productionEmployee 生产部员工表
     * @return R
     */
    @SysLog("修改生产部员工表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productionemployee_edit')" )
    public R updateById(@RequestBody ProductionEmployee productionEmployee) {
        return new R<>(productionEmployeeService.updateById(productionEmployee));
    }

    /**
     * 通过id删除生产部员工表
     * @param id id
     * @return R
     */
    @SysLog("删除生产部员工表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productionemployee_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productionEmployeeService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除生产部员工表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productionemployee_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productionEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询生产部员工表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productionEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询生产部员工表总记录}")
    @PostMapping("/countByProductionEmployeeReq")
    public R countByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq) {

        return new R<>(productionEmployeeService.count(Wrappers.query(productionEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productionEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productionEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByProductionEmployeeReq")
    public R getOneByProductionEmployeeReq(@RequestBody ProductionEmployeeReq productionEmployeeReq) {

        return new R<>(productionEmployeeService.getOne(Wrappers.query(productionEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productionEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productionemployee_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductionEmployee> productionEmployeeList) {

        return new R<>(productionEmployeeService.saveOrUpdateBatch(productionEmployeeList));
    }


}
