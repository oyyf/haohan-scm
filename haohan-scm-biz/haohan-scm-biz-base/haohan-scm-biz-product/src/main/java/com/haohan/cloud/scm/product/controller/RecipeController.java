/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.Recipe;
import com.haohan.cloud.scm.api.product.req.RecipeReq;
import com.haohan.cloud.scm.product.service.RecipeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商品加工配方表
 *
 * @author haohan
 * @date 2019-05-29 14:46:05
 */
@RestController
@AllArgsConstructor
@RequestMapping("/recipe" )
@Api(value = "recipe", tags = "recipe管理")
public class RecipeController {

    private final RecipeService recipeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param recipe 商品加工配方表
     * @return
     */
    @GetMapping("/page" )
    public R getRecipePage(Page page, Recipe recipe) {
        return new R<>(recipeService.page(page, Wrappers.query(recipe)));
    }


    /**
     * 通过id查询商品加工配方表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(recipeService.getById(id));
    }

    /**
     * 新增商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @SysLog("新增商品加工配方表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_recipe_add')" )
    public R save(@RequestBody Recipe recipe) {
        return new R<>(recipeService.save(recipe));
    }

    /**
     * 修改商品加工配方表
     * @param recipe 商品加工配方表
     * @return R
     */
    @SysLog("修改商品加工配方表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_recipe_edit')" )
    public R updateById(@RequestBody Recipe recipe) {
        return new R<>(recipeService.updateById(recipe));
    }

    /**
     * 通过id删除商品加工配方表
     * @param id id
     * @return R
     */
    @SysLog("删除商品加工配方表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_recipe_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(recipeService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除商品加工配方表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_recipe_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(recipeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询商品加工配方表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(recipeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询商品加工配方表总记录}")
    @PostMapping("/countByRecipeReq")
    public R countByRecipeReq(@RequestBody RecipeReq recipeReq) {

        return new R<>(recipeService.count(Wrappers.query(recipeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param recipeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据recipeReq查询一条货位信息表")
    @PostMapping("/getOneByRecipeReq")
    public R getOneByRecipeReq(@RequestBody RecipeReq recipeReq) {

        return new R<>(recipeService.getOne(Wrappers.query(recipeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param recipeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_recipe_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Recipe> recipeList) {

        return new R<>(recipeService.saveOrUpdateBatch(recipeList));
    }


}
