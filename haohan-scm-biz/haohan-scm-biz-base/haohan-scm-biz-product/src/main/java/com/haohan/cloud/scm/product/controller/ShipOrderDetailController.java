/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ShipOrderDetail;
import com.haohan.cloud.scm.api.product.req.ShipOrderDetailReq;
import com.haohan.cloud.scm.product.service.ShipOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 送货明细
 *
 * @author haohan
 * @date 2019-05-29 14:46:17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shiporderdetail" )
@Api(value = "shiporderdetail", tags = "shiporderdetail管理")
public class ShipOrderDetailController {

    private final ShipOrderDetailService shipOrderDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shipOrderDetail 送货明细
     * @return
     */
    @GetMapping("/page" )
    public R getShipOrderDetailPage(Page page, ShipOrderDetail shipOrderDetail) {
        return new R<>(shipOrderDetailService.page(page, Wrappers.query(shipOrderDetail)));
    }


    /**
     * 通过id查询送货明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shipOrderDetailService.getById(id));
    }

    /**
     * 新增送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @SysLog("新增送货明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shiporderdetail_add')" )
    public R save(@RequestBody ShipOrderDetail shipOrderDetail) {
        return new R<>(shipOrderDetailService.save(shipOrderDetail));
    }

    /**
     * 修改送货明细
     * @param shipOrderDetail 送货明细
     * @return R
     */
    @SysLog("修改送货明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shiporderdetail_edit')" )
    public R updateById(@RequestBody ShipOrderDetail shipOrderDetail) {
        return new R<>(shipOrderDetailService.updateById(shipOrderDetail));
    }

    /**
     * 通过id删除送货明细
     * @param id id
     * @return R
     */
    @SysLog("删除送货明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shiporderdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(shipOrderDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除送货明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shiporderdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询送货明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询送货明细总记录}")
    @PostMapping("/countByShipOrderDetailReq")
    public R countByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {

        return new R<>(shipOrderDetailService.count(Wrappers.query(shipOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shipOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByShipOrderDetailReq")
    public R getOneByShipOrderDetailReq(@RequestBody ShipOrderDetailReq shipOrderDetailReq) {

        return new R<>(shipOrderDetailService.getOne(Wrappers.query(shipOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shipOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shiporderdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ShipOrderDetail> shipOrderDetailList) {

        return new R<>(shipOrderDetailService.saveOrUpdateBatch(shipOrderDetailList));
    }


}
