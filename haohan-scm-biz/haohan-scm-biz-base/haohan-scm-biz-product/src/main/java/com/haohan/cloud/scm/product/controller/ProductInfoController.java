/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.req.ProductInfoReq;
import com.haohan.cloud.scm.product.service.ProductInfoService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 货品信息记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productinfo" )
@Api(value = "productinfo", tags = "productinfo管理")
public class ProductInfoController {

    private final ProductInfoService productInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productInfo 货品信息记录表
     * @return
     */
    @GetMapping("/page" )
    public R getProductInfoPage(Page page, ProductInfo productInfo) {
        return new R<>(productInfoService.page(page, Wrappers.query(productInfo)));
    }


    /**
     * 通过id查询货品信息记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productInfoService.getById(id));
    }

    /**
     * 新增货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @SysLog("新增货品信息记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productinfo_add')" )
    public R save(@RequestBody ProductInfo productInfo) {
        return new R<>(productInfoService.save(productInfo));
    }

    /**
     * 修改货品信息记录表
     * @param productInfo 货品信息记录表
     * @return R
     */
    @SysLog("修改货品信息记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productinfo_edit')" )
    public R updateById(@RequestBody ProductInfo productInfo) {
        return new R<>(productInfoService.updateById(productInfo));
    }

    /**
     * 通过id删除货品信息记录表
     * @param id id
     * @return R
     */
    @SysLog("删除货品信息记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productinfo_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productInfoService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除货品信息记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productinfo_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productInfoService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询货品信息记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productInfoService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询货品信息记录表总记录}")
    @PostMapping("/countByProductInfoReq")
    public R countByProductInfoReq(@RequestBody ProductInfoReq productInfoReq) {

        return new R<>(productInfoService.count(Wrappers.query(productInfoReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productInfoReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productInfoReq查询一条货位信息表")
    @PostMapping("/getOneByProductInfoReq")
    public R getOneByProductInfoReq(@RequestBody ProductInfoReq productInfoReq) {

        return new R<>(productInfoService.getOne(Wrappers.query(productInfoReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productInfoList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productinfo_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductInfo> productInfoList) {
        return new R<>(productInfoService.saveOrUpdateBatch(productInfoList));
    }


}
