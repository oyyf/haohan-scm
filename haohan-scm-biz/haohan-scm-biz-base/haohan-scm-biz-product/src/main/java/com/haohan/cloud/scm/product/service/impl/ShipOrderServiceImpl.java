/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ShipOrderMapper;
import com.haohan.cloud.scm.product.service.ShipOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 送货单
 *
 * @author haohan
 * @date 2019-05-13 18:15:20
 */
@Service
public class ShipOrderServiceImpl extends ServiceImpl<ShipOrderMapper, ShipOrder> implements ShipOrderService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ShipOrder entity) {
        if (StrUtil.isEmpty(entity.getShipId())) {
            String sn = scmIncrementUtil.inrcSnByClass(ShipOrder.class, NumberPrefixConstant.SHIP_ORDER_SN_PRE);
            entity.setShipId(sn);
        }
        return super.save(entity);
    }
}
