/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;

/**
 * 生产任务表
 *
 * @author haohan
 * @date 2019-05-13 18:16:02
 */
public interface ProductionTaskMapper extends BaseMapper<ProductionTask> {

}
