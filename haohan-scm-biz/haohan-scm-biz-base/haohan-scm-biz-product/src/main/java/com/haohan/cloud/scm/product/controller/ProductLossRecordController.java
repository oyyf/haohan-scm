/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductLossRecordReq;
import com.haohan.cloud.scm.product.core.IProductLossRecordService;
import com.haohan.cloud.scm.product.service.ProductLossRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-29 14:45:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/productlossrecord" )
@Api(value = "productlossrecord", tags = "productlossrecord管理")
public class ProductLossRecordController {

    private final ProductLossRecordService productLossRecordService;

    private final IProductLossRecordService iProductLossRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param productLossRecord 货品损耗记录表
     * @return
     */
    @GetMapping("/page" )
    public R getProductLossRecordPage(Page page, ProductLossRecord productLossRecord) {
        return new R<>(iProductLossRecordService.getLossRecordPage(page, productLossRecord));
    }


    /**
     * 通过id查询货品损耗记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(productLossRecordService.getById(id));
    }

    /**
     * 新增货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @SysLog("新增货品损耗记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_productlossrecord_add')" )
    public R save(@RequestBody ProductLossRecord productLossRecord) {
        return new R<>(productLossRecordService.save(productLossRecord));
    }

    /**
     * 修改货品损耗记录表
     * @param productLossRecord 货品损耗记录表
     * @return R
     */
    @SysLog("修改货品损耗记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_productlossrecord_edit')" )
    public R updateById(@RequestBody ProductLossRecord productLossRecord) {
        return new R<>(productLossRecordService.updateById(productLossRecord));
    }

    /**
     * 通过id删除货品损耗记录表
     * @param id id
     * @return R
     */
    @SysLog("删除货品损耗记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_productlossrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(productLossRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除货品损耗记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_productlossrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(productLossRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询货品损耗记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(productLossRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询货品损耗记录表总记录}")
    @PostMapping("/countByProductLossRecordReq")
    public R countByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq) {

        return new R<>(productLossRecordService.count(Wrappers.query(productLossRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param productLossRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据productLossRecordReq查询一条货位信息表")
    @PostMapping("/getOneByProductLossRecordReq")
    public R getOneByProductLossRecordReq(@RequestBody ProductLossRecordReq productLossRecordReq) {

        return new R<>(productLossRecordService.getOne(Wrappers.query(productLossRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param productLossRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_productlossrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ProductLossRecord> productLossRecordList) {

        return new R<>(productLossRecordService.saveOrUpdateBatch(productLossRecordList));
    }


}
