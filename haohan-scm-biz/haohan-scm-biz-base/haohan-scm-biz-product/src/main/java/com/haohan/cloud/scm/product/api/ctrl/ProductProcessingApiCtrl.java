package com.haohan.cloud.scm.product.api.ctrl;

import com.haohan.cloud.scm.api.product.dto.ProductPakingDTO;
import com.haohan.cloud.scm.api.product.dto.ProductPakingResultDTO;
import com.haohan.cloud.scm.api.product.entity.ProductProcessing;
import com.haohan.cloud.scm.api.product.req.CreateProductProcessingReq;
import com.haohan.cloud.scm.product.core.IProductProcessingService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cx
 * @date 2019/6/14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/product/productProcessing")
@Api(value = "ApiProductionProcessing", tags = "productionProcessing 货品加工记录操作api")
public class ProductProcessingApiCtrl {

    @Autowired
    @Lazy
    private IProductProcessingService iProductProcessingService;

    @PostMapping("/createProductProcessing")
    @ApiOperation(value = "创建货品加工记录" , notes = "创建货品加工记录(需生成货品损耗记录)")
    public  R<ProductProcessing> createProductProcessing(@Validated CreateProductProcessingReq req){
        return new R(iProductProcessingService.createProductProcessing(req));
    }

    @PostMapping("/productPacking")
    @ApiOperation(value = "货品组合" , notes = "货品组合按配方 合并为一个 原始货品")
    public R<ProductPakingResultDTO> productPacking(@Validated ProductPakingDTO dto){
        return new R(iProductProcessingService.productPacking(dto));
    }
}
