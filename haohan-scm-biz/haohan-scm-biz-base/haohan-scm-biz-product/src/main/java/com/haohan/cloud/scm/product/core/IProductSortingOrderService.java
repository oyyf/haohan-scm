package com.haohan.cloud.scm.product.core;

import com.haohan.cloud.scm.api.product.entity.SortingOrder;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.CreateSortingOrderReq;

/**
 * @author dy
 * @date 2019/7/5
 */
public interface IProductSortingOrderService {


    /**
     * 新增货品分拣单  根据B客户订单
     *
     * @param req
     * @return
     */
    SortingOrder createSortingOrder(CreateSortingOrderReq req);

    /**
     * 分拣单明细 开始分拣
     *
     * @param sortingOrderDetail
     * @return
     */
    Boolean sortingDetailConfirm(SortingOrderDetail sortingOrderDetail);


    /**
     * 修改分拣单状态为 已分拣
     *
     * @param sortingOrderSn
     * @return
     */
    Boolean sortingOrderConfirm(String sortingOrderSn);

    /**
     * 分拣单 确认完成
     * @param order 实体对象 必需完整属性
     * @return  执行失败抛出异常
     */
    void confirmSortingFinish(SortingOrder order);


}
