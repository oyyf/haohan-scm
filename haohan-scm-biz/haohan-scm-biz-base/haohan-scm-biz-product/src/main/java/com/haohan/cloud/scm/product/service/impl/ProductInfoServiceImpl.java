/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.product.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.product.mapper.ProductInfoMapper;
import com.haohan.cloud.scm.product.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 货品信息记录表
 *
 * @author haohan
 * @date 2019-05-13 18:21:39
 */
@Service
public class ProductInfoServiceImpl extends ServiceImpl<ProductInfoMapper, ProductInfo> implements ProductInfoService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ProductInfo entity) {
        if (StrUtil.isEmpty(entity.getProductSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ProductInfo.class, NumberPrefixConstant.PRODUCT_INFO_SN_PRE);
            entity.setProductSn(sn);
        }
        return super.save(entity);
    }

    @Override
    public ProductInfo fetchBySn(String productSn) {
        QueryWrapper<ProductInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(ProductInfo::getProductSn, productSn);
        return super.getOne(queryWrapper);
    }
}
