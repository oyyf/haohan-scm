/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;

/**
 * 单品损耗率
 *
 * @author haohan
 * @date 2019-05-13 18:20:23
 */
public interface GoodsLossRateMapper extends BaseMapper<GoodsLossRate> {

}
