/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.product.SingleRealLossDTO;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;

import java.math.BigDecimal;

/**
 * 货品损耗记录表
 *
 * @author haohan
 * @date 2019-05-13 18:16:25
 */
public interface ProductLossRecordMapper extends BaseMapper<ProductLossRecord> {
    /**
     * 根据goodsModelId查询货品损耗记录
     * @param dto
     * @return
     */
    BigDecimal queryLossByGoodsModelId(SingleRealLossDTO dto);
}
