package com.haohan.cloud.scm.product.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.feign.GoodsModelFeignService;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.feign.UPassportFeignService;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.feign.SummaryOrderFeignService;
import com.haohan.cloud.scm.api.opc.req.SummaryOrderReq;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.feign.BuyOrderDetailFeignService;
import com.haohan.cloud.scm.api.saleb.feign.BuyOrderFeignService;
import com.haohan.cloud.scm.api.saleb.feign.SalebFeignService;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderDetailReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.UpdateBuyOrderStatusReq;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.feign.ExitWarehouseDetailFeignService;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author cx
 * @date 2019/6/13
 */
@Component
@AllArgsConstructor
public class ScmProductUtils {

    private final SummaryOrderFeignService summaryOrderFeignService;
    private final UPassportFeignService uPassportFeignService;
    private final GoodsModelFeignService goodsModelFeignService;
    private final BuyOrderDetailFeignService buyOrderDetailFeignService;
    private final BuyOrderFeignService buyOrderFeignService;
    private final SalebFeignService salebFeignService;
    private final ExitWarehouseDetailFeignService exitWarehouseDetailFeignService;

    /**
     * 根据id 返回汇总单
     *
     * @param summaryOrderId
     * @param pmId
     * @return
     */
    public SummaryOrder querySummaryOrderById(String summaryOrderId, String pmId) {
        SummaryOrderReq req = new SummaryOrderReq();
        req.setPmId(pmId);
        req.setSummaryOrderId(summaryOrderId);
        R r = summaryOrderFeignService.getOneBySummaryOrderReq(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new EmptyDataException("汇总单查询有误");
        }
        SummaryOrder order = BeanUtil.toBean(r.getData(), SummaryOrder.class);
        return order;
    }

    /**
     * 根据id 查询 uPassport
     *
     * @param id
     * @return
     */
    public UPassport queryUPassportById(String id) {
        R<UPassport> byId = uPassportFeignService.getById(id, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(byId) || byId.getData() == null) {
            throw new EmptyDataException("uPassport查询有误");
        }
        UPassport data = byId.getData();
        return data;
    }

    /**
     * 根据Id查询GoodsModelDTO
     *
     * @param id
     * @return
     */
    public GoodsModelDTO queryGoodsModelById(String id) {
        R rGoods = goodsModelFeignService.getInfoById(id, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(rGoods) || null == rGoods.getData()) {
            throw new EmptyDataException("GoodsModelDTO查询有误");
        }
        return BeanUtil.toBean(rGoods.getData(), GoodsModelDTO.class);
    }

    /**
     * 根据Sn查询BuyOrderDetail
     *
     * @param detailSn
     * @return
     */
    public BuyOrderDetail queryBuyOrderDetailBySn(String detailSn) {
        BuyOrderDetailReq req = new BuyOrderDetailReq();
        req.setBuyDetailSn(detailSn);
        R r = buyOrderDetailFeignService.getOneByBuyOrderDetailReq(req,SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new EmptyDataException("BuyOrderDetail查询有误");
        }
        return BeanUtil.toBean(r.getData(), BuyOrderDetail.class);
    }

    /**
     * 根据buyId查询BuyOrder
     *
     * @param buyId
     * @return
     */
    public BuyOrder queryBuyOrderBySn(String buyId) {
        BuyOrderReq req = new BuyOrderReq();
        req.setBuyId(buyId);
        R r = buyOrderFeignService.getOneByBuyOrderReq(req,SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new EmptyDataException("BuyOrder查询有误");
        }
        return BeanUtil.toBean(r.getData(), BuyOrder.class);
    }

    /**
     * 根据Id 更新BuyOrderDetail
     *
     * @param buyOrderDetail
     * @return
     */
    public Boolean updateBuyOrderDetailById(BuyOrderDetail buyOrderDetail) {
        R r = buyOrderDetailFeignService.updateById(buyOrderDetail, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || !(Boolean)r.getData()) {
            throw new EmptyDataException("BuyOrderDetail更新有误");
        }
        return true;
    }

    /**
     * 确认 采购单的所有明细已成交 修改采购单状态为已成交
     * @param buyId
     * @return
     */
    public Boolean confirmBuyOrder(String buyId){
        R r = salebFeignService.confirmBuyOrder(buyId, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || !(Boolean)r.getData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据buyId 查询BuyOrderDetail
     *
     * @param buyId
     * @return
     */
    public List queryBuyOrderDetailList(String buyId) {
        BuyOrderDetailReq req = new BuyOrderDetailReq();
        req.setBuyId(buyId);
        R r = buyOrderDetailFeignService.getBuyOrderDetailList(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || null == r.getData()) {
            throw new EmptyDataException("BuyOrderDetail 列表查询有误");
        }
        List list = (List) r.getData();
        if(CollUtil.isEmpty(list)){
            throw new EmptyDataException("BuyOrderDetail 列表查询为空");
        }
        return list;
    }

    /**
     * 查询采购单
     * @param req
     * @return
     */
    public BuyOrder queryOneByBuyOrderReq(BuyOrderReq req){
        R r = buyOrderFeignService.getOneByBuyOrderReq(req, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r) || null == r.getData()){
            throw new EmptyDataException("查询采购单失败");
        }
        return BeanUtil.toBean(r.getData(), BuyOrder.class);
    }

    /**
     * 保存出库单明细
     *
     * @param detail
     * @return
     */
    public Boolean saveExitWarehouseDetail(ExitWarehouseDetail detail) {
        R r = exitWarehouseDetailFeignService.save(detail, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || !(Boolean)r.getData()) {
            throw new EmptyDataException("出库单保存有误");
        }
        return true;
    }

    /**
     * 修改B订单及明细的状态
     * @param updateReq
     * @return
     */
    public Boolean updateBuyOrderStatus(UpdateBuyOrderStatusReq updateReq) {
        R r = buyOrderFeignService.updateBuyOrderStatus(updateReq, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r)){
            throw new EmptyDataException("修改B订单及明细的状态失败");
        }
        return (Boolean) r.getData();
    }

    /**
     * 根据buyId 查询BuyOrderDetail
     *
     * @param req
     * @return
     */
    public List queryBuyOrderList(BuyOrderReq req) {
        R r = buyOrderFeignService.getBuyOrderList(req, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r)) {
            throw new EmptyDataException("BuyOrderDetail 列表查询有误");
        }
        List list = (List) r.getData();
        if(CollUtil.isEmpty(list)){
            return null;
        }
        return list;
    }
}
