package com.haohan.cloud.scm.product.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ProductLossRecord;
import com.haohan.cloud.scm.api.product.req.ProductInfoInventoryReq;

/**
 * @author cx
 * @date 2019/8/13
 */
public interface IProductLossRecordService {

    /**
     * 新增损耗记录
     * @param info
     * @return
     */
    Boolean addLossRecord(ProductInfoInventoryReq info);

    /**
     * 编辑损耗记录
     * @param record
     * @return
     */
    Boolean editLossRecord(ProductLossRecord record);

    /**
     * 查询损耗记录列表
     * @param page
     * @param record
     * @return
     */
    IPage getLossRecordPage(Page page, ProductLossRecord record);
}
