/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import com.haohan.cloud.scm.api.product.req.SortingOrderDetailReq;
import com.haohan.cloud.scm.product.service.SortingOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-29 14:46:59
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sortingorderdetail" )
@Api(value = "sortingorderdetail", tags = "sortingorderdetail管理")
public class SortingOrderDetailController {

    private final SortingOrderDetailService sortingOrderDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sortingOrderDetail 分拣单明细
     * @return
     */
    @GetMapping("/page" )
    public R getSortingOrderDetailPage(Page page, SortingOrderDetail sortingOrderDetail) {
        return new R<>(sortingOrderDetailService.page(page, Wrappers.query(sortingOrderDetail)));
    }


    /**
     * 通过id查询分拣单明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(sortingOrderDetailService.getById(id));
    }

    /**
     * 新增分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @SysLog("新增分拣单明细" )
    @PostMapping
 /*   @PreAuthorize("@pms.hasPermission('scm_sortingorderdetail_add')" )*/
    public R save(@RequestBody SortingOrderDetail sortingOrderDetail) {
        return new R<>(sortingOrderDetailService.save(sortingOrderDetail));
    }

    /**
     * 修改分拣单明细
     * @param sortingOrderDetail 分拣单明细
     * @return R
     */
    @SysLog("修改分拣单明细" )
    @PutMapping
/*    @PreAuthorize("@pms.hasPermission('scm_sortingorderdetail_edit')" )*/
    public R updateById(@RequestBody SortingOrderDetail sortingOrderDetail) {
        return new R<>(sortingOrderDetailService.updateById(sortingOrderDetail));
    }

    /**
     * 通过id删除分拣单明细
     * @param id id
     * @return R
     */
    @SysLog("删除分拣单明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_sortingorderdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(sortingOrderDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除分拣单明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_sortingorderdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询分拣单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(sortingOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询分拣单明细总记录}")
    @PostMapping("/countBySortingOrderDetailReq")
    public R countBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {

        return new R<>(sortingOrderDetailService.count(Wrappers.query(sortingOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param sortingOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据sortingOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneBySortingOrderDetailReq")
    public R getOneBySortingOrderDetailReq(@RequestBody SortingOrderDetailReq sortingOrderDetailReq) {

        return new R<>(sortingOrderDetailService.getOne(Wrappers.query(sortingOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param sortingOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_sortingorderdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SortingOrderDetail> sortingOrderDetailList) {

        return new R<>(sortingOrderDetailService.saveOrUpdateBatch(sortingOrderDetailList));
    }


}
