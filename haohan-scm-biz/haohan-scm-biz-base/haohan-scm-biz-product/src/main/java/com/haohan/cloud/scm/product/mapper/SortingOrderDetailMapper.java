/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.dto.SortingOrderDetailDTO;
import com.haohan.cloud.scm.api.product.entity.SortingOrderDetail;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;

/**
 * 分拣单明细
 *
 * @author haohan
 * @date 2019-05-13 18:15:00
 */
public interface SortingOrderDetailMapper extends BaseMapper<SortingOrderDetail> {

    /**
     * 查询分拣单明细列表
     *
     * @param page
     * @return
     */
    IPage<SortingOrderDetailDTO> querySortingOrderDetail(Page page, @Param("pmId") String pmId, @Param("buyerId") String buyerId,
                                                         @Param("deliveryDate") LocalDate deliveryDate,
                                                         @Param("deliverySeq") String deliverySeq, @Param("goodsName") String goodsName,
                                                         @Param("sortingStatus") String sortingStatus, @Param("goodsModelId") String goodsModelId);

}
