/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.product.entity.ShipOrder;
import com.haohan.cloud.scm.api.product.req.ShipOrderReq;
import com.haohan.cloud.scm.product.service.ShipOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 送货单
 *
 * @author haohan
 * @date 2019-05-29 14:46:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShipOrder")
@Api(value = "shiporder", tags = "shiporder内部接口服务")
public class ShipOrderFeignApiCtrl {

    private final ShipOrderService shipOrderService;


    /**
     * 通过id查询送货单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shipOrderService.getById(id));
    }


    /**
     * 分页查询 送货单 列表信息
     * @param shipOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipOrderPage")
    public R getShipOrderPage(@RequestBody ShipOrderReq shipOrderReq) {
        Page page = new Page(shipOrderReq.getPageNo(), shipOrderReq.getPageSize());
        ShipOrder shipOrder =new ShipOrder();
        BeanUtil.copyProperties(shipOrderReq, shipOrder);

        return new R<>(shipOrderService.page(page, Wrappers.query(shipOrder)));
    }


    /**
     * 全量查询 送货单 列表信息
     * @param shipOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipOrderList")
    public R getShipOrderList(@RequestBody ShipOrderReq shipOrderReq) {
        ShipOrder shipOrder =new ShipOrder();
        BeanUtil.copyProperties(shipOrderReq, shipOrder);

        return new R<>(shipOrderService.list(Wrappers.query(shipOrder)));
    }


    /**
     * 新增送货单
     * @param shipOrder 送货单
     * @return R
     */
    @Inner
    @SysLog("新增送货单")
    @PostMapping("/add")
    public R save(@RequestBody ShipOrder shipOrder) {
        return new R<>(shipOrderService.save(shipOrder));
    }

    /**
     * 修改送货单
     * @param shipOrder 送货单
     * @return R
     */
    @Inner
    @SysLog("修改送货单")
    @PostMapping("/update")
    public R updateById(@RequestBody ShipOrder shipOrder) {
        return new R<>(shipOrderService.updateById(shipOrder));
    }

    /**
     * 通过id删除送货单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除送货单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shipOrderService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除送货单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询送货单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询送货单总记录}")
    @PostMapping("/countByShipOrderReq")
    public R countByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq) {

        return new R<>(shipOrderService.count(Wrappers.query(shipOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shipOrderReq查询一条货位信息表")
    @PostMapping("/getOneByShipOrderReq")
    public R getOneByShipOrderReq(@RequestBody ShipOrderReq shipOrderReq) {

        return new R<>(shipOrderService.getOne(Wrappers.query(shipOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shipOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShipOrder> shipOrderList) {

        return new R<>(shipOrderService.saveOrUpdateBatch(shipOrderList));
    }

}
