/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;

/**
 * 货品信息记录表1
 *
 * @author haohan
 * @date 2019-05-13 18:21:39
 */
public interface ProductInfoService extends IService<ProductInfo> {

    /**
     * 根据逻辑主键获取货品信息
     * @param productSn
     * @return
     */
    ProductInfo fetchBySn(String productSn);

}
