/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;

/**
 * 货品信息记录表1
 *
 * @author haohan
 * @date 2019-05-13 18:21:39
 */
public interface ProductInfoMapper extends BaseMapper<ProductInfo> {

}
