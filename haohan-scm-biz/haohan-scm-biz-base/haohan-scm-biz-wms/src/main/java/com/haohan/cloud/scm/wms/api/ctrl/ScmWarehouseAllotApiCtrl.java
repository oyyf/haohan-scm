package com.haohan.cloud.scm.wms.api.ctrl;

import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseAllotReq;
import com.haohan.cloud.scm.api.wms.req.EditWarehouseAllotReq;
import com.haohan.cloud.scm.wms.core.IScmWarehouseAllotService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cx
 * @date 2019/8/10
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/wms/scmWarehouseAllot")
@Api(value = "ApiScmWarehouseAllot", tags = "ScmWarehouseAllot调拨管理api")
public class ScmWarehouseAllotApiCtrl {

    private final IScmWarehouseAllotService iScmWarehouseAllotService;

    @GetMapping("/queryWarehouseAllotDetails")
    @ApiOperation(value = "查询调拨详情")
    public R queryWarehouseAllotDetails(WarehouseAllot allot){
        return R.ok(iScmWarehouseAllotService.queryWarehouseAllotDetails(allot));
    }

    @PostMapping("/addWarehouseAllot")
    @ApiOperation(value = "新增调拨记录")
    public R addWarehouseAllot(AddWarehouseAllotReq req){
        return R.ok(iScmWarehouseAllotService.addWarehouseAllot(req));
    }

    @PostMapping("/editWarehouseAllot")
    @ApiOperation(value = "编辑调拨记录")
    public R editWarehouseAllot(EditWarehouseAllotReq req){
        return R.ok(iScmWarehouseAllotService.editWarehouseAllot(req));
    }
}
