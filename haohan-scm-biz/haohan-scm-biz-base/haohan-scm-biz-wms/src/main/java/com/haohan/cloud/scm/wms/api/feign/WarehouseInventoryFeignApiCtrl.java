/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.req.WarehouseInventoryReq;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 库存盘点
 *
 * @author haohan
 * @date 2019-05-29 13:23:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WarehouseInventory")
@Api(value = "warehouseinventory", tags = "warehouseinventory内部接口服务")
public class WarehouseInventoryFeignApiCtrl {

    private final WarehouseInventoryService warehouseInventoryService;


    /**
     * 通过id查询库存盘点
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warehouseInventoryService.getById(id));
    }


    /**
     * 分页查询 库存盘点 列表信息
     * @param warehouseInventoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseInventoryPage")
    public R getWarehouseInventoryPage(@RequestBody WarehouseInventoryReq warehouseInventoryReq) {
        Page page = new Page(warehouseInventoryReq.getPageNo(), warehouseInventoryReq.getPageSize());
        WarehouseInventory warehouseInventory =new WarehouseInventory();
        BeanUtil.copyProperties(warehouseInventoryReq, warehouseInventory);

        return new R<>(warehouseInventoryService.page(page, Wrappers.query(warehouseInventory)));
    }


    /**
     * 全量查询 库存盘点 列表信息
     * @param warehouseInventoryReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseInventoryList")
    public R getWarehouseInventoryList(@RequestBody WarehouseInventoryReq warehouseInventoryReq) {
        WarehouseInventory warehouseInventory =new WarehouseInventory();
        BeanUtil.copyProperties(warehouseInventoryReq, warehouseInventory);

        return new R<>(warehouseInventoryService.list(Wrappers.query(warehouseInventory)));
    }


    /**
     * 新增库存盘点
     * @param warehouseInventory 库存盘点
     * @return R
     */
    @Inner
    @SysLog("新增库存盘点")
    @PostMapping("/add")
    public R save(@RequestBody WarehouseInventory warehouseInventory) {
        return new R<>(warehouseInventoryService.save(warehouseInventory));
    }

    /**
     * 修改库存盘点
     * @param warehouseInventory 库存盘点
     * @return R
     */
    @Inner
    @SysLog("修改库存盘点")
    @PostMapping("/update")
    public R updateById(@RequestBody WarehouseInventory warehouseInventory) {
        return new R<>(warehouseInventoryService.updateById(warehouseInventory));
    }

    /**
     * 通过id删除库存盘点
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除库存盘点")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseInventoryService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除库存盘点")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseInventoryService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询库存盘点")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseInventoryService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseInventoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询库存盘点总记录}")
    @PostMapping("/countByWarehouseInventoryReq")
    public R countByWarehouseInventoryReq(@RequestBody WarehouseInventoryReq warehouseInventoryReq) {

        return new R<>(warehouseInventoryService.count(Wrappers.query(warehouseInventoryReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseInventoryReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warehouseInventoryReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseInventoryReq")
    public R getOneByWarehouseInventoryReq(@RequestBody WarehouseInventoryReq warehouseInventoryReq) {

        return new R<>(warehouseInventoryService.getOne(Wrappers.query(warehouseInventoryReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseInventoryList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WarehouseInventory> warehouseInventoryList) {

        return new R<>(warehouseInventoryService.saveOrUpdateBatch(warehouseInventoryList));
    }

}
