/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.ExitWarehouseMapper;
import com.haohan.cloud.scm.wms.service.ExitWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 出库单
 *
 * @author haohan
 * @date 2019-05-13 21:29:09
 */
@Service
public class ExitWarehouseServiceImpl extends ServiceImpl<ExitWarehouseMapper, ExitWarehouse> implements ExitWarehouseService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ExitWarehouse entity) {
        if (StrUtil.isEmpty(entity.getExitWarehouseSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ExitWarehouse.class, NumberPrefixConstant.EXIT_WAREHOUSE_SN_PRE);
            entity.setExitWarehouseSn(sn);
        }
        return super.save(entity);
    }

}
