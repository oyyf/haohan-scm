/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;

import java.util.List;

/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-13 21:29:03
 */
public interface ExitWarehouseDetailService extends IService<ExitWarehouseDetail> {


    /**
     * 汇总出库单规格商品数量
     * @param detail
     * @return
     */
    List<ExitWarehouseDetail> summaryExitNum(ExitWarehouseDetail detail);


}
