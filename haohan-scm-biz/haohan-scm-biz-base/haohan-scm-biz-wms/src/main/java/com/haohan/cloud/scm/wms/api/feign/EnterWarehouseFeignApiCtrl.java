/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseByOrderDetailReq;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseReq;
import com.haohan.cloud.scm.wms.core.IScmEnterWarehouseService;
import com.haohan.cloud.scm.wms.service.EnterWarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 入库单
 *
 * @author haohan
 * @date 2019-05-29 13:22:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/EnterWarehouse")
@Api(value = "enterwarehouse", tags = "enterwarehouse内部接口服务")
public class EnterWarehouseFeignApiCtrl {

    private final EnterWarehouseService enterWarehouseService;
    private final IScmEnterWarehouseService scmEnterWarehouseService;


    /**
     * 通过id查询入库单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(enterWarehouseService.getById(id));
    }


    /**
     * 分页查询 入库单 列表信息
     * @param enterWarehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchEnterWarehousePage")
    public R getEnterWarehousePage(@RequestBody EnterWarehouseReq enterWarehouseReq) {
        Page page = new Page(enterWarehouseReq.getPageNo(), enterWarehouseReq.getPageSize());
        EnterWarehouse enterWarehouse =new EnterWarehouse();
        BeanUtil.copyProperties(enterWarehouseReq, enterWarehouse);

        return new R<>(enterWarehouseService.page(page, Wrappers.query(enterWarehouse)));
    }


    /**
     * 全量查询 入库单 列表信息
     * @param enterWarehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchEnterWarehouseList")
    public R getEnterWarehouseList(@RequestBody EnterWarehouseReq enterWarehouseReq) {
        EnterWarehouse enterWarehouse =new EnterWarehouse();
        BeanUtil.copyProperties(enterWarehouseReq, enterWarehouse);

        return new R<>(enterWarehouseService.list(Wrappers.query(enterWarehouse)));
    }


    /**
     * 新增入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @Inner
    @SysLog("新增入库单")
    @PostMapping("/add")
    public R save(@RequestBody EnterWarehouse enterWarehouse) {
        return new R<>(enterWarehouseService.save(enterWarehouse));
    }

    /**
     * 修改入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @Inner
    @SysLog("修改入库单")
    @PostMapping("/update")
    public R updateById(@RequestBody EnterWarehouse enterWarehouse) {
        return new R<>(enterWarehouseService.updateById(enterWarehouse));
    }

    /**
     * 通过id删除入库单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除入库单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(enterWarehouseService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除入库单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询入库单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询入库单总记录}")
    @PostMapping("/countByEnterWarehouseReq")
    public R countByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq) {

        return new R<>(enterWarehouseService.count(Wrappers.query(enterWarehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据enterWarehouseReq查询一条货位信息表")
    @PostMapping("/getOneByEnterWarehouseReq")
    public R getOneByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq) {

        return new R<>(enterWarehouseService.getOne(Wrappers.query(enterWarehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param enterWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<EnterWarehouse> enterWarehouseList) {

        return new R<>(enterWarehouseService.saveOrUpdateBatch(enterWarehouseList));
    }

    /**
     * 新增入库单（入库单明细）根据采购单明细
     * @param req
     * @return
     */
    @Inner
    @SysLog("新增入库单（入库单明细）根据采购单明细")
    @PostMapping("/enterWarehouseByOrderDetail")
    public R enterWarehouseByOrderDetail(@RequestBody EnterWarehouseByOrderDetailReq req){
        return new R<>(scmEnterWarehouseService.enterWarehouseByOrderDetail(req));
    }

    /**
     * 查寻订单退货入库单详情
      * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryEnterOrderDetail")
    public R queryEnterOrderDetail(@RequestBody EnterWarehouseReq req){
        return new R<>(scmEnterWarehouseService.queryEnterDetail(req));
    }

}
