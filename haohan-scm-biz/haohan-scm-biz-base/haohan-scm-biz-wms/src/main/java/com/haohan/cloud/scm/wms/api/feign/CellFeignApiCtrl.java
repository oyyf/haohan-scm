/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Cell;
import com.haohan.cloud.scm.api.wms.req.CellReq;
import com.haohan.cloud.scm.wms.service.CellService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 货位信息表
 *
 * @author haohan
 * @date 2019-05-29 13:21:43
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Cell")
@Api(value = "cell", tags = "cell内部接口服务")
public class CellFeignApiCtrl {

    private final CellService cellService;


    /**
     * 通过id查询货位信息表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(cellService.getById(id));
    }


    /**
     * 分页查询 货位信息表 列表信息
     * @param cellReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCellPage")
    public R getCellPage(@RequestBody CellReq cellReq) {
        Page page = new Page(cellReq.getPageNo(), cellReq.getPageSize());
        Cell cell =new Cell();
        BeanUtil.copyProperties(cellReq, cell);

        return new R<>(cellService.page(page, Wrappers.query(cell)));
    }


    /**
     * 全量查询 货位信息表 列表信息
     * @param cellReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCellList")
    public R getCellList(@RequestBody CellReq cellReq) {
        Cell cell =new Cell();
        BeanUtil.copyProperties(cellReq, cell);

        return new R<>(cellService.list(Wrappers.query(cell)));
    }


    /**
     * 新增货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @Inner
    @SysLog("新增货位信息表")
    @PostMapping("/add")
    public R save(@RequestBody Cell cell) {
        return new R<>(cellService.save(cell));
    }

    /**
     * 修改货位信息表
     * @param cell 货位信息表
     * @return R
     */
    @Inner
    @SysLog("修改货位信息表")
    @PostMapping("/update")
    public R updateById(@RequestBody Cell cell) {
        return new R<>(cellService.updateById(cell));
    }

    /**
     * 通过id删除货位信息表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除货位信息表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(cellService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除货位信息表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(cellService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询货位信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(cellService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询货位信息表总记录}")
    @PostMapping("/countByCellReq")
    public R countByCellReq(@RequestBody CellReq cellReq) {

        return new R<>(cellService.count(Wrappers.query(cellReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param cellReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据cellReq查询一条货位信息表")
    @PostMapping("/getOneByCellReq")
    public R getOneByCellReq(@RequestBody CellReq cellReq) {

        return new R<>(cellService.getOne(Wrappers.query(cellReq), false));
    }


    /**
     * 批量修改OR插入
     * @param cellList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Cell> cellList) {

        return new R<>(cellService.saveOrUpdateBatch(cellList));
    }

}
