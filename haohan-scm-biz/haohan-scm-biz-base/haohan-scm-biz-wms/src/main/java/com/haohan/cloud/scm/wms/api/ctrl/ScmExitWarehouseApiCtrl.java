package com.haohan.cloud.scm.wms.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.*;
import com.haohan.cloud.scm.wms.core.IScmExitWarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @date 2019/6/13
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wms/scmExitWarehouse")
@Api(value = "ApiScmExitWarehouse", tags = "scmExitWarehouse仓储入库单管理api")
public class ScmExitWarehouseApiCtrl {

    private final IScmExitWarehouseService scmExitWarehouseService;

    /**
     * 新增出库单 根据出库单明细
     * @param req  必须：pmId/ids/warehouseSn
     * @return
     */
    @PostMapping("/createExitWarehouse")
    @ApiOperation(value = "新增出库单" , notes = "根据出库单明细")
    public R createExitWarehouse(@RequestBody @Validated CreateExitWarehouseReq req){

        return new R<>(scmExitWarehouseService.createExitWarehouse(req));
    }

    @PostMapping("/createExitWarehouseDetail")
    @ApiOperation(value = "创建出库单明细 测试接口" , notes = "创建出库单明细(锁库存)")
    public R createExitWarehouseDetail(ExitWarehouseDetail detail){
        return new R<>(scmExitWarehouseService.createExitWarehouseDetail(detail));
    }

    @PostMapping("/confirmExitWarehouseDetail")
    @ApiOperation(value = "确认出库单明细,实出数量" , notes = "确认出库单明细 实出数量")
    public R confirmExitWarehouseDetail(@Validated DetailConfirmRealExitNumReq req){
        return new R<>(scmExitWarehouseService.confirmExitWarehouseDetail(req));
    }

    @PostMapping("/confirmRealExitNum")
    @ApiOperation(value = "同一商品 出库 实出数量确认" , notes = "同一商品 出库 实出数量确认")
    public R confirmRealExitNum(@RequestBody @Validated ConfirmRealExitNumReq req){
        return new R<>(scmExitWarehouseService.confirmRealExitNum(req));
    }

    @PostMapping("/queryWaitList")
    @ApiOperation(value = "查询待出库列表" , notes = "查询待出库列表")
    public R queryWaitExitList(@Validated QueryWaitExitReq req){
        ExitWarehouseDetail detail = new ExitWarehouseDetail();
        BeanUtil.copyProperties(req, detail);
        return new R<>(scmExitWarehouseService.queryWaitList( detail));
    }
    /**
     * 查询出库单明细（包含货品信息）根据出库单号
     * @param req
     * @return
     */
    @PostMapping("/queryExitWarehouseDetail")
    @ApiOperation(value = "查询出库单明细（包含货品信息）根据出库单号")
    public R queryExitWarehouseDetail(@RequestBody @Validated QueryExitWarehouseDetailReq req){
        return new R<>(scmExitWarehouseService.queryExitWarehouseDetail(req));
    }

    /**
     * 新增出库单（出库单明细）
     * @param req
     * @return
     */
    @PostMapping("/addExitWarehouse")
    @ApiOperation(value = "新增出库单（出库单明细）")
    public R addExitWarehouse(@RequestBody @Validated AddExitWarehouseReq req){
        return new R<>(scmExitWarehouseService.addExitWarehouse(req));
    }

    /**
     * 编辑出库单（修改出库单明细）
     * @param req
     * @return
     */
    @PostMapping("/redactExitWarehouse")
    @ApiOperation(value = "编辑出库单（修改出库单明细）")
    public R redactExitWarehouse(@RequestBody @Validated RedactExitWarehouseReq req){
        return new R<>(scmExitWarehouseService.redactExitWarehouse(req));
    }
}
