/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.SummaryExitNumReq;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.ExitWarehouseDetailMapper;
import com.haohan.cloud.scm.wms.service.ExitWarehouseDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-13 21:29:03
 */
@Service
public class ExitWarehouseDetailServiceImpl extends ServiceImpl<ExitWarehouseDetailMapper, ExitWarehouseDetail> implements ExitWarehouseDetailService {

    @Autowired
    private ExitWarehouseDetailMapper detailMapper;
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ExitWarehouseDetail entity) {
        if (StrUtil.isEmpty(entity.getExitWarehouseDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ExitWarehouseDetail.class, NumberPrefixConstant.EXIT_WAREHOUSE_DETAIL_SN_PRE);
            entity.setExitWarehouseDetailSn(sn);
        }
        return super.save(entity);
    }

    /**
     * 汇总出库单规格商品数量
     * @param detail
     * @return
     */
    @Override
    public List<ExitWarehouseDetail> summaryExitNum(ExitWarehouseDetail detail) {
        SummaryExitNumReq req = new SummaryExitNumReq();
        req.copyFrom(detail);
        return detailMapper.summaryExitNum(req);
    }
}
