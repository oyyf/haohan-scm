/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.StoragePlace;
import com.haohan.cloud.scm.api.wms.req.StoragePlaceReq;
import com.haohan.cloud.scm.wms.service.StoragePlaceService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 暂存点信息表
 *
 * @author haohan
 * @date 2019-05-29 13:23:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/StoragePlace")
@Api(value = "storageplace", tags = "storageplace内部接口服务")
public class StoragePlaceFeignApiCtrl {

    private final StoragePlaceService storagePlaceService;


    /**
     * 通过id查询暂存点信息表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(storagePlaceService.getById(id));
    }


    /**
     * 分页查询 暂存点信息表 列表信息
     * @param storagePlaceReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStoragePlacePage")
    public R getStoragePlacePage(@RequestBody StoragePlaceReq storagePlaceReq) {
        Page page = new Page(storagePlaceReq.getPageNo(), storagePlaceReq.getPageSize());
        StoragePlace storagePlace =new StoragePlace();
        BeanUtil.copyProperties(storagePlaceReq, storagePlace);

        return new R<>(storagePlaceService.page(page, Wrappers.query(storagePlace)));
    }


    /**
     * 全量查询 暂存点信息表 列表信息
     * @param storagePlaceReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchStoragePlaceList")
    public R getStoragePlaceList(@RequestBody StoragePlaceReq storagePlaceReq) {
        StoragePlace storagePlace =new StoragePlace();
        BeanUtil.copyProperties(storagePlaceReq, storagePlace);

        return new R<>(storagePlaceService.list(Wrappers.query(storagePlace)));
    }


    /**
     * 新增暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @Inner
    @SysLog("新增暂存点信息表")
    @PostMapping("/add")
    public R save(@RequestBody StoragePlace storagePlace) {
        return new R<>(storagePlaceService.save(storagePlace));
    }

    /**
     * 修改暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @Inner
    @SysLog("修改暂存点信息表")
    @PostMapping("/update")
    public R updateById(@RequestBody StoragePlace storagePlace) {
        return new R<>(storagePlaceService.updateById(storagePlace));
    }

    /**
     * 通过id删除暂存点信息表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除暂存点信息表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(storagePlaceService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除暂存点信息表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(storagePlaceService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询暂存点信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(storagePlaceService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询暂存点信息表总记录}")
    @PostMapping("/countByStoragePlaceReq")
    public R countByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq) {

        return new R<>(storagePlaceService.count(Wrappers.query(storagePlaceReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据storagePlaceReq查询一条货位信息表")
    @PostMapping("/getOneByStoragePlaceReq")
    public R getOneByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq) {

        return new R<>(storagePlaceService.getOne(Wrappers.query(storagePlaceReq), false));
    }


    /**
     * 批量修改OR插入
     * @param storagePlaceList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<StoragePlace> storagePlaceList) {

        return new R<>(storagePlaceService.saveOrUpdateBatch(storagePlaceList));
    }

}
