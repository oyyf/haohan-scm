package com.haohan.cloud.scm.wms.core.impl;

import com.haohan.cloud.scm.wms.core.IEntryWareHouseService;
import com.haohan.cloud.scm.wms.service.EnterWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: haohan-fresh-scm
 * @description:
 * @author: Simon
 * @create: 2019-05-27
 **/
@Service
public class EntryWareHouseServiceImpl implements IEntryWareHouseService {

//  @Autowired
//  private PurchaseFeignService purchaseFeignService;

  @Autowired
  private EnterWarehouseDetailService enterWarehouseDetailService;

  @Override
  public R<Boolean> applyEntryWareHouseByPodId(String podId) {


//   R result= purchaseFeignService.getPurchaseOrderDetailById(podId, SecurityConstants.FROM_IN);

//    if(!RUtil.isSuccess(result)){
//      return result;
//    }
//
//    PurchaseOrderDetail orderDetail = BeanUtil.toBean(result.getData(),PurchaseOrderDetail.class);

//    EnterWarehouseDetail enterWarehouseDetail = EnterWareHouseTrans.trans(orderDetail);

//    return new R<>(enterWarehouseDetailService.save(enterWarehouseDetail));
    return new R<>();
  }

  @Override
  public R applyEntryWareHouseByPuraseSn(String purchaseSn) {
    return null;
  }

  @Override
  public R entryWareHoseAduitByEnterHouseSn(String entrySn) {
    return null;
  }

  @Override
  public R entryWareHoseAduitByEnterHouseDetailSn(String detailSn) {
    return null;
  }
}
