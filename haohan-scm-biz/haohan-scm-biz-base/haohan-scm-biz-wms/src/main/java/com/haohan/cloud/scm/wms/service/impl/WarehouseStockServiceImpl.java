/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;
import com.haohan.cloud.scm.wms.mapper.WarehouseStockMapper;
import com.haohan.cloud.scm.wms.service.WarehouseStockService;
import org.springframework.stereotype.Service;

/**
 * 仓库库存
 *
 * @author haohan
 * @date 2019-08-22 16:12:12
 */
@Service
public class WarehouseStockServiceImpl extends ServiceImpl<WarehouseStockMapper, WarehouseStock> implements WarehouseStockService {

}
