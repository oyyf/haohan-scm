/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import com.haohan.cloud.scm.api.wms.req.WarehouseAllotDetailReq;
import com.haohan.cloud.scm.wms.service.WarehouseAllotDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 库存调拨明细记录
 *
 * @author haohan
 * @date 2019-05-29 13:23:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/warehouseallotdetail" )
@Api(value = "warehouseallotdetail", tags = "warehouseallotdetail管理")
public class WarehouseAllotDetailController {

    private final WarehouseAllotDetailService warehouseAllotDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return
     */
    @GetMapping("/page" )
    public R getWarehouseAllotDetailPage(Page page, WarehouseAllotDetail warehouseAllotDetail) {
        return new R<>(warehouseAllotDetailService.page(page, Wrappers.query(warehouseAllotDetail)));
    }


    /**
     * 通过id查询库存调拨明细记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(warehouseAllotDetailService.getById(id));
    }

    /**
     * 新增库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @SysLog("新增库存调拨明细记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_warehouseallotdetail_add')" )
    public R save(@RequestBody WarehouseAllotDetail warehouseAllotDetail) {
        return new R<>(warehouseAllotDetailService.save(warehouseAllotDetail));
    }

    /**
     * 修改库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @SysLog("修改库存调拨明细记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_warehouseallotdetail_edit')" )
    public R updateById(@RequestBody WarehouseAllotDetail warehouseAllotDetail) {
        return new R<>(warehouseAllotDetailService.updateById(warehouseAllotDetail));
    }

    /**
     * 通过id删除库存调拨明细记录
     * @param id id
     * @return R
     */
    @SysLog("删除库存调拨明细记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_warehouseallotdetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseAllotDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除库存调拨明细记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_warehouseallotdetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询库存调拨明细记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询库存调拨明细记录总记录}")
    @PostMapping("/countByWarehouseAllotDetailReq")
    public R countByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {

        return new R<>(warehouseAllotDetailService.count(Wrappers.query(warehouseAllotDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据warehouseAllotDetailReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseAllotDetailReq")
    public R getOneByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {

        return new R<>(warehouseAllotDetailService.getOne(Wrappers.query(warehouseAllotDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseAllotDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_warehouseallotdetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WarehouseAllotDetail> warehouseAllotDetailList) {

        return new R<>(warehouseAllotDetailService.saveOrUpdateBatch(warehouseAllotDetailList));
    }


}
