/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.Cell;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.CellMapper;
import com.haohan.cloud.scm.wms.service.CellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 货位信息表
 *
 * @author haohan
 * @date 2019-05-13 21:26:37
 */
@Service
public class CellServiceImpl extends ServiceImpl<CellMapper, Cell> implements CellService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(Cell entity) {
        if (StrUtil.isEmpty(entity.getCellSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Cell.class, NumberPrefixConstant.CELL_SN_PRE);
            entity.setCellSn(sn);
        }
        return super.save(entity);
    }

}
