/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.EnterWarehouseDetailMapper;
import com.haohan.cloud.scm.wms.service.EnterWarehouseDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 入库单明细
 *
 * @author haohan
 * @date 2019-05-13 21:28:43
 */
@Service
public class EnterWarehouseDetailServiceImpl extends ServiceImpl<EnterWarehouseDetailMapper, EnterWarehouseDetail> implements EnterWarehouseDetailService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(EnterWarehouseDetail entity) {
        if (StrUtil.isEmpty(entity.getEnterWarehouseDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(EnterWarehouseDetail.class, NumberPrefixConstant.ENTER_WAREHOUSE_DETAIL_SN_PRE);
            entity.setEnterWarehouseDetailSn(sn);
        }
        return super.save(entity);
    }

}
