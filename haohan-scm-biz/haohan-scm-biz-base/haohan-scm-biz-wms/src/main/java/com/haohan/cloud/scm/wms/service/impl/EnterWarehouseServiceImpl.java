/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.EnterWarehouseMapper;
import com.haohan.cloud.scm.wms.service.EnterWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 入库单
 *
 * @author haohan
 * @date 2019-05-13 21:28:49
 */
@Service
public class EnterWarehouseServiceImpl extends ServiceImpl<EnterWarehouseMapper, EnterWarehouse> implements EnterWarehouseService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(EnterWarehouse entity) {
        if (StrUtil.isEmpty(entity.getEnterWarehouseSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(EnterWarehouse.class, NumberPrefixConstant.ENTER_WAREHOUSE_SN_PRE);
            entity.setEnterWarehouseSn(sn);
        }
        return super.save(entity);
    }

}
