/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.wms.entity.Warehouse;

/**
 * 仓库信息表
 *
 * @author haohan
 * @date 2019-05-13 21:32:17
 */
public interface WarehouseMapper extends BaseMapper<Warehouse> {

}
