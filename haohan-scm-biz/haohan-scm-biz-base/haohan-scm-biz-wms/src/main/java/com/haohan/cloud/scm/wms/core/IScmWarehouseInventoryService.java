package com.haohan.cloud.scm.wms.core;

import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseInventoryReq;
import com.haohan.cloud.scm.api.wms.req.EditInventoryReq;
import com.haohan.cloud.scm.api.wms.resp.WarehouseInventoryDetailResp;

/**
 * @author cx
 * @date 2019/8/10
 */

public interface IScmWarehouseInventoryService {

    /**
     * 查询盘点详情
     * @param inventory
     * @return
     */
    WarehouseInventoryDetailResp queryInventoryDetail(WarehouseInventory inventory);

    /**
     * 新增盘点记录
     * @param req
     * @return
     */
    Boolean addWarehouseInventory(AddWarehouseInventoryReq req);

    /**
     * 编辑盘点记录
     * @param req
     * @return
     */
    Boolean editWarehouseInventory(EditInventoryReq req);
}
