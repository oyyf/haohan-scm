package com.haohan.cloud.scm.wms.api.ctrl;

import com.haohan.cloud.scm.api.wms.entity.WarehouseInventory;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseInventoryReq;
import com.haohan.cloud.scm.api.wms.req.EditInventoryReq;
import com.haohan.cloud.scm.wms.core.IScmWarehouseInventoryService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author cx
 * @date 2019/8/10
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/wms/scmWarehouseInventory")
@Api(value = "ApiScmWarehouseInventory", tags = "ScmWarehouseInventory盘点管理api")
public class ScmWarehouseInventoryApiCtrl {

    private final IScmWarehouseInventoryService scmWarehouseInventoryService;

    @GetMapping("/queryInventoryDetail")
    @ApiOperation(value = "查询盘点详情")
    public R queryInventoryDetail(WarehouseInventory inventory){
        return R.ok(scmWarehouseInventoryService.queryInventoryDetail(inventory));
    }

    @PostMapping("/addWarehouseInventory")
    @ApiOperation(value = "新增盘点记录")
    public R addWarehouseInventory(@RequestBody AddWarehouseInventoryReq req){
        return R.ok(scmWarehouseInventoryService.addWarehouseInventory(req));
    }

    @PostMapping("/editWarehouseInventory")
    @ApiOperation(value = "编辑盘点记录")
    public R editWarehouseInventory(@RequestBody EditInventoryReq req){
        return R.ok(scmWarehouseInventoryService.editWarehouseInventory(req));
    }
}
