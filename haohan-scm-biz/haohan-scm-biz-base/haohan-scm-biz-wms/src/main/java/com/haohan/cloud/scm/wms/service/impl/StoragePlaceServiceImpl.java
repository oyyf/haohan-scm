/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.StoragePlace;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.StoragePlaceMapper;
import com.haohan.cloud.scm.wms.service.StoragePlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 暂存点信息表
 *
 * @author haohan
 * @date 2019-05-13 21:31:40
 */
@Service
public class StoragePlaceServiceImpl extends ServiceImpl<StoragePlaceMapper, StoragePlace> implements StoragePlaceService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(StoragePlace entity) {
        if (StrUtil.isEmpty(entity.getStoragePlaceSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(StoragePlace.class, NumberPrefixConstant.STORAGE_PLACE_SN_PRE);
            entity.setStoragePlaceSn(sn);
        }
        return super.save(entity);
    }
}
