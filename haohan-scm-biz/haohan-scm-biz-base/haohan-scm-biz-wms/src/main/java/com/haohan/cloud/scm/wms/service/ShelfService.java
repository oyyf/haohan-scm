/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.wms.entity.Shelf;

/**
 * 货架信息表
 *
 * @author haohan
 * @date 2019-05-13 21:30:10
 */
public interface ShelfService extends IService<Shelf> {

}
