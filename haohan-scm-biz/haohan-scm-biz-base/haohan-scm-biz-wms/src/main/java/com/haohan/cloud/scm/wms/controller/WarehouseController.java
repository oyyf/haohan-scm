/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Warehouse;
import com.haohan.cloud.scm.api.wms.req.WarehouseReq;
import com.haohan.cloud.scm.wms.service.WarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 仓库信息表
 *
 * @author haohan
 * @date 2019-05-29 13:23:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/warehouse" )
@Api(value = "warehouse", tags = "warehouse管理")
public class WarehouseController {

    private final WarehouseService warehouseService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param warehouse 仓库信息表
     * @return
     */
    @GetMapping("/page" )
    public R getWarehousePage(Page page, Warehouse warehouse) {
        return new R<>(warehouseService.page(page, Wrappers.query(warehouse)));
    }


    /**
     * 通过id查询仓库信息表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(warehouseService.getById(id));
    }

    /**
     * 新增仓库信息表
     * @param warehouse 仓库信息表
     * @return R
     */
    @SysLog("新增仓库信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_warehouse_add')" )
    public R save(@RequestBody Warehouse warehouse) {
        return new R<>(warehouseService.save(warehouse));
    }

    /**
     * 修改仓库信息表
     * @param warehouse 仓库信息表
     * @return R
     */
    @SysLog("修改仓库信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_warehouse_edit')" )
    public R updateById(@RequestBody Warehouse warehouse) {
        return new R<>(warehouseService.updateById(warehouse));
    }

    /**
     * 通过id删除仓库信息表
     * @param id id
     * @return R
     */
    @SysLog("删除仓库信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_warehouse_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除仓库信息表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_warehouse_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询仓库信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询仓库信息表总记录}")
    @PostMapping("/countByWarehouseReq")
    public R countByWarehouseReq(@RequestBody WarehouseReq warehouseReq) {

        return new R<>(warehouseService.count(Wrappers.query(warehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据warehouseReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseReq")
    public R getOneByWarehouseReq(@RequestBody WarehouseReq warehouseReq) {

        return new R<>(warehouseService.getOne(Wrappers.query(warehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_warehouse_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<Warehouse> warehouseList) {

        return new R<>(warehouseService.saveOrUpdateBatch(warehouseList));
    }


}
