/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.Pallet;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.PalletMapper;
import com.haohan.cloud.scm.wms.service.PalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 托盘信息表
 *
 * @author haohan
 * @date 2019-05-13 21:29:29
 */
@Service
public class PalletServiceImpl extends ServiceImpl<PalletMapper, Pallet> implements PalletService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(Pallet entity) {
        if (StrUtil.isEmpty(entity.getPalletSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Pallet.class, NumberPrefixConstant.PALLET_SN_PRE);
            entity.setPalletSn(sn);
        }
        return super.save(entity);
    }

}
