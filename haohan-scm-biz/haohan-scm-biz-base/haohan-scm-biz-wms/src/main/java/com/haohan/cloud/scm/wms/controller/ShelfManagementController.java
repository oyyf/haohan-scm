/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ShelfManagement;
import com.haohan.cloud.scm.api.wms.req.ShelfManagementReq;
import com.haohan.cloud.scm.wms.service.ShelfManagementService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 货品上下架记录
 *
 * @author haohan
 * @date 2019-05-29 13:22:57
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shelfmanagement" )
@Api(value = "shelfmanagement", tags = "shelfmanagement管理")
public class ShelfManagementController {

    private final ShelfManagementService shelfManagementService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shelfManagement 货品上下架记录
     * @return
     */
    @GetMapping("/page" )
    public R getShelfManagementPage(Page page, ShelfManagement shelfManagement) {
        return new R<>(shelfManagementService.page(page, Wrappers.query(shelfManagement)));
    }


    /**
     * 通过id查询货品上下架记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(shelfManagementService.getById(id));
    }

    /**
     * 新增货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @SysLog("新增货品上下架记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_shelfmanagement_add')" )
    public R save(@RequestBody ShelfManagement shelfManagement) {
        return new R<>(shelfManagementService.save(shelfManagement));
    }

    /**
     * 修改货品上下架记录
     * @param shelfManagement 货品上下架记录
     * @return R
     */
    @SysLog("修改货品上下架记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_shelfmanagement_edit')" )
    public R updateById(@RequestBody ShelfManagement shelfManagement) {
        return new R<>(shelfManagementService.updateById(shelfManagement));
    }

    /**
     * 通过id删除货品上下架记录
     * @param id id
     * @return R
     */
    @SysLog("删除货品上下架记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_shelfmanagement_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(shelfManagementService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除货品上下架记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_shelfmanagement_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shelfManagementService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询货品上下架记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shelfManagementService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询货品上下架记录总记录}")
    @PostMapping("/countByShelfManagementReq")
    public R countByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq) {

        return new R<>(shelfManagementService.count(Wrappers.query(shelfManagementReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shelfManagementReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shelfManagementReq查询一条货位信息表")
    @PostMapping("/getOneByShelfManagementReq")
    public R getOneByShelfManagementReq(@RequestBody ShelfManagementReq shelfManagementReq) {

        return new R<>(shelfManagementService.getOne(Wrappers.query(shelfManagementReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shelfManagementList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_shelfmanagement_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ShelfManagement> shelfManagementList) {

        return new R<>(shelfManagementService.saveOrUpdateBatch(shelfManagementList));
    }


}
