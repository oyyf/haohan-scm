/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;

/**
 * 库存调拨记录
 *
 * @author haohan
 * @date 2019-05-13 21:32:14
 */
public interface WarehouseAllotService extends IService<WarehouseAllot> {

}
