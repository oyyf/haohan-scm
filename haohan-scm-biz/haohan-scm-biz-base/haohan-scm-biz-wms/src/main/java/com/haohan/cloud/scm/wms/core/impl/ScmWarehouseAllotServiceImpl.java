package com.haohan.cloud.scm.wms.core.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.product.AllotStatusEnum;
import com.haohan.cloud.scm.api.wms.entity.Warehouse;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import com.haohan.cloud.scm.api.wms.req.AddWarehouseAllotReq;
import com.haohan.cloud.scm.api.wms.req.EditWarehouseAllotReq;
import com.haohan.cloud.scm.api.wms.resp.WarehouseAllotDetailResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.core.IScmWarehouseAllotService;
import com.haohan.cloud.scm.wms.service.WarehouseAllotDetailService;
import com.haohan.cloud.scm.wms.service.WarehouseAllotService;
import com.haohan.cloud.scm.wms.service.WarehouseService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author cx
 * @date 2019/8/10
 */
@Service
@AllArgsConstructor
public class ScmWarehouseAllotServiceImpl implements IScmWarehouseAllotService {

    private final WarehouseAllotService warehouseAllotService;

    private final WarehouseAllotDetailService warehouseAllotDetailService;

    private final WarehouseService warehouseService;

    private final ScmIncrementUtil scmIncrementUtil;
    /**
     * 查询调拨详情
     * @param allot
     * @return
     */
    @Override
    public WarehouseAllotDetailResp queryWarehouseAllotDetails(WarehouseAllot allot) {
        WarehouseAllot a = warehouseAllotService.getOne(Wrappers.query(allot));
        if(ObjectUtil.isNull(a)){
            throw new EmptyDataException();
        }
        QueryWrapper<WarehouseAllotDetail> query = new QueryWrapper<>();
        query.lambda()
                .eq(WarehouseAllotDetail::getPmId,a.getPmId())
                .eq(WarehouseAllotDetail::getWarehouseAllotSn,a.getWarehouseAllotSn());
        List<WarehouseAllotDetail> list = warehouseAllotDetailService.list(query);
        if(list.isEmpty()){
            throw new EmptyDataException();
        }
        WarehouseAllotDetailResp res = new WarehouseAllotDetailResp();
        BeanUtil.copyProperties(a,res);
        res.setDetails(list);
        return res;
    }

    /**
     * 新增调拨记录
     * @param req
     * @return
     */
    @Override
    public Boolean addWarehouseAllot(AddWarehouseAllotReq req) {
        WarehouseAllot allot = new WarehouseAllot();
        BeanUtil.copyProperties(req,allot);
        String sn = scmIncrementUtil.inrcSnByClass(WarehouseAllot.class, NumberPrefixConstant.WAREHOUSE_ALLOT_SN_PRE);
        allot.setWarehouseAllotSn(sn);
        Warehouse q = new Warehouse();
        q.setPmId(req.getPmId());
        q.setWarehouseSn(req.getInWarehouseSn());
        Warehouse in = warehouseService.getOne(Wrappers.query(q));
        if(ObjectUtil.isNull(in)){
            throw new EmptyDataException();
        }
        q.setWarehouseSn(req.getOutWarehouseSn());
        Warehouse out = warehouseService.getOne(Wrappers.query(q));
        if(ObjectUtil.isNull(out)){
            throw new EmptyDataException();
        }
        allot.setAllotStatus(AllotStatusEnum.not_accept.getType());
        allot.setInWarehouseName(in.getWarehouseName());
        allot.setOutWarehouseName(out.getWarehouseName());
        allot.setApplyTime(LocalDateTime.now());
        if(!warehouseAllotService.save(allot)){
            throw new ErrorDataException();
        }
        req.getInfos().forEach(info -> {
            WarehouseAllotDetail detail = new WarehouseAllotDetail();
            detail.setWarehouseAllotSn(sn);
            detail.setAllotStatus(AllotStatusEnum.not_accept.getType());
            detail.setOutWarehouseSn(out.getWarehouseSn());
            detail.setOutWarehouseName(out.getWarehouseName());
            detail.setInWarehouseName(in.getWarehouseName());
            detail.setInWarehouseSn(in.getWarehouseSn());
            //TODO 货架、位、盘
            detail.setProductName(info.getProductName());
            detail.setProductNumber(info.getProductNumber());
            detail.setProductSn(info.getProductSn());
            detail.setUnit(info.getUnit());
            detail.setOperateTime(LocalDateTime.now());
            if(!warehouseAllotDetailService.save(detail)){
                throw new ErrorDataException();
            }
        });
        return true;
    }

    /**
     * 编辑调拨记录
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean editWarehouseAllot(EditWarehouseAllotReq req) {
        WarehouseAllot allot = new WarehouseAllot();
        BeanUtil.copyProperties(req,allot);
        if(!warehouseAllotService.updateById(allot)){
            throw new ErrorDataException();
        }
        req.getList().forEach(list ->{
            if(!warehouseAllotDetailService.updateById(list)){
                throw new ErrorDataException();
            }
        });
        if(!req.getDelList().isEmpty()){
            req.getDelList().forEach(del -> warehouseAllotDetailService.removeById(del.getId()));
        }
        if(!req.getAddList().isEmpty()){
            req.getAddList().forEach(add -> {
                WarehouseAllotDetail detail = new WarehouseAllotDetail();
                detail.setWarehouseAllotSn(allot.getWarehouseAllotSn());
                detail.setAllotStatus(AllotStatusEnum.not_accept.getType());
                detail.setOutWarehouseSn(allot.getOutWarehouseSn());
                detail.setOutWarehouseName(allot.getOutWarehouseName());
                detail.setInWarehouseName(allot.getInWarehouseName());
                detail.setInWarehouseSn(allot.getInWarehouseSn());
                //TODO 货架、位、盘
                detail.setOperateTime(LocalDateTime.now());
                if(!warehouseAllotDetailService.save(detail)){
                    throw new ErrorDataException();
                }
            });
        }
        return null;
    }
}
