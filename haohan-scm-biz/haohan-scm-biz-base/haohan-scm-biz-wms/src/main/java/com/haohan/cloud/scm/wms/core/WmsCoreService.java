package com.haohan.cloud.scm.wms.core;

import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.entity.ProductionTask;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.wms.entity.*;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/29
 */
public interface WmsCoreService {


    // 新增入库单明细 根据采购单明细(已完成)
    EnterWarehouseDetail addEnterDetailByPurchaseDetail(PurchaseOrderDetail purchaseOrderDetail);

    // 确认入库  修改入库单明细信息（已完成）
    EnterWarehouseDetail modifyEnterDetail(EnterWarehouseDetail enterWarehouseDetail);

    // 新增入库单 根据入库单明细(已完成)
    EnterWarehouse createEnterWarehouse(List<EnterWarehouseDetail> list);

    // 货品入库操作 货品放置在 托盘 货位 上(已完成)
    boolean productEnter(ProductInfo productInfo, Cell cell);

    // 新增出库单明细 根据生产任务
    ExitWarehouseDetail addExitDeatilByTask(ProductionTask productionTask);

    // 新增出库单 根据出库单明细(已完成)
    ExitWarehouse createExitWarehouse(List<ExitWarehouseDetail> list);


    // 查询仓库货位状态信息  货位图 返回 仓库/货架/货位 信息
    Cell queryCellInfo(Cell cell);

    // 货品上架(已完成)
    ShelfManagement productPutShelf(ShelfManagement shelfManagement);

    // 货品下架(已完成)
    ShelfManagement productRemoveShelf(ShelfManagement shelfManagement);

    // 库存盘点
    WarehouseInventory inventoryProduct(WarehouseInventory warehouseInventory);

    // 库存调拨
    WarehouseAllot allotProduct(WarehouseAllot warehouseAllot);


}
