package com.haohan.cloud.scm.wms.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.constant.enums.product.EnterStatusEnum;
import com.haohan.cloud.scm.api.purchase.req.PurchaseProductEntryReq;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.entity.Pallet;
import com.haohan.cloud.scm.api.wms.req.*;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.wms.core.IScmEnterWarehouseService;
import com.haohan.cloud.scm.wms.service.EnterWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * @author xwx
 * @date 2019/6/13
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/wms/enterWarehouse")
@Api(value = "ApiScmEnterWarehouse", tags = "scmEnterWarehouse仓储入库单管理api")
public class ScmEnterWarehouseApiCtrl {
    private final IScmEnterWarehouseService scmEnterWarehouseService;
    private final EnterWarehouseDetailService enterWarehouseDetailService;

    /**
     *  新增入库单明细 根据采购单明细
     * @param req  必须：purchaseDetailSn/pmId
     * @return
     */
    @PostMapping("/addEnterDetailByPurchaseDetail")
    @ApiOperation(value = "新增入库单明细" , notes = "根据采购单明细")
    public R addEnterDetailByPurchaseDetail(@Validated AddEnterDetailReq req){

        return new R<>(scmEnterWarehouseService.addEnterDetailByPurchaseDetail(req));
    }

    /**
     * 确认入库  修改入库单明细信息
     * @param req 必须：id/pmId
     * @return
     */
    @PostMapping("/modifyEnterDetail")
    @ApiOperation(value = "确认入库" , notes = "修改入库单明细信息 修改入库状态：已验收")
    public R modifyEnterDetail(@Validated EnterWarehouseDetailReq req){
        if (StrUtil.isEmpty(req.getId())){
            throw new ErrorDataException("缺少id");
        }
        EnterWarehouseDetail enterWarehouseDetail = new EnterWarehouseDetail();
        //修改入库状态：已验收
        enterWarehouseDetail.setEnterStatus(EnterStatusEnum.accept);
        return new R<>(enterWarehouseDetailService.updateById(enterWarehouseDetail));
    }

    /**
     * 新增入库单 根据入库单明细
     * @param req  ids入库单明细编号数组/pmId/warehouseSn
     * @return
     */
    @PostMapping("/createEnterWarehouse")
    @ApiOperation(value = "新增入库单" , notes = "根据入库单明细")
    public R createEnterWarehouse(@RequestBody @Validated CreateEnterWarehouseReq req){

        return new R<>(scmEnterWarehouseService.createEnterWarehouse(req));
    }

    /**
     * 货品入库操作 货品放置在 托盘 上
     * @param req  必须：pmId/productSn/palletSn
     * @return
     */
    @PostMapping("/productEnter")
    @ApiOperation(value = "货品放入托盘" , notes = "货品放置在 托盘 上")
    public R productEnter(@RequestBody @Validated ProductEnterReq req){

        return new R<>(scmEnterWarehouseService.productEnter(req));
    }

    /**
     * 新增入库单(入库单明细) 根据货品信息
     * @param req  必须：list、warehouseSn
     * @return
     */
    @PostMapping("/createEnterWarehouseByProductInfo")
    @ApiOperation(value = "新增入库单" , notes = "根据货品信息 新增入库单、入库单明细")
    public R createEnterWarehouseByProductInfo(@Validated CreateEnterByProductInfoReq req){
        return new R<>(scmEnterWarehouseService.createEnterWarehouseByProductInfo(req));
    }

    /**
     * 货品上架
     * @param req  必须：pmId/productSn/palletSn/cellSn/operatorId
     * @return
     */
    @PostMapping("/productPutShelf")
    @ApiOperation(value = "货品上架" , notes = "修改货位信息")
    public R productPutShelf(@RequestBody @Validated ProductPutShelfReq req){
        ProductEnterReq productEnterReq = new ProductEnterReq();
        productEnterReq.setPmId(req.getPmId());
        productEnterReq.setPalletSn(req.getPalletSn());
        //将货品放入托盘
        Pallet pallet = scmEnterWarehouseService.productEnter(productEnterReq);
        req.setPallet(pallet);
        return new R<>(scmEnterWarehouseService.productPutShelf(req));
    }

    /**
     * 货品下架(货位、货品数量全下)
     * @param req  必须：pmId/productSn/operatorId
     * @return
     */
    @PostMapping("/productRemoveShelf")
    @ApiOperation(value = "货品下架" , notes = "货位、货品数量全下")
    public R productRemoveShelf(@Validated ProductRemoveShelfReq req){

        return new R<>(scmEnterWarehouseService.productRemoveShelf(req));
    }

    /**
     * 货品下架(货位、货品数量部分下架)
     * @param req  必须：pmId/sourceProductSn/subProductNum/operatorId/targetPalletSn
     * @return
     */
    @PostMapping("/productRemoveSection")
    @ApiOperation(value = "货品下架" , notes = "货位、货品数量部分下架")
    public R productRemoveSection(@Validated ProductRemoveSectionReq req){

        return new R<>(scmEnterWarehouseService.productRemoveSection(req));
    }

    /**
     * 新增入库单 入库存储 根据采购单明细
     * @param req
     * @return
     */
    @PostMapping("/warehouseStorage")
    @ApiOperation(value = "新增入库单 入库存储 根据采购单明细" , notes = "货品入库时就指定托盘和货位")
    public R warehouseStorage(@Validated WarehouseStorageReq req){
        return new R<>(scmEnterWarehouseService.warehouseStorage(req));
    }

    /**
     * 采购明细货品 实际入库
     * @param req
     * @return
     */
    @PostMapping("/purchaseProductEntry")
    @ApiOperation(value = "采购明细货品 实际入库")
    public R purchaseProductEntry(@RequestBody @Validated PurchaseProductEntryReq req){

        return new R<>(scmEnterWarehouseService.purchaseProductEntry(req));
    }

    /**
     * 新增入库单（入库单明细）根据采购单明细
     * @param req
     * @return
     */
    @PostMapping("/enterWarehouseByOrderDetail")
    @ApiOperation(value = "新增入库单（入库单明细）根据采购单明细")
    public R enterWarehouseByOrderDetail(@Validated EnterWarehouseByOrderDetailReq req){
        return new R<>(scmEnterWarehouseService.enterWarehouseByOrderDetail(req));
    }

    /**
     * 查询入库单明细（包含货品信息）根据入库单号
     * @param req
     * @return
     */
    @PostMapping("/queryEnterWarehouseDetail")
    @ApiOperation(value = "查询入库单明细（包含货品信息）根据入库单号")
    public R queryEnterWarehouseDetail(@RequestBody @Validated QueryEnterWarehouseDetailReq req){
        return new R<>(scmEnterWarehouseService.queryEnterWarehouseDetail(req));
    }

    /**
     * 根据商品创建入库单、入库单明细明细、货品信息
     * @param req
     * @return
     */
    @PostMapping("/createEnterWarehouseDetail")
    @ApiOperation(value = "根据商品创建入库单、入库单明细明细、货品信息")
    public R createEnterWarehouseDetail(@RequestBody @Validated CreateEnterWarehouseDetailReq req){
        return new R<>(scmEnterWarehouseService.createEnterWarehouse(req));
    }

    /**
     * 修改入库单明细和货品信息
     * @param req
     * @return
     */
    @PostMapping("/updateEnterWarehouseDetail")
    @ApiOperation(value = "修改入库单明细和货品信息")
    public R updateEnterWarehouseDetail(@RequestBody @Validated UpdateEnterWarehouseDetailReq req){
        return new R<>(scmEnterWarehouseService.updateEnterWarehouseDetail(req));
    }

    @GetMapping("/queryEnterOrderDetail")
    @ApiOperation(value = "查询入库单详情")
    public R queryEnterOrderDetail(EnterWarehouseReq req){
        return new R<>(scmEnterWarehouseService.queryEnterDetail(req));
    }

    @PostMapping("/editEnterWarehouse")
    @ApiOperation(value = "编辑入库单")
    public R editEnterWarehouse(@RequestBody EditEnterWarehouseReq req){
        return R.ok(scmEnterWarehouseService.editEnterWarehouse(req));
    }
}
