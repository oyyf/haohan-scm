/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;

/**
 * 仓库库存
 *
 * @author haohan
 * @date 2019-08-22 16:12:12
 */
public interface WarehouseStockService extends IService<WarehouseStock> {

}
