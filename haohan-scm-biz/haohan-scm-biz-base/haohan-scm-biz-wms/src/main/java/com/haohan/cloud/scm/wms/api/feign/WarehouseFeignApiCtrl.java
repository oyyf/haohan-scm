/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.Warehouse;
import com.haohan.cloud.scm.api.wms.req.WarehouseReq;
import com.haohan.cloud.scm.wms.service.WarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 仓库信息表
 *
 * @author haohan
 * @date 2019-05-29 13:23:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Warehouse")
@Api(value = "warehouse", tags = "warehouse内部接口服务")
public class WarehouseFeignApiCtrl {

    private final WarehouseService warehouseService;


    /**
     * 通过id查询仓库信息表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warehouseService.getById(id));
    }


    /**
     * 分页查询 仓库信息表 列表信息
     * @param warehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehousePage")
    public R getWarehousePage(@RequestBody WarehouseReq warehouseReq) {
        Page page = new Page(warehouseReq.getPageNo(), warehouseReq.getPageSize());
        Warehouse warehouse =new Warehouse();
        BeanUtil.copyProperties(warehouseReq, warehouse);

        return new R<>(warehouseService.page(page, Wrappers.query(warehouse)));
    }


    /**
     * 全量查询 仓库信息表 列表信息
     * @param warehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseList")
    public R getWarehouseList(@RequestBody WarehouseReq warehouseReq) {
        Warehouse warehouse =new Warehouse();
        BeanUtil.copyProperties(warehouseReq, warehouse);

        return new R<>(warehouseService.list(Wrappers.query(warehouse)));
    }


    /**
     * 新增仓库信息表
     * @param warehouse 仓库信息表
     * @return R
     */
    @Inner
    @SysLog("新增仓库信息表")
    @PostMapping("/add")
    public R save(@RequestBody Warehouse warehouse) {
        return new R<>(warehouseService.save(warehouse));
    }

    /**
     * 修改仓库信息表
     * @param warehouse 仓库信息表
     * @return R
     */
    @Inner
    @SysLog("修改仓库信息表")
    @PostMapping("/update")
    public R updateById(@RequestBody Warehouse warehouse) {
        return new R<>(warehouseService.updateById(warehouse));
    }

    /**
     * 通过id删除仓库信息表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除仓库信息表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除仓库信息表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询仓库信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询仓库信息表总记录}")
    @PostMapping("/countByWarehouseReq")
    public R countByWarehouseReq(@RequestBody WarehouseReq warehouseReq) {

        return new R<>(warehouseService.count(Wrappers.query(warehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warehouseReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseReq")
    public R getOneByWarehouseReq(@RequestBody WarehouseReq warehouseReq) {

        return new R<>(warehouseService.getOne(Wrappers.query(warehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<Warehouse> warehouseList) {

        return new R<>(warehouseService.saveOrUpdateBatch(warehouseList));
    }

}
