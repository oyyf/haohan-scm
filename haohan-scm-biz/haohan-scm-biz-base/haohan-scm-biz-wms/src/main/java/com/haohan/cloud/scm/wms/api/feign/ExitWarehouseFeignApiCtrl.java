/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouse;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseReq;
import com.haohan.cloud.scm.wms.service.ExitWarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 出库单
 *
 * @author haohan
 * @date 2019-05-29 13:22:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ExitWarehouse")
@Api(value = "exitwarehouse", tags = "exitwarehouse内部接口服务")
public class ExitWarehouseFeignApiCtrl {

    private final ExitWarehouseService exitWarehouseService;


    /**
     * 通过id查询出库单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(exitWarehouseService.getById(id));
    }


    /**
     * 分页查询 出库单 列表信息
     * @param exitWarehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchExitWarehousePage")
    public R getExitWarehousePage(@RequestBody ExitWarehouseReq exitWarehouseReq) {
        Page page = new Page(exitWarehouseReq.getPageNo(), exitWarehouseReq.getPageSize());
        ExitWarehouse exitWarehouse =new ExitWarehouse();
        BeanUtil.copyProperties(exitWarehouseReq, exitWarehouse);

        return new R<>(exitWarehouseService.page(page, Wrappers.query(exitWarehouse)));
    }


    /**
     * 全量查询 出库单 列表信息
     * @param exitWarehouseReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchExitWarehouseList")
    public R getExitWarehouseList(@RequestBody ExitWarehouseReq exitWarehouseReq) {
        ExitWarehouse exitWarehouse =new ExitWarehouse();
        BeanUtil.copyProperties(exitWarehouseReq, exitWarehouse);

        return new R<>(exitWarehouseService.list(Wrappers.query(exitWarehouse)));
    }


    /**
     * 新增出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @Inner
    @SysLog("新增出库单")
    @PostMapping("/add")
    public R save(@RequestBody ExitWarehouse exitWarehouse) {
        return new R<>(exitWarehouseService.save(exitWarehouse));
    }

    /**
     * 修改出库单
     * @param exitWarehouse 出库单
     * @return R
     */
    @Inner
    @SysLog("修改出库单")
    @PostMapping("/update")
    public R updateById(@RequestBody ExitWarehouse exitWarehouse) {
        return new R<>(exitWarehouseService.updateById(exitWarehouse));
    }

    /**
     * 通过id删除出库单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除出库单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(exitWarehouseService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除出库单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询出库单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询出库单总记录}")
    @PostMapping("/countByExitWarehouseReq")
    public R countByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq) {

        return new R<>(exitWarehouseService.count(Wrappers.query(exitWarehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param exitWarehouseReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据exitWarehouseReq查询一条货位信息表")
    @PostMapping("/getOneByExitWarehouseReq")
    public R getOneByExitWarehouseReq(@RequestBody ExitWarehouseReq exitWarehouseReq) {

        return new R<>(exitWarehouseService.getOne(Wrappers.query(exitWarehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param exitWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ExitWarehouse> exitWarehouseList) {

        return new R<>(exitWarehouseService.saveOrUpdateBatch(exitWarehouseList));
    }

}
