/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import com.haohan.cloud.scm.api.wms.req.WarehouseAllotDetailReq;
import com.haohan.cloud.scm.wms.service.WarehouseAllotDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 库存调拨明细记录
 *
 * @author haohan
 * @date 2019-05-29 13:23:40
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WarehouseAllotDetail")
@Api(value = "warehouseallotdetail", tags = "warehouseallotdetail内部接口服务")
public class WarehouseAllotDetailFeignApiCtrl {

    private final WarehouseAllotDetailService warehouseAllotDetailService;


    /**
     * 通过id查询库存调拨明细记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warehouseAllotDetailService.getById(id));
    }


    /**
     * 分页查询 库存调拨明细记录 列表信息
     * @param warehouseAllotDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseAllotDetailPage")
    public R getWarehouseAllotDetailPage(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {
        Page page = new Page(warehouseAllotDetailReq.getPageNo(), warehouseAllotDetailReq.getPageSize());
        WarehouseAllotDetail warehouseAllotDetail =new WarehouseAllotDetail();
        BeanUtil.copyProperties(warehouseAllotDetailReq, warehouseAllotDetail);

        return new R<>(warehouseAllotDetailService.page(page, Wrappers.query(warehouseAllotDetail)));
    }


    /**
     * 全量查询 库存调拨明细记录 列表信息
     * @param warehouseAllotDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseAllotDetailList")
    public R getWarehouseAllotDetailList(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {
        WarehouseAllotDetail warehouseAllotDetail =new WarehouseAllotDetail();
        BeanUtil.copyProperties(warehouseAllotDetailReq, warehouseAllotDetail);

        return new R<>(warehouseAllotDetailService.list(Wrappers.query(warehouseAllotDetail)));
    }


    /**
     * 新增库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @Inner
    @SysLog("新增库存调拨明细记录")
    @PostMapping("/add")
    public R save(@RequestBody WarehouseAllotDetail warehouseAllotDetail) {
        return new R<>(warehouseAllotDetailService.save(warehouseAllotDetail));
    }

    /**
     * 修改库存调拨明细记录
     * @param warehouseAllotDetail 库存调拨明细记录
     * @return R
     */
    @Inner
    @SysLog("修改库存调拨明细记录")
    @PostMapping("/update")
    public R updateById(@RequestBody WarehouseAllotDetail warehouseAllotDetail) {
        return new R<>(warehouseAllotDetailService.updateById(warehouseAllotDetail));
    }

    /**
     * 通过id删除库存调拨明细记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除库存调拨明细记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseAllotDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除库存调拨明细记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询库存调拨明细记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseAllotDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询库存调拨明细记录总记录}")
    @PostMapping("/countByWarehouseAllotDetailReq")
    public R countByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {

        return new R<>(warehouseAllotDetailService.count(Wrappers.query(warehouseAllotDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseAllotDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warehouseAllotDetailReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseAllotDetailReq")
    public R getOneByWarehouseAllotDetailReq(@RequestBody WarehouseAllotDetailReq warehouseAllotDetailReq) {

        return new R<>(warehouseAllotDetailService.getOne(Wrappers.query(warehouseAllotDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseAllotDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WarehouseAllotDetail> warehouseAllotDetailList) {

        return new R<>(warehouseAllotDetailService.saveOrUpdateBatch(warehouseAllotDetailList));
    }

}
