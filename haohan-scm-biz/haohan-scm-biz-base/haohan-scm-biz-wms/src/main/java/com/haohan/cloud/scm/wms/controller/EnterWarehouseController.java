/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouse;
import com.haohan.cloud.scm.api.wms.req.EnterWarehouseReq;
import com.haohan.cloud.scm.wms.service.EnterWarehouseService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 入库单
 *
 * @author haohan
 * @date 2019-05-29 13:22:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/enterwarehouse" )
@Api(value = "enterwarehouse", tags = "enterwarehouse管理")
public class EnterWarehouseController {

    private final EnterWarehouseService enterWarehouseService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param enterWarehouse 入库单
     * @return
     */
    @GetMapping("/page" )
    public R getEnterWarehousePage(Page page, EnterWarehouse enterWarehouse) {
        return new R<>(enterWarehouseService.page(page, Wrappers.query(enterWarehouse)));
    }


    /**
     * 通过id查询入库单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(enterWarehouseService.getById(id));
    }

    /**
     * 新增入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @SysLog("新增入库单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_enterwarehouse_add')" )
    public R save(@RequestBody EnterWarehouse enterWarehouse) {
        return new R<>(enterWarehouseService.save(enterWarehouse));
    }

    /**
     * 修改入库单
     * @param enterWarehouse 入库单
     * @return R
     */
    @SysLog("修改入库单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_enterwarehouse_edit')" )
    public R updateById(@RequestBody EnterWarehouse enterWarehouse) {
        return new R<>(enterWarehouseService.updateById(enterWarehouse));
    }

    /**
     * 通过id删除入库单
     * @param id id
     * @return R
     */
    @SysLog("删除入库单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_enterwarehouse_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(enterWarehouseService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除入库单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_enterwarehouse_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询入库单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(enterWarehouseService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询入库单总记录}")
    @PostMapping("/countByEnterWarehouseReq")
    public R countByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq) {

        return new R<>(enterWarehouseService.count(Wrappers.query(enterWarehouseReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param enterWarehouseReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据enterWarehouseReq查询一条货位信息表")
    @PostMapping("/getOneByEnterWarehouseReq")
    public R getOneByEnterWarehouseReq(@RequestBody EnterWarehouseReq enterWarehouseReq) {

        return new R<>(enterWarehouseService.getOne(Wrappers.query(enterWarehouseReq), false));
    }


    /**
     * 批量修改OR插入
     * @param enterWarehouseList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_enterwarehouse_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<EnterWarehouse> enterWarehouseList) {

        return new R<>(enterWarehouseService.saveOrUpdateBatch(enterWarehouseList));
    }


}
