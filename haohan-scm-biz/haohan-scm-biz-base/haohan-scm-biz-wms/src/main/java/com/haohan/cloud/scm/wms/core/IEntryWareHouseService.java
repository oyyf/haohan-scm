package com.haohan.cloud.scm.wms.core;

import com.pig4cloud.pigx.common.core.util.R;

/**
 * 仓储入库接口服务
 * @Author simon
 * @Date 2019-05-27
 */
public interface IEntryWareHouseService {


  /**
   * 单品申请入库
   * @param podId
   * @return
   */
  R<Boolean>  applyEntryWareHouseByPodId(String podId);


  /**
   * 采购单申请入库
   * @param purchaseSn
   * @return
   */
  R  applyEntryWareHouseByPuraseSn(String purchaseSn);


  /**
   * 入库单验收
   * @param entrySn
   * @return
   */
  R entryWareHoseAduitByEnterHouseSn(String entrySn);

  /**
   * 入库单明细验收
   * @param detailSn
   * @return
   */
  R entryWareHoseAduitByEnterHouseDetailSn(String detailSn);

}
