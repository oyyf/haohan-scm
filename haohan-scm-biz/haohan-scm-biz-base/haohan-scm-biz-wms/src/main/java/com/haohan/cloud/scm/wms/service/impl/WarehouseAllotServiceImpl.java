/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllot;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.WarehouseAllotMapper;
import com.haohan.cloud.scm.wms.service.WarehouseAllotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 库存调拨记录
 *
 * @author haohan
 * @date 2019-05-13 21:32:14
 */
@Service
public class WarehouseAllotServiceImpl extends ServiceImpl<WarehouseAllotMapper, WarehouseAllot> implements WarehouseAllotService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarehouseAllot entity) {
        if (StrUtil.isEmpty(entity.getWarehouseAllotSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarehouseAllot.class, NumberPrefixConstant.WAREHOUSE_ALLOT_SN_PRE);
            entity.setWarehouseAllotSn(sn);
        }
        return super.save(entity);
    }

}
