/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.Shelf;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.ShelfMapper;
import com.haohan.cloud.scm.wms.service.ShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 货架信息表
 *
 * @author haohan
 * @date 2019-05-13 21:30:10
 */
@Service
public class ShelfServiceImpl extends ServiceImpl<ShelfMapper, Shelf> implements ShelfService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(Shelf entity) {
        if (StrUtil.isEmpty(entity.getShelfSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(Shelf.class, NumberPrefixConstant.SHELF_SN_PRE);
            entity.setShelfSn(sn);
        }
        return super.save(entity);
    }

}
