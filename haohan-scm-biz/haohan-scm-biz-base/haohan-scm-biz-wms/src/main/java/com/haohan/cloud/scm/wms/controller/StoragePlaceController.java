/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.StoragePlace;
import com.haohan.cloud.scm.api.wms.req.StoragePlaceReq;
import com.haohan.cloud.scm.wms.service.StoragePlaceService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 暂存点信息表
 *
 * @author haohan
 * @date 2019-05-29 13:23:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/storageplace" )
@Api(value = "storageplace", tags = "storageplace管理")
public class StoragePlaceController {

    private final StoragePlaceService storagePlaceService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param storagePlace 暂存点信息表
     * @return
     */
    @GetMapping("/page" )
    public R getStoragePlacePage(Page page, StoragePlace storagePlace) {
        return new R<>(storagePlaceService.page(page, Wrappers.query(storagePlace)));
    }


    /**
     * 通过id查询暂存点信息表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(storagePlaceService.getById(id));
    }

    /**
     * 新增暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @SysLog("新增暂存点信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_storageplace_add')" )
    public R save(@RequestBody StoragePlace storagePlace) {
        return new R<>(storagePlaceService.save(storagePlace));
    }

    /**
     * 修改暂存点信息表
     * @param storagePlace 暂存点信息表
     * @return R
     */
    @SysLog("修改暂存点信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_storageplace_edit')" )
    public R updateById(@RequestBody StoragePlace storagePlace) {
        return new R<>(storagePlaceService.updateById(storagePlace));
    }

    /**
     * 通过id删除暂存点信息表
     * @param id id
     * @return R
     */
    @SysLog("删除暂存点信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_storageplace_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(storagePlaceService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除暂存点信息表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_storageplace_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(storagePlaceService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询暂存点信息表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(storagePlaceService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询暂存点信息表总记录}")
    @PostMapping("/countByStoragePlaceReq")
    public R countByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq) {

        return new R<>(storagePlaceService.count(Wrappers.query(storagePlaceReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param storagePlaceReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据storagePlaceReq查询一条货位信息表")
    @PostMapping("/getOneByStoragePlaceReq")
    public R getOneByStoragePlaceReq(@RequestBody StoragePlaceReq storagePlaceReq) {

        return new R<>(storagePlaceService.getOne(Wrappers.query(storagePlaceReq), false));
    }


    /**
     * 批量修改OR插入
     * @param storagePlaceList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_storageplace_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<StoragePlace> storagePlaceList) {

        return new R<>(storagePlaceService.saveOrUpdateBatch(storagePlaceList));
    }


}
