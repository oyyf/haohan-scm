/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.WarehouseInventoryDetailMapper;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 库存盘点明细
 *
 * @author haohan
 * @date 2019-05-13 21:31:59
 */
@Service
public class WarehouseInventoryDetailServiceImpl extends ServiceImpl<WarehouseInventoryDetailMapper, WarehouseInventoryDetail> implements WarehouseInventoryDetailService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarehouseInventoryDetail entity) {
        if (StrUtil.isEmpty(entity.getInventoryDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarehouseInventoryDetail.class, NumberPrefixConstant.WAREHOUSE_INVENTORY_DETAIL_SN_PRE);
            entity.setInventoryDetailSn(sn);
        }
        return super.save(entity);
    }
}
