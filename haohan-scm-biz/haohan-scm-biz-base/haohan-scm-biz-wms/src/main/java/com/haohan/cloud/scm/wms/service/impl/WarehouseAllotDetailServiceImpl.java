/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.wms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.wms.mapper.WarehouseAllotDetailMapper;
import com.haohan.cloud.scm.wms.service.WarehouseAllotDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 库存调拨明细记录
 *
 * @author haohan
 * @date 2019-05-13 21:32:10
 */
@Service
public class WarehouseAllotDetailServiceImpl extends ServiceImpl<WarehouseAllotDetailMapper, WarehouseAllotDetail> implements WarehouseAllotDetailService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarehouseAllotDetail entity) {
        if (StrUtil.isEmpty(entity.getAllotDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarehouseAllotDetail.class, NumberPrefixConstant.WAREHOUSE_ALLOT_DETAIL_SN_PRE);
            entity.setAllotDetailSn(sn);
        }
        return super.save(entity);
    }
}
