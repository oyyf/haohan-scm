/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.wms.entity.WarehouseAllotDetail;

/**
 * 库存调拨明细记录
 *
 * @author haohan
 * @date 2019-05-13 21:32:10
 */
public interface WarehouseAllotDetailMapper extends BaseMapper<WarehouseAllotDetail> {

}
