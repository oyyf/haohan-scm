/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.WarehouseInventoryDetail;
import com.haohan.cloud.scm.api.wms.req.WarehouseInventoryDetailReq;
import com.haohan.cloud.scm.wms.service.WarehouseInventoryDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 库存盘点明细
 *
 * @author haohan
 * @date 2019-05-29 13:23:51
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WarehouseInventoryDetail")
@Api(value = "warehouseinventorydetail", tags = "warehouseinventorydetail内部接口服务")
public class WarehouseInventoryDetailFeignApiCtrl {

    private final WarehouseInventoryDetailService warehouseInventoryDetailService;


    /**
     * 通过id查询库存盘点明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warehouseInventoryDetailService.getById(id));
    }


    /**
     * 分页查询 库存盘点明细 列表信息
     * @param warehouseInventoryDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseInventoryDetailPage")
    public R getWarehouseInventoryDetailPage(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq) {
        Page page = new Page(warehouseInventoryDetailReq.getPageNo(), warehouseInventoryDetailReq.getPageSize());
        WarehouseInventoryDetail warehouseInventoryDetail =new WarehouseInventoryDetail();
        BeanUtil.copyProperties(warehouseInventoryDetailReq, warehouseInventoryDetail);

        return new R<>(warehouseInventoryDetailService.page(page, Wrappers.query(warehouseInventoryDetail)));
    }


    /**
     * 全量查询 库存盘点明细 列表信息
     * @param warehouseInventoryDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarehouseInventoryDetailList")
    public R getWarehouseInventoryDetailList(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq) {
        WarehouseInventoryDetail warehouseInventoryDetail =new WarehouseInventoryDetail();
        BeanUtil.copyProperties(warehouseInventoryDetailReq, warehouseInventoryDetail);

        return new R<>(warehouseInventoryDetailService.list(Wrappers.query(warehouseInventoryDetail)));
    }


    /**
     * 新增库存盘点明细
     * @param warehouseInventoryDetail 库存盘点明细
     * @return R
     */
    @Inner
    @SysLog("新增库存盘点明细")
    @PostMapping("/add")
    public R save(@RequestBody WarehouseInventoryDetail warehouseInventoryDetail) {
        return new R<>(warehouseInventoryDetailService.save(warehouseInventoryDetail));
    }

    /**
     * 修改库存盘点明细
     * @param warehouseInventoryDetail 库存盘点明细
     * @return R
     */
    @Inner
    @SysLog("修改库存盘点明细")
    @PostMapping("/update")
    public R updateById(@RequestBody WarehouseInventoryDetail warehouseInventoryDetail) {
        return new R<>(warehouseInventoryDetailService.updateById(warehouseInventoryDetail));
    }

    /**
     * 通过id删除库存盘点明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除库存盘点明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warehouseInventoryDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除库存盘点明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseInventoryDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询库存盘点明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warehouseInventoryDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warehouseInventoryDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询库存盘点明细总记录}")
    @PostMapping("/countByWarehouseInventoryDetailReq")
    public R countByWarehouseInventoryDetailReq(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq) {

        return new R<>(warehouseInventoryDetailService.count(Wrappers.query(warehouseInventoryDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warehouseInventoryDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warehouseInventoryDetailReq查询一条货位信息表")
    @PostMapping("/getOneByWarehouseInventoryDetailReq")
    public R getOneByWarehouseInventoryDetailReq(@RequestBody WarehouseInventoryDetailReq warehouseInventoryDetailReq) {

        return new R<>(warehouseInventoryDetailService.getOne(Wrappers.query(warehouseInventoryDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warehouseInventoryDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WarehouseInventoryDetail> warehouseInventoryDetailList) {

        return new R<>(warehouseInventoryDetailService.saveOrUpdateBatch(warehouseInventoryDetailList));
    }

}
