/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseDetailReq;
import com.haohan.cloud.scm.wms.core.IScmExitWarehouseService;
import com.haohan.cloud.scm.wms.service.ExitWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-29 13:22:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ExitWarehouseDetail")
@Api(value = "exitwarehousedetail", tags = "exitwarehousedetail内部接口服务")
public class ExitWarehouseDetailFeignApiCtrl {

    private final ExitWarehouseDetailService exitWarehouseDetailService;
    private final IScmExitWarehouseService scmExitWarehouseService;


    /**
     * 通过id查询出库单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(exitWarehouseDetailService.getById(id));
    }


    /**
     * 分页查询 出库单明细 列表信息
     * @param exitWarehouseDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchExitWarehouseDetailPage")
    public R getExitWarehouseDetailPage(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {
        Page page = new Page(exitWarehouseDetailReq.getPageNo(), exitWarehouseDetailReq.getPageSize());
        ExitWarehouseDetail exitWarehouseDetail =new ExitWarehouseDetail();
        BeanUtil.copyProperties(exitWarehouseDetailReq, exitWarehouseDetail);

        return new R<>(exitWarehouseDetailService.page(page, Wrappers.query(exitWarehouseDetail)));
    }


    /**
     * 全量查询 出库单明细 列表信息
     * @param exitWarehouseDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchExitWarehouseDetailList")
    public R getExitWarehouseDetailList(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {
        ExitWarehouseDetail exitWarehouseDetail =new ExitWarehouseDetail();
        BeanUtil.copyProperties(exitWarehouseDetailReq, exitWarehouseDetail);

        return new R<>(exitWarehouseDetailService.list(Wrappers.query(exitWarehouseDetail)));
    }


    /**
     * 新增出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @Inner
    @SysLog("新增出库单明细")
    @PostMapping("/add")
    public R save(@RequestBody ExitWarehouseDetail exitWarehouseDetail) {
        return new R<>(exitWarehouseDetailService.save(exitWarehouseDetail));
    }

    /**
     * 修改出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @Inner
    @SysLog("修改出库单明细")
    @PostMapping("/update")
    public R updateById(@RequestBody ExitWarehouseDetail exitWarehouseDetail) {
        return new R<>(exitWarehouseDetailService.updateById(exitWarehouseDetail));
    }

    /**
     * 通过id删除出库单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除出库单明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(exitWarehouseDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除出库单明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询出库单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询出库单明细总记录}")
    @PostMapping("/countByExitWarehouseDetailReq")
    public R countByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {

        return new R<>(exitWarehouseDetailService.count(Wrappers.query(exitWarehouseDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据exitWarehouseDetailReq查询一条货位信息表")
    @PostMapping("/getOneByExitWarehouseDetailReq")
    public R getOneByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {

        return new R<>(exitWarehouseDetailService.getOne(Wrappers.query(exitWarehouseDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param exitWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ExitWarehouseDetail> exitWarehouseDetailList) {

        return new R<>(exitWarehouseDetailService.saveOrUpdateBatch(exitWarehouseDetailList));
    }

    @Inner
    @PostMapping("/createExitWarehouseDetail")
    @SysLog("创建出库单明细(锁库存)")
    @ApiOperation(value = "创建出库单明细" , notes = "创建出库单明细(锁库存)")
    public R<ExitWarehouseDetail> createExitWarehouseDetail(@RequestBody ExitWarehouseDetail detail){
        return new R<ExitWarehouseDetail>(scmExitWarehouseService.createExitWarehouseDetail(detail));
    }

}
