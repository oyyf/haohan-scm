/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.wms.entity.Pallet;

/**
 * 托盘信息表
 *
 * @author haohan
 * @date 2019-05-13 21:29:29
 */
public interface PalletService extends IService<Pallet> {

}
