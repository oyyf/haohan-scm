package com.haohan.cloud.scm.wms.core;

import com.haohan.cloud.scm.api.purchase.req.PurchaseProductEntryReq;
import com.haohan.cloud.scm.api.wms.entity.EnterWarehouseDetail;
import com.haohan.cloud.scm.api.wms.entity.Pallet;
import com.haohan.cloud.scm.api.wms.req.*;
import com.haohan.cloud.scm.api.wms.resp.EnterWarehouseDetailResp;
import com.haohan.cloud.scm.api.wms.resp.QueryEnterDetailResp;

import java.util.List;

/**
 * @author xwx
 * @date 2019/6/13
 */
public interface IScmEnterWarehouseService {

    /**
     * 新增入库单明细 根据采购单明细
     *
     * @param req
     * @return
     */
    EnterWarehouseDetail addEnterDetailByPurchaseDetail(AddEnterDetailReq req);

    /**
     * 新增入库单 根据入库单明细
     *
     * @param req
     * @return
     */
    Boolean createEnterWarehouse(CreateEnterWarehouseReq req);

    /**
     * 新增入库单 入库存储
     *
     * @param req
     * @return
     */
    Boolean warehouseStorage(WarehouseStorageReq req);

    /**
     * 获取托盘
     *
     * @param req
     * @return
     */
    Pallet gainPallet(GainPalletReq req);

    /**
     * 货品入库操作 货品放置在 托盘 上
     *
     * @param req
     * @return
     */
    Pallet productEnter(ProductEnterReq req);

    /**
     * 新增入库单(入库单明细) 根据货品信息
     *
     * @param req
     * @return
     */
    Boolean createEnterWarehouseByProductInfo(CreateEnterByProductInfoReq req);

    /**
     * 货品上架
     *
     * @param req
     * @return
     */
    Boolean productPutShelf(ProductPutShelfReq req);

    /**
     * 货品下架(货位、货品数量全下)
     *
     * @param req
     * @return
     */
    Boolean productRemoveShelf(ProductRemoveShelfReq req);

    /**
     * 货品下架(货位、货品数量部分下架)
     *
     * @param req
     * @return
     */
    Boolean productRemoveSection(ProductRemoveSectionReq req);


    /**
     * 采购明细货品 实际入库
     *
     * @param req
     * @return
     */
    Boolean purchaseProductEntry(PurchaseProductEntryReq req);

    /**
     * 新增入库单（入库单明细）根据采购单明细
     *
     * @param req
     * @return
     */
    Boolean enterWarehouseByOrderDetail(EnterWarehouseByOrderDetailReq req);

    /**
     * 查询入库单明细（包含货品信息）根据入库单号
     *
     * @param req
     * @return
     */
    List<EnterWarehouseDetailResp> queryEnterWarehouseDetail(QueryEnterWarehouseDetailReq req);

    /**
     * 根据商品创建入库单、入库单明细明细、货品信息
     *
     * @param req
     * @return
     */
    Boolean createEnterWarehouse(CreateEnterWarehouseDetailReq req);

    /**
     * 修改入库单明细和货品信息
     * @param req
     * @return
     */
    Boolean updateEnterWarehouseDetail(UpdateEnterWarehouseDetailReq req);

    /**
     * 查寻订单退货入库单详情
     * @param req
     * @return
     */
    QueryEnterDetailResp queryEnterDetail(EnterWarehouseReq req);

    /**
     * 编辑入库单
     * @param req
     * @return
     */
    Boolean editEnterWarehouse(EditEnterWarehouseReq req);
}
