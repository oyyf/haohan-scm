/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.wms.entity.ExitWarehouseDetail;
import com.haohan.cloud.scm.api.wms.req.ExitWarehouseDetailReq;
import com.haohan.cloud.scm.wms.service.ExitWarehouseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 出库单明细
 *
 * @author haohan
 * @date 2019-05-29 13:22:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/exitwarehousedetail" )
@Api(value = "exitwarehousedetail", tags = "exitwarehousedetail管理")
public class ExitWarehouseDetailController {

    private final ExitWarehouseDetailService exitWarehouseDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param exitWarehouseDetail 出库单明细
     * @return
     */
    @GetMapping("/page" )
    public R getExitWarehouseDetailPage(Page page, ExitWarehouseDetail exitWarehouseDetail) {
        return new R<>(exitWarehouseDetailService.page(page, Wrappers.query(exitWarehouseDetail)));
    }


    /**
     * 通过id查询出库单明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(exitWarehouseDetailService.getById(id));
    }

    /**
     * 新增出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @SysLog("新增出库单明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_exitwarehousedetail_add')" )
    public R save(@RequestBody ExitWarehouseDetail exitWarehouseDetail) {
        return new R<>(exitWarehouseDetailService.save(exitWarehouseDetail));
    }

    /**
     * 修改出库单明细
     * @param exitWarehouseDetail 出库单明细
     * @return R
     */
    @SysLog("修改出库单明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_exitwarehousedetail_edit')" )
    public R updateById(@RequestBody ExitWarehouseDetail exitWarehouseDetail) {
        return new R<>(exitWarehouseDetailService.updateById(exitWarehouseDetail));
    }

    /**
     * 通过id删除出库单明细
     * @param id id
     * @return R
     */
    @SysLog("删除出库单明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_exitwarehousedetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(exitWarehouseDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除出库单明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_exitwarehousedetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询出库单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(exitWarehouseDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询出库单明细总记录}")
    @PostMapping("/countByExitWarehouseDetailReq")
    public R countByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {

        return new R<>(exitWarehouseDetailService.count(Wrappers.query(exitWarehouseDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param exitWarehouseDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据exitWarehouseDetailReq查询一条货位信息表")
    @PostMapping("/getOneByExitWarehouseDetailReq")
    public R getOneByExitWarehouseDetailReq(@RequestBody ExitWarehouseDetailReq exitWarehouseDetailReq) {

        return new R<>(exitWarehouseDetailService.getOne(Wrappers.query(exitWarehouseDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param exitWarehouseDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_exitwarehousedetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<ExitWarehouseDetail> exitWarehouseDetailList) {

        return new R<>(exitWarehouseDetailService.saveOrUpdateBatch(exitWarehouseDetailList));
    }


}
