/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.CostControl;

/**
 * 成本管控
 *
 * @author haohan
 * @date 2019-05-13 20:36:45
 */
public interface CostControlService extends IService<CostControl> {

}
