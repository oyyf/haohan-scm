/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.CostControl;
import com.haohan.cloud.scm.api.opc.req.CostControlReq;
import com.haohan.cloud.scm.opc.service.CostControlService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 成本管控
 *
 * @author haohan
 * @date 2019-05-30 10:16:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/costcontrol" )
@Api(value = "costcontrol", tags = "costcontrol管理")
public class CostControlController {

    private final CostControlService costControlService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param costControl 成本管控
     * @return
     */
    @GetMapping("/page" )
    public R getCostControlPage(Page page, CostControl costControl) {
        return new R<>(costControlService.page(page, Wrappers.query(costControl)));
    }


    /**
     * 通过id查询成本管控
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(costControlService.getById(id));
    }

    /**
     * 新增成本管控
     * @param costControl 成本管控
     * @return R
     */
    @SysLog("新增成本管控" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_costcontrol_add')" )
    public R save(@RequestBody CostControl costControl) {
        return new R<>(costControlService.save(costControl));
    }

    /**
     * 修改成本管控
     * @param costControl 成本管控
     * @return R
     */
    @SysLog("修改成本管控" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_costcontrol_edit')" )
    public R updateById(@RequestBody CostControl costControl) {
        return new R<>(costControlService.updateById(costControl));
    }

    /**
     * 通过id删除成本管控
     * @param id id
     * @return R
     */
    @SysLog("删除成本管控" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_costcontrol_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(costControlService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除成本管控")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_costcontrol_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(costControlService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询成本管控")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(costControlService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param costControlReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询成本管控总记录}")
    @PostMapping("/countByCostControlReq")
    public R countByCostControlReq(@RequestBody CostControlReq costControlReq) {

        return new R<>(costControlService.count(Wrappers.query(costControlReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param costControlReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据costControlReq查询一条货位信息表")
    @PostMapping("/getOneByCostControlReq")
    public R getOneByCostControlReq(@RequestBody CostControlReq costControlReq) {

        return new R<>(costControlService.getOne(Wrappers.query(costControlReq), false));
    }


    /**
     * 批量修改OR插入
     * @param costControlList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_costcontrol_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CostControl> costControlList) {

        return new R<>(costControlService.saveOrUpdateBatch(costControlList));
    }


}
