/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.TradeMatch;
import com.haohan.cloud.scm.api.opc.req.TradeMatchReq;
import com.haohan.cloud.scm.opc.service.TradeMatchService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 交易匹配
 *
 * @author haohan
 * @date 2019-05-30 10:21:33
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tradematch" )
@Api(value = "tradematch", tags = "tradematch管理")
public class TradeMatchController {

    private final TradeMatchService tradeMatchService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param tradeMatch 交易匹配
     * @return
     */
    @GetMapping("/page" )
    public R getTradeMatchPage(Page page, TradeMatch tradeMatch) {
        return new R<>(tradeMatchService.page(page, Wrappers.query(tradeMatch)));
    }


    /**
     * 通过id查询交易匹配
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(tradeMatchService.getById(id));
    }

    /**
     * 新增交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @SysLog("新增交易匹配" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_tradematch_add')" )
    public R save(@RequestBody TradeMatch tradeMatch) {
        return new R<>(tradeMatchService.save(tradeMatch));
    }

    /**
     * 修改交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @SysLog("修改交易匹配" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_tradematch_edit')" )
    public R updateById(@RequestBody TradeMatch tradeMatch) {
        return new R<>(tradeMatchService.updateById(tradeMatch));
    }

    /**
     * 通过id删除交易匹配
     * @param id id
     * @return R
     */
    @SysLog("删除交易匹配" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_tradematch_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(tradeMatchService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除交易匹配")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_tradematch_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(tradeMatchService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询交易匹配")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(tradeMatchService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询交易匹配总记录}")
    @PostMapping("/countByTradeMatchReq")
    public R countByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq) {

        return new R<>(tradeMatchService.count(Wrappers.query(tradeMatchReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据tradeMatchReq查询一条货位信息表")
    @PostMapping("/getOneByTradeMatchReq")
    public R getOneByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq) {

        return new R<>(tradeMatchService.getOne(Wrappers.query(tradeMatchReq), false));
    }


    /**
     * 批量修改OR插入
     * @param tradeMatchList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_tradematch_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<TradeMatch> tradeMatchList) {

        return new R<>(tradeMatchService.saveOrUpdateBatch(tradeMatchList));
    }


}
