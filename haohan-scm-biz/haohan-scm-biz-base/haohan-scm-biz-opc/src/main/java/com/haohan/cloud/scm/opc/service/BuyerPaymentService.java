/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;

import java.time.LocalDate;

/**
 * 采购商货款统计
 *
 * @author haohan
 * @date 2019-05-13 20:35:09
 */
public interface BuyerPaymentService extends IService<BuyerPayment> {

    /**
     * 查询到期的账单数 (未结算)
     * @param endDate
     * @param buyerId
     * @param type  状态 是否结算 0否1是  null默认否
     * @return
     */
    int countBill(LocalDate endDate, String buyerId, YesNoEnum type);

    /**
     * 根据 编号查询
     * @param payment 可传pmId
     * @return
     */
    BuyerPayment fetchBySn(BuyerPayment payment);
}
