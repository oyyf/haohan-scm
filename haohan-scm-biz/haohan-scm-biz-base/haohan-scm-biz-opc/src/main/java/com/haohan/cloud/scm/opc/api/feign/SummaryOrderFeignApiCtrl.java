/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.req.SummaryOrderReq;
import com.haohan.cloud.scm.opc.service.SummaryOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购单汇总
 *
 * @author haohan
 * @date 2019-05-30 10:21:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SummaryOrder")
@Api(value = "summaryorder", tags = "summaryorder内部接口服务")
public class SummaryOrderFeignApiCtrl {

    private final SummaryOrderService summaryOrderService;


    /**
     * 通过id查询采购单汇总
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(summaryOrderService.getById(id));
    }


    /**
     * 分页查询 采购单汇总 列表信息
     * @param summaryOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSummaryOrderPage")
    public R getSummaryOrderPage(@RequestBody SummaryOrderReq summaryOrderReq) {
        Page page = new Page(summaryOrderReq.getPageNo(), summaryOrderReq.getPageSize());
        SummaryOrder summaryOrder =new SummaryOrder();
        BeanUtil.copyProperties(summaryOrderReq, summaryOrder);

        return new R<>(summaryOrderService.page(page, Wrappers.query(summaryOrder)));
    }


    /**
     * 全量查询 采购单汇总 列表信息
     * @param summaryOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSummaryOrderList")
    public R getSummaryOrderList(@RequestBody SummaryOrderReq summaryOrderReq) {
        SummaryOrder summaryOrder =new SummaryOrder();
        BeanUtil.copyProperties(summaryOrderReq, summaryOrder);

        return new R<>(summaryOrderService.list(Wrappers.query(summaryOrder)));
    }


    /**
     * 新增采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @Inner
    @SysLog("新增采购单汇总")
    @PostMapping("/add")
    public R save(@RequestBody SummaryOrder summaryOrder) {
        return new R<>(summaryOrderService.save(summaryOrder));
    }

    /**
     * 修改采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @Inner
    @SysLog("修改采购单汇总")
    @PostMapping("/update")
    public R updateById(@RequestBody SummaryOrder summaryOrder) {
        return new R<>(summaryOrderService.updateById(summaryOrder));
    }

    /**
     * 通过id删除采购单汇总
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购单汇总")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(summaryOrderService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购单汇总")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(summaryOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购单汇总")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(summaryOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购单汇总总记录}")
    @PostMapping("/countBySummaryOrderReq")
    public R countBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq) {

        return new R<>(summaryOrderService.count(Wrappers.query(summaryOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据summaryOrderReq查询一条货位信息表")
    @PostMapping("/getOneBySummaryOrderReq")
    public R getOneBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq) {

        return new R<>(summaryOrderService.getOne(Wrappers.query(summaryOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param summaryOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SummaryOrder> summaryOrderList) {

        return new R<>(summaryOrderService.saveOrUpdateBatch(summaryOrderList));
    }

}
