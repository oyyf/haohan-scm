/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.opc.req.payment.SupplierPaymentReq;
import com.haohan.cloud.scm.opc.core.ISupplierPaymentCoreService;
import com.haohan.cloud.scm.opc.service.SupplierPaymentService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:13:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplierPayment")
@Api(value = "supplierpayment", tags = "supplierpayment内部接口服务")
public class SupplierPaymentFeignApiCtrl {

    private final SupplierPaymentService supplierPaymentService;
    private final ISupplierPaymentCoreService scmSupplierPaymentService;

    /**
     * 通过id查询供应商货款统计
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierPaymentService.getById(id));
    }


    /**
     * 分页查询 供应商货款统计 列表信息
     * @param supplierPaymentReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierPaymentPage")
    public R getSupplierPaymentPage(@RequestBody SupplierPaymentReq supplierPaymentReq) {
        Page page = new Page(supplierPaymentReq.getPageNo(), supplierPaymentReq.getPageSize());
        SupplierPayment supplierPayment =new SupplierPayment();
        BeanUtil.copyProperties(supplierPaymentReq, supplierPayment);

        return new R<>(supplierPaymentService.page(page, Wrappers.query(supplierPayment)));
    }


    /**
     * 全量查询 供应商货款统计 列表信息
     * @param supplierPaymentReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierPaymentList")
    public R getSupplierPaymentList(@RequestBody SupplierPaymentReq supplierPaymentReq) {
        SupplierPayment supplierPayment =new SupplierPayment();
        BeanUtil.copyProperties(supplierPaymentReq, supplierPayment);

        return new R<>(supplierPaymentService.list(Wrappers.query(supplierPayment)));
    }


    /**
     * 新增供应商货款统计
     * @param supplierPayment 供应商货款统计
     * @return R
     */
    @Inner
    @SysLog("新增供应商货款统计")
    @PostMapping("/add")
    public R save(@RequestBody SupplierPayment supplierPayment) {
        return new R<>(supplierPaymentService.save(supplierPayment));
    }

    /**
     * 修改供应商货款统计
     * @param supplierPayment 供应商货款统计
     * @return R
     */
    @Inner
    @SysLog("修改供应商货款统计")
    @PostMapping("/update")
    public R updateById(@RequestBody SupplierPayment supplierPayment) {
        return new R<>(supplierPaymentService.updateById(supplierPayment));
    }

    /**
     * 通过id删除供应商货款统计
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应商货款统计")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierPaymentService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除供应商货款统计")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierPaymentService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询供应商货款统计")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierPaymentService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierPaymentReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应商货款统计总记录}")
    @PostMapping("/countBySupplierPaymentReq")
    public R countBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq) {

        return new R<>(supplierPaymentService.count(Wrappers.query(supplierPaymentReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierPaymentReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierPaymentReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierPaymentReq")
    public R getOneBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq) {

        return new R<>(supplierPaymentService.getOne(Wrappers.query(supplierPaymentReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierPaymentList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SupplierPayment> supplierPaymentList) {

        return new R<>(supplierPaymentService.saveOrUpdateBatch(supplierPaymentList));
    }

    /**
     * 查询应付账单 是否通过审核 带商家名称
     * @param supplierPayment 必需 pmId / supplierPaymentId
     * @return 不为确认状态时 返回null
     */
    @Inner
    @PostMapping("/checkPayment")
    public R<SupplierPayment> checkPayment(@RequestBody SupplierPayment supplierPayment) {
        if (StrUtil.isEmpty(supplierPayment.getPmId()) || StrUtil.isEmpty(supplierPayment.getSupplierPaymentId())) {
            return R.failed("缺少必需参数pmId/supplierPaymentId");
        }
        return R.ok(scmSupplierPaymentService.checkPayment(supplierPayment));
    }

}
