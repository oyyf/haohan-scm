/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;

import java.util.List;

/**
 * 结算单账单关系表
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
public interface SettlementRelationService extends IService<SettlementRelation> {

    /**
     * 根据 结算记录编号/账单编号 删除关联关系
     * @param settlementSn
     * @param paymentSn
     * @return
     */
    boolean delete(String settlementSn, String paymentSn);

    /**
     * 查询 同一结算单的账单关系
     * @param settlementSn
     * @return
     */
    List<SettlementRelation> fetchListBySn(String settlementSn);
}
