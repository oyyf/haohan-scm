/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.TradeMatch;

/**
 * 交易匹配
 *
 * @author haohan
 * @date 2019-05-13 20:39:36
 */
public interface TradeMatchService extends IService<TradeMatch> {

}
