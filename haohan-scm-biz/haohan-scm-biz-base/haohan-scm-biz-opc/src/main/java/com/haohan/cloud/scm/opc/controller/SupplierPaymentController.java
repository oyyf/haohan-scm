/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.opc.req.payment.SupplierPaymentListReq;
import com.haohan.cloud.scm.api.opc.req.payment.SupplierPaymentReq;
import com.haohan.cloud.scm.opc.core.ISupplierPaymentCoreService;
import com.haohan.cloud.scm.opc.service.SupplierPaymentService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:13:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/supplierpayment" )
@Api(value = "supplierpayment", tags = "supplierpayment管理")
public class SupplierPaymentController {

    private final SupplierPaymentService supplierPaymentService;
    private final ISupplierPaymentCoreService scmSupplierPaymentService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param supplierPayment 供应商货款统计
     * @return
     */
    @GetMapping("/page" )
    public R getSupplierPaymentPage(Page page, SupplierPayment supplierPayment) {
        return new R<>(supplierPaymentService.page(page, Wrappers.query(supplierPayment)));
    }

    /**
     * 分页查询
     * @param page 分页对象
     * @param req 供应商货款统计
     * @return
     */
    @GetMapping("/fetchPage" )
    public R fetchPage(Page<SupplierPayment> page, @Validated SupplierPaymentListReq req) {
        QueryWrapper<SupplierPayment> queryWrapper = Wrappers.query(req.transTo());
        // 商家名称/客户名称  起止时间 / 订单状态
        queryWrapper.orderByDesc("supplier_payment_id")
                .lambda()
                .like(StrUtil.isNotEmpty(req.getMerchantName()), SupplierPayment::getMerchantName, req.getMerchantName())
                .like(StrUtil.isNotEmpty(req.getCustomerName()), SupplierPayment::getCustomerName, req.getCustomerName());
        //  起止时间
        if (null != req.getBeginDate() && null != req.getEndDate()) {
            queryWrapper
                    .lambda()
                    .ge(SupplierPayment::getSupplyDate, req.getBeginDate())
                    .le(SupplierPayment::getSupplyDate, req.getEndDate());
        }
        return R.ok(supplierPaymentService.page(page, queryWrapper));
    }


    /**
     * 通过id查询供应商货款统计
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(supplierPaymentService.getById(id));
    }

    /**
     * 新增供应商货款统计
     * @param supplierPayment 供应商货款统计
     * @return R
     */
    @SysLog("新增供应商货款统计" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_supplierpayment_add')" )
    public R save(@RequestBody SupplierPayment supplierPayment) {
        return R.failed("不支持");
    }

    /**
     * 修改供应商货款统计
     * @param supplierPayment 供应商货款统计
     * @return R
     */
    @SysLog("修改供应商货款统计" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_supplierpayment_edit')" )
    public R updateById(@RequestBody SupplierPayment supplierPayment) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除供应商货款统计
     * @param id id
     * @return R
     */
    @SysLog("删除供应商货款统计" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('supply_supplierpayment_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除供应商货款统计")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('supply_supplierpayment_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询供应商货款统计")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierPaymentService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierPaymentReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countBySupplierPaymentReq")
    public R countBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq) {

        return new R<>(supplierPaymentService.count(Wrappers.query(supplierPaymentReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierPaymentReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/getOneBySupplierPaymentReq")
    public R getOneBySupplierPaymentReq(@RequestBody SupplierPaymentReq supplierPaymentReq) {

        return new R<>(supplierPaymentService.getOne(Wrappers.query(supplierPaymentReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierPaymentList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('supply_supplierpayment_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SupplierPayment> supplierPaymentList) {

        return R.failed("不支持");
    }


}
