/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;

/**
 * 采购商货款统计
 *
 * @author haohan
 * @date 2019-05-13 20:35:09
 */
public interface BuyerPaymentMapper extends BaseMapper<BuyerPayment> {

}
