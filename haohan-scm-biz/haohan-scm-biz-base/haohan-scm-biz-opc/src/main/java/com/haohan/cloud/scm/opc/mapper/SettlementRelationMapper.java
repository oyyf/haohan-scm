/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;

/**
 * 结算单账单关系表
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
public interface SettlementRelationMapper extends BaseMapper<SettlementRelation> {

}
