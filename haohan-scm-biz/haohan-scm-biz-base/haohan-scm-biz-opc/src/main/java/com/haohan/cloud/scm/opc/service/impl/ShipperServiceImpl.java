/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.opc.mapper.ShipperMapper;
import com.haohan.cloud.scm.opc.service.ShipperService;
import org.springframework.stereotype.Service;

/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
@Service
public class ShipperServiceImpl extends ServiceImpl<ShipperMapper, Shipper> implements ShipperService {

}
