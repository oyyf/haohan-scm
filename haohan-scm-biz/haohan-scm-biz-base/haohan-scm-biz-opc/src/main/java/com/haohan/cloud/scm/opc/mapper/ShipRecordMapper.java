/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;

/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
public interface ShipRecordMapper extends BaseMapper<ShipRecord> {

}
