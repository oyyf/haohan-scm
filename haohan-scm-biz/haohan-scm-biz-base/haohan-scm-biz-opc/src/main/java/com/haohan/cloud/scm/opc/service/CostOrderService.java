/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.CostOrder;

/**
 * 成本核算单
 *
 * @author haohan
 * @date 2019-05-13 20:36:32
 */
public interface CostOrderService extends IService<CostOrder> {

}
