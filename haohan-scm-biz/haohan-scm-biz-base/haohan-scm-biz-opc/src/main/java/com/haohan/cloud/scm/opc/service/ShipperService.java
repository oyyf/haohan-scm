/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.Shipper;

/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
public interface ShipperService extends IService<Shipper> {

}
