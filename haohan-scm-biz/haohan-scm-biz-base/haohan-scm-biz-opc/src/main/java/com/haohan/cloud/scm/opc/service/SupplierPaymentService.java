/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;

/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-13 17:45:06
 */
public interface SupplierPaymentService extends IService<SupplierPayment> {

    /**
     * 根据编号查询
     * @param payment
     * @return
     */
    SupplierPayment fetchBySn(SupplierPayment payment);
}
