/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.TradeOrderReq;
import com.haohan.cloud.scm.opc.service.TradeOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 交易订单
 *
 * @author haohan
 * @date 2019-05-30 10:21:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tradeorder" )
@Api(value = "tradeorder", tags = "tradeorder管理")
public class TradeOrderController {

    private final TradeOrderService tradeOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param tradeOrder 交易订单
     * @return
     */
    @GetMapping("/page" )
    public R getTradeOrderPage(Page page, TradeOrder tradeOrder) {
        return new R<>(tradeOrderService.page(page, Wrappers.query(tradeOrder)));
    }


    /**
     * 通过id查询交易订单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(tradeOrderService.getById(id));
    }

    /**
     * 新增交易订单
     * @param tradeOrder 交易订单
     * @return R
     */
    @SysLog("新增交易订单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_tradeorder_add')" )
    public R save(@RequestBody TradeOrder tradeOrder) {
        return new R<>(tradeOrderService.save(tradeOrder));
    }

    /**
     * 修改交易订单
     * @param tradeOrder 交易订单
     * @return R
     */
    @SysLog("修改交易订单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_tradeorder_edit')" )
    public R updateById(@RequestBody TradeOrder tradeOrder) {
        return new R<>(tradeOrderService.updateById(tradeOrder));
    }

    /**
     * 通过id删除交易订单
     * @param id id
     * @return R
     */
    @SysLog("删除交易订单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_tradeorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(tradeOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除交易订单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_tradeorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(tradeOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询交易订单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(tradeOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询交易订单总记录}")
    @PostMapping("/countByTradeOrderReq")
    public R countByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq) {

        return new R<>(tradeOrderService.count(Wrappers.query(tradeOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param tradeOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据tradeOrderReq查询一条货位信息表")
    @PostMapping("/getOneByTradeOrderReq")
    public R getOneByTradeOrderReq(@RequestBody TradeOrderReq tradeOrderReq) {

        return new R<>(tradeOrderService.getOne(Wrappers.query(tradeOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param tradeOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_tradeorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<TradeOrder> tradeOrderList) {

        return new R<>(tradeOrderService.saveOrUpdateBatch(tradeOrderList));
    }


}
