package com.haohan.cloud.scm.opc.core.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.common.TaskStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.AllocateTypeEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.PdsSummaryStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.SummaryStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.constant.enums.purchase.*;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.DetailSummaryFlagEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.feign.GoodsFeignService;
import com.haohan.cloud.scm.api.goods.feign.GoodsModelFeignService;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.*;
import com.haohan.cloud.scm.api.opc.trans.OpcTrans;
import com.haohan.cloud.scm.api.product.entity.ProductInfo;
import com.haohan.cloud.scm.api.product.feign.ProductInfoFeignService;
import com.haohan.cloud.scm.api.product.req.ProductInfoReq;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderDetailFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseOrderFeignService;
import com.haohan.cloud.scm.api.purchase.feign.PurchaseTaskFeignService;
import com.haohan.cloud.scm.api.purchase.req.AddPurchaseOrderReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseOrderResp;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.feign.BuyOrderDetailFeignService;
import com.haohan.cloud.scm.api.saleb.feign.SalebFeignService;
import com.haohan.cloud.scm.api.saleb.req.SalebBuyOrderDetailQueryReq;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.req.AddOfferOrderReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.core.IScmOpcOperationService;
import com.haohan.cloud.scm.opc.service.SummaryOrderService;
import com.haohan.cloud.scm.opc.service.TradeOrderService;
import com.haohan.cloud.scm.opc.utils.ScmOpcUtils;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2019/5/27
 */
@Service
@Log4j2
public class ScmOpcOperationServiceImpl implements IScmOpcOperationService {

    @Autowired
    @Lazy
    private SummaryOrderService summaryOrderService;
    @Autowired
    @Lazy
    private SalebFeignService salebFeignService;
    @Autowired
    @Lazy
    private GoodsModelFeignService goodsModelFeignService;
    @Autowired
    @Lazy
    private GoodsFeignService goodsFeignService;
    @Autowired
    @Lazy
    private PurchaseOrderDetailFeignService purchaseOrderDetailFeignService;
    @Autowired
    @Lazy
    private PurchaseOrderFeignService purchaseOrderFeignService;
    @Autowired
    @Lazy
    private OfferOrderFeignService offerOrderFeignService;
    @Autowired
    @Lazy
    private PurchaseTaskFeignService purchaseTaskFeignService;
    @Autowired
    @Lazy
    private BuyOrderDetailFeignService buyOrderDetailFeignService;
    @Autowired
    @Lazy
    private TradeOrderService tradeOrderService;
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Autowired
    @Lazy
    private ProductInfoFeignService productInfoFeignService;

    @Autowired
    @Lazy
    private ScmOpcUtils scmOpcUtils;

    @Override
    public SummaryOrder queryGoodsNeeds(SalebSummaryGoodsNumReq summaryNumReq) {
        // 汇总商品数量 采购单明细
        // 根据 商品规格id  送货日期 批次
        // 未汇总;采购状态为 待处理
        summaryNumReq.setSummaryFlag(YesNoEnum.no.getType());
        summaryNumReq.setStatus(BuyOrderStatusEnum.wait.getType());
        R resp = salebFeignService.summaryGoodsNum(summaryNumReq, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp)) {
            throw new EmptyDataException("无待汇总信息");
        }
        SummaryOrder result = BeanUtil.toBean(resp.getData(), SummaryOrder.class);
        return result;
    }

    /**
     * 商品需求汇总 创建汇总单
     * 根据需采购商品 创建 汇总单数据(单商品) summaryOrder
     * 必需参数: pmId/buySeq/goodsModelId/deliveryTime/allocateType
     *   可选参数: summaryStatus
     *
     *   若为已汇总的汇总单(id不为空) 则更新
     * @param req
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SummaryOrder createSummaryByGoodsNeeds(SummaryOrder req) {
        // 查询 客户 采购单明细
        // 同一批次 日期;  未汇总;采购状态为 待处理
        SalebBuyOrderDetailQueryReq queryReq = new SalebBuyOrderDetailQueryReq();
        queryReq.copyFromSummaryOrder(req);
        // 状态为 未汇总
        String summaryFlag = StrUtil.isEmpty(req.getId()) ? DetailSummaryFlagEnum.wait.getType() : DetailSummaryFlagEnum.summary.getType();
        queryReq.setSummaryFlag(summaryFlag);
        queryReq.setStatus(BuyOrderStatusEnum.wait.getType());
        R resp = salebFeignService.queryListBuyOrderDetail(queryReq, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(resp)) {
            throw new EmptyDataException("无待汇总信息");
        }
        // 关联商品信息
        GoodsModelDTO goodsModel = scmOpcUtils.fetchGoodsModel(req.getGoodsModelId());

        SummaryOrder summaryOrder = new SummaryOrder();
        OpcTrans.copyGoodsModelInfo(summaryOrder, goodsModel);
        // 初始化 保存
        OpcTrans.initSummaryOrder(summaryOrder);
        // 汇总单状态 有请求值时设置
        if(null != req.getSummaryStatus()){
            summaryOrder.setSummaryStatus(req.getSummaryStatus());
        }
        summaryOrder.setPmId(req.getPmId());
        summaryOrder.setBuySeq(req.getBuySeq());
        summaryOrder.setDeliveryTime(req.getDeliveryTime());

        List list = (List) resp.getData();
        BigDecimal totalNum = BigDecimal.ZERO;
        BuyOrderDetailDTO detailDTO;
        List<BuyOrderDetail> detailList = new ArrayList<>(list.size());
        BuyOrderDetail detail;
        for (Object d : list) {
            detailDTO = BeanUtil.toBean(d, BuyOrderDetailDTO.class);
            if (null != detailDTO.getGoodsNum()) {
                totalNum = totalNum.add(detailDTO.getGoodsNum());
            }
            detail = new BuyOrderDetail();
            detail.setId(detailDTO.getId());
            detailList.add(detail);
        }
        // purchaseOption 此处不设置
        if(null != req.getNeedBuyNum()){
            summaryOrder.setNeedBuyNum(req.getNeedBuyNum());
        }else{
            summaryOrder.setNeedBuyNum(totalNum);
        }
        summaryOrder.setAllocateType(req.getAllocateType());
        // 新增或修改
        String id = req.getId();
        if (StrUtil.isNotEmpty(id)) {
            summaryOrder.setId(id);
            summaryOrderService.updateById(summaryOrder);
        } else {
            summaryOrderService.save(summaryOrder);
        }
        String summaryOrderId = summaryOrder.getSummaryOrderId();
        // 修改采购单明细 汇总状态 汇总单号
        for (BuyOrderDetail d : detailList) {
            d.setSummaryFlag(DetailSummaryFlagEnum.summary);
            d.setSummaryBuyId(summaryOrderId);
            //  采购明细状态 不修改
            R updateResp = buyOrderDetailFeignService.updateById(d, SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(updateResp) || !((boolean) updateResp.getData())) {
                throw new ErrorDataException("采购明细修改失败");
            }
        }
        return summaryOrder;
    }

    /**
     * 根据 客户采购单明细 buyOrderDetail 创建 交易单 tradeOrder
     *
     * @param req pmId  buyDetailSn  productSn
     * @return
     */
    @Override
    public TradeOrder createTradeOrderBySorting(OpcCreateTradeOrderReq req) {
        // 查询 采购单明细 关联采购单
        SalebBuyOrderDetailQueryReq queryReq = new SalebBuyOrderDetailQueryReq();
        queryReq.setPmId(req.getPmId());
        queryReq.setBuyDetailSn(req.getBuyDetailSn());
        R r = salebFeignService.queryOneBuyOrderDetail(queryReq, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r) || r.getData()==null){
            throw new ErrorDataException("查询 采购单明细并关联采购单出错");
        }
        BuyOrderDetailDTO orderDetailDTO = BeanUtil.toBean(r.getData(), BuyOrderDetailDTO.class);

        // 查询货品信息
        ProductInfoReq productInfoReq = new ProductInfoReq();
        productInfoReq.setProductSn(req.getProductSn());
        R info = productInfoFeignService.getOneByProductInfoReq(productInfoReq, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(info) || info.getData()==null){
            throw new ErrorDataException("关联货品信息错误");
        }
        ProductInfo productInfo = BeanUtil.toBean(info.getData(), ProductInfo.class);
        // 判断商品是否相同
        if(!StrUtil.equals(orderDetailDTO.getGoodsModelId(), productInfo.getGoodsModelId())){
            throw new ErrorDataException("交易商品有误");
        }
        // 获取商品信息
        GoodsModelDTO goodsModelDTO = scmOpcUtils.fetchGoodsModel(orderDetailDTO.getGoodsModelId());

        // 属性设置
        TradeOrder tradeOrder = new TradeOrder();
        OpcTrans.initTradeBySorting(tradeOrder);
        OpcTrans.copyFromBuyOrderDetailDTO(tradeOrder, orderDetailDTO);
        OpcTrans.copyFromProductInfo(tradeOrder, productInfo);
        OpcTrans.copyFromGoodsModelDTO(tradeOrder, goodsModelDTO);
        // 保存
        if(!tradeOrderService.save(tradeOrder)){
            throw new ErrorDataException("交易单保存有误");
        }
        return tradeOrder;
    }

    /**
     * 库存不足，根据采购方式类型生成采购明细
     *
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<PurchaseOrderDetail> setPropertiesByType(NotSatisfiedReq req) {
        //验证SummaryOrder
        QueryWrapper<SummaryOrder> query = new QueryWrapper<>();
        query.lambda()
                .eq(SummaryOrder::getPmId, req.getPmId())
                .eq(SummaryOrder::getSummaryStatus, SummaryStatusEnum.wait)
                .eq(SummaryOrder::getSummaryOrderId, req.getSummaryOrderId());
        SummaryOrder order = summaryOrderService.getOne(query);
        if (null == order) {
            throw new ErrorDataException("无可操作汇总单");
        }
        if (!order.getAllocateType().equals(AllocateTypeEnum.purchase)) {
            throw new ErrorDataException("调度方式错误");
        }
        // 采购截止时间
        LocalDateTime buyFinalTime = LocalDateTime.of(order.getDeliveryTime(), LocalTime.MIDNIGHT);
        List<PurchaseOrderDetail> list = new ArrayList<>();
        //生成PurchaseOrderDetail
        for (FormulaReq formula : req.getPurchaseInfo()) {
            MethodTypeEnum methodType = MethodTypeEnum.getByType(formula.getMethodType());

            R oneByGoodsModelReq = goodsModelFeignService.getInfoById(formula.getGoodsModelId(), SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(oneByGoodsModelReq)) {
                throw new EmptyDataException("数据为空");
            }
            GoodsModelDTO model = BeanUtil.toBean(oneByGoodsModelReq.getData(), GoodsModelDTO.class);
            PurchaseOrderDetail detail = OpcTrans.setPropertiesTrans(model, order);
            detail.setNeedBuyNum(formula.getNeedBuyNum());
            detail.setMethodType(methodType);
            detail.setPurchaseStatus(PurchaseStatusEnum.pending);
            detail.setPurchaseOrderType(PurchaseOrderTypeEnum.materialsNeeded);
            detail.setActionType(ActionTypeEnum.normal);
            detail.setInitiatorId(req.getInitiatorId());
            // 采购截止时间设置
            detail.setBuyFinalTime(buyFinalTime);

            // 若采购单明细 中设置了 单品采购及协议采购的 供应商信息 创建报价单
            AddOfferOrderReq offerReq = null;
            if (methodType == MethodTypeEnum.agreement) {
                // 协议
                detail.setSupplierId(formula.getSupplierId());
                detail.setOfferType(OfferTypeEnum.notQuote);
                offerReq = new AddOfferOrderReq();
                offerReq.setOfferType(PdsOfferTypeEnum.appointOffer.getType());
            } else if (methodType == MethodTypeEnum.single) {
                //单品
                detail.setSupplierId(formula.getSupplierId());
                detail.setBuyPrice(formula.getBuyPrice());
                detail.setOfferType(OfferTypeEnum.notQuote);
                offerReq = new AddOfferOrderReq();
                offerReq.setOfferType(PdsOfferTypeEnum.platformOffer.getType());
            } else {
                //竞价 methodType== MethodTypeEnum.bidding  || null
                detail.setBiddingEndTime(formula.getBiddingEndTime());
                detail.setOfferType(OfferTypeEnum.noQuotation);
            }
            // 设置采购明细编号
            String detailSn = scmIncrementUtil.inrcSnByClass(PurchaseOrderDetail.class, NumberPrefixConstant.PURCHASE_DETAIL_SN_PRE);
            detail.setPurchaseDetailSn(detailSn);
            // 新增报价单 调用 supply 的新增接口
            if (null != offerReq) {
                offerReq.setSupplierId(formula.getSupplierId());
                offerReq.setSupplyPrice(formula.getBuyPrice());
                offerReq.setGoodsModelId(formula.getGoodsModelId());
                offerReq.setPurchaseDetailSn(detailSn);
                offerReq.setBuyNum(formula.getNeedBuyNum());
                offerReq.setAskOrderId(order.getSummaryOrderId());
                offerReq.setAskPriceTime(order.getSummaryTime());
                // 新增报价单
                OfferOrder offerOrder = addOfferOrder(offerReq);
                detail.setSupplierName(offerOrder.getSupplierName());
            }

            R detailSaveResp = purchaseOrderDetailFeignService.save(detail, SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(detailSaveResp) || !((boolean) detailSaveResp.getData())) {
                throw new ErrorDataException("采购明细保存失败");
            }
            list.add(detail);
        }
        // 状态更新
        SummaryOrder updateOrder = new SummaryOrder();
        updateOrder.setId(order.getId());
        updateOrder.setSummaryStatus(SummaryStatusEnum.purchase);
        // TODO 暂定状态
        updateOrder.setStatus(PdsSummaryStatusEnum.confirm);
        if (!summaryOrderService.updateById(updateOrder)) {
            throw new ErrorDataException("汇总单状态更新失败");
        }
        return list;
    }

    /**
     * 新增报价单
     * @param orderReq
     */
    private OfferOrder addOfferOrder(AddOfferOrderReq orderReq){
        R offerSaveResp;
        OfferOrder offerOrder = null;
        try{
            offerSaveResp = offerOrderFeignService.addOfferOrder(orderReq, SecurityConstants.FROM_IN);
            offerOrder = BeanUtil.toBean(offerSaveResp, OfferOrder.class);
        }catch (Exception e){
            throw new ErrorDataException("新增报价单缺少对应参数");
        }
        if (!RUtil.isSuccess(offerSaveResp)) {
            throw new ErrorDataException("新增报价单失败");
        }
        return offerOrder;
    }

    /**
     * 运营 向采购部 发采购任务
     *
     * @return
     * @param编号列表 采购订单分类  采购截止时间 审核方式
     * List<PurchaseTa req
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean sendPurchaseTask(SendPurchaseTaskReq req) {
        PurchaseOrderTypeEnum purchaseOrderType = PurchaseOrderTypeEnum.getByType(req.getPurchaseOrderType());
        // 截止时间设置
        LocalDateTime buyFinalTime = req.getBuyFinalTime();
        if (null == buyFinalTime) {
            buyFinalTime = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        }
        PurchaseOrder order = new PurchaseOrder();
        order.setPmId(req.getPmId());
        ReviewTypeEnum reviewType = ReviewTypeEnum.getByType(req.getReviewType());
        if(null == reviewType){
            reviewType = ReviewTypeEnum.need_audit;
        }
        order.setReviewType(reviewType);
        order.setPurchaseOrderType(purchaseOrderType);
        order.setPurchaseStatus(PurchaseStatusEnum.checking);
        order.setInitiatorId(req.getInitiatorId());
        order.setBuyFinalTime(buyFinalTime);
        BigDecimal estimateAmount = BigDecimal.ZERO;
        // 设置sn
        String purchaseSn = scmIncrementUtil.inrcSnByClass(PurchaseOrder.class, NumberPrefixConstant.PURCHASE_SN_PRE);
        order.setPurchaseSn(purchaseSn);
        //查询采购明细
        for (String detailSn : req.getPurchaseDetailSnList()) {
            PurchaseOrderDetailReq detailReq = new PurchaseOrderDetailReq();
            detailReq.setPmId(req.getPmId());
            detailReq.setPurchaseDetailSn(detailSn);
            detailReq.setPurchaseStatus(PurchaseStatusEnum.pending);
            R rDetail = purchaseOrderDetailFeignService.getOneByPurchaseOrderDetailReq(detailReq, SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(rDetail) || null == rDetail.getData()) {
                throw new ErrorDataException("采购明细有误");
            }
            PurchaseOrderDetail detail = BeanUtil.toBean(rDetail.getData(), PurchaseOrderDetail.class);

            estimateAmount = estimateAmount.add(detail.getNeedBuyNum().multiply(detail.getMarketPrice()));
            // 更新明细
            PurchaseOrderDetail updateDetail = new PurchaseOrderDetail();
            updateDetail.setId(detail.getId());
            updateDetail.setPurchaseSn(purchaseSn);
            updateDetail.setBuyFinalTime(buyFinalTime);
            R updateResp = purchaseOrderDetailFeignService.updateById(updateDetail, SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(updateResp) || !((boolean) updateResp.getData())) {
                throw new ErrorDataException("更新采购明细的采购单编号失败");
            }
        }
        order.setEstimateAmount(estimateAmount);
        // 创建采购单
        R saveResp = purchaseOrderFeignService.save(order, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(saveResp) || !((boolean) saveResp.getData())) {
            throw new ErrorDataException("创建采购单失败");
        }
        // 创建采购任务(审核)
        PurchaseTask task = new PurchaseTask();
        task.setPmId(req.getPmId());
        task.setInitiatorId(req.getInitiatorId());
        task.setPurchaseSn(purchaseSn);
        task.setTaskActionType(TaskActionTypeEnum.audit);
        task.setTaskStatus(TaskStatusEnum.wait);
        task.setContent("商品采购");
        task.setDeadlineTime(buyFinalTime);
        R taskSaveResp = purchaseTaskFeignService.save(task, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(taskSaveResp) || !((boolean) taskSaveResp.getData())) {
            throw new ErrorDataException("创建采购任务失败");
        }
        return true;
    }



    /**
     * 运营 生成采购单
     * @param req
     * @return
     */
    @Override
    public PurchaseOrderResp addPurchaseOrder(OpcAddPurchaseOrderReq req) {
        List<String> details = new ArrayList<>();
        BigDecimal sumPrice = BigDecimal.ZERO;
        List<PurchaseOrderDetail> detailList = new ArrayList<>();
        //查询汇总单
        for(String id : req.getSummaryOrderId()){
            SummaryOrder order = scmOpcUtils.querySummaryOrder(req.getPmId(), id);
            PurchaseOrderDetailReq detailReq = new PurchaseOrderDetailReq();
            detailReq.setPmId(req.getPmId());
            detailReq.setSummaryDetailSn(id);
            //查询采购明细
            R r = purchaseOrderDetailFeignService.getOneByPurchaseOrderDetailReq(detailReq,SecurityConstants.FROM_IN);
            if(!RUtil.isSuccess(r)){
                throw new EmptyDataException();
                //若无对应采购明细创建采购明细
            }else if(r.getData()==null){
                r = purchaseOrderDetailFeignService.addPurchaseOrderDetail(order, SecurityConstants.FROM_IN);
                if(!RUtil.isSuccess(r)){
                    throw new EmptyDataException();
                }
            }
            PurchaseOrderDetail detail = BeanUtil.toBean(r.getData(), PurchaseOrderDetail.class);
            detail.setMethodType(req.getMethodType());
            if(!RUtil.isSuccess(purchaseOrderDetailFeignService.updateById(detail, SecurityConstants.FROM_IN))){
                throw new ErrorDataException();
            }
            sumPrice = sumPrice.add(detail.getMarketPrice().multiply(detail.getNeedBuyNum()));
            details.add(detail.getPurchaseDetailSn());
            detailList.add(detail);
        }
        //创建采购单
        AddPurchaseOrderReq orderReq = new AddPurchaseOrderReq();
        orderReq.setPurchaseDetailSn(details);
        orderReq.setPmId(req.getPmId());
        R r = purchaseOrderFeignService.addPurchaseOrder(orderReq, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r) || null == r.getData()){
            throw new ErrorDataException();
        }
        // 返回值及明细信息
        PurchaseOrderResp rOrder = BeanUtil.toBean(r.getData(), PurchaseOrderResp.class);
        rOrder.setDetailList(detailList);

        //创建采购审核任务
        PurchaseTask task = new PurchaseTask();
        task.setPmId(req.getPmId());
        task.setTaskStatus(TaskStatusEnum.wait);
        task.setTaskActionType(TaskActionTypeEnum.audit);
        task.setPurchaseSn(rOrder.getPurchaseSn());
        task.setDeadlineTime(rOrder.getBuyFinalTime());
        if(!RUtil.isSuccess(purchaseTaskFeignService.save(task,SecurityConstants.FROM_IN))){
            throw new EmptyDataException();
        }
        return rOrder;
    }


}
