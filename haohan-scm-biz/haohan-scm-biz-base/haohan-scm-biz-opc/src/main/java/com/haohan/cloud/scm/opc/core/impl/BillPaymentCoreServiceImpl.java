package com.haohan.cloud.scm.opc.core.impl;

import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderDetailReq;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderReq;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.opc.dto.BillPayment;
import com.haohan.cloud.scm.api.opc.dto.BillPaymentDetail;
import com.haohan.cloud.scm.api.opc.dto.BillPaymentInfoDTO;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderDetailReq;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.opc.core.BillPaymentCoreService;
import com.haohan.cloud.scm.opc.service.BuyerPaymentService;
import com.haohan.cloud.scm.opc.service.SupplierPaymentService;
import com.haohan.cloud.scm.opc.utils.ScmOpcUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dy
 * @date 2019/9/20
 */
@Service
@AllArgsConstructor
public class BillPaymentCoreServiceImpl implements BillPaymentCoreService {

    private final BuyerPaymentService buyerPaymentService;
    private final SupplierPaymentService supplierPaymentService;
    private final ScmOpcUtils scmOpcUtils;

    /**
     * 查询账单详情
     * @param payment 必须 pmId / billSn / settlementType
     * @return
     */
    @Override
    public BillPaymentInfoDTO fetchBillInfo(BillPayment payment) {
        BillPaymentInfoDTO result = null;
        switch (payment.getSettlementType()){
            case receivable:
                // 应收账单
                result = fetchReceivableBill(payment);
                break;
            case payable:
                // 应付账单
                result = fetchPayableBill(payment);
                break;
            default:
        }
        return result;
    }

    /**
     * 应收账单查询
     * @param payment
     * @return
     */
    @Override
    public BillPaymentInfoDTO fetchReceivableBill(BillPayment payment){
        //查询 应收账单
        BuyerPayment query = new BuyerPayment();
        query.setPmId(payment.getPmId());
        query.setBuyerPaymentId(payment.getBillSn());
        BuyerPayment buyerPayment = buyerPaymentService.fetchBySn(query);
        if (null==buyerPayment){
            throw new EmptyDataException("没有查到应收账单");
        }
        BillPaymentInfoDTO bill = new BillPaymentInfoDTO(buyerPayment);
        // 查询账单详情
        List<BillPaymentDetail> detailList = new ArrayList<>(10);
        switch (buyerPayment.getBillType()){
            case order:
                fetchBuyOrderInfo(bill, detailList);
                break;
            case offerBack:
                fetchReturnOrderInfo(bill, detailList);
                break;
            default:
        }
        bill.setDetailList(detailList);
        return bill;
    }

    /**
     * 退货单明细
     * @param bill
     * @param detailList
     */
    private void fetchReturnOrderInfo(BillPaymentInfoDTO bill, List<BillPaymentDetail> detailList) {
        // 退货单
        ReturnOrderReq req = new ReturnOrderReq();
        req.setPmId(bill.getPmId());
        req.setReturnSn(bill.getOrderSn());
        ReturnOrder returnOrder = scmOpcUtils.queryReturnOrder(req);
        if(null == returnOrder){
            return ;
        }
        bill.setOtherAmount(returnOrder.getOtherAmount());
        ReturnOrderDetailReq detailReq = new ReturnOrderDetailReq();
        detailReq.setPmId(bill.getPmId());
        detailReq.setReturnSn(bill.getOrderSn());
        List<ReturnOrderDetail> returnOrderDetailList = scmOpcUtils.queryReturnOrderDetailList(detailReq);
        for(ReturnOrderDetail detail: returnOrderDetailList){
            if(detail.getReturnStatus() == ReturnStatusEnum.failed){
                continue;
            }
            detailList.add(new BillPaymentDetail(detail));
        }
    }

    /**
     * 客户采购单明细
     * @param bill
     * @param detailList
     */
    private void fetchBuyOrderInfo(BillPaymentInfoDTO bill, List<BillPaymentDetail> detailList) {
        BuyOrderReq req = new BuyOrderReq();
        req.setPmId(bill.getPmId());
        req.setBuyId(bill.getOrderSn());
        BuyOrder buyOrder = scmOpcUtils.queryBuyOrder(req);
        if(null == buyOrder || buyOrder.getStatus() == BuyOrderStatusEnum.cancel){
            return ;
        }
        bill.setOtherAmount(buyOrder.getShipFee());
        BuyOrderDetailReq detailReq = new BuyOrderDetailReq();
        detailReq.setPmId(bill.getPmId());
        detailReq.setBuyId(bill.getOrderSn());
        List<BuyOrderDetail> buyOrderDetailList = scmOpcUtils.queryBuyOrderDetailList(detailReq);
        for(BuyOrderDetail detail : buyOrderDetailList){
            if(detail.getStatus() == BuyOrderStatusEnum.cancel){
                continue;
            }
            detailList.add(new BillPaymentDetail(detail));
        }
    }

    /**
     * 应付账单查询
     * @param payment
     * @return
     */
    @Override
    public BillPaymentInfoDTO fetchPayableBill(BillPayment payment) {
        // 查询 应付账单
        SupplierPayment query = new SupplierPayment();
        query.setPmId(payment.getPmId());
        query.setSupplierPaymentId(payment.getBillSn());
        SupplierPayment supplierPayment = supplierPaymentService.fetchBySn(query);
        if (null==supplierPayment){
            throw new EmptyDataException("没有查到应付账单");
        }
        BillPaymentInfoDTO bill = new BillPaymentInfoDTO(supplierPayment);
        // 查询账单详情
        List<BillPaymentDetail> detailList = new ArrayList<>(10);
        switch (supplierPayment.getBillType()){
            case purchase:
                fetchSupplyOrderInfo(bill, detailList);
                break;
            case orderBack:
                fetchReturnOrderInfo(bill, detailList);
                break;
            default:
        }
        bill.setDetailList(detailList);
        return bill;
    }

    /**
     * 供应单明细
     * @param bill
     * @param detailList
     */
    private void fetchSupplyOrderInfo(BillPaymentInfoDTO bill, List<BillPaymentDetail> detailList) {
        SupplyOrderReq req = new SupplyOrderReq();
        req.setSupplySn(bill.getOrderSn());
        SupplyOrder supplyOrder = scmOpcUtils.querySupplyOrder(req);
        if(null == supplyOrder){
            return;
        }
        bill.setOtherAmount(supplyOrder.getOtherAmount());
        OfferOrderReq offerOrderReq = new OfferOrderReq();
        offerOrderReq.setPmId(bill.getPmId());
        offerOrderReq.setSupplySn(bill.getOrderSn());
        List<OfferOrder> offerOrderList = scmOpcUtils.queryOfferOrderList(offerOrderReq);
        for(OfferOrder offer: offerOrderList){
            if(offer.getStatus() != PdsOfferStatusEnum.bidding){
                continue;
            }
            detailList.add(new BillPaymentDetail(offer));
        }
    }


}
