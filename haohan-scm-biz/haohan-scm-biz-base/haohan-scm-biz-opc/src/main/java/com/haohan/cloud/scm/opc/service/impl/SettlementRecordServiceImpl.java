/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.SettlementRecordMapper;
import com.haohan.cloud.scm.opc.service.SettlementRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-13 20:39:12
 */
@Service
public class SettlementRecordServiceImpl extends ServiceImpl<SettlementRecordMapper, SettlementRecord> implements SettlementRecordService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SettlementRecord entity) {
        if (StrUtil.isEmpty(entity.getSettlementSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(SettlementRecord.class, NumberPrefixConstant.SETTLEMENT_RECORD_SN_PRE);
            entity.setSettlementSn(sn);
        }
        return super.save(entity);
    }

    /**
     * 根据 结算记录编号查询
     * @param settlementRecord pmId可传
     * @return
     */
    @Override
    public SettlementRecord fetchBySn(SettlementRecord settlementRecord) {
        QueryWrapper<SettlementRecord> query = new QueryWrapper<>();
        String pmId = settlementRecord.getPmId();
        query.lambda()
                .eq(StrUtil.isNotBlank(pmId),SettlementRecord::getPmId, pmId)
                .eq(SettlementRecord::getSettlementSn, settlementRecord.getSettlementSn());
        return super.getOne(query);
    }
}
