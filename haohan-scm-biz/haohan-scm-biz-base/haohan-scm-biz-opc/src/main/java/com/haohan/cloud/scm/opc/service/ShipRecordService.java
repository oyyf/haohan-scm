/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;

/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
public interface ShipRecordService extends IService<ShipRecord> {

    /**
     * 根据编号查询
     *
     * @param shipSn
     * @return
     */
    ShipRecord fetchBySn(String shipSn);

    ShipRecord fetchByOrderSn(String orderSn);
}
