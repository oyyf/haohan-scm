/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.haohan.cloud.scm.opc;

/**
 * @author dy
 * 运营平台 模块
 */
//@EnablePigxSwagger2
//@SpringCloudApplication
//@EnablePigxFeignClients(basePackages = {"com.pig4cloud.pigx.*", "com.haohan.cloud.scm.api.*"})
//@EnablePigxResourceServer
//@ComponentScan(basePackages = "com.haohan.cloud.scm")
//@MapperScan(basePackages = "com.haohan.cloud.scm.*.mapper")
//public class HaohanScmOpcServiceApplication {
//
//    public static void main(String[] args) {
//        SpringApplication.run(HaohanScmOpcServiceApplication.class, args);
//    }
//}
