/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.api.opc.req.ShipperReq;
import com.haohan.cloud.scm.opc.service.ShipperService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Shipper")
@Api(value = "shipper", tags = "shipper-发货人内部接口服务}")
public class ShipperFeignApiCtrl {

    private final ShipperService shipperService;


    /**
     * 通过id查询发货人
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询发货人")
    public R<Shipper> getById(@PathVariable("id") String id) {
        return R.ok(shipperService.getById(id));
    }


    /**
     * 分页查询 发货人 列表信息
     *
     * @param shipperReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipperPage")
    @ApiOperation(value = "分页查询 发货人 列表信息")
    public R<IPage<Shipper>> getShipperPage(@RequestBody ShipperReq shipperReq) {
        Page<Shipper> page = new Page<>(shipperReq.getPageNo(), shipperReq.getPageSize());
        Shipper shipper = new Shipper();
        BeanUtil.copyProperties(shipperReq, shipper);
        return R.ok(shipperService.page(page, Wrappers.query(shipper)));
    }


    /**
     * 全量查询 发货人 列表信息
     *
     * @param shipperReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipperList")
    @ApiOperation(value = "全量查询 发货人 列表信息")
    public R<List<Shipper>> getShipperList(@RequestBody ShipperReq shipperReq) {
        Shipper shipper = new Shipper();
        BeanUtil.copyProperties(shipperReq, shipper);
        return R.ok(shipperService.list(Wrappers.query(shipper)));
    }


    /**
     * 新增发货人
     *
     * @param shipper 发货人
     * @return R
     */
    @Inner
    @SysLog("新增发货人")
    @PostMapping("/add")
    @ApiOperation(value = "新增发货人")
    public R<Boolean> save(@RequestBody Shipper shipper) {
        return R.ok(shipperService.save(shipper));
    }

    /**
     * 修改发货人
     *
     * @param shipper 发货人
     * @return R
     */
    @Inner
    @SysLog("修改发货人")
    @PostMapping("/update")
    @ApiOperation(value = "修改发货人")
    public R<Boolean> updateById(@RequestBody Shipper shipper) {
        return R.ok(shipperService.updateById(shipper));
    }

    /**
     * 通过id删除发货人
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除发货人")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除发货人")
    public R<Boolean> removeById(@PathVariable String id) {
        return R.ok(shipperService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除发货人")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除发货人")
    public R<Boolean> removeByIds(@RequestBody List<String> idList) {
        return R.ok(shipperService.removeByIds(idList));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询发货人")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询发货人")
    public R<List<Shipper>> listByIds(@RequestBody List<String> idList) {
        return R.ok(new ArrayList<>(shipperService.listByIds(idList)));
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipperReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询发货人总记录")
    @PostMapping("/countByShipperReq")
    @ApiOperation(value = "查询发货人总记录")
    public R<Integer> countByShipperReq(@RequestBody ShipperReq shipperReq) {
        return R.ok(shipperService.count(Wrappers.query(shipperReq)));
    }

    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipperReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shipperReq查询一条发货人信息")
    @PostMapping("/getOneByShipperReq")
    @ApiOperation(value = "根据shipperReq查询一条发货人信息")
    public R<Shipper> getOneByShipperReq(@RequestBody ShipperReq shipperReq) {
        return R.ok(shipperService.getOne(Wrappers.query(shipperReq), false));
    }

    /**
     * 批量修改OR插入发货人
     *
     * @param shipperList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入发货人数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入发货人数据")
    public R<Boolean> saveOrUpdateBatch(@RequestBody List<Shipper> shipperList) {
        return R.ok(shipperService.saveOrUpdateBatch(shipperList));
    }

}
