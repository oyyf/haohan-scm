package com.haohan.cloud.scm.opc.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.*;
import com.haohan.cloud.scm.api.opc.resp.TemporarySummaryOrderResp;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseOrderResp;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryCreateReq;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.opc.core.IScmOpcDemandSummaryService;
import com.haohan.cloud.scm.opc.core.IScmOpcOperationService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
@RestController
@AllArgsConstructor
@RequestMapping("/api/opc/operation")
@Api(value = "ApiOpcOperation", tags = "opcOperation运营管理api")
public class OpcOperationApiCtrl {

    @Autowired
    @Lazy
    private IScmOpcOperationService operationService;
    @Autowired
    @Lazy
    private IScmOpcDemandSummaryService iScmOpcDemandSummaryService;
    @Autowired
    @Lazy
    private IScmOpcOperationService iScmOpcOperationService;


    /**
     * 查看单商品购买需求
     * 运营  汇总采购商品(数量 展示) buyOrderDetail
     *
     * @param summaryReq
     * @return
     */
    @PostMapping("/queryGoodsNeeds")
    @ApiOperation(value = "查看单商品购买需求", notes = "运营操作,汇总采购商品(数量 展示)")
    public R<SummaryOrder> queryGoodsNeeds(@Validated SalebSummaryGoodsNumReq summaryReq) {
        SummaryOrder resp = operationService.queryGoodsNeeds(summaryReq);
        return new R(resp);
    }

    /**
     * 商品需求汇总 创建汇总单
     * 根据需采购商品 创建 汇总单数据(单商品) summaryOrder
     *
     * @param summaryReq
     * @return
     */
    @PostMapping("/createSummaryByGoodsNeeds")
    public R<SummaryOrder> createSummaryByGoodsNeeds(@Validated SalebSummaryCreateReq summaryReq) {
        SummaryOrder summaryOrder = new SummaryOrder();
        BeanUtil.copyProperties(summaryReq, summaryOrder);
        if(null == summaryOrder.getBuySeq() || null == summaryOrder.getAllocateType()){
            R<SummaryOrder> r = RUtil.error();
            r.setMsg("buySeq/allocateType有误");
            return r;
        }
        SummaryOrder resp = operationService.createSummaryByGoodsNeeds(summaryOrder);
        return new R(resp);
    }

    /**
     * 根据 客户采购单明细 buyOrderDetail 创建 交易单 tradeOrder
     *
     * @param req
     * @return
     */
    @PostMapping("/createTradeOrderBySorting")
    public R<TradeOrder> createTradeOrderBySorting(@Validated OpcCreateTradeOrderReq req) {
        TradeOrder resp = operationService.createTradeOrderBySorting(req);
        return new R(resp);
    }

    /**
     * 返回汇总单和汇总临时数据列表
     * @param req
     * @return
     */
    @PostMapping("/summaryOrderList")
    @ApiOperation(value = "返回汇总单和汇总临时数据列表")
    public R getSummaryOrderList(@Validated OpcSummaryOrderListReq req){
      List<TemporarySummaryOrderResp> summaryOrderList = iScmOpcDemandSummaryService.getSummaryOrderList(req);
      return new R(summaryOrderList);
    }
    /**
     * 库存不满足时  运营 发起需求采购
     * @param req
     * @return
     */
    @PostMapping("/notSatisfiedDelivery")
    public R getNotSatisfiedDeliveryDetail(@Validated NotSatisfiedReq req) {
      List<PurchaseOrderDetail> list = iScmOpcOperationService.setPropertiesByType(req);
      return new R(list);
    }

    /**
     * 运营 向采购部 发采购任务
     * @param req
     * @return
     */
    @PostMapping("/sendPurchaseTask")
    public R sendPurchaseTask(@Validated SendPurchaseTaskReq req) {
      return new R(iScmOpcOperationService.sendPurchaseTask(req));
    }

    @PostMapping("/addPurchaseOrder")
    @ApiOperation(value = "创建采购单",notes = "运营 生成采购单")
    public R<PurchaseOrderResp> addPurchaseOrder(@Validated OpcAddPurchaseOrderReq req){
        return new R<>(iScmOpcOperationService.addPurchaseOrder(req));
    }

    /**
     * 创建汇总单
     * @param resp
     * @return
     */
    @PostMapping("createSummaryOrderForOrder")
    @ApiOperation(value = "创建汇总单")
    public R<List<SummaryOrder>> createSummaryOrder(@RequestBody List<TemporarySummaryOrderResp> resp){
        return new R<>(iScmOpcDemandSummaryService.createSummaryOrderForOrder(resp));
    }

    /**
     * 创建汇总单 并生成采购单
     * @param req
     * @return
     */
    @PostMapping("createPurchaseOrder")
    @ApiOperation(value = "创建汇总单 并生成采购单")
    public R<PurchaseOrderResp> createPurchaseOrderWithSummaryOrder(@RequestBody @Validated CreatePurchaseOrderWithSummaryReq req){
        return new R<>(iScmOpcDemandSummaryService.createPurchaseOrderWithSummaryOrder(req));
    }

}
