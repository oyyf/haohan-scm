package com.haohan.cloud.scm.opc.api.ctrl;

import com.pig4cloud.pigx.common.core.util.R;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author dy
 * @date 2019/5/29
 */
@Data
public class OpcEventApiCtrl {


    // 运营 查询 汇总采购商品数量
    //  /api/opc/operation/summaryOrderList
    // 测: 商品属性/库存来源product
    @PostMapping("/..")
    public R<String> queryNumList(@Validated String req) {
        // 入参:送货时间 批次

        // 汇总采购商品数量
        // 查询对应商品的实际库存数量

        // 返回: 汇总采购商品数量 列表
        return new R();
    }

    // 运营 汇总采购商品需求
    //  /api/opc/operation/createSummaryByGoodsNeeds
    // 已测试
    @PostMapping("/..")
    public R<String> createSummaryOrder(@Validated String req) {
        // 入参:送货时间 批次 商品规格id 调配类型

        // 查询 汇总采购商品数量
        // 按查询结果 保存汇总单; 修改客户订单的汇总标识;

        // 返回:
        return new R();
    }


    // 库存满足时  运营 发起送货
    //   /api/product/shipOrder/initiateDelivery
    @PostMapping("/..")
    public R<String> action1(@Validated String req) {
        // 入参:汇总单编号
        // 调配类型2.库存

        // 创建生产任务(配送)
        // 创建出库单明细

        // 返回:
        return new R();
    }


    // 库存不满足时  运营 发起需求采购 设置项(损耗率/协议供应)查询
    //
    @PostMapping("/..")
    public R<String> action2(@Validated String req) {
        // 入参:汇总单编号
      // 判断商品是否需组合 (查询配方表)
      //  查询对应单品的损耗率 (组合的为多个对应的原材
        // 调配类型1.采购

        // 判断商品是否需组合 (查询配方表)
        //  查询对应单品的损耗率 (组合的为多个对应的原材料的损耗率)

        // 判断是否协议供应商品 (查询协议供应商品表)
        //  对应协议供应商

        // 返回: 商品对应损耗率列表
        return new R();
    }

    // 库存不满足时  运营 发起需求采购
    //  /api/opc/operation/notSatisfiedDelivery
    // 已测
    @PostMapping("/..")
    public R<String> action3(@Validated String req) {
        // 入参:汇总单编号 采购数量(商品需组合的是多个) 采购方式类型
                // (若为协议供应需对应供应商id;若为单品采购可指定供应商及价格;若为竞价采购需指定竞价截止时间)

        // 调配类型1.采购
        // 采购方式:需求采购

        // 创建采购部采购明细
        // 返回:
        return new R();
    }

    // 采购部 发起需求采购
    //  /api/purchase/purchaseOrder/initiateDemandPurchase
    @PostMapping("/..")
    public R<String> action4(@Validated String req) {
        // 入参:采购明细编号 采购数量 采购方式类型
                // (若为协议供应需对应供应商id;若为单品采购可指定供应商及价格;若为竞价采购需指定竞价截止时间)

        // 调配类型1.采购
        // 采购方式:需求采购

        // 创建采购部采购明细
        // 返回:
        return new R();
    }

    // 采购部/高管/运营  发起采购计划
    //  /api/purchase/purchaseOrder/initiatePurchasePlan
    @PostMapping("/..")
    public R<String> action5(@Validated String req) {
        // 入参:商品规格id 采购数量 采购方式类型
        // (若为协议供应需对应供应商id;若为单品采购可指定供应商及价格;若为竞价采购需指定竞价截止时间)
        // 调配类型1.采购
        // 采购方式:采购计划
        // 创建采购部采购明细
        // 返回:
        return new R();
    }

    // 运营 向采购部 发采购任务
    // /api/opc/operation/sendPurchaseTask
    // 已测: 任务待完善
    @PostMapping("/..")
    public R<String> action6(@Validated String req) {
      // 入参:采购明细编号列表  采购订单分类  采购截止时间 审核方式

      // 创建采购部采购单
      // 创建采购任务(审核)
      // 若采购单明细 中设置了 单品采购及协议采购的 供应商信息 需创建报价单

      // 返回:
        return new R();
    }

// 采购部

    // 总监 执行采购任务(审核) 批量通过
    //   /api/purchase/purchaseOrder/auditTaskBatch
    @PostMapping("/..")
    public R<String> action7(@Validated String req) {
        // 入参:采购任务id

        // 修改采购任务(审核)

        // 按对应采购单中的明细信息, 匹配商品和采购经理
        // 创建采购任务(分配) 每个商品都对应一个任务

        // 返回:
        return new R();
    }

    // 总监 执行采购任务(审核) 修改采购单明细
    //  /api/purchase/purchaseOrder/auditTask
    @PostMapping("/..")
    public R<String> action8(@Validated String req) {
        // 入参:采购任务id 采购单明细信息(关闭 或 修改采购方式/数量)

        // 返回:
        return new R();
    }

    // 经理 执行采购任务(分配) 指定采购员 (可批量指定同一个采购员)
    //     /api/purchase/purchaseOrder/appointTask
    @PostMapping("/..")
    public R<String> action9(@Validated String req) {
        // 入参:采购任务id 采购员id

        // 修改采购任务(分配)
        // 创建采购任务(采购)

        // 返回:
        return new R();
    }

    // 采购员 确认供应商
    //  /api/purchase/purchaseOrder/selectOrderSupplier
    // 已测
    @PostMapping("/..")
    public R<String> action10(@Validated String req) {
        // 入参:采购明细编号  供应商id 价格

        // 修改采购单明细
        // 采购明细编号 填入报价单

        // 返回:
        return new R();
    }

    // 采购员  发起 订单异常汇报
    @PostMapping("/..")
    public R<String> action11(@Validated String req) {
        // 入参:采购明细编号  汇报信息(类型/分类/..)

        // 修改采购单明细 的采购执行状态为异常
        // 创建采购汇报记录

        // 返回:
        return new R();
    }

    // 采购经理/总监  回复 订单异常汇报
    @PostMapping("/..")
    public R<String> action12(@Validated String req) {
        // 入参:采购汇报id  回复信息

        // 修改采购汇报记录  状态:已反馈

        // 返回:
        return new R();
    }

    // 采购员 处理 订单异常
    @PostMapping("/..")
    public R<String> action13(@Validated String req) {
        // 入参:采购汇报id

        // 修改采购汇报记录  状态:已处理
        // 修改采购单明细  的采购执行状态为正常

        // 返回:
        return new R();
    }

    // 采购员 揽货确认
    @PostMapping("/..")
    public R<String> action14(@Validated String req) {
        // 入参:采购明细编号 请款类型
                // 若为需请款 可指定请款人类型(默认为执行人)

        // 修改采购单明细
        // 若为需请款 创建账户扣减记录

        // 返回:
        return new R();
    }

// 供应商

    // 供应商(竞价采购) 根据采购明细报价
    //    /api/supply/supplyOrder/addOfferBySupplier
    @PostMapping("/..")
    public R<String> action15(@Validated String req) {
        // 入参:采购明细编号

        // 返回:
        return new R();
    }

// 生成入库单明细

// 生成货品信息

    //  生成货品信息  根据采购单明细
    //  /api/product/productInfo/addProductInfo
    // 已测
    @PostMapping("/..")
    public R<String> action16(@Validated String req) {
        // 入参:采购明细编号

        // 返回:
        return new R();
    }

    //  商品库存更新  根据goodsModelId
    //  /api/product/productInfo/updateInventory
    @PostMapping("/..")
    public R<String> action17(@Validated String req) {
        // 入参: goodsModelId

        // 查询 货品信息表 正常状态的货品,数量求和 为 库存数量
        // 更新 零售商品规格的 库存数量(加上虚拟库存数量)

        // 返回:
        return new R();
    }

    // 分拣匹配生成交易单
    // 根据 客户采购单明细 buyOrderDetail 创建 交易单 tradeOrder
    //  /api/opc/operation/createTradeOrderBySorting
    // 已测
    @PostMapping("/..")
    public R<String> action18(@Validated String req) {
        // 入参: buyId  productSn

        // 返回:
        return new R();
    }


}
