package com.haohan.cloud.scm.opc.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.OpcSummaryOrderQueryReq;
import com.haohan.cloud.scm.api.opc.req.OpcTradeOrderQueryReq;
import com.haohan.cloud.scm.opc.service.SummaryOrderService;
import com.haohan.cloud.scm.opc.service.TradeOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/opc")
@Api(value = "ApiOpcFeign", tags = "opcFeign运营管理内部调用")
public class OpcFeignApiCtrl {

    @Autowired
    private SummaryOrderService summaryOrderService;
    @Autowired
    private TradeOrderService tradeOrderService;

    /**
     * 查询 汇总单
     * 通过 id summaryOrderId
     *
     * @param req id
     * @return R
     */
    @Inner
    @PostMapping("summaryOrder/queryOne")
    public R queryOne(@RequestBody OpcSummaryOrderQueryReq req) {
        SummaryOrder summaryOrder = new SummaryOrder();
        summaryOrder.setId(req.getId());
        summaryOrder.setSummaryOrderId(req.getSummaryOrderId());
        SummaryOrder s = summaryOrderService.getOne(Wrappers.query(summaryOrder));
        return new R<>(s);
    }

    /**
     * 查询 汇总单列表
     * 通过 时间 批次 等
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("summaryOrder/queryList")
    public R queryList(@RequestBody OpcSummaryOrderQueryReq req) {
        SummaryOrder summaryOrder = new SummaryOrder();
        BeanUtil.copyProperties(req, summaryOrder);
        return new R<>(summaryOrderService.list(new QueryWrapper<SummaryOrder>(summaryOrder)));
    }


    /**
     * 查询交易单
     * 通过 id tradeId
     *
     * @param req id
     * @return R
     */
    @Inner
    @PostMapping("tradeOrder/queryOne")
    public R queryOne(@RequestBody OpcTradeOrderQueryReq req) {
        TradeOrder tradeOrder = new TradeOrder();
        tradeOrder.setId(req.getId());
        tradeOrder.setTradeId(req.getTradeId());
        return new R<>(tradeOrderService.getOne(new QueryWrapper<TradeOrder>(tradeOrder)));
    }

    /**
     * 查询交易单列表
     * 通过 buyId 等
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("tradeOrder/queryList")
    public R queryList(@RequestBody OpcTradeOrderQueryReq req) {
        TradeOrder tradeOrder = new TradeOrder();
        BeanUtil.copyProperties(req, tradeOrder);
        return new R<>(tradeOrderService.list(new QueryWrapper<TradeOrder>(tradeOrder)));
    }


}
