package com.haohan.cloud.scm.opc.api.ctrl;

import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.req.payment.*;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.opc.core.BillPaymentCoreService;
import com.haohan.cloud.scm.opc.core.IBuyerPaymentCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/7/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/opc/receivable")
@Api(value = "ApiBuyerPayment", tags = "采购商货款统计 应收账单")
public class BuyerPaymentApiCtrl {

    private final IBuyerPaymentCoreService buyerPaymentCoreService;
    private final BillPaymentCoreService billPaymentCoreService;

    /**
     * 创建应收账单
     *  根据 应收来源订单编号:receivableSn/账单类型:billType 去查对应订单获取金额
     * @param req 必需:pmId/receivableSn/billType
     * @return
     */
    @PostMapping("/createReceivable")
    @ApiOperation(value = "创建应收账单")
    public R<BuyerPayment> createReceivable(@RequestBody @Validated CreateReceivableReq req) {
        BuyerPayment buyerPayment = req.transTo();
        buyerPayment = buyerPaymentCoreService.createReceivable(buyerPayment);
        if(null == buyerPayment){
            return R.failed("创建应收账单失败");
        }
        return R.ok(buyerPayment);
    }

    /**
     * 批量创建应收账单  都是 订单应收
     *  根据 配送日期/批次
     * @param req pmId/ 配送日期/批次
     * @return
     */
    @PostMapping("/createReceivable/batch")
    @ApiOperation(value = "批量创建应收账单")
    public R createReceivableBatch(@RequestBody @Validated CreateReceivableBatchReq req) {
        BuyOrder buyOrder = req.transTo();
        String resp = buyerPaymentCoreService.createReceivableBatch(buyOrder);
        if(null == resp){
            R.failed("无可创建账单");
        }
        return R.ok(resp);
    }

    /**
     *  查询应收账单是否可审核 对应订单需为确认状态
     * @param req 必需 pmId / buyerPaymentId
     * @return 账单更新金额
     */
    @PostMapping("/checkReceivable")
    @ApiOperation(value = "查询应收账单是否可审核")
    public R<BuyerPayment> checkReceivable(@Validated CheckReceivableReq req) {
        BuyerPayment buyerPayment = buyerPaymentCoreService.checkReceivable(req.getPmId(), req.getBuyerPaymentId());
        if(null == buyerPayment){
            return R.failed("应收账单未确认,不可审核");
        }
        return R.ok(buyerPayment);
    }

    /**
     *  审核应收账单
     * @param req 必需 pmId / reviewStatus / buyerPayment/buyerPaymentId 可选: remarks
     * @return
     */
    @PostMapping("/reviewPayment")
    @ApiOperation(value = "审核应收账单")
    public R<Boolean> reviewPayment(@Validated ReviewPaymentReq req) {
        BuyerPayment buyerPayment = req.transTo();
        if (!buyerPaymentCoreService.reviewPayment(buyerPayment)) {
            return R.failed(false);
        }
        return R.ok(true);
    }

    @GetMapping("/queryPaymentDetail")
    @ApiOperation(value = "查询账单详情")
    public R queryPaymentDetail(@Validated PaymentDetailReq req){
        return R.ok(buyerPaymentCoreService.queryPaymentDetail(req));
    }

    @GetMapping("/countBill")
    @ApiOperation(value = "查询到期的应收账单数 (未结算)")
    public R<Integer> countReceivableBill(@Validated CountReceivableBillReq req) {
        return R.ok(buyerPaymentCoreService.countReceivableBill(req));
    }


}
