/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.ShipRecordMapper;
import com.haohan.cloud.scm.opc.service.ShipRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
@Service
@AllArgsConstructor
public class ShipRecordServiceImpl extends ServiceImpl<ShipRecordMapper, ShipRecord> implements ShipRecordService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ShipRecord entity) {
        if (StrUtil.isEmpty(entity.getShipRecordSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ShipRecord.class, NumberPrefixConstant.SHIP_RECORD_SN_PRE);
            entity.setShipRecordSn(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    /**
     * 根据编号查询
     *
     * @param shipSn
     * @return
     */
    @Override
    public ShipRecord fetchBySn(String shipSn) {
        return baseMapper.selectList(Wrappers.<ShipRecord>query().lambda()
                .eq(ShipRecord::getShipRecordSn, shipSn)).stream()
                .findFirst().orElse(null);
    }

    @Override
    public ShipRecord fetchByOrderSn(String orderSn) {
        return baseMapper.selectList(Wrappers.<ShipRecord>query().lambda()
                .eq(ShipRecord::getOrderSn, orderSn)).stream()
                .findFirst().orElse(null);
    }
}
