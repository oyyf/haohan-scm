/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import com.haohan.cloud.scm.api.opc.req.ShipRecordDetailReq;
import com.haohan.cloud.scm.opc.service.ShipRecordDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shiprecorddetail")
@Api(value = "shiprecorddetail", tags = "发货记录明细管理")
public class ShipRecordDetailController {

    private final  ShipRecordDetailService shipRecordDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shipRecordDetail 发货记录明细
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public R getShipRecordDetailPage(Page page, ShipRecordDetail shipRecordDetail) {
        return R.ok(shipRecordDetailService.page(page, Wrappers.query(shipRecordDetail)));
    }


    /**
     * 通过id查询发货记录明细
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(shipRecordDetailService.getById(id));
    }

    /**
     * 新增发货记录明细
     * @param shipRecordDetail 发货记录明细
     * @return R
     */
    @ApiOperation(value = "新增发货记录明细", notes = "新增发货记录明细")
    @SysLog("新增发货记录明细")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_add')")
    public R save(@RequestBody ShipRecordDetail shipRecordDetail) {
        return R.ok(shipRecordDetailService.save(shipRecordDetail));
    }

    /**
     * 修改发货记录明细
     * @param shipRecordDetail 发货记录明细
     * @return R
     */
    @ApiOperation(value = "修改发货记录明细", notes = "修改发货记录明细")
    @SysLog("修改发货记录明细")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_edit')")
    public R updateById(@RequestBody ShipRecordDetail shipRecordDetail) {
        return R.ok(shipRecordDetailService.updateById(shipRecordDetail));
    }

    /**
     * 通过id删除发货记录明细
     * @param id id
     * @return R
     */
    @SysLog("删除发货记录明细")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_del')")
    @ApiOperation(value = "通过id删除发货记录明细")
    public R removeById(@PathVariable String id) {
        return new R<>(shipRecordDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除发货记录明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_del')")
    @ApiOperation(value = "根据IDS批量删除发货记录明细")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipRecordDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询发货记录明细")
    @PostMapping("/listByIds")
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_del')")
    @ApiOperation(value = "根据IDS批量查询发货记录明细")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipRecordDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipRecordDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询发货记录明细总记录")
    @PostMapping("/countByShipRecordDetailReq")
    @ApiOperation(value = "查询发货记录明细总记录")
    public R countByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {

        return new R<>(shipRecordDetailService.count(Wrappers.query(shipRecordDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipRecordDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shipRecordDetailReq查询一条发货记录明细记录")
    @PostMapping("/getOneByShipRecordDetailReq")
    @ApiOperation(value = "根据shipRecordDetailReq查询一条发货记录明细记录")
    public R getOneByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {

        return new R<>(shipRecordDetailService.getOne(Wrappers.query(shipRecordDetailReq), false));
    }


    /**
     * 批量修改OR插入发货记录明细
     * @param shipRecordDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入发货记录明细")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('opc_shiprecorddetail_edit')")
    @ApiOperation(value = "批量修改OR插入发货记录明细信息")
    public R saveOrUpdateBatch(@RequestBody List<ShipRecordDetail> shipRecordDetailList) {

        return new R<>(shipRecordDetailService.saveOrUpdateBatch(shipRecordDetailList));
    }


}
