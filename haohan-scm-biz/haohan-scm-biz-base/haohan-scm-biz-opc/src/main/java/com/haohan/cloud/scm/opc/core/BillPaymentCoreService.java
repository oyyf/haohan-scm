package com.haohan.cloud.scm.opc.core;

import com.haohan.cloud.scm.api.opc.dto.BillPayment;
import com.haohan.cloud.scm.api.opc.dto.BillPaymentInfoDTO;

/**
 * @author dy
 * @date 2019/9/20
 */
public interface BillPaymentCoreService {

    /**
     * 查询账单详情
     * @param payment
     * @return
     */
    BillPaymentInfoDTO fetchBillInfo(BillPayment payment);

    /**
     * 查询应收账单详情
     * @param payment
     * @return
     */
    BillPaymentInfoDTO fetchReceivableBill(BillPayment payment);

    /**
     * 查询应付账单详情
     * @param payment
     * @return
     */
    BillPaymentInfoDTO fetchPayableBill(BillPayment payment);
}
