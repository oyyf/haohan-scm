/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.CostControl;

/**
 * 成本管控
 *
 * @author haohan
 * @date 2019-05-13 20:36:45
 */
public interface CostControlMapper extends BaseMapper<CostControl> {

}
