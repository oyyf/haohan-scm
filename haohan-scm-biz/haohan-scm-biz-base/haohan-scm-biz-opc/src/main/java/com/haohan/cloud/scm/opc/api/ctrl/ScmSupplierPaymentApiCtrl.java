package com.haohan.cloud.scm.opc.api.ctrl;

import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.opc.req.payment.*;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.opc.core.ISupplierPaymentCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/7/19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/opc/payable")
@Api(value = "ApiScmSupplierPayment", tags = "scmSupplierPayment供应商货款管理 应付账单")
public class ScmSupplierPaymentApiCtrl {

    private final ISupplierPaymentCoreService scmSupplierPaymentService;

    /**
     * 查询应付账单详情
     * @param req
     * @return
     */
    @PostMapping("/fetchSupplierPayment")
    @ApiOperation(value = "查询应付账单详情")
    public R fetchSupplierPayment(@Validated FetchSupplierPaymentReq req){

        return new R<>(scmSupplierPaymentService.fetchSupplierPayment(req));
    }

    /**
     * 创建应付账单
     *  根据 应付来源订单编号:payableSn/账单类型:billType 去查对应订单获取金额
     * @param req 必需:pmId/payableSn/billType
     * @return
     */
    @PostMapping("/createPayable")
    @ApiOperation(value = "创建应付账单")
    public R<SupplierPayment> createPayable(@RequestBody @Validated CreatePayableReq req) {
        SupplierPayment supplierPayment = req.transTo();
        supplierPayment = scmSupplierPaymentService.createPayable(supplierPayment);
        if(null == supplierPayment){
            return R.failed("创建应付账单失败");
        }
        return R.ok(supplierPayment);
    }

    /**
     * 批量创建 应付账单  都是 采购应付
     * 根据 备货日期/批次
     *
     * @param req pmId/ 备货日期/批次
     * @return
     */
    @PostMapping("/createPayable/batch")
    @ApiOperation(value = "批量创建应收账单")
    public R createPayableBatch(@RequestBody @Validated CreatePayableBatchReq req) {
        SupplyOrder supplyOrder = req.transTo();
        String resp = scmSupplierPaymentService.createPayableBatch(supplyOrder);
        if(null == resp){
            R.failed("无可创建账单");
        }
        return R.ok(resp);
    }

    /**
     * 查询应付账单是否可审核 对应订单需为确认状态
     *
     * @param req 必需 pmId / supplierPaymentId
     * @return 账单更新金额
     */
    @PostMapping("/checkPayable")
    @ApiOperation(value = "查询应付账单是否可审核")
    public R<SupplierPayment> checkPayable(@Validated CheckPayableReq req) {
        SupplierPayment supplierPayment = scmSupplierPaymentService.checkPayable(req.getPmId(), req.getSupplierPaymentId());
        if (null == supplierPayment) {
            return R.failed("应付账单未确认,不可审核");
        }
        return R.ok(supplierPayment);
    }

    /**
     *  审核应付账单
     * @param req 必需 pmId / reviewStatus / supplierPayment/supplierPaymentId 可选: remarks
     * @return
     */
    @PostMapping("/reviewPayment")
    @ApiOperation(value = "审核应付账单")
    public R<Boolean> reviewPayment(@Validated ReviewPaymentPayableReq req) {
        SupplierPayment supplierPayment = req.transTo();
        if (!scmSupplierPaymentService.reviewPayment(supplierPayment)) {
            return R.failed(false);
        }
        return R.ok(true);
    }

    @GetMapping("/queryPaymentDetail")
    @ApiOperation(value = "查看账单详情")
    public R queryPaymentDetail(PaymentDetailReq req){
        return R.ok(scmSupplierPaymentService.queryPaymentDetail(req));
    }
}
