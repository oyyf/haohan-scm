/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.opc.entity.Shipper;

/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
public interface ShipperMapper extends BaseMapper<Shipper> {

}
