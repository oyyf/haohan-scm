/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.req.ShipRecordReq;
import com.haohan.cloud.scm.opc.service.ShipRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shiprecord")
@Api(value = "shiprecord", tags = "发货记录管理")
public class ShipRecordController {

    private final  ShipRecordService shipRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param shipRecord 发货记录
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public R getShipRecordPage(Page page, ShipRecord shipRecord) {
        return R.ok(shipRecordService.page(page, Wrappers.query(shipRecord)));
    }


    /**
     * 通过id查询发货记录
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(shipRecordService.getById(id));
    }

    /**
     * 新增发货记录
     * @param shipRecord 发货记录
     * @return R
     */
    @ApiOperation(value = "新增发货记录", notes = "新增发货记录")
    @SysLog("新增发货记录")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_add')")
    public R save(@RequestBody ShipRecord shipRecord) {
        return R.ok(shipRecordService.save(shipRecord));
    }

    /**
     * 修改发货记录
     * @param shipRecord 发货记录
     * @return R
     */
    @ApiOperation(value = "修改发货记录", notes = "修改发货记录")
    @SysLog("修改发货记录")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_edit')")
    public R updateById(@RequestBody ShipRecord shipRecord) {
        return R.ok(shipRecordService.updateById(shipRecord));
    }

    /**
     * 通过id删除发货记录
     * @param id id
     * @return R
     */
    @SysLog("删除发货记录")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_del')")
    @ApiOperation(value = "通过id删除发货记录")
    public R removeById(@PathVariable String id) {
        return new R<>(shipRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除发货记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_del')")
    @ApiOperation(value = "根据IDS批量删除发货记录")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询发货记录")
    @PostMapping("/listByIds")
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_del')")
    @ApiOperation(value = "根据IDS批量查询发货记录")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shipRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询发货记录总记录")
    @PostMapping("/countByShipRecordReq")
    @ApiOperation(value = "查询发货记录总记录")
    public R countByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq) {

        return new R<>(shipRecordService.count(Wrappers.query(shipRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shipRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shipRecordReq查询一条发货记录记录")
    @PostMapping("/getOneByShipRecordReq")
    @ApiOperation(value = "根据shipRecordReq查询一条发货记录记录")
    public R getOneByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq) {

        return new R<>(shipRecordService.getOne(Wrappers.query(shipRecordReq), false));
    }


    /**
     * 批量修改OR插入发货记录
     * @param shipRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入发货记录")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('opc_shiprecord_edit')")
    @ApiOperation(value = "批量修改OR插入发货记录信息")
    public R saveOrUpdateBatch(@RequestBody List<ShipRecord> shipRecordList) {

        return new R<>(shipRecordService.saveOrUpdateBatch(shipRecordList));
    }


}
