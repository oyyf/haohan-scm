/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.req.payment.BuyerPaymentListReq;
import com.haohan.cloud.scm.api.opc.req.payment.BuyerPaymentReq;
import com.haohan.cloud.scm.opc.core.IBuyerPaymentCoreService;
import com.haohan.cloud.scm.opc.service.BuyerPaymentService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:28:21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/buyerpayment" )
@Api(value = "buyerpayment", tags = "buyerpayment管理")
public class BuyerPaymentController {

    private final BuyerPaymentService buyerPaymentService;
    private final IBuyerPaymentCoreService buyerPaymentCoreService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param buyerPayment 采购商货款统计
     * @return
     */
    @GetMapping("/page" )
    public R getBuyerPaymentPage(Page page, BuyerPayment buyerPayment) {
        return new R<>(buyerPaymentService.page(page, Wrappers.query(buyerPayment)));
    }

    /**
     * 分页查询
     * @param page 分页对象
     * @param req 查询条件
     * @return
     */
    @GetMapping("/fetchPage" )
    public R fetchPage(Page<BuyerPayment> page, @Validated BuyerPaymentListReq req) {
        QueryWrapper<BuyerPayment> queryWrapper = Wrappers.query(req.transTo());
        // 商家名称/客户名称  起止时间 / 订单状态
        queryWrapper.orderByDesc("buyer_payment_id")
                .lambda()
                .like(StrUtil.isNotEmpty(req.getMerchantName()), BuyerPayment::getMerchantName, req.getMerchantName())
                .like(StrUtil.isNotEmpty(req.getCustomerName()), BuyerPayment::getCustomerName, req.getCustomerName());
        //  起止时间
        if (null != req.getBeginDate() && null != req.getEndDate()) {
            queryWrapper
                    .lambda()
                    .ge(BuyerPayment::getBuyDate, req.getBeginDate())
                    .le(BuyerPayment::getBuyDate, req.getEndDate());
        }
        return R.ok(buyerPaymentService.page(page, queryWrapper));
    }



    /**
     * 通过id查询采购商货款统计
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(buyerPaymentService.getById(id));
    }

    /**
     * 新增采购商货款统计
     * @param buyerPayment 采购商货款统计
     * @return R
     */
    @SysLog("新增采购商货款统计" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_buyerpayment_add')" )
    public R save(@RequestBody BuyerPayment buyerPayment) {
        return R.failed("不支持");
    }

    /**
     * 修改采购商货款统计
     * @param buyerPayment 采购商货款统计
     * @return R
     */
    @SysLog("修改采购商货款统计" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_buyerpayment_edit')" )
    public R updateById(@RequestBody BuyerPayment buyerPayment) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除采购商货款统计
     * @param id id
     * @return R
     */
    @SysLog("删除采购商货款统计" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_buyerpayment_del')" )
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购商货款统计")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_buyerpayment_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return R.failed("不支持");
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购商货款统计")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyerPaymentService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param buyerPaymentReq 实体对象,可以为空
     * @return R
     */
    @PostMapping("/countByBuyerPaymentReq")
    public R countByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq) {

        return new R<>(buyerPaymentService.count(Wrappers.query(buyerPaymentReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param buyerPaymentReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据buyerPaymentReq查询一条货位信息表")
    @PostMapping("/getOneByBuyerPaymentReq")
    public R getOneByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq) {

        return new R<>(buyerPaymentService.getOne(Wrappers.query(buyerPaymentReq), false));
    }


    /**
     * 批量修改OR插入
     * @param buyerPaymentList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_buyerpayment_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<BuyerPayment> buyerPaymentList) {

        return R.failed("不支持");
    }


}
