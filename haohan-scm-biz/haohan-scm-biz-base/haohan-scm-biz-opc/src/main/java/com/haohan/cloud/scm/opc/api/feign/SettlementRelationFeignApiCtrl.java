/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SettlementRelation;
import com.haohan.cloud.scm.api.opc.req.settlement.SettlementRelationReq;
import com.haohan.cloud.scm.opc.service.SettlementRelationService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 结算单账单关系表
 *
 * @author haohan
 * @date 2019-09-18 17:36:02
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SettlementRelation")
@Api(value = "settlementrelation", tags = "settlementrelation-结算单账单关系表内部接口服务}")
public class SettlementRelationFeignApiCtrl {

    private final SettlementRelationService settlementRelationService;


    /**
     * 通过id查询结算单账单关系表
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询结算单账单关系表")
    public R getById(@PathVariable("id") String id) {
        return new R<>(settlementRelationService.getById(id));
    }


    /**
     * 分页查询 结算单账单关系表 列表信息
     *
     * @param settlementRelationReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSettlementRelationPage")
    @ApiOperation(value = "分页查询 结算单账单关系表 列表信息")
    public R getSettlementRelationPage(@RequestBody SettlementRelationReq settlementRelationReq) {
        Page page = new Page(settlementRelationReq.getPageNo(), settlementRelationReq.getPageSize());
        SettlementRelation settlementRelation = new SettlementRelation();
        BeanUtil.copyProperties(settlementRelationReq, settlementRelation);

        return new R<>(settlementRelationService.page(page, Wrappers.query(settlementRelation)));
    }


    /**
     * 全量查询 结算单账单关系表 列表信息
     *
     * @param settlementRelationReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSettlementRelationList")
    @ApiOperation(value = "全量查询 结算单账单关系表 列表信息")
    public R getSettlementRelationList(@RequestBody SettlementRelationReq settlementRelationReq) {
        SettlementRelation settlementRelation = new SettlementRelation();
        BeanUtil.copyProperties(settlementRelationReq, settlementRelation);

        return new R<>(settlementRelationService.list(Wrappers.query(settlementRelation)));
    }


    /**
     * 新增结算单账单关系表
     *
     * @param settlementRelation 结算单账单关系表
     * @return R
     */
    @Inner
    @SysLog("新增结算单账单关系表")
    @PostMapping("/add")
    @ApiOperation(value = "新增结算单账单关系表")
    public R save(@RequestBody SettlementRelation settlementRelation) {
        return new R<>(settlementRelationService.save(settlementRelation));
    }

    /**
     * 修改结算单账单关系表
     *
     * @param settlementRelation 结算单账单关系表
     * @return R
     */
    @Inner
    @SysLog("修改结算单账单关系表")
    @PostMapping("/update")
    @ApiOperation(value = "修改结算单账单关系表")
    public R updateById(@RequestBody SettlementRelation settlementRelation) {
        return new R<>(settlementRelationService.updateById(settlementRelation));
    }

    /**
     * 通过id删除结算单账单关系表
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除结算单账单关系表")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除结算单账单关系表")
    public R removeById(@PathVariable String id) {
        return new R<>(settlementRelationService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除结算单账单关系表")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除结算单账单关系表")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(settlementRelationService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询结算单账单关系表")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询结算单账单关系表")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(settlementRelationService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param settlementRelationReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询结算单账单关系表总记录")
    @PostMapping("/countBySettlementRelationReq")
    @ApiOperation(value = "查询结算单账单关系表总记录")
    public R countBySettlementRelationReq(@RequestBody SettlementRelationReq settlementRelationReq) {

        return new R<>(settlementRelationService.count(Wrappers.query(settlementRelationReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param settlementRelationReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据settlementRelationReq查询一条结算单账单关系表信息")
    @PostMapping("/getOneBySettlementRelationReq")
    @ApiOperation(value = "根据settlementRelationReq查询一条结算单账单关系表信息")
    public R getOneBySettlementRelationReq(@RequestBody SettlementRelationReq settlementRelationReq) {

        return new R<>(settlementRelationService.getOne(Wrappers.query(settlementRelationReq), false));
    }


    /**
     * 批量修改OR插入结算单账单关系表
     *
     * @param settlementRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入结算单账单关系表数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入结算单账单关系表数据")
    public R saveOrUpdateBatch(@RequestBody List<SettlementRelation> settlementRelationList) {

        return new R<>(settlementRelationService.saveOrUpdateBatch(settlementRelationList));
    }

}
