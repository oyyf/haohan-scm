/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.ShipRecord;
import com.haohan.cloud.scm.api.opc.req.ShipRecordReq;
import com.haohan.cloud.scm.api.opc.req.ship.ShipRecordFeignReq;
import com.haohan.cloud.scm.api.opc.vo.ShipRecordVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.opc.core.ShipManageCoreService;
import com.haohan.cloud.scm.opc.service.ShipRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * 发货记录
 *
 * @author haohan
 * @date 2020-01-06 14:16:07
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShipRecord")
@Api(value = "shiprecord", tags = "shiprecord-发货记录内部接口服务}")
public class ShipRecordFeignApiCtrl {

    private final ShipRecordService shipRecordService;
    private final ShipManageCoreService shipManageCoreService;


    /**
     * 通过id查询发货记录
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询发货记录")
    public R<ShipRecord> getById(@PathVariable("id") String id) {
        return R.ok(shipRecordService.getById(id));
    }


    /**
     * 分页查询 发货记录 列表信息
     *
     * @param req 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipRecordPage")
    @ApiOperation(value = "分页查询 发货记录 列表信息")
    public R<IPage<ShipRecord>> getShipRecordPage(@RequestBody ShipRecordFeignReq req) {
        return RUtil.success(shipManageCoreService.fetchPage(new Page(req.getCurrent(), req.getSize()), req));
    }


    /**
     * 通过sn查询发货记录详情
     *
     * @param shipRecordSn
     * @return R
     */
    @Inner
    @GetMapping("/fetchInfo/{shipRecordSn}")
    @ApiOperation(value = "通过id查询发货记录")
    public R<ShipRecordVO> fetchInfo(@PathVariable("shipRecordSn") String shipRecordSn) {
        return RUtil.success(shipManageCoreService.fetchInfo(shipRecordSn));
    }


    /**
     * 全量查询 发货记录 列表信息
     *
     * @param shipRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipRecordList")
    @ApiOperation(value = "全量查询 发货记录 列表信息")
    public R<List<ShipRecord>> getShipRecordList(@RequestBody ShipRecordReq shipRecordReq) {
        ShipRecord shipRecord = new ShipRecord();
        BeanUtil.copyProperties(shipRecordReq, shipRecord);
        return R.ok(shipRecordService.list(Wrappers.query(shipRecord)));
    }


    /**
     * 新增发货记录
     *
     * @param shipRecord 发货记录
     * @return R
     */
    @Inner
    @SysLog("新增发货记录")
    @PostMapping("/add")
    @ApiOperation(value = "新增发货记录")
    public R<Boolean> save(@RequestBody ShipRecord shipRecord) {
        return R.ok(shipRecordService.save(shipRecord));
    }

    /**
     * 修改发货记录
     *
     * @param shipRecord 发货记录
     * @return R
     */
    @Inner
    @SysLog("修改发货记录")
    @PostMapping("/update")
    @ApiOperation(value = "修改发货记录")
    public R<Boolean> updateById(@RequestBody ShipRecord shipRecord) {
        return R.ok(shipRecordService.updateById(shipRecord));
    }

    /**
     * 通过id删除发货记录
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除发货记录")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除发货记录")
    public R<Boolean> removeById(@PathVariable String id) {
        return R.ok(shipRecordService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除发货记录")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除发货记录")
    public R<Boolean> removeByIds(@RequestBody List<String> idList) {
        return R.ok(shipRecordService.removeByIds(idList));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询发货记录")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询发货记录")
    public R<List<ShipRecord>> listByIds(@RequestBody List<String> idList) {
        return R.ok(new ArrayList<>(shipRecordService.listByIds(idList)));
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询发货记录总记录")
    @PostMapping("/countByShipRecordReq")
    @ApiOperation(value = "查询发货记录总记录")
    public R<Integer> countByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq) {
        return R.ok(shipRecordService.count(Wrappers.query(shipRecordReq)));
    }

    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shipRecordReq查询一条发货记录信息")
    @PostMapping("/getOneByShipRecordReq")
    @ApiOperation(value = "根据shipRecordReq查询一条发货记录信息")
    public R<ShipRecord> getOneByShipRecordReq(@RequestBody ShipRecordReq shipRecordReq) {
        return R.ok(shipRecordService.getOne(Wrappers.query(shipRecordReq), false));
    }

    /**
     * 批量修改OR插入发货记录
     *
     * @param shipRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入发货记录数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入发货记录数据")
    public R<Boolean> saveOrUpdateBatch(@RequestBody List<ShipRecord> shipRecordList) {
        return R.ok(shipRecordService.saveOrUpdateBatch(shipRecordList));
    }

    @Inner
    @PostMapping("/createByOrder")
    @ApiOperation(value = "根据订单创建发货记录(已存在发货记录则修改)")
    public R<ShipRecord> createShipRecordByOrder(@RequestBody ShipRecordFeignReq req) {
        return R.ok(shipManageCoreService.createShipRecordByOrder(req.getOrderSn(), req.getOrderType()));
    }

    @Inner
    @PostMapping("/createBatchByOrder")
    @ApiOperation(value = "根据订单创建发货记录(批量)")
    public R<Boolean> createShipRecordBatchByOrder(@RequestBody ShipRecordFeignReq req) {
        return R.ok(shipManageCoreService.createShipRecordBatchByOrder(req.getOrderSnSet(), req.getOrderType()));
    }

    @Inner
    @PostMapping("/deleteShipRecordByOrder")
    @ApiOperation(value = "删除发货记录 (不为已发货)")
    public R<Boolean> deleteShipRecord(@RequestBody ShipRecordFeignReq req) {
        return R.ok(shipManageCoreService.deleteShipRecordByOrder(req.getOrderSn(), req.getOrderType()));
    }

    @Inner
    @PostMapping("/fetchOneByOrder")
    @ApiOperation(value = "通过订单sn查询发货记录")
    public R<ShipRecord> fetchOneByOrder(@RequestBody ShipRecordFeignReq req) {
        return R.ok(shipRecordService.fetchByOrderSn(req.getOrderSn()));
    }

    @Inner
    @SysLog("发货记录商家名称修改")
    @PostMapping("/updateMerchantName")
    public R<Boolean> updateMerchantName(@RequestBody ShipRecordReq req) {
        return RUtil.success(shipManageCoreService.updateMerchantName(req.getPmId(), req.getPmName()));
    }


    @Inner
    @PostMapping("/updateShipRecordByOrder")
    @ApiOperation(value = "根据订单信息更新发货记录")
    public R<Boolean> updateShipRecordByOrder(@RequestBody ShipRecordFeignReq req) {
        return R.ok(shipManageCoreService.updateShipRecordByOrder(req.getOrderSn(), req.getOrderType()));
    }

}
