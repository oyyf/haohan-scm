package com.haohan.cloud.scm.opc.core;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.req.CreatePurchaseOrderWithSummaryReq;
import com.haohan.cloud.scm.api.opc.req.OpcSummaryOrderListReq;
import com.haohan.cloud.scm.api.opc.resp.TemporarySummaryOrderResp;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseOrderResp;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;

import java.util.List;


/**
 * @author cx
 * @date 2019/5/29
 */
public interface IScmOpcDemandSummaryService {

  /**
   *  根据当前时间与批次查询客户下单、汇总商品数量
   * @param req
   * @return
   */
  List<BuyOrderDetailDTO> queryDemandSummaryList(SalebSummaryGoodsNumReq req);

  /**
   * 返回汇总单和汇总临时数据列表
   * @param req
   * @return
   */
  List<TemporarySummaryOrderResp> getSummaryOrderList(OpcSummaryOrderListReq req);

    /**
     * 保存汇总单
     * @param resp
     * @return
     */
  List<SummaryOrder> createSummaryOrderForOrder(List<TemporarySummaryOrderResp> resp);

  /**
     * 创建汇总单 并生成采购单
     * @param req
     * @return
     */
  PurchaseOrderResp createPurchaseOrderWithSummaryOrder(CreatePurchaseOrderWithSummaryReq req);


}
