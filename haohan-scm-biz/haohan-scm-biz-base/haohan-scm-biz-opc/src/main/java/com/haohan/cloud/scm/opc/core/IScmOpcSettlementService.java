package com.haohan.cloud.scm.opc.core;

import com.haohan.cloud.scm.api.opc.dto.SettlementRecordDTO;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;
import com.haohan.cloud.scm.api.opc.req.CompleteSettlementReq;
import com.haohan.cloud.scm.api.opc.req.ReadySettlementReq;
import com.haohan.cloud.scm.api.pay.entity.OrderPayRecord;
import com.pig4cloud.pigx.common.core.util.R;

/**
 * @author dy
 * @date 2019/7/25
 */
public interface IScmOpcSettlementService {

    /**
     * 准备结算(发起结算)  多个账单
     * 先判断对应的账单 是否已审核
     * 通过时会创建对应结算记录(初始化,未结算)
     * 失败时不创建结算记录
     *
     * @param req 必需:pmId/ paymentList
     * @return 失败时返回null
     */
    R<SettlementRecord> readySettlement(ReadySettlementReq req);

    /**
     * 完成结算
     *    若提交金额和账单金额不一致 不结算
     * 修改对应账单状态
     *
     * @param settlementRecord 必需:settlementId /settlementAmount / payType/
     *                         / payDate / GroupNum/companyOperator/operator
     *                         可选:settlementDesc
     * @return
     */
    R<Boolean> completeSettlement(SettlementRecord settlementRecord);

    /**
     * 完成结算  先处理图片
     *
     * @param req
     * @return
     */
    R<Boolean> completeSettlementWithPhoto(CompleteSettlementReq req);

    /**
     * 结算详情
     *
     * @param settlementRecord
     * @return
     */
    SettlementRecordDTO queryInfo(SettlementRecord settlementRecord);

    /**
     * 在线支付后完成结算
     * @param payRecord
     * @return
     */
    R<Boolean> payToSettlement(OrderPayRecord payRecord);
}
