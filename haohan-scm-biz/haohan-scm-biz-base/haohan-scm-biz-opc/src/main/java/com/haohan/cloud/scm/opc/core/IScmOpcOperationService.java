package com.haohan.cloud.scm.opc.core;

import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.req.NotSatisfiedReq;
import com.haohan.cloud.scm.api.opc.req.OpcAddPurchaseOrderReq;
import com.haohan.cloud.scm.api.opc.req.OpcCreateTradeOrderReq;
import com.haohan.cloud.scm.api.opc.req.SendPurchaseTaskReq;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseOrderResp;
import com.haohan.cloud.scm.api.saleb.req.SalebSummaryGoodsNumReq;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/25
 */
public interface IScmOpcOperationService {


    /**
     * 查看单商品购买需求
     * 运营  汇总采购商品(数量 展示) buyOrderDetail
     *
     * @param req
     * @return
     */
    SummaryOrder queryGoodsNeeds(SalebSummaryGoodsNumReq req);

    /**
     * 商品需求汇总 创建汇总单
     * 根据需采购商品 创建 汇总单数据(单商品) summaryOrder
     * 必需参数: pmId/buySeq/goodsModelId/deliveryTime/allocateType
     *
     * @param req
     * @return
     */
    SummaryOrder createSummaryByGoodsNeeds(SummaryOrder req);


    /**
     * 根据 客户采购单明细 buyOrderDetail 创建 交易单 tradeOrder
     *
     * @param req
     * @return
     */
    TradeOrder createTradeOrderBySorting(OpcCreateTradeOrderReq req);

    /**
     * 根据采购方式类型设置属性
     *
     * @param req
     * @return
     */
    List<PurchaseOrderDetail> setPropertiesByType(NotSatisfiedReq req);

    /**
     * 运营 向采购部 发采购任务
     * @param req
     * @return
     */
    Boolean sendPurchaseTask(SendPurchaseTaskReq req);

    /**
     * 运营 生成采购单
     * @param req
     * @return
     */
    PurchaseOrderResp addPurchaseOrder(OpcAddPurchaseOrderReq req);

}
