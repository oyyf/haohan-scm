/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.DealCostDetail;
import com.haohan.cloud.scm.api.opc.req.DealCostDetailReq;
import com.haohan.cloud.scm.opc.service.DealCostDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 交易成本明细
 *
 * @author haohan
 * @date 2019-05-30 10:17:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/DealCostDetail")
@Api(value = "dealcostdetail", tags = "dealcostdetail内部接口服务")
public class DealCostDetailFeignApiCtrl {

    private final DealCostDetailService dealCostDetailService;


    /**
     * 通过id查询交易成本明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(dealCostDetailService.getById(id));
    }


    /**
     * 分页查询 交易成本明细 列表信息
     * @param dealCostDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchDealCostDetailPage")
    public R getDealCostDetailPage(@RequestBody DealCostDetailReq dealCostDetailReq) {
        Page page = new Page(dealCostDetailReq.getPageNo(), dealCostDetailReq.getPageSize());
        DealCostDetail dealCostDetail =new DealCostDetail();
        BeanUtil.copyProperties(dealCostDetailReq, dealCostDetail);

        return new R<>(dealCostDetailService.page(page, Wrappers.query(dealCostDetail)));
    }


    /**
     * 全量查询 交易成本明细 列表信息
     * @param dealCostDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchDealCostDetailList")
    public R getDealCostDetailList(@RequestBody DealCostDetailReq dealCostDetailReq) {
        DealCostDetail dealCostDetail =new DealCostDetail();
        BeanUtil.copyProperties(dealCostDetailReq, dealCostDetail);

        return new R<>(dealCostDetailService.list(Wrappers.query(dealCostDetail)));
    }


    /**
     * 新增交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @Inner
    @SysLog("新增交易成本明细")
    @PostMapping("/add")
    public R save(@RequestBody DealCostDetail dealCostDetail) {
        return new R<>(dealCostDetailService.save(dealCostDetail));
    }

    /**
     * 修改交易成本明细
     * @param dealCostDetail 交易成本明细
     * @return R
     */
    @Inner
    @SysLog("修改交易成本明细")
    @PostMapping("/update")
    public R updateById(@RequestBody DealCostDetail dealCostDetail) {
        return new R<>(dealCostDetailService.updateById(dealCostDetail));
    }

    /**
     * 通过id删除交易成本明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除交易成本明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(dealCostDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除交易成本明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(dealCostDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询交易成本明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(dealCostDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询交易成本明细总记录}")
    @PostMapping("/countByDealCostDetailReq")
    public R countByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq) {

        return new R<>(dealCostDetailService.count(Wrappers.query(dealCostDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param dealCostDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据dealCostDetailReq查询一条货位信息表")
    @PostMapping("/getOneByDealCostDetailReq")
    public R getOneByDealCostDetailReq(@RequestBody DealCostDetailReq dealCostDetailReq) {

        return new R<>(dealCostDetailService.getOne(Wrappers.query(dealCostDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param dealCostDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<DealCostDetail> dealCostDetailList) {

        return new R<>(dealCostDetailService.saveOrUpdateBatch(dealCostDetailList));
    }

}
