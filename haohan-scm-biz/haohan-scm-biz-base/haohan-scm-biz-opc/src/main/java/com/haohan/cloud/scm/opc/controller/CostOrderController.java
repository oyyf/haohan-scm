/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.CostOrder;
import com.haohan.cloud.scm.api.opc.req.CostOrderReq;
import com.haohan.cloud.scm.opc.service.CostOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 成本核算单
 *
 * @author haohan
 * @date 2019-05-30 10:17:10
 */
@RestController
@AllArgsConstructor
@RequestMapping("/costorder" )
@Api(value = "costorder", tags = "costorder管理")
public class CostOrderController {

    private final CostOrderService costOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param costOrder 成本核算单
     * @return
     */
    @GetMapping("/page" )
    public R getCostOrderPage(Page page, CostOrder costOrder) {
        return new R<>(costOrderService.page(page, Wrappers.query(costOrder)));
    }


    /**
     * 通过id查询成本核算单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(costOrderService.getById(id));
    }

    /**
     * 新增成本核算单
     * @param costOrder 成本核算单
     * @return R
     */
    @SysLog("新增成本核算单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_costorder_add')" )
    public R save(@RequestBody CostOrder costOrder) {
        return new R<>(costOrderService.save(costOrder));
    }

    /**
     * 修改成本核算单
     * @param costOrder 成本核算单
     * @return R
     */
    @SysLog("修改成本核算单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_costorder_edit')" )
    public R updateById(@RequestBody CostOrder costOrder) {
        return new R<>(costOrderService.updateById(costOrder));
    }

    /**
     * 通过id删除成本核算单
     * @param id id
     * @return R
     */
    @SysLog("删除成本核算单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_costorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(costOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除成本核算单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_costorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(costOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询成本核算单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(costOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param costOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询成本核算单总记录}")
    @PostMapping("/countByCostOrderReq")
    public R countByCostOrderReq(@RequestBody CostOrderReq costOrderReq) {

        return new R<>(costOrderService.count(Wrappers.query(costOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param costOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据costOrderReq查询一条货位信息表")
    @PostMapping("/getOneByCostOrderReq")
    public R getOneByCostOrderReq(@RequestBody CostOrderReq costOrderReq) {

        return new R<>(costOrderService.getOne(Wrappers.query(costOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param costOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_costorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CostOrder> costOrderList) {

        return new R<>(costOrderService.saveOrUpdateBatch(costOrderList));
    }


}
