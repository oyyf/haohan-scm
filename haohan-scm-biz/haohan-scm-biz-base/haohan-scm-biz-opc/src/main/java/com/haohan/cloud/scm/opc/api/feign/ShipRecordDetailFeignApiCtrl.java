/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import com.haohan.cloud.scm.api.opc.req.ShipRecordDetailReq;
import com.haohan.cloud.scm.opc.service.ShipRecordDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShipRecordDetail")
@Api(value = "shiprecorddetail", tags = "shiprecorddetail-发货记录明细内部接口服务}")
public class ShipRecordDetailFeignApiCtrl {

    private final ShipRecordDetailService shipRecordDetailService;


    /**
     * 通过id查询发货记录明细
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询发货记录明细")
    public R<ShipRecordDetail> getById(@PathVariable("id") String id) {
        return R.ok(shipRecordDetailService.getById(id));
    }


    /**
     * 分页查询 发货记录明细 列表信息
     *
     * @param shipRecordDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipRecordDetailPage")
    @ApiOperation(value = "分页查询 发货记录明细 列表信息")
    public R<IPage<ShipRecordDetail>> getShipRecordDetailPage(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {
        Page<ShipRecordDetail> page = new Page<>(shipRecordDetailReq.getPageNo(), shipRecordDetailReq.getPageSize());
        ShipRecordDetail shipRecordDetail = new ShipRecordDetail();
        BeanUtil.copyProperties(shipRecordDetailReq, shipRecordDetail);
        return R.ok(shipRecordDetailService.page(page, Wrappers.query(shipRecordDetail)));
    }


    /**
     * 全量查询 发货记录明细 列表信息
     *
     * @param shipRecordDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShipRecordDetailList")
    @ApiOperation(value = "全量查询 发货记录明细 列表信息")
    public R<List<ShipRecordDetail>> getShipRecordDetailList(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {
        ShipRecordDetail shipRecordDetail = new ShipRecordDetail();
        BeanUtil.copyProperties(shipRecordDetailReq, shipRecordDetail);
        return R.ok(shipRecordDetailService.list(Wrappers.query(shipRecordDetail)));
    }


    /**
     * 新增发货记录明细
     *
     * @param shipRecordDetail 发货记录明细
     * @return R
     */
    @Inner
    @SysLog("新增发货记录明细")
    @PostMapping("/add")
    @ApiOperation(value = "新增发货记录明细")
    public R<Boolean> save(@RequestBody ShipRecordDetail shipRecordDetail) {
        return R.ok(shipRecordDetailService.save(shipRecordDetail));
    }

    /**
     * 修改发货记录明细
     *
     * @param shipRecordDetail 发货记录明细
     * @return R
     */
    @Inner
    @SysLog("修改发货记录明细")
    @PostMapping("/update")
    @ApiOperation(value = "修改发货记录明细")
    public R<Boolean> updateById(@RequestBody ShipRecordDetail shipRecordDetail) {
        return R.ok(shipRecordDetailService.updateById(shipRecordDetail));
    }

    /**
     * 通过id删除发货记录明细
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除发货记录明细")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除发货记录明细")
    public R<Boolean> removeById(@PathVariable String id) {
        return R.ok(shipRecordDetailService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除发货记录明细")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除发货记录明细")
    public R<Boolean> removeByIds(@RequestBody List<String> idList) {
        return R.ok(shipRecordDetailService.removeByIds(idList));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询发货记录明细")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询发货记录明细")
    public R<List<ShipRecordDetail>> listByIds(@RequestBody List<String> idList) {
        return R.ok(new ArrayList<>(shipRecordDetailService.listByIds(idList)));
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipRecordDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询发货记录明细总记录")
    @PostMapping("/countByShipRecordDetailReq")
    @ApiOperation(value = "查询发货记录明细总记录")
    public R<Integer> countByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {
        return R.ok(shipRecordDetailService.count(Wrappers.query(shipRecordDetailReq)));
    }

    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipRecordDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shipRecordDetailReq查询一条发货记录明细信息")
    @PostMapping("/getOneByShipRecordDetailReq")
    @ApiOperation(value = "根据shipRecordDetailReq查询一条发货记录明细信息")
    public R<ShipRecordDetail> getOneByShipRecordDetailReq(@RequestBody ShipRecordDetailReq shipRecordDetailReq) {
        return R.ok(shipRecordDetailService.getOne(Wrappers.query(shipRecordDetailReq), false));
    }

    /**
     * 批量修改OR插入发货记录明细
     *
     * @param shipRecordDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入发货记录明细数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入发货记录明细数据")
    public R<Boolean> saveOrUpdateBatch(@RequestBody List<ShipRecordDetail> shipRecordDetailList) {
        return R.ok(shipRecordDetailService.saveOrUpdateBatch(shipRecordDetailList));
    }

}
