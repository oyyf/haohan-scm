/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.SettlementRecord;

/**
 * 结算记录
 *
 * @author haohan
 * @date 2019-05-13 20:39:12
 */
public interface SettlementRecordService extends IService<SettlementRecord> {

    /**
     * 根据 结算记录编号查询
     * @param settlementRecord pmId可传
     * @return
     */
    SettlementRecord fetchBySn(SettlementRecord settlementRecord);
}
