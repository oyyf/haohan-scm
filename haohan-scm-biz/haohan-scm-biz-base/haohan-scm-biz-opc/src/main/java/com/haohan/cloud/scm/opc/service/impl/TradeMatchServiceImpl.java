/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.TradeMatch;
import com.haohan.cloud.scm.opc.mapper.TradeMatchMapper;
import com.haohan.cloud.scm.opc.service.TradeMatchService;
import org.springframework.stereotype.Service;

/**
 * 交易匹配
 *
 * @author haohan
 * @date 2019-05-13 20:39:36
 */
@Service
public class TradeMatchServiceImpl extends ServiceImpl<TradeMatchMapper, TradeMatch> implements TradeMatchService {

}
