/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.opc.mapper.SupplierPaymentMapper;
import com.haohan.cloud.scm.opc.service.SupplierPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 供应商货款统计
 *
 * @author haohan
 * @date 2019-05-13 17:45:06
 */
@Service
public class SupplierPaymentServiceImpl extends ServiceImpl<SupplierPaymentMapper, SupplierPayment> implements SupplierPaymentService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SupplierPayment entity) {
        if (StrUtil.isEmpty(entity.getSupplierPaymentId())) {
            String sn = scmIncrementUtil.inrcSnByClass(SupplierPayment.class, NumberPrefixConstant.SUPPLIER_PAYMENT_SN_PRE);
            entity.setSupplierPaymentId(sn);
        }
        return super.save(entity);
    }

    @Override
    public SupplierPayment fetchBySn(SupplierPayment payment) {
        QueryWrapper<SupplierPayment> query = new QueryWrapper<>();
        query.lambda()
                .eq(SupplierPayment::getSupplierPaymentId, payment.getSupplierPaymentId())
                .eq(StrUtil.isNotBlank(payment.getPmId()), SupplierPayment::getPmId, payment.getPmId());
        return super.getOne(query);
    }
}
