/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.BuyerPayment;
import com.haohan.cloud.scm.api.opc.req.payment.BuyerPaymentReq;
import com.haohan.cloud.scm.api.opc.req.payment.CountReceivableBillReq;
import com.haohan.cloud.scm.api.opc.req.payment.CreateReceivableReq;
import com.haohan.cloud.scm.opc.core.IBuyerPaymentCoreService;
import com.haohan.cloud.scm.opc.service.BuyerPaymentService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购商货款统计
 *
 * @author haohan
 * @date 2019-05-29 13:28:21
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/BuyerPayment")
@Api(value = "buyerpayment", tags = "buyerpayment内部接口服务")
public class BuyerPaymentFeignApiCtrl {

    private final BuyerPaymentService buyerPaymentService;
    private final IBuyerPaymentCoreService buyerPaymentCoreService;


    /**
     * 通过id查询采购商货款统计
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(buyerPaymentService.getById(id));
    }


    /**
     * 分页查询 采购商货款统计 列表信息
     * @param buyerPaymentReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyerPaymentPage")
    public R getBuyerPaymentPage(@RequestBody BuyerPaymentReq buyerPaymentReq) {
        Page page = new Page(buyerPaymentReq.getPageNo(), buyerPaymentReq.getPageSize());
        BuyerPayment buyerPayment =new BuyerPayment();
        BeanUtil.copyProperties(buyerPaymentReq, buyerPayment);

        return new R<>(buyerPaymentService.page(page, Wrappers.query(buyerPayment)));
    }


    /**
     * 全量查询 采购商货款统计 列表信息
     * @param buyerPaymentReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyerPaymentList")
    public R getBuyerPaymentList(@RequestBody BuyerPaymentReq buyerPaymentReq) {
        BuyerPayment buyerPayment =new BuyerPayment();
        BeanUtil.copyProperties(buyerPaymentReq, buyerPayment);

        return new R<>(buyerPaymentService.list(Wrappers.query(buyerPayment)));
    }


    /**
     * 新增采购商货款统计
     * @param buyerPayment 采购商货款统计
     * @return R
     */
    @Inner
    @SysLog("新增采购商货款统计")
    @PostMapping("/add")
    public R save(@RequestBody BuyerPayment buyerPayment) {
        return new R<>(buyerPaymentService.save(buyerPayment));
    }

    /**
     * 修改采购商货款统计
     * @param buyerPayment 采购商货款统计
     * @return R
     */
    @Inner
    @SysLog("修改采购商货款统计")
    @PostMapping("/update")
    public R updateById(@RequestBody BuyerPayment buyerPayment) {
        return new R<>(buyerPaymentService.updateById(buyerPayment));
    }

    /**
     * 通过id删除采购商货款统计
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购商货款统计")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(buyerPaymentService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购商货款统计")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(buyerPaymentService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购商货款统计")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyerPaymentService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param buyerPaymentReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购商货款统计总记录}")
    @PostMapping("/countByBuyerPaymentReq")
    public R countByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq) {

        return new R<>(buyerPaymentService.count(Wrappers.query(buyerPaymentReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param buyerPaymentReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据buyerPaymentReq查询一条货位信息表")
    @PostMapping("/getOneByBuyerPaymentReq")
    public R getOneByBuyerPaymentReq(@RequestBody BuyerPaymentReq buyerPaymentReq) {

        return new R<>(buyerPaymentService.getOne(Wrappers.query(buyerPaymentReq), false));
    }


    /**
     * 批量修改OR插入
     * @param buyerPaymentList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<BuyerPayment> buyerPaymentList) {

        return new R<>(buyerPaymentService.saveOrUpdateBatch(buyerPaymentList));
    }

    /**
     * 查询应收账单 是否通过审核 带商家名称
     * @param buyerPayment 必需 pmId / BuyerPaymentId
     * @return 不为确认状态时 返回null
     */
    @Inner
    @PostMapping("/checkPayment")
    public R<BuyerPayment> checkPayment(@RequestBody BuyerPayment buyerPayment) {
        if (StrUtil.isEmpty(buyerPayment.getPmId()) || StrUtil.isEmpty(buyerPayment.getBuyerPaymentId())) {
            return R.failed("缺少必需参数pmId/buyerPaymentId");
        }
        return R.ok(buyerPaymentCoreService.checkPayment(buyerPayment));
    }

    @Inner
    @PostMapping("/countBill")
    @ApiOperation(value = "查询到期的应收账单数 (未结算)")
    public R<Integer> countReceivableBill(@RequestBody CountReceivableBillReq req) {
        return R.ok(buyerPaymentCoreService.countReceivableBill(req));
    }

    @Inner
    @PostMapping("/createReceivable")
    @ApiOperation(value = "创建应收账单")
    public R<BuyerPayment> createReceivable(@RequestBody CreateReceivableReq req) {
        BuyerPayment buyerPayment = req.transTo();
        buyerPayment = buyerPaymentCoreService.createReceivable(buyerPayment);
        if (null == buyerPayment) {
            return R.failed("创建应收账单失败");
        }
        return R.ok(buyerPayment);
    }

}
