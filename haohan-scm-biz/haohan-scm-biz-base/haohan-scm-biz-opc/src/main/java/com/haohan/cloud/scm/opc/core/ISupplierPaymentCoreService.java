package com.haohan.cloud.scm.opc.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SupplierPayment;
import com.haohan.cloud.scm.api.opc.req.payment.FetchSupplierPaymentReq;
import com.haohan.cloud.scm.api.opc.req.payment.PaymentDetailReq;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.resp.FetchSupplierPaymentResp;

import java.util.List;

/**
 * @author xwx
 * @date 2019/7/17
 */
public interface ISupplierPaymentCoreService {

    /**
     * 查询应付账单详情
     * @param req
     * @return
     */
    FetchSupplierPaymentResp fetchSupplierPayment(FetchSupplierPaymentReq req);

    /**
     * 创建应付账单
     *  根据 应付来源订单编号:payableSn/账单类型:billType 去查对应订单获取金额
     * @param supplierPayment 必需:pmId/payableSn/billType
     * @return
     */
    SupplierPayment createPayable(SupplierPayment supplierPayment);

    /**
     * 查询应付账单 是否通过审核 带商家名称
     * @param supplierPayment 必需 pmId / supplierPaymentId
     * @return 不为确认状态时 返回null
     */
    SupplierPayment checkPayment(SupplierPayment supplierPayment);

    /**
     * 批量创建 应付账单  都是 采购应付
     *  根据 备货日期/批次
     * @param offerOrder pmId/ 备货日期/批次
     * @return
     */
    String createPayableBatch(SupplyOrder offerOrder);

    /**
     * 查询应付账单是否可审核 对应订单需为确认状态
     * @param pmId
     * @param supplierPaymentId
     * @return 账单更新金额
     */
    SupplierPayment checkPayable(String pmId, String supplierPaymentId);

    /**
     *  审核应付账单
     * @param supplierPayment
     * @return
     */
    boolean reviewPayment(SupplierPayment supplierPayment);

    /**
     * 分页查询
     * @param page
     * @param supplierPayment
     * @return
     */
    IPage fetchPage(Page page, SupplierPayment supplierPayment);

    /**
     * 查询账单详情
     * @param req
     * @return
     */
    List queryPaymentDetail(PaymentDetailReq req);
}
