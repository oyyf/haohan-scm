/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.TradeMatch;
import com.haohan.cloud.scm.api.opc.req.TradeMatchReq;
import com.haohan.cloud.scm.opc.service.TradeMatchService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 交易匹配
 *
 * @author haohan
 * @date 2019-05-30 10:21:33
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/TradeMatch")
@Api(value = "tradematch", tags = "tradematch内部接口服务")
public class TradeMatchFeignApiCtrl {

    private final TradeMatchService tradeMatchService;


    /**
     * 通过id查询交易匹配
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(tradeMatchService.getById(id));
    }


    /**
     * 分页查询 交易匹配 列表信息
     * @param tradeMatchReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTradeMatchPage")
    public R getTradeMatchPage(@RequestBody TradeMatchReq tradeMatchReq) {
        Page page = new Page(tradeMatchReq.getPageNo(), tradeMatchReq.getPageSize());
        TradeMatch tradeMatch =new TradeMatch();
        BeanUtil.copyProperties(tradeMatchReq, tradeMatch);

        return new R<>(tradeMatchService.page(page, Wrappers.query(tradeMatch)));
    }


    /**
     * 全量查询 交易匹配 列表信息
     * @param tradeMatchReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchTradeMatchList")
    public R getTradeMatchList(@RequestBody TradeMatchReq tradeMatchReq) {
        TradeMatch tradeMatch =new TradeMatch();
        BeanUtil.copyProperties(tradeMatchReq, tradeMatch);

        return new R<>(tradeMatchService.list(Wrappers.query(tradeMatch)));
    }


    /**
     * 新增交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @Inner
    @SysLog("新增交易匹配")
    @PostMapping("/add")
    public R save(@RequestBody TradeMatch tradeMatch) {
        return new R<>(tradeMatchService.save(tradeMatch));
    }

    /**
     * 修改交易匹配
     * @param tradeMatch 交易匹配
     * @return R
     */
    @Inner
    @SysLog("修改交易匹配")
    @PostMapping("/update")
    public R updateById(@RequestBody TradeMatch tradeMatch) {
        return new R<>(tradeMatchService.updateById(tradeMatch));
    }

    /**
     * 通过id删除交易匹配
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除交易匹配")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(tradeMatchService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除交易匹配")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(tradeMatchService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询交易匹配")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(tradeMatchService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询交易匹配总记录}")
    @PostMapping("/countByTradeMatchReq")
    public R countByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq) {

        return new R<>(tradeMatchService.count(Wrappers.query(tradeMatchReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param tradeMatchReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据tradeMatchReq查询一条货位信息表")
    @PostMapping("/getOneByTradeMatchReq")
    public R getOneByTradeMatchReq(@RequestBody TradeMatchReq tradeMatchReq) {

        return new R<>(tradeMatchService.getOne(Wrappers.query(tradeMatchReq), false));
    }


    /**
     * 批量修改OR插入
     * @param tradeMatchList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<TradeMatch> tradeMatchList) {

        return new R<>(tradeMatchService.saveOrUpdateBatch(tradeMatchList));
    }

}
