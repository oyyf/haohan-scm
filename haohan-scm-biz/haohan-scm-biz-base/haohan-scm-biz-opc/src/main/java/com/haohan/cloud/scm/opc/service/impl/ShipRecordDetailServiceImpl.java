/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;
import com.haohan.cloud.scm.opc.mapper.ShipRecordDetailMapper;
import com.haohan.cloud.scm.opc.service.ShipRecordDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
@Service
public class ShipRecordDetailServiceImpl extends ServiceImpl<ShipRecordDetailMapper, ShipRecordDetail> implements ShipRecordDetailService {

    @Override
    public List<ShipRecordDetail> fetchListBySn(String shipSn) {
        return baseMapper.selectList(Wrappers.<ShipRecordDetail>query().lambda()
                .eq(ShipRecordDetail::getShipRecordSn, shipSn)
        );
    }
}
