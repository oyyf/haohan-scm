/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.PlatformEmployee;

/**
 * 平台员工管理
 *
 * @author haohan
 * @date 2019-05-13 20:33:57
 */
public interface PlatformEmployeeService extends IService<PlatformEmployee> {

}
