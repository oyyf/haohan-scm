/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.req.ShipperReq;
import com.haohan.cloud.scm.api.opc.entity.Shipper;
import com.haohan.cloud.scm.opc.service.ShipperService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 发货人
 *
 * @author haohan
 * @date 2020-03-04 13:37:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/shipper")
@Api(value = "shipper", tags = "发货人管理")
public class ShipperController {

    private final ShipperService shipperService;

    /**
     * 分页查询
     *
     * @param page    分页对象
     * @param shipper 发货人
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public R getShipperPage(Page page, Shipper shipper) {
        return R.ok(shipperService.page(page, Wrappers.query(shipper)));
    }


    /**
     * 通过id查询发货人
     *
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(shipperService.getById(id));
    }

    /**
     * 新增发货人
     *
     * @param shipper 发货人
     * @return R
     */
    @ApiOperation(value = "新增发货人", notes = "新增发货人")
    @SysLog("新增发货人")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('opc_shipper_add')")
    public R save(@RequestBody Shipper shipper) {
        return R.ok(shipperService.save(shipper));
    }

    /**
     * 修改发货人
     *
     * @param shipper 发货人
     * @return R
     */
    @ApiOperation(value = "修改发货人", notes = "修改发货人")
    @SysLog("修改发货人")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('opc_shipper_edit')")
    public R updateById(@RequestBody Shipper shipper) {
        return R.ok(shipperService.updateById(shipper));
    }

    /**
     * 通过id删除发货人
     *
     * @param id id
     * @return R
     */
    @SysLog("删除发货人")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('opc_shipper_del')")
    @ApiOperation(value = "通过id删除发货人")
    public R removeById(@PathVariable String id) {
        return new R<>(shipperService.removeById(id));
    }


    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @SysLog("批量删除发货人")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('opc_shipper_del')")
    @ApiOperation(value = "根据IDS批量删除发货人")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shipperService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @SysLog("根据IDS批量查询发货人")
    @PostMapping("/listByIds")
    @PreAuthorize("@pms.hasPermission('opc_shipper_del')")
    @ApiOperation(value = "根据IDS批量查询发货人")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shipperService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param shipperReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询发货人总记录")
    @PostMapping("/countByShipperReq")
    @ApiOperation(value = "查询发货人总记录")
    public R countByShipperReq(@RequestBody ShipperReq shipperReq) {

        return new R<>(shipperService.count(Wrappers.query(shipperReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param shipperReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据shipperReq查询一条发货人记录")
    @PostMapping("/getOneByShipperReq")
    @ApiOperation(value = "根据shipperReq查询一条发货人记录")
    public R getOneByShipperReq(@RequestBody ShipperReq shipperReq) {

        return new R<>(shipperService.getOne(Wrappers.query(shipperReq), false));
    }


    /**
     * 批量修改OR插入发货人
     *
     * @param shipperList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入发货人")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('opc_shipper_edit')")
    @ApiOperation(value = "批量修改OR插入发货人信息")
    public R saveOrUpdateBatch(@RequestBody List<Shipper> shipperList) {

        return new R<>(shipperService.saveOrUpdateBatch(shipperList));
    }


}
