/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.opc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.opc.entity.CostOrder;
import com.haohan.cloud.scm.opc.mapper.CostOrderMapper;
import com.haohan.cloud.scm.opc.service.CostOrderService;
import org.springframework.stereotype.Service;

/**
 * 成本核算单
 *
 * @author haohan
 * @date 2019-05-13 20:36:32
 */
@Service
public class CostOrderServiceImpl extends ServiceImpl<CostOrderMapper, CostOrder> implements CostOrderService {

}
