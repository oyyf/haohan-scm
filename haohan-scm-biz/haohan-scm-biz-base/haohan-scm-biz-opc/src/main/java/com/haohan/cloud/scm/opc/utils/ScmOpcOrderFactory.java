package com.haohan.cloud.scm.opc.utils;

import com.haohan.cloud.scm.api.constant.enums.bill.OrderTypeEnum;
import com.haohan.cloud.scm.api.order.OrderCommonService;
import com.haohan.cloud.scm.api.order.impl.BuyOrderFeignCommonServiceImpl;
import com.haohan.cloud.scm.api.order.impl.ReturnOrderFeignCommonServiceImpl;
import com.haohan.cloud.scm.api.order.impl.SalesOrderFeignCommonServiceImpl;
import com.haohan.cloud.scm.api.order.impl.SupplyOrderFeignCommonServiceImpl;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/6/16
 * 各种订单
 */
@Component
@AllArgsConstructor
public class ScmOpcOrderFactory {

    private final BuyOrderFeignCommonServiceImpl buyOrderService;
    private final SupplyOrderFeignCommonServiceImpl supplyOrderService;
    private final ReturnOrderFeignCommonServiceImpl returnOrderService;
    private final SalesOrderFeignCommonServiceImpl salesOrderService;
    private static final Map<OrderTypeEnum, OrderCommonService> SERVICE_MAP = new HashMap<>(8);

    @PostConstruct
    private void init() {
        // 注册服务
        SERVICE_MAP.put(OrderTypeEnum.buy, buyOrderService);
        SERVICE_MAP.put(OrderTypeEnum.supply, supplyOrderService);
        SERVICE_MAP.put(OrderTypeEnum.back, returnOrderService);
        SERVICE_MAP.put(OrderTypeEnum.sales, salesOrderService);
    }

    /**
     * 获取订单服务对象
     *
     * @param type
     * @return
     */
    public static OrderCommonService getOrderService(OrderTypeEnum type) {
        OrderCommonService service = SERVICE_MAP.get(type);
        if (null == service) {
            throw new EmptyDataException("未注册订单服务对象");
        }
        return service;
    }
}
