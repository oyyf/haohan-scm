/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.opc.entity.ShipRecordDetail;

import java.util.List;

/**
 * 发货记录明细
 *
 * @author haohan
 * @date 2020-01-06 14:28:22
 */
public interface ShipRecordDetailService extends IService<ShipRecordDetail> {

    List<ShipRecordDetail> fetchListBySn(String shipSn);
}
