/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.opc.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.opc.req.SummaryOrderReq;
import com.haohan.cloud.scm.opc.service.SummaryOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购单汇总
 *
 * @author haohan
 * @date 2019-05-30 10:21:03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/summaryorder" )
@Api(value = "summaryorder", tags = "summaryorder管理")
public class SummaryOrderController {

    private final SummaryOrderService summaryOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param summaryOrder 采购单汇总
     * @return
     */
    @GetMapping("/page" )
    public R getSummaryOrderPage(Page page, SummaryOrder summaryOrder) {
        return new R<>(summaryOrderService.page(page, Wrappers.query(summaryOrder)));
    }


    /**
     * 通过id查询采购单汇总
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(summaryOrderService.getById(id));
    }

    /**
     * 新增采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @SysLog("新增采购单汇总" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_summaryorder_add')" )
    public R save(@RequestBody SummaryOrder summaryOrder) {
        return new R<>(summaryOrderService.save(summaryOrder));
    }

    /**
     * 修改采购单汇总
     * @param summaryOrder 采购单汇总
     * @return R
     */
    @SysLog("修改采购单汇总" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_summaryorder_edit')" )
    public R updateById(@RequestBody SummaryOrder summaryOrder) {
        return new R<>(summaryOrderService.updateById(summaryOrder));
    }

    /**
     * 通过id删除采购单汇总
     * @param id id
     * @return R
     */
    @SysLog("删除采购单汇总" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_summaryorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(summaryOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购单汇总")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_summaryorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(summaryOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购单汇总")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(summaryOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购单汇总总记录}")
    @PostMapping("/countBySummaryOrderReq")
    public R countBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq) {

        return new R<>(summaryOrderService.count(Wrappers.query(summaryOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param summaryOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据summaryOrderReq查询一条货位信息表")
    @PostMapping("/getOneBySummaryOrderReq")
    public R getOneBySummaryOrderReq(@RequestBody SummaryOrderReq summaryOrderReq) {

        return new R<>(summaryOrderService.getOne(Wrappers.query(summaryOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param summaryOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_summaryorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SummaryOrder> summaryOrderList) {

        return new R<>(summaryOrderService.saveOrUpdateBatch(summaryOrderList));
    }


}
