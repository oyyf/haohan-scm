/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;

/**
 * 客户项目售后维护管理
 *
 * @author haohan
 * @date 2019-05-13 20:12:38
 */
public interface CustomerServiceManageService extends IService<CustomerServiceManage> {

}
