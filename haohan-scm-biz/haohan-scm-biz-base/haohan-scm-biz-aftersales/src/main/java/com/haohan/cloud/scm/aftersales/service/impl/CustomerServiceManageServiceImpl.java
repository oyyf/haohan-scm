/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.CustomerServiceManageMapper;
import com.haohan.cloud.scm.aftersales.service.CustomerServiceManageService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;
import org.springframework.stereotype.Service;

/**
 * 客户项目售后维护管理
 *
 * @author haohan
 * @date 2019-05-13 20:12:38
 */
@Service
public class CustomerServiceManageServiceImpl extends ServiceImpl<CustomerServiceManageMapper, CustomerServiceManage> implements CustomerServiceManageService {

}
