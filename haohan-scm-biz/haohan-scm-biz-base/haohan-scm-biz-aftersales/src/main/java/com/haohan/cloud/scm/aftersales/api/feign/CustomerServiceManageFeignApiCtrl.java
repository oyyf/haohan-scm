/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerServiceManageService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;
import com.haohan.cloud.scm.api.aftersales.req.CustomerServiceManageReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 客户项目售后维护管理
 *
 * @author haohan
 * @date 2019-05-30 10:28:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/CustomerServiceManage")
@Api(value = "customerservicemanage", tags = "customerservicemanage内部接口服务")
public class CustomerServiceManageFeignApiCtrl {

    private final CustomerServiceManageService customerServiceManageService;


    /**
     * 通过id查询客户项目售后维护管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(customerServiceManageService.getById(id));
    }


    /**
     * 分页查询 客户项目售后维护管理 列表信息
     * @param customerServiceManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCustomerServiceManagePage")
    public R getCustomerServiceManagePage(@RequestBody CustomerServiceManageReq customerServiceManageReq) {
        Page page = new Page(customerServiceManageReq.getPageNo(), customerServiceManageReq.getPageSize());
        CustomerServiceManage customerServiceManage =new CustomerServiceManage();
        BeanUtil.copyProperties(customerServiceManageReq, customerServiceManage);

        return new R<>(customerServiceManageService.page(page, Wrappers.query(customerServiceManage)));
    }


    /**
     * 全量查询 客户项目售后维护管理 列表信息
     * @param customerServiceManageReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCustomerServiceManageList")
    public R getCustomerServiceManageList(@RequestBody CustomerServiceManageReq customerServiceManageReq) {
        CustomerServiceManage customerServiceManage =new CustomerServiceManage();
        BeanUtil.copyProperties(customerServiceManageReq, customerServiceManage);

        return new R<>(customerServiceManageService.list(Wrappers.query(customerServiceManage)));
    }


    /**
     * 新增客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @Inner
    @SysLog("新增客户项目售后维护管理")
    @PostMapping("/add")
    public R save(@RequestBody CustomerServiceManage customerServiceManage) {
        return new R<>(customerServiceManageService.save(customerServiceManage));
    }

    /**
     * 修改客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @Inner
    @SysLog("修改客户项目售后维护管理")
    @PostMapping("/update")
    public R updateById(@RequestBody CustomerServiceManage customerServiceManage) {
        return new R<>(customerServiceManageService.updateById(customerServiceManage));
    }

    /**
     * 通过id删除客户项目售后维护管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除客户项目售后维护管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(customerServiceManageService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除客户项目售后维护管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerServiceManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询客户项目售后维护管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerServiceManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询客户项目售后维护管理总记录}")
    @PostMapping("/countByCustomerServiceManageReq")
    public R countByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq) {

        return new R<>(customerServiceManageService.count(Wrappers.query(customerServiceManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据customerServiceManageReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerServiceManageReq")
    public R getOneByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq) {

        return new R<>(customerServiceManageService.getOne(Wrappers.query(customerServiceManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerServiceManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<CustomerServiceManage> customerServiceManageList) {

        return new R<>(customerServiceManageService.saveOrUpdateBatch(customerServiceManageList));
    }

}
