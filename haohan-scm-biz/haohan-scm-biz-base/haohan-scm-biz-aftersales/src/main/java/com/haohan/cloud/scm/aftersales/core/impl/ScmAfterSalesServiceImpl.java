package com.haohan.cloud.scm.aftersales.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.aftersales.core.ScmAfterSalesService;
import com.haohan.cloud.scm.aftersales.service.AfterSalesService;
import com.haohan.cloud.scm.aftersales.utils.ScmAfterSalesUtils;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.haohan.cloud.scm.api.aftersales.resp.QueryAfterSalesDetailResp;
import com.haohan.cloud.scm.api.constant.enums.aftersales.AfterOrderTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author xwx
 * @date 2019/7/23
 */
@Service
@AllArgsConstructor
public class ScmAfterSalesServiceImpl implements ScmAfterSalesService {
    private final AfterSalesService afterSalesService;
    private final ScmAfterSalesUtils scmAfterSalesUtils;
    /**
     * 查询售后单详情(待商品信息)
     * @param req
     * @return
     */
    @Override
    public QueryAfterSalesDetailResp queryAfterSalesDetail(QueryAfterSalesDetailReq req) {
        QueryAfterSalesDetailResp resp = new QueryAfterSalesDetailResp();
        //查询售后单
        AfterSales afterSales = new AfterSales();
        afterSales.setPmId(req.getPmId());
        afterSales.setId(req.getId());
        AfterSales one = afterSalesService.getOne(Wrappers.query(afterSales));
        BeanUtil.copyProperties(one,resp);
        if (null==one){
            throw new EmptyDataException("没有这个售后单");
        }
        if (one.getAfterOrderType()== AfterOrderTypeEnum.productorder){
            //售后单类型为供应商报价  查询报价单
            OfferOrderReq offerOrderReq = new OfferOrderReq();
            offerOrderReq.setPmId(req.getPmId());
            offerOrderReq.setOfferOrderId(one.getOfferOrderId());
            OfferOrder offerOrder = scmAfterSalesUtils.fetchOfferOrder(offerOrderReq);
            resp.setGoodsImg(offerOrder.getGoodsImg());
            resp.setGoodsName(offerOrder.getGoodsName());
            resp.setSupplierName(offerOrder.getSupplierName());
            resp.setBuyNum(offerOrder.getBuyNum());
            resp.setDealPrice(offerOrder.getDealPrice());
            resp.setUnit(offerOrder.getUnit());
        }else if (one.getAfterOrderType()== AfterOrderTypeEnum.salebpurchaseorder){
            //售后单类型为B客户采购单  查询采购单
        }else {
            //售后单类型为C客户零售单  查询C客户零售单
        }
        return resp;
    }
}
