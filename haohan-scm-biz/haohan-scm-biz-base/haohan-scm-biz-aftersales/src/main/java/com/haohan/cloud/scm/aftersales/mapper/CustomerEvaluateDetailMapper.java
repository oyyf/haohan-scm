/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;

/**
 * 客户服务评分明细
 *
 * @author haohan
 * @date 2019-05-13 20:27:10
 */
public interface CustomerEvaluateDetailMapper extends BaseMapper<CustomerEvaluateDetail> {

}
