/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerProjectManageService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerProjectManage;
import com.haohan.cloud.scm.api.aftersales.req.CustomerProjectManageReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 客户项目管理
 *
 * @author haohan
 * @date 2019-05-30 10:25:38
 */
@RestController
@AllArgsConstructor
@RequestMapping("/customerprojectmanage" )
@Api(value = "customerprojectmanage", tags = "customerprojectmanage管理")
public class CustomerProjectManageController {

    private final CustomerProjectManageService customerProjectManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param customerProjectManage 客户项目管理
     * @return
     */
    @GetMapping("/page" )
    public R getCustomerProjectManagePage(Page page, CustomerProjectManage customerProjectManage) {
        return new R<>(customerProjectManageService.page(page, Wrappers.query(customerProjectManage)));
    }


    /**
     * 通过id查询客户项目管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(customerProjectManageService.getById(id));
    }

    /**
     * 新增客户项目管理
     * @param customerProjectManage 客户项目管理
     * @return R
     */
    @SysLog("新增客户项目管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_customerprojectmanage_add')" )
    public R save(@RequestBody CustomerProjectManage customerProjectManage) {
        return new R<>(customerProjectManageService.save(customerProjectManage));
    }

    /**
     * 修改客户项目管理
     * @param customerProjectManage 客户项目管理
     * @return R
     */
    @SysLog("修改客户项目管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_customerprojectmanage_edit')" )
    public R updateById(@RequestBody CustomerProjectManage customerProjectManage) {
        return new R<>(customerProjectManageService.updateById(customerProjectManage));
    }

    /**
     * 通过id删除客户项目管理
     * @param id id
     * @return R
     */
    @SysLog("删除客户项目管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_customerprojectmanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(customerProjectManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除客户项目管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_customerprojectmanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerProjectManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询客户项目管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerProjectManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerProjectManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询客户项目管理总记录}")
    @PostMapping("/countByCustomerProjectManageReq")
    public R countByCustomerProjectManageReq(@RequestBody CustomerProjectManageReq customerProjectManageReq) {

        return new R<>(customerProjectManageService.count(Wrappers.query(customerProjectManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerProjectManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据customerProjectManageReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerProjectManageReq")
    public R getOneByCustomerProjectManageReq(@RequestBody CustomerProjectManageReq customerProjectManageReq) {

        return new R<>(customerProjectManageService.getOne(Wrappers.query(customerProjectManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerProjectManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_customerprojectmanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CustomerProjectManage> customerProjectManageList) {

        return new R<>(customerProjectManageService.saveOrUpdateBatch(customerProjectManageList));
    }


}
