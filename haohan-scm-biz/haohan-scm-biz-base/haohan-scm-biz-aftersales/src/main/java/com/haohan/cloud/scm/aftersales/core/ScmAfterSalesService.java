package com.haohan.cloud.scm.aftersales.core;

import com.haohan.cloud.scm.api.aftersales.req.QueryAfterSalesDetailReq;
import com.haohan.cloud.scm.api.aftersales.resp.QueryAfterSalesDetailResp;

/**
 * @author xwx
 * @date 2019/7/23
 */
public interface ScmAfterSalesService {

    /**
     * 查询售后单详情(待商品信息)
     * @param req
     * @return
     */
    QueryAfterSalesDetailResp queryAfterSalesDetail(QueryAfterSalesDetailReq req);
}
