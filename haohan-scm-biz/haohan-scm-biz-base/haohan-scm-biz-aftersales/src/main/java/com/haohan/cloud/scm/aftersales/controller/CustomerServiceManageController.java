/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerServiceManageService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerServiceManage;
import com.haohan.cloud.scm.api.aftersales.req.CustomerServiceManageReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 客户项目售后维护管理
 *
 * @author haohan
 * @date 2019-05-30 10:28:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/customerservicemanage" )
@Api(value = "customerservicemanage", tags = "customerservicemanage管理")
public class CustomerServiceManageController {

    private final CustomerServiceManageService customerServiceManageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param customerServiceManage 客户项目售后维护管理
     * @return
     */
    @GetMapping("/page" )
    public R getCustomerServiceManagePage(Page page, CustomerServiceManage customerServiceManage) {
        return new R<>(customerServiceManageService.page(page, Wrappers.query(customerServiceManage)));
    }


    /**
     * 通过id查询客户项目售后维护管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(customerServiceManageService.getById(id));
    }

    /**
     * 新增客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @SysLog("新增客户项目售后维护管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_customerservicemanage_add')" )
    public R save(@RequestBody CustomerServiceManage customerServiceManage) {
        return new R<>(customerServiceManageService.save(customerServiceManage));
    }

    /**
     * 修改客户项目售后维护管理
     * @param customerServiceManage 客户项目售后维护管理
     * @return R
     */
    @SysLog("修改客户项目售后维护管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_customerservicemanage_edit')" )
    public R updateById(@RequestBody CustomerServiceManage customerServiceManage) {
        return new R<>(customerServiceManageService.updateById(customerServiceManage));
    }

    /**
     * 通过id删除客户项目售后维护管理
     * @param id id
     * @return R
     */
    @SysLog("删除客户项目售后维护管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_customerservicemanage_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(customerServiceManageService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除客户项目售后维护管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_customerservicemanage_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerServiceManageService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询客户项目售后维护管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerServiceManageService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询客户项目售后维护管理总记录}")
    @PostMapping("/countByCustomerServiceManageReq")
    public R countByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq) {

        return new R<>(customerServiceManageService.count(Wrappers.query(customerServiceManageReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerServiceManageReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据customerServiceManageReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerServiceManageReq")
    public R getOneByCustomerServiceManageReq(@RequestBody CustomerServiceManageReq customerServiceManageReq) {

        return new R<>(customerServiceManageService.getOne(Wrappers.query(customerServiceManageReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerServiceManageList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_customerservicemanage_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CustomerServiceManage> customerServiceManageList) {

        return new R<>(customerServiceManageService.saveOrUpdateBatch(customerServiceManageList));
    }


}
