/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.CustomerProjectManageMapper;
import com.haohan.cloud.scm.aftersales.service.CustomerProjectManageService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerProjectManage;
import org.springframework.stereotype.Service;

/**
 * 客户项目管理
 *
 * @author haohan
 * @date 2019-05-13 20:13:33
 */
@Service
public class CustomerProjectManageServiceImpl extends ServiceImpl<CustomerProjectManageMapper, CustomerProjectManage> implements CustomerProjectManageService {

}
