/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.AfterSalesRecordService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;
import com.haohan.cloud.scm.api.aftersales.req.AfterSalesRecordReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 售后记录
 *
 * @author haohan
 * @date 2019-05-30 10:24:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/AfterSalesRecord")
@Api(value = "aftersalesrecord", tags = "aftersalesrecord内部接口服务")
public class AfterSalesRecordFeignApiCtrl {

    private final AfterSalesRecordService afterSalesRecordService;


    /**
     * 通过id查询售后记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(afterSalesRecordService.getById(id));
    }


    /**
     * 分页查询 售后记录 列表信息
     * @param afterSalesRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAfterSalesRecordPage")
    public R getAfterSalesRecordPage(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {
        Page page = new Page(afterSalesRecordReq.getPageNo(), afterSalesRecordReq.getPageSize());
        AfterSalesRecord afterSalesRecord =new AfterSalesRecord();
        BeanUtil.copyProperties(afterSalesRecordReq, afterSalesRecord);

        return new R<>(afterSalesRecordService.page(page, Wrappers.query(afterSalesRecord)));
    }


    /**
     * 全量查询 售后记录 列表信息
     * @param afterSalesRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchAfterSalesRecordList")
    public R getAfterSalesRecordList(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {
        AfterSalesRecord afterSalesRecord =new AfterSalesRecord();
        BeanUtil.copyProperties(afterSalesRecordReq, afterSalesRecord);

        return new R<>(afterSalesRecordService.list(Wrappers.query(afterSalesRecord)));
    }


    /**
     * 新增售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @Inner
    @SysLog("新增售后记录")
    @PostMapping("/add")
    public R save(@RequestBody AfterSalesRecord afterSalesRecord) {
        return new R<>(afterSalesRecordService.save(afterSalesRecord));
    }

    /**
     * 修改售后记录
     * @param afterSalesRecord 售后记录
     * @return R
     */
    @Inner
    @SysLog("修改售后记录")
    @PostMapping("/update")
    public R updateById(@RequestBody AfterSalesRecord afterSalesRecord) {
        return new R<>(afterSalesRecordService.updateById(afterSalesRecord));
    }

    /**
     * 通过id删除售后记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除售后记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(afterSalesRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除售后记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询售后记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(afterSalesRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询售后记录总记录}")
    @PostMapping("/countByAfterSalesRecordReq")
    public R countByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {

        return new R<>(afterSalesRecordService.count(Wrappers.query(afterSalesRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param afterSalesRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据afterSalesRecordReq查询一条货位信息表")
    @PostMapping("/getOneByAfterSalesRecordReq")
    public R getOneByAfterSalesRecordReq(@RequestBody AfterSalesRecordReq afterSalesRecordReq) {

        return new R<>(afterSalesRecordService.getOne(Wrappers.query(afterSalesRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param afterSalesRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<AfterSalesRecord> afterSalesRecordList) {

        return new R<>(afterSalesRecordService.saveOrUpdateBatch(afterSalesRecordList));
    }

}
