/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.core.ScmReturnOrderService;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderReq;
import com.haohan.cloud.scm.api.aftersales.resp.QueryReturnOrderDetailResp;
import com.haohan.cloud.scm.api.aftersales.trans.AfterSalesTrans;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.aftersales.ReturnStatusEnum;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ReturnOrder")
@Api(value = "returnorder", tags = "returnorder-退货单内部接口服务}")
public class ReturnOrderFeignApiCtrl {

    private final ReturnOrderService returnOrderService;
    private final ScmReturnOrderService scmReturnOrderService;


    /**
     * 通过id查询退货单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询退货单")
    public R getById(@PathVariable("id") String id) {
        return new R<>(returnOrderService.getById(id));
    }


    /**
     * 分页查询 退货单 列表信息
     * @param returnOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchReturnOrderPage")
    @ApiOperation(value = "分页查询 退货单 列表信息")
    public R getReturnOrderPage(@RequestBody ReturnOrderReq returnOrderReq) {
        Page page = new Page(returnOrderReq.getPageNo(), returnOrderReq.getPageSize());
        ReturnOrder returnOrder =new ReturnOrder();
        BeanUtil.copyProperties(returnOrderReq, returnOrder);

        return new R<>(returnOrderService.page(page, Wrappers.query(returnOrder)));
    }


    /**
     * 全量查询 退货单 列表信息
     * @param returnOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchReturnOrderList")
    @ApiOperation(value = "全量查询 退货单 列表信息")
    public R getReturnOrderList(@RequestBody ReturnOrderReq returnOrderReq) {
        ReturnOrder returnOrder =new ReturnOrder();
        BeanUtil.copyProperties(returnOrderReq, returnOrder);

        return new R<>(returnOrderService.list(Wrappers.query(returnOrder)));
    }


    /**
     * 新增退货单
     * @param returnOrder 退货单
     * @return R
     */
    @Inner
    @SysLog("新增退货单")
    @PostMapping("/add")
    @ApiOperation(value = "新增退货单")
    public R save(@RequestBody ReturnOrder returnOrder) {
        return new R<>(returnOrderService.save(returnOrder));
    }

    /**
     * 修改退货单
     * @param returnOrder 退货单
     * @return R
     */
    @Inner
    @SysLog("修改退货单")
    @PostMapping("/update")
    @ApiOperation(value = "修改退货单")
    public R updateById(@RequestBody ReturnOrder returnOrder) {
        return new R<>(returnOrderService.updateById(returnOrder));
    }

    /**
     * 通过id删除退货单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除退货单")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除退货单")
    public R removeById(@PathVariable String id) {
        return new R<>(returnOrderService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除退货单")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除退货单")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询退货单")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询退货单")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询退货单总记录")
    @PostMapping("/countByReturnOrderReq")
    @ApiOperation(value = "查询退货单总记录")
    public R countByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq) {

        return new R<>(returnOrderService.count(Wrappers.query(returnOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param returnOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据returnOrderReq查询一条退货单信息")
    @PostMapping("/getOneByReturnOrderReq")
    @ApiOperation(value = "根据returnOrderReq查询一条退货单信息")
    public R getOneByReturnOrderReq(@RequestBody ReturnOrderReq returnOrderReq) {

        return new R<>(returnOrderService.getOne(Wrappers.query(returnOrderReq), false));
    }


    /**
     * 批量修改OR插入退货单
     * @param returnOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入退货单数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入退货单数据")
    public R saveOrUpdateBatch(@RequestBody List<ReturnOrder> returnOrderList) {

        return new R<>(returnOrderService.saveOrUpdateBatch(returnOrderList));
    }

    /**
     * 查询 可退款的退货单  根据pmId/ returnSn
     * @param req pmId/ returnSn
     * @return R
     */
    @Inner
    @PostMapping("/fetchRefund")
    @ApiOperation(value = "批量修改OR插入退货单数据")
    public R<ReturnOrder> fetchRefund(@RequestBody ReturnOrderReq req) {
        QueryWrapper<ReturnOrder> queryWrapper = new QueryWrapper<>();
        // 状态不为  1:待审核 2：审核不通过
        queryWrapper.lambda()
                .eq(ReturnOrder::getPmId, req.getPmId())
                .ne(ReturnOrder::getReturnStatus, ReturnStatusEnum.wait)
                .ne(ReturnOrder::getReturnStatus, ReturnStatusEnum.failed)
                .eq(ReturnOrder::getReturnSn, req.getReturnSn());
        return new R<>(returnOrderService.getOne(queryWrapper));
    }


    @Inner
    @GetMapping("/orderInfo")
    @ApiOperation(value = "通过sn查询退货单详情")
    public R<OrderInfoDTO> fetchOrderIfo(@RequestParam("orderSn") String orderSn) {
        ReturnOrder query = new ReturnOrder();
        query.setReturnSn(orderSn);
        QueryReturnOrderDetailResp order = scmReturnOrderService.queryReturnOrderDetail(query);
        return R.ok(AfterSalesTrans.transToOrderInfo(order));
    }

}
