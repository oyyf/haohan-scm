/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.SalesReturnService;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;
import com.haohan.cloud.scm.api.aftersales.req.SalesReturnReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 售后退货记录
 *
 * @author haohan
 * @date 2019-05-30 10:28:49
 */
@RestController
@AllArgsConstructor
@RequestMapping("/salesreturn" )
@Api(value = "salesreturn", tags = "salesreturn管理")
public class SalesReturnController {

    private final SalesReturnService salesReturnService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param salesReturn 售后退货记录
     * @return
     */
    @GetMapping("/page" )
    public R getSalesReturnPage(Page page, SalesReturn salesReturn) {
        return new R<>(salesReturnService.page(page, Wrappers.query(salesReturn)));
    }


    /**
     * 通过id查询售后退货记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(salesReturnService.getById(id));
    }

    /**
     * 新增售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @SysLog("新增售后退货记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_salesreturn_add')" )
    public R save(@RequestBody SalesReturn salesReturn) {
        return new R<>(salesReturnService.save(salesReturn));
    }

    /**
     * 修改售后退货记录
     * @param salesReturn 售后退货记录
     * @return R
     */
    @SysLog("修改售后退货记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_salesreturn_edit')" )
    public R updateById(@RequestBody SalesReturn salesReturn) {
        return new R<>(salesReturnService.updateById(salesReturn));
    }

    /**
     * 通过id删除售后退货记录
     * @param id id
     * @return R
     */
    @SysLog("删除售后退货记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_salesreturn_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(salesReturnService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除售后退货记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_salesreturn_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(salesReturnService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询售后退货记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(salesReturnService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询售后退货记录总记录}")
    @PostMapping("/countBySalesReturnReq")
    public R countBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq) {

        return new R<>(salesReturnService.count(Wrappers.query(salesReturnReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param salesReturnReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据salesReturnReq查询一条货位信息表")
    @PostMapping("/getOneBySalesReturnReq")
    public R getOneBySalesReturnReq(@RequestBody SalesReturnReq salesReturnReq) {

        return new R<>(salesReturnService.getOne(Wrappers.query(salesReturnReq), false));
    }


    /**
     * 批量修改OR插入
     * @param salesReturnList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_salesreturn_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<SalesReturn> salesReturnList) {

        return new R<>(salesReturnService.saveOrUpdateBatch(salesReturnList));
    }


}
