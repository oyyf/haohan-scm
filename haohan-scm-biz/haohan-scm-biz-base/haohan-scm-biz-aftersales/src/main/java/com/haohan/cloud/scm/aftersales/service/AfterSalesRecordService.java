/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;

/**
 * 售后记录
 *
 * @author haohan
 * @date 2019-05-13 20:29:08
 */
public interface AfterSalesRecordService extends IService<AfterSalesRecord> {

}
