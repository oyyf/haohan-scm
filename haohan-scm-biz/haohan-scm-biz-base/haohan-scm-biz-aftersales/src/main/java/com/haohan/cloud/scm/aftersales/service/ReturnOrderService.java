/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;

/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
public interface ReturnOrderService extends IService<ReturnOrder> {

}
