/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderDetailService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;
import com.haohan.cloud.scm.api.aftersales.req.ReturnOrderDetailReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 退货单明细
 *
 * @author haohan
 * @date 2019-07-30 15:55:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ReturnOrderDetail")
@Api(value = "returnorderdetail", tags = "returnorderdetail-退货单明细内部接口服务}")
public class ReturnOrderDetailFeignApiCtrl {

    private final ReturnOrderDetailService returnOrderDetailService;


    /**
     * 通过id查询退货单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询退货单明细")
    public R getById(@PathVariable("id") String id) {
        return new R<>(returnOrderDetailService.getById(id));
    }


    /**
     * 分页查询 退货单明细 列表信息
     * @param returnOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchReturnOrderDetailPage")
    @ApiOperation(value = "分页查询 退货单明细 列表信息")
    public R getReturnOrderDetailPage(@RequestBody ReturnOrderDetailReq returnOrderDetailReq) {
        Page page = new Page(returnOrderDetailReq.getPageNo(), returnOrderDetailReq.getPageSize());
        ReturnOrderDetail returnOrderDetail =new ReturnOrderDetail();
        BeanUtil.copyProperties(returnOrderDetailReq, returnOrderDetail);

        return new R<>(returnOrderDetailService.page(page, Wrappers.query(returnOrderDetail)));
    }


    /**
     * 全量查询 退货单明细 列表信息
     * @param returnOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchReturnOrderDetailList")
    @ApiOperation(value = "全量查询 退货单明细 列表信息")
    public R<List<ReturnOrderDetail>> getReturnOrderDetailList(@RequestBody ReturnOrderDetailReq returnOrderDetailReq) {
        return R.ok(returnOrderDetailService.list(Wrappers.query(returnOrderDetailReq)));
    }


    /**
     * 新增退货单明细
     * @param returnOrderDetail 退货单明细
     * @return R
     */
    @Inner
    @SysLog("新增退货单明细")
    @PostMapping("/add")
    @ApiOperation(value = "新增退货单明细")
    public R save(@RequestBody ReturnOrderDetail returnOrderDetail) {
        return new R<>(returnOrderDetailService.save(returnOrderDetail));
    }

    /**
     * 修改退货单明细
     * @param returnOrderDetail 退货单明细
     * @return R
     */
    @Inner
    @SysLog("修改退货单明细")
    @PostMapping("/update")
    @ApiOperation(value = "修改退货单明细")
    public R updateById(@RequestBody ReturnOrderDetail returnOrderDetail) {
        return new R<>(returnOrderDetailService.updateById(returnOrderDetail));
    }

    /**
     * 通过id删除退货单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除退货单明细")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除退货单明细")
    public R removeById(@PathVariable String id) {
        return new R<>(returnOrderDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除退货单明细")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除退货单明细")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询退货单明细")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询退货单明细")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(returnOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param returnOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询退货单明细总记录")
    @PostMapping("/countByReturnOrderDetailReq")
    @ApiOperation(value = "查询退货单明细总记录")
    public R countByReturnOrderDetailReq(@RequestBody ReturnOrderDetailReq returnOrderDetailReq) {

        return new R<>(returnOrderDetailService.count(Wrappers.query(returnOrderDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param returnOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据returnOrderDetailReq查询一条退货单明细信息")
    @PostMapping("/getOneByReturnOrderDetailReq")
    @ApiOperation(value = "根据returnOrderDetailReq查询一条退货单明细信息")
    public R getOneByReturnOrderDetailReq(@RequestBody ReturnOrderDetailReq returnOrderDetailReq) {

        return new R<>(returnOrderDetailService.getOne(Wrappers.query(returnOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入退货单明细
     * @param returnOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入退货单明细数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入退货单明细数据")
    public R saveOrUpdateBatch(@RequestBody List<ReturnOrderDetail> returnOrderDetailList) {

        return new R<>(returnOrderDetailService.saveOrUpdateBatch(returnOrderDetailList));
    }

}
