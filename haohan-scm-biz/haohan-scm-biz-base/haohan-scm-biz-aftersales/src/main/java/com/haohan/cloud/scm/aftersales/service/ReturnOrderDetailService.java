/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrderDetail;

/**
 * 退货单明细
 *
 * @author haohan
 * @date 2019-07-30 15:55:55
 */
public interface ReturnOrderDetailService extends IService<ReturnOrderDetail> {

}
