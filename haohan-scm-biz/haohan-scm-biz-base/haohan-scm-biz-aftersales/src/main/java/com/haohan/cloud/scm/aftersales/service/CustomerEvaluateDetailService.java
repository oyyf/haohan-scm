/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;

/**
 * 客户服务评分明细
 *
 * @author haohan
 * @date 2019-05-13 20:27:10
 */
public interface CustomerEvaluateDetailService extends IService<CustomerEvaluateDetail> {

}
