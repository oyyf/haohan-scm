/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerRevaluateService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerRevaluate;
import com.haohan.cloud.scm.api.aftersales.req.CustomerRevaluateReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 客户服务评分
 *
 * @author haohan
 * @date 2019-05-30 10:27:30
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/CustomerRevaluate")
@Api(value = "customerrevaluate", tags = "customerrevaluate内部接口服务")
public class CustomerRevaluateFeignApiCtrl {

    private final CustomerRevaluateService customerRevaluateService;


    /**
     * 通过id查询客户服务评分
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(customerRevaluateService.getById(id));
    }


    /**
     * 分页查询 客户服务评分 列表信息
     * @param customerRevaluateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCustomerRevaluatePage")
    public R getCustomerRevaluatePage(@RequestBody CustomerRevaluateReq customerRevaluateReq) {
        Page page = new Page(customerRevaluateReq.getPageNo(), customerRevaluateReq.getPageSize());
        CustomerRevaluate customerRevaluate =new CustomerRevaluate();
        BeanUtil.copyProperties(customerRevaluateReq, customerRevaluate);

        return new R<>(customerRevaluateService.page(page, Wrappers.query(customerRevaluate)));
    }


    /**
     * 全量查询 客户服务评分 列表信息
     * @param customerRevaluateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchCustomerRevaluateList")
    public R getCustomerRevaluateList(@RequestBody CustomerRevaluateReq customerRevaluateReq) {
        CustomerRevaluate customerRevaluate =new CustomerRevaluate();
        BeanUtil.copyProperties(customerRevaluateReq, customerRevaluate);

        return new R<>(customerRevaluateService.list(Wrappers.query(customerRevaluate)));
    }


    /**
     * 新增客户服务评分
     * @param customerRevaluate 客户服务评分
     * @return R
     */
    @Inner
    @SysLog("新增客户服务评分")
    @PostMapping("/add")
    public R save(@RequestBody CustomerRevaluate customerRevaluate) {
        return new R<>(customerRevaluateService.save(customerRevaluate));
    }

    /**
     * 修改客户服务评分
     * @param customerRevaluate 客户服务评分
     * @return R
     */
    @Inner
    @SysLog("修改客户服务评分")
    @PostMapping("/update")
    public R updateById(@RequestBody CustomerRevaluate customerRevaluate) {
        return new R<>(customerRevaluateService.updateById(customerRevaluate));
    }

    /**
     * 通过id删除客户服务评分
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除客户服务评分")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(customerRevaluateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除客户服务评分")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerRevaluateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询客户服务评分")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerRevaluateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerRevaluateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询客户服务评分总记录}")
    @PostMapping("/countByCustomerRevaluateReq")
    public R countByCustomerRevaluateReq(@RequestBody CustomerRevaluateReq customerRevaluateReq) {

        return new R<>(customerRevaluateService.count(Wrappers.query(customerRevaluateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerRevaluateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据customerRevaluateReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerRevaluateReq")
    public R getOneByCustomerRevaluateReq(@RequestBody CustomerRevaluateReq customerRevaluateReq) {

        return new R<>(customerRevaluateService.getOne(Wrappers.query(customerRevaluateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerRevaluateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<CustomerRevaluate> customerRevaluateList) {

        return new R<>(customerRevaluateService.saveOrUpdateBatch(customerRevaluateList));
    }

}
