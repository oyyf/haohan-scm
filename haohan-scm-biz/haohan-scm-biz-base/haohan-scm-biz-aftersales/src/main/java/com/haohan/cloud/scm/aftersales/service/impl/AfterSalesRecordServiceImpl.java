/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.AfterSalesRecordMapper;
import com.haohan.cloud.scm.aftersales.service.AfterSalesRecordService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSalesRecord;
import org.springframework.stereotype.Service;

/**
 * 售后记录
 *
 * @author haohan
 * @date 2019-05-13 20:29:08
 */
@Service
public class AfterSalesRecordServiceImpl extends ServiceImpl<AfterSalesRecordMapper, AfterSalesRecord> implements AfterSalesRecordService {

}
