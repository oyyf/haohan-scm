package com.haohan.cloud.scm.aftersales.core;

import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.aftersales.entity.SalesReturn;

/**
 * @author dy
 * @date 2019/5/29
 */
public interface AfterSalesCoreService {

    // 新增售后单  采购员发起售后(只添加售后单)
    AfterSales addAfterSalesByPurchase(AfterSales afterSales);

    // 新增售后单 客户发起售后(添加售后单及售后记录)
    AfterSales addAfterSalesByCustomer(AfterSales afterSales);

    // 新增退货记录 发起退货
    SalesReturn addSalesReturn(SalesReturn salesReturn);

}
