/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.ReturnOrderMapper;
import com.haohan.cloud.scm.aftersales.service.ReturnOrderService;
import com.haohan.cloud.scm.api.aftersales.entity.ReturnOrder;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 退货单
 *
 * @author haohan
 * @date 2019-07-30 16:09:37
 */
@Service
@AllArgsConstructor
public class ReturnOrderServiceImpl extends ServiceImpl<ReturnOrderMapper, ReturnOrder> implements ReturnOrderService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ReturnOrder entity) {
        if (StrUtil.isEmpty(entity.getReturnSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ReturnOrder.class, NumberPrefixConstant.RETURN_ORDER_SN_PRE);
            entity.setReturnSn(sn);
        }
        return super.save(entity);
    }
}
