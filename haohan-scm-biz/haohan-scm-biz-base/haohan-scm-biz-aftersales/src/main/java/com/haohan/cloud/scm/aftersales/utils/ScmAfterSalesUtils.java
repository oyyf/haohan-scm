package com.haohan.cloud.scm.aftersales.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.bill.entity.PayableBill;
import com.haohan.cloud.scm.api.bill.entity.ReceivableBill;
import com.haohan.cloud.scm.api.bill.feign.PayableBillFeignService;
import com.haohan.cloud.scm.api.bill.feign.ReceivableBillFeignService;
import com.haohan.cloud.scm.api.manage.dto.PhotoGroupDTO;
import com.haohan.cloud.scm.api.manage.entity.PhotoGallery;
import com.haohan.cloud.scm.api.manage.entity.PhotoGroupManage;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.manage.feign.PhotoGroupManageFeignService;
import com.haohan.cloud.scm.api.manage.feign.UPassportFeignService;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.feign.OfferOrderFeignService;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.api.wms.entity.WarehouseStock;
import com.haohan.cloud.scm.api.wms.feign.WarehouseStockFeignService;
import com.haohan.cloud.scm.api.wms.req.WarehouseStockReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xwx
 * @date 2019/7/22
 */
@Component
@AllArgsConstructor
public class ScmAfterSalesUtils {
    private final UPassportFeignService uPassportFeignService;
    private final OfferOrderFeignService offerOrderFeignService;
    private final WarehouseStockFeignService warehouseStockFeignService;
    private final PhotoGroupManageFeignService photoGroupManageFeignService;
    private final ReceivableBillFeignService receivableBillFeignService;
    private final PayableBillFeignService payableBillFeignService;

    /**
     * 根据id 查询 uPassport
     * @param id
     * @return
     */
    public UPassport queryUPassportById(String id) {
        R<UPassport> byId = uPassportFeignService.getById(id, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(byId) || byId.getData() == null){
            throw new EmptyDataException();
        }
        UPassport data = byId.getData();
        return data;
    }

    /**
     * 查询报价单
     * @param req
     * @return
     */
    public OfferOrder fetchOfferOrder(OfferOrderReq req) {
        R r = offerOrderFeignService.getOneByOfferOrderReq(req, SecurityConstants.FROM_IN);
        if(!RUtil.isSuccess(r) || null==r.getData()){
            throw new EmptyDataException("查询报价单出错");
        }
        return BeanUtil.toBean(r.getData(),OfferOrder.class);
    }

    /**
     * 保存库存商品
     * @param stock
     * @return
     */
    public Boolean saveWarehouseStock(WarehouseStock stock){
        WarehouseStockReq query = new WarehouseStockReq();
        query.setWarehouseId(stock.getWarehouseId());
        query.setGoodsCode(stock.getGoodsCode());
        R<WarehouseStock> res = warehouseStockFeignService.getOneBywarehouseStockReq(query, SecurityConstants.FROM_IN);
        if(res.getData() == null){
            R<Boolean> save = warehouseStockFeignService.save(stock, SecurityConstants.FROM_IN);
            if(!RUtil.isSuccess(save)){
                throw new ErrorDataException();
            }
        }else{
            WarehouseStock data = res.getData();
            data.setStockNum(data.getStockNum().add(stock.getStockNum()));
            data.setAmount(data.getStockNum().multiply(data.getPrice()));
            if(!warehouseStockFeignService.updateById(data,SecurityConstants.FROM_IN).getData()){
                throw new EmptyDataException();
            }
        }

        return true;
    }

    /**
     * 保存或修改图片组图片
     *
     * @param photoGroupDTO
     * @return
     */
    public String savePhotoGroup(PhotoGroupDTO photoGroupDTO) {
        R<PhotoGroupManage> r = photoGroupManageFeignService.savePhotoGroup(photoGroupDTO, SecurityConstants.FROM_IN);
        if (!RUtil.isSuccess(r)) {
            throw new ErrorDataException("保存或修改图片组图片失败");
        } else if (null == r.getData()) {
            return null;
        }
        return BeanUtil.toBean(r.getData(), PhotoGroupManage.class).getGroupNum();
    }

    /**
     * 查询图片组图片
     *
     * @param groupNum
     * @return
     */
    public List<PhotoGallery> fetchPhotoGroup(String groupNum) {
        List<PhotoGallery> photoList = new ArrayList<>();
        if (StrUtil.isNotBlank(groupNum)) {
            R<PhotoGroupDTO> r = photoGroupManageFeignService.fetchByGroupNum(groupNum, SecurityConstants.FROM_IN);
            if (!RUtil.isSuccess(r)) {
                throw new ErrorDataException("查询图片组图片失败");
            } else if (null == r.getData()) {
                return photoList;
            }
            photoList = BeanUtil.toBean(r.getData(), PhotoGroupDTO.class).getPhotoList();
        }
        return photoList;
    }

    public ReceivableBill fetchReceivableBill(String billSn) {
        R<ReceivableBill> r = receivableBillFeignService.fetchOneBySn(billSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || r.getData() == null) {
            throw new ErrorDataException("查询应收账单失败");
        }
        return r.getData();
    }

    public PayableBill fetchPayableBill(String billSn) {
        R<PayableBill> r = payableBillFeignService.fetchOneBySn(billSn, SecurityConstants.FROM_IN);
        if (RUtil.isFailed(r) || r.getData() == null) {
            throw new ErrorDataException("查询应付账单失败");
        }
        return r.getData();
    }

}
