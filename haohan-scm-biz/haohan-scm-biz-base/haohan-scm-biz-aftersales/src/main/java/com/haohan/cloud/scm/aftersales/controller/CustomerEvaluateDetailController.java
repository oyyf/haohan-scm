/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.aftersales.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.aftersales.service.CustomerEvaluateDetailService;
import com.haohan.cloud.scm.api.aftersales.entity.CustomerEvaluateDetail;
import com.haohan.cloud.scm.api.aftersales.req.CustomerEvaluateDetailReq;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 客户服务评分明细
 *
 * @author haohan
 * @date 2019-05-30 10:25:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/customerevaluatedetail" )
@Api(value = "customerevaluatedetail", tags = "customerevaluatedetail管理")
public class CustomerEvaluateDetailController {

    private final CustomerEvaluateDetailService customerEvaluateDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param customerEvaluateDetail 客户服务评分明细
     * @return
     */
    @GetMapping("/page" )
    public R getCustomerEvaluateDetailPage(Page page, CustomerEvaluateDetail customerEvaluateDetail) {
        return new R<>(customerEvaluateDetailService.page(page, Wrappers.query(customerEvaluateDetail)));
    }


    /**
     * 通过id查询客户服务评分明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(customerEvaluateDetailService.getById(id));
    }

    /**
     * 新增客户服务评分明细
     * @param customerEvaluateDetail 客户服务评分明细
     * @return R
     */
    @SysLog("新增客户服务评分明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_customerevaluatedetail_add')" )
    public R save(@RequestBody CustomerEvaluateDetail customerEvaluateDetail) {
        return new R<>(customerEvaluateDetailService.save(customerEvaluateDetail));
    }

    /**
     * 修改客户服务评分明细
     * @param customerEvaluateDetail 客户服务评分明细
     * @return R
     */
    @SysLog("修改客户服务评分明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_customerevaluatedetail_edit')" )
    public R updateById(@RequestBody CustomerEvaluateDetail customerEvaluateDetail) {
        return new R<>(customerEvaluateDetailService.updateById(customerEvaluateDetail));
    }

    /**
     * 通过id删除客户服务评分明细
     * @param id id
     * @return R
     */
    @SysLog("删除客户服务评分明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_customerevaluatedetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(customerEvaluateDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除客户服务评分明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_customerevaluatedetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(customerEvaluateDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询客户服务评分明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(customerEvaluateDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param customerEvaluateDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询客户服务评分明细总记录}")
    @PostMapping("/countByCustomerEvaluateDetailReq")
    public R countByCustomerEvaluateDetailReq(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq) {

        return new R<>(customerEvaluateDetailService.count(Wrappers.query(customerEvaluateDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param customerEvaluateDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据customerEvaluateDetailReq查询一条货位信息表")
    @PostMapping("/getOneByCustomerEvaluateDetailReq")
    public R getOneByCustomerEvaluateDetailReq(@RequestBody CustomerEvaluateDetailReq customerEvaluateDetailReq) {

        return new R<>(customerEvaluateDetailService.getOne(Wrappers.query(customerEvaluateDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param customerEvaluateDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_customerevaluatedetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<CustomerEvaluateDetail> customerEvaluateDetailList) {

        return new R<>(customerEvaluateDetailService.saveOrUpdateBatch(customerEvaluateDetailList));
    }


}
