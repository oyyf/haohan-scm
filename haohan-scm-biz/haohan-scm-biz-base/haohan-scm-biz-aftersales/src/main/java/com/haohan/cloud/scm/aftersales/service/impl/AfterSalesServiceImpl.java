/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.aftersales.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.aftersales.mapper.AfterSalesMapper;
import com.haohan.cloud.scm.aftersales.service.AfterSalesService;
import com.haohan.cloud.scm.api.aftersales.entity.AfterSales;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 售后单
 *
 * @author haohan
 * @date 2019-05-13 20:29:17
 */
@Service
public class AfterSalesServiceImpl extends ServiceImpl<AfterSalesMapper, AfterSales> implements AfterSalesService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(AfterSales entity) {
        if (StrUtil.isEmpty(entity.getAfterSalesSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(AfterSales.class, NumberPrefixConstant.AFTER_SALES_SN_PRE);
            entity.setAfterSalesSn(sn);
        }
        return super.save(entity);
    }

}
