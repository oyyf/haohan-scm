/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.saleb.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.dto.GoodsProfitDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.QuerySummaryOrderDetailReq;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.saleb.mapper.BuyOrderDetailMapper;
import com.haohan.cloud.scm.saleb.service.BuyOrderDetailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-13 20:34:19
 */
@Service
@AllArgsConstructor
public class BuyOrderDetailServiceImpl extends ServiceImpl<BuyOrderDetailMapper, BuyOrderDetail> implements BuyOrderDetailService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(BuyOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getBuyDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(BuyOrderDetail.class, NumberPrefixConstant.BUY_ORDER_DETAIL_SN_PRE);
            entity.setBuyDetailSn(sn);
        }
        entity.setId(null);
        entity.setTenantId(null);
        return retBool(baseMapper.insert(entity));
    }

    /**
     * 汇总商品下单数量
     *
     * @param buyOrderDetailDTO 必需参数:
     *                          pmId/buySeq/goodsModelId/status/
     *                          deliveryTime/summaryFlag
     * @return
     */
    @Override
    public BuyOrderDetailDTO summaryGoodsNum(BuyOrderDetailDTO buyOrderDetailDTO) {
        // 正常情况 只有一个返回对象
        return baseMapper.summaryGoodsNum(buyOrderDetailDTO).stream().findFirst().orElse(null);
    }

    @Override
    public List<BuyOrderDetailDTO> summaryGoodsNumList(BuyOrderDetailDTO buyOrderDetailDTO) {
        return baseMapper.summaryGoodsNum(buyOrderDetailDTO);
    }

    /**
     * 查询采购明细
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param buyOrderDetailDTO
     * @return
     */
    @Override
    public BuyOrderDetailDTO getOneWithOrder(BuyOrderDetailDTO buyOrderDetailDTO) {
        // 正常情况 只有一个返回对象
        return baseMapper.getOneWithOrder(buyOrderDetailDTO).stream().findFirst().orElse(null);
    }

    /**
     * 查询采购明细
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     *
     * @param buyOrderDetailDTO
     * @return
     */
    @Override
    public List<BuyOrderDetailDTO> getListWithOrder(BuyOrderDetailDTO buyOrderDetailDTO) {
        return baseMapper.getOneWithOrder(buyOrderDetailDTO);
    }

    /**
     * 查询商品毛利
     *
     * @param pmId
     * @return
     */
    @Override
    public List<GoodsProfitDTO> queryGoodsProfit(String pmId) {
        return baseMapper.queryGoodsPrifit(pmId);
    }

    /**
     * 查询汇总单对应订单详情
     *
     * @param req
     * @return
     */
    @Override
    public List<BuyOrderDetail> queryWaitSummaryOrderDetail(QuerySummaryOrderDetailReq req) {
        return baseMapper.queryWaitSummaryOrderDetail(req);
    }

    /**
     * 查询商品数量
     *
     * @param buyId
     * @return
     */
    @Override
    public BigDecimal queryPriceSum(String buyId) {
        return baseMapper.queryPriceSum(buyId);
    }

    /**
     * 根据编号查询采购明细
     *
     * @param buyDetailSn
     * @return
     */
    @Override
    public BuyOrderDetail fetchBySn(String buyDetailSn) {
        return baseMapper.selectList(Wrappers.<BuyOrderDetail>query().lambda()
                .eq(BuyOrderDetail::getBuyDetailSn, buyDetailSn)
        ).stream().findFirst().orElse(null);
    }

    @Override
    public List<BuyOrderDetail> findListBySn(String buyOrderSn) {
        return baseMapper.selectList(Wrappers.<BuyOrderDetail>query().lambda()
                .eq(BuyOrderDetail::getBuyId, buyOrderSn));
    }

    /**
     * 查询采购商常购买的商品规格id列表(销量最高的30个)
     *
     * @param query
     * @return
     */
    @Override
    public List<BuyOrderDetail> queryOftenModelIds(BuyOrderSqlDTO query) {
        query.setCurrent(1L);
        query.setSize(30L);
        return baseMapper.queryOftenModelIds(query);
    }

    /**
     * 修改订单明细的订单状态(不包括已取消)
     *
     * @param buyOrderSn
     * @param status     (修改后状态)
     * @return
     */
    @Override
    public boolean updateStatusByOrderSn(String buyOrderSn, BuyOrderStatusEnum status) {
        BuyOrderDetail update = new BuyOrderDetail();
        update.setStatus(status);
        return retBool(baseMapper.update(update, Wrappers.<BuyOrderDetail>query().lambda()
                .eq(BuyOrderDetail::getBuyerId, buyOrderSn)
                .ne(BuyOrderDetail::getStatus, BuyOrderStatusEnum.cancel)
        ));
    }
}
