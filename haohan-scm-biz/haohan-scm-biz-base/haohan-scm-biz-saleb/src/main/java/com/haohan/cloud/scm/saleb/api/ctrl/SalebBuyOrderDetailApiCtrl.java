package com.haohan.cloud.scm.saleb.api.ctrl;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.ModifyBuyOrderDetailReq;
import com.haohan.cloud.scm.saleb.core.BuyOrderDetailCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/9/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/saleb/buyOrderDetail")
@Api(value = "ApiSalebBuyOrderDetail", tags = "buyOrderDetail相关操作")
public class SalebBuyOrderDetailApiCtrl {

    private final BuyOrderDetailCoreService buyOrderDetailCoreService;

    /**
     * 修改采购单明细采购价及采购数量
     * 采购数量 在 已下单/待确认 可修改
     * 采购价  在 已下单/待确认 可修改
     *    此接口用于管理后台pds使用 scm使用时采购数量未更新到库存锁定
     * @param req
     * @return
     */
    @PostMapping("/modifyDetail")
    @ApiOperation(value = "修改采购明细价格/数量")
    public R<BuyOrderDetail> modifyDetail(@Validated ModifyBuyOrderDetailReq req) {
        BuyOrderDetail buyOrderDetail = req.transTo();
        return buyOrderDetailCoreService.modifyDetail(buyOrderDetail);
    }


}
