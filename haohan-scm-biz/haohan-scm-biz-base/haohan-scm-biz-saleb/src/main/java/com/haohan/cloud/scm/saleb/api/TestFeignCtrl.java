package com.haohan.cloud.scm.saleb.api;

import com.haohan.cloud.scm.api.opc.entity.TradeOrder;
import com.haohan.cloud.scm.api.opc.feign.OpcFeignService;
import com.haohan.cloud.scm.api.opc.req.OpcSummaryOrderQueryReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/27
 */
@Data
//@RestController
//@AllArgsConstructor
//@RequestMapping("/api/saleb/test")
//@Api(value = "ApiOpcOperation", tags = "opcOperation运营管理操作")
public class TestFeignCtrl {

    private final OpcFeignService opcFeignService;

//    @PostMapping("/list")
    public R callTest(@RequestBody OpcSummaryOrderQueryReq req) {
//        SummaryOrder summaryOrder = new SummaryOrder();
//        BeanUtil.copyProperties(req, summaryOrder);
//        R resp = opcFeignService.queryOneSummaryOrder(summaryOrder, SecurityConstants.FROM_IN);
//        if (RUtil.isSuccess(resp)){
//            summaryOrder = BeanUtil.toBean(resp.getData(),SummaryOrder.class);
//        }
        TradeOrder tradeOrder = new TradeOrder();
//        tradeOrder.setId(req.getId());
        tradeOrder.setBuyId(req.getSummaryOrderId());
        R resp = opcFeignService.queryListTradeOrder(tradeOrder, SecurityConstants.FROM_IN);
        if (RUtil.isSuccess(resp)) {
            List list = (List) resp.getData();
        }
        return resp;

    }
}
