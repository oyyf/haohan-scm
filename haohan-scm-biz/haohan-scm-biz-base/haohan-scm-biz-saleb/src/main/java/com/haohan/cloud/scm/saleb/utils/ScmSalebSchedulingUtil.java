package com.haohan.cloud.scm.saleb.utils;

import com.haohan.cloud.scm.api.constant.ScmCacheNameConstant;
import com.haohan.cloud.scm.api.saleb.req.CreateBillBathReq;
import com.haohan.cloud.scm.common.tools.util.BaseScmSchedulingUtil;
import com.haohan.cloud.scm.saleb.core.impl.BuyOrderCoreServiceImpl;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

/**
 * @author dy
 * @date 2020/3/14
 * 定时任务: 创建采购订单对应账单
 */
@Slf4j
@Component
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ScmSalebSchedulingUtil extends BaseScmSchedulingUtil {

    private final BuyOrderCoreServiceImpl buyOrderCoreService;
    private final ScmSaleBUtils scmSaleBUtils;

    @Override
    protected void setTaskName() {
        super.taskName = "创建采购订单对应账单";
    }

    /**
     * 每日 01:10 执行
     */
    @Override
    @Scheduled(cron = "0 10 1 * * ?")
    public void run() {
        // 多租户
        List<Integer> tenantIdList = scmSaleBUtils.findTenantIdList();
        // 3600 秒内不重复执行
        this.beforeProcess(tenantIdList, 3600, ScmCacheNameConstant.SCM_SCHEDULED + "saleb_order_create_bill");
    }

    /**
     * 创建 配送日期为今天的 采购订单对应账单,
     */
    @Override
    public void process() {
        LocalDate today = LocalDate.now();
        log.debug("定时任务: {}, 配送日期: {}", taskName, today);
        CreateBillBathReq req = new CreateBillBathReq();
        req.setStartDate(today);
        req.setEndDate(today);
        buyOrderCoreService.createBillBatch(req);
    }
}
