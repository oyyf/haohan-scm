/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.dto.GoodsProfitDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderDetailReq;
import com.haohan.cloud.scm.api.saleb.req.QuerySummaryOrderDetailReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.service.BuyOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-29 13:29:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/BuyOrderDetail")
@Api(value = "buyorderdetail", tags = "buyorderdetail内部接口服务")
public class BuyOrderDetailFeignApiCtrl {

    private final BuyOrderDetailService buyOrderDetailService;


    /**
     * 通过id查询采购单明细
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(buyOrderDetailService.getById(id));
    }


    /**
     * 分页查询 采购单明细 列表信息
     * @param buyOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyOrderDetailPage")
    public R getBuyOrderDetailPage(@RequestBody BuyOrderDetailReq buyOrderDetailReq) {
        Page page = new Page(buyOrderDetailReq.getPageNo(), buyOrderDetailReq.getPageSize());
        BuyOrderDetail buyOrderDetail =new BuyOrderDetail();
        BeanUtil.copyProperties(buyOrderDetailReq, buyOrderDetail);

        return new R<>(buyOrderDetailService.page(page, Wrappers.query(buyOrderDetail)));
    }


    /**
     * 全量查询 采购单明细 列表信息
     * @param buyOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyOrderDetailList")
    public R<List<BuyOrderDetail>> getBuyOrderDetailList(@RequestBody BuyOrderDetailReq buyOrderDetailReq) {
        BuyOrderDetail buyOrderDetail =new BuyOrderDetail();
        BeanUtil.copyProperties(buyOrderDetailReq, buyOrderDetail);
        return R.ok(buyOrderDetailService.list(Wrappers.query(buyOrderDetail)));
    }


    /**
     * 新增采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @Inner
    @SysLog("新增采购单明细")
    @PostMapping("/add")
    public R save(@RequestBody BuyOrderDetail buyOrderDetail) {
        return new R<>(buyOrderDetailService.save(buyOrderDetail));
    }

    /**
     * 修改采购单明细
     * @param buyOrderDetail 采购单明细
     * @return R
     */
    @Inner
    @SysLog("修改采购单明细")
    @PostMapping("/update")
    public R updateById(@RequestBody BuyOrderDetail buyOrderDetail) {
        return new R<>(buyOrderDetailService.updateById(buyOrderDetail));
    }

    /**
     * 通过id删除采购单明细
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购单明细")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(buyOrderDetailService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购单明细")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(buyOrderDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购单明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyOrderDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param req 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购单明细总记录}")
    @PostMapping("/countByBuyOrderDetailReq")
    public R<Integer> countByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq req) {
        return RUtil.success(buyOrderDetailService.count(Wrappers.<BuyOrderDetail>query(req).lambda()
                .in(CollUtil.isNotEmpty(req.getModeIdSet()), BuyOrderDetail::getGoodsModelId, req.getModeIdSet()))
        );
    }


    /**
     * 根据对象条件，查询一条记录
     * @param buyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据buyOrderDetailReq查询一条货位信息表")
    @PostMapping("/getOneByBuyOrderDetailReq")
    public R<BuyOrderDetail> getOneByBuyOrderDetailReq(@RequestBody BuyOrderDetailReq buyOrderDetailReq) {
        return RUtil.success(buyOrderDetailService.getOne(Wrappers.query(buyOrderDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param buyOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<BuyOrderDetail> buyOrderDetailList) {

        return new R<>(buyOrderDetailService.saveOrUpdateBatch(buyOrderDetailList));
    }

    /**
     * 查询商品毛利
     * @param pmId
     * @return
     */
    @Inner
    @PostMapping("/queryGoodsProfit")
    public R queryGoodsProfit(@RequestBody String pmId){
        List<GoodsProfitDTO> goodsProfitDTOS = buyOrderDetailService.queryGoodsProfit(pmId);
        return new R<>(goodsProfitDTOS);
    }

    /**
     * 查询汇总单对应订单明细
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryWaitSummaryOrderDetail")
    public R queryWaitSummaryOrderDetail(@RequestBody QuerySummaryOrderDetailReq req){
        return new R<>(buyOrderDetailService.queryWaitSummaryOrderDetail(req));
    }

}
