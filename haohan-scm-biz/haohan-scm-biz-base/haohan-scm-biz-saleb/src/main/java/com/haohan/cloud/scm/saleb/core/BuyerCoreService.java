package com.haohan.cloud.scm.saleb.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerEditReq;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerQueryReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.api.saleb.vo.BuyerVO;

/**
 * @author dy
 * @date 2019/9/20
 */
public interface BuyerCoreService {

    BuyerVO fetchInfoWithRole(String buyerId);

    boolean addBuyer(BuyerEditReq req);

    boolean modifyBuyer(BuyerEditReq req);

    /**
     * 查询采购商 当前时间未结算账单数
     *
     * @param pmId
     * @param buyerId
     * @return
     */
    Integer countBuyerBill(String pmId, String buyerId);


    /**
     * 根据uid查询
     * 查询不到时新增采购商
     * 创建采购商,若无对应商家也创建
     * (salec用户转采购商, 默认无saleb小程序、管理后台登陆功能)
     *
     * @param buyer
     * @return
     */
    Buyer fetchBuyerByUidWithAdd(Buyer buyer);

    /**
     * 客户联系人创建采购商(用于登录saleb小程序)
     * (默认无管理后台登陆功能)
     *
     * @param buyer
     * @return
     */
    Buyer createBuyerByCustomer(Buyer buyer);

    /**
     * 删除采购商
     *
     * @param buyerId
     * @return
     */
    boolean deleteBuyer(String buyerId);

    boolean updateMerchantName(String merchantId, String merchantName);

    IPage<BuyerVO> findPage(Page<Buyer> page, BuyerQueryReq req);

    /**
     * 查询采购商是否需下单支付
     *
     * @param buyerId
     * @return
     */
    BuyerPayVO queryBuyerPayInfo(String buyerId);
}
