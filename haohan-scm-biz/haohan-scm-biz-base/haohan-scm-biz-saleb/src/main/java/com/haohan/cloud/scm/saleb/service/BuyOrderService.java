/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.resp.QueryOrderSumAndPriceSumResp;
import com.haohan.cloud.scm.api.salec.resp.OrderMonitorResp;

import java.util.List;

/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-13 20:34:39
 */
public interface BuyOrderService extends IService<BuyOrder> {
    List<QueryOrderSumAndPriceSumResp> countBuyOrder(CountBuyOrderReq req);

    List<OrderMonitorResp> queryBuyOrderByTime(BuyOrderReq req);

    BuyOrder fetchBySn(String buyOrderSn);

    IPage<OrderInfoDTO> findExtPage(BuyOrderSqlDTO query);

    /**
     * 根据来源订单id查询采购单
     *
     * @param sourceOrderId
     * @return
     */
    BuyOrder fetchBySourceOrderId(String sourceOrderId);
}
