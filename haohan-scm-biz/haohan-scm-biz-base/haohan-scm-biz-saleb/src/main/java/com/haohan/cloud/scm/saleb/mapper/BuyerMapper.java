/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.CountBuyerNumReq;

/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-13 20:35:22
 */
public interface BuyerMapper extends BaseMapper<Buyer> {

    Integer countBuyerNum(CountBuyerNumReq req);

}
