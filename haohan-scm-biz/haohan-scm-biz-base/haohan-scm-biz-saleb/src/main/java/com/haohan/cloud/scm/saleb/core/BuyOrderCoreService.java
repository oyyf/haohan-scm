package com.haohan.cloud.scm.saleb.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.saleb.BuyOrderStatusEnum;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderModifyReq;
import com.haohan.cloud.scm.api.saleb.req.CreateBillBathReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;

import java.util.List;

/**
 * @author dy
 * @date 2019/5/29
 * B客户模块
 */
public interface BuyOrderCoreService {

    /**
     * 新增采购订单
     *
     * @param req
     * @return
     */
    BuyOrderVO addBuyOrder(BuyOrderEditReq req);

    /**
     * 修改采购单
     * 状态限制: 已下单
     *
     * @param req
     * @return
     */
    boolean modifyBuyOrder(BuyOrderEditReq req);

    boolean cancelBuyOrder(String buyOrderSn);

    /**
     * 修改采购单 (以前接口, 迁移后废弃)
     * 状态限制: 已下单
     *
     * @param modifyReq
     * @return
     */
    boolean modifyOrder(BuyOrderModifyReq modifyReq);

    /**
     * 订单结算状态修改
     *
     * @param buyOrderSn
     * @param status
     * @return
     */
    boolean updateOrderSettlement(String buyOrderSn, PayStatusEnum status);

    /**
     * 采购订单状态改变(不包括已取消)
     * 采购单状态：1.已下单2.待确认3.成交4.取消  5.待发货 6.待收货
     *
     * @param buyOrderSn
     * @param status (修改后状态)
     * @return
     */
    boolean updateOrderStatus(String buyOrderSn, BuyOrderStatusEnum status);

    /**
     * 按日期批量创建账单
     *
     * @param req
     */
    boolean createBillBatch(CreateBillBathReq req);

    /**
     * 账单重建(重新创建)
     *
     * @param orderSn
     * @return
     */
    boolean resetBill(String orderSn);


    /**
     * 根据订单编号(buyId)查询订单详情(带pmName,merchantName)
     *
     * @param orderSn
     * @return
     */
    OrderInfoDTO fetchOrderInfoWithExt(String orderSn);

    BuyOrderVO fetchInfo(String buyOrderSn);

    IPage<BuyOrderVO> findPage(Page<BuyOrder> page, QueryBuyOrderReq req);

    IPage<BuyOrderVO> findPage(Page<BuyOrder> page, QueryBuyOrderReq req, boolean needDetail);

    /**
     * 分页查询  buyOrder联查buyer 返回商家
     *
     * @param page
     * @param req
     * @return
     */
    IPage<OrderInfoDTO> findExtPage(Page page, QueryBuyOrderReq req);

    /**
     * 查询采购商常购买的商品规格id列表(销量最高的30个)
     *
     * @param buyerId
     * @return
     */
    List<String> queryOftenModelIds(String buyerId);

}
