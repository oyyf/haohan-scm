package com.haohan.cloud.scm.saleb.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.*;
import com.haohan.cloud.scm.api.saleb.req.order.BuyOrderEditReq;
import com.haohan.cloud.scm.api.saleb.resp.QueryOrderSumAndPriceSumResp;
import com.haohan.cloud.scm.api.saleb.vo.BuyOrderVO;
import com.haohan.cloud.scm.common.tools.constant.FirstGroup;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyOrderCoreService;
import com.haohan.cloud.scm.saleb.core.ISalebBuyOrderService;
import com.haohan.cloud.scm.saleb.service.BuyOrderDetailService;
import com.haohan.cloud.scm.saleb.service.BuyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author dy
 * @date 2019/6/14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/saleb/buyOrder")
@Api(value = "ApiSalebBuyOrder", tags = "buyOrder相关操作")
public class SalebBuyOrderApiCtrl {

    private final ISalebBuyOrderService salebBuyOrderService;
    private final BuyOrderDetailService buyOrderDetailService;
    private final BuyOrderService buyOrderService;
    private final BuyOrderCoreService buyOrderCoreService;

    /**
     * 确认采购单的所有明细已成交
     *
     * @param req
     * @return
     */
    @PostMapping("/confirmBuyOrder")
    @ApiOperation(value = "确认采购单的所有明细已成交")
    public R<Boolean> queryEmployeeDetail(@Validated ConfirmBuyOrderReq req) {
        return new R(salebBuyOrderService.confirmBuyOrder(req.getBuyId()));
    }

    /**
     * 查询订单详情列表
     *
     * @param pmId
     * @param buyId
     * @return
     */
    @GetMapping("/queryBuyOrderDetailList/{pmId}/{buyId}")
    public R queryBuyOrderDetailList(@PathVariable("pmId") String pmId, @PathVariable("buyId") String buyId) {
        BuyOrderDetail detail = new BuyOrderDetail();
        detail.setBuyId(buyId);
        detail.setPmId(pmId);
        return new R<>(buyOrderDetailService.list(Wrappers.query(detail)));
    }

    /**
     * 批量修改采购单明细
     *
     * @param list
     * @return
     */
    @PostMapping("/updBuyOrderDetailList")
    public R<Boolean> updBuyOrderDetailList(@RequestBody List<BuyOrderDetail> list) {
        return new R<>(salebBuyOrderService.updBuyOrderDetailList(list));
    }

    /**
     * 取消订单 (以前接口, 迁移后废弃)
     *
     * @param req
     * @return
     */
    @PostMapping("/cancelOrder")
    public R<Boolean> cancelOrder(@Validated CancelBuyOrderReq req) {
        if (StrUtil.isEmpty(req.getBuyId()) && StrUtil.isEmpty(req.getBuyDetailSn())) {
            throw new ErrorDataException("缺少buyId或buyDetailSn");
        }
        return new R<>(salebBuyOrderService.cancelBuyOrder(req));
    }

    /**
     * 采购明细 判断库存备货 锁定库存(批量)
     *
     * @param order
     * @return
     */
    @PostMapping("/detailConfirmStockList")
    public R<Boolean> detailConfirmStockList(@RequestBody @Validated BuyOrder order) {
        return new R<>(salebBuyOrderService.detailConfirmStockList(order));
    }

    /**
     * 触发修改采购单状态 为wait
     *
     * @return
     */
    @PostMapping("/changeStatus")
    public R buyOrderChangeStatusWait(@Validated BuyOrderChangeStatusReq req) {
        return new R<>(salebBuyOrderService.buyOrderChangeStatusWait(req.getPmId(), req.getMinute()));
    }

    /**
     * 根据商品创建订单明细
     *
     * @param req
     * @return
     */
    @PostMapping("/createBuyOrderDetailByGoods")
    public R createBuyOrderDetailByGoods(@RequestBody @Validated CreateBuyOrderDetailReq req) {
        return new R<>(salebBuyOrderService.createBuyOrderDetail(req));
    }

    /**
     * 查询区域对应的订单总数和订单总金额
     *
     * @param req
     * @return
     */
    @PostMapping("/queryBuyOrder")
    public R queryBuyOrder(@Validated CountBuyOrderReq req) {
        List<QueryOrderSumAndPriceSumResp> list = buyOrderService.countBuyOrder(req);
        return new R<>(list);
    }

    /**
     * 查询汇总单对应订单详情(临时数据)
     *
     * @param req
     * @return
     */
    @PostMapping("/queryWaitSummaryOrderDetail")
    public R queryWaitSummaryOrderDetail(@RequestBody @Validated QuerySummaryOrderDetailReq req) {
        return new R<>(buyOrderDetailService.queryWaitSummaryOrderDetail(req));
    }

    /**
     * 查询指定时间b订单
     *
     * @param req
     * @return
     */
    @PostMapping("/queryBuyOrderBuyTime")
    public R queryBuyOrderBuyTime(@RequestBody BuyOrderReq req) {
        return new R<>(salebBuyOrderService.queryBuyOrderByTime(req));
    }

    /**
     * 订单确认收货 交易完成
     *
     * @param req
     * @return
     */
    @PostMapping("/confirmReceive")
    public R confirmReceive(@Validated ConfirmReceiveReq req) {
        try {
            BuyOrder buyOrder = req.transTo();
            salebBuyOrderService.confirmReceive(buyOrder);
            return R.ok(true, "当前订单收货成功");
        } catch (Exception e) {
            return R.failed("当前订单不可收货");
        }
    }

    @GetMapping("/info")
    @ApiOperation(value = "查询订单详情")
    public R<BuyOrderVO> fetchInfo(String buyOrderSn) {
        if (StrUtil.isEmpty(buyOrderSn)) {
            return R.failed("缺少参数buyOrderSn");
        }
        return RUtil.success(buyOrderCoreService.fetchInfo(buyOrderSn));
    }

    /**
     * B客户订单修改 状态限制: 已下单 (以前接口, 迁移后废弃)
     *
     * @param req
     * @return
     */
    @PostMapping("/modify")
    public R<Boolean> modifyBuyOrder(@RequestBody @Valid BuyOrderModifyReq req) {
        boolean flag = buyOrderCoreService.modifyOrder(req);
        return R.ok(flag, flag ? "订单修改成功" : "当前状态不可修改");
    }

    /**
     * 新增采购订单
     *
     * @param req
     * @return
     */
    @PostMapping("/info")
    public R<BuyOrderVO> addBuyOrder(@RequestBody @Validated(FirstGroup.class) BuyOrderEditReq req) {
        return RUtil.success(buyOrderCoreService.addBuyOrder(req));
    }

    /**
     * 修改采购订单
     * 状态限制: 已下单
     *
     * @param req
     * @return
     */
    @PutMapping("/info")
    public R<Boolean> modifyBuyOrder(@RequestBody @Validated(SecondGroup.class) BuyOrderEditReq req) {
        return RUtil.success(buyOrderCoreService.modifyBuyOrder(req));
    }

    /**
     * 采购单取消
     *
     * @param buyOrderSn
     * @return
     */
    @DeleteMapping("/info")
    public R<Boolean> cancelBuyOrder(String buyOrderSn) {
        if (StrUtil.isEmpty(buyOrderSn)) {
            return R.failed("缺少参数buyOrderSn");
        }
        return RUtil.success(buyOrderCoreService.cancelBuyOrder(buyOrderSn));
    }

    /**
     * 批量创建账单
     *
     * @param req
     * @return
     */
    @PostMapping("/createBillBatch")
    public R<Boolean> createBillBatch(@Validated CreateBillBathReq req) {
        if (buyOrderCoreService.createBillBatch(req)) {
            return RUtil.success(true);
        }
        return R.failed("批量创建账单失败, 无可创建的账单");
    }

    /**
     * 账单重建(重新创建)
     *
     * @param orderSn
     * @return
     */
    @PostMapping("/resetBill")
    public R<Boolean> resetBill(String orderSn) {
        if (StrUtil.isBlank(orderSn)) {
            return R.failed("订单编号不能为空");
        }
        if (buyOrderCoreService.resetBill(orderSn)) {
            return RUtil.success(true);
        }
        return R.failed("账单重算失败, 执行错误");
    }


}
