/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.CountBuyerNumReq;

/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-13 20:35:22
 */
public interface BuyerService extends IService<Buyer> {
    Integer countBuyerNum(CountBuyerNumReq req);

    /**
     * 根据采购商名称查询, 相同时只返回一条
     * @param buyerName
     * @return
     */
    Buyer fetchByName(String buyerName);

    Buyer fetchByTelephone(String telephone);

    Buyer fetchByUid(String uid);
}
