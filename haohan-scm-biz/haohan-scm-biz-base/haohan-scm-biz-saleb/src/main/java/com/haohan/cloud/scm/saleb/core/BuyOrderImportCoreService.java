package com.haohan.cloud.scm.saleb.core;

import com.haohan.cloud.scm.api.saleb.dto.BuyOrderImport;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderImportOneReq;
import com.pig4cloud.pigx.common.core.util.R;

import java.util.List;

/**
 * @author dy
 * @date 2019/9/13
 */
public interface BuyOrderImportCoreService {

    /**
     * 处理导入的采购单信息列表
     *
     * @param list
     * @return
     */
    R<String> saveBuyOrder(List<BuyOrderImport> list);

    /**
     * 保存导入单个采购单
     * @param list
     * @param req
     * @return
     */
    R<String> saveOneBuyOrder(List<BuyOrderImport> list, BuyOrderImportOneReq req);

    /**
     * 一个B客户订单 新增明细
     * @param list
     * @param buyId
     * @return
     */
    R<String> addBuyOrderDetail(List<BuyOrderImport> list, String buyId);
}
