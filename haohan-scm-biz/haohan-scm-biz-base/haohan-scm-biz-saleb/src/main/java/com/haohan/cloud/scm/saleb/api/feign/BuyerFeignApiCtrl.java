/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.saleb.entity.Buyer;
import com.haohan.cloud.scm.api.saleb.req.BuyerReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyerNumReq;
import com.haohan.cloud.scm.api.saleb.req.QueryBuyerStatusReq;
import com.haohan.cloud.scm.api.saleb.req.buyer.BuyerFeignReq;
import com.haohan.cloud.scm.api.saleb.vo.BuyerPayVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.saleb.core.BuyerCoreService;
import com.haohan.cloud.scm.saleb.service.BuyerService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购商
 *
 * @author haohan
 * @date 2019-05-29 13:28:25
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Buyer")
@Api(value = "buyer", tags = "buyer内部接口服务")
public class BuyerFeignApiCtrl {

    private final BuyerService buyerService;
    private final BuyerCoreService buyerCoreService;


    /**
     * 通过id查询采购商
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R<Buyer> getById(@PathVariable("id") String id) {
        return RUtil.success(buyerService.getById(id));
    }


    /**
     * 分页查询 采购商 列表信息
     *
     * @param buyerReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyerPage")
    public R getBuyerPage(@RequestBody BuyerReq buyerReq) {
        Page page = new Page(buyerReq.getPageNo(), buyerReq.getPageSize());
        Buyer buyer = new Buyer();
        BeanUtil.copyProperties(buyerReq, buyer);


        return new R<>(buyerService.page(page, Wrappers.query(buyer)));
    }


    /**
     * 全量查询 采购商 列表信息
     *
     * @param buyerReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyerList")
    public R getBuyerList(@RequestBody BuyerReq buyerReq) {
        Buyer buyer = new Buyer();
        BeanUtil.copyProperties(buyerReq, buyer);

        return new R<>(buyerService.list(Wrappers.query(buyer)));
    }


    /**
     * 新增采购商
     *
     * @param buyer 采购商
     * @return R
     */
    @Inner
    @SysLog("新增采购商")
    @PostMapping("/add")
    public R save(@RequestBody Buyer buyer) {
        return new R<>(buyerService.save(buyer));
    }

    /**
     * 修改采购商
     *
     * @param buyer 采购商
     * @return R
     */
    @Inner
    @SysLog("修改采购商")
    @PostMapping("/update")
    public R updateById(@RequestBody Buyer buyer) {
        return new R<>(buyerService.updateById(buyer));
    }

    /**
     * 通过id删除采购商
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购商")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(buyerService.removeById(id));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询采购商")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(buyerService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param buyerReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @PostMapping("/countByBuyerReq")
    public R<Integer> countByBuyerReq(@RequestBody BuyerReq buyerReq) {

        return new R<>(buyerService.count(Wrappers.query(buyerReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param buyerReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @PostMapping("/getOneByBuyerReq")
    public R<Buyer> getOneByBuyerReq(@RequestBody BuyerReq buyerReq) {
        return R.ok(buyerService.getOne(Wrappers.query(buyerReq), false));
    }


    /**
     * 查询B客户某时间范围段人数
     *
     * @param countBuyerNumReq
     * @return
     */
    @Inner
    @PostMapping("/countRangeBuyerNum")
    public R countRangeBuyerNum(@RequestBody CountBuyerNumReq countBuyerNumReq) {

        return new R<>(buyerService.countBuyerNum(countBuyerNumReq));
    }

    /**
     * 查询采购商是否可下单
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryStatus")
    public R<Boolean> buyerStatus(@RequestBody QueryBuyerStatusReq req) {
        Integer num = buyerCoreService.countBuyerBill(req.getPmId(), req.getBuyerId());
        return RUtil.success(null != num && num == 0);
    }

    /**
     * 查询采购商是否需下单支付
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/buyerPayInfo")
    public R<BuyerPayVO> buyerPayInfo(@RequestBody QueryBuyerStatusReq req) {
        return RUtil.success(buyerCoreService.queryBuyerPayInfo(req.getBuyerId()));
    }

    /**
     * 根据uid查询
     * 查询不到时新增采购商及商家
     * (salec用户转采购商, 默认无saleb小程序、管理后台登陆功能)
     *
     * @param query
     * @return
     */
    @Inner
    @PostMapping("/fetchBuyerByUidWithAdd")
    public R<Buyer> fetchBuyerByUidWithAdd(@RequestBody BuyerFeignReq query) {
        return RUtil.success(buyerCoreService.fetchBuyerByUidWithAdd(query.transTo()));
    }

    /**
     * 客户联系人创建采购商(用于登录saleb小程序)
     * (默认无管理后台登陆功能)
     *
     * @param buyer
     * @return
     */
    @Inner
    @PostMapping("/createByCustomer")
    public R<Buyer> createBuyerByCustomer(@RequestBody Buyer buyer) {
        return R.ok(buyerCoreService.createBuyerByCustomer(buyer));
    }

    @Inner
    @SysLog("采购商商家名称修改")
    @PostMapping("/updateMerchantName")
    public R<Boolean> updateMerchantName(@RequestBody BuyerReq req) {
        return RUtil.success(buyerCoreService.updateMerchantName(req.getMerchantId(), req.getMerchantName()));
    }

}
