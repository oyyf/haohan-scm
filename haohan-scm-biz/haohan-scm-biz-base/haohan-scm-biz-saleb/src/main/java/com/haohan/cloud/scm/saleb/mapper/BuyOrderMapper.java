/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.common.dto.SelfPageMapper;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrder;
import com.haohan.cloud.scm.api.saleb.req.BuyOrderReq;
import com.haohan.cloud.scm.api.saleb.req.CountBuyOrderReq;
import com.haohan.cloud.scm.api.saleb.resp.QueryOrderSumAndPriceSumResp;
import com.haohan.cloud.scm.api.salec.resp.OrderMonitorResp;

import java.util.List;

/**
 * 采购单
 *
 * @author haohan
 * @date 2019-05-13 20:34:39
 */
public interface BuyOrderMapper extends BaseMapper<BuyOrder>, SelfPageMapper {

    List<QueryOrderSumAndPriceSumResp> countBuyOrder(CountBuyOrderReq req);

    List<OrderMonitorResp> queryBuyOrderByTime(BuyOrderReq req);

    /**
     * 自定义分页使用 扩展联查buyer
     *
     * @param params
     * @return
     */
    Integer queryCount(BuyOrderSqlDTO params);

    /**
     * 自定义分页使用 扩展联查buyer
     *
     * @param params
     * @return
     */
    List<OrderInfoDTO> queryPage(BuyOrderSqlDTO params);
}
