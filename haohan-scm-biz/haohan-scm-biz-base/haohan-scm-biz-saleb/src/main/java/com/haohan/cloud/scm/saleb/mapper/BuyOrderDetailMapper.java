/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.saleb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderDetailDTO;
import com.haohan.cloud.scm.api.saleb.dto.BuyOrderSqlDTO;
import com.haohan.cloud.scm.api.saleb.dto.GoodsProfitDTO;
import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.haohan.cloud.scm.api.saleb.req.QuerySummaryOrderDetailReq;

import java.math.BigDecimal;
import java.util.List;

/**
 * 采购单明细
 *
 * @author haohan
 * @date 2019-05-13 20:34:19
 */
public interface BuyOrderDetailMapper extends BaseMapper<BuyOrderDetail> {

    /**
     * 汇总商品下单数量
     * @param buyOrderDetailDTO 必需参数
     * @return
     */
    List<BuyOrderDetailDTO> summaryGoodsNum(BuyOrderDetailDTO buyOrderDetailDTO);

    /**
     * 查询采购明细
     * 采购单 参数: buySeq/deliveryTime
     * 明细 参数: summaryFlag/status/buyId/buyDetailSn/pmId/id
     * @param buyOrderDetailDTO
     * @return
     */
    List<BuyOrderDetailDTO> getOneWithOrder(BuyOrderDetailDTO buyOrderDetailDTO);


//    List<BuyOrderDetail> sunmmaryGoodsDetailList(BuyOrderDetailDTO buyOrderDetailDTO);

    /**
     * 查询商品毛利排行
     * @param pmId
     * @return
     */
    List<GoodsProfitDTO> queryGoodsPrifit(String pmId);

    /**
     * 查询汇总单对应订单详情
     * @param req
     * @return
     */
    List<BuyOrderDetail> queryWaitSummaryOrderDetail(QuerySummaryOrderDetailReq req);

    BigDecimal queryPriceSum(String buyId);

    /**
     * 查询采购商常购买的商品规格id列表(销量最高的30个)
     * @param query
     * @return
     */
    List<BuyOrderDetail> queryOftenModelIds(BuyOrderSqlDTO query);
}
