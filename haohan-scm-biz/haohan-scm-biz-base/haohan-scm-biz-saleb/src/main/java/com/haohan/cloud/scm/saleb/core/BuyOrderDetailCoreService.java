package com.haohan.cloud.scm.saleb.core;

import com.haohan.cloud.scm.api.saleb.entity.BuyOrderDetail;
import com.pig4cloud.pigx.common.core.util.R;

/**
 * @author dy
 * @date 2019/9/17
 */
public interface BuyOrderDetailCoreService {

    /**
     * 修改采购单明细采购价及采购数量
     * 采购数量 在 已下单/待确认 可修改
     * 采购价  在 已下单/待确认 可修改
     * @param buyOrderDetail
     * @return
     */
    R<BuyOrderDetail> modifyDetail(BuyOrderDetail buyOrderDetail);


}
