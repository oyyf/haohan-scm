/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;

/**
 * 采购员工账户
 *
 * @author haohan
 * @date 2019-05-13 19:05:49
 */
public interface PurchaseAccountMapper extends BaseMapper<PurchaseAccount> {

}
