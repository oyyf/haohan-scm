/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseCateCountDTO;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskDetailResp;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskListByDirectorResp;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseOrderService;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;



/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-29 13:34:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseOrder")
@Api(value = "purchaseorder", tags = "purchaseorder内部接口服务")
public class PurchaseOrderFeignApiCtrl {

    private final PurchaseOrderService purchaseOrderService;

    private final IScmPurchaseOrderService iScmPurchaseOrderService;

    private final IScmPurchaseOrderService orderService;

    /**
     * 通过id查询采购部采购单
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseOrderService.getById(id));
    }


    /**
     * 分页查询 采购部采购单 列表信息
     *
     * @param purchaseOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseOrderPage")
    public R getPurchaseOrderPage(@RequestBody PurchaseOrderReq purchaseOrderReq) {
        Page page = new Page(purchaseOrderReq.getPageNo(), purchaseOrderReq.getPageSize());
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        BeanUtil.copyProperties(purchaseOrderReq, purchaseOrder);

        return new R<>(purchaseOrderService.page(page, Wrappers.query(purchaseOrder)));
    }


    /**
     * 全量查询 采购部采购单 列表信息
     *
     * @param purchaseOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseOrderList")
    public R getPurchaseOrderList(@RequestBody PurchaseOrderReq purchaseOrderReq) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        BeanUtil.copyProperties(purchaseOrderReq, purchaseOrder);

        return new R<>(purchaseOrderService.list(Wrappers.query(purchaseOrder)));
    }


    /**
     * 新增采购部采购单
     *
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @Inner
    @SysLog("新增采购部采购单")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseOrder purchaseOrder) {
        return new R<>(purchaseOrderService.save(purchaseOrder));
    }

    /**
     * 修改采购部采购单
     *
     * @param purchaseOrder 采购部采购单
     * @return R
     */
    @Inner
    @SysLog("修改采购部采购单")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseOrder purchaseOrder) {
        return new R<>(purchaseOrderService.updateById(purchaseOrder));
    }

    /**
     * 通过id删除采购部采购单
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购部采购单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseOrderService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除采购部采购单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询采购部采购单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购部采购单总记录}")
    @PostMapping("/countByPurchaseOrderReq")
    public R countByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq) {

        return new R<>(purchaseOrderService.count(Wrappers.query(purchaseOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param purchaseOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseOrderReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseOrderReq")
    public R getOneByPurchaseOrderReq(@RequestBody PurchaseOrderReq purchaseOrderReq) {

        return new R<>(purchaseOrderService.getOne(Wrappers.query(purchaseOrderReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param purchaseOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseOrder> purchaseOrderList) {
        return new R<>(purchaseOrderService.saveOrUpdateBatch(purchaseOrderList));
    }

    /**
     * 根据purchaseOrderDetailSn添加PurchaseOrde
     *
     * @param req
     * @return R
     */
    @Inner
    @PostMapping("/addPurchaseOrder")
    public R<PurchaseOrder> addPurchaseOrder(@RequestBody AddPurchaseOrderReq req) {
        return new R<>(iScmPurchaseOrderService.addPurchaseOrder(req));
    }

    /**
     * 查询今日采购单总数
     *
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/queryTodayPurchaseCount")
    public R<Integer> queryTodayPurchaseCount(@RequestBody CountPurchaseOrderReq req) {
        Integer integer = purchaseOrderService.countPurchaseOrderNum(req);
        return new R<>(integer);
    }

    /**
     * 查询已入库商品种类
     *
     * @return
     */
    @Inner
    @PostMapping("/queryEnterGoodsCate")
    public R<Integer> queryEnterGoodsCate(@RequestBody PurchaseTodayDTO dto) {
        return new R<>(purchaseOrderService.queryEnterGoodsCate(dto));
    }

    /**
     * 查询当天采购金额
     *
     * @return
     */
    @Inner
    @PostMapping("/queryTodayAmount")
    public R<BigDecimal> queryTodayAmount(@RequestBody PurchaseTodayDTO dto) {
        return new R<>(iScmPurchaseOrderService.queryTodayAmount(dto));
    }

    /**
     * 查询已入库采购单
     *
     * @return
     */
    @Inner
    @PostMapping("/queryEnterPurchase")
    public R<Integer> queryEnterPurchase() {
        PurchaseTodayDTO dto = new PurchaseTodayDTO();
        return new R<>(purchaseOrderService.queryEnterPurchase(dto));
    }

    /**
     * 查询采购状态数量
     * @param pmId
     * @return
     */
    @Inner
    @PostMapping("/queryPurchaseStatus/{pmId}")
    public R<List<Map<String,Object>>> queryPurchaseStatus(@PathVariable("pmId") String pmId){
        return new R<>(iScmPurchaseOrderService.queryPurchaseStatus(pmId));
    }

    /**
     * 查询采购商品分类统计
     * @param pmId
     * @return
     */
    @Inner
    @PostMapping("/queryPurchaseGoodsCateCount/{pmId}")
    public R<List<PurchaseCateCountDTO>> queryPurchaseGoodsCateCount(@PathVariable("pmId") String pmId){
        return new R<>(purchaseOrderService.queryPurchaseGoodsCateCount(pmId));
    }

    @Inner
    @PostMapping("/queryTaskListByDirector")
    @ApiOperation(value = "总监查询当前任务列表-小程序", notes = "总监 查询采购任务列表(带采购商品信息)")
    public R<Page<PurchaseTaskListByDirectorResp>> queryTaskListByDirector(@RequestBody @Validated PurchasePageReq req) {
        return new R(orderService.queryTaskListByDirector(req));
    }

    @Inner
    @PostMapping("/queryPurchaseTask")
    @ApiOperation(value = "查询采购任务", notes = "查询采购任务详情(带采购商品信息)")
    public R<PurchaseTaskDetailResp> queryPurchaseTask(@RequestBody @Validated QueryPurchaseTaskReq req) {
        return new R(orderService.queryPurchaseTask(req));
    }

    @Inner
    @PostMapping("/appointTask")
    @ApiOperation(value = "经理 执行采购任务(分配) 指定采购员")
    public R appointTask(@RequestBody AppointTaskReq req) {
        return new R(orderService.appointTask(req));
    }

    @Inner
    @PostMapping("/auditTaskBatch")
    @ApiOperation(value = "批量通过采购任务", notes = "总监 执行采购任务(审核) 通过—小程序")
    public R<Boolean> auditTaskBatch(@RequestBody AuditTaskBatchReq req) {
        return new R(orderService.auditTaskBatch(req));
    }

    @Inner
    @PostMapping("/singlePurchase")
    @ApiOperation(value = "采购单明细 供应商确定(小程序:单品采购)")
    public R singlePurchase(@RequestBody SinglePurchaseReq req){
        return new R(orderService.singlePurchase(req));
    }

    @Inner
    @PostMapping("/selectOrderSupplier")
    @ApiOperation(value = "采购单明细 供应商确定(小程序:竞价采购)")
    public R selectOrderSupplier(@RequestBody ChoiceSupplierReq req){
        return new R(orderService.selectOrderSupplier(req));
    }

    @Inner
    @PostMapping("/agreementPurchase")
    @ApiOperation(value = "采购单明细 供应商确定(协议供应)")
    public R agreementPurchase(@RequestBody AgreementPurchaseReq req) {
        return new R(orderService.agreementPurchase(req));
    }

    @Inner
    @PostMapping("/queryPurchaseTaskList")
    @ApiOperation(value ="采购员工(经理/采购员) 查询采购任务列表(带采购商品信息)")
    public R queryPurchaseTaskList(@RequestBody PurchasePageReq req) {
        return new R(orderService.queryPurchaseTaskList(req));
    }
    /**
     *  采购部  总监发起采购计划
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/initiatePurchasePlanByDirector")
    @ApiOperation(value = "发起采购计划", notes = "总监发起采购计划—小程序")
    public R<PurchaseOrderDetail> initiatePurchasePlanByDirector(@RequestBody InitiatePurchasePlanReq req){
        return new R(iScmPurchaseOrderService.initiatePurchasePlan(req));

    }
}