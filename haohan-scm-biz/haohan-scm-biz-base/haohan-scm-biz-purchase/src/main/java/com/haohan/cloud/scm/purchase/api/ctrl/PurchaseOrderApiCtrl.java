package com.haohan.cloud.scm.purchase.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskDetailResp;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskListByDirectorResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseOrderService;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderService;
import com.haohan.cloud.scm.purchase.utils.ScmPurchaseUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author dy
 * @date 2019/5/17
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/purchase/purchaseOrder")
@Api(value = "ApiPurchaseOrder", tags = "purchaseOrder采购单api")
public class PurchaseOrderApiCtrl {
    private final IScmPurchaseOrderService iScmPurchaseOrderService;
    private final ScmPurchaseUtils scmPurchaseUtils;
    private final PurchaseOrderService purchaseOrderService;
    private final PurchaseOrderDetailService purchaseOrderDetailService;

    /**
     * 根据汇总单生成采购单明细
     *
     * @param summaryOrder
     * @return
     */

    @PostMapping("/addPurchaseOrderDetail")
    @ApiOperation(value = "生成采购单明细", notes = "根据汇总单生成采购单明细")
    public R<PurchaseOrderDetail> addPurchaseOrderDetail(@Validated SummaryOrder summaryOrder) {
        return new R(iScmPurchaseOrderService.addPurchaseOrderDetailByQuery(summaryOrder));
    }

    /**
     * 根据采购明细生成采购单
     *
     * @param req
     * @return
     */

    @PostMapping("/addPurchaseOrder")
    @ApiOperation(value = "生成采购单", notes = "根据采购单明细生成采购单")
    public R<PurchaseOrder> addPurchaseOrder(@Validated AddPurchaseOrderReq req) {
        return new R(iScmPurchaseOrderService.addPurchaseOrder(req));
    }

    /**
     * 采购单明细 供应商确定(竞价采购)
     *
     * @param req
     * @return
     */
    @PostMapping("/selectOrderSupplier")
    @ApiOperation(value = "供应商确认", notes = "采购单明细 供应商确定(小程序:竞价采购)")
    public R<PurchaseOrderDetail> selectOrderSupplier(@Validated ChoiceSupplierReq req) {
        return new R(iScmPurchaseOrderService.selectOrderSupplier(req));
    }

    /**
     * 采购单明细 供应商确定(单品采购)
     *   管理后台中使用
     * @param req
     * @return
     */
    @PostMapping("/singlePurchase")
    @ApiOperation(value = "供应商确认", notes = "采购单明细 供应商确定(小程序:单品采购)")
    public R<PurchaseOrderDetail> singlePurchase(@RequestBody @Validated SinglePurchaseReq req) {
        return new R(iScmPurchaseOrderService.singlePurchase(req));
    }

    /**
     * 采购单明细 供应商确定(协议供应)
     *
     * @param req
     * @return
     */
    @PostMapping("agreementPurchase")
    @ApiOperation(value = "供应商确认", notes = "采购单明细 供应商确定(小程序:协议供应)")
    public R<PurchaseOrderDetail> agreementPurchase(@Validated AgreementPurchaseReq req) {
        return new R(iScmPurchaseOrderService.agreementPurchase(req));
    }

    /**
     * 查询当前任务列表(带采购商品信息) 采购员工(经理/采购员)
     *
     * @param req
     * @return
     */
    @PostMapping("/queryPurchaseTaskList")
    @ApiOperation(value = "查询当前任务列表-小程序", notes = "采购员工(经理/采购员) 查询采购任务列表(带采购商品信息)")
    public R<Page> queryPurchaseTaskList(@RequestBody @Validated PurchasePageReq req) {
        return new R(iScmPurchaseOrderService.queryPurchaseTaskList(req));
    }

    /**
     * 查询当前任务列表(带采购商品信息) 采购总监
     *
     * @param req
     * @return
     */
    @PostMapping("/queryTaskListByDirector")
    @ApiOperation(value = "总监查询当前任务列表-小程序", notes = "总监 查询采购任务列表(带采购商品信息)")
    public R<Page<PurchaseTaskListByDirectorResp>> queryTaskListByDirector(@RequestBody @Validated PurchasePageReq req) {
        return new R(iScmPurchaseOrderService.queryTaskListByDirector(req));
    }

    /**
     * 查询采购任务详情(带采购商品信息)
     *
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseTask")
    @ApiOperation(value = "查询采购任务", notes = "查询采购任务详情(带采购商品信息)")
    public R<PurchaseTaskDetailResp> queryPurchaseTask(@Validated QueryPurchaseTaskReq req) {
        return new R(iScmPurchaseOrderService.queryPurchaseTask(req));
    }

    /**
     * 查询采购任务详情(带采购商品信息)
     *
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseTaskApp")
    @ApiOperation(value = "查询采购任务--小程序", notes = "查询采购任务详情(带采购商品信息)")
    public R<PurchaseTaskDetailResp> queryPurchaseTaskApp(@Validated QueryPurchaseTaskReq req) {
        if (StrUtil.isEmpty(req.getUid())) {
            throw new EmptyDataException("没有通行证");
        }
        scmPurchaseUtils.fetchByUid(req.getPmId(), req.getUid());
        return new R(iScmPurchaseOrderService.queryPurchaseTask(req));
    }

    /**
     * 总监 执行采购任务(审核) 批量通过
     *
     * @param req 采购任务id、执行人id、执行人备注
     * @return
     */
    @PostMapping("/auditTaskBatch")
    @ApiOperation(value = "批量通过采购任务", notes = "总监 执行采购任务(审核) 通过—小程序")
    public R<Boolean> auditTaskBatch(@Validated AuditTaskBatchReq req) {
        return new R(iScmPurchaseOrderService.auditTaskBatch(req));
    }

    /**
     * 总监 执行采购任务(审核) 修改采购单明细
     *
     * @param req
     * @return
     */
    @PostMapping("/auditTask")
    @ApiOperation(value = "执行采购任务(审核)——小程序", notes = "总监 执行采购任务(审核) 修改采购单明细")
    public R<PurchaseOrderDetail> auditTask(@Validated AuditTaskReq req) {
        return new R(iScmPurchaseOrderService.auditTask(req));
    }

    /**
     * 经理 执行采购任务(分配) 指定采购员
     *
     * @param req
     * @return
     */
    @PostMapping("/appointTask")
    @ApiOperation(value = "执行采购任务(分配)", notes = "经理 执行采购任务(分配) 指定采购员")
    public R<PurchaseTask> appointTask(@RequestBody @Validated AppointTaskReq req) {
        return new R(iScmPurchaseOrderService.appointTask(req));
    }

    /**
     * 经理 执行采购任务(分配) 指定采购员
     *
     * @param req
     * @return
     */
    @PostMapping("/appointTaskApp")
    @ApiOperation(value = "执行采购任务(分配)--小程序", notes = "经理 执行采购任务(分配) 指定采购员")
    public R<PurchaseTask> appointTaskApp(@RequestBody @Validated AppointTaskReq req) {
        if (StrUtil.isEmpty(req.getUid())) {
            throw new EmptyDataException("通行证id为空");
        }
        scmPurchaseUtils.fetchByUid(req.getPmId(), req.getUid());
        return new R(iScmPurchaseOrderService.appointTask(req));
    }

    /**
     * 采购部 发起需求采购
     *
     * @param req
     * @return
     */
    @PostMapping("/initiateDemandPurchase")
    @ApiOperation(value = "发起需求采购", notes = "采购部 发起需求采购")
    public R<List<PurchaseOrderDetail>> initiateDemandPurchase(@Validated PurchaseNeedReq req) {
        return new R(iScmPurchaseOrderService.initiateDemandPurchase(req));
    }

    /**
     * 采购部/高管/运营  发起采购计划
     *
     * @param req
     * @return
     */
    @PostMapping("/initiatePurchasePlan")
    @ApiOperation(value = "发起采购计划", notes = "采购部/高管/运营 发起采购计划")
    public R<PurchaseOrderDetail> initiatePurchasePlan(@Validated InitiatePurchasePlanReq req) {
        return new R(iScmPurchaseOrderService.initiatePurchasePlan(req));
    }

    /**
     * 采购部  总监发起采购计划
     *
     * @param req
     * @return
     */
    @PostMapping("/initiatePurchasePlanByDirector")
    @ApiOperation(value = "发起采购计划", notes = "总监发起采购计划—小程序")
    public R<PurchaseOrderDetail> initiatePurchasePlanByDirector(@Validated InitiatePurchasePlanReq req) {
        if (StrUtil.isEmpty(req.getUId())) {
            throw new EmptyDataException("通行证id为空");
        }
        scmPurchaseUtils.fetchByUid(req.getPmId(), req.getUId());
        return new R(iScmPurchaseOrderService.initiatePurchasePlan(req));
    }

    /**
     * 查询采购单列表
     *
     * @param req
     * @return
     */
    @PostMapping("/queryPurchaseOrderList")
    @ApiOperation(value = "查询采购单列表")
    public R queryPurchaseOrderList(@Validated QueryPurchaseOrderListReq req) {
        Page queryPage = new Page();
        queryPage.setCurrent(req.getCurrent());
        queryPage.setSize(req.getSize());
        return new R<>(purchaseOrderService.page(queryPage));
    }

    /**
     * 根据采购单号查询采购单明细列表
     *
     * @param purchaseSn
     * @return
     */
    @GetMapping("/queryPurchaseOrderDetailList")
    @ApiOperation(value = "根据采购单号查询采购单明细列表")
    public R queryPurchaseOrderDetailList(@Validated String purchaseSn) {
        if (StrUtil.isEmpty(purchaseSn)) {
            throw new EmptyDataException();
        }
        PurchaseOrderDetail orderDetail = new PurchaseOrderDetail();
        orderDetail.setPurchaseSn(purchaseSn);
        return new R<>(purchaseOrderDetailService.list(Wrappers.query(orderDetail)));
    }

    /**
     * 新增采购单(采购单明细)/初始化采购任务/创建货品信息/初始化入库单
     *
     * @param req
     * @return
     */
    @PostMapping("/addPurchaseOrderAndDetail")
    @ApiOperation(value = "直接新增采购单和采购单明细")
    public R addPurchaseOrderAndDetail(@RequestBody @Valid AddPurchaseOrderAndDetailReq req) {

        return new R<>(iScmPurchaseOrderService.addPurchaseOrderAndDetail(req));
    }

    /**
     * 发起采购
     *
     * @param req
     * @return
     */
    @PostMapping("/beginPurchase")
    @ApiOperation(value = "直接新增采购单和采购单明细")
    public R<Boolean> beginPurchase(@Validated BeginPurchaseReq req) {
        return R.ok(iScmPurchaseOrderService.beginPurchase(req));
    }

    /**
     * 查询今日采购单数
     *
     * @param req
     * @return
     */
    @PostMapping("/countPurchaseOrder")
    @ApiOperation(value = "查询今日采购单数")
    public R countPurchaseOrder(@RequestBody @Validated CountPurchaseOrderReq req) {

        return new R<>(purchaseOrderService.countPurchaseOrderNum(req));
    }

    /**
     * 查询今日采购订单总金额
     *
     * @return
     */
    @PostMapping("/queryTodayAmount")
    public R countPurchaseOrder(PurchaseTodayDTO dto) {
        return new R<>(purchaseOrderService.queryTodayAmount(dto));
    }

    /**
     * 查询已入库采购单
     *
     * @return
     */
    @GetMapping("/queryEnterPurchase")
    public R queryEnterPurchase(PurchaseTodayDTO dto) {
        return new R<>(purchaseOrderService.queryEnterPurchase(dto));
    }

    /**
     * 编辑采购单详情
     *
     * @param req
     * @return
     */
    @PostMapping("/editPurchaseOrder")
    public R editPurchaseOrder(@RequestBody @Validated EditPurchaseOrderReq req) {
        return new R<>(iScmPurchaseOrderService.editPurchaseOrder(req));
    }

    /**
     * 查询采购单及详情
     *
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseOrderAndDetail")
    public R queryPurchaseOrderDetail(@Validated OrderDetailReq req) {
        return new R<>(iScmPurchaseOrderService.queryPurchaseOrderAndDetail(req));
    }

    /**
     * 总监 执行采购任务(审核) 批量通过
     *
     * @param req
     * @return
     */
    @PostMapping("/checkTaskBatch")
    @ApiOperation(value = "执行采购任务(审核)——B端", notes = "总监 执行采购任务(审核) 修改采购单明细")
    public R checkTaskBatch(@RequestBody @Validated CheckTaskBatchReq req) {
        return new R<>(iScmPurchaseOrderService.checkTaskBatch(req));
    }

    /**
     * 查询当前任务列表(联查采购单明细)
     *
     * @param req
     * @return
     */
    @PostMapping("/fetchTaskList")
    @ApiOperation(value = "查询当前任务列表")
    public R<Page> fetchTaskList(@RequestBody @Validated PurchasePageReq req) {
        return new R(iScmPurchaseOrderService.fetchTaskList(req));
    }

    @GetMapping("/queryOrderList")
    @ApiOperation(value = "查询采购单列表")
    public R queryOrderList(Page page, QueryPurchasaeOrderListReq req){
        PurchaseOrder order = new PurchaseOrder();
        BeanUtil.copyProperties(req,order);
        QueryWrapper<PurchaseOrder> query = Wrappers.query(order);
        query.orderByDesc("purchase_sn");
        return new R(purchaseOrderService.page(page, query));
    }
}