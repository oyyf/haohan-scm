/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.req.MarketRecordReq;
import com.haohan.cloud.scm.purchase.service.MarketRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 市场行情价格记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/marketrecord" )
@Api(value = "marketrecord", tags = "marketrecord管理")
public class MarketRecordController {

    private final MarketRecordService marketRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param marketRecord 市场行情价格记录
     * @return
     */
    @GetMapping("/page" )
    public R getMarketRecordPage(Page page, MarketRecord marketRecord) {
        return new R<>(marketRecordService.page(page, Wrappers.query(marketRecord)));
    }


    /**
     * 通过id查询市场行情价格记录
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(marketRecordService.getById(id));
    }

    /**
     * 新增市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @SysLog("新增市场行情价格记录" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_add')" )
    public R save(@RequestBody MarketRecord marketRecord) {
        return new R<>(marketRecordService.save(marketRecord));
    }

    /**
     * 修改市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @SysLog("修改市场行情价格记录" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_edit')" )
    public R updateById(@RequestBody MarketRecord marketRecord) {
        return new R<>(marketRecordService.updateById(marketRecord));
    }

    /**
     * 通过id删除市场行情价格记录
     * @param id id
     * @return R
     */
    @SysLog("删除市场行情价格记录" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(marketRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除市场行情价格记录")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(marketRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询市场行情价格记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(marketRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询市场行情价格记录总记录}")
    @PostMapping("/countByMarketRecordReq")
    public R countByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq) {

        return new R<>(marketRecordService.count(Wrappers.query(marketRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据marketRecordReq查询一条货位信息表")
    @PostMapping("/getOneByMarketRecordReq")
    public R getOneByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq) {

        return new R<>(marketRecordService.getOne(Wrappers.query(marketRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param marketRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_lendingrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<MarketRecord> marketRecordList) {

        return new R<>(marketRecordService.saveOrUpdateBatch(marketRecordList));
    }


}
