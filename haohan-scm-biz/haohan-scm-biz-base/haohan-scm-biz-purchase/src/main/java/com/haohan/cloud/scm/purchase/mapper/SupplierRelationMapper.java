/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.purchase.entity.SupplierRelation;

/**
 * 采购员工管理供应商
 *
 * @author haohan
 * @date 2019-05-13 18:47:46
 */
public interface SupplierRelationMapper extends BaseMapper<SupplierRelation> {

}
