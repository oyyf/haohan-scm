package com.haohan.cloud.scm.purchase.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAddLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseQueryLendingResp;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseLendingService;
import com.haohan.cloud.scm.purchase.service.LendingRecordService;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import com.haohan.cloud.scm.purchase.utils.ScmPurchaseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author cx
 * @date 2019/5/24
 */
@Service
public class ScmPurchaseLendingServiceImpl implements IScmPurchaseLendingService {
    @Autowired
    @Lazy
    private LendingRecordService lendingRecordService;

    @Autowired
    @Lazy
    private PurchaseOrderDetailService purchaseOrderDetailService;

    @Autowired
    @Lazy
    private ScmPurchaseUtils scmPurchaseUtils;
    /**
     * 查询请款记录列表
     *
     * @param req
     * @return
     */
    @Override
    public Page queryLendingList(PurchaseQueryLendingListReq req) {
        scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUId());
        Page queryPage = new Page();
        queryPage.setSize(req.getSize());
        queryPage.setCurrent(req.getCurrent());
        LendingRecord record = new LendingRecord();
        BeanUtil.copyProperties(req,record);
        Page page = (Page)lendingRecordService.page(queryPage, Wrappers.query(record));
        if(page.getRecords().isEmpty()){
            throw new EmptyDataException();
        }
        return page;
    }
    /**
     * 新增请款记录
     *
     * @param req
     * @return
     */
    @Override
    public Boolean addLending(PurchaseAddLendingReq req) {
        LendingRecord record = new LendingRecord();
        BeanUtil.copyProperties(req,record);
        if(!lendingRecordService.save(record)){
            return false;
        }
        return true;
    }
    /**
     * 请款审核
     *
     * @param req
     * @return
     */
    @Override
    public Boolean auditLending(PurchaseAuditLendingReq req) {
        scmPurchaseUtils.fetchByUid(req.getPmId(),req.getId());
        QueryWrapper<LendingRecord> query = new QueryWrapper<>();
        query.lambda()
            .eq(LendingRecord::getPmId,req.getPmId())
            .eq(LendingRecord::getId,req.getId());
        LendingRecord record = lendingRecordService.getOne(query);
        if(null==record){
            throw new EmptyDataException();
        }
        BeanUtil.copyProperties(req,record);
        if(!lendingRecordService.updateById(record)){
            return false;
        }
        return true;
    }

    /**
     * 查询请款记录详情
     * @param req
     * @return
     */
    @Override
    public PurchaseQueryLendingResp queryLendingRecord(PurchaseQueryLendingReq req) {
        if(scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUId()) == null){
            throw new EmptyDataException();
        }
        LendingRecord queryRecord = new LendingRecord();
        queryRecord.setId(req.getLendingId());
        queryRecord.setPmId(req.getPmId());
        LendingRecord record = lendingRecordService.getOne(Wrappers.query(queryRecord));
        if(null == record) {
            throw new EmptyDataException();
        }
        PurchaseOrderDetail queryDetail = new PurchaseOrderDetail();
        queryDetail.setPurchaseDetailSn(record.getPurchaseDetailSn());
        queryDetail.setPmId(req.getPmId());
        PurchaseOrderDetail detail = purchaseOrderDetailService.getOne(Wrappers.query(queryDetail));
        if(null == detail){
            throw new EmptyDataException();
        }
        PurchaseQueryLendingResp resp = new PurchaseQueryLendingResp();
        BeanUtil.copyProperties(record,resp);
        resp.setDetail(detail);
        return resp;
    }
}
