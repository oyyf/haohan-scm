/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAccountReq;
import com.haohan.cloud.scm.purchase.service.PurchaseAccountService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购员工账户
 *
 * @author haohan
 * @date 2019-05-29 13:34:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseAccount")
@Api(value = "purchaseaccount", tags = "purchaseaccount内部接口服务")
public class PurchaseAccountFeignApiCtrl {

    private final PurchaseAccountService purchaseAccountService;


    /**
     * 通过id查询采购员工账户
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseAccountService.getById(id));
    }


    /**
     * 分页查询 采购员工账户 列表信息
     * @param purchaseAccountReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseAccountPage")
    public R getPurchaseAccountPage(@RequestBody PurchaseAccountReq purchaseAccountReq) {
        Page page = new Page(purchaseAccountReq.getPageNo(), purchaseAccountReq.getPageSize());
        PurchaseAccount purchaseAccount =new PurchaseAccount();
        BeanUtil.copyProperties(purchaseAccountReq, purchaseAccount);

        return new R<>(purchaseAccountService.page(page, Wrappers.query(purchaseAccount)));
    }


    /**
     * 全量查询 采购员工账户 列表信息
     * @param purchaseAccountReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseAccountList")
    public R getPurchaseAccountList(@RequestBody PurchaseAccountReq purchaseAccountReq) {
        PurchaseAccount purchaseAccount =new PurchaseAccount();
        BeanUtil.copyProperties(purchaseAccountReq, purchaseAccount);

        return new R<>(purchaseAccountService.list(Wrappers.query(purchaseAccount)));
    }


    /**
     * 新增采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @Inner
    @SysLog("新增采购员工账户")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseAccount purchaseAccount) {
        return new R<>(purchaseAccountService.save(purchaseAccount));
    }

    /**
     * 修改采购员工账户
     * @param purchaseAccount 采购员工账户
     * @return R
     */
    @Inner
    @SysLog("修改采购员工账户")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseAccount purchaseAccount) {
        return new R<>(purchaseAccountService.updateById(purchaseAccount));
    }

    /**
     * 通过id删除采购员工账户
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购员工账户")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseAccountService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购员工账户")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseAccountService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购员工账户")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseAccountService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购员工账户总记录}")
    @PostMapping("/countByPurchaseAccountReq")
    public R countByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq) {

        return new R<>(purchaseAccountService.count(Wrappers.query(purchaseAccountReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseAccountReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseAccountReq查询一条信息")
    @PostMapping("/getOneByPurchaseAccountReq")
    public R getOneByPurchaseAccountReq(@RequestBody PurchaseAccountReq purchaseAccountReq) {

        return new R<>(purchaseAccountService.getOne(Wrappers.query(purchaseAccountReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseAccountList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseAccount> purchaseAccountList) {

        return new R<>(purchaseAccountService.saveOrUpdateBatch(purchaseAccountList));
    }

}
