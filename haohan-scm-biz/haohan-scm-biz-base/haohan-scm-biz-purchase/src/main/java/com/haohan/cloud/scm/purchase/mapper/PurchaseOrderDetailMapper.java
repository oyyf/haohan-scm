/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;

/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-13 18:48:01
 */
public interface PurchaseOrderDetailMapper extends BaseMapper<PurchaseOrderDetail> {

}
