/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.api.purchase.req.AuditTaskReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseTaskReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseOrderService;
import com.haohan.cloud.scm.purchase.service.PurchaseTaskService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购任务
 *
 * @author haohan
 * @date 2019-05-29 13:35:16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseTask")
@Api(value = "purchasetask", tags = "purchasetask内部接口服务")
public class PurchaseTaskFeignApiCtrl {

    private final PurchaseTaskService purchaseTaskService;
    private final IScmPurchaseOrderService orderService;


    /**
     * 通过id查询采购任务
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseTaskService.getById(id));
    }


    /**
     * 分页查询 采购任务 列表信息
     * @param purchaseTaskReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseTaskPage")
    public R getPurchaseTaskPage(@RequestBody PurchaseTaskReq purchaseTaskReq) {
        Page page = new Page(purchaseTaskReq.getPageNo(), purchaseTaskReq.getPageSize());
        PurchaseTask purchaseTask =new PurchaseTask();
        BeanUtil.copyProperties(purchaseTaskReq, purchaseTask);

        return new R<>(purchaseTaskService.page(page, Wrappers.query(purchaseTask)));
    }


    /**
     * 全量查询 采购任务 列表信息
     * @param purchaseTaskReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseTaskList")
    public R getPurchaseTaskList(@RequestBody PurchaseTaskReq purchaseTaskReq) {
        PurchaseTask purchaseTask =new PurchaseTask();
        BeanUtil.copyProperties(purchaseTaskReq, purchaseTask);

        return new R<>(purchaseTaskService.list(Wrappers.query(purchaseTask)));
    }


    /**
     * 新增采购任务
     * @param purchaseTask 采购任务
     * @return R
     */
    @Inner
    @SysLog("新增采购任务")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseTask purchaseTask) {
        return new R<>(purchaseTaskService.save(purchaseTask));
    }

    /**
     * 修改采购任务
     * @param purchaseTask 采购任务
     * @return R
     */
    @Inner
    @SysLog("修改采购任务")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseTask purchaseTask) {
        return new R<>(purchaseTaskService.updateById(purchaseTask));
    }

    /**
     * 通过id删除采购任务
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购任务")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseTaskService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购任务")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseTaskService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购任务")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseTaskService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseTaskReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购任务总记录}")
    @PostMapping("/countByPurchaseTaskReq")
    public R countByPurchaseTaskReq(@RequestBody PurchaseTaskReq purchaseTaskReq) {

        return new R<>(purchaseTaskService.count(Wrappers.query(purchaseTaskReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseTaskReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseTaskReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseTaskReq")
    public R getOneByPurchaseTaskReq(@RequestBody PurchaseTaskReq purchaseTaskReq) {

        return new R<>(purchaseTaskService.getOne(Wrappers.query(purchaseTaskReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseTaskList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseTask> purchaseTaskList) {

        return new R<>(purchaseTaskService.saveOrUpdateBatch(purchaseTaskList));
    }

    /**
     * 总监 执行采购任务(审核) 修改采购单明细
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/auditTask")
    @SysLog("总监执行采购任务(单个审核)——小程序")
    public R auditTask(@RequestBody AuditTaskReq req) {
        return new R<>(orderService.auditTask(req));
    }

}
