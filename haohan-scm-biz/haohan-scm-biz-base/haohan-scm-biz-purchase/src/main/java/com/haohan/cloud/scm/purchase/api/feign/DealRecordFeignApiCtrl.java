/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.DealRecord;
import com.haohan.cloud.scm.api.purchase.req.DealRecordReq;
import com.haohan.cloud.scm.purchase.service.DealRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 账户扣减记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/DealRecord")
@Api(value = "dealrecord", tags = "dealrecord内部接口服务")
public class DealRecordFeignApiCtrl {

    private final DealRecordService dealRecordService;


    /**
     * 通过id查询账户扣减记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(dealRecordService.getById(id));
    }


    /**
     * 分页查询 账户扣减记录 列表信息
     * @param dealRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchDealRecordPage")
    public R getDealRecordPage(@RequestBody DealRecordReq dealRecordReq) {
        Page page = new Page(dealRecordReq.getPageNo(), dealRecordReq.getPageSize());
        DealRecord dealRecord =new DealRecord();
        BeanUtil.copyProperties(dealRecordReq, dealRecord);

        return new R<>(dealRecordService.page(page, Wrappers.query(dealRecord)));
    }


    /**
     * 全量查询 账户扣减记录 列表信息
     * @param dealRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchDealRecordList")
    public R getDealRecordList(@RequestBody DealRecordReq dealRecordReq) {
        DealRecord dealRecord =new DealRecord();
        BeanUtil.copyProperties(dealRecordReq, dealRecord);

        return new R<>(dealRecordService.list(Wrappers.query(dealRecord)));
    }


    /**
     * 新增账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @Inner
    @SysLog("新增账户扣减记录")
    @PostMapping("/add")
    public R save(@RequestBody DealRecord dealRecord) {
        return new R<>(dealRecordService.save(dealRecord));
    }

    /**
     * 修改账户扣减记录
     * @param dealRecord 账户扣减记录
     * @return R
     */
    @Inner
    @SysLog("修改账户扣减记录")
    @PostMapping("/update")
    public R updateById(@RequestBody DealRecord dealRecord) {
        return new R<>(dealRecordService.updateById(dealRecord));
    }

    /**
     * 通过id删除账户扣减记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除账户扣减记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(dealRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除账户扣减记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(dealRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询账户扣减记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(dealRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询账户扣减记录总记录}")
    @PostMapping("/countByDealRecordReq")
    public R countByDealRecordReq(@RequestBody DealRecordReq dealRecordReq) {

        return new R<>(dealRecordService.count(Wrappers.query(dealRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param dealRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据dealRecordReq查询一条货位信息表")
    @PostMapping("/getOneByDealRecordReq")
    public R getOneByDealRecordReq(@RequestBody DealRecordReq dealRecordReq) {

        return new R<>(dealRecordService.getOne(Wrappers.query(dealRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param dealRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<DealRecord> dealRecordList) {

        return new R<>(dealRecordService.saveOrUpdateBatch(dealRecordList));
    }

}
