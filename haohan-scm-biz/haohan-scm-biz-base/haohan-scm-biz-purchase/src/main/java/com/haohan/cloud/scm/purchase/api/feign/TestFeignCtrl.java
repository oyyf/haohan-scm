package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.framework.entity.BaseResp;
import com.haohan.cloud.scm.api.common.BaseApiService;
import com.haohan.cloud.scm.api.common.CommonApiConstant;
import com.haohan.cloud.scm.api.common.entity.ApiRespPage;
import com.haohan.cloud.scm.api.common.req.PdsBuyerGoodsReq;
import com.haohan.cloud.scm.api.common.retail.GoodsInfoExt;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.PurchaseEmployeeReq;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zgw on 2019/5/22.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/test")
@Api(value = "Test", tags = "apiTest")
public class TestFeignCtrl {

  @Autowired
  private BaseApiService apiService;

  private final PurchaseEmployeeService purchaseEmployeeService;


  /**
   * 通过id查询采购员工管理
   *
   * @param id id
   * @return R
   */
  @Inner
  @GetMapping("/{id}")
  public R getById(@PathVariable("id") String id) {
    return new R<>(purchaseEmployeeService.getById(id));
  }

  @Inner
  @PostMapping("/list")
  @ResponseBody
  public R callTest(@RequestBody PdsBuyerGoodsReq goodsReq){
    BaseResp baseResp =  apiService.call(CommonApiConstant.goodsList,goodsReq);
    ApiRespPage dataResp =  JSONUtil.toBean(baseResp.getExt().toString(),ApiRespPage.class);
    List<GoodsInfoExt> list = dataResp.getList();

   return  new R(dataResp);

  }


  @Inner
  @PostMapping("/page2")
  public R getPage(@RequestBody PurchaseEmployeeReq purchaseEmployee) {
    Page page = new Page(purchaseEmployee.getPageNo(), purchaseEmployee.getPageSize());
    PurchaseEmployee employee = new PurchaseEmployee();
    BeanUtil.copyProperties(purchaseEmployee, employee);

    return new R<>(purchaseEmployeeService.page(page, Wrappers.query(employee)));
  }

    @PostMapping("/page3")
    public R getPage3(@RequestBody PurchaseEmployeeReq purchaseEmployee) {
        Page page = new Page(purchaseEmployee.getPageNo(), purchaseEmployee.getPageSize());
        PurchaseEmployee employee = new PurchaseEmployee();
        BeanUtil.copyProperties(purchaseEmployee, employee);

        return new R<>(purchaseEmployeeService.page(page, Wrappers.query(employee)));
    }

}
