package com.haohan.cloud.scm.purchase.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseEmployeeDetailResp;
import com.pig4cloud.pigx.common.core.util.R;

/**
 * 采购员工 相关信息
 *
 * @author dy
 * @date 2019/5/16
 */
public interface IScmPurchaseEmployeeInfoService {


    /**
     * 查询采购员工详情
     *
     * @param req
     * @return
     */
    PurchaseEmployeeDetailResp queryEmployeeDetail(PurchaseEmployeeDetailReq req);

    /**
     * 修改采购员工信息
     *
     * @param req
     * @return
     */
    Boolean modifyEmployeeDetail(PurchaseModifyEmployeeReq req);

    /**
     * 查询采购员工列表 管理商品
     *
     * @param req
     * @return
     */
    Page queryEmployeeGoodsList(PurchaseEmployeeGoodsReq req);

    /**
     * 查询采购员工列表 管理供应商
     *
     * @param req
     * @return
     */
    Page queryEmployeeSupplierList(PurchaseEmployeeSupplierReq req);

    /**
     * 查询采购员工 资金扣减记录
     *
     * @param req
     * @return
     */
    Page queryEmployeeBillList(PurchaseEmployeeBillReq req);

    /**
     * 新增采购员工
     * @param purchaseEmployee
     *          必需参数: telephone/ name
     * @return
     */
    R<PurchaseEmployee> addPurchaseEmployee(PurchaseEmployee purchaseEmployee);

     /**
     * 采购员工 绑定系统用户 根据手机号
     * @param telephone 手机号
     * @return data返回PurchaseEmployee
     */
     R<PurchaseEmployee> relationEmployeeUser(String telephone);


    /**
     * 采购员工新增供应商
     * @param req
     * @return
     */
    Boolean addSupplier(BuyerAddSupplierReq req);

}
