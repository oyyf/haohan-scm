/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.LendingRecord;
import com.haohan.cloud.scm.api.purchase.req.LendingRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseQueryLendingResp;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseLendingService;
import com.haohan.cloud.scm.purchase.service.LendingRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 放款申请记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:06
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/LendingRecord")
@Api(value = "lendingrecord", tags = "lendingrecord内部接口服务")
public class LendingRecordFeignApiCtrl {

    private final LendingRecordService lendingRecordService;

    private final IScmPurchaseLendingService lendingService;

    /**
     * 通过id查询放款申请记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(lendingRecordService.getById(id));
    }


    /**
     * 分页查询 放款申请记录 列表信息
     * @param lendingRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchLendingRecordPage")
    public R getLendingRecordPage(@RequestBody LendingRecordReq lendingRecordReq) {
        Page page = new Page(lendingRecordReq.getPageNo(), lendingRecordReq.getPageSize());
        LendingRecord lendingRecord =new LendingRecord();
        BeanUtil.copyProperties(lendingRecordReq, lendingRecord);

        return new R<>(lendingRecordService.page(page, Wrappers.query(lendingRecord)));
    }


    /**
     * 全量查询 放款申请记录 列表信息
     * @param lendingRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchLendingRecordList")
    public R getLendingRecordList(@RequestBody LendingRecordReq lendingRecordReq) {
        LendingRecord lendingRecord =new LendingRecord();
        BeanUtil.copyProperties(lendingRecordReq, lendingRecord);

        return new R<>(lendingRecordService.list(Wrappers.query(lendingRecord)));
    }


    /**
     * 新增放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @Inner
    @SysLog("新增放款申请记录")
    @PostMapping("/add")
    public R save(@RequestBody LendingRecord lendingRecord) {
        return new R<>(lendingRecordService.save(lendingRecord));
    }

    /**
     * 修改放款申请记录
     * @param lendingRecord 放款申请记录
     * @return R
     */
    @Inner
    @SysLog("修改放款申请记录")
    @PostMapping("/update")
    public R updateById(@RequestBody LendingRecord lendingRecord) {
        return new R<>(lendingRecordService.updateById(lendingRecord));
    }

    /**
     * 通过id删除放款申请记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除放款申请记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(lendingRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除放款申请记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(lendingRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询放款申请记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(lendingRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询放款申请记录总记录}")
    @PostMapping("/countByLendingRecordReq")
    public R countByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq) {

        return new R<>(lendingRecordService.count(Wrappers.query(lendingRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param lendingRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据lendingRecordReq查询一条货位信息表")
    @PostMapping("/getOneByLendingRecordReq")
    public R getOneByLendingRecordReq(@RequestBody LendingRecordReq lendingRecordReq) {

        return new R<>(lendingRecordService.getOne(Wrappers.query(lendingRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param lendingRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<LendingRecord> lendingRecordList) {
        return new R<>(lendingRecordService.saveOrUpdateBatch(lendingRecordList));
    }

    @Inner
    @PostMapping("/queryLendingList")
    @ApiOperation(value = "请款记录列表",notes = "请款记录列表——小程序")
    public R<Page> queryLendingList(@RequestBody PurchaseQueryLendingListReq req) {
        return new R(lendingService.queryLendingList(req));
    }

    @Inner
    @PostMapping("/queryLendingRecord")
    @ApiOperation(value = "查询请款记录详情",notes = "查询请款记录详情——小程序")
    public R<PurchaseQueryLendingResp> queryLendingRecord(@RequestBody PurchaseQueryLendingReq req){
        return new R(lendingService.queryLendingRecord(req));
    }

    @Inner
    @PostMapping("/auditLending")
    @ApiModelProperty(value = "请款审核", notes = "审核请款——小程序")
    public R<Boolean> auditLending(@RequestBody PurchaseAuditLendingReq req) {
            return new R(lendingService.auditLending(req));
    }

}
