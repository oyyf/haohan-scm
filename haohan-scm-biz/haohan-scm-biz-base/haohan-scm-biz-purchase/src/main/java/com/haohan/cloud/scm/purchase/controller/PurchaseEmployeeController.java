/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.PurchaseEmployeeReq;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购员工管理
 *
 * @author haohan
 * @date 2019-05-29 13:34:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/purchaseemployee" )
@Api(value = "purchaseemployee", tags = "purchaseemployee管理")
public class PurchaseEmployeeController {

    private final PurchaseEmployeeService purchaseEmployeeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param purchaseEmployee 采购员工管理
     * @return
     */
    @GetMapping("/page" )
    public R getPurchaseEmployeePage(Page page, PurchaseEmployee purchaseEmployee) {
        // 模糊查询
        String name = purchaseEmployee.getName();
        String telephone = purchaseEmployee.getTelephone();
        purchaseEmployee.setName(null);
        purchaseEmployee.setTelephone(null);
        QueryWrapper<PurchaseEmployee> queryWrapper = new QueryWrapper<>(purchaseEmployee);
        queryWrapper.lambda()
                .like(StrUtil.isNotEmpty(name), PurchaseEmployee::getName, name)
                .like(StrUtil.isNotEmpty(telephone), PurchaseEmployee::getTelephone, telephone);
        return new R<>(purchaseEmployeeService.page(page, queryWrapper));
    }


    /**
     * 通过id查询采购员工管理
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(purchaseEmployeeService.getById(id));
    }

    /**
     * 新增采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @SysLog("新增采购员工管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseemployee_add')" )
    public R save(@RequestBody PurchaseEmployee purchaseEmployee) {
        return new R<>(purchaseEmployeeService.save(purchaseEmployee));
    }

    /**
     * 修改采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @SysLog("修改采购员工管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchaseemployee_edit')" )
    public R updateById(@RequestBody PurchaseEmployee purchaseEmployee) {
        return new R<>(purchaseEmployeeService.updateById(purchaseEmployee));
    }

    /**
     * 通过id删除采购员工管理
     * @param id id
     * @return R
     */
    @SysLog("删除采购员工管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_purchaseemployee_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseEmployeeService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购员工管理")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseemployee_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购员工管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购员工管理总记录}")
    @PostMapping("/countByPurchaseEmployeeReq")
    public R countByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {

        return new R<>(purchaseEmployeeService.count(Wrappers.query(purchaseEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据purchaseEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseEmployeeReq")
    public R getOneByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {

        return new R<>(purchaseEmployeeService.getOne(Wrappers.query(purchaseEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_purchaseemployee_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PurchaseEmployee> purchaseEmployeeList) {

        return new R<>(purchaseEmployeeService.saveOrUpdateBatch(purchaseEmployeeList));
    }


}
