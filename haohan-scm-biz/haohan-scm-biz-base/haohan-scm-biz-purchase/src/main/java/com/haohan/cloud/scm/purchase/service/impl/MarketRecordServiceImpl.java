/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.purchase.mapper.MarketRecordMapper;
import com.haohan.cloud.scm.purchase.service.MarketRecordService;
import org.springframework.stereotype.Service;

/**
 * 市场行情价格记录
 *
 * @author haohan
 * @date 2019-05-13 18:48:22
 */
@Service
public class MarketRecordServiceImpl extends ServiceImpl<MarketRecordMapper, MarketRecord> implements MarketRecordService {

}
