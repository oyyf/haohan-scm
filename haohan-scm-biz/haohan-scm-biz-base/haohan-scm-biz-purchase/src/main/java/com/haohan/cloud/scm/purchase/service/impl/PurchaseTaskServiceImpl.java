/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;
import com.haohan.cloud.scm.purchase.mapper.PurchaseTaskMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseTaskService;
import org.springframework.stereotype.Service;

/**
 * 采购任务
 *
 * @author haohan
 * @date 2019-05-13 18:52:46
 */
@Service
public class PurchaseTaskServiceImpl extends ServiceImpl<PurchaseTaskMapper, PurchaseTask> implements PurchaseTaskService {

}
