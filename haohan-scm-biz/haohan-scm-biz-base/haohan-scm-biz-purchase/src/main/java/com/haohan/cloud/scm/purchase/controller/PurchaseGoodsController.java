/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;
import com.haohan.cloud.scm.api.purchase.req.PurchaseGoodsReq;
import com.haohan.cloud.scm.purchase.service.PurchaseGoodsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 采购员工管理商品
 *
 * @author haohan
 * @date 2019-05-29 13:34:42
 */
@RestController
@AllArgsConstructor
@RequestMapping("/purchasegoods" )
@Api(value = "purchasegoods", tags = "purchasegoods管理")
public class PurchaseGoodsController {

    private final PurchaseGoodsService purchaseGoodsService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param purchaseGoods 采购员工管理商品
     * @return
     */
    @GetMapping("/page" )
    public R getPurchaseGoodsPage(Page page, PurchaseGoods purchaseGoods) {
        return new R<>(purchaseGoodsService.page(page, Wrappers.query(purchaseGoods)));
    }


    /**
     * 通过id查询采购员工管理商品
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(purchaseGoodsService.getById(id));
    }

    /**
     * 新增采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @SysLog("新增采购员工管理商品" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchasegoods_add')" )
    public R save(@RequestBody PurchaseGoods purchaseGoods) {
        return new R<>(purchaseGoodsService.save(purchaseGoods));
    }

    /**
     * 修改采购员工管理商品
     * @param purchaseGoods 采购员工管理商品
     * @return R
     */
    @SysLog("修改采购员工管理商品" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('purchase_purchasegoods_edit')" )
    public R updateById(@RequestBody PurchaseGoods purchaseGoods) {
        return new R<>(purchaseGoodsService.updateById(purchaseGoods));
    }

    /**
     * 通过id删除采购员工管理商品
     * @param id id
     * @return R
     */
    @SysLog("删除采购员工管理商品" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('purchase_purchasegoods_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseGoodsService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除采购员工管理商品")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('purchase_purchasegoods_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseGoodsService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询采购员工管理商品")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseGoodsService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询采购员工管理商品总记录}")
    @PostMapping("/countByPurchaseGoodsReq")
    public R countByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {

        return new R<>(purchaseGoodsService.count(Wrappers.query(purchaseGoodsReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseGoodsReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据purchaseGoodsReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseGoodsReq")
    public R getOneByPurchaseGoodsReq(@RequestBody PurchaseGoodsReq purchaseGoodsReq) {

        return new R<>(purchaseGoodsService.getOne(Wrappers.query(purchaseGoodsReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseGoodsList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('purchase_purchasegoods_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<PurchaseGoods> purchaseGoodsList) {

        return new R<>(purchaseGoodsService.saveOrUpdateBatch(purchaseGoodsList));
    }


}
