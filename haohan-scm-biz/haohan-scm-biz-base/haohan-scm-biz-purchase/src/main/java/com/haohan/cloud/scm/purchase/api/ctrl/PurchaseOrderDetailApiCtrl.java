package com.haohan.cloud.scm.purchase.api.ctrl;

import com.haohan.cloud.scm.api.purchase.req.ConfirmGoodsReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/8/8
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/purchase/purchaseOrderDetail")
@Api(value = "ApiPurchaseOrderDetail", tags = "purchaseOrderDetail采购明细api")
public class PurchaseOrderDetailApiCtrl {

    private IScmPurchaseDetailService iScmPurchaseDetailService;

    /**
     * 揽货确认
     *
     * @param req
     * @return
     */
    @PostMapping("/confirmGoods")
    @ApiOperation(value = "揽货确认", notes = "揽货确认,采购任务")
    public R<Boolean> confirmGoods(@Validated ConfirmGoodsReq req) {
        return R.ok(iScmPurchaseDetailService.confirmGoods(req));
    }


}
