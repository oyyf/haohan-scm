package com.haohan.cloud.scm.purchase.core.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.entity.GoodsCategory;
import com.haohan.cloud.scm.api.goods.feign.GoodsCategoryFeignService;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.req.MarketRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAnswerPurchaseReportReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyMarketPriceReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyPurchaseReportReq;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseMarketReportService;
import com.haohan.cloud.scm.purchase.service.MarketRecordService;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import com.haohan.cloud.scm.purchase.service.PurchaseReportService;
import com.pig4cloud.pigx.common.core.constant.SecurityConstants;
import com.pig4cloud.pigx.common.core.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author cx
 * @date 2019/5/24
 */
@Service
public class ScmPurchaseMarketReportServiceImpl implements IScmPurchaseMarketReportService {

    @Autowired
    @Lazy
    private MarketRecordService marketRecordService;

    @Autowired
    @Lazy
    private PurchaseReportService purchaseReportService;

    @Autowired
    @Lazy
    private GoodsCategoryFeignService goodsCategoryFeignService;

    @Autowired
    @Lazy
    private PurchaseEmployeeService purchaseEmployeeService;
    /**
     * 修改市场行情
     *
     * @param req
     * @return
     */
    @Override
    public Boolean modifyMarketPrice(PurchaseModifyMarketPriceReq req) {
        MarketRecord record = new MarketRecord();
        record.setId(req.getId());
        record.setPmId(req.getPmId());
        if(null ==marketRecordService.getOne(Wrappers.query(record))){
          throw new EmptyDataException();
        }
        BeanUtil.copyProperties(req,record);
        if(!marketRecordService.updateById(record)){
          return false;
        }
      return true;
    }
    /**
     * 修改 采购汇报
     *
     * @param req
     * @return
     */
    @Override
    public Boolean modifyPurchaseReport(PurchaseModifyPurchaseReportReq req) {
        PurchaseReport report = new PurchaseReport();
        report.setPmId(req.getPmId());
        report.setId(req.getId());
        if(null == purchaseReportService.getOne(Wrappers.query(report))){
          throw new EmptyDataException();
        }
        BeanUtil.copyProperties(req,report);
        if(!purchaseReportService.updateById(report)){
          return false;
        }
        return true;
    }
    /**
     * 回复采购汇报
     *
     * @param req
     * @return
     */
    @Override
    public Boolean answerPurchaseReport(PurchaseAnswerPurchaseReportReq req) {
        PurchaseReport report = new PurchaseReport();
        report.setId(req.getId());
        report.setPmId(req.getPmId());
        if(null == purchaseReportService.getOne(Wrappers.query(report))){
          throw new EmptyDataException();
        }
        BeanUtil.copyProperties(req,report);
        if(!purchaseReportService.updateById(report)){
          return false;
        }
        return true;
    }

    /**
     * 查询市场行情列表
     * @param page
     * @param marketRecord
     * @return
     */
    @Override
    public IPage queryMarketRecordsList(Page page, MarketRecord marketRecord) {
        IPage pList = marketRecordService.page(page, Wrappers.query(marketRecord));
        if(!pList.getRecords().isEmpty()){
            List<MarketRecordReq> records = new ArrayList<>();
            pList.getRecords().forEach(r -> {
                MarketRecord record = BeanUtil.toBean(r, MarketRecord.class);
                R queryCate = goodsCategoryFeignService.getById(record.getGoodsCategoryId(), SecurityConstants.FROM_IN);
                if (!RUtil.isSuccess(queryCate) || ObjectUtil.isNull(queryCate.getData())) {
                    throw new EmptyDataException();
                }
                GoodsCategory category = BeanUtil.toBean(queryCate.getData(), GoodsCategory.class);
                PurchaseEmployee em = purchaseEmployeeService.getById(record.getInitiatorId());
                if(ObjectUtil.isNull(em)){
                    throw new EmptyDataException();
                }
                MarketRecordReq req = new MarketRecordReq();
                BeanUtil.copyProperties(record,req);
                req.setGoodsCategoryName(category.getName());
                req.setTransactorName(em.getName());
                records.add(req);
            });
            pList.setRecords(records);
        }
        return pList;
    }
}
