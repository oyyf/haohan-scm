/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.SupplierRelation;
import com.haohan.cloud.scm.api.purchase.req.SupplierRelationReq;
import com.haohan.cloud.scm.purchase.service.SupplierRelationService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购员工管理供应商
 *
 * @author haohan
 * @date 2019-05-29 13:34:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplierRelation")
@Api(value = "supplierrelation", tags = "supplierrelation内部接口服务")
public class SupplierRelationFeignApiCtrl {

    private final SupplierRelationService supplierRelationService;


    /**
     * 通过id查询采购员工管理供应商
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierRelationService.getById(id));
    }


    /**
     * 分页查询 采购员工管理供应商 列表信息
     * @param supplierRelationReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierRelationPage")
    public R getSupplierRelationPage(@RequestBody SupplierRelationReq supplierRelationReq) {
        Page page = new Page(supplierRelationReq.getPageNo(), supplierRelationReq.getPageSize());
        SupplierRelation supplierRelation =new SupplierRelation();
        BeanUtil.copyProperties(supplierRelationReq, supplierRelation);

        return new R<>(supplierRelationService.page(page, Wrappers.query(supplierRelation)));
    }


    /**
     * 全量查询 采购员工管理供应商 列表信息
     * @param supplierRelationReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierRelationList")
    public R getSupplierRelationList(@RequestBody SupplierRelationReq supplierRelationReq) {
        SupplierRelation supplierRelation =new SupplierRelation();
        BeanUtil.copyProperties(supplierRelationReq, supplierRelation);

        return new R<>(supplierRelationService.list(Wrappers.query(supplierRelation)));
    }


    /**
     * 新增采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @Inner
    @SysLog("新增采购员工管理供应商")
    @PostMapping("/add")
    public R save(@RequestBody SupplierRelation supplierRelation) {
        return new R<>(supplierRelationService.save(supplierRelation));
    }

    /**
     * 修改采购员工管理供应商
     * @param supplierRelation 采购员工管理供应商
     * @return R
     */
    @Inner
    @SysLog("修改采购员工管理供应商")
    @PostMapping("/update")
    public R updateById(@RequestBody SupplierRelation supplierRelation) {
        return new R<>(supplierRelationService.updateById(supplierRelation));
    }

    /**
     * 通过id删除采购员工管理供应商
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购员工管理供应商")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierRelationService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购员工管理供应商")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierRelationService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购员工管理供应商")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierRelationService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购员工管理供应商总记录}")
    @PostMapping("/countBySupplierRelationReq")
    public R countBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq) {

        return new R<>(supplierRelationService.count(Wrappers.query(supplierRelationReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierRelationReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierRelationReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierRelationReq")
    public R getOneBySupplierRelationReq(@RequestBody SupplierRelationReq supplierRelationReq) {

        return new R<>(supplierRelationService.getOne(Wrappers.query(supplierRelationReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierRelationList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SupplierRelation> supplierRelationList) {

        return new R<>(supplierRelationService.saveOrUpdateBatch(supplierRelationList));
    }

}
