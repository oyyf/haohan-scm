/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseGoods;

/**
 * 采购员工管理商品
 *
 * @author haohan
 * @date 2019-05-13 19:06:17
 */
public interface PurchaseGoodsService extends IService<PurchaseGoods> {

}
