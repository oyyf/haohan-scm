package com.haohan.cloud.scm.purchase.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseMarketReportService;
import com.haohan.cloud.scm.purchase.service.MarketRecordService;
import com.haohan.cloud.scm.purchase.service.PurchaseReportService;
import com.haohan.cloud.scm.purchase.utils.ScmPurchaseUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/5/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/purchaseMarketReport")
@Api(value = "ApiPurchaseMarketReport", tags = "purchaseMarketReport采购汇报/行情api")
public class PurchaseMarketReportApiCtrl {

    private final IScmPurchaseMarketReportService marketReportService;
    private final MarketRecordService marketRecordService;
    private final PurchaseReportService purchaseReportService;
    private final ScmPurchaseUtils scmPurchaseUtils;

    /**
     * 查询市场行情列表
     * @param req
     * @return
     */
    @GetMapping("/queryMarketPriceList")
    @ApiOperation(value = "市场行情列表" , notes = "按分类分页查询市场行情列表—小程序")
    public R<Page<MarketRecord>> queryMarketPriceList(@Validated PurchaseQueryMarketPriceListReq req) {
        scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUid());
        Page<MarketRecord> page = new Page<>();
        page.setSize(req.getSize());
        page.setCurrent(req.getCurrent());
        MarketRecord record = new MarketRecord();
        record.setPmId(req.getPmId());
        record.setGoodsCategoryId(req.getGoodsCategoryId());
        page = (Page<MarketRecord>) marketRecordService.page(page, Wrappers.query(record));
        return new R(page);
    }
    /**
     * 查询市场行情详情
     * @param req
     * @return
     */
    @GetMapping("/queryMarketPrice")
    @ApiOperation(value = "市场行情详情" , notes = "查询市场行情详情—小程序")
    public R<MarketRecord> queryMarketPrice(@Validated PurchaseQueryMarketPriceReq req) {
        scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUId());
        MarketRecord record = new MarketRecord();
        record.setPmId(req.getPmId());
        record.setId(req.getId());
        return new R(marketRecordService.getOne(Wrappers.query(record)));
    }

    /**
     * 增加市场行情
     * @param req
     * @return
     */
    @PostMapping("/addMarketPrice")
    @ApiOperation(value = "增加市场行情" , notes = "增加市场行情详情—小程序")
    public R<Boolean> addMarketPrice(@Validated PurchaseAddMarketPriceReq req) {
        if(StrUtil.isNotBlank(req.getUId())){
            scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUId());
        }
        MarketRecord record = new MarketRecord();
        BeanUtil.copyProperties(req,record);
        return new R(marketRecordService.save(record));

    }
    /**
     * 修改市场行情
     *
     * @param req
     * @return
     */
    @PostMapping("/modifyMarketPrice")
    @ApiOperation(value = "修改市场行情详情" , notes = "修改市场行情详情—小程序")
    public R<Boolean> modifyMarketPrice(@Validated PurchaseModifyMarketPriceReq req) {
        return new R(marketReportService.modifyMarketPrice(req));
    }

    /**
     * 查询采购汇报列表
     *
     * @param req
     * @return
     */
    @GetMapping("/queryPurchaseReportList")
    @ApiModelProperty(value = "采购汇报列表",notes = "查询采购汇报列表——小程序")
    public R<Page<PurchaseReport>> queryPurchaseReportList(@Validated PurchaseQueryPurchaseReportListReq req) {
        scmPurchaseUtils.fetchByUid(req.getPmId(),req.getUId());
        Page<PurchaseReport> page = new Page<>();
        page.setSize(req.getSize());
        page.setCurrent(req.getCurrent());
        PurchaseReport report = new PurchaseReport();
        BeanUtil.copyProperties(req,report);
        page = (Page<PurchaseReport>)purchaseReportService.page(page,Wrappers.query(report));
        return new R(page);
    }
    /**
     * 新增 采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/addPurchaseReport")
    public R<Boolean> addPurchaseReport(@Validated PurchaseAddPurchaseReportReq req) {
        PurchaseReport report = new PurchaseReport();
        BeanUtil.copyProperties(req,report);
        return new R(purchaseReportService.save(report));
    }
    /**
     * 修改 采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/modifyPurchaseReport")
    public R<Boolean> modifyPurchaseReport(@Validated PurchaseModifyPurchaseReportReq req) {
        return new R(marketReportService.modifyPurchaseReport(req));
    }
    /**
     * 回复采购汇报
     *
     * @param req
     * @return
     */
    @PostMapping("/answerPurchaseReport")
    @ApiOperation(value = "回复采购汇报",notes = "回复采购汇报——小程序")
    public R<Boolean> answerPurchaseReport(@Validated PurchaseAnswerPurchaseReportReq req) {
        return new R(marketReportService.answerPurchaseReport(req));
    }

    @GetMapping("/queryMarketRecords")
    @ApiOperation(value = "查询市场行情列表")
    public R queryMarketRecords(Page page, MarketRecord marketRecord){
        return new R(marketReportService.queryMarketRecordsList(page,marketRecord));
    }
}


