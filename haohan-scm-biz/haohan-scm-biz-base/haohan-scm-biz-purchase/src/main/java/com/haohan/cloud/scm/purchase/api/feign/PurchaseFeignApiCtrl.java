package com.haohan.cloud.scm.purchase.api.feign;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.PurchaseOrderDetailReq;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @date 2019/5/27
 */
@Data
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/purchase")
@Api(value = "ApiPurchaseOperation", tags = "purchaseOperation采购管理操作")
public class PurchaseFeignApiCtrl {

    @Autowired
    private PurchaseOrderDetailService purchaseOrderDetailService;

    /**
     * 根据id查询采购部采购单明细
     * @param req
     * @return
     */
    @Inner
    @PostMapping("purchaseOrderDetail/queryOne")
    public R queryOne(@RequestBody PurchaseOrderDetailReq req) {
        PurchaseOrderDetail orderDetail = new PurchaseOrderDetail();
        orderDetail.setId(req.getId());
        orderDetail.setPurchaseDetailSn(req.getPurchaseDetailSn());
        return new R<>(purchaseOrderDetailService.getOne(new QueryWrapper<PurchaseOrderDetail>(orderDetail)));
    }
}
