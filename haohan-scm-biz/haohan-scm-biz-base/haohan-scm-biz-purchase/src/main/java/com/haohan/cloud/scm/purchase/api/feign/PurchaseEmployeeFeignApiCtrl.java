/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;
import com.haohan.cloud.scm.api.purchase.req.BuyerAddSupplierReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseEmployeeReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseEmployeeInfoService;
import com.haohan.cloud.scm.purchase.service.PurchaseEmployeeService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 采购员工管理
 *
 * @author haohan
 * @date 2019-05-29 13:34:34
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/PurchaseEmployee")
@Api(value = "purchaseemployee", tags = "purchaseemployee内部接口服务")
public class PurchaseEmployeeFeignApiCtrl {

    private final PurchaseEmployeeService purchaseEmployeeService;
    private final IScmPurchaseEmployeeInfoService employeeInfoService;


    /**
     * 通过id查询采购员工管理
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(purchaseEmployeeService.getById(id));
    }


    /**
     * 分页查询 采购员工管理 列表信息
     * @param purchaseEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseEmployeePage")
    public R getPurchaseEmployeePage(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {
        Page page = new Page(purchaseEmployeeReq.getPageNo(), purchaseEmployeeReq.getPageSize());
        PurchaseEmployee purchaseEmployee =new PurchaseEmployee();
        BeanUtil.copyProperties(purchaseEmployeeReq, purchaseEmployee);

        return new R<>(purchaseEmployeeService.page(page, Wrappers.query(purchaseEmployee)));
    }


    /**
     * 全量查询 采购员工管理 列表信息
     * @param purchaseEmployeeReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchPurchaseEmployeeList")
    public R getPurchaseEmployeeList(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {
        PurchaseEmployee purchaseEmployee =new PurchaseEmployee();
        BeanUtil.copyProperties(purchaseEmployeeReq, purchaseEmployee);

        return new R<>(purchaseEmployeeService.list(Wrappers.query(purchaseEmployee)));
    }


    /**
     * 新增采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @Inner
    @SysLog("新增采购员工管理")
    @PostMapping("/add")
    public R save(@RequestBody PurchaseEmployee purchaseEmployee) {
        return new R<>(purchaseEmployeeService.save(purchaseEmployee));
    }

    /**
     * 修改采购员工管理
     * @param purchaseEmployee 采购员工管理
     * @return R
     */
    @Inner
    @SysLog("修改采购员工管理")
    @PostMapping("/update")
    public R updateById(@RequestBody PurchaseEmployee purchaseEmployee) {
        return new R<>(purchaseEmployeeService.updateById(purchaseEmployee));
    }

    /**
     * 通过id删除采购员工管理
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除采购员工管理")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(purchaseEmployeeService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除采购员工管理")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseEmployeeService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询采购员工管理")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(purchaseEmployeeService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询采购员工管理总记录}")
    @PostMapping("/countByPurchaseEmployeeReq")
    public R countByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {

        return new R<>(purchaseEmployeeService.count(Wrappers.query(purchaseEmployeeReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param purchaseEmployeeReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据purchaseEmployeeReq查询一条货位信息表")
    @PostMapping("/getOneByPurchaseEmployeeReq")
    public R getOneByPurchaseEmployeeReq(@RequestBody PurchaseEmployeeReq purchaseEmployeeReq) {

        return new R<>(purchaseEmployeeService.getOne(Wrappers.query(purchaseEmployeeReq), false));
    }


    /**
     * 批量修改OR插入
     * @param purchaseEmployeeList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<PurchaseEmployee> purchaseEmployeeList) {

        return new R<>(purchaseEmployeeService.saveOrUpdateBatch(purchaseEmployeeList));
    }
    /**
     * 采购员工新增供应商
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/addSupplier")
    @SysLog("采购员工新增供应商")
    public R addSupplier(@RequestBody BuyerAddSupplierReq req){

        return new R<>(employeeInfoService.addSupplier(req));
    }

}
