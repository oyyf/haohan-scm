/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseAccount;
import com.haohan.cloud.scm.purchase.mapper.PurchaseAccountMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseAccountService;
import org.springframework.stereotype.Service;

/**
 * 采购员工账户
 *
 * @author haohan
 * @date 2019-05-13 19:05:49
 */
@Service
public class PurchaseAccountServiceImpl extends ServiceImpl<PurchaseAccountMapper, PurchaseAccount> implements PurchaseAccountService {

}
