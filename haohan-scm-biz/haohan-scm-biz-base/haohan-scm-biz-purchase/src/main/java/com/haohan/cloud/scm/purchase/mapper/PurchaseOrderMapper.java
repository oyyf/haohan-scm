/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseCateCountDTO;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.req.CountPurchaseOrderReq;

import java.math.BigDecimal;
import java.util.List;

/**
 * 采购部采购单
 *
 * @author haohan
 * @date 2019-05-13 18:48:11
 */
public interface PurchaseOrderMapper extends BaseMapper<PurchaseOrder> {

    /**
     * 查询今日采购单数
     * @param req
     * @return
     */
    Integer countPurchaseOrderNum(CountPurchaseOrderReq req);

    /**
     * 查询今日采购单总额
     * @param dto
     * @return
     */
    BigDecimal queryTodayAmount(PurchaseTodayDTO dto);

    /**
     * 查询今日已入库采购单
     * @param dto
     * @return
     */
    Integer queryEnterPurchase(PurchaseTodayDTO dto);

    /**
     * 查询今日入库商品种类
     * @return
     */
    List<String> quryEnterGoodsCate(PurchaseTodayDTO dto);

    /**
     * 查询采购商品分类统计
     * @param pmId
     * @return
     */
    List<PurchaseCateCountDTO> queryPurchaseGoodsCateCount(String pmId);
}
