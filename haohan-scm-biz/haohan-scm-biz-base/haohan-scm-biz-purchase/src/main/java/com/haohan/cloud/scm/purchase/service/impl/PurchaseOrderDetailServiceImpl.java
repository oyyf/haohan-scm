/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.purchase.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.purchase.mapper.PurchaseOrderDetailMapper;
import com.haohan.cloud.scm.purchase.service.PurchaseOrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 采购部采购单明细
 *
 * @author haohan
 * @date 2019-05-13 18:48:01
 */
@Service
public class PurchaseOrderDetailServiceImpl extends ServiceImpl<PurchaseOrderDetailMapper, PurchaseOrderDetail> implements PurchaseOrderDetailService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(PurchaseOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getPurchaseDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(PurchaseOrderDetail.class, NumberPrefixConstant.PURCHASE_DETAIL_SN_PRE);
            entity.setPurchaseDetailSn(sn);
        }
        return super.save(entity);
    }

}
