/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseEmployee;

/**
 * 采购员工管理
 *
 * @author haohan
 * @date 2019-05-13 18:48:18
 */
public interface PurchaseEmployeeService extends IService<PurchaseEmployee> {
}
