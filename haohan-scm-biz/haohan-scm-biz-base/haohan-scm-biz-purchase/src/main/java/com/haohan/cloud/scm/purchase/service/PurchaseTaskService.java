/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseTask;

/**
 * 采购任务
 *
 * @author haohan
 * @date 2019-05-13 18:52:46
 */
public interface PurchaseTaskService extends IService<PurchaseTask> {

}
