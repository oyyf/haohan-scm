package com.haohan.cloud.scm.purchase.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAddLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseAuditLendingReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingListReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseQueryLendingReq;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseQueryLendingResp;

/**
 * @author dy
 * @date 2019/5/17
 */
public interface IScmPurchaseLendingService {
    /**
     * 查询请款记录列表
     *
     * @param req
     * @return
     */
    Page queryLendingList(PurchaseQueryLendingListReq req);

    /**
     * 新增请款记录
     *
     * @param req
     * @return
     */
    Boolean addLending(PurchaseAddLendingReq req);

    /**
     * 请款审核
     *
     * @param req
     * @return
     */
    Boolean auditLending(PurchaseAuditLendingReq req);

    /**
     * 查询请款详情
     * @param req
     * @return
     */
    PurchaseQueryLendingResp queryLendingRecord(PurchaseQueryLendingReq req);

}
