/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.purchase.entity.MarketRecord;
import com.haohan.cloud.scm.api.purchase.req.MarketRecordReq;
import com.haohan.cloud.scm.api.purchase.req.PurchaseModifyMarketPriceReq;
import com.haohan.cloud.scm.purchase.core.IScmPurchaseMarketReportService;
import com.haohan.cloud.scm.purchase.service.MarketRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 市场行情价格记录
 *
 * @author haohan
 * @date 2019-05-29 13:35:01
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/MarketRecord")
@Api(value = "marketrecord", tags = "marketrecord内部接口服务")
public class MarketRecordFeignApiCtrl {

    private final MarketRecordService marketRecordService;
    private final IScmPurchaseMarketReportService marketReportService;


    /**
     * 通过id查询市场行情价格记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(marketRecordService.getById(id));
    }


    /**
     * 分页查询 市场行情价格记录 列表信息
     * @param marketRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMarketRecordPage")
    public R getMarketRecordPage(@RequestBody MarketRecordReq marketRecordReq) {
        Page page = new Page(marketRecordReq.getPageNo(), marketRecordReq.getPageSize());
        MarketRecord marketRecord =new MarketRecord();
        BeanUtil.copyProperties(marketRecordReq, marketRecord);

        return new R<>(marketRecordService.page(page, Wrappers.query(marketRecord)));
    }


    /**
     * 全量查询 市场行情价格记录 列表信息
     * @param marketRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMarketRecordList")
    public R getMarketRecordList(@RequestBody MarketRecordReq marketRecordReq) {
        MarketRecord marketRecord =new MarketRecord();
        BeanUtil.copyProperties(marketRecordReq, marketRecord);

        return new R<>(marketRecordService.list(Wrappers.query(marketRecord)));
    }


    /**
     * 新增市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @Inner
    @SysLog("新增市场行情价格记录")
    @PostMapping("/add")
    public R save(@RequestBody MarketRecord marketRecord) {
        return new R<>(marketRecordService.save(marketRecord));
    }

    /**
     * 修改市场行情价格记录
     * @param marketRecord 市场行情价格记录
     * @return R
     */
    @Inner
    @SysLog("修改市场行情价格记录")
    @PostMapping("/update")
    public R updateById(@RequestBody MarketRecord marketRecord) {
        return new R<>(marketRecordService.updateById(marketRecord));
    }

    /**
     * 通过id删除市场行情价格记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除市场行情价格记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(marketRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除市场行情价格记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(marketRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询市场行情价格记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(marketRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询市场行情价格记录总记录}")
    @PostMapping("/countByMarketRecordReq")
    public R countByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq) {

        return new R<>(marketRecordService.count(Wrappers.query(marketRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param marketRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据marketRecordReq查询一条货位信息表")
    @PostMapping("/getOneByMarketRecordReq")
    public R getOneByMarketRecordReq(@RequestBody MarketRecordReq marketRecordReq) {

        return new R<>(marketRecordService.getOne(Wrappers.query(marketRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param marketRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<MarketRecord> marketRecordList) {

        return new R<>(marketRecordService.saveOrUpdateBatch(marketRecordList));
    }

    @Inner
    @SysLog("修改市场行情详情")
    @PostMapping("/modifyMarketPrice")
    public R modifyMarketPrice(@RequestBody PurchaseModifyMarketPriceReq req) {
        return new R(marketReportService.modifyMarketPrice(req));
    }

}
