package com.haohan.cloud.scm.purchase.core;

import com.haohan.cloud.scm.api.purchase.req.ConfirmGoodsReq;

/**
 * @author dy
 * @date 2019/8/8
 */
public interface IScmPurchaseDetailService {


    /**
     * 揽货确认
     * @param req
     * @return
     */
    Boolean confirmGoods(ConfirmGoodsReq req);
}
