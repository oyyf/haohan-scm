package com.haohan.cloud.scm.purchase.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.opc.entity.SummaryOrder;
import com.haohan.cloud.scm.api.product.entity.GoodsLossRate;
import com.haohan.cloud.scm.api.purchase.dto.PurchaseTodayDTO;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrder;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseOrderDetail;
import com.haohan.cloud.scm.api.purchase.req.*;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseOrderDetailResp;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskDetailResp;
import com.haohan.cloud.scm.api.purchase.resp.PurchaseTaskListByDirectorResp;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author zgw
 * @date 2019/5/15.
 */
public interface IScmPurchaseOrderService {

    /**
     * 新增商品采购明细
     *
     * @param summaryOrder 汇总单实体
     * @return
     */
    PurchaseOrderDetail addPurchaseOrderDetail(SummaryOrder summaryOrder);

     /**
     * 新增商品采购明细
     *
     * @param summaryOrder  根据查询的汇总单
     * @return
     */
    PurchaseOrderDetail addPurchaseOrderDetailByQuery(SummaryOrder summaryOrder);

    /**
     * 新增商品采购单
     *
     * @param req
     * @return
     */
    PurchaseOrder addPurchaseOrder(AddPurchaseOrderReq req);

    /**
     * 采购单明细 供应商确定(传入供应商/价格)
     *
     * @param req
     * @return
     */
    PurchaseOrderDetail selectOrderSupplier(ChoiceSupplierReq req);

    /**
     * 查询采购任务列表(带采购商品信息) 采购员工(经理/采购员)
     *
     * @param req
     * @return
     */
    Page queryPurchaseTaskList(PurchasePageReq req);

    /**
     * 查询当前任务列表(带采购商品信息) 采购总监
     *
     * @param req
     * @return
     */
    Page<PurchaseTaskListByDirectorResp> queryTaskListByDirector(PurchasePageReq req);

    /**
     * 查询采购任务详情(带采购商品信息)
     *
     * @param req
     * @return
     */
    PurchaseTaskDetailResp queryPurchaseTask(QueryPurchaseTaskReq req);

    /**
     * 总监 执行采购任务(审核) 批量通过
     *
     * @param req
     * @return
     */
    Boolean auditTaskBatch(AuditTaskBatchReq req);

    /**
     * 总监 执行采购任务(审核) 批量通过 --B端
     * @param req
     * @return
     */
    Boolean checkTaskBatch (CheckTaskBatchReq req);

    /**
     * 总监 执行采购任务(审核) 修改采购单明细
     *
     * @param req
     * @return
     */
    PurchaseOrderDetail auditTask(AuditTaskReq req);

    /**
     * 经理 执行采购任务(分配) 指定采购员
     * @param req
     * @return
     */
    Boolean appointTask(AppointTaskReq req);

    /**
     * 库存不满足时  运营 发起需求采购 设置项(损耗率/协议供应)查询
     * @param req
     * @return
     */
    List<GoodsLossRate> queryGoodsLossList(QueryGoodsLossReq req);

    /**
     * 判断商品是否需要组合
     * @param order
     * @return
     */
    Boolean isGoodsRecipe(SummaryOrder order);
    /**
     *  采购部 发起需求采购
     * @param req
     * @return
     */
    List<PurchaseOrderDetail> initiateDemandPurchase(PurchaseNeedReq req);
    /**
     * 采购部/高管/运营  发起采购计划
     * @param req
     * @return
     */
    PurchaseOrderDetail initiatePurchasePlan(InitiatePurchasePlanReq req);

    /**
   * 采购单明细 供应商确定(传入供应商/价格)
   * 小程序：单品采购
   * @param req
   * @return
   */
    PurchaseOrderDetail singlePurchase(SinglePurchaseReq req);

    /**
     * 采购单明细 供应商确定(传入供应商/价格)
     * 小程序：协议供应
     * @param req
     * @return
     */
    PurchaseOrderDetail agreementPurchase(AgreementPurchaseReq req);


    /**
     * 新增采购单(采购单明细)
     * @param req
     * @return
     */
    Boolean addPurchaseOrderAndDetail(AddPurchaseOrderAndDetailReq req);

    /**
     * 发起采购
     * @param req
     * @return
     */
    Boolean beginPurchase(BeginPurchaseReq req);

    /**
     * 查询今日采购总额
     * @return
     */
    BigDecimal queryTodayAmount(PurchaseTodayDTO dto);

    /**
     * 查询采购状态数量
     * @return
     */
    List<Map<String,Object>> queryPurchaseStatus(String pmId);

    /**
     * 编辑采购单详情
     * @param req
     * @return
     */
    Boolean editPurchaseOrder(EditPurchaseOrderReq req);

    /**
     * 查询采购单及详情
     * @param req
     * @return
     */
    PurchaseOrderDetailResp queryPurchaseOrderAndDetail(OrderDetailReq req);

    /**
     * 查询所有采购要有任务（联查采购单明细）
     * @param req
     * @return
     */
    Page<PurchaseTaskListByDirectorResp> fetchTaskList(PurchasePageReq req);

    /**
     * 确认揽货
     * @param req
     * @return
     */
    Boolean deliveryTask(CompleteTaskReq req);
}
