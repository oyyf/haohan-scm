/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.purchase.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.purchase.entity.PurchaseReport;

/**
 * 采购汇报
 *
 * @author haohan
 * @date 2019-05-13 18:52:53
 */
public interface PurchaseReportService extends IService<PurchaseReport> {

}
