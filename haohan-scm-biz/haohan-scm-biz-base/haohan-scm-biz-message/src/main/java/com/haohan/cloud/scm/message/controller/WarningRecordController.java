/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WarningRecord;
import com.haohan.cloud.scm.api.message.req.WarningRecordReq;
import com.haohan.cloud.scm.message.service.WarningRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 预警记录表
 *
 * @author haohan
 * @date 2019-05-29 13:49:41
 */
@RestController
@AllArgsConstructor
@RequestMapping("/warningrecord" )
@Api(value = "warningrecord", tags = "warningrecord管理")
public class WarningRecordController {

    private final WarningRecordService warningRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param warningRecord 预警记录表
     * @return
     */
    @GetMapping("/page" )
    public R getWarningRecordPage(Page page, WarningRecord warningRecord) {
        return new R<>(warningRecordService.page(page, Wrappers.query(warningRecord)));
    }


    /**
     * 通过id查询预警记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(warningRecordService.getById(id));
    }

    /**
     * 新增预警记录表
     * @param warningRecord 预警记录表
     * @return R
     */
    @SysLog("新增预警记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_warningrecord_add')" )
    public R save(@RequestBody WarningRecord warningRecord) {
        return new R<>(warningRecordService.save(warningRecord));
    }

    /**
     * 修改预警记录表
     * @param warningRecord 预警记录表
     * @return R
     */
    @SysLog("修改预警记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_warningrecord_edit')" )
    public R updateById(@RequestBody WarningRecord warningRecord) {
        return new R<>(warningRecordService.updateById(warningRecord));
    }

    /**
     * 通过id删除预警记录表
     * @param id id
     * @return R
     */
    @SysLog("删除预警记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_warningrecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(warningRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除预警记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_warningrecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warningRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询预警记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warningRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warningRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询预警记录表总记录}")
    @PostMapping("/countByWarningRecordReq")
    public R countByWarningRecordReq(@RequestBody WarningRecordReq warningRecordReq) {

        return new R<>(warningRecordService.count(Wrappers.query(warningRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warningRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据warningRecordReq查询一条货位信息表")
    @PostMapping("/getOneByWarningRecordReq")
    public R getOneByWarningRecordReq(@RequestBody WarningRecordReq warningRecordReq) {

        return new R<>(warningRecordService.getOne(Wrappers.query(warningRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warningRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_warningrecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WarningRecord> warningRecordList) {

        return new R<>(warningRecordService.saveOrUpdateBatch(warningRecordList));
    }


}
