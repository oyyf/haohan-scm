package com.haohan.cloud.scm.message.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.dto.SendInMailMessageDTO;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import com.haohan.cloud.scm.api.message.req.InMailModifyReq;
import com.haohan.cloud.scm.api.message.req.QueryMessageReq;
import com.haohan.cloud.scm.api.message.vo.MsgInfoVO;

/**
 * @author dy
 * @date 2020/1/14
 */
public interface MessageCoreService {


    IPage<MsgInfoVO> findPage(Page<MessageRecord> page, QueryMessageReq req);

    MsgInfoVO fetchInMailInfo(String messageSn);

    void sendInMailMessage(SendInMailMessageDTO message);

    boolean inMailModify(InMailModifyReq req);

    void modifyByDeleteSource(SendInMailMessageDTO message);
}
