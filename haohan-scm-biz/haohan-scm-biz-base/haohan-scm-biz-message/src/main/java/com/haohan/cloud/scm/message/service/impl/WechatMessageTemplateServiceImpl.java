/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.WechatMessageTemplate;
import com.haohan.cloud.scm.message.mapper.WechatMessageTemplateMapper;
import com.haohan.cloud.scm.message.service.WechatMessageTemplateService;
import org.springframework.stereotype.Service;

/**
 * 消息模板
 *
 * @author haohan
 * @date 2019-05-13 18:34:07
 */
@Service
public class WechatMessageTemplateServiceImpl extends ServiceImpl<WechatMessageTemplateMapper, WechatMessageTemplate> implements WechatMessageTemplateService {

}
