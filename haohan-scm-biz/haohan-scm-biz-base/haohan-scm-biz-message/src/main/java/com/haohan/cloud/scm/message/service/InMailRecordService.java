/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;

/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-13 18:34:31
 */
public interface InMailRecordService extends IService<InMailRecord> {

    InMailRecord fetchBySn(String messageSn);
}
