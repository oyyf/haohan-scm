/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WarningSend;
import com.haohan.cloud.scm.api.message.req.WarningSendReq;
import com.haohan.cloud.scm.message.service.WarningSendService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 预警发送记录表
 *
 * @author haohan
 * @date 2019-05-29 13:49:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WarningSend")
@Api(value = "warningsend", tags = "warningsend内部接口服务")
public class WarningSendFeignApiCtrl {

    private final WarningSendService warningSendService;


    /**
     * 通过id查询预警发送记录表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(warningSendService.getById(id));
    }


    /**
     * 分页查询 预警发送记录表 列表信息
     * @param warningSendReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarningSendPage")
    public R getWarningSendPage(@RequestBody WarningSendReq warningSendReq) {
        Page page = new Page(warningSendReq.getPageNo(), warningSendReq.getPageSize());
        WarningSend warningSend =new WarningSend();
        BeanUtil.copyProperties(warningSendReq, warningSend);

        return new R<>(warningSendService.page(page, Wrappers.query(warningSend)));
    }


    /**
     * 全量查询 预警发送记录表 列表信息
     * @param warningSendReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWarningSendList")
    public R getWarningSendList(@RequestBody WarningSendReq warningSendReq) {
        WarningSend warningSend =new WarningSend();
        BeanUtil.copyProperties(warningSendReq, warningSend);

        return new R<>(warningSendService.list(Wrappers.query(warningSend)));
    }


    /**
     * 新增预警发送记录表
     * @param warningSend 预警发送记录表
     * @return R
     */
    @Inner
    @SysLog("新增预警发送记录表")
    @PostMapping("/add")
    public R save(@RequestBody WarningSend warningSend) {
        return new R<>(warningSendService.save(warningSend));
    }

    /**
     * 修改预警发送记录表
     * @param warningSend 预警发送记录表
     * @return R
     */
    @Inner
    @SysLog("修改预警发送记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody WarningSend warningSend) {
        return new R<>(warningSendService.updateById(warningSend));
    }

    /**
     * 通过id删除预警发送记录表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除预警发送记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(warningSendService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除预警发送记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warningSendService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询预警发送记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warningSendService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warningSendReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询预警发送记录表总记录}")
    @PostMapping("/countByWarningSendReq")
    public R countByWarningSendReq(@RequestBody WarningSendReq warningSendReq) {

        return new R<>(warningSendService.count(Wrappers.query(warningSendReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warningSendReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据warningSendReq查询一条货位信息表")
    @PostMapping("/getOneByWarningSendReq")
    public R getOneByWarningSendReq(@RequestBody WarningSendReq warningSendReq) {

        return new R<>(warningSendService.getOne(Wrappers.query(warningSendReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warningSendList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WarningSend> warningSendList) {

        return new R<>(warningSendService.saveOrUpdateBatch(warningSendList));
    }

}
