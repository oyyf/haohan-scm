/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.message.entity.WechatUserMsgevent;

/**
 * 微信用户消息事件
 *
 * @author haohan
 * @date 2019-05-13 18:34:03
 */
public interface WechatUserMsgeventService extends IService<WechatUserMsgevent> {

}
