/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatUserMsgevent;
import com.haohan.cloud.scm.api.message.req.WechatUserMsgeventReq;
import com.haohan.cloud.scm.message.service.WechatUserMsgeventService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 微信用户消息事件
 *
 * @author haohan
 * @date 2019-05-29 13:50:08
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WechatUserMsgevent")
@Api(value = "wechatusermsgevent", tags = "wechatusermsgevent内部接口服务")
public class WechatUserMsgeventFeignApiCtrl {

    private final WechatUserMsgeventService wechatUserMsgeventService;


    /**
     * 通过id查询微信用户消息事件
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(wechatUserMsgeventService.getById(id));
    }


    /**
     * 分页查询 微信用户消息事件 列表信息
     * @param wechatUserMsgeventReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWechatUserMsgeventPage")
    public R getWechatUserMsgeventPage(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {
        Page page = new Page(wechatUserMsgeventReq.getPageNo(), wechatUserMsgeventReq.getPageSize());
        WechatUserMsgevent wechatUserMsgevent =new WechatUserMsgevent();
        BeanUtil.copyProperties(wechatUserMsgeventReq, wechatUserMsgevent);

        return new R<>(wechatUserMsgeventService.page(page, Wrappers.query(wechatUserMsgevent)));
    }


    /**
     * 全量查询 微信用户消息事件 列表信息
     * @param wechatUserMsgeventReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWechatUserMsgeventList")
    public R getWechatUserMsgeventList(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {
        WechatUserMsgevent wechatUserMsgevent =new WechatUserMsgevent();
        BeanUtil.copyProperties(wechatUserMsgeventReq, wechatUserMsgevent);

        return new R<>(wechatUserMsgeventService.list(Wrappers.query(wechatUserMsgevent)));
    }


    /**
     * 新增微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @Inner
    @SysLog("新增微信用户消息事件")
    @PostMapping("/add")
    public R save(@RequestBody WechatUserMsgevent wechatUserMsgevent) {
        return new R<>(wechatUserMsgeventService.save(wechatUserMsgevent));
    }

    /**
     * 修改微信用户消息事件
     * @param wechatUserMsgevent 微信用户消息事件
     * @return R
     */
    @Inner
    @SysLog("修改微信用户消息事件")
    @PostMapping("/update")
    public R updateById(@RequestBody WechatUserMsgevent wechatUserMsgevent) {
        return new R<>(wechatUserMsgeventService.updateById(wechatUserMsgevent));
    }

    /**
     * 通过id删除微信用户消息事件
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除微信用户消息事件")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(wechatUserMsgeventService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除微信用户消息事件")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatUserMsgeventService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询微信用户消息事件")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatUserMsgeventService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询微信用户消息事件总记录}")
    @PostMapping("/countByWechatUserMsgeventReq")
    public R countByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {

        return new R<>(wechatUserMsgeventService.count(Wrappers.query(wechatUserMsgeventReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatUserMsgeventReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据wechatUserMsgeventReq查询一条货位信息表")
    @PostMapping("/getOneByWechatUserMsgeventReq")
    public R getOneByWechatUserMsgeventReq(@RequestBody WechatUserMsgeventReq wechatUserMsgeventReq) {

        return new R<>(wechatUserMsgeventService.getOne(Wrappers.query(wechatUserMsgeventReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatUserMsgeventList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WechatUserMsgevent> wechatUserMsgeventList) {

        return new R<>(wechatUserMsgeventService.saveOrUpdateBatch(wechatUserMsgeventList));
    }

}
