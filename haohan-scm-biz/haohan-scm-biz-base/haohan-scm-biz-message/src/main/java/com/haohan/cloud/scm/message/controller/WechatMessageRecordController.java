/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatMessageRecord;
import com.haohan.cloud.scm.api.message.req.WechatMessageRecordReq;
import com.haohan.cloud.scm.message.service.WechatMessageRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 微信消息记录表
 *
 * @author haohan
 * @date 2019-05-29 13:48:09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wechatmessagerecord" )
@Api(value = "wechatmessagerecord", tags = "wechatmessagerecord管理")
public class WechatMessageRecordController {

    private final WechatMessageRecordService wechatMessageRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param wechatMessageRecord 微信消息记录表
     * @return
     */
    @GetMapping("/page" )
    public R getWechatMessageRecordPage(Page page, WechatMessageRecord wechatMessageRecord) {
        return new R<>(wechatMessageRecordService.page(page, Wrappers.query(wechatMessageRecord)));
    }


    /**
     * 通过id查询微信消息记录表
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(wechatMessageRecordService.getById(id));
    }

    /**
     * 新增微信消息记录表
     * @param wechatMessageRecord 微信消息记录表
     * @return R
     */
    @SysLog("新增微信消息记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagerecord_add')" )
    public R save(@RequestBody WechatMessageRecord wechatMessageRecord) {
        return new R<>(wechatMessageRecordService.save(wechatMessageRecord));
    }

    /**
     * 修改微信消息记录表
     * @param wechatMessageRecord 微信消息记录表
     * @return R
     */
    @SysLog("修改微信消息记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagerecord_edit')" )
    public R updateById(@RequestBody WechatMessageRecord wechatMessageRecord) {
        return new R<>(wechatMessageRecordService.updateById(wechatMessageRecord));
    }

    /**
     * 通过id删除微信消息记录表
     * @param id id
     * @return R
     */
    @SysLog("删除微信消息记录表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagerecord_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(wechatMessageRecordService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除微信消息记录表")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagerecord_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询微信消息记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询微信消息记录表总记录}")
    @PostMapping("/countByWechatMessageRecordReq")
    public R countByWechatMessageRecordReq(@RequestBody WechatMessageRecordReq wechatMessageRecordReq) {

        return new R<>(wechatMessageRecordService.count(Wrappers.query(wechatMessageRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据wechatMessageRecordReq查询一条货位信息表")
    @PostMapping("/getOneByWechatMessageRecordReq")
    public R getOneByWechatMessageRecordReq(@RequestBody WechatMessageRecordReq wechatMessageRecordReq) {

        return new R<>(wechatMessageRecordService.getOne(Wrappers.query(wechatMessageRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatMessageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagerecord_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WechatMessageRecord> wechatMessageRecordList) {

        return new R<>(wechatMessageRecordService.saveOrUpdateBatch(wechatMessageRecordList));
    }


}
