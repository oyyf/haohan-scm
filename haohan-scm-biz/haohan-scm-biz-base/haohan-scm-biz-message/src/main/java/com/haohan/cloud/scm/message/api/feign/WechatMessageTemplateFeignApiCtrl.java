/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatMessageTemplate;
import com.haohan.cloud.scm.api.message.req.WechatMessageTemplateReq;
import com.haohan.cloud.scm.message.service.WechatMessageTemplateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 消息模板
 *
 * @author haohan
 * @date 2019-05-29 13:48:04
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/WechatMessageTemplate")
@Api(value = "wechatmessagetemplate", tags = "wechatmessagetemplate内部接口服务")
public class WechatMessageTemplateFeignApiCtrl {

    private final WechatMessageTemplateService wechatMessageTemplateService;


    /**
     * 通过id查询消息模板
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(wechatMessageTemplateService.getById(id));
    }


    /**
     * 分页查询 消息模板 列表信息
     * @param wechatMessageTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWechatMessageTemplatePage")
    public R getWechatMessageTemplatePage(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {
        Page page = new Page(wechatMessageTemplateReq.getPageNo(), wechatMessageTemplateReq.getPageSize());
        WechatMessageTemplate wechatMessageTemplate =new WechatMessageTemplate();
        BeanUtil.copyProperties(wechatMessageTemplateReq, wechatMessageTemplate);

        return new R<>(wechatMessageTemplateService.page(page, Wrappers.query(wechatMessageTemplate)));
    }


    /**
     * 全量查询 消息模板 列表信息
     * @param wechatMessageTemplateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchWechatMessageTemplateList")
    public R getWechatMessageTemplateList(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {
        WechatMessageTemplate wechatMessageTemplate =new WechatMessageTemplate();
        BeanUtil.copyProperties(wechatMessageTemplateReq, wechatMessageTemplate);

        return new R<>(wechatMessageTemplateService.list(Wrappers.query(wechatMessageTemplate)));
    }


    /**
     * 新增消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @Inner
    @SysLog("新增消息模板")
    @PostMapping("/add")
    public R save(@RequestBody WechatMessageTemplate wechatMessageTemplate) {
        return new R<>(wechatMessageTemplateService.save(wechatMessageTemplate));
    }

    /**
     * 修改消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @Inner
    @SysLog("修改消息模板")
    @PostMapping("/update")
    public R updateById(@RequestBody WechatMessageTemplate wechatMessageTemplate) {
        return new R<>(wechatMessageTemplateService.updateById(wechatMessageTemplate));
    }

    /**
     * 通过id删除消息模板
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除消息模板")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(wechatMessageTemplateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除消息模板")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageTemplateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询消息模板")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageTemplateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询消息模板总记录}")
    @PostMapping("/countByWechatMessageTemplateReq")
    public R countByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {

        return new R<>(wechatMessageTemplateService.count(Wrappers.query(wechatMessageTemplateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据wechatMessageTemplateReq查询一条货位信息表")
    @PostMapping("/getOneByWechatMessageTemplateReq")
    public R getOneByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {

        return new R<>(wechatMessageTemplateService.getOne(Wrappers.query(wechatMessageTemplateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatMessageTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<WechatMessageTemplate> wechatMessageTemplateList) {

        return new R<>(wechatMessageTemplateService.saveOrUpdateBatch(wechatMessageTemplateList));
    }

}
