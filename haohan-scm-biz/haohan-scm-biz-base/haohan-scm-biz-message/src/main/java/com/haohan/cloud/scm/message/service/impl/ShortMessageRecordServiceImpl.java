/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.message.entity.ShortMessageRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.message.mapper.ShortMessageRecordMapper;
import com.haohan.cloud.scm.message.service.ShortMessageRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 短信消息记录表
 *
 * @author haohan
 * @date 2019-05-13 18:34:23
 */
@Service
public class ShortMessageRecordServiceImpl extends ServiceImpl<ShortMessageRecordMapper, ShortMessageRecord> implements ShortMessageRecordService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(ShortMessageRecord entity) {
        if (StrUtil.isEmpty(entity.getShortMsgSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(ShortMessageRecord.class, NumberPrefixConstant.SHORT_MESSAGE_RECORD_SN_PRE);
            entity.setShortMsgSn(sn);
        }
        return super.save(entity);
    }


}
