/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.message.entity.WechatMessageDetail;

/**
 * 消息模板明细
 *
 * @author haohan
 * @date 2019-05-13 18:34:15
 */
public interface WechatMessageDetailMapper extends BaseMapper<WechatMessageDetail> {

}
