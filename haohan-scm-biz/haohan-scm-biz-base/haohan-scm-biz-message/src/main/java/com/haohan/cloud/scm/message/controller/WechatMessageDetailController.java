/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatMessageDetail;
import com.haohan.cloud.scm.api.message.req.WechatMessageDetailReq;
import com.haohan.cloud.scm.message.service.WechatMessageDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 消息模板明细
 *
 * @author haohan
 * @date 2019-05-29 13:48:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wechatmessagedetail" )
@Api(value = "wechatmessagedetail", tags = "wechatmessagedetail管理")
public class WechatMessageDetailController {

    private final WechatMessageDetailService wechatMessageDetailService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param wechatMessageDetail 消息模板明细
     * @return
     */
    @GetMapping("/page" )
    public R getWechatMessageDetailPage(Page page, WechatMessageDetail wechatMessageDetail) {
        return new R<>(wechatMessageDetailService.page(page, Wrappers.query(wechatMessageDetail)));
    }


    /**
     * 通过id查询消息模板明细
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(wechatMessageDetailService.getById(id));
    }

    /**
     * 新增消息模板明细
     * @param wechatMessageDetail 消息模板明细
     * @return R
     */
    @SysLog("新增消息模板明细" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagedetail_add')" )
    public R save(@RequestBody WechatMessageDetail wechatMessageDetail) {
        return new R<>(wechatMessageDetailService.save(wechatMessageDetail));
    }

    /**
     * 修改消息模板明细
     * @param wechatMessageDetail 消息模板明细
     * @return R
     */
    @SysLog("修改消息模板明细" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagedetail_edit')" )
    public R updateById(@RequestBody WechatMessageDetail wechatMessageDetail) {
        return new R<>(wechatMessageDetailService.updateById(wechatMessageDetail));
    }

    /**
     * 通过id删除消息模板明细
     * @param id id
     * @return R
     */
    @SysLog("删除消息模板明细" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagedetail_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(wechatMessageDetailService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除消息模板明细")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagedetail_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageDetailService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询消息模板明细")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageDetailService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatMessageDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询消息模板明细总记录}")
    @PostMapping("/countByWechatMessageDetailReq")
    public R countByWechatMessageDetailReq(@RequestBody WechatMessageDetailReq wechatMessageDetailReq) {

        return new R<>(wechatMessageDetailService.count(Wrappers.query(wechatMessageDetailReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatMessageDetailReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据wechatMessageDetailReq查询一条货位信息表")
    @PostMapping("/getOneByWechatMessageDetailReq")
    public R getOneByWechatMessageDetailReq(@RequestBody WechatMessageDetailReq wechatMessageDetailReq) {

        return new R<>(wechatMessageDetailService.getOne(Wrappers.query(wechatMessageDetailReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatMessageDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagedetail_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WechatMessageDetail> wechatMessageDetailList) {

        return new R<>(wechatMessageDetailService.saveOrUpdateBatch(wechatMessageDetailList));
    }


}
