/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.message.entity.WarningRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.message.mapper.WarningRecordMapper;
import com.haohan.cloud.scm.message.service.WarningRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 预警记录表
 *
 * @author haohan
 * @date 2019-05-13 18:23:48
 */
@Service
public class WarningRecordServiceImpl extends ServiceImpl<WarningRecordMapper, WarningRecord> implements WarningRecordService {

    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WarningRecord entity) {
        if (StrUtil.isEmpty(entity.getWarningSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WarningRecord.class, NumberPrefixConstant.WARNING_RECORD_SN_PRE);
            entity.setWarningSn(sn);
        }
        return super.save(entity);
    }

}
