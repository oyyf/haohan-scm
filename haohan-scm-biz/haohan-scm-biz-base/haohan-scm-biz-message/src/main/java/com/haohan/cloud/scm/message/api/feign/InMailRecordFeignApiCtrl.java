/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.dto.SendInMailMessageDTO;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.api.message.req.InMailRecordReq;
import com.haohan.cloud.scm.api.message.vo.MsgInfoVO;
import com.haohan.cloud.scm.message.core.MessageCoreService;
import com.haohan.cloud.scm.message.service.InMailRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-29 13:47:41
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/InMailRecord")
@Api(value = "inmailrecord", tags = "inmailrecord内部接口服务")
public class InMailRecordFeignApiCtrl {

    private final InMailRecordService inMailRecordService;
    private final MessageCoreService messageCoreService;


    /**
     * 通过id查询站内信记录表
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(inMailRecordService.getById(id));
    }


    /**
     * 分页查询 站内信记录表 列表信息
     *
     * @param inMailRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchInMailRecordPage")
    public R getInMailRecordPage(@RequestBody InMailRecordReq inMailRecordReq) {
        Page page = new Page(inMailRecordReq.getPageNo(), inMailRecordReq.getPageSize());
        InMailRecord inMailRecord = new InMailRecord();
        BeanUtil.copyProperties(inMailRecordReq, inMailRecord);

        return new R<>(inMailRecordService.page(page, Wrappers.query(inMailRecord)));
    }


    /**
     * 全量查询 站内信记录表 列表信息
     *
     * @param inMailRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchInMailRecordList")
    public R getInMailRecordList(@RequestBody InMailRecordReq inMailRecordReq) {
        InMailRecord inMailRecord = new InMailRecord();
        BeanUtil.copyProperties(inMailRecordReq, inMailRecord);

        return new R<>(inMailRecordService.list(Wrappers.query(inMailRecord)));
    }


    /**
     * 新增站内信记录表
     *
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @Inner
    @SysLog("新增站内信记录表")
    @PostMapping("/add")
    public R save(@RequestBody InMailRecord inMailRecord) {
        return new R<>(inMailRecordService.save(inMailRecord));
    }

    /**
     * 修改站内信记录表
     *
     * @param inMailRecord 站内信记录表
     * @return R
     */
    @Inner
    @SysLog("修改站内信记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody InMailRecord inMailRecord) {
        return new R<>(inMailRecordService.updateById(inMailRecord));
    }

    /**
     * 通过id删除站内信记录表
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除站内信记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(inMailRecordService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除站内信记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(inMailRecordService.removeByIds(idList));
    }


    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询站内信记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(inMailRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询站内信记录表总记录}")
    @PostMapping("/countByInMailRecordReq")
    public R countByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq) {

        return new R<>(inMailRecordService.count(Wrappers.query(inMailRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     *
     * @param inMailRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据inMailRecordReq查询一条货位信息表")
    @PostMapping("/getOneByInMailRecordReq")
    public R getOneByInMailRecordReq(@RequestBody InMailRecordReq inMailRecordReq) {

        return new R<>(inMailRecordService.getOne(Wrappers.query(inMailRecordReq), false));
    }


    /**
     * 批量修改OR插入
     *
     * @param inMailRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<InMailRecord> inMailRecordList) {

        return new R<>(inMailRecordService.saveOrUpdateBatch(inMailRecordList));
    }

    @Inner
    @GetMapping("/fetchInfo/{messageSn}")
    public R<MsgInfoVO> fetchInfo(@PathVariable("messageSn") String messageSn) {
        return R.ok(messageCoreService.fetchInMailInfo(messageSn));
    }

    @Inner
    @PostMapping("/sendMessage")
    public R<Boolean> sendInMailMessage(@RequestBody SendInMailMessageDTO message) {
        messageCoreService.sendInMailMessage(message);
        return R.ok(true);
    }

    @Inner
    @PostMapping("/modifyByDeleteSource")
    public R<Boolean> modifyByDeleteSource(@RequestBody SendInMailMessageDTO message) {
        messageCoreService.modifyByDeleteSource(message);
        return R.ok(true);
    }

}
