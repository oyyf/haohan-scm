/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.WechatUserMsgevent;
import com.haohan.cloud.scm.message.mapper.WechatUserMsgeventMapper;
import com.haohan.cloud.scm.message.service.WechatUserMsgeventService;
import org.springframework.stereotype.Service;

/**
 * 微信用户消息事件
 *
 * @author haohan
 * @date 2019-05-13 18:34:03
 */
@Service
public class WechatUserMsgeventServiceImpl extends ServiceImpl<WechatUserMsgeventMapper, WechatUserMsgevent> implements WechatUserMsgeventService {

}
