/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.message.entity.WechatMessageRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.message.mapper.WechatMessageRecordMapper;
import com.haohan.cloud.scm.message.service.WechatMessageRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * 微信消息记录表
 *
 * @author haohan
 * @date 2019-05-13 18:34:11
 */
@Service
public class WechatMessageRecordServiceImpl extends ServiceImpl<WechatMessageRecordMapper, WechatMessageRecord> implements WechatMessageRecordService {
    @Autowired
    @Lazy
    private ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(WechatMessageRecord entity) {
        if (StrUtil.isEmpty(entity.getWechatMsgSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(WechatMessageRecord.class, NumberPrefixConstant.WECHAT_MESSAGE_RECORD_SN_PRE);
            entity.setWechatMsgSn(sn);
        }
        return super.save(entity);
    }

}
