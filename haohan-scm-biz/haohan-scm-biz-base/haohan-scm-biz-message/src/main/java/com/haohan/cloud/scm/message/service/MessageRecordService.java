/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;

/**
 * 消息发送记录
 *
 * @author haohan
 * @date 2019-05-13 18:34:27
 */
public interface MessageRecordService extends IService<MessageRecord> {

    MessageRecord fetchBySn(String messageSn);
}
