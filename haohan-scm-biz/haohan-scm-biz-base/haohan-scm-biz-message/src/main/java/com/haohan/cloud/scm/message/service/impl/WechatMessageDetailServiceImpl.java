/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.message.entity.WechatMessageDetail;
import com.haohan.cloud.scm.message.mapper.WechatMessageDetailMapper;
import com.haohan.cloud.scm.message.service.WechatMessageDetailService;
import org.springframework.stereotype.Service;

/**
 * 消息模板明细
 *
 * @author haohan
 * @date 2019-05-13 18:34:15
 */
@Service
public class WechatMessageDetailServiceImpl extends ServiceImpl<WechatMessageDetailMapper, WechatMessageDetail> implements WechatMessageDetailService {

}
