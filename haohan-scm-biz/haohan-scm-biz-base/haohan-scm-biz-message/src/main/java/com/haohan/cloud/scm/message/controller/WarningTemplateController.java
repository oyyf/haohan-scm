/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WarningTemplate;
import com.haohan.cloud.scm.api.message.req.WarningTemplateReq;
import com.haohan.cloud.scm.message.service.WarningTemplateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 预警信息模板
 *
 * @author haohan
 * @date 2019-05-29 13:49:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/warningtemplate" )
@Api(value = "warningtemplate", tags = "warningtemplate管理")
public class WarningTemplateController {

    private final WarningTemplateService warningTemplateService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param warningTemplate 预警信息模板
     * @return
     */
    @GetMapping("/page" )
    public R getWarningTemplatePage(Page page, WarningTemplate warningTemplate) {
        return new R<>(warningTemplateService.page(page, Wrappers.query(warningTemplate)));
    }


    /**
     * 通过id查询预警信息模板
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(warningTemplateService.getById(id));
    }

    /**
     * 新增预警信息模板
     * @param warningTemplate 预警信息模板
     * @return R
     */
    @SysLog("新增预警信息模板" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_warningtemplate_add')" )
    public R save(@RequestBody WarningTemplate warningTemplate) {
        return new R<>(warningTemplateService.save(warningTemplate));
    }

    /**
     * 修改预警信息模板
     * @param warningTemplate 预警信息模板
     * @return R
     */
    @SysLog("修改预警信息模板" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_warningtemplate_edit')" )
    public R updateById(@RequestBody WarningTemplate warningTemplate) {
        return new R<>(warningTemplateService.updateById(warningTemplate));
    }

    /**
     * 通过id删除预警信息模板
     * @param id id
     * @return R
     */
    @SysLog("删除预警信息模板" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_warningtemplate_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(warningTemplateService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除预警信息模板")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_warningtemplate_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(warningTemplateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询预警信息模板")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(warningTemplateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param warningTemplateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询预警信息模板总记录}")
    @PostMapping("/countByWarningTemplateReq")
    public R countByWarningTemplateReq(@RequestBody WarningTemplateReq warningTemplateReq) {

        return new R<>(warningTemplateService.count(Wrappers.query(warningTemplateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param warningTemplateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据warningTemplateReq查询一条货位信息表")
    @PostMapping("/getOneByWarningTemplateReq")
    public R getOneByWarningTemplateReq(@RequestBody WarningTemplateReq warningTemplateReq) {

        return new R<>(warningTemplateService.getOne(Wrappers.query(warningTemplateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param warningTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_warningtemplate_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WarningTemplate> warningTemplateList) {

        return new R<>(warningTemplateService.saveOrUpdateBatch(warningTemplateList));
    }


}
