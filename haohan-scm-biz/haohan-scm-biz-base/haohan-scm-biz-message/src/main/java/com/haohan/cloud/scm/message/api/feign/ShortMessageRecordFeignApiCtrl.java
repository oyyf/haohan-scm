/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.ShortMessageRecord;
import com.haohan.cloud.scm.api.message.req.ShortMessageRecordReq;
import com.haohan.cloud.scm.message.service.ShortMessageRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 短信消息记录表
 *
 * @author haohan
 * @date 2019-05-29 13:48:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/ShortMessageRecord")
@Api(value = "shortmessagerecord", tags = "shortmessagerecord内部接口服务")
public class ShortMessageRecordFeignApiCtrl {

    private final ShortMessageRecordService shortMessageRecordService;


    /**
     * 通过id查询短信消息记录表
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(shortMessageRecordService.getById(id));
    }


    /**
     * 分页查询 短信消息记录表 列表信息
     * @param shortMessageRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShortMessageRecordPage")
    public R getShortMessageRecordPage(@RequestBody ShortMessageRecordReq shortMessageRecordReq) {
        Page page = new Page(shortMessageRecordReq.getPageNo(), shortMessageRecordReq.getPageSize());
        ShortMessageRecord shortMessageRecord =new ShortMessageRecord();
        BeanUtil.copyProperties(shortMessageRecordReq, shortMessageRecord);

        return new R<>(shortMessageRecordService.page(page, Wrappers.query(shortMessageRecord)));
    }


    /**
     * 全量查询 短信消息记录表 列表信息
     * @param shortMessageRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchShortMessageRecordList")
    public R getShortMessageRecordList(@RequestBody ShortMessageRecordReq shortMessageRecordReq) {
        ShortMessageRecord shortMessageRecord =new ShortMessageRecord();
        BeanUtil.copyProperties(shortMessageRecordReq, shortMessageRecord);

        return new R<>(shortMessageRecordService.list(Wrappers.query(shortMessageRecord)));
    }


    /**
     * 新增短信消息记录表
     * @param shortMessageRecord 短信消息记录表
     * @return R
     */
    @Inner
    @SysLog("新增短信消息记录表")
    @PostMapping("/add")
    public R save(@RequestBody ShortMessageRecord shortMessageRecord) {
        return new R<>(shortMessageRecordService.save(shortMessageRecord));
    }

    /**
     * 修改短信消息记录表
     * @param shortMessageRecord 短信消息记录表
     * @return R
     */
    @Inner
    @SysLog("修改短信消息记录表")
    @PostMapping("/update")
    public R updateById(@RequestBody ShortMessageRecord shortMessageRecord) {
        return new R<>(shortMessageRecordService.updateById(shortMessageRecord));
    }

    /**
     * 通过id删除短信消息记录表
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除短信消息记录表")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(shortMessageRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除短信消息记录表")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(shortMessageRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询短信消息记录表")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(shortMessageRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param shortMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询短信消息记录表总记录}")
    @PostMapping("/countByShortMessageRecordReq")
    public R countByShortMessageRecordReq(@RequestBody ShortMessageRecordReq shortMessageRecordReq) {

        return new R<>(shortMessageRecordService.count(Wrappers.query(shortMessageRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param shortMessageRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据shortMessageRecordReq查询一条货位信息表")
    @PostMapping("/getOneByShortMessageRecordReq")
    public R getOneByShortMessageRecordReq(@RequestBody ShortMessageRecordReq shortMessageRecordReq) {

        return new R<>(shortMessageRecordService.getOne(Wrappers.query(shortMessageRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param shortMessageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<ShortMessageRecord> shortMessageRecordList) {

        return new R<>(shortMessageRecordService.saveOrUpdateBatch(shortMessageRecordList));
    }

}
