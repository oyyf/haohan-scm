/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.MessageRecord;
import com.haohan.cloud.scm.api.message.req.MessageRecordReq;
import com.haohan.cloud.scm.api.message.req.QueryMessageFeignReq;
import com.haohan.cloud.scm.api.message.vo.MsgInfoVO;
import com.haohan.cloud.scm.message.core.MessageCoreService;
import com.haohan.cloud.scm.message.service.MessageRecordService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 消息发送记录
 *
 * @author haohan
 * @date 2019-05-29 13:48:28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/MessageRecord")
@Api(value = "messagerecord", tags = "messagerecord内部接口服务")
public class MessageRecordFeignApiCtrl {

    private final MessageRecordService messageRecordService;
    private final MessageCoreService messageCoreService;


    /**
     * 通过id查询消息发送记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(messageRecordService.getById(id));
    }


    /**
     * 分页查询 消息发送记录 列表信息
     * @param messageRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMessageRecordPage")
    public R getMessageRecordPage(@RequestBody MessageRecordReq messageRecordReq) {
        Page page = new Page(messageRecordReq.getPageNo(), messageRecordReq.getPageSize());
        MessageRecord messageRecord =new MessageRecord();
        BeanUtil.copyProperties(messageRecordReq, messageRecord);

        return new R<>(messageRecordService.page(page, Wrappers.query(messageRecord)));
    }


    /**
     * 全量查询 消息发送记录 列表信息
     * @param messageRecordReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchMessageRecordList")
    public R getMessageRecordList(@RequestBody MessageRecordReq messageRecordReq) {
        MessageRecord messageRecord =new MessageRecord();
        BeanUtil.copyProperties(messageRecordReq, messageRecord);

        return new R<>(messageRecordService.list(Wrappers.query(messageRecord)));
    }


    /**
     * 新增消息发送记录
     * @param messageRecord 消息发送记录
     * @return R
     */
    @Inner
    @SysLog("新增消息发送记录")
    @PostMapping("/add")
    public R save(@RequestBody MessageRecord messageRecord) {
        return new R<>(messageRecordService.save(messageRecord));
    }

    /**
     * 修改消息发送记录
     * @param messageRecord 消息发送记录
     * @return R
     */
    @Inner
    @SysLog("修改消息发送记录")
    @PostMapping("/update")
    public R updateById(@RequestBody MessageRecord messageRecord) {
        return new R<>(messageRecordService.updateById(messageRecord));
    }

    /**
     * 通过id删除消息发送记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除消息发送记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(messageRecordService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除消息发送记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(messageRecordService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询消息发送记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(messageRecordService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param messageRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询消息发送记录总记录}")
    @PostMapping("/countByMessageRecordReq")
    public R countByMessageRecordReq(@RequestBody MessageRecordReq messageRecordReq) {

        return new R<>(messageRecordService.count(Wrappers.query(messageRecordReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param messageRecordReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据messageRecordReq查询一条货位信息表")
    @PostMapping("/getOneByMessageRecordReq")
    public R getOneByMessageRecordReq(@RequestBody MessageRecordReq messageRecordReq) {

        return new R<>(messageRecordService.getOne(Wrappers.query(messageRecordReq), false));
    }


    /**
     * 批量修改OR插入
     * @param messageRecordList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<MessageRecord> messageRecordList) {

        return new R<>(messageRecordService.saveOrUpdateBatch(messageRecordList));
    }

    @Inner
    @PostMapping("/findRecordPage")
    public R<IPage<MsgInfoVO>> findRecordPage(@RequestBody QueryMessageFeignReq req) {
        return R.ok(messageCoreService.findPage(new Page<>(req.getCurrent(), req.getSize()), req));
    }

}
