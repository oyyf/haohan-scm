/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.message.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.message.entity.WechatMessageTemplate;
import com.haohan.cloud.scm.api.message.req.WechatMessageTemplateReq;
import com.haohan.cloud.scm.message.service.WechatMessageTemplateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 消息模板
 *
 * @author haohan
 * @date 2019-05-29 13:48:04
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wechatmessagetemplate" )
@Api(value = "wechatmessagetemplate", tags = "wechatmessagetemplate管理")
public class WechatMessageTemplateController {

    private final WechatMessageTemplateService wechatMessageTemplateService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param wechatMessageTemplate 消息模板
     * @return
     */
    @GetMapping("/page" )
    public R getWechatMessageTemplatePage(Page page, WechatMessageTemplate wechatMessageTemplate) {
        return new R<>(wechatMessageTemplateService.page(page, Wrappers.query(wechatMessageTemplate)));
    }


    /**
     * 通过id查询消息模板
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(wechatMessageTemplateService.getById(id));
    }

    /**
     * 新增消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @SysLog("新增消息模板" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagetemplate_add')" )
    public R save(@RequestBody WechatMessageTemplate wechatMessageTemplate) {
        return new R<>(wechatMessageTemplateService.save(wechatMessageTemplate));
    }

    /**
     * 修改消息模板
     * @param wechatMessageTemplate 消息模板
     * @return R
     */
    @SysLog("修改消息模板" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagetemplate_edit')" )
    public R updateById(@RequestBody WechatMessageTemplate wechatMessageTemplate) {
        return new R<>(wechatMessageTemplateService.updateById(wechatMessageTemplate));
    }

    /**
     * 通过id删除消息模板
     * @param id id
     * @return R
     */
    @SysLog("删除消息模板" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagetemplate_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(wechatMessageTemplateService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除消息模板")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagetemplate_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageTemplateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询消息模板")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(wechatMessageTemplateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询消息模板总记录}")
    @PostMapping("/countByWechatMessageTemplateReq")
    public R countByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {

        return new R<>(wechatMessageTemplateService.count(Wrappers.query(wechatMessageTemplateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param wechatMessageTemplateReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据wechatMessageTemplateReq查询一条货位信息表")
    @PostMapping("/getOneByWechatMessageTemplateReq")
    public R getOneByWechatMessageTemplateReq(@RequestBody WechatMessageTemplateReq wechatMessageTemplateReq) {

        return new R<>(wechatMessageTemplateService.getOne(Wrappers.query(wechatMessageTemplateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param wechatMessageTemplateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('scm_wechatmessagetemplate_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<WechatMessageTemplate> wechatMessageTemplateList) {

        return new R<>(wechatMessageTemplateService.saveOrUpdateBatch(wechatMessageTemplateList));
    }


}
