/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.message.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.message.entity.InMailRecord;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.message.mapper.InMailRecordMapper;
import com.haohan.cloud.scm.message.service.InMailRecordService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 站内信记录表
 *
 * @author haohan
 * @date 2019-05-13 18:34:31
 */
@Service
@AllArgsConstructor
public class InMailRecordServiceImpl extends ServiceImpl<InMailRecordMapper, InMailRecord> implements InMailRecordService {

    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(InMailRecord entity) {
        if (StrUtil.isEmpty(entity.getInMailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(InMailRecord.class, NumberPrefixConstant.IN_MAIL_RECORD_SN_PRE);
            entity.setInMailSn(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    @Override
    public InMailRecord fetchBySn(String messageSn) {
        List<InMailRecord> list = baseMapper.selectList(Wrappers.<InMailRecord>query().lambda()
                .eq(InMailRecord::getInMailSn, messageSn)
        );
        return CollUtil.isEmpty(list) ? null : list.get(0);
    }

}
