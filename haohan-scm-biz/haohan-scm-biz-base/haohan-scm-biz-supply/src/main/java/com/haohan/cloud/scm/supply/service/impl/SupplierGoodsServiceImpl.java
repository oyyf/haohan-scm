/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.supply.dto.SupplyGoodsDTO;
import com.haohan.cloud.scm.api.supply.dto.SupplySqlDTO;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.supply.mapper.SupplierGoodsMapper;
import com.haohan.cloud.scm.supply.service.SupplierGoodsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-13 17:44:41
 */
@Service
public class SupplierGoodsServiceImpl extends ServiceImpl<SupplierGoodsMapper, SupplierGoods> implements SupplierGoodsService {

    /**
     * 查询一个供应管理关系
     *
     * @param merchantId
     * @param supplyModelId
     * @param platformModelId 可null
     * @return
     */
    @Override
    public SupplierGoods fetchOneByModel(String merchantId, String supplyModelId, String platformModelId) {
        return baseMapper.selectList(Wrappers.<SupplierGoods>query().lambda()
                .eq(SupplierGoods::getSupplierMerchantId, merchantId)
                .eq(StrUtil.isNotEmpty(supplyModelId), SupplierGoods::getSupplyModelId, supplyModelId)
                .eq(StrUtil.isNotEmpty(platformModelId), SupplierGoods::getGoodsModelId, platformModelId)
        ).stream().findFirst().orElse(null);
    }

    @Override
    public List<SupplierGoods> findListByPlatform(String goodsModelId, UseStatusEnum status) {
        return baseMapper.selectList(Wrappers.<SupplierGoods>query().lambda()
                .eq(SupplierGoods::getGoodsModelId, goodsModelId)
                .eq(null != status, SupplierGoods::getStatus, status)
        );
    }

    /**
     * 联查供应商表, 带商家名称
     * @param query 平台规格id
     * @return
     */
    @Override
    public List<SupplyGoodsDTO> findListWithExt(SupplySqlDTO query) {
        return baseMapper.findListWithExt(query);
    }
}
