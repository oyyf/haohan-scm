package com.haohan.cloud.scm.supply.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.OrderInfoDTO;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.req.order.SupplyOrderConfirmReq;
import com.haohan.cloud.scm.api.supply.req.order.SupplyOrderQueryReq;
import com.haohan.cloud.scm.api.supply.vo.SupplyBuyOrderInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyOrderVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyRelationBuyOrderVO;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/21
 */
public interface ScmSupplyOrderCoreService {

    /**
     * 分页查询
     *
     * @param page
     * @param req
     * @return
     */
    IPage<SupplyOrder> findPage(Page<SupplyOrder> page, SupplyOrderQueryReq req);

    /**
     * 供应订单详情
     *
     * @param supplySn
     * @return
     */
    SupplyOrderVO fetchInfo(String supplySn);

    /**
     * 根据客户采购单创建供应单
     *
     * @param buyOrderSn
     * @return
     */
    String addByBuyOrder(String buyOrderSn);

    /**
     * 根据报价单 新增供应订单
     *
     * @param supplyOrder
     * @param offerOrderList
     * @param isAdd
     * @return
     */
    SupplyOrder addOrderByOffer(SupplyOrder supplyOrder, List<OfferOrder> offerOrderList, boolean isAdd);

    /**
     * 供应订单确认
     *
     * @param req
     * @return
     */
    boolean orderConfirm(SupplyOrderConfirmReq req);

    /**
     * 根据id删除供应订单
     *
     * @param id
     * @return
     */
    boolean deleteOrder(String id);

    /**
     * 重新供应订单的信息 (供应商商品)
     *
     * @param supplySn
     * @return
     */
    OrderInfoDTO fetchOrderInfoBySupply(String supplySn);

    /**
     * 查询B客户采购的关联供应订单信息
     * 供应订单编号, 支付状态
     *
     * @param buyOrderSn
     * @return
     */
    SupplyRelationBuyOrderVO buyOrderRelation(String buyOrderSn);

    /**
     * 查询供应订单对应采购单信息
     * 采购单编号, 发货单编号
     *
     * @param supplySn
     * @return
     */
    SupplyBuyOrderInfoVO relationBuyInfo(String supplySn);
}
