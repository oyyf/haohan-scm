package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsEditReq;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsPlatformSalesReq;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsReq;
import com.haohan.cloud.scm.api.supply.vo.PlatformRelationModelVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyGoodsInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyGoodsVO;
import com.haohan.cloud.scm.common.tools.constant.SecondGroup;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplyGoodsCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/4/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supply/goods")
@Api(value = "ApiSupplyGoods", tags = "供应商品管理")
public class SupplyGoodsApiCtrl {

    private final ScmSupplyGoodsCoreService supplyGoodsCoreService;

    @GetMapping("/page")
    @ApiOperation(value = "查询供应商的商品列表")
    public R<IPage<SupplyGoodsVO>> findPage(Page page, @Validated SupplyGoodsReq req) {
        return RUtil.success(supplyGoodsCoreService.findPage(page, req));
    }

    @GetMapping("/info")
    @ApiOperation(value = "查询供应商的商品详情")
    public R<SupplyGoodsInfoVO> fetchInfo(@Validated({SingleGroup.class, Default.class}) SupplyGoodsReq req) {
        return RUtil.success(supplyGoodsCoreService.fetchInfo(req));
    }

    @GetMapping("/platformModel")
    @ApiOperation(value = "查询平台商品规格对应的供应商品信息")
    public R<PlatformRelationModelVO> platformModelRelation(String goodsModelId) {
        if(StrUtil.isEmpty(goodsModelId)){
            return R.failed("缺少参数goodsModelId");
        }
        return RUtil.success(supplyGoodsCoreService.platformModelRelation(goodsModelId));
    }

    @SysLog("供应商品新增到平台售卖")
    @PostMapping("/platformSales")
    @ApiOperation(value = "供应商品新增到平台售卖", notes = "spu关联")
    public R<Boolean> platformSales(@Validated SupplyGoodsPlatformSalesReq req) {
        return RUtil.success(supplyGoodsCoreService.platformSales(req));
    }

    @SysLog("供应商品新增到平台售卖")
    @DeleteMapping("/platformSales")
    @ApiOperation(value = "取消供应商品平台售卖", notes = "spu关联")
    public R<Boolean> removePlatformSales(@Validated SupplyGoodsPlatformSalesReq req) {
        return RUtil.success(supplyGoodsCoreService.removePlatformSales(req));
    }

    @SysLog("新增关联供应商品规格到平台")
    @PostMapping("/relation")
    @ApiOperation(value = "新增关联供应商品规格到平台", notes = "sku关联")
    public R<Boolean> relationGoodsModel(@Validated SupplyGoodsEditReq req) {
        return RUtil.success(supplyGoodsCoreService.relationGoodsModel(req));
    }

    @SysLog("取消供应商品关联")
    @DeleteMapping("/relation")
    @ApiOperation(value = "取消供应商品关联")
    public R<Boolean> removeRelationGoods(@Validated({SecondGroup.class}) SupplyGoodsEditReq req) {
        return RUtil.success(supplyGoodsCoreService.removeRelationGoods(req));
    }

}