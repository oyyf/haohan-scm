/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */
package com.haohan.cloud.scm.supply.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import com.haohan.cloud.scm.supply.mapper.SupplyOrderDetailMapper;
import com.haohan.cloud.scm.supply.service.SupplyOrderDetailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
@Service
@AllArgsConstructor
public class SupplyOrderDetailServiceImpl extends ServiceImpl<SupplyOrderDetailMapper, SupplyOrderDetail> implements SupplyOrderDetailService {
    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(SupplyOrderDetail entity) {
        if (StrUtil.isEmpty(entity.getSupplyDetailSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(SupplyOrderDetail.class, NumberPrefixConstant.SUPPLY_ORDER_DETAIL_SN_PRE);
            entity.setSupplyDetailSn(sn);
        }
        return super.save(entity);
    }

    @Override
    public List<SupplyOrderDetail> findListBySn(String supplySn) {
        return baseMapper.selectList(Wrappers.<SupplyOrderDetail>query().lambda()
                .eq(SupplyOrderDetail::getSupplySn, supplySn)
        );
    }


}
