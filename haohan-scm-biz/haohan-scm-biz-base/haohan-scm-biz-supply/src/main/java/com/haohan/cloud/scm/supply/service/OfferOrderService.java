/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;

import java.util.List;

/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-13 17:14:26
 */
public interface OfferOrderService extends IService<OfferOrder> {

    /**
     * 根据采购单明细编号列表  查询 已中标的报价单
     *
     * @param buyDetailSnList
     * @return
     */
    List<OfferOrder> findListByBuyDetailSn(List<String> buyDetailSnList);

}
