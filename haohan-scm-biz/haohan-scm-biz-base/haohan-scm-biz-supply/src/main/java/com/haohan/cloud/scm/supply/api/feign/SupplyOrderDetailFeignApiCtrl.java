/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderDetailReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.service.SupplyOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplyOrderDetail")
@Api(value = "supplyorderdetail", tags = "supplyorderdetail-供应订单明细内部接口服务}")
public class SupplyOrderDetailFeignApiCtrl {

    private final SupplyOrderDetailService supplyOrderDetailService;


    /**
     * 通过id查询供应订单明细
     *
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    @ApiOperation(value = "通过id查询供应订单明细")
    public R<SupplyOrderDetail> getById(@PathVariable("id") String id) {
        return R.ok(supplyOrderDetailService.getById(id));
    }


    /**
     * 分页查询 供应订单明细 列表信息
     *
     * @param supplyOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplyOrderDetailPage")
    @ApiOperation(value = "分页查询 供应订单明细 列表信息")
    public R<IPage<SupplyOrderDetail>> getSupplyOrderDetailPage(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq) {
        Page<SupplyOrderDetail> page = new Page<>(supplyOrderDetailReq.getPageNo(), supplyOrderDetailReq.getPageSize());
        SupplyOrderDetail supplyOrderDetail = new SupplyOrderDetail();
        BeanUtil.copyProperties(supplyOrderDetailReq, supplyOrderDetail);
        return R.ok(supplyOrderDetailService.page(page, Wrappers.query(supplyOrderDetail)));
    }


    /**
     * 全量查询 供应订单明细 列表信息
     *
     * @param supplyOrderDetailReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplyOrderDetailList")
    @ApiOperation(value = "全量查询 供应订单明细 列表信息")
    public R<List<SupplyOrderDetail>> getSupplyOrderDetailList(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq) {
        SupplyOrderDetail supplyOrderDetail = new SupplyOrderDetail();
        BeanUtil.copyProperties(supplyOrderDetailReq, supplyOrderDetail);
        return R.ok(supplyOrderDetailService.list(Wrappers.query(supplyOrderDetail)));
    }


    /**
     * 新增供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @return R
     */
    @Inner
    @SysLog("新增供应订单明细")
    @PostMapping("/add")
    @ApiOperation(value = "新增供应订单明细")
    public R<Boolean> save(@RequestBody SupplyOrderDetail supplyOrderDetail) {
        return R.ok(supplyOrderDetailService.save(supplyOrderDetail));
    }

    /**
     * 修改供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @return R
     */
    @Inner
    @SysLog("修改供应订单明细")
    @PostMapping("/update")
    @ApiOperation(value = "修改供应订单明细")
    public R<Boolean> updateById(@RequestBody SupplyOrderDetail supplyOrderDetail) {
        return R.ok(supplyOrderDetailService.updateById(supplyOrderDetail));
    }

    /**
     * 通过id删除供应订单明细
     *
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应订单明细")
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "通过id删除供应订单明细")
    public R<Boolean> removeById(@PathVariable String id) {
        return R.ok(supplyOrderDetailService.removeById(id));
    }

    /**
     * 删除（根据ID 批量删除)
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("批量删除供应订单明细")
    @PostMapping("/batchDelete")
    @ApiOperation(value = "通过IDS批量删除供应订单明细")
    public R<Boolean> removeByIds(@RequestBody List<String> idList) {
        return R.ok(supplyOrderDetailService.removeByIds(idList));
    }

    /**
     * 批量查询（根据IDS）
     *
     * @param idList 主键ID列表
     * @return R
     */
    @Inner
    @SysLog("根据IDS批量查询供应订单明细")
    @PostMapping("/listByIds")
    @ApiOperation(value = "根据IDS批量查询供应订单明细")
    public R<List<SupplyOrderDetail>> listByIds(@RequestBody List<String> idList) {
        return R.ok(new ArrayList<>(supplyOrderDetailService.listByIds(idList)));
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param req 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应订单明细总记录")
    @PostMapping("/countBySupplyOrderDetailReq")
    @ApiOperation(value = "查询供应订单明细总记录")
    public R<Integer> countBySupplyOrderDetailReq(@RequestBody SupplyOrderDetailReq req) {
        return RUtil.success(supplyOrderDetailService.count(Wrappers.<SupplyOrderDetail>query(req).lambda()
                .in(CollUtil.isNotEmpty(req.getModeIdSet()), SupplyOrderDetail::getGoodsModelId, req.getModeIdSet()))
        );
    }

    /**
     * 根据对象条件，查询一条记录
     *
     * @param supplyOrderDetailReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplyOrderDetailReq查询一条供应订单明细信息")
    @PostMapping("/getOneBySupplyOrderDetailReq")
    @ApiOperation(value = "根据supplyOrderDetailReq查询一条供应订单明细信息")
    public R<SupplyOrderDetail> getOneBySupplyOrderDetailReq(@RequestBody SupplyOrderDetailReq supplyOrderDetailReq) {
        return R.ok(supplyOrderDetailService.getOne(Wrappers.query(supplyOrderDetailReq), false));
    }

    /**
     * 批量修改OR插入供应订单明细
     *
     * @param supplyOrderDetailList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入供应订单明细数据")
    @PostMapping("/saveOrUpdateBatch")
    @ApiOperation(value = "批量修改OR插入供应订单明细数据")
    public R<Boolean> saveOrUpdateBatch(@RequestBody List<SupplyOrderDetail> supplyOrderDetailList) {
        return R.ok(supplyOrderDetailService.saveOrUpdateBatch(supplyOrderDetailList));
    }

}
