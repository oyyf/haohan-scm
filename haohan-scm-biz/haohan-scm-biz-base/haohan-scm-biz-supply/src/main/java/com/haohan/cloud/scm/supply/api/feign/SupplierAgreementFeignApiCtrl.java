/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierAgreement;
import com.haohan.cloud.scm.api.supply.req.SupplierAgreementReq;
import com.haohan.cloud.scm.supply.service.SupplierAgreementService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 协议供应商品记录
 *
 * @author haohan
 * @date 2019-05-29 13:13:43
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplierAgreement")
@Api(value = "supplieragreement", tags = "supplieragreement内部接口服务")
public class SupplierAgreementFeignApiCtrl {

    private final SupplierAgreementService supplierAgreementService;


    /**
     * 通过id查询协议供应商品记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierAgreementService.getById(id));
    }


    /**
     * 分页查询 协议供应商品记录 列表信息
     * @param supplierAgreementReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierAgreementPage")
    public R getSupplierAgreementPage(@RequestBody SupplierAgreementReq supplierAgreementReq) {
        Page page = new Page(supplierAgreementReq.getPageNo(), supplierAgreementReq.getPageSize());
        SupplierAgreement supplierAgreement =new SupplierAgreement();
        BeanUtil.copyProperties(supplierAgreementReq, supplierAgreement);

        return new R<>(supplierAgreementService.page(page, Wrappers.query(supplierAgreement)));
    }


    /**
     * 全量查询 协议供应商品记录 列表信息
     * @param supplierAgreementReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierAgreementList")
    public R getSupplierAgreementList(@RequestBody SupplierAgreementReq supplierAgreementReq) {
        SupplierAgreement supplierAgreement =new SupplierAgreement();
        BeanUtil.copyProperties(supplierAgreementReq, supplierAgreement);

        return new R<>(supplierAgreementService.list(Wrappers.query(supplierAgreement)));
    }


    /**
     * 新增协议供应商品记录
     * @param supplierAgreement 协议供应商品记录
     * @return R
     */
    @Inner
    @SysLog("新增协议供应商品记录")
    @PostMapping("/add")
    public R save(@RequestBody SupplierAgreement supplierAgreement) {
        return new R<>(supplierAgreementService.save(supplierAgreement));
    }

    /**
     * 修改协议供应商品记录
     * @param supplierAgreement 协议供应商品记录
     * @return R
     */
    @Inner
    @SysLog("修改协议供应商品记录")
    @PostMapping("/update")
    public R updateById(@RequestBody SupplierAgreement supplierAgreement) {
        return new R<>(supplierAgreementService.updateById(supplierAgreement));
    }

    /**
     * 通过id删除协议供应商品记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除协议供应商品记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierAgreementService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除协议供应商品记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierAgreementService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询协议供应商品记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierAgreementService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierAgreementReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询协议供应商品记录总记录}")
    @PostMapping("/countBySupplierAgreementReq")
    public R countBySupplierAgreementReq(@RequestBody SupplierAgreementReq supplierAgreementReq) {

        return new R<>(supplierAgreementService.count(Wrappers.query(supplierAgreementReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierAgreementReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierAgreementReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierAgreementReq")
    public R getOneBySupplierAgreementReq(@RequestBody SupplierAgreementReq supplierAgreementReq) {

        return new R<>(supplierAgreementService.getOne(Wrappers.query(supplierAgreementReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierAgreementList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SupplierAgreement> supplierAgreementList) {

        return new R<>(supplierAgreementService.saveOrUpdateBatch(supplierAgreementList));
    }

}
