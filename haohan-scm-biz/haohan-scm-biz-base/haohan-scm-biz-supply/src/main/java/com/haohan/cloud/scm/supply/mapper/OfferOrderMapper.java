/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;

/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-13 17:14:26
 */
public interface OfferOrderMapper extends BaseMapper<OfferOrder> {

}
