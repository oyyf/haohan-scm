/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.supply.service.SupplierGoodsService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-29 13:13:14
 */
@RestController
@AllArgsConstructor
@RequestMapping("/suppliergoods")
@Api(value = "suppliergoods", tags = "suppliergoods管理")
public class SupplierGoodsController {

    private final SupplierGoodsService supplierGoodsService;

    /**
     * 分页查询
     *
     * @param page          分页对象
     * @param supplierGoods 供应商货物表
     * @return
     */
    @GetMapping("/page")
    public R getSupplierGoodsPage(Page page, SupplierGoods supplierGoods) {
        return new R<>(supplierGoodsService.page(page, Wrappers.query(supplierGoods)));
    }


    /**
     * 通过id查询供应商货物表
     *
     * @param id id
     * @return R
     */
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierGoodsService.getById(id));
    }

    /**
     * 新增供应商货物表
     *
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @SysLog("新增供应商货物表")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_suppliergoods_add')")
    public R save(@RequestBody SupplierGoods supplierGoods) {
        return R.failed("不支持");
    }

    /**
     * 修改供应商货物表
     *
     * @param supplierGoods 供应商货物表
     * @return R
     */
    @SysLog("修改供应商货物表")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_suppliergoods_edit')")
    public R updateById(@RequestBody SupplierGoods supplierGoods) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除供应商货物表
     *
     * @param id id
     * @return R
     */
    @SysLog("删除供应商货物表")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('supply_suppliergoods_del')")
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


}
