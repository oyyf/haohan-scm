/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.supply.service.OfferOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-29 13:10:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/offerorder" )
@Api(value = "offerorder", tags = "offerorder管理")
public class OfferOrderController {

    private final OfferOrderService offerOrderService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param offerOrder 报价单
     * @return
     */
    @GetMapping("/page" )
    public R getOfferOrderPage(Page page, OfferOrder offerOrder) {
        return new R<>(offerOrderService.page(page, Wrappers.query(offerOrder).orderByDesc("offer_order_id")));
    }


    /**
     * 通过id查询报价单
     * @param id id
     * @return R
     */
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) String id) {
        return new R<>(offerOrderService.getById(id));
    }

    /**
     * 新增报价单
     * @param offerOrder 报价单
     * @return R
     */
    @SysLog("新增报价单" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_offerorder_add')" )
    public R save(@RequestBody OfferOrder offerOrder) {
        return new R<>(offerOrderService.save(offerOrder));
    }

    /**
     * 修改报价单
     * @param offerOrder 报价单
     * @return R
     */
    @SysLog("修改报价单" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_offerorder_edit')" )
    public R updateById(@RequestBody OfferOrder offerOrder) {
        return new R<>(offerOrderService.updateById(offerOrder));
    }

    /**
     * 通过id删除报价单
     * @param id id
     * @return R
     */
    @SysLog("删除报价单" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('supply_offerorder_del')" )
    public R removeById(@PathVariable String id) {
        return new R<>(offerOrderService.removeById(id));
    }


    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @SysLog("批量删除报价单")
    @PostMapping("/batchDelete")
    @PreAuthorize("@pms.hasPermission('supply_offerorder_del')" )
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(offerOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @SysLog("根据IDS批量查询报价单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(offerOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("查询报价单总记录}")
    @PostMapping("/countByOfferOrderReq")
    public R countByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq) {

        return new R<>(offerOrderService.count(Wrappers.query(offerOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @SysLog("根据offerOrderReq查询一条货位信息表")
    @PostMapping("/getOneByOfferOrderReq")
    public R getOneByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq) {

        return new R<>(offerOrderService.getOne(Wrappers.query(offerOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param offerOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    @PreAuthorize("@pms.hasPermission('supply_offerorder_edit')" )
    public R saveOrUpdateBatch(@RequestBody List<OfferOrder> offerOrderList) {

        return new R<>(offerOrderService.saveOrUpdateBatch(offerOrderList));
    }


}
