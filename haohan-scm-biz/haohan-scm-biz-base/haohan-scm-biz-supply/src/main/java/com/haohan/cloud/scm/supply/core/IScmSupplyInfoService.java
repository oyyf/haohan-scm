package com.haohan.cloud.scm.supply.core;

import com.haohan.cloud.scm.api.supply.entity.Supplier;

/**
 * @author dy
 * @date 2019/6/10
 */
public interface IScmSupplyInfoService {

    /**
     * 新增供应商
     * @param supplier
     *      必需参数: telephone/ SupplierName
     * @return
     */
    Supplier add(Supplier supplier);
}
