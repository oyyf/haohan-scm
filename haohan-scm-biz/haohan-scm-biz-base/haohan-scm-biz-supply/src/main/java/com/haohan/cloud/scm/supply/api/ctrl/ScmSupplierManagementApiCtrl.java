package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.AddSupplierReq;
import com.haohan.cloud.scm.api.supply.req.QuerySupplierListReq;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.haohan.cloud.scm.supply.core.IScmSupplyInfoService;
import com.haohan.cloud.scm.supply.service.SupplierService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author xwx
 * @date 2019/5/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supplierManagement")
@Api(value = "ApiScmSupplierManagement", tags = "scmSupplierManagement供应商管理api")
public class ScmSupplierManagementApiCtrl {
    private final SupplierService supplierService;
    private final IScmSupplyInfoService scmSupplyInfoService;
    private final ScmSupplyUtils scmSupplyUtils;

//    /**
//     * 查询供应商货款详情
//     * @param req   必须：id/pmId/uid
//     * @return
//     */
//    @GetMapping("/queryPayment")
//    @ApiOperation(value = "查询供应商货款详情")
//    public R querySupplierPayment(@Validated QueryPaymentDetailReq req) {
//        if (StrUtil.isEmpty(req.getUid())){
//            //判断供应商
//            scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
//        }
//
//        SupplierPayment payment = new SupplierPayment();
//        payment.setId(req.getId());
//        payment.setPmId(req.getPmId());
//        return new R<>(supplierPaymentService.getOne(Wrappers.query(payment)));
//    }
//
//    /**
//     * 查询供应商货款列表
//     * @param req  必须：pmId/uId
//     * @return
//     */
//    @GetMapping("/queryPaymentList")
//    @ApiOperation(value = "查询供应商货款列表")
//    public R querySupplierPaymentList(@Validated QueryPaymentListReq req) {
//        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
//        Page page = new Page(req.getPageNo(), req.getPageSize());
//        SupplierPayment supplierPayment = new SupplierPayment();
//        supplierPayment.setSupplierId(supplier.getId());
//        supplierPayment.setStatus(req.getStatus());
//        return new R<>(supplierPaymentService.page(page, Wrappers.query(supplierPayment)));
//    }

    /**
     * 查询供应商详情
     * @param req   必须：pmId/uId/id
     * @return
     */
    @GetMapping("/querySupplier")
    @ApiOperation(value = "查询供应商详情-小程序")
    public R querySupplier(@Validated SupplierReq req) {
        scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Supplier supplier = new Supplier();
        supplier.setPmId(req.getPmId());
        supplier.setId(req.getId());
        return new R<>(supplierService.getOne(Wrappers.query(supplier)));
    }

    /**
     * 查询供应商列表
     * @param req
     * @return
     */
    @GetMapping("/querySupplierList")
    @ApiOperation(value = "查询供应商列表-小程序")
    public R querySupplierList(@Validated QuerySupplierListReq req){
        scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Page page = new Page(req.getCurrent(), req.getSize());
        Supplier supplier = new Supplier();
        BeanUtil.copyProperties(req,supplier);
        return new R<>(supplierService.page(page,Wrappers.query(supplier)));
    }

    /**
     * 修改供应商信息
     * @param req   必须：pmId/uId
     * @return
     */
    @PostMapping("/updateSupplier")
    @ApiOperation(value = "修改供应商信息-小程序")
    public R updateSupplier(@Validated SupplierReq req){
        Supplier one = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Supplier supplier = new Supplier();
        BeanUtil.copyProperties(req,supplier);
        supplier.setId(one.getId());
        return new R<>(supplierService.updateById(supplier));
    }

    /**
     * 新增供应商
     * @param req  必须：telephone、SupplierName
     * @return
     */
    @PostMapping("/addSupplier")
    @ApiOperation(value = "新增供应商")
    public R addSupplier(@RequestBody @Validated AddSupplierReq req){
        Supplier supplier = new Supplier();
        BeanUtil.copyProperties(req,supplier);
        return new R<>(scmSupplyInfoService.add(supplier));
    }

}
