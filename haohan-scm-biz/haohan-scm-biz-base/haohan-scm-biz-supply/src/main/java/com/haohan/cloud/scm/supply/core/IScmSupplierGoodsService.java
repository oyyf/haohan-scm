package com.haohan.cloud.scm.supply.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.supply.req.AddSupplierGoodsReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsDetailReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsListReq;

/**
 * @author xwx
 * @date 2019/5/17
 */
public interface IScmSupplierGoodsService {

    /**
     * 查询供应商售卖商品信息列表
     * @param req
     * @return
     */
    Page querySupplierGoodsList (SupplierGoodsListReq req);

    /**
     * 新增供应商 售卖商品
     * @param req
     * @return
     */
    Boolean addSupplierGoods (AddSupplierGoodsReq req);

    /**
     * 查询供应商 售卖商品信息详情
     * @param req
     * @return
     */
    GoodsModelDTO querySupplierGoodsDetail(SupplierGoodsDetailReq req);

}
