package com.haohan.cloud.scm.supply.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.supplier.SupplierEditReq;
import com.haohan.cloud.scm.api.supply.req.supplier.SupplierQueryReq;
import com.haohan.cloud.scm.api.supply.vo.SupplierMerchantInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplierVO;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/16
 * 供应商功能
 */
public interface ScmSupplierCoreService {

    /**
     * 获取供应商的商家店铺 (采购配送店)
     *
     * @param supplierId
     * @return
     */
    SupplierMerchantInfoVO fetchMerchantInfo(String supplierId);

    /**
     * 查询商家下所有供应商
     *
     * @param merchantId
     * @return
     */
    List<Supplier> findListByMerchant(String merchantId);

    /**
     * 获取已有供应商的商家列表
     *
     * @return
     */
    List<MerchantVO> findMerchantListOfSupplier();

    IPage<SupplierVO> findPage(Page<Supplier> page, SupplierQueryReq req);

    SupplierVO fetchInfoWithRole(String supplierId);

    boolean addSupplier(SupplierEditReq req);


    boolean modifySupplier(SupplierEditReq req);

    /**
     * 供应商删除
     *
     * @param supplierId
     * @return
     */
    boolean deleteSupplier(String supplierId);

    boolean updateMerchantName(String merchantId, String merchantName);

}
