/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;

/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
public interface SupplyOrderMapper extends BaseMapper<SupplyOrder> {

}
