package com.haohan.cloud.scm.supply.core.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.goods.dto.GoodsModelDTO;
import com.haohan.cloud.scm.api.goods.entity.GoodsModel;
import com.haohan.cloud.scm.api.goods.req.GoodsModelReq;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;
import com.haohan.cloud.scm.api.supply.req.AddSupplierGoodsReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsDetailReq;
import com.haohan.cloud.scm.api.supply.req.SupplierGoodsListReq;
import com.haohan.cloud.scm.api.supply.resp.SupplierGoodsModelResp;
import com.haohan.cloud.scm.api.supply.trans.SupplierGoodsTrans;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.supply.core.IScmSupplierGoodsService;
import com.haohan.cloud.scm.supply.service.SupplierGoodsService;
import com.haohan.cloud.scm.supply.service.SupplierService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xwx
 * @date 2019/5/17
 */
@Service
public class ScmSupplierGoodsServiceImpl implements IScmSupplierGoodsService {

    @Autowired
    @Lazy
    private SupplierGoodsService supplierGoodsService;
    @Autowired
    @Lazy
    private ScmSupplyUtils scmSupplyUtils;
    @Autowired
    @Lazy
    private SupplierService supplierService;

    /**
     * 查询供应商 售卖商品信息列表
     * @param req
     * @return
     */
    @Override
    public Page querySupplierGoodsList(SupplierGoodsListReq req) {
        //查询供应商货物列表
        Page page = new Page(req.getPageNo(), req.getPageSize());
        SupplierGoods supplierGoods = new SupplierGoods();
        supplierGoods.setPmId(req.getPmId());
        supplierGoods.setSupplierId(req.getSupplierId());
        IPage iPage = supplierGoodsService.page(page, Wrappers.query(supplierGoods));
        //关联零售商品规格
        GoodsModelReq goodsModelReq = new GoodsModelReq();
        List<SupplierGoodsModelResp> list = new ArrayList<>();
        for (SupplierGoods goods : (List<SupplierGoods>)iPage.getRecords()) {
            goodsModelReq.setId(goods.getGoodsModelId());
            GoodsModelDTO dto = scmSupplyUtils.fetchGoodsModelDTO(goods.getGoodsModelId());
            SupplierGoodsModelResp res = new SupplierGoodsModelResp();
            BeanUtil.copyProperties(dto,res);
            res.setSupplierGoodsId(goods.getId());
            list.add(res);
        }
        Page goodsModelPage = new Page<>();
        goodsModelPage.setCurrent(iPage.getCurrent());
        goodsModelPage.setSize(iPage.getSize());
        goodsModelPage.setTotal(iPage.getTotal());
        goodsModelPage.setPages(iPage.getPages());
        goodsModelPage.setRecords(list);
        return goodsModelPage;
    }

    /**
     * 新增供应商 售卖商品
     * @param req
     * @return
     */
    @Override
    public Boolean addSupplierGoods(AddSupplierGoodsReq req) {
        //判断供应商是否被该采购员工管理
        scmSupplyUtils.fetchByBuyerUid(req.getUid(),req.getSupplierId());
        //查询供应商
        Supplier supplierReq = new Supplier();
        supplierReq.setPmId(req.getPmId());
        supplierReq.setId(req.getSupplierId());
        Supplier supplier = supplierService.getOne(Wrappers.query(supplierReq));
        if (supplier==null){
            throw new ErrorDataException("没有查到供应商信息");
        }
        //查询商品规格信息
        GoodsModelReq goodsModelReq = new GoodsModelReq();
        goodsModelReq.setId(req.getGoodsModelId());
        GoodsModel goodsModel = scmSupplyUtils.fetchGoodsModel(goodsModelReq);
        //根据商品规格id;sku 查询供应商 售卖商品
        SupplierGoods supplierGoods = new SupplierGoods();
        supplierGoods.setPmId(req.getPmId());
        supplierGoods.setSupplierId(req.getSupplierId());
        supplierGoods.setGoodsModelId(req.getGoodsModelId());
        SupplierGoods one = supplierGoodsService.getOne(Wrappers.query(supplierGoods));
        if (one==null){
            //新增
            SupplierGoods goods = SupplierGoodsTrans.trans(goodsModel, supplier);
            return supplierGoodsService.save(goods);
        }else {
            //修改
            SupplierGoods goods = new SupplierGoods();
            goods.setId(one.getId());
            goods.setStatus(req.getStatus());
            goods.setSupplyType(req.getSupplyType());
            return supplierGoodsService.updateById(goods);
        }

    }

    /**
     * 查询供应商 售卖商品信息详情
     * @param req
     * @return
     */
    @Override
    public GoodsModelDTO querySupplierGoodsDetail(SupplierGoodsDetailReq req) {
        //查询供应商货物对象
        SupplierGoods supplierGoods = new SupplierGoods();
        supplierGoods.setPmId(req.getPmId());
        supplierGoods.setId(req.getId());
        supplierGoods.setSupplierId(req.getSupplierId());
        SupplierGoods one = supplierGoodsService.getOne(Wrappers.query(supplierGoods));
        //查询商品规格信息 (关联了商品信息)
        GoodsModelDTO goodsModelDTO = scmSupplyUtils.fetchGoodsModelDTO(one.getGoodsModelId());
        return goodsModelDTO;
    }
}
