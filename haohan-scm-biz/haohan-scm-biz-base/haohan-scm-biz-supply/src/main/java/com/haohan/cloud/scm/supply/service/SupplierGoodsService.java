/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.constant.enums.common.UseStatusEnum;
import com.haohan.cloud.scm.api.supply.dto.SupplyGoodsDTO;
import com.haohan.cloud.scm.api.supply.dto.SupplySqlDTO;
import com.haohan.cloud.scm.api.supply.entity.SupplierGoods;

import java.util.List;

/**
 * 供应商货物表
 *
 * @author haohan
 * @date 2019-05-13 17:44:41
 */
public interface SupplierGoodsService extends IService<SupplierGoods> {

    /**
     * 查询一个供应管理关系
     * @param merchantId
     * @param supplyModelId
     * @param platformModelId 可null
     * @return
     */
    SupplierGoods fetchOneByModel(String merchantId, String supplyModelId, String platformModelId);

    /**
     * 查询平台商品规格 的 关联商品
     * @param goodsModelId
     * @param status
     * @return
     */
    List<SupplierGoods> findListByPlatform(String goodsModelId, UseStatusEnum status);

    /**
     * 联查供应商表, 带商家名称
     * @param query 平台规格id
     * @return
     */
    List<SupplyGoodsDTO> findListWithExt(SupplySqlDTO query);
}
