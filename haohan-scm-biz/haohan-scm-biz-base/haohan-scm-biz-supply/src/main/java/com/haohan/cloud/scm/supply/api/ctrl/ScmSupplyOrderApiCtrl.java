package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferTypeEnum;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.IScmSupplyOrderService;
import com.haohan.cloud.scm.supply.service.OfferOrderService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author  xwx
 * @date 2019/5/15.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supplyOrder")
@Api(value = "ApiSupplyOrder", tags = "supplyOrder报价单管理api")
public class ScmSupplyOrderApiCtrl {

    private final IScmSupplyOrderService scmSupplyOrderService;
    private final OfferOrderService offerOrderService;
    private final ScmSupplyUtils scmSupplyUtils;

    /**
     * 查询报价单详情
     * @param req  必须：pmId/uId/offerType/id
     * @return
     */
    @GetMapping("/queryOfferDetail")
    @ApiOperation(value = "查询报价单详情")
    public R queryOfferDetail(@RequestBody @Validated QueryOfferDetailReq req){
        if (req.getOfferType()== PdsOfferTypeEnum.supplierOffer){
            throw new ErrorDataException("不需要货源报价");
        }
        //查询供应商
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        OfferOrder offerOrder = new OfferOrder();
        offerOrder.setPmId(req.getPmId());
        offerOrder.setId(req.getId());
        offerOrder.setSupplierId(supplier.getId());
        offerOrder.setOfferType(req.getOfferType());
        return new R<>(offerOrderService.getOne(Wrappers.query(offerOrder)));
    }

    /**
     * 查询报价单列表
     * @param req 必须：pmId/uId
     * @return
     */
    @GetMapping("/queryOfferList")
    @ApiOperation(value = "查询报价单列表")
    public R queryOfferList(@Validated QueryOfferListReq req) {
        if (req.getOfferType()== PdsOfferTypeEnum.supplierOffer){
            throw new ErrorDataException("不需要货源报价");
        }
        //判断供应商是否存在
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Page page = new Page(req.getPageNo(), req.getPageSize());
        OfferOrder offerOrder =new OfferOrder();
        offerOrder.setSupplierId(supplier.getId());
        offerOrder.setOfferType(req.getOfferType());
        return new R<>(offerOrderService.page(page, Wrappers.query(offerOrder)));
    }

    /**
     * 查询货源信息列表
     * @param req 必须：pmId/uId
     * @return
     */
    @GetMapping("/querySupplyGoodsList")
    @ApiOperation(value = "查询货源信息列表小程序")
    public R querySupplyGoodsList(@Validated SupplyGoodsListReq req){
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Page page = new Page(req.getPageNo(), req.getPageSize());
        OfferOrder offerOrder =new OfferOrder();
        offerOrder.setSupplierId(supplier.getId());
        offerOrder.setOfferType(PdsOfferTypeEnum.supplierOffer);
        return new R<>(offerOrderService.page(page, Wrappers.query(offerOrder)));
    }

    /**
     * 查询货源信息详情
     * @param req  必须：id/pmId/uId
     * @return
     */
    @GetMapping("/querySupplyGoodsDetail")
    @ApiOperation(value = "查询货源信息详情小程序")
    public R querySupplyGoodsDetail(@Validated OfferOrderReq req){
        scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        if (StrUtil.isEmpty(req.getId())){
            return RUtil.error().setMsg("需要id");
        }
        OfferOrder offerOrder = new OfferOrder();
        BeanUtil.copyProperties(req,offerOrder);
        offerOrder.setOfferType(PdsOfferTypeEnum.supplierOffer);
        return new R<>(offerOrderService.getOne(Wrappers.query(offerOrder)));
    }

    /**
     * 查询供应商报价成交记录列表
     * @param req  必须：pmId/uId
     * @return
     */
    @GetMapping("/queryOfferSucceedList")
    @ApiOperation(value = "查询供应商报价/成交记录列表小程序")
    public R querySupplierOfferSucceedList(@Validated OfferOrderReq req){
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Page page = new Page(req.getPageNo(), req.getPageSize());
        OfferOrder offerOrder =new OfferOrder();
        offerOrder.setSupplierId(supplier.getId());
        offerOrder.setPmId(req.getPmId());
        offerOrder.setStatus(PdsOfferStatusEnum.bidding);
        return new R<>(offerOrderService.page(page, Wrappers.query(offerOrder)));
    }

    /**
     * 查询供应商报价成交记录详情
     * @param req  必须：pmId/uId/id
     * @return
     */
    @GetMapping("/queryOfferSucceedDetail")
    @ApiOperation(value = "查询供应商报价/成交记录详情小程序")
    public R querySupplierOfferSucceedDetail(@Validated OfferOrderReq req){
        scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        if (StrUtil.isEmpty(req.getId())){
            return RUtil.error().setMsg("需要id");
        }
        OfferOrder offerOrder = new OfferOrder();
        BeanUtil.copyProperties(req,offerOrder);
        offerOrder.setStatus(PdsOfferStatusEnum.bidding);
        return new R<>(offerOrderService.getOne(Wrappers.query(offerOrder)));
    }

    /**
     * 查询供应商品库存信息列表
     * @param req  必须：pmId/supplierId
     * @return
     */
    @GetMapping("/querySupplyGoodsRepertoryList")
    @ApiOperation(value = "查询供应商品库存信息列表")
    public R querySupplyGoodsRepertoryList(@Validated QueryGoodsRepertoryListReq req){

        Page page = scmSupplyOrderService.querySupplyGoodsRepertoryList(req);
        return new R<>(page);
    }

    /**
     * 查询供应商品库存详情
     * @param req   必须：id/pmId
     * @return
     */
    @GetMapping("/querySupplyGoodsRepertoryDetail")
    @ApiOperation(value = "查询供应商品库存详情")
    public R querySupplyGoodsRepertoryDetail(@Validated QueryGoodsRepertoryDetailReq req){

        return new R<>(scmSupplyOrderService.querySupplyGoodsRepertory(req));
    }

    /**
     * 供应商报价  (新增报价单 货源报价)
     * @param req  必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @PostMapping("/addSupplierOffer")
    public R addSupplierOffer(@Validated OfferOrderReq req){
        OfferOrder offerOrder = scmSupplyOrderService.addSupplierOffer(req);
        return new R<>(offerOrder);
    }

    /**
     * 供应商报价  (新增报价单 竞价采购)
     * @param req 必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @PostMapping("/addOfferBySupplier")
    public R addOfferBySupplier(@RequestBody @Validated OfferOrderReq req){
        OfferOrder offerOrder = scmSupplyOrderService.addOfferBySupplier(req);
        return new R<>(offerOrder);
    }

    /**
     * 采购员挑选供应商  (新增报价单  单品采购)
     * @param req 必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @PostMapping("/addOfferByPurchase")
    public R addOfferByPurchase(@Validated OfferOrderReq req){
        OfferOrder offerOrder = scmSupplyOrderService.addOfferByPurchase(req);
        return new R<>(offerOrder);
    }

    /**
     * 根据供应协议设置供应商 (新增报价单 协议采购)
     * @param req 必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @PostMapping("/addOfferByAgreement")
    public R addOfferByAgreement(@Validated OfferOrderReq req){
        OfferOrder offerOrder = scmSupplyOrderService.addOfferByAgreement(req);
        return new R<>(offerOrder);
    }

    /**
     * 查询供应商货源（加筛选条件）
     * @param req
     * @return
     */
    @PostMapping("/filterOfferOrder")
    @ApiOperation(value = "查询供应商货源（加筛选条件）")
    public R filterOfferOrder(@RequestBody @Validated FilterOfferOrderReq req){

        return new R<>(scmSupplyOrderService.filterOfferOrder(req));
    }

    /**
     * 供应商报价
     * @param req
     * @return
     */
    @PostMapping("/affirmOffer")
    @ApiOperation(value = "供应商报价")
    public R affirmOffer(@RequestBody @Validated AffirmOfferReq req){

        return new R<>(scmSupplyOrderService.affirmOffer(req));
    }

    /**
     * 修改报价单
     * @param req
     * @return
     */
    @PostMapping("/updateOfferOrder")
    @ApiOperation(value = "修改报价单")
    public R updateOfferOrder(@RequestBody @Validated UpdateOfferOrderReq req){

        return new R<>(scmSupplyOrderService.updateOfferOrder(req));
    }
}
