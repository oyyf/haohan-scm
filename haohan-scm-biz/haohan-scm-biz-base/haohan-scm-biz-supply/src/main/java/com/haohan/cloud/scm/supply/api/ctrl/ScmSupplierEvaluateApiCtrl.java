package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.entity.UPassport;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;
import com.haohan.cloud.scm.api.supply.req.AddSupplierEvaluateReq;
import com.haohan.cloud.scm.api.supply.req.SupplierEvaluateListReq;
import com.haohan.cloud.scm.api.supply.req.UpdateSupplierEvaluateReq;
import com.haohan.cloud.scm.api.supply.trans.SupplierEvaluateTrans;
import com.haohan.cloud.scm.supply.service.SupplierEvaluateService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @date 2019/5/17
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supplierEvaluate")
@Api(value = "ApiScmSupplierEvaluate", tags = "scmSupplierEvaluate供应商评价管理(给采购部用的接口)api")
public class ScmSupplierEvaluateApiCtrl {

    @Autowired
    @Lazy
    private SupplierEvaluateService supplierEvaluateService;

    @Autowired
    @Lazy
    private ScmSupplyUtils scmSupplyUtils;

    /**
     * 查询供应商评价记录列表
     * @param req  必须：pmId/uId/supplierId
     * @return
     */
    @GetMapping("/queryList")
    @ApiOperation(value = "查询供应商评价记录")
    public R querySupplierEvaluateList(@Validated SupplierEvaluateListReq req) {
        if (!StrUtil.isEmpty(req.getUid())) {
            //判断供应商是否被该采购员工管理
            scmSupplyUtils.fetchByBuyerUid(req.getUid(), req.getSupplierId());
        }
        Page page = new Page(req.getPageNo(), req.getPageSize());
        SupplierEvaluate supplierEvaluate = new SupplierEvaluate();
        supplierEvaluate.setSupplierId(req.getSupplierId());
        supplierEvaluate.setPmId(req.getPmId());
        return new R<>(supplierEvaluateService.page(page, Wrappers.query(supplierEvaluate)));
    }

    /**
     * 新增供应商评价
     * @param req  必须：pmId/uid/supplierId/score
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增供应商评价-小程序")
    public R addSupplierEvaluate(@Validated AddSupplierEvaluateReq req) {
        //判断供应商是否被该采购员工管理
        scmSupplyUtils.fetchByBuyerUid(req.getUid(), req.getSupplierId());
        //查询登录人
        UPassport uPassport = scmSupplyUtils.queryUPassportById(req.getUid());
        //查询供应商
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getSupplierId());
        SupplierEvaluate supplierEvaluate = SupplierEvaluateTrans.trans(supplier, uPassport);
        supplierEvaluate.setScore(req.getScore());
        supplierEvaluate.setEvaluateDesc(req.getEvaluateDesc());
        return new R<>(supplierEvaluateService.save(supplierEvaluate));
    }

    /**
     * 修改供应商评价
     * @param req  必须：pmId/uid/id
     * @return
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改供应商评价-小程序")
    public R updateSupplierEvaluate(@Validated UpdateSupplierEvaluateReq req) {

        //查询供应商评价
        SupplierEvaluate supplierEvaluate = new SupplierEvaluate();
        supplierEvaluate.setPmId(req.getPmId());
        supplierEvaluate.setSupplierId(req.getId());
        SupplierEvaluate one = supplierEvaluateService.getOne(Wrappers.query(supplierEvaluate));
        //判断供应商是否被该采购员工管理
        scmSupplyUtils.fetchByBuyerUid(req.getUid(), one.getSupplierId());

        SupplierEvaluate evaluate = new SupplierEvaluate();
        evaluate.setId(one.getId());
        evaluate.setScore(req.getScore());
        evaluate.setEvaluateDesc(req.getEvaluateDesc());
        return new R<>(supplierEvaluateService.updateById(evaluate));
    }

}
