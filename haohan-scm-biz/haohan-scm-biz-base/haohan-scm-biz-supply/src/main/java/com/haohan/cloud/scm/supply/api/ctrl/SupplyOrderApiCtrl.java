package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.api.supply.req.order.SupplyAddByBuyOrderReq;
import com.haohan.cloud.scm.api.supply.req.order.SupplyOrderConfirmReq;
import com.haohan.cloud.scm.api.supply.resp.SupplyAddByBuyOrderResp;
import com.haohan.cloud.scm.api.supply.vo.SupplyBuyOrderInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyOrderVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyRelationBuyOrderVO;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplyOrderCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author dy
 * @date 2020/4/24
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supply/order")
@Api(value = "ApiSupplyOrder", tags = "供应订单管理")
public class SupplyOrderApiCtrl {

    private final ScmSupplyOrderCoreService supplyOrderCoreService;

    @GetMapping("/info")
    @ApiOperation(value = "查询供应商的订单详情")
    public R<SupplyOrderVO> fetchInfo(String supplySn) {
        if (StrUtil.isBlank(supplySn)) {
            return R.failed("缺少参数supplySn");
        }
        return RUtil.success(supplyOrderCoreService.fetchInfo(supplySn));
    }

    @SysLog("据采购单向供应商下单")
    @PostMapping("/addByBuyOrder")
    @ApiOperation(value = "据采购单向供应商下单", notes = "单个采购单")
    public R<SupplyAddByBuyOrderResp> addByBuyOrder(@Validated SupplyAddByBuyOrderReq req) {
        return RUtil.success(addByBuyOrderSingle(req.getBuyOrderSn()));
    }

    private SupplyAddByBuyOrderResp addByBuyOrderSingle(String buyOrderSn) {
        SupplyAddByBuyOrderResp result = new SupplyAddByBuyOrderResp();
        result.setBuyOrderSn(buyOrderSn);
        String msg;
        boolean flag = true;
        try {
            msg = "下单成功, 供应单为: " + supplyOrderCoreService.addByBuyOrder(buyOrderSn);
        } catch (ErrorDataException e) {
            msg = "下单失败: " + e.getMessage();
            flag = false;
        }
        result.setStatus(flag);
        result.setMsg(msg);
        return result;
    }

    @SysLog("批量据采购单向供应商下单")
    @PostMapping("/addByBuyOrderBatch")
    @ApiOperation(value = "批量据采购单向供应商下单", notes = "批量, 多个采购单")
    public R addByBuyOrderBatch(@RequestBody @Validated(SingleGroup.class) SupplyAddByBuyOrderReq req) {
        Map<String, Object> result = new HashMap<>(8);
        AtomicInteger successNum = new AtomicInteger();
        AtomicInteger failedNum = new AtomicInteger();
        List<SupplyAddByBuyOrderResp> list = req.getBuyOrderSnList().stream()
                .filter(StrUtil::isNotBlank)
                .map(sn -> {
                    SupplyAddByBuyOrderResp res = addByBuyOrderSingle(sn);
                    if (res.getStatus()) {
                        successNum.getAndIncrement();
                    } else {
                        failedNum.getAndIncrement();
                    }
                    return res;
                })
                .collect(Collectors.toList());
        result.put("successNum", successNum.get());
        result.put("failedNum", failedNum.get());
        result.put("list", list);
        return RUtil.success(result);
    }

    @SysLog("供应订单确认")
    @PostMapping("/confirm")
    @ApiOperation(value = "供应订单确认")
    public R<Boolean> orderConfirm(@Validated SupplyOrderConfirmReq req) {
        return RUtil.success(supplyOrderCoreService.orderConfirm(req));
    }

    /**
     * 供应订单编号, 支付状态
     *
     * @param buyOrderSn
     * @return
     */
    @GetMapping("/buyOrder/relation")
    @ApiOperation(value = "查询B客户采购的关联供应订单信息")
    public R<SupplyRelationBuyOrderVO> buyOrderRelation(String buyOrderSn) {
        if (StrUtil.isBlank(buyOrderSn)) {
            return R.failed("缺少参数buyOrderSn");
        }
        return RUtil.success(supplyOrderCoreService.buyOrderRelation(buyOrderSn));
    }

    /**
     * 采购单编号, 发货单编号
     *
     * @param supplySn
     * @return
     */
    @GetMapping("/buyInfo")
    @ApiOperation(value = "查询供应订单对应采购单信息")
    public R<SupplyBuyOrderInfoVO> relationBuyInfo(String supplySn) {
        if (StrUtil.isBlank(supplySn)) {
            return R.failed("缺少参数supplySn");
        }
        return RUtil.success(supplyOrderCoreService.relationBuyInfo(supplySn));
    }
}
