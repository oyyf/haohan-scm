package com.haohan.cloud.scm.supply.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsEditReq;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsPlatformSalesReq;
import com.haohan.cloud.scm.api.supply.req.goods.SupplyGoodsReq;
import com.haohan.cloud.scm.api.supply.vo.PlatformRelationModelVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyGoodsInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplyGoodsVO;

/**
 * @author dy
 * @date 2020/4/17
 */
public interface ScmSupplyGoodsCoreService {


    /**
     * 查询供应商的商品列表 已关联平台的商品标记
     *
     * @param page
     * @param req
     * @return
     */
    IPage<SupplyGoodsVO> findPage(Page page, SupplyGoodsReq req);

    /**
     * 查询供应商品详情
     *
     * @param req
     * @return
     */
    SupplyGoodsInfoVO fetchInfo(SupplyGoodsReq req);

    /**
     * 新增关联供应商品规格到平台
     *
     * @param req
     * @return
     */
    boolean relationGoodsModel(SupplyGoodsEditReq req);

    /**
     * 取消供应商品关联
     *
     * @param req
     * @return
     */
    boolean removeRelationGoods(SupplyGoodsEditReq req);

    /**
     * 供应商品新增到平台售卖
     *
     * @param req
     * @return
     */
    boolean platformSales(SupplyGoodsPlatformSalesReq req);

    /**
     * 取消供应商品平台售卖
     *
     * @param req
     * @return
     */
    boolean removePlatformSales(SupplyGoodsPlatformSalesReq req);

    /**
     * 查询平台商品规格对应的供应商品信息
     *
     * @param goodsModelId
     * @return
     */
    PlatformRelationModelVO platformModelRelation(String goodsModelId);

    /**
     * 删除商品的供应关联关系
     *
     * @param goodsId
     * @return
     */
    boolean deleteRelationByGoods(String goodsId);
}
