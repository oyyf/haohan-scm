/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;
import com.haohan.cloud.scm.api.supply.req.SupplyOrderDetailReq;
import com.haohan.cloud.scm.supply.service.SupplyOrderDetailService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/supplyorderdetail")
@Api(value = "supplyorderdetail", tags = "供应订单明细管理")
public class SupplyOrderDetailController {

    private final SupplyOrderDetailService supplyOrderDetailService;

    /**
     * 分页查询
     *
     * @param page              分页对象
     * @param supplyOrderDetail 供应订单明细
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public R getSupplyOrderDetailPage(Page page, SupplyOrderDetail supplyOrderDetail) {
        return R.ok(supplyOrderDetailService.page(page, Wrappers.query(supplyOrderDetail)));
    }


    /**
     * 通过id查询供应订单明细
     *
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(supplyOrderDetailService.getById(id));
    }

    /**
     * 新增供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @return R
     */
    @ApiOperation(value = "新增供应订单明细", notes = "新增供应订单明细")
    @SysLog("新增供应订单明细")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_supplyorderdetail_add')")
    public R save(@RequestBody SupplyOrderDetail supplyOrderDetail) {
        return R.failed("不支持");
    }

    /**
     * 修改供应订单明细
     *
     * @param supplyOrderDetail 供应订单明细
     * @return R
     */
    @ApiOperation(value = "修改供应订单明细", notes = "修改供应订单明细")
    @SysLog("修改供应订单明细")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_supplyorderdetail_edit')")
    public R updateById(@RequestBody SupplyOrderDetail supplyOrderDetail) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除供应订单明细
     *
     * @param id id
     * @return R
     */
    @SysLog("删除供应订单明细")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('supply_supplyorderdetail_del')")
    @ApiOperation(value = "通过id删除供应订单明细")
    public R removeById(@PathVariable String id) {
        return R.failed("不支持");
    }


}
