package com.haohan.cloud.scm.supply.api.ctrl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.constant.enums.supply.PdsOfferStatusEnum;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.OfferOrderReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.service.OfferOrderService;
import com.haohan.cloud.scm.supply.utils.ScmSupplyUtils;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @date 2019/6/12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supplyByPurchase")
@Api(value = "ApiSupplyByPurchaseApiCtrl", tags = "supplyByPurchase报价单管理(给采购商的接口)api")
public class SupplyByPurchaseApiCtrl {

    @Autowired
    @Lazy
    private ScmSupplyUtils scmSupplyUtils;
    @Autowired
    @Lazy
    private OfferOrderService offerOrderService;

    /**
     * 查询供应商报价记录列表
     * @param req  必须：pmId/uId 、报价单状态:已报价
     * @return
     */
    @GetMapping("/querySupplierOfferSucceedList")
    @ApiOperation(value = "查询供应商报价记录列表小程序")
    public R querySupplierOfferSucceedList(@Validated OfferOrderReq req){
        Supplier supplier = scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        Page page = new Page(req.getPageNo(), req.getPageSize());
        OfferOrder offerOrder =new OfferOrder();
        offerOrder.setSupplierId(supplier.getId());
        offerOrder.setStatus(PdsOfferStatusEnum.alreadyOffer);
        return new R<>(offerOrderService.page(page, Wrappers.query(offerOrder)));
    }

    /**
     * 查询供应商报价记录详情
     * @param req  必须：pmId/uId/id 、报价单状态:已报价
     * @return
     */
    @GetMapping("/querySupplierOfferSucceed")
    @ApiOperation(value = "查询供应商报价记录详情小程序")
    public R querySupplierOfferSucceed(@Validated OfferOrderReq req){
        scmSupplyUtils.fetchByUid(req.getPmId(), req.getUid());
        if (StrUtil.isEmpty(req.getId())){
            return RUtil.error().setMsg("需要id");
        }
        OfferOrder offerOrder = new OfferOrder();
        BeanUtil.copyProperties(req,offerOrder);
        offerOrder.setStatus(PdsOfferStatusEnum.alreadyOffer);
        return new R<>(offerOrderService.getOne(Wrappers.query(offerOrder)));
    }
}
