/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.Supplier;
import com.haohan.cloud.scm.api.supply.req.SupplierReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.IScmSupplyInfoService;
import com.haohan.cloud.scm.supply.core.ScmSupplierCoreService;
import com.haohan.cloud.scm.supply.service.SupplierService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-29 13:13:47
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/Supplier")
@Api(value = "supplier", tags = "supplier内部接口服务")
public class SupplierFeignApiCtrl {

    private final SupplierService supplierService;
    private final ScmSupplierCoreService supplierCoreService;
    private final IScmSupplyInfoService scmSupplyInfoService;

    /**
     * 通过id查询供应商
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierService.getById(id));
    }


    /**
     * 分页查询 供应商 列表信息
     * @param supplierReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierPage")
    public R getSupplierPage(@RequestBody SupplierReq supplierReq) {
        Page page = new Page(supplierReq.getPageNo(), supplierReq.getPageSize());
        Supplier supplier =new Supplier();
        BeanUtil.copyProperties(supplierReq, supplier);

        return new R<>(supplierService.page(page, Wrappers.query(supplier)));
    }


    /**
     * 全量查询 供应商 列表信息
     * @param supplierReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierList")
    public R getSupplierList(@RequestBody SupplierReq supplierReq) {
        Supplier supplier =new Supplier();
        BeanUtil.copyProperties(supplierReq, supplier);

        return new R<>(supplierService.list(Wrappers.query(supplier)));
    }


    /**
     * 新增供应商
     * @param supplier 供应商
     * @return R
     */
    @Inner
    @SysLog("新增供应商")
    @PostMapping("/add")
    public R save(@RequestBody Supplier supplier) {
        return new R<>(supplierService.save(supplier));
    }

    /**
     * 修改供应商
     * @param supplier 供应商
     * @return R
     */
    @Inner
    @SysLog("修改供应商")
    @PostMapping("/update")
    public R updateById(@RequestBody Supplier supplier) {
        return new R<>(supplierService.updateById(supplier));
    }

    /**
     * 通过id删除供应商
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应商")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierService.removeById(id));
    }

    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询供应商")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应商总记录}")
    @PostMapping("/countBySupplierReq")
    public R<Integer> countBySupplierReq(@RequestBody SupplierReq supplierReq) {

        return new R<>(supplierService.count(Wrappers.query(supplierReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierReq")
    public R getOneBySupplierReq(@RequestBody SupplierReq supplierReq) {

        return new R<>(supplierService.getOne(Wrappers.query(supplierReq), false));
    }

    /**
     * 新增供应商
     * @param supplier 供应商
     * @return R
     */
    @Inner
    @SysLog("采购员新增供应商")
    @PostMapping("/addBuyerSupplier")
    public R addBuyerSupplier(@RequestBody Supplier supplier) {
        return new R<>(scmSupplyInfoService.add(supplier));
    }

    @Inner
    @SysLog("供应商商家名称修改")
    @PostMapping("/updateMerchantName")
    public R<Boolean> updateMerchantName(@RequestBody SupplierReq req) {
        return RUtil.success(supplierCoreService.updateMerchantName(req.getMerchantId(), req.getMerchantName()));
    }

}
