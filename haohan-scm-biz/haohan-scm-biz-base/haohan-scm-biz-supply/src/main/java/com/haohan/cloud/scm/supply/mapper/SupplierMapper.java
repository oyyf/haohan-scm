/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.haohan.cloud.scm.api.supply.dto.SupplySqlDTO;
import com.haohan.cloud.scm.api.supply.entity.Supplier;

import java.util.List;

/**
 * 供应商
 *
 * @author haohan
 * @date 2019-05-13 17:44:16
 */
public interface SupplierMapper extends BaseMapper<Supplier> {

    List<String> findMerchantList(SupplySqlDTO query);
}
