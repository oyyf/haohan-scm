/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrderDetail;

import java.util.List;

/**
 * 供应订单明细
 *
 * @author haohan
 * @date 2020-04-26 11:13:16
 */
public interface SupplyOrderDetailService extends IService<SupplyOrderDetail> {

    List<SupplyOrderDetail> findListBySn(String supplySn);
}
