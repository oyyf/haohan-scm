/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.OfferOrder;
import com.haohan.cloud.scm.api.supply.req.*;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.IScmSupplyOrderService;
import com.haohan.cloud.scm.supply.service.OfferOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 报价单
 *
 * @author haohan
 * @date 2019-05-29 13:10:55
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/OfferOrder")
@Api(value = "offerorder", tags = "offerorder内部接口服务")
public class OfferOrderFeignApiCtrl {
    private final OfferOrderService offerOrderService;
    private final IScmSupplyOrderService supplyOrderService;


    /**
     * 通过id查询报价单
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(offerOrderService.getById(id));
    }


    /**
     * 分页查询 报价单 列表信息
     * @param offerOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchOfferOrderPage")
    public R getOfferOrderPage(@RequestBody OfferOrderReq offerOrderReq) {
        Page page = new Page(offerOrderReq.getPageNo(), offerOrderReq.getPageSize());
        OfferOrder offerOrder =new OfferOrder();
        BeanUtil.copyProperties(offerOrderReq, offerOrder);
        IPage p = offerOrderService.page(page, Wrappers.query(offerOrder));
        return new R<>(p);
    }


    /**
     * 全量查询 报价单 列表信息
     * @param offerOrderReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchOfferOrderList")
    public R<List<OfferOrder>> getOfferOrderList(@RequestBody OfferOrderReq offerOrderReq) {
        return R.ok(offerOrderService.list(Wrappers.query(offerOrderReq)));
    }


    /**
     * 新增报价单
     * @param offerOrder 报价单
     * @return R
     */
    @Inner
    @SysLog("新增报价单")
    @PostMapping("/add")
    public R save(@RequestBody OfferOrder offerOrder) {
        return new R<>(offerOrderService.save(offerOrder));
    }

    /**
     * 修改报价单
     * @param offerOrder 报价单
     * @return R
     */
    @Inner
    @SysLog("修改报价单")
    @PostMapping("/update")
    public R updateById(@RequestBody OfferOrder offerOrder) {
        return new R<>(offerOrderService.updateById(offerOrder));
    }

    /**
     * 通过id删除报价单
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除报价单")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(offerOrderService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除报价单")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(offerOrderService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询报价单")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(offerOrderService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询报价单总记录}")
    @PostMapping("/countByOfferOrderReq")
    public R countByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq) {

        return new R<>(offerOrderService.count(Wrappers.query(offerOrderReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param offerOrderReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据offerOrderReq查询一条货位信息表")
    @PostMapping("/getOneByOfferOrderReq")
    public R<OfferOrder> getOneByOfferOrderReq(@RequestBody OfferOrderReq offerOrderReq) {
        return R.ok(offerOrderService.getOne(Wrappers.query(offerOrderReq), false));
    }


    /**
     * 批量修改OR插入
     * @param offerOrderList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<OfferOrder> offerOrderList) {

        return new R<>(offerOrderService.saveOrUpdateBatch(offerOrderList));
    }


    /**
     * 初始创建 报价单
     * @param offerOrderReq 请求对象
     *       必需参数: supplierId / supplyPrice / offerType / goodsModelId
     * @return OfferOrder
     */
    @Inner
    @PostMapping("/addOfferOrder")
    public R addOfferOrder(@RequestBody @Validated AddOfferOrderReq offerOrderReq) {
        OfferOrder offerOrder;
        try {
            offerOrder = supplyOrderService.addOfferOrder(offerOrderReq);
        }catch (Exception e){
            return RUtil.error().setMsg("创建报价单失败");
        }
        return new R<>(offerOrder);
    }
    /**
     * 查询供应商货源（加筛选条件）
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/filterOfferOrder")
    @SysLog("查询供应商货源（加筛选条件）")
    public R filterOfferOrder(@RequestBody @Validated FilterOfferOrderReq req){

        return new R<>(supplyOrderService.filterOfferOrder(req));
    }
    /**
     * 供应商报价  (新增报价单 货源报价)
     * @param req  必须：goodsModelId、supplierId、purchaseDetailSn
     * @return
     */
    @Inner
    @PostMapping("/addSupplierOffer")
    @SysLog("供应商报价  (新增报价单 货源报价)")
    public R addSupplierOffer(@Validated OfferOrderReq req){
        OfferOrder offerOrder = supplyOrderService.addSupplierOffer(req);
        return new R<>(offerOrder);
    }

    /**
     * 供应商报价
     * @param req
     * @return
     */
    @Inner
    @PostMapping("/affirmOffer")
    @SysLog("供应商报价")
    public R affirmOffer(@RequestBody AffirmOfferReq req){

        return new R<>(supplyOrderService.affirmOffer(req));
    }

    /**
     * 修改报价单
     * @param req
     * @return
     */
    @PostMapping("/updateOfferOrder")
    @ApiOperation(value = "修改报价单")
    public R updateOfferOrder(@RequestBody UpdateOfferOrderReq req){

        return new R<>(supplyOrderService.updateOfferOrder(req));
    }

}
