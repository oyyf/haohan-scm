/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.api.feign;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplierEvaluate;
import com.haohan.cloud.scm.api.supply.req.SupplierEvaluateReq;
import com.haohan.cloud.scm.supply.service.SupplierEvaluateService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;



/**
 * 供应商评价记录
 *
 * @author haohan
 * @date 2019-05-29 13:13:19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/SupplierEvaluate")
@Api(value = "supplierevaluate", tags = "supplierevaluate内部接口服务")
public class SupplierEvaluateFeignApiCtrl {

    private final SupplierEvaluateService supplierEvaluateService;


    /**
     * 通过id查询供应商评价记录
     * @param id id
     * @return R
     */
    @Inner
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return new R<>(supplierEvaluateService.getById(id));
    }


    /**
     * 分页查询 供应商评价记录 列表信息
     * @param supplierEvaluateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierEvaluatePage")
    public R getSupplierEvaluatePage(@RequestBody SupplierEvaluateReq supplierEvaluateReq) {
        Page page = new Page(supplierEvaluateReq.getPageNo(), supplierEvaluateReq.getPageSize());
        SupplierEvaluate supplierEvaluate =new SupplierEvaluate();
        BeanUtil.copyProperties(supplierEvaluateReq, supplierEvaluate);

        return new R<>(supplierEvaluateService.page(page, Wrappers.query(supplierEvaluate)));
    }


    /**
     * 全量查询 供应商评价记录 列表信息
     * @param supplierEvaluateReq 请求对象
     * @return
     */
    @Inner
    @PostMapping("/fetchSupplierEvaluateList")
    public R getSupplierEvaluateList(@RequestBody SupplierEvaluateReq supplierEvaluateReq) {
        SupplierEvaluate supplierEvaluate =new SupplierEvaluate();
        BeanUtil.copyProperties(supplierEvaluateReq, supplierEvaluate);

        return new R<>(supplierEvaluateService.list(Wrappers.query(supplierEvaluate)));
    }


    /**
     * 新增供应商评价记录
     * @param supplierEvaluate 供应商评价记录
     * @return R
     */
    @Inner
    @SysLog("新增供应商评价记录")
    @PostMapping("/add")
    public R save(@RequestBody SupplierEvaluate supplierEvaluate) {
        return new R<>(supplierEvaluateService.save(supplierEvaluate));
    }

    /**
     * 修改供应商评价记录
     * @param supplierEvaluate 供应商评价记录
     * @return R
     */
    @Inner
    @SysLog("修改供应商评价记录")
    @PostMapping("/update")
    public R updateById(@RequestBody SupplierEvaluate supplierEvaluate) {
        return new R<>(supplierEvaluateService.updateById(supplierEvaluate));
    }

    /**
     * 通过id删除供应商评价记录
     * @param id id
     * @return R
     */
    @Inner
    @SysLog("删除供应商评价记录")
    @PostMapping("/delete/{id}")
    public R removeById(@PathVariable String id) {
        return new R<>(supplierEvaluateService.removeById(id));
    }

    /**
      * 删除（根据ID 批量删除)
      * @param idList 主键ID列表
      * @return R
      */
    @Inner
    @SysLog("批量删除供应商评价记录")
    @PostMapping("/batchDelete")
    public R removeByIds(@RequestBody List<String> idList) {
        return new R<>(supplierEvaluateService.removeByIds(idList));
    }


    /**
    * 批量查询（根据IDS）
    * @param idList 主键ID列表
    * @return R
    */
    @Inner
    @SysLog("根据IDS批量查询供应商评价记录")
    @PostMapping("/listByIds")
    public R listByIds(@RequestBody List<String> idList) {
        return new R<>(supplierEvaluateService.listByIds(idList));
    }


    /**
     * 根据 Wrapper 条件，查询总记录数
     * @param supplierEvaluateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("查询供应商评价记录总记录}")
    @PostMapping("/countBySupplierEvaluateReq")
    public R countBySupplierEvaluateReq(@RequestBody SupplierEvaluateReq supplierEvaluateReq) {

        return new R<>(supplierEvaluateService.count(Wrappers.query(supplierEvaluateReq)));
    }


    /**
     * 根据对象条件，查询一条记录
     * @param supplierEvaluateReq 实体对象,可以为空
     * @return R
     */
    @Inner
    @SysLog("根据supplierEvaluateReq查询一条货位信息表")
    @PostMapping("/getOneBySupplierEvaluateReq")
    public R getOneBySupplierEvaluateReq(@RequestBody SupplierEvaluateReq supplierEvaluateReq) {

        return new R<>(supplierEvaluateService.getOne(Wrappers.query(supplierEvaluateReq), false));
    }


    /**
     * 批量修改OR插入
     * @param supplierEvaluateList 实体对象集合 大小不超过1000条数据
     * @return R
     */
    @Inner
    @SysLog("批量修改OR插入货位信息表")
    @PostMapping("/saveOrUpdateBatch")
    public R saveOrUpdateBatch(@RequestBody List<SupplierEvaluate> supplierEvaluateList) {

        return new R<>(supplierEvaluateService.saveOrUpdateBatch(supplierEvaluateList));
    }

}
