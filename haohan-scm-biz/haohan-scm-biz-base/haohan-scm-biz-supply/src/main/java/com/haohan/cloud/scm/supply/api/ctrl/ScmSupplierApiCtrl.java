package com.haohan.cloud.scm.supply.api.ctrl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.manage.vo.MerchantVO;
import com.haohan.cloud.scm.api.supply.req.supplier.SupplierMerchantInfoReq;
import com.haohan.cloud.scm.api.supply.vo.SupplierMerchantInfoVO;
import com.haohan.cloud.scm.api.supply.vo.SupplierVO;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplierCoreService;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author dy
 * @date 2020/4/16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/supply/supplier")
@Api(value = "ApiSupplier", tags = "供应商管理")
public class ScmSupplierApiCtrl {

    private final ScmSupplierCoreService supplierCoreService;

    @GetMapping("/merchantInfo")
    @ApiOperation(value = "获取供应商的商家店铺")
    public R<SupplierMerchantInfoVO> merchantInfo(@Validated SupplierMerchantInfoReq req) {
        return RUtil.success(supplierCoreService.fetchMerchantInfo(req.getSupplierId()));
    }

    @GetMapping("/merchantList")
    @ApiOperation(value = "获取已有供应商的商家列表")
    public R<IPage<MerchantVO>> merchantList() {
        Page<MerchantVO> page = new Page<>();
        List<MerchantVO> list = supplierCoreService.findMerchantListOfSupplier();
        page.setRecords(list);
        page.setSize(list.size());
        page.setSearchCount(false);
        return RUtil.success(page);
    }

    @GetMapping("/info")
    @ApiOperation(value = "获取供应商的详情")
    public R<SupplierVO> info(@Validated SupplierMerchantInfoReq req) {
        return RUtil.success(supplierCoreService.fetchInfoWithRole(req.getSupplierId()));
    }


}
