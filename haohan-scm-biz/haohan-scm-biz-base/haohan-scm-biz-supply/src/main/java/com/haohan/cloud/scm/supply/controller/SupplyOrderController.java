/*
 *    Copyright (c) 2018-2025, haohanwork.com All rights reserved.
 */

package com.haohan.cloud.scm.supply.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.supply.entity.SupplyOrder;
import com.haohan.cloud.scm.api.supply.req.order.SupplyOrderQueryReq;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.haohan.cloud.scm.supply.core.ScmSupplyOrderCoreService;
import com.haohan.cloud.scm.supply.service.SupplyOrderService;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 供应订单
 *
 * @author haohan
 * @date 2019-09-21 14:58:37
 */
@RestController
@AllArgsConstructor
@RequestMapping("/supplyorder")
@Api(value = "supplyorder", tags = "供应订单管理")
public class SupplyOrderController {

    private final SupplyOrderService supplyOrderService;
    private final ScmSupplyOrderCoreService supplyOrderCoreService;

    /**
     * 分页查询
     *
     * @param page 分页对象
     * @param req  供应订单
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page")
    public R<IPage<SupplyOrder>> getSupplyOrderPage(Page<SupplyOrder> page, @Validated SupplyOrderQueryReq req) {
        return RUtil.success(supplyOrderCoreService.findPage(page, req));
    }


    /**
     * 通过id查询供应订单
     *
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(supplyOrderService.getById(id));
    }

    /**
     * 新增供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @ApiOperation(value = "新增供应订单", notes = "新增供应订单")
    @SysLog("新增供应订单")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('supply_supplyorder_add')")
    public R save(@RequestBody SupplyOrder supplyOrder) {
        return R.failed("不支持");
    }

    /**
     * 修改供应订单
     *
     * @param supplyOrder 供应订单
     * @return R
     */
    @ApiOperation(value = "修改供应订单", notes = "修改供应订单")
    @SysLog("修改供应订单")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('supply_supplyorder_edit')")
    public R updateById(@RequestBody SupplyOrder supplyOrder) {
        return R.failed("不支持");
    }

    /**
     * 通过id删除供应订单
     *
     * @param id id
     * @return R
     */
    @SysLog("删除供应订单")
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('supply_supplyorder_del')")
    @ApiOperation(value = "通过id删除供应订单")
    public R removeById(@PathVariable String id) {
        return RUtil.success(supplyOrderCoreService.deleteOrder(id));
    }


}
