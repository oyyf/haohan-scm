package com.haohan.cloud.scm.bill.api.ctrl;

import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.req.BillManageReq;
import com.haohan.cloud.scm.bill.core.impl.PayableBillCoreServiceImpl;
import com.haohan.cloud.scm.bill.core.impl.ReceivableBillCoreServiceImpl;
import com.haohan.cloud.scm.common.tools.constant.SingleGroup;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2019/11/26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/bill/manage")
@Api(value = "BillManageApi", tags = "账单管理")
public class BillManageApiCtrl {

    private final ReceivableBillCoreServiceImpl receivableBillCoreService;
    private final PayableBillCoreServiceImpl payableBillCoreService;

    @SysLog("应收账单审核通过(创建结算单)")
    @PostMapping("/receivable/reviewSuccess")
    @ApiOperation(value = "账单审核通过(创建结算单)")
    public R<Boolean> receivableReviewSuccess(@Validated BillManageReq req) {
        if (receivableBillCoreService.reviewBill(req.getBillSn(), true, req.getRemarks())) {
            return RUtil.success(true);
        }
        return R.failed(false, "审核失败");
    }

    @SysLog("应收账单审核不通过")
    @PostMapping("/receivable/reviewFailed")
    @ApiOperation(value = "账单审核不通过")
    public R<Boolean> receivableReviewFailed(@Validated BillManageReq req) {
        if (receivableBillCoreService.reviewBill(req.getBillSn(), false, req.getRemarks())) {
            return RUtil.success(true);
        }
        return R.failed(false, "审核失败");
    }

    @SysLog("应收账单金额修改")
    @PostMapping("/receivable/modify")
    @ApiOperation(value = "账单金额修改")
    public R<Boolean> receivableModify(@Validated({SingleGroup.class, Default.class}) BillManageReq req) {
        BillInfoDTO bill = new BillInfoDTO();
        bill.setBillSn(req.getBillSn());
        bill.setBillAmount(req.getBillAmount());
        bill.setRemarks(req.getRemarks());
        return RUtil.success(receivableBillCoreService.modifyBill(bill));
    }

    // 应付账单

    @SysLog("应付账单审核通过(创建结算单)")
    @PostMapping("/payable/reviewSuccess")
    @ApiOperation(value = "账单审核通过(创建结算单)")
    public R<Boolean> payableReviewSuccess(@Validated BillManageReq req) {
        if (payableBillCoreService.reviewBill(req.getBillSn(), true, req.getRemarks())) {
            return RUtil.success(true);
        }
        return R.failed(false, "审核失败");
    }

    @SysLog("应付账单审核不通过")
    @PostMapping("/payable/reviewFailed")
    @ApiOperation(value = "账单审核不通过")
    public R<Boolean> payableReviewFailed(@Validated BillManageReq req) {
        if (payableBillCoreService.reviewBill(req.getBillSn(), false, req.getRemarks())) {
            return RUtil.success(true);
        }
        return R.failed(false, "审核失败");
    }

    @SysLog("应付账单金额修改")
    @PostMapping("/payable/modify")
    @ApiOperation(value = "账单金额修改")
    public R<Boolean> payableModify(@Validated({SingleGroup.class, Default.class}) BillManageReq req) {
        BillInfoDTO bill = new BillInfoDTO();
        bill.setBillSn(req.getBillSn());
        bill.setBillAmount(req.getBillAmount());
        bill.setRemarks(req.getRemarks());
        return RUtil.success(payableBillCoreService.modifyBill(bill));
    }

}
