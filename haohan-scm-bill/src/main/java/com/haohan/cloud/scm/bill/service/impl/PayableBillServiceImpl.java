package com.haohan.cloud.scm.bill.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.dto.BillSqlDTO;
import com.haohan.cloud.scm.api.bill.entity.PayableBill;
import com.haohan.cloud.scm.api.constant.NumberPrefixConstant;
import com.haohan.cloud.scm.api.constant.enums.opc.YesNoEnum;
import com.haohan.cloud.scm.api.crm.vo.app.BillPageVO;
import com.haohan.cloud.scm.bill.mapper.PayableBillMapper;
import com.haohan.cloud.scm.bill.service.PayableBillService;
import com.haohan.cloud.scm.common.tools.util.ScmIncrementUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/27
 */
@Service
@AllArgsConstructor
public class PayableBillServiceImpl extends ServiceImpl<PayableBillMapper, PayableBill> implements PayableBillService {
    private final ScmIncrementUtil scmIncrementUtil;

    @Override
    public boolean save(PayableBill entity) {
        if (StrUtil.isEmpty(entity.getBillSn())) {
            String sn = scmIncrementUtil.inrcSnByClass(PayableBill.class, NumberPrefixConstant.PAYABLE_BILL_SN_PRE);
            entity.setBillSn(sn);
        }
        return retBool(baseMapper.insert(entity));
    }

    @Override
    public PayableBill fetchBySn(String billSn) {
        return baseMapper.selectList(Wrappers.<PayableBill>query().lambda()
                .eq(PayableBill::getBillSn, billSn)).stream().findFirst().orElse(null);
    }

    /**
     * 查询订单的预付账单
     *
     * @param orderSn
     * @return
     */
    @Override
    public PayableBill fetchAdvanceByOrder(String orderSn) {
        return fetchByOrderSn(orderSn, YesNoEnum.yes);
    }

    private PayableBill fetchByOrderSn(String orderSn, YesNoEnum advanceFlag) {
        return baseMapper.selectList(Wrappers.<PayableBill>query().lambda()
                .eq(PayableBill::getAdvanceFlag, advanceFlag)
                .eq(PayableBill::getOrderSn, orderSn)).stream().findFirst().orElse(null);
    }

    /**
     * 查询订单的账单 (普通，非预付账单)
     *
     * @param orderSn
     * @return
     */
    @Override
    public PayableBill fetchNormalByOrder(String orderSn) {
        return fetchByOrderSn(orderSn, YesNoEnum.no);
    }

    /**
     * 按下单客户统计 账单金额
     *
     * @param params reviewStatus、billType、settlementStatus、customerIdSet
     *               excludeStatus
     *               current、size
     * @return customerId、customerName、billAmount
     */
    @Override
    public BillPageVO<BillInfoDTO> countBillByCustomer(BillSqlDTO params) {
        // 总数
        BillInfoDTO total = baseMapper.countBill(params);
        List<BillInfoDTO> list = baseMapper.countBillByCustomer(params);

        BillPageVO<BillInfoDTO> result = new BillPageVO<>(total.getTotal(), total.getBillAmount());
        result.setPage(params.getCurrent(), params.getSize());
        result.setRecords(list);
        return result;
    }

    /**
     * 账单统计 总数量、总金额
     *
     * @param params customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *               excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     * @return total、billAmount
     */
    @Override
    public BillInfoDTO countBill(BillSqlDTO params) {
        return baseMapper.countBill(params);
    }
}
