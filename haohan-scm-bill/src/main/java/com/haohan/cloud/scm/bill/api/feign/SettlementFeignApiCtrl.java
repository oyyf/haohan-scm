package com.haohan.cloud.scm.bill.api.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.bill.req.SettlementFeignReq;
import com.haohan.cloud.scm.api.bill.req.SummarySettlementReq;
import com.haohan.cloud.scm.api.bill.vo.SettlementInfoVO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;
import com.haohan.cloud.scm.bill.core.SettlementCoreService;
import com.haohan.cloud.scm.bill.service.SettlementAccountService;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author dy
 * @date 2019/12/28
 * 结算
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/feign/settlement")
@Api(value = "settlement", tags = "settlement 内部接口服务")
public class SettlementFeignApiCtrl {

    private final SettlementCoreService settlementCoreService;
    private final SettlementAccountService settlementAccountService;

    /**
     * 在线支付后完成结算
     *
     * @param req 结算必须参数
     *            settlementSn
     *            companyOperator
     *            settlementTime
     * @return
     */
    @Inner
    @PostMapping("/payToSettlement")
    public R<Boolean> payToSettlement(@RequestBody SettlementFeignReq req) {
        try {
            settlementCoreService.payToSettlement(req.transToDTO());
        } catch (Exception e) {
            e.printStackTrace();
            return RUtil.success(false);
        }
        return RUtil.success(true);
    }

    /**
     * 准备结算
     *
     * @param req settlementSn 和 settlementAmount
     * @return R
     */
    @Inner
    @PostMapping("/ready")
    public R<SettlementInfoVO> readySettlement(@RequestBody SettlementFeignReq req) {
        return RUtil.success(settlementCoreService.readySettlement(req.getSettlementSn(), req.getSettlementAmount()));
    }

    /**
     * 线上准备结算, 多账单合并后准备, 单个账单准备
     *
     * @param req settlementSn 和 settlementAmount
     * @return R
     */
    @Inner
    @PostMapping("/readyOnline")
    public R<SettlementInfoVO> readyOnlineSettlement(@RequestBody SettlementFeignReq req) {
        SummarySettlementReq settlementReq = new SummarySettlementReq();
        settlementReq.setBillSnList(req.getBillSnList());
        return RUtil.success(settlementCoreService.readyOnlineSettlement(settlementReq));
    }

    @Inner
    @PostMapping("/finish")
    public R<Boolean> finishSettlement(@RequestBody SettlementFeignReq req) {
        settlementCoreService.finishSettlement(req.transToDTO());
        return RUtil.success(true);
    }

    @Inner
    @PostMapping("/page")
    public R<IPage<SettlementInfoVO>> findPage(@RequestBody SettlementFeignReq req) {
        return RUtil.success(settlementCoreService.findPage(new Page<>(req.getCurrent(), req.getSize()), req));
    }

    @Inner
    @GetMapping("/fetchInfo")
    public R<SettlementInfoVO> fetchInfo(@RequestParam("settlementSn") String settlementSn) {
        return RUtil.success(settlementCoreService.fetchInfo(settlementSn));
    }

    @Inner
    @GetMapping("/fetchOne")
    public R<SettlementAccount> fetchSettlementAccount(@RequestParam("settlementSn") String settlementSn) {
        return RUtil.success(settlementAccountService.fetchBySn(settlementSn));
    }

    /**
     * 查询订单的支付状态(默认未付)
     *
     * @param orderSn
     * @return
     */
    @Inner
    @GetMapping("/orderPayStatus")
    public R<PayStatusEnum> queryOrderPayStatus(@RequestParam("orderSn") String orderSn) {
        return RUtil.success(settlementCoreService.queryOrderPayStatus(orderSn));
    }

    @Inner
    @SysLog("账单相关商家名称修改")
    @PostMapping("/updateMerchantName")
    public R<Boolean> updateMerchantName(@RequestBody SettlementFeignReq req) {
        return RUtil.success(settlementCoreService.updateMerchantName(req.getCompanyId(), req.getCompanyName()));
    }

}
