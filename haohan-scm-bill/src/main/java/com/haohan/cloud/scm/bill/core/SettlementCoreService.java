package com.haohan.cloud.scm.bill.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.SettlementDTO;
import com.haohan.cloud.scm.api.bill.entity.BaseBill;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;
import com.haohan.cloud.scm.api.bill.req.SettlementReq;
import com.haohan.cloud.scm.api.bill.req.SummarySettlementReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.bill.vo.SettlementInfoVO;
import com.haohan.cloud.scm.api.constant.enums.crm.PayStatusEnum;

import java.math.BigDecimal;

/**
 * @author dy
 * @date 2019/12/6
 */
public interface SettlementCoreService {

    /**
     * 详情
     *
     * @param settlementSn
     * @return
     */
    SettlementInfoVO fetchInfo(String settlementSn);

    /**
     * 分页查询
     *
     * @param page
     * @param req
     * @return
     */
    IPage<SettlementInfoVO> findPage(Page<SettlementAccount> page, SettlementReq req);

    /**
     * 是否汇总结算 设置汇总结算编号、状态
     *
     * @param settlementSn
     * @param billInfo
     */
    void checkSettlementSummary(String settlementSn, BillInfoVO billInfo);


    /**
     * 创建普通结算单，已存在则修改
     * 会修改账单状态(审核通过)、记录结算单号
     *
     * @param bill id/billSn/billType
     * @param <T>
     * @return
     */
    <T extends BaseBill> SettlementAccount createNormalSettlement(T bill);


    /**
     * 多个账单(已审核)
     * 合并结算, 创建汇总结算单
     *
     * @param req
     * @return
     */
    SettlementAccount createSummarySettlement(SummarySettlementReq req);


    /**
     * 准备结算 返回随机码用于验证
     *
     * @param settlementSn 结算单编号
     * @param amount       结算金额
     * @return
     */
    SettlementInfoVO readySettlement(String settlementSn, BigDecimal amount);


    /**
     * 线上准备结算, 多账单合并后准备, 单个账单准备
     *
     * @param req 账单编号列表
     * @return
     */
    SettlementInfoVO readyOnlineSettlement(SummarySettlementReq req);

    /**
     * 完成结算记录
     *
     * @param settlement
     * @return
     */
    void finishSettlement(SettlementDTO settlement);

    /**
     * 结算撤销 (结算单对应账单需重新审核, 结算单删除)
     *
     * @param settlementSn
     * @return
     */
    void revokeSettlement(String settlementSn);

    /**
     * 在线支付后完成结算
     *
     * @param settlement
     */
    void payToSettlement(SettlementDTO settlement);

    /**
     * 查询订单的支付状态
     *
     * @param orderSn
     * @return
     */
    PayStatusEnum queryOrderPayStatus(String orderSn);

    boolean updateMerchantName(String companyId, String companyName);
}
