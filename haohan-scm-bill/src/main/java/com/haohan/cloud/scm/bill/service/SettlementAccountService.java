package com.haohan.cloud.scm.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.api.bill.entity.SettlementAccount;

import java.util.List;

/**
 * @author dy
 * @date 2019/11/27
 */
public interface SettlementAccountService extends IService<SettlementAccount> {


    /**
     * 结算编号查询
     *
     * @param settlementSn
     * @return
     */
    SettlementAccount fetchBySn(String settlementSn);

    /**
     * 账单编号查询
     *
     * @param billSn
     * @return
     */
    SettlementAccount fetchByBillSn(String billSn);

    /**
     * 根据汇总结算单号 查询所有明细
     *
     * @param settlementSn
     * @return
     */
    List<SettlementAccount> fetchDetailOfSummarySn(String settlementSn);

    List<SettlementAccount> findListByOrderSn(String orderSn);
}
