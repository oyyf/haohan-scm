package com.haohan.cloud.scm.bill.core;

import com.haohan.cloud.scm.api.constant.enums.common.SettlementTypeEnum;
import com.haohan.cloud.scm.bill.core.impl.PayableBillCoreServiceImpl;
import com.haohan.cloud.scm.bill.core.impl.ReceivableBillCoreServiceImpl;
import com.haohan.cloud.scm.common.tools.exception.EmptyDataException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dy
 * @date 2020/3/6
 * 账单服务
 */
@Data
@Component
@AllArgsConstructor
public class BillServiceFactory {

    private final ReceivableBillCoreServiceImpl receivableBillService;
    private final PayableBillCoreServiceImpl payableBillService;
    private static Map<SettlementTypeEnum, BillCoreService> SERVICE_MAP = new HashMap<>(8);

    @PostConstruct
    private void init() {
        SERVICE_MAP.put(SettlementTypeEnum.receivable, receivableBillService);
        SERVICE_MAP.put(SettlementTypeEnum.payable, payableBillService);
    }

    /**
     * 获取账单服务对象
     *
     * @param type
     * @return
     */
    public static BillCoreService getBillService(SettlementTypeEnum type) {
        BillCoreService service = SERVICE_MAP.get(type);
        if (null == service) {
            throw new EmptyDataException("未注册订单服务对象");
        }
        return service;
    }

}
