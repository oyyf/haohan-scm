package com.haohan.cloud.scm.bill.api.ctrl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.req.BillInfoReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.bill.core.impl.PayableBillCoreServiceImpl;
import com.haohan.cloud.scm.bill.core.impl.ReceivableBillCoreServiceImpl;
import com.haohan.cloud.scm.common.tools.util.RUtil;
import com.pig4cloud.pigx.common.core.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dy
 * @date 2019/11/26
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/bill/query")
@Api(value = "BillQueryApi", tags = "账单查询")
public class BillQueryApiCtrl {

    private final ReceivableBillCoreServiceImpl receivableBillCoreService;
    private final PayableBillCoreServiceImpl payableBillCoreService;

    @GetMapping("/receivable/billInfo")
    @ApiOperation(value = "应收账单详情", notes = "根据billSn或orderSn查询")
    public R<BillInfoVO> queryReceivableBillInfo(@Validated BillInfoReq req) {
        if (StrUtil.isAllEmpty(req.getBillSn(), req.getOrderSn())) {
            return R.failed("缺少billSn或orderSn");
        }
        return RUtil.success(receivableBillCoreService.queryBillInfo(req.getBillSn(), req.getOrderSn()));
    }

    @GetMapping("/receivable/page")
    @ApiOperation(value = "应收账单分页查询")
    public R<IPage> queryReceivableBillPage(Page page, @Validated BillInfoReq req) {
        return RUtil.success(receivableBillCoreService.queryBillPage(page, req));
    }


    @GetMapping("/payable/billInfo")
    @ApiOperation(value = "应付账单详情", notes = "根据billSn或orderSn查询")
    public R<BillInfoVO> queryPayableBillInfo(@Validated BillInfoReq req) {
        if (StrUtil.isAllEmpty(req.getBillSn(), req.getOrderSn())) {
            return R.failed("缺少billSn或orderSn");
        }
        return RUtil.success(payableBillCoreService.queryBillInfo(req.getBillSn(), req.getOrderSn()));
    }

    @GetMapping("/payable/page")
    @ApiOperation(value = "应付账单分页查询")
    public R<IPage> queryPayableBillPage(Page page, @Validated BillInfoReq req) {
        return RUtil.success(payableBillCoreService.queryBillPage(page, req));
    }


}
