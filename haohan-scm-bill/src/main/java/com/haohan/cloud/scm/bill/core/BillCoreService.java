package com.haohan.cloud.scm.bill.core;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.haohan.cloud.scm.api.bill.dto.BillInfoDTO;
import com.haohan.cloud.scm.api.bill.entity.BaseBill;
import com.haohan.cloud.scm.api.bill.req.BillInfoReq;
import com.haohan.cloud.scm.api.bill.vo.BillInfoVO;
import com.haohan.cloud.scm.api.constant.enums.saleb.BillTypeEnum;
import com.haohan.cloud.scm.api.crm.vo.app.BillPageVO;

import java.util.Set;

/**
 * @author dy
 * @date 2019/11/26
 */
public interface BillCoreService<T extends BaseBill> {


    /**
     * 查询账单详情
     * 根据billSn或orderSn查询
     *
     * @param billSn
     * @param orderSn
     * @return
     */
    BillInfoVO queryBillInfo(String billSn, String orderSn);


    /**
     * 分页查询 账单列表
     *
     * @param page
     * @param req
     * @return
     * @paq
     */
    IPage<T> queryBillPage(Page<T> page, BillInfoReq req);

    /**
     * 创建账单 (若已存在，advanceAmount不同时修改)
     *
     * @param params 必需: orderSn/billType/advanceAmount
     * @return
     */
    BillInfoDTO createBill(BillInfoDTO params);

    /**
     * 账单审核通过（创建结算单）
     * 审核不通过 改变状态
     *
     * @param billSn
     * @param successFlag 审核通过 true
     * @param remarks
     * @return
     */
    Boolean reviewBill(String billSn, boolean successFlag, String remarks);

    /**
     * 审核通过账单重置 （状态改为待审核,未结算）
     *
     * @param billSn
     * @return
     */
    Boolean backBill(String billSn);

    /**
     * 修改账单金额
     * 账单审核状态：不为 审核通过
     *
     * @param params billSn、billAmount
     * @return
     */
    Boolean modifyBill(BillInfoDTO params);

    /**
     * 账单更新
     *
     * @param params
     */
    void updateBillById(BillInfoDTO params);

    /**
     * 账单完成结算, 更新订单支付状态
     *
     * @param billSn
     */
    void finishBillSettlement(String billSn);

    /**
     * 批量创建普通账单(未曾创建过)
     *
     * @param orderSnSet
     * @param billType
     * @return
     */
    boolean createNormalBillBatch(Set<String> orderSnSet, BillTypeEnum billType);

    /**
     * 根据订单编号获取账单信息
     *
     * @param orderSn
     * @return
     */
    BillInfoVO fetchNormalByOrder(String orderSn);

    /**
     * 按下单客户统计 账单金额
     *
     * @param req  customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *             excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     * @param page current、size
     * @return customerId、customerName、billAmount
     */
    BillPageVO<BillInfoDTO> countBillByCustomer(Page page, BillInfoReq req);

    /**
     * 账单统计 总数量、总金额
     *
     * @param req customerId、reviewStatus、billType、settlementStatus、customerIdSet
     *            excludeStatus、advanceFlag 、startDate、endDate(可单独使用)
     * @return total、billAmount
     */
    BillInfoDTO countBill(BillInfoReq req);

    /**
     * 修改账单中客户名称
     *
     * @param customerId
     * @param customerName
     * @return
     */
    boolean updateCustomerName(String customerId, String customerName);

    /**
     * 修改账单中的商家名称
     *
     * @param merchantId
     * @param merchantName
     * @return
     */
    boolean updateMerchantName(String merchantId, String merchantName);

    /**
     * 删除账单 不为已审核通过
     *
     * @param orderSn
     * @param billType
     * @return
     */
    boolean deleteBillByOrder(String orderSn, BillTypeEnum billType);

    /**
     * 根据订单信息更新账单(普通账单)
     * 无账单时无操作
     * 账单不能已结算
     *
     * @param orderSn
     * @param billType
     * @return
     */
    boolean updateBillByOrder(String orderSn, BillTypeEnum billType);
}
