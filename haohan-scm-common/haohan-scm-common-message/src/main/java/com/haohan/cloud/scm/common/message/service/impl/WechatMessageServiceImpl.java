package com.haohan.cloud.scm.common.message.service.impl;

import com.haohan.cloud.scm.common.message.entity.WechatMessageNotify;
import com.haohan.cloud.scm.common.message.service.WechatMessageService;
import org.springframework.stereotype.Service;

/**
 * @author cx
 * @date 2019/6/19
 */
@Service
public class WechatMessageServiceImpl implements WechatMessageService {

    @Override
    public String wechatNotify(WechatMessageNotify notify) {
        return null;
    }
}
