package com.haohan.cloud.scm.common.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author cx
 * @date 2019/6/19
 */
@Data
@ApiModel(description = "发送微信消息需求")
public class WechatMessageNotify {

    @NotBlank(message = "toUser不能为空")
    @ApiModelProperty(value = "接收人",required = true)
    private String toUser;

    @NotBlank(message = "templateId不能为空")
    @ApiModelProperty(value = "消息模板id",required = true)
    private String templateId;

    @NotNull(message = "departmentType不能为空")
    @ApiModelProperty(value = "业务部门类型:1供应2采购3生产4物流5市场6平台7门店8客户",required = true)
    private String departmentType;

    @NotBlank(message = "content不能为空")
    @ApiModelProperty(value = "通知内容",required = true)
    private String content;
}
