package com.haohan.cloud.scm.common.tools;

import com.haohan.cloud.scm.common.tools.util.PinYin4jUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author dy
 * @date 2019/10/22
 */
@Slf4j
public class PinyinTest {

    @Test
    public void test() throws Exception {
        String s = "白天";
        String p = PinYin4jUtils.fetchPinYin(s);
        log.debug("===============");
        log.debug(p);

        p = PinYin4jUtils.fetchFirst(s);
        log.debug("===============");
        log.debug(p);

        p = PinYin4jUtils.fetchFirst('多');
        log.debug("===============");
        log.debug(p);

    }
}
