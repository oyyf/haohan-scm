package com.haohan.cloud.scm.common.tools.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author dy
 * @date 2019/6/20
 * 工具类中的常量
 */
@Configuration
public interface ToolsConstant {

    /**
     * 主键 id  初始值
     */
    @Value("${redis.id.index}")
    int ID_START_NUM = 500;

    /**
     * 自定义 逻辑主键 初始值
     */
    @Value("${redis.sn.index}")
    int SN_START_INDEX = 10000;

    /**
     * 主键 id  缓存键值前缀
     */
    String ID_PREFIX = "scm-id:";

    /**
     * 逻辑主键 sn  缓存键值前缀
     */
    String SN_PREFIX = "scm-sn:";

    /**
     * excel文件最大导入行数
     */
    int MAX_IMPORT_ROW = 3000;

    /**
     * jedis 操作成功返回码
     */
    String JEDIS_SUCCEEDED_CODE = "OK";

}
