package com.haohan.cloud.scm.common.tools.constant;

import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2019/11/12
 * 用于分组校验,继承默认分组
 */
public interface SecondGroup extends Default {
}
