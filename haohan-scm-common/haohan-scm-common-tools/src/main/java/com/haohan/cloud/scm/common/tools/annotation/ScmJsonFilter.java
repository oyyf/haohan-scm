package com.haohan.cloud.scm.common.tools.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dy
 * @date 2019/5/17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ScmJsonFilter {
    Class<?> type();

    /**
     * 只显示的属性
     *
     * @return
     */
    String include() default "";

    /**
     * 不显示的属性
     *
     * @return
     */
    String filter() default "pmId,createBy,createDate,updateBy,updateDate,delFlag,tenantId";

    /**
     * 在filter 的基础上 添加显示的属性
     *
     * @return
     */
    String add() default "";
}
