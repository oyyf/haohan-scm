package com.haohan.cloud.scm.common.tools.exception;

/**
 * 数据错误时抛出
 *
 * @author dy
 * @date 2019/5/17
 */
public class ErrorDataException extends RuntimeException {


    public ErrorDataException() {
    }

    public ErrorDataException(String msg) {
        super(msg);
    }

    public ErrorDataException(Throwable cause) {
        super(cause);
    }

    public ErrorDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
