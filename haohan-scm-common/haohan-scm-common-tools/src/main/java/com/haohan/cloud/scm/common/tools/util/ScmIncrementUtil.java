package com.haohan.cloud.scm.common.tools.util;

import cn.hutool.core.util.StrUtil;
import com.haohan.cloud.scm.common.tools.constant.ToolsConstant;
import com.pig4cloud.pigx.common.data.tenant.TenantContextHolder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * @author dy
 * @date 2019/5/16
 */
@Slf4j
@Component
@AllArgsConstructor
public class ScmIncrementUtil implements ToolsConstant, Serializable {

    private static final long serialVersionUID = 1L;
    private final JedisUtils jedisUtils;

    /**
     * 使用类名首字母生成编号(废弃)
     *
     * @param t
     * @return
     */
    @Deprecated
    public String inrcSnByClass(Class t) {
        String name = t.getSimpleName();
        String prefix = name.substring(0, 1).toUpperCase();
        return prefix + jedisUtils.fetchIncrNum(name, SN_START_INDEX).toString();
    }

    /**
     * 使用前缀生成编号
     * key: 前缀 + 实体类类名 + ":"  + 租户id
     *
     * @param clazz
     * @param prefix
     * @return
     */
    public String inrcSnByClass(Class clazz, String prefix) {
        Integer tenantId = TenantContextHolder.getTenantId();
        String name = SN_PREFIX + clazz.getSimpleName() + ":" + tenantId;
        return prefix + tenantId + jedisUtils.fetchIncrNum(name, SN_START_INDEX).toString();
    }

    /**
     * 生成主键id 根据实体类名
     * key:  前缀 + 实体类类名
     *
     * @param clazz 实体类
     * @return
     */
    public String genId(Class clazz) {
        return jedisUtils.fetchIncrNum(ID_PREFIX + clazz.getSimpleName(), ID_START_NUM).toString();
    }

    public int genIdInt(Class clazz) {
        return jedisUtils.fetchIncrNum(ID_PREFIX + clazz.getSimpleName(), ID_START_NUM).intValue();
    }

    /**
     * 缓存获取值
     *
     * @param key
     * @return
     */
    public String fetchValue(String key) {
        return jedisUtils.get(key);
    }

    /**
     * 缓存设置值
     *
     * @param key
     * @param value
     * @param cacheSeconds 有效时间，0为不超时
     * @return
     */
    public boolean setValue(String key, String value, int cacheSeconds) {
        if (cacheSeconds < 0) {
            cacheSeconds = 1800;
        }
        String resp = jedisUtils.set(key, value, cacheSeconds);
        return StrUtil.equals(resp, JEDIS_SUCCEEDED_CODE);
    }

    /**
     * 获取Map缓存, 返回可为null
     *
     * @param key
     * @return
     */
    public Map<String, String> fetchMap(String key) {
        return jedisUtils.getMap(key);
    }

    /**
     * 向Map缓存中添加值
     *
     * @param key
     * @param valueMap 添加的键值对
     * @return
     */
    public boolean mapPut(String key, Map<String, String> valueMap) {
        String resp = jedisUtils.mapPut(key, valueMap);
        return StrUtil.equals(resp, JEDIS_SUCCEEDED_CODE);
    }

    /**
     * 查询是否存在缓存
     * 不存在时新增缓存, 及有效时间
     *
     * @param key
     * @param cacheSeconds
     * @return
     */
    public boolean existKey(String key, int cacheSeconds) {
        if (null == fetchValue(key)) {
            setValue(key, "begin", cacheSeconds);
            return false;
        }
        return true;
    }

}
