package com.haohan.cloud.scm.common.tools.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author dy
 * @date 2019/10/30
 */
@Getter
@Configuration
public class AliyunOssConfig {

    public static String ENDPOINT;

    public static String ACCESS_KEY_ID;

    public static String ACCESS_KEY_SECRET;

    public static String BUCKET_NAME;

    public static String FILE_BUCKET_HTTP;

    public static String MERCHANT_FILE_FOLDER;

    @Value("${aliyun.endpoint}")
    public void setEndpoint(String value) {
        ENDPOINT = value;
    }

    @Value("${aliyun.access_key_id}")
    public void setAccessKeyId(String value) {
        ACCESS_KEY_ID = value;
    }

    @Value("${aliyun.access_key_secret}")
    public void setAccessKeySecret(String value) {
        ACCESS_KEY_SECRET = value;
    }

    @Value("${aliyun.bucket_name}")
    public void setBucketName(String value) {
        BUCKET_NAME = value;
    }

    @Value("${aliyun.file_bucket_http}")
    public void setFileBucketHttp(String value) {
        FILE_BUCKET_HTTP = value;
    }

    @Value("${aliyun.merchant_file_folder}")
    public void setMerchantFileFolder(String value) {
        MERCHANT_FILE_FOLDER = value;
    }
}
