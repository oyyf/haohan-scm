package com.haohan.cloud.scm.common.tools.util;

import com.baomidou.mybatisplus.extension.service.IService;
import com.haohan.cloud.scm.common.tools.exception.ErrorDataException;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author dy
 * @date 2019/12/3
 * 使用mybatis-plus 的查询  封装方法
 */
@UtilityClass
public class ScmQueryUtils {

    /**
     * 从map中获取实体，不存在时通过service方法查询（getById、fetchBySn）
     * 查询不到时，返回null
     *
     * @param service 服务实例
     * @param map     已有实例 的映射
     * @param key     map的键
     * @param isId    true使用方法getById， false使用方法fetchBySn
     * @param <T>
     * @return
     */
    @SuppressWarnings({"unchecked"})
    public <T> T fetchEntity(IService<T> service, Map<String, T> map, String key, boolean isId) {
        T entity = null;
        try {
            if (map.containsKey(key)) {
                entity = map.get(key);
            } else {
                if (isId) {
                    entity = service.getById(key);
                } else {
                    Method fetchBySn = service.getClass().getMethod("fetchBySn", String.class);
                    entity = (T) fetchBySn.invoke(service, key);
                }
                if (null != entity) {
                    map.put(key, entity);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ErrorDataException("获取实体属性错误");
        }
        return entity;
    }

}
