package com.haohan.cloud.scm.common.tools.constant;

import javax.validation.groups.Default;

/**
 * @author dy
 * @date 2020/5/14
 * 用于分组校验,继承默认分组
 */
public interface ThreeGroup extends Default {
}

